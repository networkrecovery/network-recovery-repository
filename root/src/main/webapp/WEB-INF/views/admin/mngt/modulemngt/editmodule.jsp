<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/template_start.jsp"%>
<%@include file="../../inc/page_head.jsp"%>


<%-- <%@include file="../../inc/template_scripts.jsp"%> --%>

<div id="page-content">
	<div class="content-header">
		<div class="header-section">
			<h1>
				<spring:message code="heading.modulemaster" />
				<br> <small><spring:message
						code="heading.editmodule" /></small>
			</h1>
			<span id="errorMsg"></span>
			<c:if test="${!empty success}">
				<div class="alert alert-success alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-check-circle"></i> <spring:message code="label.success"/>
					</h4>
					<spring:message code="${success}" />
				</div>
			</c:if>

			<c:if test="${!empty error}">
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">�</button>
					<h4>
						<i class="fa fa-times-circle"></i> <spring:message code="label.error"/>
					</h4>
					<spring:message code="${error}" />
				</div>
			</c:if>
		</div>
	</div>
	<ul class="breadcrumb breadcrumb-top">
		<li><spring:message code="menu.module" /></li>
	</ul>
	<form action="<%=contexturl %>ManageModule/ModuleList/UpdateModule" method="post" class="form-horizontal "
		id="Module_form" enctype="multipart/form-data">
		<div class="row">
			<input name="moduleId" value="${module.moduleId}" type="hidden">
<%-- 			<input name="userId" value="${module.moduleId}" type="hidden"> --%>
			<div class="">
				<div class="block">
					<div class="block-title">
						<h2>
							<strong><spring:message code="heading.moduleinfo" /></strong>
<!-- 							<small style="float:left; margin-top: 4px;">This info is non editable and for view only purpose.</small> -->
						</h2>
					</div>
						
					<div class="form-group">
                        <label class="col-md-4 control-label"><spring:message code="label.modulename"/></label>
                        <div class="col-md-8">
                            <input id="moduleName" name="moduleName" class="form-control"
        					placeholder="<spring:message code='label.modulename'/>.."
        					type="text" value="${module.moduleName}">
                        </div>
                    </div>
                    
                    <div class="form-group">
           				<label class="col-md-4 control-label" for="moduleDescription"><spring:message code="label.decsription"/></label>
           				<div class="col-md-6">
           						<textarea id="moduleDescription" name="moduleDescription" 
           						class="form-control"
           						placeholder="<spring:message code='label.description'/>..">${module.moduleDescription}</textarea>
           				</div>
           			</div>
           			
           			
           			<div class="form-group img-list1" id="profilepic">
						<label class="col-md-4 control-label" for="offer_validtill"><spring:message
								code="label.moduleimage" /> <span class="text-danger">*</span></label>
						<div class="col-md-8">
							<div class="img-list2" id="imagediv">
								<c:choose>
									<c:when test="${! empty module.profileImage}">
										<img border="0"
											src="<%=contexturl%>${module.profileImage.imagePath}"
											width="120" height="120" title="Click here to change photo"
											onclick="OpenFileDialog();"
											id="profileimage" style="cursor: pointer;" />

										<span class="text-content" onclick="OpenFileDialog();"
											title="Click here to change photo"><br> <br>
										<span>Change Module Image</span></span>
									</c:when>
									<c:otherwise>
										<img border="0"
											src="<%=contexturl%>resources/img/application/default_profile.png"
											width="120" height="120" title="Click here to change photo"
											onclick="OpenFileDialog();"
											id="profileimage" style="cursor: pointer;" />

										<span class="text-content" onclick="OpenFileDialog();"
											title="Click here to change photo"><br> <br>
										<span>Upload Module Image</span></span>
									</c:otherwise>
								</c:choose>
								
							</div>
							<div>
							<input type="file" name="imageChooser1" id="imageChooser1"
								style="visibility: hidden; height: 0px" />
							</div>
						</div>
					</div>
           			
           		
           			
             </div>
            </div>
           </div>
           
           
    <div class="block full  gridView">
		<div class="block-title">
			<h2>
				<strong><spring:message code="heading.listofcategories" /></strong>
			</h2>
		</div>

		<div class="table-responsive">
			<table id="active-datatable"
				class="table table-vcenter table-condensed table-bordered">
				<thead>
					<tr>
						<th class="text-center"><spring:message code="label.id" /></th>
						<th class="text-center"><spring:message code="label.categoryname" /></th>
						<th class="text-center"><spring:message code="label.description" /></th>
						<th class="text-center"><spring:message code="label.status" /></th>
					</tr>
				</thead>
				<tbody id="tbody">
					<c:forEach var="currentCategory" items="${listofcategories}"
						varStatus="count">

						<tr>
							<td class="text-center">${count.count}</td>
							<td class="text-left"><c:out
									value="${currentCategory.categoryName}" /></td>
							<td class="text-left wrap-word"><c:out value="${currentCategory.categoryDescription}" /></td>
							<td class="text-left">
							<c:choose>
							<c:when test="${currentCategory.active == false}"><c:out value="Inactive" /></c:when>
							<c:otherwise><c:out value="Active" /></c:otherwise>
							</c:choose>
							</td>
						</tr>
					</c:forEach>

				</tbody>
			</table>
		</div>
	</div>

<!-- 	<div class="block">  -->
			<div style="text-align: center; margin-bottom: 10px"> 
				<button id="module_submit" class="btn btn-sm btn-primary save"
					type="submit">
					<i class="fa fa-angle-right"></i>
					<spring:message code="button.save" />
				</button>
				<security:authorize ifAnyGranted="<%=Role.SUPER_ADMIN.name() %>">
					<a href="<%=contexturl %>ManageModule/ModuleList"  class="btn btn-sm btn-primary save" style="text-decoration: none;"><i class="gi gi-remove"></i> <spring:message code="button.cancel"/></a>
				</security:authorize>
				
<%-- 				<security:authorize ifAnyGranted="<%=Role.UNIVERSITY_ADMIN.name() %>"> --%>
<%-- 						 <a href="<%=contexturl %>Index"  class="btn btn-sm btn-primary" style="text-decoration: none;"><i class="gi gi-remove"></i> <spring:message code="button.cancel"/></a> --%>
<!-- 					 Redirect to view company page. -->
<%-- 					 <a href="<%=contexturl %>ManageCompany/CompanyList/ViewCompany?id=${company.universityId}"  class="btn btn-sm btn-primary" style="text-decoration: none;"><i class="gi gi-remove"></i> <spring:message code="button.cancel"/></a> --%>
<%-- 				</security:authorize> --%>
			</div>
		
	</form>
	


</div>



<%@include file="../../inc/page_footer.jsp"%>
<%@include file="../../inc/template_scripts.jsp"%>
<%@include file="../../inc/chosen_scripts.jsp"%>
<script src="<%=contexturl %>resources/js/custom/featurecontrol.js"></script>
<script src="<%=contexturl %>resources/js/additional-methods.js"></script>

<script
	src="<%=contexturl%>resources/js/pages/activeTablesDatatables.js"></script>
<script>$(function(){ ActiveTablesDatatables.init(3); });</script>
<script type="text/javascript">
	$(document)
			.ready(
					function(){
						$("#manageModule").addClass("active");
						$("#Module_form").validate(
								{	errorClass:"help-block animation-slideDown",
									errorElement:"div",
									errorPlacement:function(e,a){a.parents(".form-group > div").append(e)},
									highlight:function(e){$(e).closest(".form-group").removeClass("has-success has-error").addClass("has-error")},
									success:function(e){e.closest(".form-group").removeClass("has-success has-error")},
												rules : {
													moduleName : {
														required : !0,maxlength : 75
													},
													moduleDescription : {
														 maxlength: 500
													},
													imageChooser1 :{
														<c:if test = "${empty module.profileImage}">
														       required: true,
														</c:if>
														     extension: "jpg|jpeg",
														     filesize: 900000,
														     maxlength: 1000,
													  },
													
												},
												messages : {
													moduleName : {
														required :'<spring:message code="validation.pleaseentermoduleName"/>',
															maxlength :'<spring:message code="validation.modulename75character"/>'
													},
												moduleDescription : {
													
													maxlength :'<spring:message code="validation.decription500character"/>'
													
													},
													imageChooser1 : {
														<c:if test = "${empty module.profileImage}">
														 	required : 'Please select image',
														</c:if>
									                     	extension : 'Please select image of (.jpg,.jpeg) extension.' , 
									                    	maxlength :'The Image is too long. Please select another image'
									           		},
												},
										});
										
								$("#module_submit").click(function() {
									$("#Module_form").submit();
								});
					
	
	});
</script>

<script type="text/javascript">
	function OpenFileDialog(){
		document.getElementById("imageChooser1").click();
	}
	
	function changePhoto(){
		if($('#imageChooser1').val() != ""){			
			var imageName = document.getElementById("imageChooser1").value;
			var ext1 = imageName.substring(imageName.lastIndexOf("."));
			var ext = ext1.toLowerCase();
			if(ext == '.jpg' || ext == '.jpeg')
				$('#changePhoto').submit();		
			else
				alert("Please select only image file");		
		}
	}
	
</script>

<script type="text/javascript">
    function showimages(files) {
        for (var i = 0, f; f = files[i]; i++) {
           
                       
            var reader = new FileReader();
            reader.onload = function (evt) {
            	$( "#image" ).remove();
            	$( "#imagediv" ).html("");
            	
//                 var img = '<div class="col-md-6 id="imagediv"><img border="0" src="' + evt.target.result + '" width="120" height="120" title="Click here to change photo" style="cursor: pointer;"  onclick="OpenFileDialog();" class="img-circle pic" id="image"/></div>';
                  var img ='<div><img border="0" src="' + evt.target.result + '" width="120" height="120" title="Click here to change photo" onclick="OpenFileDialog();" id="image" style="cursor: pointer;" /><span class="text-content" onclick="OpenFileDialog();" title="Click here to change photo"><br><br><span>Change Module Image</span></span></div>';
                 $('#imagediv').html(img);
            }
            reader.readAsDataURL(f);
        }
    }

    $('#imageChooser1').change(function (evt) {
        showimages(evt.target.files);
    });
</script>


<script
	src="<%=contexturl%>resources/js/pages/activeTablesDatatables.js"></script>
<script
	src="<%=contexturl%>resources/js/pages/tablesDatatables.js"></script>


<style>
.chosen-container {
	width: 250px !important;
}
#map-canvas {
	height: 300px
}
</style>


<%@include file="../../inc/template_end.jsp"%>