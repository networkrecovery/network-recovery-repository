
<%@include file="../inc/config.jsp"%>
<%@include file="../inc/template_start.jsp"%>
<%@include file="../inc/page_head.jsp"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<div id="page-content">
	<div class="content-header">
		<div class="header-section">
			<h1>
				
				${module.moduleName}
				<br> <small>Forum List </small>
			</h1>
			<c:if test="${!empty success}">
				<div class="alert alert-success alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-check-circle"></i>
						<spring:message code="label.success" />
					</h4>
					<spring:message code="${success}" />
				</div>
			</c:if>
			<div class="alert alert-danger alert-dismissable"
				style="display: none" id="error">
				<button type="button" class="close" data-dismiss="alert"
					aria-hidden="true">x</button>
				<h4>
					<i class="fa fa-times-circle"> </i> <spring:message code="label.error"/>
				</h4>
				<span id="errorMsg"></span>
			</div>
		</div>
	</div>
	
	<div class="block full  gridView">
	<a href="<%=contexturl %>AtmaAdmin" id="atmaDashboardBack" class="btn btn-sm btn-primary save">Back to dashboard</a>
		<div class="table-responsive">
			<table id="active-datatable"
				class="table table-vcenter table-condensed table-bordered">
				<thead>
					<tr>
					<th class="text-center"><spring:message code="label.id" /></th>
						<th class="text-center">Title</th> 
						<th class="text-center">Tag</th> 
						<th class="text-center">Created By</th>
						<th class="text-center">Created On</th>
				        <th class="text-center">Actions</th>
					</tr>
				</thead>
				<tbody id="tbody">
					<c:forEach var="forum" items="${forumList}" varStatus="count">
						<tr>
							<td class="text-center">${count.count}</td>
							<td class="text-center">${forum.title}</td>
							<td class="text-center">${forum.tag.name}</td>
							<td class="text-center">${forum.createdBy.fullName}</td>
							<td class="text-center"> <fmt:formatDate type="date" value="${forum.createdDate}" /></td>
							<td class="text-center">
								<div class="btn-group">
									<a href="<%=contexturl %>ForumComments/${forum.forumId}"
										data-toggle="tooltip" title="Reply"
										class="btn btn-xs btn-default save"><i
										class="fa fa-pencil"></i></a> 
									<a href="#delete_forum"
										data-toggle="modal"
										onclick="deleteForum(${forum.forumId})"
										title="Delete Forum" class="btn btn-xs btn-danger save"><i
										class="fa fa-times"></i></a>
								</div>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
	<div id="delete_forum" class="modal fade" tabindex="-1"
		role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<form action="<%=contexturl %>ManageForums/ForumList/DeleteForum" method="post" class="form-horizontal form-bordered save"
						id="delete_forum_form">
						<input type="hidden" name="moduleId" value="${module.moduleId}"/>
						<div style="padding: 10px; height: 110px;">
							<label>Do you want to delete this forum?</label><br>
							<label>This will delete the forum permanently</label>
							<div class="col-xs-12 text-right">
								<button type="button" class="btn btn-sm btn-default"
									data-dismiss="modal">
									<spring:message code="label.no" />
								</button>
								<div id="deleteForum" class="btn btn-sm btn-primary">
									<spring:message code="label.yes" />
								</div>
							</div>
						</div>
						<input type="hidden" name="id" id="forumId">

					</form>

				</div>
			</div>
		</div>
	</div>


</div>

<%@include file="../inc/page_footer.jsp"%>
<%@include file="../inc/template_scripts.jsp"%>

<script
	src="<%=contexturl%>resources/js/pages/activeTablesDatatables.js"></script>
<script
	src="<%=contexturl%>resources/js/pages/inactiveTablesDatatables.js"></script>
<script>$(function(){ ActiveTablesDatatables.init(3); });</script>
<script>$(function(){ InActiveTablesDatatables.init(3); });</script>

<script type="text/javascript">
	var selectedId = 0;
	var restoreId=0;
	$(document).ready(function() {

		
        $("#restore_Company").click(function(){
			$("#restorecompanyId").val(restoreId);
			$("#restore_company_Form").submit();
		});
		
		
		$("#deleteForum").click(function(){
			$("#forumId").val(selectedId);
			$("#delete_forum_form").submit();
		});
	});
	
	function deleteForum(id){
		selectedId = id;
	}
	function restoreCompany(id){
		restoreId = id;
	}
</script>

<%@include file="../inc/template_end.jsp"%>