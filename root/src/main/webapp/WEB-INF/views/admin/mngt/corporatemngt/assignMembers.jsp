<%@page import="com.astrika.membershipmngt.model.CorporateCodeStatus"%>


<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/template_start.jsp"%>
<%@include file="../../inc/page_head.jsp"%>

<div id="page-content">
	<div class="content-header">
		<div class="header-section">
			<h1>
				<i class="fa fa-map-marker"></i> <spring:message code="heading.assignmember" /><br> <small><spring:message code="heading.assigncode" />
					 </small>
			</h1>
			<c:if test="${!empty success}">
				<div class="alert alert-success alert-dismissable">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
					<h4><i class="fa fa-check-circle"></i> <spring:message code="label.success"/></h4> <spring:message code="${success}"/>
				</div>
			</c:if>
			<c:if test="${!empty error}">
			<div class="alert alert-danger alert-dismissable">
				<button type="button" class="close" data-dismiss="alert"
					aria-hidden="true">x</button>
				<h4>
					<i class="fa fa-times-circle"></i>
					<spring:message code="label.error" />
				</h4>
				${error}
			</div>
		</c:if>
		</div>
	</div>
	<ul class="breadcrumb breadcrumb-top">
		<li><spring:message code="heading.corporateaccount" /></li>
		<li><a href="#"><spring:message code="heading.assignmember" /></a></li>
	</ul>
	<div class="block full" id="gridView">
		<div class="block-title">
			<h2>
				<strong><spring:message code="heading.assignmember" /></strong>
			</h2>
		</div>
		<form id="assign_form" action="<%=contexturl %>ManageCorporate/AssignMembers" method="post">
			<input type="hidden" name="corporateId" value="<%=user.getModuleId()%>">
			<div class="table-responsive">
				<c:set var="readyFlag" value="false"/>
				<table 
					class="table table-vcenter table-condensed table-bordered">
					<thead>
						<tr>
							<th class="text-center"><spring:message code="label.id" /></th>
							<th class="text-center"><spring:message code="label.code" /></th>
							<th class="text-center"><spring:message code="label.status" /></th>
							<th class="text-center"><spring:message code="label.emailid" /></th>
							<th class="text-center"><spring:message code="label.actions" /></th>
							
							
						</tr>
					</thead>
					<tbody id="tbody">
					<c:set var="status" value="<%=CorporateCodeStatus.READY %>"/>
					<c:set var="status1" value="<%=CorporateCodeStatus.ASSIGNED %>"/>
					<c:set var="status2" value="<%=CorporateCodeStatus.ACTIVE %>"/>
					<c:set var="status3" value="<%=CorporateCodeStatus.DELINKED %>"/>
				
				
				
						<c:forEach var="currentCode" items="${corporateCodeList}" varStatus="count">
							<tr>
								<td class="text-center">${count.count}</td>
								<td class="text-center"><c:out value="${currentCode.code}" /></td>
								<td class="text-center"><c:out value="${currentCode.status}" /></td>
								<td class="text-center">
									<input value="${currentCode.email}" type="text" name="email_${currentCode.id}" 
									<c:if test="${currentCode.status eq status1}"> disabled</c:if>
									<c:if test="${currentCode.status eq status2}"> disabled</c:if> 
									<c:if test="${currentCode.status eq status3}"> disabled</c:if>/>
								</td>
								<c:choose>
									<c:when test="${currentCode.status eq status}">
										<c:set var="readyFlag" value="true" />
									</c:when>
								</c:choose>
										<td>
											<div class="col-md-9 col-md-offset-3">
												<c:choose>
													<c:when
														test="${currentCode.status ne status  and currentCode.status ne status3}">
														<a href="#dlink_Memeber_popup" data-toggle="modal"
															onclick="dlinkMember(${currentCode.id})" title="Dlink"
															class="btn btn-sm btn-primary save"><i class="fa fa-times"></i>
														<spring:message code="button.Dlink" /> </a>
													</c:when>

													<c:otherwise>
													</c:otherwise>
												</c:choose>
											</div>
										</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
				
				<c:if test="${readyFlag eq true}"> 
				<div class="form-group form-actions">
					<div class="col-md-9 col-md-offset-3">
						<button type="submit" id="assign_submit" class="btn btn-sm btn-primary save"  style="margin-top: -16px;">
							<i class="fa fa-angle-right"></i> <spring:message code="button.save" />
						</button>
					</div>
				</div>
			</c:if>
			</div>
		</form>
	</div>
	
	<div id="dlink_Memeber_popup" class="modal fade" tabindex="-1"
		role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<form action="<%=contexturl %>ManageCorporate/DLinkMember"  method="post" class="form-horizontal form-bordered" id="dlink_User_Form">
						
						<div style="padding: 10px; height: 110px;">
							<label><spring:message code="validation.doyouwanttodlinkmember" /></label><br>
							<div class="col-xs-12 text-right">
								<button type="button" class="btn btn-sm btn-default"
									data-dismiss="modal"><spring:message code="label.no" /></button>
								<button type="submit" id="dlink_User" class="btn btn-sm btn-primary save">	<spring:message code="label.yes" /></button>
							</div>
						</div>
						<input type="hidden"  name="corporateCodeId" id="corporateCodeId">
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<%@include file="../../inc/page_footer.jsp"%>
<%@include file="../../inc/template_scripts.jsp"%>
<script src="<%=contexturl %>resources/js/pages/tablesDatatables.js"></script>
<script>$(function(){ TablesDatatables.init(3); });</script>
<script type="text/javascript">
	$(document).ready(function() {
		
		  $("#dlink_User").click(function(){
				
				$("#corporateCodeId").val(dlinkId);
				
			});
		
		
// 		$("#assign_submit").click(function(){
// 			var data = $("#assign_form").serialize();
// 			//alert(data);
// 			$.ajax({
// 				type: "POST",
// 				async: false,
<%-- 				url: "AssignMembers?corporateId=<%=user.getModuleId()%>", --%>
// 				data:data,
// 				success: function(data, textStatus, jqXHR){
// 					$('#page-content').html(data);
					
// 				},
// 				error: function(request,status,error) {
// 				},
// 				dataType: 'html'
// 			});
// 		});
	});
	
	
	function dlinkMember(id){
		dlinkId = id;
	}
</script>