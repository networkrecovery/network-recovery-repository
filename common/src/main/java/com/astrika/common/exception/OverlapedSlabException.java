package com.astrika.common.exception;

import com.astrika.kernel.exception.BusinessException;
import com.astrika.kernel.exception.CustomException;

public class OverlapedSlabException extends BusinessException{
	
	public OverlapedSlabException() {
		super(CustomException.DISCOUNT_SLAB_OVERLAP.getCode());
	}

	public OverlapedSlabException(String errorCode){
		super(errorCode);
	}
	 
}
