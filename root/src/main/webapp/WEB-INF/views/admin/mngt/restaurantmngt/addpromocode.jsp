<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->

<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/template_start.jsp"%>
<%@include file="../../inc/page_head.jsp"%>

<link rel="stylesheet" type="text/css" href="<%=contexturl %>resources/css/jquery.cleditor.css" />
<style>
.cleditorToolbar {
	background: url('<%=contexturl %>resources/img/cleditor/toolbar.gif') repeat
}

.cleditorButton {
	float: left;
	width: 24px;
	height: 24px;
	margin: 1px 0 1px 0;
	background: url('<%=contexturl %>resources/img/cleditor/buttons.gif')
}

.cleditorMain {
	height: 250px !important
}
</style>

<div id="page-content">
	<div class="content-header">
		<div class="header-section">
			<c:if test="${!empty error}">
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-times-circle"></i> <spring:message code="label.error"/>
					</h4>
					<spring:message code="${error}" />
				</div>
			</c:if>
			<h1>
				<i class="fa fa-map-marker"></i>
				<spring:message code="heading.promocodeDescription" />
				<br> <small><spring:message code="heading.createpromocode" />
				</small>
			</h1>
			<span id="errorMsg"></span>
		</div>
	</div>
	<ul class="breadcrumb breadcrumb-top">
		<li><spring:message code="heading.promocodeDescription" />
		</li>
	</ul>
	<div class="block full" id="formView">
		<form action="SavePromoDescription" method="post" class="form-horizontal"
			enctype="multipart/form-data" id="promoDescription_form">
			<div class="form-group">
				<label class="col-md-3 control-label" for="promocode_sheet">
					<spring:message code="label.samplepromocode"/></label>
				<div class="col-md-8">
                    <p class="form-control-static">
                    	<a href="<%=contexturl %>resources/files/samplepromocode.csv"
							download="samplepromocode.csv"><spring:message
							code="label.download" />
						</a>
                    </p>
                </div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="promocode_sheet">
					<spring:message code="label.uploadpromocode"/><span class="text-danger">*</span></label>
				<div class="col-md-9">
					<input type="file" name="promocodeFile" id="promocodeFile"
						accept=".csv"
						style="padding-top: 7px;" />
				</div>
			</div>
			<div class="form-group">
				<input id="promoDescription_Id" name="descriptionId" type="hidden" value="0">
				<input id="outlet_Id" name="outletId" type="hidden" value="${outletId}"}> 
				<label class="col-md-3 control-label"
					for="promoDescription_Name"><spring:message code="label.title" /><span class="text-danger">*
				</label>
				<div class="col-md-9">
					<input id="promoDescription_Name" name="title" class="form-control" value="${promoDescription.title}"
						placeholder="<spring:message code="label.title"/>.."
						type="text">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="promoDescription_description"><spring:message
						code="label.description" /><span class="text-danger">*</label>
				<div class="col-md-9">
					<textarea id="promoDescription_description" name="description"
						class="form-control" rows="5"
						placeholder="<spring:message code="label.description"/>..">${promoDescription.description}</textarea>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="offer_image"><spring:message code="label.promocodepicture"/>
					</label>
				<div class="col-md-9">
					<input type="file" name="photo" id="profile"
						accept="image/jpg,image/jpeg,image/png,image/gif,image/bmp,image/tiff"
						style="padding-top: 7px;" />
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="promoDescription_validity"><spring:message
						code="label.offerValidity" /></label>
				<div class="col-md-9">
					<label class="switch switch-primary"> <input
						id="promoDescription_validity" name="promocodeValidity" type="checkbox" 
						<c:if test="${promoDescription.promocodeValidity}"><spring:message code="label.checked"/> </c:if> /> <span></span>
					</label>
				</div>
			</div>

			<div class="form-group" id="expdate" style="display:none;">
				<label class="col-md-3 control-label" for="promoDescription_validtill"><spring:message
						code="label.offervalidtill" /><span class="text-danger">*</span> </label>
				<div class="col-md-9">
					<input type="text" id="example-datepicker3" name="expirydate"
						style="width: 200px;" class="form-control input-datepicker"
						data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy" value="${expirydate}">
				</div>
				<label style="color: #e74c3c;margin-left: 289px;" id="expiryDateError"></label>
			</div>

			<div class="form-group">
				<label class="col-md-3 control-label" for="promoDescription_termsCondition"><spring:message
						code="label.termsandcondition" /><span class="text-danger">* </label>
				<div class="col-md-9">
					<textarea id="promoDescription_termsCondition" name="termsandcondition">${promoDescription.termsandcondition}</textarea>
					<label style="color: #e74c3c;" id="termsError"></label>
				</div>
			</div>

			<div class="form-group form-actions">
				<div class="col-md-9 col-md-offset-3">
					<button id="promoDescription_submit" type="submit"
						class="btn btn-sm btn-primary save">
						<i class="fa fa-angle-right"></i> <spring:message code="button.save" />
					</button>
					<button type="reset" class="btn btn-sm btn-warning">
						<i class="fa fa-repeat"></i>
						<spring:message code="button.reset" />
					</button>
						<a class="btn btn-sm btn-primary" href="PromoDescriptionList?outletId=${outletId}"><i class="gi gi-remove"></i>
							<spring:message code="button.cancel" /> </a>
				</div>
			</div>
		</form>
	</div>
</div>

<%@include file="../../inc/page_footer.jsp"%>
<%@include file="../../inc/template_scripts.jsp"%>

<script type="text/javascript" src="<%=contexturl %>resources/js/jquery.cleditor.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		var validityFlag=false;
 		$("#promoDescription_termsCondition").cleditor(); 
		
		$(".input-datepicker, .input-daterange").datepicker({weekStart : 1});
		
		$(".input-timepicker24").timepicker({minuteStep:1,showSeconds:!0,showMeridian:!1});

		$("#promoDescription_form").validate(
				{	errorClass:"help-block animation-slideDown",
					errorElement:"div",
					errorPlacement:function(e,a){a.parents(".form-group > div").append(e)},
					highlight:function(e){$(e).closest(".form-group").removeClass("has-success has-error").addClass("has-error")},
					success:function(e){e.closest(".form-group").removeClass("has-success has-error")},
					rules:{ title:{required:!0, maxlength : 100,minlength : 5},
							description:{required:!0,maxlength : 500},
							promocodeFile:{required:!0},
							termsandcondition:{required:!0}
					},
					messages:{title:{required:'<spring:message code="validation.title"/>',
						maxlength :'<spring:message code="validation.title100character"/>',
						minlength :'<spring:message code="validation.title5character"/>'},
							description:{required:'<spring:message code="validation.description"/>',
								maxlength :'<spring:message code="validation.description500character"/>'},
							promocodeFile:{required:'<spring:message code="validation.promocodeFile"/>'},
							termsandcondition:{required:'<spring:message code="validation.termsandconditions"/>'}
					},
					
				});
		
		$("#promoDescription_submit").click(function(event){
			var termCondition = $('#promoDescription_termsCondition').val();
			var expirydate = $('#example-datepicker3').val();
			if(validityFlag && expirydate.length<1){
				$("#expiryDateError").text("<spring:message code="validation.expirydate"/>")
				 return false;
				}
			if(termCondition.length<3){
				$("#termsError").text("Please enter terms and conditions")
				return false;
			}
			if(termCondition.length>5000){
				$("#termsError").text("<spring:message code="validation.termsandconditionsmax"/>")
				return false;
			}
 			 $("#promoDescription_form").submit();
		});
		
		 $('#promoDescription_validity').click(function() {
		        if ($('#promoDescription_validity').is(':checked')) {
		        	$("#expdate").css("display", "block");
		        	
		        	 validityFlag=true;
		        }
		        else{
		        	$("#expdate").css("display", "none");
		        	
		        	$("#example-datepicker3").text("");
		        
		        	$("#expiryDateError").text("");
		        	 validityFlag=false;
		        }
		    });
		
		
	});
</script>
<%@include file="../../inc/template_end.jsp"%>