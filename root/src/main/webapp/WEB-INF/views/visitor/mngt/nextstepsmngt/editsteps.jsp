<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/template_start.jsp"%>
<%@include file="/WEB-INF/views/admin/inc/page_head.jsp"%>
<div id="page-content">
	<div class="content-header">
		<div class="header-section">
			<c:if test="${!empty success}">
				<div class="alert alert-success alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-check-circle"></i>
						<spring:message code="label.success" />
					</h4>
					<spring:message code="${success}" />
				</div>
			</c:if>
			
			<c:if test="${!empty error}">
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-times-circle"></i>
						<spring:message code="label.error" />
					</h4>
					<spring:message code="${error}" />
				</div>
			</c:if>

			<h1>
				<!-- 				<i class="fa fa-map-marker"></i> -->
				<spring:message code="heading.nextsteps" />
				<br> <small><spring:message code="heading.managenextsteps" /></small>
			</h1>
			<span id="errorMsg"></span>
		</div>
	</div>
	<ul class="breadcrumb breadcrumb-top">
<%-- 		<li><spring:message code="menu.survey" /></li> --%>
           <li><strong>${module.moduleName}</strong></li>
	</ul>

	<form action="<%=contexturl%>ManageNextSteps/CreateNextSteps/SaveNextSteps"
		method="post" class="form-horizontal ui-formwizard" id="NextSteps_Form"
		enctype="multipart/form-data">
		<input name="moduleId" id="moduleId" type="hidden" value="${moduleId}" />
		<div class="row">
			<div class="">
				<div class="block">
					<div class="block">
						<div class="block-title">
							<h2>
								<strong><spring:message code="heading.lifeStage1" /></strong>
							<input  id="act" type="hidden" name="act"> 
							</h2>
						</div>

						<c:forEach var="list1" items="${nextStepsList1}" varStatus="counter">
							<div class="form-group">								
								<div class="col-md-6">
									<input id="${counter.count}" name="${counter.count}" class="form-control"
										placeholder="Enter the question" type="text"
										value="${list1.step}">
								</div>
								<label class="col-md-2 control-label" for="chosenCategory${counter.count}"><spring:message
										code="label.categoryname" /></label>
								<div class="col-md-4">
									<select class="category_chosen${counter.count}" style="width: 200px;"
										id="chosenCategory${counter.count}"
										data-placeholder="<spring:message code='label.choosecategory' />"
										name="chosenCategory${counter.count}">
										<option value=""></option>
										<c:forEach items="${categoryList}" var="category1">
											<option value="${category1.categoryId}"
												<c:if test="${category1.categoryId eq list1.category.categoryId}">selected="selected"</c:if>>${category1.categoryName}</option>
										</c:forEach>
									</select>
								</div>
							</div>
						</c:forEach>
					</div>
					<div class="block">
						<div class="block-title">
							<h2>
								<strong><spring:message code="heading.lifeStage2" /></strong>
							</h2>
						</div>
						<c:forEach var="list2" items="${nextStepsList2}" varStatus="counter">
							<div class="form-group">								
								<div class="col-md-6">
									<input id="${counter.count+4}" name="${counter.count+4}" class="form-control"
										placeholder="Enter the question" type="text"
										value="${list2.step}">
								</div>
								<label class="col-md-2 control-label" for="chosenCategory${counter.count}"><spring:message
										code="label.categoryname" /></label>
								<div class="col-md-4">
									<select class="category_chosen${counter.count+4}" style="width: 200px;"
										id="chosenCategory${counter.count+4}"
										data-placeholder="<spring:message code='label.choosecategory' />"
										name="chosenCategory${counter.count+4}">
										<option value=""></option>
										<c:forEach items="${categoryList}" var="category2">
											<option value="${category2.categoryId}"
												<c:if test="${category2.categoryId eq list2.category.categoryId}">selected="selected"</c:if>>${category2.categoryName}</option>
										</c:forEach>
									</select>
								</div>
							</div>
						</c:forEach>
					</div>
					<div class="block">
						<div class="block-title">
							<h2>
								<strong><spring:message code="heading.lifeStage3" /></strong>
							</h2>
						</div>
						<c:forEach var="list3" items="${nextStepsList3}" varStatus="counter">
							<div class="form-group">								
								<div class="col-md-6">
									<input id="${counter.count+8}" name="${counter.count+8}" class="form-control"
										placeholder="Enter the question" type="text"
										value="${list3.step}">
								</div>
								<label class="col-md-2 control-label" for="chosenCategory${counter.count+8}"><spring:message
										code="label.categoryname" /></label>
								<div class="col-md-4">
									<select class="category_chosen${counter.count+8}" style="width: 200px;"
										id="chosenCategory${counter.count+8}"
										data-placeholder="<spring:message code='label.choosecategory' />"
										name="chosenCategory${counter.count+8}">
										<option value=""></option>
										<c:forEach items="${categoryList}" var="category3">
											<option value="${category3.categoryId}"
												<c:if test="${category3.categoryId eq list3.category.categoryId}">selected="selected"</c:if>>${category3.categoryName}</option>
										</c:forEach>
									</select>
								</div>
							</div>
						</c:forEach>
					</div>
					<div class="block">
						<div class="block-title">
							<h2>
								<strong><spring:message code="heading.lifeStage4" /></strong>
							</h2>
						</div>
						<c:forEach var="list4" items="${nextStepsList4}" varStatus="counter">
							<div class="form-group">								
								<div class="col-md-6">
									<input id="${counter.count+12}" name="${counter.count+12}" class="form-control"
										placeholder="Enter the question" type="text"
										value="${list4.step}">
								</div>
								<label class="col-md-2 control-label" for="chosenCategory${counter.count+12}"><spring:message
										code="label.categoryname" /></label>
								<div class="col-md-4">
									<select class="category_chosen${counter.count+12}" style="width: 200px;"
										id="chosenCategory${counter.count+12}"
										data-placeholder="<spring:message code='label.choosecategory' />"
										name="chosenCategory${counter.count+12}">
										<option value=""></option>
										<c:forEach items="${categoryList}" var="category4">
											<option value="${category4.categoryId}"
												<c:if test="${category4.categoryId eq list4.category.categoryId}">selected="selected"</c:if>>${category4.categoryName}</option>
										</c:forEach>
									</select>
								</div>
							</div>
						</c:forEach>
					</div>
					<div class="block">
						<div class="block-title">
							<h2>
								<strong><spring:message code="heading.lifeStage5" /></strong>
							</h2>
						</div>
						<c:forEach var="list5" items="${nextStepsList5}" varStatus="counter">
							<div class="form-group">								
								<div class="col-md-6">
									<input id="${counter.count+16}" name="${counter.count+16}" class="form-control"
										placeholder="Enter the question" type="text"
										value="${list5.step}">
								</div>
								<label class="col-md-2 control-label" for="chosenCategory${counter.count+16}"><spring:message
										code="label.categoryname" /></label>
								<div class="col-md-4">
									<select class="category_chosen${counter.count+16}" style="width: 200px;"
										id="chosenCategory${counter.count+16}"
										data-placeholder="<spring:message code='label.choosecategory' />"
										name="chosenCategory${counter.count+16}">
										<option value=""></option>
										<c:forEach items="${categoryList}" var="category5">
											<option value="${category5.categoryId}"
												<c:if test="${category5.categoryId eq list5.category.categoryId}">selected="selected"</c:if>>${category5.categoryName}</option>
										</c:forEach>
									</select>
								</div>
							</div>
						</c:forEach>
					</div>
				</div>
			</div>
		</div>
		<div style="text-align: center; margin-bottom: 10px">
			<div class="form-group form-actions">
				<!-- 				<div class="col-md-9 col-md-offset-3"> -->
						<button id="nextsteps_submit" type="submit" value="Submit" class="btn btn-sm btn-primary save">
						<i class="fa fa-angle-right"></i>
						<spring:message code="button.save" />
					</button>
				<a class="btn btn-sm btn-primary save"
					href="<%=contexturl%>AtmaAdmin"> <spring:message
						code="button.cancel" />
				</a>

			</div>
		</div>
	</form>
</div>
<%@include file="../../inc/page_footer.jsp"%>
<%@include file="../../inc/template_scripts.jsp"%>
<%@include file="../../inc/chosen_scripts.jsp"%>
	<script type="text/javascript">
	
		$(document)
				.ready(
						function() {
							$("#manageNextSteps").addClass("active");

							for (var i = 1; i <= 20; i++) {
								$(".category_chosen" + i).chosen();
							}

							$("#nextsteps_submit").click(function() {
								unsaved = false;
								$("#act").val("1");
								$("#NextSteps_Form").submit();
							});

							$("#NextSteps_Form")
									.validate(
											{
												errorClass : "help-block animation-slideDown",
												errorElement : "div",
												errorPlacement : function(e, a) {
													a
															.parents(
																	".form-group > div")
															.append(e)
												},
												highlight : function(e) {
													$(e)
															.closest(
																	".form-group")
															.removeClass(
																	"has-success has-error")
															.addClass(
																	"has-error")
												},
												success : function(e) {
													e
															.closest(
																	".form-group")
															.removeClass(
																	"has-success has-error")
												},
												rules : {
													1 : {
														maxlength : 100
													},
													2 : {
														maxlength : 100
													},
													3 : {
														maxlength : 100
													},
													4 : {
														maxlength : 100
													},
													5 : {
														maxlength : 100

													},
													6 : {
														maxlength : 100
													},
													7 : {
														maxlength : 100
													},

													8 : {
														maxlength : 100
													},
													9 : {
														maxlength : 100
													},
													10 : {
														maxlength : 100
													},
													11 : {
														maxlength : 100
													},
													12 : {
														maxlength : 100
													},

													13 : {
														maxlength : 100
													},
													14 : {
														maxlength : 100
													},
													15 : {
														maxlength : 100
													},
													16 : {
														maxlength : 100
													},
													17 : {
														maxlength : 100
													},
													18 : {
														maxlength : 100
													},
													19 : {
														maxlength : 100
													},
													20 : {
														maxlength : 100
													}
												},
												messages : {
													1 : {
														maxlength : 'Step should not be more than 100 words'
													},
													2 : {
														maxlength : 'Step should not be more than 100 words'
													},
													3 : {
														maxlength : 'Step should not be more than 100 words'
													},
													4 : {
														maxlength : 'Step should not be more than 100 words'
													},
													5 : {
														maxlength : 'Step should not be more than 100 words'
													},
													6 : {
														maxlength : 'Step should not be more than 100 words'
													},
													7 : {
														maxlength : 'Step should not be more than 100 words'
													},
													8 : {
														maxlength : 'Step should not be more than 100 words'
													},
													9 : {
														maxlength : 'Step should not be more than 100 words'
													},
													10 : {
														maxlength : 'Step should not be more than 100 words'
													},
													11 : {
														maxlength : 'Step should not be more than 100 words'
													},
													12 : {
														maxlength : 'Step should not be more than 100 words'
													},
													13 : {
														maxlength : 'Step should not be more than 100 words'
													},
													14 : {
														maxlength : 'Step should not be more than 100 words'
													},
													15 : {
														maxlength : 'Step should not be more than 100 words'
													},
													16 : {
														maxlength : 'Step should not be more than 100 words'
													},
													17 : {
														maxlength : 'Step should not be more than 100 words'
													},
													18 : {
														maxlength : 'Step should not be more than 100 words'
													},
													19 : {
														maxlength : 'Step should not be more than 100 words'
													},
													20 : {
														maxlength : 'Step should not be more than 100 words'
													}
												},
											});
						});
	</script>