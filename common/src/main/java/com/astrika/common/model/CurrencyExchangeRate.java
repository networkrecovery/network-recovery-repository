package com.astrika.common.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.envers.Audited;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

@Entity
@Audited
public class CurrencyExchangeRate {



	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

	@ManyToOne
	private CurrencyMaster currencyFrom;


	@ManyToOne
	private CurrencyMaster currencyTo;

	@Column(nullable = false)
	private BigDecimal rate;

	@CreatedBy
	@ManyToOne(fetch = FetchType.LAZY)
	private User createdBy;

	@CreatedDate
	private Date createdOn;

	@LastModifiedBy
	@ManyToOne(fetch = FetchType.LAZY)
	private User modifiedBy;

	@LastModifiedDate
	private Date modifiedOn;

	@Column(columnDefinition = "boolean default true", nullable = false)
	private boolean active = true;


	public CurrencyExchangeRate(){
		super();
	}

	public CurrencyExchangeRate(CurrencyMaster currencyFrom, CurrencyMaster currencyTo, BigDecimal rate){
		super();
		this.currencyFrom = currencyFrom;
		this.currencyTo = currencyTo;
		this.rate = rate;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public CurrencyMaster getCurrencyFrom() {
		return currencyFrom;
	}

	public void setCurrencyFrom(CurrencyMaster currencyFrom) {
		this.currencyFrom = currencyFrom;
	}

	public CurrencyMaster getCurrencyTo() {
		return currencyTo;
	}

	public void setCurrencyTo(CurrencyMaster currencyTo) {
		this.currencyTo = currencyTo;
	}

	public BigDecimal getRate() {
		return rate;
	}

	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}
	@JsonIgnore
	public User getCreatedBy() {
		return createdBy;
	}
	@JsonIgnore
	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@JsonIgnore
	public User getModifiedBy() {
		return modifiedBy;
	}
	@JsonIgnore
	public void setModifiedBy(User modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CurrencyExchangeRate other = (CurrencyExchangeRate) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}



}
