package com.astrika.categorymngt.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.astrika.categorymngt.model.CategoryMaster;



public interface CategoryRepository extends JpaRepository<CategoryMaster,Long>{
	
	@Query(value="select c from CategoryMaster c  where c.active=1 Order By c.categoryName ASC ",countQuery="select count(c) from CategoryMaster c where c.active = 1")
	Page<CategoryMaster> findByActiveTrueOrderByCategoryNameAsc(Pageable page);
	
	@Query(value="select c from CategoryMaster c  where c.active=0 Order By c.categoryName ASC ",countQuery="select count(c) from CategoryMaster c where c.active = 0")
	Page<CategoryMaster> findByActiveFalseOrderByCategoryNameAsc(Pageable page);
	
	@Query("select c from CategoryMaster c  left join fetch c.createdBy  left join fetch c.module where c.active=?1 Order By c.categoryName ASC ")
	List<CategoryMaster> findAllByActive(boolean active);
	
 
	@Query("select c from CategoryMaster c join fetch c.module where c.categoryName=?1")
	CategoryMaster findByCategoryName(String categoryName);
	
	
	@Query("select c from CategoryMaster c left join fetch c.module  where c.active = ?1 and c.module.moduleId=?1 Order By c.categoryName Asc")
	List<CategoryMaster> findByCategoryCategoryIdAndActiveTrueOrderByCategoryNameAsc(
			long categoryId);
	
    @Query("select c from CategoryMaster c left join fetch c.module where c.categoryId = ?1")
	CategoryMaster findByCategoryId(long categoryId);
	
    @Query("select c from CategoryMaster c where c.categoryName = ?1 and c.module.moduleId = ?2")
	CategoryMaster findByCategoryNameAndModuleId(String categoryName , Long moduleId);	
	//@Query("select state from StateMaster state join fetch state.country where state.active = 1 and state.country.countryId=?1 Order By state.stateName Asc")
 
    @Query("select c from CategoryMaster c where c.module.moduleId = ?1")
    List<CategoryMaster> findByModuleId(Long moduleId);
    
    @Query("select c from CategoryMaster c where c.module.moduleId = ?1 and c.active = ?2")
    List<CategoryMaster> findByModuleIdandActive(Long moduleId, boolean active);

    @Query("select count(categoryId) from CategoryMaster c where c.module.moduleId = ?1 and c.active = ?2")
    int totalActiveCategoriesbyModule(Long moduleId, boolean active);
    
}
