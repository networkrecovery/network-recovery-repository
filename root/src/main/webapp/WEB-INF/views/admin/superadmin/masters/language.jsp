<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@include file="../../inc/config.jsp"%>
	<div class="content-header">
		<div class="header-section">
			<h1>
				<i class="fa fa-map-marker"></i> <spring:message code="heading.languagemaster" /><br> <small><spring:message code="heading.specifylistoflanguages" />
					 </small>
			</h1>
			<span id="errorMsg"></span>
			<c:if test="${!empty success}">
				<div class="alert alert-success alert-dismissable">
					<button type="button" id="success_close" class="close" data-dismiss="alert" aria-hidden="true">x</button>
					<h4><i class="fa fa-check-circle"></i> <spring:message code="label.success"/></h4> <spring:message code="${success}"/>
				</div>
			</c:if>
			<c:if test="${!empty error}">
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
					<h4><i class="fa fa-times-circle"></i> <spring:message code="label.error"/></h4> <spring:message code="${error}"/>
				</div>
			</c:if>
		</div>
	</div>
	<ul class="breadcrumb breadcrumb-top">
		<li><spring:message code="heading.managelanguage" /></li>
		<li><a href="#"><spring:message code="heading.language" /></a></li>
	</ul>
	<div class="block full gridView">
		<div class="block-title">
			<h2>
				<strong><spring:message code="heading.activelanguage" /></strong> 
			</h2>
		</div>
		<a id="addLanguage" class="btn btn-sm btn-primary" href="#">
		<i class="fa fa-angle-right"></i>
		<spring:message code="label.addlanguage" />
	</a>
<%-- 		<a href="#" id="addLanguage"><spring:message code="label.addlanguage" /></a> --%>
		<div class="table-responsive">
			<table id="active-datatable"	class="table table-vcenter table-condensed table-bordered">
				<thead>
					<tr>
						<th class="text-center"><spring:message code="label.id" /></th>
						<th class="text-center"><spring:message code="label.languagename" /></th>
						<th class="text-center"><spring:message code="label.languageshortname" /></th>
						<th class="text-center"><spring:message code="label.actions" /></th>
					</tr>
				</thead>
				<tbody id="tbody">
					<c:forEach var="currentLanguage" items="${languageList}"
						varStatus="count">
						<tr>
							<td class="text-center">${count.count}</td>
							<td class="text-left"><c:out
									value="${currentLanguage.languageName}" /></td>
							<td class="text-left"><c:out
									value="${currentLanguage.shortName}" /></td>
							<td class="text-center">
								<div class="btn-group">
									<a href="#" data-toggle="tooltip" title="Edit"
										onclick="editLanguage(${currentLanguage.languageId},${count.count})"
										class="btn btn-xs btn-default"><i class="fa fa-pencil"></i>
									<a href="#delete_Language_popup" data-toggle="modal"
										onclick="deleteLanguage(${currentLanguage.languageId})"
										title="Delete" class="btn btn-xs btn-danger"><i
										class="fa fa-times"></i> </a>
								</div>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
		<div class="block full gridView">
			<div class="block-title">
				<h2>
					<strong><spring:message code="heading.inactivelanguage" /></strong> 
				</h2>
			</div>
			<div class="table-responsive">
				<table  id="inactive-datatable" class="table table-vcenter table-condensed table-bordered ">
					<thead>
						<tr>
							<th class="text-center"><spring:message code="label.id" /></th>
							<th class="text-center"><spring:message code="label.languagename" /></th>
							<th class="text-center"><spring:message code="label.languageshortname" /></th>
							<th class="text-center"><spring:message code="label.actions" /></th>
						</tr>
					</thead>
					<tbody id="tablebody">
						<c:forEach var="currentLanguage" items="${inactivelanguageList}"
							varStatus="count">
							<tr>
								<td class="text-center">${count.count}</td>
								<td class="text-left"><c:out
										value="${currentLanguage.languageName}" /></td>
								<td class="text-left"><c:out
										value="${currentLanguage.shortName}" /></td>
								<td class="text-center">
									<div class="btn-group">
										<a href="#restore_Language_popup" data-toggle="modal"
											onclick="restoreLanguage(${currentLanguage.languageId})"
											title="Restore" class="btn btn-xs btn-danger"><i
											class="fa fa-times"></i> </a>
									</div>
								</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>

	<div id="delete_Language_popup" class="modal fade" tabindex="-1"
		role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<form action="" method="post" class="form-horizontal form-bordered">
						<div style="padding: 10px; height: 110px;">
							<label><spring:message code="validation.doyouwanttodeletethislanguage"/></label>
							<div class="col-xs-12 text-right">
								<button type="button" class="btn btn-sm btn-default"
									data-dismiss="modal"><spring:message code="label.no" /></button>
								<div id="delete_language" class="btn btn-sm btn-primary"><spring:message code="label.yes"/></div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	
	<div id="restore_Language_popup" class="modal fade" tabindex="-1"
		role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<form action="" method="post" class="form-horizontal form-bordered">
						<div style="padding: 10px; height: 110px;">
							<label><spring:message code="validation.doyouwanttorestorethislanguage"/></label>
							<div class="col-xs-12 text-right">
								<button type="button" class="btn btn-sm btn-default"
									data-dismiss="modal"><spring:message code="label.no"/></button>
								<div id="restore_language" class="btn btn-sm btn-primary"><spring:message code="label.yes"/></div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

	<div class="block full" id="formView" style="display: none">
		<div class="block-title">
			<h2>
				<strong><spring:message code="label.addlanguagemaster" /></strong> 
			</h2>
		</div>
		<form action="" method="post" class="form-horizontal"
			id="Language_form">
			<div class="form-group">
				<input id="language_Id" name="languageId" type="hidden" value="0">
				<label class="col-md-3 control-label" for="language_Name"><spring:message code="label.languagename" /><span class="text-danger">*</span>
					</label>
				<div class="col-md-9">
					<input id="language_Name" name="languageName" class="form-control"
						placeholder="Language Name.." type="text">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="languageShortName"><spring:message code="label.languageshortname" /><span class="text-danger">*</span>
					</label>
				<div class="col-md-9">
					<input id="languageShortName" name="shortName" class="form-control"
						placeholder="Language Short Name.." type="text"> 
				</div>
			</div>

			<div class="form-group form-actions">
				<div class="col-md-9 col-md-offset-3">
					<div id="Language_submit" class="btn btn-sm btn-primary">
						<i class="fa fa-angle-right"></i> <spring:message code="button.save" />
					</div>
					<a class="btn btn-sm btn-primary" id="cancel" href="#">
						 <spring:message code="button.cancel"/>
					</a>
				</div>
			</div>
		</form>
	</div>

<script src="<%=contexturl %>resources/js/pages/activeTablesDatatables.js"></script>
<script src="<%=contexturl %>resources/js/pages/inactiveTablesDatatables.js"></script>
<script>$(function(){ ActiveTablesDatatables.init(3); });</script>
<script>$(function(){ InActiveTablesDatatables.init(3); });</script>

<script type="text/javascript">
	$(document).ready(function() {
		$("#addLanguage").click(function() {
			$('#Language_form').find("input[type=text], textarea").val("");
			$('#Language_form').find("input[type=hidden], textarea").val("0");
			$("#formView").css("display", "block");
			$(".gridView").css("display", "none");
			$("#success_close").click();
		});
		
		$("#cancel").click(function() {
			$("#formView").css("display", "none");
			$(".gridView").css("display", "block");
			$("#success_close").click();
			$('.help-block').css('display', 'none');
			$("div.form-group").removeClass('has-error');
		});
		
		$("#Language_form").validate(
				{	errorClass:"help-block animation-slideDown",
					errorElement:"div",
					errorPlacement:function(e,a){a.parents(".form-group > div").append(e)},
					highlight:function(e){$(e).closest(".form-group").removeClass("has-success has-error").addClass("has-error")},
					success:function(e){e.closest(".form-group").removeClass("has-success has-error")},
					rules:{ languageName:{required:!0,maxlength : 30},
						    shortName:{required:!0, maxlength : 6}
					},
					messages:{languageName:{required:'<spring:message code="validation.pleaseenterlanguagename"/>',
						  					maxlength :'<spring:message code="validation.language30character"/>'},
							  shortName:{required:'<spring:message code="validation.pleaseenterlanguageshortname"/>',
							 			 maxlength :'<spring:message code="validation.shortname6character"/>'}
					},
					
					submitHandler: function() {
						var data = $("#Language_form").serialize();
						$.ajax({
							type: "POST",
							async: false,
							url: "<%=contexturl%>Admin/SaveLanguage",
							data:data,
							success: function(data, textStatus, jqXHR){
								var i = data.trim().indexOf("errorCode");
								if(i == 2){
									var obj = $.parseJSON(data.trim());
									var html = '<div class="alert alert-danger alert-dismissable">	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="fa fa-times-circle"></i> Error</h4> <span >'+obj.errorCode+'</span></div>';
									$("#errorMsg").html(html).show();
									$("#errorMsg").html(html).fadeOut(8000);
									$('html, body').animate({ scrollTop: $("#errorMsg").offset().top },500);
								}
								else{
									$('html, body').animate({ scrollTop: $("#errorMsg").offset().top },500);
									$('#page-content').html(data);
								}
							},
							dataType: 'html'
						});
					}
				});
		
		$("#Language_submit").click(function(){
			 $("#Language_form").submit();
		});
		
		$("#delete_language").click(function(){
			$.ajax({
				type: "GET",
				async: false,
				url: "<%=contexturl%>Admin/DeleteLanguage?id="+selectedId,
				success: function(data, textStatus, jqXHR){
					$('#page-content').html(data);
					$('body').removeClass('modal-open');
					$('.modal-backdrop').remove();
					$('#delete_language_popup').modal('hide');
					$('html, body').animate({ scrollTop: $("#errorMsg").offset().top },500);
				},
				dataType: 'html'
			});
		});
		
		
		$("#restore_language").click(function(){
			$.ajax({
				type: "GET",
				async: false,
				url: "<%=contexturl%>Admin/RestoreLanguage?id="+restoreId,
				success: function(data, textStatus, jqXHR){
					$('#page-content').html(data);
					$('body').removeClass('modal-open');
					$('.modal-backdrop').remove();
					$('#restore_language_popup').modal('hide');
					$('html, body').animate({ scrollTop: $("#errorMsg").offset().top },500);
				},
				dataType: 'html'
			});
		});
	});
	
	function editLanguage(id,position){
		var oTable = $("#active-datatable").dataTable();
		var data = oTable.fnGetData()[position-1];
		$("#language_Id").val(id);
		$("#language_Name").val(data[1]);
		$("#languageShortName").val(data[2]);
		$("#formView").css("display", "block");
		$(".gridView").css("display", "none");
		$('.help-block').css('display', 'none');
		$("div.form-group").removeClass('has-error');
		$("#success_close").click();
		
	}
	
	function deleteLanguage(id){
		selectedId = id;
	}
	
	function restoreLanguage(id){
		restoreId = id;
	}
</script>