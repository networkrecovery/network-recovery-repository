<%@page import="org.springframework.web.servlet.support.RequestContextUtils"%>
<%@page import="com.astrika.common.util.PropsValues"%>
<%@page import="org.springframework.context.ApplicationContext"%>
<%@page import="com.astrika.kernel.exception.CustomException"%>
<%@page import="org.springframework.web.util.HtmlUtils"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%
	ApplicationContext ac = RequestContextUtils
			.getWebApplicationContext(request);
	PropsValues propvalue = (PropsValues) ac.getBean("propsValues");
	String contexturl = propvalue.CONTEXT_URL;
%>

<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Atma - Strengthning organisations to impact Society</title>
<!-- Style -->
<!-- Bootstrap -->
<link rel="shortcut icon" href="<%=contexturl %>resources/img/favicon.ico">
<link href="<%=contexturl %>resources/css/bootstrap.css" rel="stylesheet" media="screen">
<link href="<%=contexturl %>resources/css/fontface.css" rel="stylesheet" media="screen">
<link href="<%=contexturl %>resources/css/font-awesome.css" rel="stylesheet" media="screen">
<link href="<%=contexturl %>resources/css/jquery.bxslider.css" rel="stylesheet" media="screen">
<!--[if lt IE 9]>
  <script src="js/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="js/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
<!--[if IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!--SLIDER -->
<link href="<%=contexturl %>resources/css/login.css" rel="stylesheet" media="screen">
</head>

<body>
	<div class="container">
		<div class="row">
			<div class="col-md-4"></div>
			<div class="col-md-4">
				<div class="bg-white">
					<div class="text-center">

						<img src="<%=contexturl%>resources/img/atma.jpg" />
						<p>Strengthening Orgnisations to impact Education</p>
					</div>
					<hr>
					<c:if test="${!empty success}">
						<div class="alert alert-success alert-dismissable">
							<button type="button" class="close" data-dismiss="alert"
								aria-hidden="true">x</button>
							<h4>
								<i class="fa fa-check-circle"></i> Success
							</h4>
							<spring:message code="${success}" />
						</div>
					</c:if>
					<c:if test="${ !empty errorCode}">
						<div class="alert alert-danger alert-dismissable">
							<button type="button" class="close" data-dismiss="alert"
								aria-hidden="true">x</button>
							<h4>
								<i class="fa fa-times-circle"></i> Error
							</h4>
							<spring:message code='${errorCode}' />
						</div>
					</c:if>
					<form action="SubmitResetPassword" method="post"
						id="candidate_login" class="mt10">
						<div class="form-group">
							<div>
								<label>Password </label> <input type="password" class="form-control"
									id="pass" name="password1">
							</div>
						</div>
						<div class="form-group">
							<div>
								<label>Confirm Password </label> <input type="password"
									class="form-control" id="confirmPass" name="password2">
									<input type="hidden" name="token" value="${token}" />
							</div>
						</div>
						<div class="form-group">
							<p class="pull-left">
								<a href="Login" id="link-login" class="save"><small>Go back to Login Page</small> </a>
							</p>
							<button type="submit" id="loginSubmit" class="btn pull-right">Submit</button>
							<span class="clearfix"></span>
						</div>
					</form>
				</div>
			</div>

			<div class="col-md-4"></div>
		</div>
	</div>
</body>

<script src="<%=contexturl %>resources/js/vendor/jquery-1.11.0.min.js"></script>
<script src="<%=contexturl %>resources/js/vendor/bootstrap.min.js"></script>
<script src="<%=contexturl %>resources/js/vendor/validate.js"></script>
<script type="text/javascript">
	$(document)
			.ready(
					function() {
						$("#candidate_login").validate(
										{
											errorClass:"help-block animation-slideDown",
											errorElement:"div",
											errorPlacement:function(e,a){a.parents(".form-group > div").append(e)},
											highlight:function(e){$(e).closest(".form-group").removeClass("has-success has-error").addClass("has-error")},
											success:function(e){e.closest(".form-group").removeClass("has-success has-error")},
											rules : {
												password1 : {
													required : !0,
													minlength : 6
												},
												password2 : {
													required : !0,
													equalTo : "#pass"
												}
											},
											messages : {
												password1 : {
													required : "Please enter a password",
													minlength : "Password must be atleast 6 character"
												},
												password2 : {
													required : "Please confirm password",
													equalTo : "Please enter the same password as above"
												}
											}
										});

						$("#loginSubmit").click(function() {
							$("#candidate_login").submit();
						});
					});
</script>

</html>
