<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/template_start.jsp"%>
<%@include file="../../inc/page_head.jsp"%>


<%-- <%@include file="../../inc/template_scripts.jsp"%> --%>

<div id="page-content">
	<div class="content-header">
		<div class="header-section">
			<h1>
				<spring:message code="heading.contactimageMaster" />
				<br> <small><spring:message
						code="heading.editcontact" /></small>
			</h1>
			<span id="errorMsg"></span>
			<c:if test="${!empty success}">
				<div class="alert alert-success alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-check-circle"></i> <spring:message code="label.success"/>
					</h4>
					<spring:message code="${success}" />
				</div>
			</c:if>

			<c:if test="${!empty error}">
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">�</button>
					<h4>
						<i class="fa fa-times-circle"></i> <spring:message code="label.error"/>
					</h4>
					<spring:message code="${error}" />
				</div>
			</c:if>
		</div>
	</div>
	<ul class="breadcrumb breadcrumb-top">
		<li><spring:message code="menu.contactImage" /></li>
	</ul>
	<form action="<%=contexturl %>ManageGlobalSponsoredImage/GlobalSponsoredImageList/UpdateGlobalSponsoredImage" method="post" class="form-horizontal "
		id="ContactImage_form" enctype="multipart/form-data">
		<div class="row">
			<input name="id" value="${imageManagement.globalId}" type="hidden">
<%-- 			<input name="userId" value="${module.moduleId}" type="hidden"> --%>
			<div class="">
				<div class="block">
					<div class="block-title">
						<h2>
							<strong>Contact Image</strong>
<!-- 							<small style="float:left; margin-top: 4px;">This info is non editable and for view only purpose.</small> -->
						</h2>
					</div>
						
					<div class="form-group">
                        <label class="col-md-4 control-label" for="imageType"><spring:message
								code="label.imageType" /><span class="text-danger">*</span></label>
                        <label class="col-md-1 control-label">${imageManagement.imageType}
							</label>
                    </div>
                    
                    
                    <div class="form-group img-list1" id="profilepic">
						<label class="col-md-4 control-label" for="offer_validtill">Atma Contact Image <span class="text-danger">*</span></label>
						<div class="col-md-8">
							<div class="img-list2" id="imagediv">
								<c:choose>
									<c:when test="${! empty imageManagement.contactImage}">
										<img border="0"
											src="<%=contexturl%>${imageManagement.contactImage.imagePath}"
											width="120" height="120" title="Click here to change photo"
											onclick="OpenFileDialog();"
											id="profileimage" style="cursor: pointer;" />

										<span class="text-content" onclick="OpenFileDialog();"
											title="Click here to change photo"><br> <br>
										<span>Change Contact Image</span></span>
									</c:when>
									<c:otherwise>
										<img border="0"
											src="<%=contexturl%>resources/img/application/default_profile.png"
											width="120" height="120" title="Click here to change photo"
											onclick="OpenFileDialog();"
											id="profileimage" style="cursor: pointer;" />

										<span class="text-content" onclick="OpenFileDialog();"
											title="Click here to change photo"><br> <br>
										<span>Upload Contact Image</span></span>
									</c:otherwise>
								</c:choose>
								
							</div>
							<div>
							<input type="file" name="imageChooser1" id="imageChooser1"
								style="visibility: hidden; height: 0px" />
							</div>
						</div>
					</div>
                    
         
           			
             </div>
            </div>
           </div>
           <div style="text-align: center; margin-bottom: 10px">
			<div class="form-group form-actions">
				<!-- 				<div class="col-md-9 col-md-offset-3"> -->
				<button id="imageForm_submit" type="submit" value="Validate!"
					class="btn btn-sm btn-primary save">
					<i class="fa fa-angle-right"></i>
					<spring:message code="button.save" />
				</button>
				<a class="btn btn-sm btn-primary"
					href="<%=contexturl%>ManageGlobalSponsoredImage/GlobalSponsoredImageList"></i> <spring:message
						code="button.cancel" /> </a>
				<!-- 				</div> -->
			</div>
		</div>
         
	</form>
	


</div>



<%@include file="../../inc/page_footer.jsp"%>
<%@include file="../../inc/template_scripts.jsp"%>
<%@include file="../../inc/chosen_scripts.jsp"%>
<script src="<%=contexturl %>resources/js/custom/featurecontrol.js"></script>
<script src="<%=contexturl %>resources/js/additional-methods.js"></script>

<script type="text/javascript">
	$(document)
			.ready(
					function(){
						$("#manageGlobalSponsoredImage").addClass("active");
						$("#ContactImage_form").validate(
								{	errorClass:"help-block animation-slideDown",
									errorElement:"div",
									errorPlacement:function(e,a){a.parents(".form-group > div").append(e)},
									highlight:function(e){$(e).closest(".form-group").removeClass("has-success has-error").addClass("has-error")},
									success:function(e){e.closest(".form-group").removeClass("has-success has-error")},
												rules : {
													imageChooser1 :{
														<c:if test = "${empty imageManagement.contactImage}">
														       required: true,
														</c:if>
														     extension: "jpg|jpeg",
														     filesize: 900000,
														     maxlength: 1000,
													  },
													
												},
												messages : {
													imageChooser1 : {
														<c:if test = "${empty imageManagement.contactImage}">
														 	required : 'Please select image',
														</c:if>
									                     	extension : 'Please select image of (.jpg,.jpeg) extension.' , 
									                    	maxlength :'The Image is too long. Please select another image'
									           },
												},
										});
										
								$("#module_submit").click(function() {
									$("#ContactImage_form").submit();
								});
					
	
	});
</script>


<script type="text/javascript">
	function OpenFileDialog(){
		document.getElementById("imageChooser1").click();
	}
	
	function changePhoto(){
		if($('#imageChooser1').val() != ""){			
			var imageName = document.getElementById("imageChooser1").value;
			var ext1 = imageName.substring(imageName.lastIndexOf("."));
			var ext = ext1.toLowerCase();
			if(ext == '.jpg' || ext == '.jpeg')
				$('#changePhoto').submit();		
			else
				alert("Please select only image file");		
		}
	}
	
</script>

<script type="text/javascript">
    function showimages(files) {
        for (var i = 0, f; f = files[i]; i++) {
           
                       
            var reader = new FileReader();
            reader.onload = function (evt) {
            	$( "#image" ).remove();
            	$( "#imagediv" ).html("");
            	
//                 var img = '<div class="col-md-6 id="imagediv"><img border="0" src="' + evt.target.result + '" width="120" height="120" title="Click here to change photo" style="cursor: pointer;"  onclick="OpenFileDialog();" class="img-circle pic" id="image"/></div>';
                  var img ='<div><img border="0" src="' + evt.target.result + '" width="120" height="120" title="Click here to change photo" onclick="OpenFileDialog();" id="image" style="cursor: pointer;" /><span class="text-content" onclick="OpenFileDialog();" title="Click here to change photo"><br><br><span>Change Contact Image</span></span></div>';
                 $('#imagediv').html(img);
            }
            reader.readAsDataURL(f);
        }
    }

    $('#imageChooser1').change(function (evt) {
        showimages(evt.target.files);
    });
</script>



<style>
.chosen-container {
	width: 250px !important;
}
#map-canvas {
	height: 300px
}
</style>


<%@include file="../../inc/template_end.jsp"%>