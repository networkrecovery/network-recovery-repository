<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->

<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/template_start.jsp"%>
<%@include file="../../inc/page_head.jsp"%>

<link rel="stylesheet" type="text/css" href="<%=contexturl %>resources/css/jquery.cleditor.css" />
<style>
.cleditorToolbar {
	background: url('<%=contexturl %>resources/img/cleditor/toolbar.gif') repeat
}

.cleditorButton {
	float: left;
	width: 24px;
	height: 24px;
	margin: 1px 0 1px 0;
	background: url('<%=contexturl %>resources/img/cleditor/buttons.gif')
}

.cleditorMain {
	height: 250px !important
}

.rest_photo {
    width: 170px;
    text-align: left;
    font: 11px 'Calibri';
    color: #666;
    border: 10px solid #E6E6E6;;
    float: left;
    margin: 5px;
}

#delete{
	float: right;
	margin-top: -8px;
	margin-left: 142px;
	padding-left: 4px;
	padding-right: 2px;
	position: absolute;
	z-index: 100000;
}
</style>

<div id="page-content">
	<div class="content-header">
		<div class="header-section">
			<h1>
				<i class="fa fa-map-marker"></i>
				<spring:message code="heading.promocodeDescription" />
				<br> <small><spring:message code="heading.editpromocode" />
				</small>
			</h1>
			<span id="errorMsg"></span>
			<c:if test="${!empty success}">
				<div class="alert alert-success alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-check-circle"></i> Success
					</h4>
					<spring:message code="${success}" />
				</div>
			</c:if>
		</div>
	</div>
	<ul class="breadcrumb breadcrumb-top">
		<li><spring:message code="heading.promocodeDescription" />
		</li>
	</ul>
	<div class="block full" id="formView">
		<form action="UpdatePromoDescription" method="post" class="form-horizontal"
			enctype="multipart/form-data" id="promoDescription_form">
			<div class="form-group">
				<input id="PromoDescription_Id" name="descriptionId" type="hidden"
					value="${promoDescription.promoDescriptionId}"> 
				<input id="outlet_Id" name="outletId" type="hidden" value="${outletId}"> 
				<label class="col-md-3 control-label" for="promoDescription_Name"><spring:message
						code="label.title" /><span class="text-danger">*</label>
				<div class="col-md-9">
					<input id="promoDescription_Name" name="title" class="form-control"
						value="${promoDescription.title}"
						placeholder="<spring:message code="label.title"/>.."
						type="text">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="promoDescription_description"><spring:message
						code="label.description" /><span class="text-danger">*</label>
				<div class="col-md-9">
					<textarea id="promoDescription_description" name="description"
						class="form-control" rows="5"
						placeholder="<spring:message code="label.description"/>..">${promoDescription.description}</textarea>
				</div>
			</div>
			<c:if test="${image!=null}">
				<div class="form-group">
					<label class="col-md-3 control-label" for="promoDescription_validtill"></label>
					<div class="col-md-9">
						<span id="delete" ><a href="#delete_promoDescriptionImg_popup" data-toggle="modal"
							onclick="deletePromoDescriptionImg(${image.imageId})" title="Delete"class="btn btn-xs btn-danger">
							<i class="fa fa-times"></i></a>
						</span>
						<img border="0" src="<%=contexturl %>${image.imagePath}" width="150"
							height="110">
					</div>
				</div>
			</c:if>
			<div class="form-group">
				<c:if test="${image!=null}">
					<label class="col-md-3 control-label" for="promoDescription_image"><spring:message code="label.changepicture"/>
						</label>
				</c:if>
				<c:if test="${image==null}">
					<label class="col-md-3 control-label" for="promoDescription_image"><spring:message code="label.addpicture"/>
						</label>
				</c:if>
				<div class="col-md-9">
					<input type="file" name="photo" id="profile"
						accept="image/jpg,image/jpeg,image/png,image/gif,image/bmp,image/tiff"
						style="padding-top: 7px;" />
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-3 control-label" for="promoDescription_validity"><spring:message
						code="label.offerValidity" /></label>
				<div class="col-md-9">
					<label class="switch switch-primary"> <input
						id="promoDescription_validity" name="promocodeValidity" type="checkbox"
						<c:if test="${promoDescription.promocodeValidity}"><spring:message code="label.checked"/> </c:if> /> <span></span>
					</label>
				</div>
			</div>

			<div class="form-group" id="expdate" style="display:none;">
				<label class="col-md-3 control-label" for="promoDescription_validtill"><spring:message
						code="label.offervalidtill" /><span class="text-danger">*</span></label>
				<div class="col-md-9">
					<input type="text" id="example-datepicker3" name="expirydate"
						style="width: 200px;" value="${expirydate}"
						class="form-control input-datepicker"
						data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy">
				</div>
				<label style="color: #e74c3c;margin-left: 289px;" id="expiryDateError"></label>
			</div>

			<div class="form-group">
				<label class="col-md-3 control-label" for="promoDescription_termsCondition"><spring:message
						code="label.termsandcondition" /><span class="text-danger">*</label>
				<div class="col-md-9">
					<textarea id="promoDescription_termsCondition" name="termsandcondition">${promoDescription.termsandcondition}</textarea>
					<label style="color: #e74c3c;" id="termsError"></label>
				</div>
			</div>
			<div class="form-group form-actions">
				<div class="col-md-9 col-md-offset-3">
					<button id="promoDescription_submit" type="submit"
						class="btn btn-sm btn-primary save">
						<i class="fa fa-angle-right"></i> <spring:message code="button.save" />
					</button>
						<a  class="btn btn-sm btn-primary" href="PromoDescriptionList?outletId=${outletId}"><i class="gi gi-remove"></i>
							<spring:message code="button.cancel" /> </a>
				</div>
			</div>
		</form>
	</div>
</div>

<div class="block full" id="gridView">
	<div class="block-title">
		<h2>
			<strong><spring:message code="label.promocode"/></strong>
		</h2>
	</div>
	<div class="table-responsive">
		<table id="example-datatable"
			class="table table-vcenter table-condensed table-bordered">
			<thead>
				<tr>
					<th class="text-center"><spring:message code="label.id"/></th>
					<th class="text-center"><spring:message code="label.promocode"/></th>
					<th class="text-center"><spring:message code="label.codeholder"/></th>
					<th class="text-center"><spring:message code="label.activationdate"/></th>
				</tr>
			</thead>
			<tbody id="tbody">
				<c:forEach var="currentPromocode" items="${promocodeList}" varStatus="count">
					<tr>
						<td class="text-center">${count.count}</td>
						<td class="text-center"><c:out value="${currentPromocode.code}" /></td>
						<td class="text-center"><c:out value="${currentPromocode.codeholder.fullName}" /></td>
						<td class="text-center"><c:out value="${currentPromocode.activationDate}" /></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
</div>

<div id="delete_promoDescriptionImg_popup" class="modal fade" tabindex="-1"
		role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<form action="DeletePromoDescriptionImage" method="post" class="form-horizontal form-bordered"
						id="delete_promoDescriptionImg_Form">
						<div style="padding: 10px; height: 110px;">
							<label><spring:message code="label.doyouwanttodeletetheimage"/></label>
							<div class="col-xs-12 text-right">
								<button type="button" class="btn btn-sm btn-default"
									data-dismiss="modal">No</button>
								<div id="delete_promoDescriptionImgYes" class="btn btn-sm btn-primary">Yes</div>
								<input type="hidden" name="promoDescriptionImgId" id="promoDescriptionImgId">
								<input type="hidden" name="outletId" id="outletId">
								<input type="hidden" name="promoDescriptionId" id="promoDescriptionId">
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

<%@include file="../../inc/page_footer.jsp"%>
<%@include file="../../inc/template_scripts.jsp"%>

<script src="<%= contexturl %>resources/js/pages/tablesDatatables.js"></script>
<script>$(function(){ TablesDatatables.init(4); });</script>
<script type="text/javascript" src="<%=contexturl %>resources/js/jquery.cleditor.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		var validityFlag=false;
		$("#promoDescription_termsCondition").cleditor(); 
		
		$(".input-datepicker, .input-daterange").datepicker({weekStart : 1});
		
		$(".input-timepicker24").timepicker({minuteStep:1,showSeconds:!0,showMeridian:!1});
		
		
		 if ($('#promoDescription_validity').is(':checked')) {
	        	$("#expdate").css("display", "block");
	        	 validityFlag=true;
	        }else{
	        	$("#expdate").css("display", "none");
	        	 validityFlag=false;
	        }

		$("#promoDescription_form").validate(
		{	errorClass:"help-block animation-slideDown",
			errorElement:"div",
			errorPlacement:function(e,a){a.parents(".form-group > div").append(e)},
			highlight:function(e){$(e).closest(".form-group").removeClass("has-success has-error").addClass("has-error")},
			success:function(e){e.closest(".form-group").removeClass("has-success has-error")},
			rules:{ title:{required:!0, maxlength : 100,minlength : 5},
				description:{required:!0,maxlength : 500},
				promocodeFile:{required:!0},
				termsandcondition:{required:!0}
		},
		 messages:{title:{required:'<spring:message code="validation.title"/>',
			maxlength :'<spring:message code="validation.title100character"/>',
			minlength :'<spring:message code="validation.title5character"/>'},
				description:{required:'<spring:message code="validation.description"/>',
					maxlength :'<spring:message code="validation.description500character"/>'},
				promocodeFile:{required:'<spring:message code="validation.promocodeFile"/>'},
				termsandcondition:{required:'<spring:message code="validation.termsandconditions"/>'}
		},
		});
						
		$("#promoDescription_submit").click(function(event){
			var termCondition = $('#promoDescription_termsCondition').val();
			var expirydate = $('#example-datepicker3').val();
			if(validityFlag && expirydate.length<1){
				$("#expiryDateError").text("<spring:message code="validation.expirydate"/>");
				 return false;
				}
			if(termCondition.length<3){
				$("#termsError").text("Please enter terms and conditions");
				return false;
			}
			if(termCondition.length>5000){
				$("#termsError").text("<spring:message code="validation.termsandconditionsmax"/>");
				return false;
			}
 			 $("#promoDescription_form").submit();
		});
		 $('#promoDescription_validity').click(function() {
		        if ($('#promoDescription_validity').is(':checked')) {
		        	$("#expdate").css("display", "block");
		        	
		        	 validityFlag=true;
		        }
		        else{
		        	$("#expdate").css("display", "none");
		        	
		        	$("#example-datepicker3").text("");
		        
		        	$("#expiryDateError").text("");
		        	 validityFlag=false;
		        }
		    });
		$("#delete_promoDescriptionImgYes").click(function(){
			$("#promoDescriptionImgId").val(selectedId);
			$("#outletId").val(${outletId});
			$("#promoDescriptionId").val(${promoDescription.promoDescriptionId});
			$("#delete_promoDescriptionImg_Form").submit();
		});
		
		
		
		
		
	});
	
	function deletePromoDescriptionImg(id){
		selectedId = id;
	}
</script>
<%@include file="../../inc/template_end.jsp"%>