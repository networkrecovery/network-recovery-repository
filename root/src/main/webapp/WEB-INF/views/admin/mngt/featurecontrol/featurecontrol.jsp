<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<div class="block-title">
	<h2>
		<strong><spring:message code="heading.featurecontrol" /></strong>
		<small style="float:left; margin-top: 4px;">This section is for optional services of Gourmet7. Make sure you understand the product thoroughly before clicking. For more details about any product please email at product@gourmet7.com</small>
	</h2>
</div>
<input type="hidden" name="controlId" value="${control.controlId}"/>

<div class="form-group">
	<label class="col-md-4 control-label" for="level"><spring:message
			code="label.offer" /></label>
	<div class="col-md-6">

		<input id="offer" name="offer" class="form-control feature-control" type="checkbox"
			<c:if test="${control.offer }">checked="checked"</c:if> <c:if test="${!empty viewMode }">disabled="disabled"</c:if> >
	</div>
	
</div>

<div class="form-group">
	<label class="col-md-4 control-label" for="level"><spring:message
			code="label.promocode" /></label>
	<div class="col-md-6">

		<input id="promocode" name="promocode" class="form-control feature-control"
			type="checkbox" <c:if test="${control.promocode}">checked="checked"</c:if> <c:if test="${!empty viewMode }">disabled="disabled"</c:if>>
	</div>
</div>

<div class="form-group">
	<label class="col-md-4 control-label" for="level"><spring:message
			code="label.giftameal" /></label>
	<div class="col-md-6">

		<input id="giftameal" name="giftameal" class="form-control feature-control"
			type="checkbox" <c:if test="${control.giftAMeal }">checked="checked"</c:if> <c:if test="${!empty viewMode }">disabled="disabled"</c:if>>
	</div>
</div>

<div class="form-group">
	<label class="col-md-4 control-label" for="level"><spring:message
			code="label.evite" /></label>
	<div class="col-md-6">

		<input id="evite" name="evite" class="form-control feature-control" type="checkbox"
			<c:if test="${control.evite }">checked="checked"</c:if> <c:if test="${!empty viewMode }">disabled="disabled"</c:if>>
	</div>
</div>

<div class="form-group">
	<label class="col-md-4 control-label" for="level"><spring:message
			code="label.preordermeal" /></label>
	<div class="col-md-6">

		<input id="preordermeal" name="preorder" class="form-control feature-control" type="checkbox"
			<c:if test="${control.preorder }">checked="checked"</c:if> <c:if test="${!empty viewMode }">disabled="disabled"</c:if>>
	</div>
</div>

<div class="form-group">
	<label class="col-md-4 control-label" for="level"><spring:message
			code="label.mpay" /></label>
	<div class="col-md-6">

		<input id="mpay" name="mpay" class="form-control feature-control" type="checkbox"
			<c:if test="${control.mpay }">checked="checked"</c:if> <c:if test="${!empty viewMode }">disabled="disabled"</c:if>>
	</div>
</div>

<div class="form-group">
	<label class="col-md-4 control-label" for="level"><spring:message
			code="label.waittimetracker" /></label>
	<div class="col-md-6">

		<input id="waittimetracker" name="waittimetracker" class="form-control feature-control"
			type="checkbox" <c:if test="${control.waitTimeTracker }">checked="checked"</c:if> <c:if test="${!empty viewMode }">disabled="disabled"</c:if>>
	</div>
</div>

<div class="form-group">
	<label class="col-md-4 control-label" for="level"><spring:message
			code="label.splitbill" /></label>
	<div class="col-md-6">

		<input id="splitbill" name="splitbill" class="form-control feature-control"
			type="checkbox" <c:if test="${control.splitBill }">checked="checked"</c:if> <c:if test="${!empty viewMode }">disabled="disabled"</c:if>>
	</div>
</div>

<div class="form-group">
	<label class="col-md-4 control-label" for="level"><spring:message
			code="lable.tableregistration" /></label>
	<div class="col-md-6">

		<input id="tableregistration" name="tableregistration" class="form-control feature-control"
			type="checkbox" <c:if test="${control.tableRegistration }">checked="checked"</c:if> <c:if test="${!empty viewMode }">disabled="disabled"</c:if>>
	</div>
</div>

<div class="form-group">
	<label class="col-md-4 control-label" for="level"><spring:message
			code="lable.callforservice" /></label>
	<div class="col-md-6">

		<input id="callforservice" name="callForService" class="form-control feature-control"
			type="checkbox" <c:if test="${control.callForService }">checked="checked"</c:if> <c:if test="${!empty viewMode }">disabled="disabled"</c:if>>
	</div>
</div>

<a href="#message_popup" data-toggle="modal" id="message_link"></a>
<a href="#warning_message_popup" data-toggle="modal" id="warning_message_link"></a>

<div id="message_popup" class="modal fade" tabindex="-1"
	role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
					<div class="alert alert-info alert-dismissable">
						<h3>Terms & Conditions</h3>
						<div id="messageInfo">
<%-- 							<spring:message code="label.featurecontrolselection" /> --%>
						</div>
						
					</div>
					<div style="padding: 10px; margin-bottom: 10px;">
						<div class="col-xs-12 text-right">
							<button type="button" class="btn btn-sm btn-info"
								data-dismiss="modal"><spring:message code="label.ok"/></button>
						</div>
					</div>
			</div>
		</div>
	</div>
</div>

<div id="warning_message_popup" class="modal fade" tabindex="-1"
	role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
					<div class="alert alert-warning alert-dismissable">
						<i class="fa fa-exclamation-circle"></i> <spring:message code="label.featurecontroldeselection" />
					</div>
					<div>
						<spring:message code="label.confirmdeselect" />
					</div>
					<div style="padding: 10px; margin-bottom: 10px;">
						<div class="col-xs-12 text-right">
							<button type="button" class="btn btn-sm btn-default"
								data-dismiss="modal" id="no_deselect"><spring:message code="label.no" /></button>
							<button type="button" class="btn btn-sm btn-warning"
								data-dismiss="modal"><spring:message code="label.yes"/></button>
						</div>
					</div>
					<input type="hidden"  id="current_feature">
			</div>
		</div>
	</div>
</div>