package com.astrika.companymngt.model.company;

import org.joda.time.DateTime;

import com.astrika.surveymngt.model.SurveyManager;

/**
 * Created by gaurav on 20/02/16.
 */
public class CompanySurveyResetManager {
    private CompanyMaster companyMaster;
    private SurveyManager surveyManager;

    public CompanySurveyResetManager(){
    	/*empty method*/
    }

    public CompanySurveyResetManager(CompanyMaster companyMaster,SurveyManager surveyManager){
        this.companyMaster = companyMaster;
        this.surveyManager = surveyManager;
    }


    public CompanyMaster getCompanyMaster() {
        return companyMaster;
    }

    public void setCompanyMaster(CompanyMaster companyMaster) {
        this.companyMaster = companyMaster;
    }

    public SurveyManager getSurveyManager() {
        return surveyManager;
    }

    public void setSurveyManager(SurveyManager surveyManager) {
        this.surveyManager = surveyManager;
    }

    public boolean isResetEligible(){
        if(surveyManager == null) 
            return false;
        DateTime now = new DateTime();
        DateTime lastSurveydate = surveyManager.getLastDateToComplete();
        return now.isAfter(lastSurveydate);
    }
}
