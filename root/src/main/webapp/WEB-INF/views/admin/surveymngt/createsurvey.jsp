<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<%@include file="../inc/config.jsp"%>
<%@include file="../inc/template_start.jsp"%>
<%@include file="../inc/page_head.jsp"%>

<div id="page-content">
	<div class="content-header">
		<div class="header-section">
			<c:if test="${!empty error}">
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-times-circle"></i>
						<spring:message code="label.error" />
					</h4>
					<spring:message code="${error}" />
				</div>
			</c:if>

			<h1>
				<!-- 				<i class="fa fa-map-marker"></i> -->
				<spring:message code="heading.surveymaster" />
				<br> <small><spring:message code="heading.managesurvey" /></small>
			</h1>
			<span id="errorMsg"></span>
		</div>
	</div>
	<ul class="breadcrumb breadcrumb-top">
		<%-- 		<li><spring:message code="menu.survey" /></li> --%>
		<li><strong>${module.moduleName}</strong></li>
	</ul>

	<form action="<%=contexturl%>ManageSurvey/CreateSurvey/SaveSurvey"
		method="post" class="form-horizontal ui-formwizard" id="Survey_form">
		<div class="row">

			<div class="">
				<div class="block">
					<div class="block">
						<div class="block-title">
							<h2>
								<strong><spring:message code="heading.section1" /></strong>
								 
								 <input  id="act" type="hidden" name="act"> 
								
							</h2>
						</div>
						<div class="form-group">

							<input name="moduleId" id="moduleId" type="hidden"
								value="${moduleId}" /> <label class="col-md-3 control-label"
								for="question_one"> <spring:message
									code="label.Question1" />
							</label>
							<div class="col-md-6">

								<input id="question1" name="1" class="form-control"
									placeholder="Enter the first question" type="text">
							</div>
						</div>
						<div class="form-group">
                            <label class="col-md-3 control-label">
                                Explanation 1
                            </label>
                            <div class="col-md-6">

                                <input id="question_exp_1" name="e1" class="form-control"
                                    placeholder="Enter the first question explanation" type="text">
                            </div>
                        </div>
						<div class="form-group">
							<label class="col-md-3 control-label" for="question_two">
								<spring:message code="label.Question2" />
							</label>
							<div class="col-md-6">

								<input id="question2" name="2" class="form-control"
									placeholder="Enter the Second question" type="text">
							</div>
						</div>
						<div class="form-group">
                            <label class="col-md-3 control-label">
                                Explanation 2
                            </label>
                            <div class="col-md-6">

                                <input id="question_exp_2" name="e2" class="form-control"
                                    placeholder="Enter the second question explanation" type="text">
                            </div>
                        </div>
						<div class="form-group">
							<label class="col-md-3 control-label" for="question_three">
								<spring:message code="label.Question3" />
							</label>
							<div class="col-md-6">

								<input id="question3" name="3" class="form-control"
									placeholder="Enter the third question" type="text">
							</div>

						</div>
						<div class="form-group">
                            <label class="col-md-3 control-label">
                                Explanation 3
                            </label>
                            <div class="col-md-6">

                                <input id="question_exp_3" name="e3" class="form-control"
                                    placeholder="Enter the third question explanation" type="text">
                            </div>
                        </div>

					</div>
					<div class="block">
						<div class="block-title">
							<h2>
								<%-- 							<strong><spring:message code="heading.section1" /></strong> --%>
								<strong>Section2</strong>
							</h2>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label" for="question_one">
								<spring:message code="label.Question1" />
							</label>
							<div class="col-md-6">

								<input id="question4" name="4" class="form-control"
									placeholder="Enter the first question" type="text">
							</div>
						</div>
						<div class="form-group">
                            <label class="col-md-3 control-label">
                                Explanation 1
                            </label>
                            <div class="col-md-6">

                                <input id="question_exp_4" name="e4" class="form-control"
                                    placeholder="Enter the first question explanation" type="text">
                            </div>
                        </div>
						<div class="form-group">
							<label class="col-md-3 control-label" for="question_two">
								<spring:message code="label.Question2" />
							</label>
							<div class="col-md-6">

								<input id="question5" name="5" class="form-control"
									placeholder="Enter the Second question" type="text">
							</div>
						</div>
						<div class="form-group">
                            <label class="col-md-3 control-label">
                                Explanation 2
                            </label>
                            <div class="col-md-6">

                                <input id="question_exp_5" name="e5" class="form-control"
                                    placeholder="Enter the second question explanation" type="text">
                            </div>
                        </div>
						<div class="form-group">
							<label class="col-md-3 control-label" for="question_three">
								<spring:message code="label.Question3" />
							</label>
							<div class="col-md-6">

								<input id="question6" name="6" class="form-control"
									placeholder="Enter the third question" type="text">

							</div>
						</div>
						<div class="form-group">
                            <label class="col-md-3 control-label">
                                Explanation 3
                            </label>
                            <div class="col-md-6">

                                <input id="question_exp_6" name="e6" class="form-control"
                                    placeholder="Enter the third question explanation" type="text">
                            </div>
                        </div>
					</div>
					<div class="block">
						<div class="block-title">
							<h2>
								<%-- 							<strong><spring:message code="heading.section1" /></strong> --%>
								<strong>Section3</strong>
							</h2>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label" for="question_one">
								<spring:message code="label.Question1" />
							</label>
							<div class="col-md-6">

								<input id="question7" name="7" class="form-control"
									placeholder="Enter the first question" type="text">
							</div>
						</div>
						<div class="form-group">
                            <label class="col-md-3 control-label">
                                Explanation 1
                            </label>
                            <div class="col-md-6">

                                <input id="question_exp_7" name="e7" class="form-control"
                                    placeholder="Enter the first question explanation" type="text">
                            </div>
                        </div>
						<div class="form-group">
							<label class="col-md-3 control-label" for="question_two">
								<spring:message code="label.Question2" />
							</label>
							<div class="col-md-6">

								<input id="question8" name="8" class="form-control"
									placeholder="Enter the Second question" type="text">
							</div>
						</div>
						<div class="form-group">
                            <label class="col-md-3 control-label">
                                Explanation 2
                            </label>
                            <div class="col-md-6">

                                <input id="question_exp_8" name="e8" class="form-control"
                                    placeholder="Enter the second question explanation" type="text">
                            </div>
                        </div>
						<div class="form-group">
							<label class="col-md-3 control-label" for="question_three">
								<spring:message code="label.Question3" />
							</label>
							<div class="col-md-6">

								<input id="question9" name="9" class="form-control"
									placeholder="Enter the third question" type="text">

							</div>
						</div>
						<div class="form-group">
                            <label class="col-md-3 control-label">
                                Explanation 3
                            </label>
                            <div class="col-md-6">

                                <input id="question_exp_9" name="e9" class="form-control"
                                    placeholder="Enter the third question explanation" type="text">
                            </div>
                        </div>
					</div>
					<div class="block">
						<div class="block-title">
							<h2>
								<%-- 							<strong><spring:message code="heading.section1" /></strong> --%>
								<strong>Section4</strong>
							</h2>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label" for="question_one">
								<spring:message code="label.Question1" />
							</label>
							<div class="col-md-6">

								<input id="question10" name="10" class="form-control"
									placeholder="Enter the first question" type="text">
							</div>
						</div>
						<div class="form-group">
                            <label class="col-md-3 control-label">
                                Explanation 1
                            </label>
                            <div class="col-md-6">

                                <input id="question_exp_10" name="e10" class="form-control"
                                    placeholder="Enter the first question explanation" type="text">
                            </div>
                        </div>
						<div class="form-group">
							<label class="col-md-3 control-label" for="question_two">
								<spring:message code="label.Question2" />
							</label>
							<div class="col-md-6">

								<input id="question11" name="11" class="form-control"
									placeholder="Enter the Second question" type="text">
							</div>
						</div>
						<div class="form-group">
                            <label class="col-md-3 control-label">
                                Explanation 2
                            </label>
                            <div class="col-md-6">

                                <input id="question_exp_11" name="e11" class="form-control"
                                    placeholder="Enter the second question explanation" type="text">
                            </div>
                        </div>
						<div class="form-group">
							<label class="col-md-3 control-label" for="question_three">
								<spring:message code="label.Question3" />
							</label>
							<div class="col-md-6">

								<input id="question12" name="12" class="form-control"
									placeholder="Enter the third question" type="text">

							</div>
						</div>
						<div class="form-group">
                            <label class="col-md-3 control-label">
                                Explanation 3
                            </label>
                            <div class="col-md-6">

                                <input id="question_exp_12" name="e12" class="form-control"
                                    placeholder="Enter the third question explanation" type="text">
                            </div>
                        </div>
					</div>
					<div class="block">
						<div class="block-title">
							<h2>
								<%-- 							<strong><spring:message code="heading.section1" /></strong> --%>
								<strong>Section5</strong>
							</h2>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label" for="question_one">
								<spring:message code="label.Question1" />
							</label>
							<div class="col-md-6">

								<input id="question13" name="13" class="form-control"
									placeholder="Enter the first question" type="text">
							</div>
						</div>
						<div class="form-group">
                            <label class="col-md-3 control-label">
                                Explanation 1
                            </label>
                            <div class="col-md-6">

                                <input id="question_exp_13" name="e13" class="form-control"
                                    placeholder="Enter the first question explanation" type="text">
                            </div>
                        </div>
						<div class="form-group">
							<label class="col-md-3 control-label" for="question_two">
								<spring:message code="label.Question2" />
							</label>
							<div class="col-md-6">

								<input id="question14" name="14" class="form-control"
									placeholder="Enter the Second question" type="text">
							</div>
						</div>
						<div class="form-group">
                            <label class="col-md-3 control-label">
                                Explanation 2
                            </label>
                            <div class="col-md-6">

                                <input id="question_exp_14" name="e14" class="form-control"
                                    placeholder="Enter the second question explanation" type="text">
                            </div>
                        </div>
						<div class="form-group">
							<label class="col-md-3 control-label" for="question_three">
								<spring:message code="label.Question3" />
							</label>
							<div class="col-md-6">

								<input id="question15" name="15" class="form-control"
									placeholder="Enter the third question" type="text">

							</div>
						</div>
						<div class="form-group">
                            <label class="col-md-3 control-label">
                                Explanation 3
                            </label>
                            <div class="col-md-6">

                                <input id="question_exp_15" name="e15" class="form-control"
                                    placeholder="Enter the third question explanation" type="text">
                            </div>
                        </div>
					</div>
				</div>
			</div>
			<div style="text-align: center; margin-bottom: 10px">
				<div class="form-group form-actions">
					<!-- 				<div class="col-md-9 col-md-offset-3"> -->
					<div id="survey_submit"
						class="btn btn-sm btn-primary save">
						<i class="fa fa-angle-right"></i>
						<spring:message code="button.savepublish" />
					</div>				
					<a class="btn btn-sm btn-primary save" href="<%=contexturl %>AtmaAdmin">
							<spring:message code="button.cancel" /> </a>				
				</div>
			</div>
		</div>	
	</form>	


<a href="#create_survey_popup" data-toggle="modal" id = "surveypopup"></a>
<div id="create_survey_popup" class="modal fade" tabindex="-1"
		role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<div style="padding: 10px; height: 110px;">
							<label>Are you sure you want publish the survey for this ODA?</label>
							<div class="col-xs-12 text-right">
								<button type="button" class="btn btn-sm btn-default"
									data-dismiss="modal">
									<spring:message code="label.no" />
								</button>
								<div id="delete_company" class="btn btn-sm btn-primary">
									<spring:message code="label.yes" />
								</div>
							</div>
						</div>
				</div>
			</div>
		</div>
	</div>
</div>

<%@include file="../inc/page_footer.jsp"%>
<%@include file="../inc/template_scripts.jsp"%>
<%-- <%@include file="../inc/chosen_scripts.jsp"%> --%>
<script src="<%=contexturl %>resources/js/custom/featurecontrol.js"></script>
<script src="http://jqueryvalidation.org/files/dist/additional-methods.min.js"></script>
	<script type="text/javascript">
$(document).ready(
			function() {
				var companyForm ;
				var count = 0;
				$("#surveyBuilder").addClass("active");
			
				

	                  $("#survey_submit").click(function(){     

	                	  $("#Survey_form").submit();
	                  });
		
	                  $("#delete_company").click(function(){
	                	  count++;
	                	  if(count > 0){
	                		  $("#delete_company").attr('disabled', true);
	                	  }
	                	for(i = 1; i <= 15; ++i){
                              if (!($("#question" + i).val()).trim()) {
                                  // is empty or whitespace
                                  continue;
                              }
                              $("#question" + i).val($("#question" + i).val() + "<->" + $("#question_exp_" + i).val());
                          }
	          			companyForm.submit();
	          		});
	                  
	                  
	$("#Survey_form").validate(
	{	errorClass:"help-block animation-slideDown",
		errorElement:"div",
		errorPlacement:function(e,a){a.parents(".form-group > div").append(e)},
		highlight:function(e){$(e).closest(".form-group").removeClass("has-success has-error").addClass("has-error")},
		success:function(e){e.closest(".form-group").removeClass("has-success has-error")},
		rules : {
				1:{required:!0,maxlength:255},2:{required:!0,maxlength:255},3:{required:!0,maxlength:255},4:{required:!0,maxlength:255},5:{required:!0,maxlength:255},6:{required:!0,maxlength:255},7:{required:!0,maxlength:255},8:{required:!0,maxlength:255},9:{required:!0,maxlength:255},10:{required:!0,maxlength:255},11:{required:!0,maxlength:255},12:{required:!0,maxlength:255},13:{required:!0,maxlength:255},14:{required:!0,maxlength:255},15:{required:!0,maxlength:255},
				e1:{required:!0,maxlength:255},e2:{required:!0,maxlength:255},e3:{required:!0,maxlength:255},e4:{required:!0,maxlength:255},e5:{required:!0,maxlength:255},e6:{required:!0,maxlength:255},e7:{required:!0,maxlength:255},e8:{required:!0,maxlength:255},e9:{required:!0,maxlength:255},e10:{required:!0,maxlength:255},e11:{required:!0,maxlength:255},e12:{required:!0,maxlength:255},e13:{required:!0,maxlength:255},e14:{required:!0,maxlength:255},e15:{required:!0,maxlength:255}
												},
												messages : {
													1 : {
														required : 'Please enter a Question',
														maxlength : 'Question should not be more than 255 words'
													},
													2 : {
														required : 'Please enter a Question',
														maxlength : 'Question should not be more than 255 words'
													},
													3 : {
														required : 'Please enter a Question',
														maxlength : 'Question should not be more than 255 words'
													},
													4 : {
														required : 'Please enter a Question',
														maxlength : 'Question should not be more than 255 words'
													},
													5 : {
														required : 'Please enter a Question',
														maxlength : 'Question should not be more than 255 words'
													},
													6 : {
														required : 'Please enter a Question',
														maxlength : 'Question should not be more than 255 words'
													},
													7 : {
														required : 'Please enter a Question',
														maxlength : 'Question should not be more than 255 words'
													},
													8 : {
														required : 'Please enter a Question',
														maxlength : 'Question should not be more than 255 words'
													},

													9 : {

														required : 'Please enter a Question',
														maxlength : 'Question should not be more than 255 words'
													},
													10 : {

														required : 'Please enter a Question',
														maxlength : 'Question should not be more than 255 words'
													},
													11 : {
														required : 'Please enter a Question',
														maxlength : 'Question should not be more than 255 words'
													},
													12 : {
														required : 'Please enter a Question',
														maxlength : 'Question should not be more than 255 words'
													},

													13 : {

														required : 'Please enter a Question',
														maxlength : 'Question should not be more than 255 words'
													},
													14 : {
														required : 'Please enter a Question',
														maxlength : 'Question should not be more than 255 words'
													},
													15 : {
														required : 'Please enter a Question',
														maxlength : 'Question should not be more than 255 words'
													}
												},
												
												submitHandler: function(form) {
													$("#surveypopup").click();
													companyForm = form;								
							                }
					});
});
	</script>

<%@include file="../inc/template_end.jsp"%>