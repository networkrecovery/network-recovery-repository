<%
/**
 * page_footer.jsp
 *
 * Author: pixelcave
 *
 * The footer of each page
 *
 */
%>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="slider-container">
				<div id="slider" class="sponsorar-slider">
					<c:choose>
						<c:when test="${empty globalSponsorImageList}">

						</c:when>
						<c:otherwise>
							<c:forEach var="globalSponsor" items="${globalSponsorImageList}">
								<div class="slide">
									<img class="img-responsive"
										src="<%=contexturl%>${globalSponsor.globalImage.imagePath}" />
								</div>
							</c:forEach>
						</c:otherwise>
					</c:choose>
				</div>
			</div>
		</div>
	</div>
</div>


<!-- Footer -->

<!-- <div class="container"> -->
<!-- 	<div class=" social-icon pull-right"> -->
<!-- 		<strong>Follows us</strong> <img -->
<%-- 			src="<%=contexturl %>resources/img/01_twitter.png" alt="twitter"> --%>
<%-- 		<img src="<%=contexturl %>resources/img/02_facebook.png" --%>
<!-- 			alt="facebook"> <img -->
<%-- 			src="<%=contexturl %>resources/img/03_youtube.png" alt="youtube"> --%>
<%-- 		<img src="<%=contexturl %>resources/img/07_linkedin.png" --%>
<!-- 			alt="linkedin"> -->
<!-- 	</div> -->
<!-- </div> -->

<div class="container" id="footer">
		<%@include file="../inc/footer_items.jsp"%>
</div>



</body>
        <!-- END Footer -->
<%-- <script src="<%=contexturl %>resources/js/vendor/jquery-1.11.0.min.js"></script> --%>
<%-- <script src="<%=contexturl %>resources/js/vendor/bootstrap.min.js"></script> --%>
<script src="<%=contexturl %>resources/js/jquery.bxslider.js"></script>
<script src="<%=contexturl %>resources/js/ion-rangeSlider/ion.rangeSlider.min.js"></script>

<link rel="stylesheet" type="text/css" href="<%=contexturl %>resources/css/shieldui.css" />
<script type="text/javascript" src="<%=contexturl %>resources/js/progress/shieldui-all.min.js"></script>

<script type="text/javascript">
    //initialization options for the progress bar
    


    function resetActive(event, percent, step) {
        progress.value(percent);

        $(".progress-bar").css("width", percent + "%").attr("aria-valuenow", percent);
        $(".progress-completed").text(percent + "%");

        $("div").each(function () {
            if ($(this).hasClass("activestep")) {
                $(this).removeClass("activestep");
            }
        });

        if (event.target.className == "col-md-2") {
            $(event.target).addClass("activestep");
        } else {
            $(event.target.parentNode).addClass("activestep");
        }

        hideSteps();
        showCurrentStepInfo(step);
    }

    function hideSteps() {
        $("div").each(function () {
            if ($(this).hasClass("activeStepInfo")) {
                $(this).removeClass("activeStepInfo");
                $(this).addClass("hiddenStepInfo");
            }
        });
    }

    function showCurrentStepInfo(step) {
        var id = "#" + step;
        $(id).addClass("activeStepInfo");
    }
</script>

<!-- Steps Circular Progress - END -->
<script type="text/javascript">
    $(document).ready(function () {
        $('.sponsorar-slider').bxSlider({
            slideWidth: 1000,
            minSlides: 6,
            maxSlides: 4,
            moveSlides: 1,
            slideMargin: 0,
            auto: true,
            pager: false
        });
        $('.steps-slider').bxSlider({
            controls:false,
            auto: true,
            pager: true
        });
    });
</script>

</html>
