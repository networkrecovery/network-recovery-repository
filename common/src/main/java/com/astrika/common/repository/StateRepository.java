package com.astrika.common.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.astrika.common.model.location.StateMaster;

public interface StateRepository extends JpaRepository<StateMaster, Long> {

	
	@Query("select state from StateMaster state join fetch state.country where state.stateId = ?1") 
	StateMaster findByStateId(Long stateId);

	@Query("select state from StateMaster state join fetch state.country where state.stateName=?1")
	List<StateMaster> findByStateName(String stateName);


	@Query("select state from StateMaster state join fetch state.country where state.active = ?1 Order By state.stateName Asc")
	List<StateMaster> findByActive(boolean active);
	
	
	@Query("select state from StateMaster state join fetch state.country where state.active = true Order By state.stateName Asc")
	List<StateMaster> findAllByActiveTrue();
	
	@Query("select state from StateMaster state join fetch state.country where state.active = 1 and state.country.countryId=?1 Order By state.stateName Asc")
	List<StateMaster> findByCountryCountryIdAndActiveTrueOrderByStateNameAsc(
			long countryId);
	
	
	List<StateMaster> findByCountryCountryId(Long countryId);
	
	
	
	@Query(value="select  state.stateId,state.stateName,state.country.countryName from StateMaster state " +
			"where state.active = ?1 and (state.stateName like %?2% or "+
			"state.country.countryName like %?2%)"
	        , countQuery="select count(*) from StateMaster state  where" +
	        		"  state.active = ?1 and (state.stateName like %?2% or "+
	        		"state.country.countryName like %?2%)")
	Page<Object[]> findAllOnlyRequiredParamByStatus(boolean status,String searchOn,Pageable page);
}
