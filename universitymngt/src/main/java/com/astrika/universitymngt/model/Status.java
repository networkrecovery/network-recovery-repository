package com.astrika.universitymngt.model;

public enum Status {
	DRAFT(0),
	PUBLISH(1),
	CONFIRMED(2),
	PENDING(3),
	CANCELLED(4),
	COMPLETED(5);

	private final int id;

	private Status(int id){
		this.id = id;
	}

	public int getId() {
		return id;
	}


	public static Status fromInt(int id) {
		switch (id) {
		case 0:
			return DRAFT;
		case 1:
			return PUBLISH;
		case 2:
			return CONFIRMED;
		case 3:
			return PENDING;
		case 4:
			return CANCELLED;
		case 5:
			return COMPLETED;
		default:
			return null;
		}
	}
}
