<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->

<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/template_start.jsp"%>
<%@include file="../../inc/page_head.jsp"%>

<link rel="stylesheet" type="text/css" href="<%=contexturl %>resources/css/jquery.cleditor.css" />
<style>
.cleditorToolbar {
	background: url('<%=contexturl %>resources/img/cleditor/toolbar.gif') repeat
}

.cleditorButton {
	float: left;
	width: 24px;
	height: 24px;
	margin: 1px 0 1px 0;
	background: url('<%=contexturl %>resources/img/cleditor/buttons.gif')
}

.cleditorMain {
	height: 250px !important
}
</style>

<div id="page-content">
	<div class="content-header">
		<div class="header-section">
			<c:if test="${!empty error}">
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-times-circle"></i> <spring:message code="label.error"/>
					</h4>
					<spring:message code="${error}" />
				</div>
			</c:if>
			<h1>
				<i class="fa fa-map-marker"></i>
				<spring:message code="heading.offer" />
				<br> <small><spring:message code="heading.createaofferforthisrestaurant" />
				</small>
			</h1>
			<span id="errorMsg"></span>
		</div>
	</div>
	<ul class="breadcrumb breadcrumb-top">
		<li><spring:message code="heading.offer" />
		</li>
	</ul>
	<div class="block full" id="formView">
		<form action="SaveOffer" method="post" class="form-horizontal"
			enctype="multipart/form-data" id="Offer_form">
			<div class="form-group">
				<input id="offer_Id" name="offerId" type="hidden" value="0">
				<input id="outlet_Id" name="outletId" type="hidden"
					value="${outletId}"}> <label class="col-md-3 control-label"
					for="offer_Name"><spring:message code="label.title" /><span class="text-danger">*</span>
				</label>
				<div class="col-md-9">
					<input id="offer_Name" name="title" class="form-control" value="${offer.title}"
						placeholder="<spring:message code="label.title"/>.."
						type="text">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="offer_description"><spring:message
						code="label.description" /><span class="text-danger">*</span></label>
				<div class="col-md-9">
					<textarea id="offer_description" name="description"
						class="form-control" rows="5"
						placeholder="<spring:message code="label.description"/>..">${offer.description}</textarea>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="offer_image"><spring:message code="label.offerpicture"/>
					</label>
				<div class="col-md-9">
					<input type="file" name="photo" id="profile"
						accept="image/jpg,image/jpeg,image/png,image/gif,image/bmp,image/tiff"
						style="padding-top: 7px;" />
				</div>
				<small style="padding-left: 15px;">{File supported: .jpg, .jpeg, .png, .gif}</small>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="offer_validity"><spring:message
						code="label.offerValidity" /></label>
				<div class="col-md-9">
					<label class="switch switch-primary"> <input
						id="offer_validity" name="offerValidity" type="checkbox" 
						<c:if test="${offer.offerValidity}"><spring:message code="label.checked"/> </c:if>/> <span></span>
					</label>
				</div>
			</div>
            <c:set var="flag" value="true" /> 
			<div class="form-group" id="expdate" style="display:none;">
				<label class="col-md-3 control-label" for="offer_validtill"><spring:message
						code="label.offervalidtill" /><span class="text-danger" >*</span></label>
				<div class="col-md-9">
					<input type="text" id="example-datepicker3" name="expirydate"
						style="width: 200px;" class="form-control input-datepicker"
						data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy">
				</div>
				<label style="color: #e74c3c;margin-left: 289px;" id="expiryDateError"></label>
			</div>

			<div class="form-group" id="exptime" style="display:none;">
				<label class="col-md-3 control-label" for="example-timepicker24"></label>
				<div class="col-md-9">
					<div class="input-group bootstrap-timepicker" style="width: 200px;">
						<div class="bootstrap-timepicker-widget dropdown-menu">
							<table>
								<tbody>
									<tr>
										<td><a href="#" data-action="incrementHour"><i
												class="fa fa-chevron-up"></i> </a></td>
										<td class="separator">&nbsp;</td>
										<td><a href="#" data-action="incrementMinute"><i
												class="fa fa-chevron-up"></i> </a></td>
										<td class="separator">&nbsp;</td>
										<td><a href="#" data-action="incrementSecond"><i
												class="fa fa-chevron-up"></i> </a></td>
									</tr>
									<tr>
										<td><input type="text"
											class="form-control bootstrap-timepicker-hour" maxlength="2">
										</td>
										<td class="separator">:</td>
										<td><input type="text"
											class="form-control bootstrap-timepicker-minute"
											maxlength="2"></td>
										<td class="separator">:</td>
										<td><input type="text"
											class="form-control bootstrap-timepicker-second"
											maxlength="2"></td>
									</tr>
									<tr>
										<td><a href="#" data-action="decrementHour"><i
												class="fa fa-chevron-down"></i> </a></td>
										<td class="separator"></td>
										<td><a href="#" data-action="decrementMinute"><i
												class="fa fa-chevron-down"></i> </a></td>
										<td class="separator">&nbsp;</td>
										<td><a href="#" data-action="decrementSecond"><i
												class="fa fa-chevron-down"></i> </a></td>
									</tr>
								</tbody>
							</table>
						</div>
						<input type="text" id="example-timepicker24" name="expirytime"
							class="form-control input-timepicker24"> <span
							class="input-group-btn"> <a href="javascript:void(0)"
							class="btn btn-primary"><i class="fa fa-clock-o"></i> </a> </span>
					</div>
					<label style="color: #e74c3c;margin-left: 289px;" id="expiryTimeError"></label>
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-3 control-label" for="offer_termsCondition"><spring:message
						code="label.termsandcondition" /><span class="text-danger">*</span></label>
				<div class="col-md-9">
					<textarea id="offer_termsCondition" name="termsandcondition" >${offer.termsandcondition}</textarea>
					<label style="color: #e74c3c;" id="termsError"></label>
				</div>
			</div>

			<div class="form-group form-actions">
				<div class="col-md-9 col-md-offset-3">
					<button id="offer_submit" type="submit"
						class="btn btn-sm btn-primary save">
						<i class="fa fa-angle-right"></i> <spring:message code="button.save" />
					</button>
					<button id="offer_reset"  type="reset" class="btn btn-sm btn-warning">
						<i class="fa fa-repeat"></i>
						<spring:message code="button.reset" />
					</button>
						<a class="btn btn-sm btn-primary" href="OfferList?outletId=${outletId}"><i class="gi gi-remove"></i>
							<spring:message code="button.cancel" /> </a>
				</div>
			</div>
		</form>
	</div>
</div>
<%@include file="../../inc/page_footer.jsp"%>
<%@include file="../../inc/template_scripts.jsp"%>

<script type="text/javascript" src="<%=contexturl %>resources/js/jquery.cleditor.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		var validityFlag=false;
 		$("#offer_termsCondition").cleditor(); 
		
		$(".input-datepicker, .input-daterange").datepicker({weekStart : 1});
		
		$(".input-timepicker24").timepicker({minuteStep:1,showSeconds:!0,showMeridian:!1});
		$('#example-datepicker3').datepicker('setStartDate', new Date());
		$("#Offer_form").validate(
				{	errorClass:"help-block animation-slideDown",
					errorElement:"div",
					errorPlacement:function(e,a){a.parents(".form-group > div").append(e)},
					highlight:function(e){$(e).closest(".form-group").removeClass("has-success has-error").addClass("has-error")},
					success:function(e){e.closest(".form-group").removeClass("has-success has-error")},
					rules:{ title:{required:!0, maxlength : 100,minlength : 5},
							description:{required:!0, maxlength : 500},
							termsandcondition:{required:!0},
							
					},
					messages:{title:{required:'<spring:message code="validation.title"/>',
						maxlength :'<spring:message code="validation.title100character"/>',
							minlength :'<spring:message code="validation.title5character"/>'},
							description:{required:'<spring:message code="validation.description"/>',
								maxlength :'<spring:message code="validation.description500character"/>'},
							termsandcondition:{required:'<spring:message code="validation.termsandconditions"/>'},
						
						
					}
					
				});
		
		$("#offer_submit").click(function(event){
			var termCondition = $('#offer_termsCondition').val();
			var expirydate = $('#example-datepicker3').val();
			var expirytime = $('#example-timepicker24').val();
			if(validityFlag && expirydate.length<1){
			$("#expiryDateError").text("<spring:message code="validation.expirydate"/>")
			 return false;
			}
			if(validityFlag && expirytime.length<1){
				$("#expiryTimeError").text("<spring:message code="validation.expirytime"/>")
				 return false;
				}
			
			if(termCondition.length<3){
				$("#termsError").text("<spring:message code="validation.termsandconditions"/>")
				return false;
			}
			if(termCondition.length>1000){
				$("#termsError").text("<spring:message code="validation.termsandconditionsmax"/>")
				return false;
			}
			 $("#Offer_form").submit();
		});
		$("#offer_reset").click(function(event){
			
			$("iframe").contents().find("body").html('');
			
			
		});
		
		
		 $('#offer_validity').click(function() {
		        if ($('#offer_validity').is(':checked')) {
		        	$("#expdate").css("display", "block");
		        	$("#exptime").css("display", "block");
		        	 validityFlag=true;
		        }
		        else{
		        	$("#expdate").css("display", "none");
		        	$("#exptime").css("display", "none");
		        	$("#example-datepicker3").text("");
		        	$("#example-timepicker24").text("");
		        	$("#expiryDateError").text("");
		        	$("#expiryTimeError").text("");
		        	 validityFlag=false;
		        }
		    });
		
	});
</script>
<%@include file="../../inc/template_end.jsp"%>