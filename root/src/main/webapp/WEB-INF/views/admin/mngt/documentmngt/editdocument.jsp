<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/template_start.jsp"%>
<%@include file="../../inc/page_head.jsp"%>


<div id="page-content">
	<div class="content-header">
		<div class="header-section">
			<c:if test="${!empty error}">
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-times-circle"></i>
						<spring:message code="label.error" />
					</h4>
					<spring:message code="${error}" />
				</div>
			</c:if>

			<h1>
				<i class="fa fa-map-marker"></i>
				<spring:message code="heading.document" />
				<br> <small><spring:message code="heading.newdocument" /></small>
			</h1>
			<span id="errorMsg"></span>
		</div>
	</div>
	<ul class="breadcrumb breadcrumb-top">
		<li><spring:message code="menu.managedocument" /></li>
	</ul>
	<form action="<%=contexturl%>ManageDocument/DocumentList/UpdateDocument"
		method="post" class="form-horizontal ui-formwizard" id="Document_form"
		enctype="multipart/form-data">
		<div class="row">
		<input name="documentId" value="${documentMaster.documentId}" type="hidden">
			<div class="">
				<div class="block">
					<div class="block-title">
						<h2>
							<strong><spring:message code="heading.documentinfo" /></strong>
						</h2>
					</div>
					
					<input type="hidden" value="${documentMaster.contentType}" name="chosenContentId">
					<div class="form-group">
						<label class="col-md-4 control-label" for="contentType"><spring:message
								code="label.contentname" /><span class="text-danger">*</span></label>
						<div class="col-md-6">					
									<input id="contentTypeDoc" name="contentType"
										class="contentType" type="radio" value="Document" disabled
										<c:if test="${documentMaster.contentType eq 'Document'}">checked = "checked"</c:if>>Document <br>
									<input id="contentTypeLink" name="contentType"
										class="contentType" type="radio" value="Link" disabled
										<c:if test="${documentMaster.contentType eq 'Link'}">checked = "checked"</c:if>>Link			
						</div>
					</div>

					<div class="form-group" id="moduleDiv">
						<label class="col-md-4 control-label" for="chosenModuleId"><spring:message
								code="label.modulename" /><span class="text-danger">*</span></label>
						<div class="col-md-6">
							<select class="module_chosen" style="width: 200px;"
								id="chosenModuleId"
								data-placeholder="<spring:message code='label.choosemodule' />"
								name="chosenModuleId">
								<option value=""></option>
								<c:forEach items="${moduleList}" var="module">
									<option value="${module.moduleId}"
										<c:if test="${module.moduleId eq documentMaster.module.moduleId}">selected="selected"</c:if>>${module.moduleName}</option>
								</c:forEach>
							</select>
						</div>
					</div>

					<div class="form-group" id="categoryDiv">
						<label class="col-md-4 control-label" for="chosenCategoryId"><spring:message
								code="label.categoryname" /><span class="text-danger">*</span></label>
						<div class="col-md-6">
							<select class="category_chosen" style="width: 200px;"
								id="chosenCategoryId"
								data-placeholder="<spring:message code='label.choosecategory' />"
								name="chosenCategoryId">
								<option value=""></option>
								<c:forEach items="${categoryList}" var="category">
									<option value="${category.categoryId}"
										<c:if test="${category.categoryId eq documentMaster.category.categoryId}">selected="selected"</c:if>>${category.categoryName}</option>
								</c:forEach>
							</select>
						</div>
					</div>
					<input type="hidden" value="${documentMaster.section}" name="chosenSectionId">
					<div class="form-group" id="sectionDiv">
						<label class="col-md-4 control-label" for="chosenSectionId"><spring:message
								code="label.sectionname" /><span class="text-danger">*</span></label>
						<div class="col-md-6">
							<select class="section_chosen" style="width: 200px;"
								id="chosenSectionId"
								data-placeholder="<spring:message code='label.choosesection' />"
								name="chosenSectionId" disabled="disabled"> 
								<option value=""></option>
								<c:forEach items="${sectionList}" var="section">
									<option value="${section}"
										<c:if test="${section eq documentMaster.section}">selected="selected"</c:if>>${section}</option>
								</c:forEach>
							</select>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label" for="title"><spring:message
								code="label.title" /> <span class="text-danger">*</span> </label>
						<div class="col-md-6">

							<input id="title" name="title" class="form-control"
								placeholder="<spring:message code='label.title'/>.." type="text"
								value="${documentMaster.title}">
						</div>
					</div>
					<c:if test="${(documentMaster.contentType eq 'Document') && (documentMaster.section eq 'BluePrint')}">
						<div class="form-group" id="docDescription">
							<label class="col-md-4 control-label" for="documentDescription"><spring:message
									code="label.documentdescription" /><span class="text-danger">*</span></label>
							<div class="col-md-6">
								<textarea id="documentDescription" name="documentDescription"
									class="form-control"
									placeholder="<spring:message code='label.description'/>..">${documentMaster.documentDescription}</textarea>
							</div>
						</div>
				
						<div class="form-group" id="lifestageDiv">
							<label class="col-md-4 control-label" for="chosenLifeStage"><spring:message
									code="label.lifestage" /><span class="text-danger">*</span></label>
							<div class="col-md-6">
								<select class="lifestage_chosen" style="width: 200px;"
									id="chosenLifeStage"
									data-placeholder="<spring:message code='label.chooselifestage' />"
									name="chosenLifeStage">
									<option value=""></option>
									<c:forEach items="${lifeStageList}" var="lifeStages">
										<option value="${lifeStages}"
											<c:if test="${lifeStages eq documentMaster.lifeStages}">selected="selected"</c:if>>${lifeStages.name}</option>
									</c:forEach>
								</select>
							</div>
						</div>
				
				
						<div class="form-group" id="doc" >
							<label class="col-md-4 control-label" for="document"><spring:message
									code="label.document" /> <span class="text-danger">*</span> </label>
							<div class="col-md-6">
				
								<input id="document" name="document" class=""
									placeholder="<spring:message code='label.uploaddocument'/>.."
									type="file"><a target="_blank"
									href="<%=contexturl%>Files${documentMaster.documenturlpath}">${documentMaster.documentName}</a>
							</div>
						</div>				
					
					<div class="form-group" id="showImgDiv">
						
							<label class="col-md-4 control-label" for="document4">BluePrint
								Images <span class="text-danger">*</span>
							</label>
							<input type="button" name="answer" value="Upload Image" onclick="showUploadImgDiv()" style="display:inherit;"/>
						<div class = "col-md-6" style="min-height: 350px; height: auto; overflow:hidden;border: 2px dashed #eaedf1; background-color: #f9fafc; padding: 1em;">
							<c:forEach items="${documentMaster.blueprintImages}" var="image">
								<div class="col-md-3 rest_photo" style="margin-top:5px; margin-bottom:5px;">
									<span id="delete" style=" position: absolute; right: 16px;">
										<a href="#delete_restImg_popup" data-toggle="modal"
										onclick="deleteRestImg(${image.blueprintImageId})" title="Delete"
										class="btn btn-xs btn-danger"><i class="fa fa-times"></i>
									</a>
									</span>
									<img border="0" class="img-responsive" src="<%=contexturl %>${image.image.imagePath}" width="150"
									height="110">
								</div>
							</c:forEach>
						</div>
					</div>

						<div class="form-group" id="bpImages" style="display:none;">
							<label class="col-md-4 control-label" for="document4">BluePrint
								Images <span class="text-danger">*</span>
							</label>
							<input type="button" name="answer2" value="Show Image" onclick="showRestImgDiv()" /><br>
							<div id="myId" class="dropzone dz-clickable col-md-6"
								enctype="multipart/form-data">
								<div class="dz-default dz-message">
									<span>Upload Images here</span>
								</div>
							</div>
						</div>
					</c:if>
					
					<c:if test="${(documentMaster.contentType eq 'Document') && not(documentMaster.section eq 'BluePrint')}">
						<div class="form-group" id="docDescription">
							<label class="col-md-4 control-label" for="documentDescription"><spring:message
									code="label.documentdescription" /><span class="text-danger">*</span></label>
							<div class="col-md-6">
								<textarea id="documentDescription" name="documentDescription"
									class="form-control"
									placeholder="<spring:message code='label.description'/>..">${documentMaster.documentDescription}</textarea>
							</div>
						</div>
				
						<div class="form-group" id="lifestageDiv">
							<label class="col-md-4 control-label" for="chosenLifeStage"><spring:message
									code="label.lifestage" /><span class="text-danger">*</span></label>
							<div class="col-md-6">
								<select class="lifestage_chosen" style="width: 200px;"
									id="chosenLifeStage"
									data-placeholder="<spring:message code='label.chooselifestage' />"
									name="chosenLifeStage">
									<option value=""></option>
									<c:forEach items="${lifeStageList}" var="lifeStages">
										<option value="${lifeStages}"
											<c:if test="${lifeStages eq documentMaster.lifeStages}">selected="selected"</c:if>>${lifeStages.name}</option>
									</c:forEach>
								</select>
							</div>
						</div>
				
				
						<div class="form-group" id="doc" >
							<label class="col-md-4 control-label" for="document"><spring:message
									code="label.document" /> <span class="text-danger">*</span> </label>
							<div class="col-md-6">
				
								<input id="document" name="document" class=""
									placeholder="<spring:message code='label.uploaddocument'/>.."
									type="file"><a target="_blank"
									href="<%=contexturl.replace('\\', '/') %>Files${fn:replace(documentMaster.documenturlpath,'\\','/')}"
									>${documentMaster.documentName}</a>
							</div>
						</div>				
					</c:if>
					
					<c:if test="${(documentMaster.contentType eq 'Link') && not(documentMaster.section eq 'BluePrint')}">
						<div class="form-group" id="lname">
							<label class="col-md-4 control-label" for="link_Name"><spring:message
									code="label.linkname" /> <span class="text-danger">*</span> </label>
							<div class="col-md-6">
								<input id="link_Name" name="linkName" class="form-control"
									placeholder="<spring:message code='label.linkname'/>.." type="text"
									value="${documentMaster.link}">
							</div>
						</div>
					</c:if>
				</div>
			</div>
		</div>

		<div style="text-align: center; margin-bottom: 10px">
			<div class="form-group form-actions">
				<!-- 				<div class="col-md-9 col-md-offset-3"> -->
				<button id="company_submit" type="submit"
					class="btn btn-sm btn-primary save">
					<i class="fa fa-angle-right"></i>
					<spring:message code="button.save" />
				</button>
				<a class="btn btn-sm btn-primary save"
					href="<%=contexturl %>ManageDocument/DocumentList"></i> <spring:message
						code="button.cancel" /> </a>
			</div>
		</div>
	</form>
	
</div>
<%@include file="../../inc/page_footer.jsp"%>
<div id="delete_restImg_popup" class="modal fade" tabindex="-1"
		role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<form action="<%=contexturl %>ManageDocument/DocumentList/DeleteBlueprintImage" method="post" class="form-horizontal form-bordered"
						id="delete_restImg_Form">
						<div style="padding: 10px; height: 110px;">
							<label><spring:message code="validation.doyouwanttodeletetheimage"/></label>
							<div class="col-xs-12 text-right">
								<button type="button" class="btn btn-sm btn-default"
									data-dismiss="modal"><spring:message code="label.no"/></button>
								<div id="delete_restImgYes" class="btn btn-sm btn-primary save"><spring:message code="label.yes"/></div>
								<input type="hidden" name="blueprintImgId" id="blueprintImgId">
								<input type="hidden" name="docId" id="docId">
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

<%@include file="../../inc/template_scripts.jsp"%>
<%@include file="../../inc/chosen_scripts.jsp"%>
<script src="<%=contexturl %>resources/js/custom/featurecontrol.js"></script>
<script type="text/javascript"
	src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCheQF_yjKacwuVL4_59EO-3BThy85gbu4&sensor=false&region=GB">
	
</script>

<script type="text/javascript">
	$(document)
			.ready(
					function() {
						$("#manageDocument").addClass("active");
						
						Dropzone.autoDiscover = false;
						var i=0;
						$("div#myId")
								.dropzone(
										{
											acceptedFiles: "image/*",
											url : "<%=contexturl %>ManageDocument/DocumentList/UploadBluePrintImage"
										});

						$(".section_chosen").data("placeholder",
								"Select Section From...").chosen();
						$(".content_chosen").data("placeholder",
								"Select Content From...").chosen();
						$(".category_chosen").chosen();
						$(".lifestage_chosen").data("placeholder",
								"Select LifeStage From...").chosen();
						$(".module_chosen").data("placeholder",
								"Select Module From...").chosen();

						$('select[name="chosenModuleId"]').chosen().change( function() {
							var moduleId = ($(this).val());
							$.ajax({
								type: "GET",
								async: false,
								url: "<%=contexturl %>category/categorybymoduleid/"+moduleId,
								success: function(data, textStatus, jqXHR){
									var obj = jQuery.parseJSON(data);
									var len=obj.length;
									var myOptions = "<option value></option>";
// 									var myCityOptions = "<option value></option>";
									for(var i=0; i<len; i++){
									myOptions += '<option value="'+obj[i].categoryId+'">'+obj[i].categoryName+'</option>';
									}
									$(".category_chosen").html(myOptions).chosen().trigger("chosen:updated");
// 									$(".city_chosen").html(myCityOptions).chosen().trigger("chosen:updated");
								},
								dataType: 'html'
							});
							
						});
						
						
						
						$("#Document_form").validate(
								{	errorClass:"help-block animation-slideDown",
									errorElement:"div",
									errorPlacement:function(e,a){a.parents(".form-group > div").append(e)},
									highlight:function(e){$(e).closest(".form-group").removeClass("has-success has-error").addClass("has-error")},
									success:function(e){e.closest(".form-group").removeClass("has-success has-error")},
									
												rules : {		
													chosenModuleId: {
														required : !0
													},
													chosenCategoryId: {
														required : !0
													},
													chosenSectionId: {
														required : !0
													},
													chosenContentId: {
														required : !0
													},
													title : {
														required : !0,maxlength : 75
													},
													
													documentDescription: {
														required : !0,
														maxlength : 500
													}, 
													<c:if test = "${(empty documentMaster.documentName) && not(documentMaster.section eq 'BluePrint')}">
													document: {
														required : !0,
														documentExtension: "doc|docx|pdf|xlsx|xls|pptx|ppt"
													},
													</c:if>
													<c:if test = "${(empty documentMaster.documentName) && (documentMaster.section eq 'BluePrint')}">
													document: {
														required : !0,
														blueprintExtension : "pptx|ppt"
													},
													</c:if>
													
													<c:if test = "${not(empty documentMaster.documentName) && not(documentMaster.section eq 'BluePrint')}">
													document: {
														documentExtension: "doc|docx|pdf|xlsx|xls|pptx|ppt"
													},
													</c:if>
													<c:if test = "${not (empty documentMaster.documentName) && (documentMaster.section eq 'BluePrint')}">
													document: {
														blueprintExtension : "pptx|ppt"
													},
													</c:if>
													linkName: {
														required : !0,
														url :true,
														maxlength : 500
													},
													chosenLifeStage: {
														required : !0
													},
													
												},
												messages : {
													
													chosenModuleId: {
														required : '<spring:message code="validation.selectmodule"/>'
													},
													chosenCategoryId: {
														required : '<spring:message code="validation.selectcategory"/>'
													},
													chosenSectionId: {
														required : '<spring:message code="validation.selectsection"/>'
													},
													chosenContentId: {
														required : '<spring:message code="validation.selectcontenttype"/>'
													},
													
													title : {
														required :'Please enter a Title.',
															maxlength :'Title should not be more than 75 characters.'
													},
													documentDescription: {
														required : 'Please enter a description',
														maxlength : 'Description should not be more than 500 characters'
													},
													<c:if test = "${(empty documentMaster.documentName) && not(documentMaster.section eq 'BluePrint')}">
													document : {
														required : '<spring:message code="validation.selectadocument"/>',
														documentExtension: 'Please select a document with (.doc, .docx, .pdf, .xlsx, .xls, .ppt, .pptx) extension only'
													},
													</c:if>
													
													<c:if test = "${(empty documentMaster.documentName) && (documentMaster.section eq 'BluePrint')}">
													document : {
														required : '<spring:message code="validation.selectadocument"/>',
														blueprintExtension : 'Please select a document with (.ppt, .pptx) extension only'
													},
													</c:if>
													
													<c:if test = "${not(empty documentMaster.documentName) && not(documentMaster.section eq 'BluePrint')}">
													document : {
														documentExtension: 'Please select a document with (.doc, .docx, .pdf, .xlsx, .xls, .ppt, .pptx) extension only'
													},
													</c:if>
													
													<c:if test = "${not(empty documentMaster.documentName) && (documentMaster.section eq 'BluePrint')}">
													document : {
														blueprintExtension : 'Please select a document with (.ppt, .pptx) extension only'
													},
													</c:if>
													
													linkName : {
														required : '<spring:message code="validation.selectalink"/>',
														url : '<spring:message code="validation.pleaseenteravalidurl"/>',
														maxlength : 'Link should not be more than 500 characters'
													},
													chosenLifeStage: {
														required : 'Please select a LifeStage'
													},
													
												},
										});
														
						jQuery.validator.addMethod("documentExtension", function(value, element, param) {
							param = typeof param === "string" ? param.replace(/,/g, '|') : "doc|docx|pdf|xlsx|xls|pptx|ppt";
							return this.optional(element) || value.match(new RegExp(".(" + param + ")$", "i"));
						}, jQuery.format("Please select images of (.doc, .docx, .pdf, .xlsx, .xls, .ppt, .pptx) extension."));
								
						jQuery.validator.addMethod("blueprintExtension", function(value, element, param) {
							param = typeof param === "string" ? param.replace(/,/g, '|') : "pptx|ppt";
							return this.optional(element) || value.match(new RegExp(".(" + param + ")$", "i"));
						}, jQuery.format("Please select images of (.ppt, .pptx) extension."));

						$("#company_submit").click(function() {
							unsaved=false;
							$("#Document_form").submit();
						});
						
						

						
					});
	

</script>

<script>
function deleteRestImg(id){
	selectedId = id;
}

$("#delete_restImgYes").click(function(){
	var documentid = ${documentMaster.documentId};
	$("#blueprintImgId").val(selectedId);
	$("#docId").val(documentid);
	$("#delete_restImg_Form").submit();
});

function showUploadImgDiv() {
	   document.getElementById('bpImages').style.display = "block";
	   document.getElementById('showImgDiv').style.display = "none";
	}

function showRestImgDiv() {
	   document.getElementById('bpImages').style.display = "none";
	   document.getElementById('showImgDiv').style.display = "block";
	}
</script>

<style>
.chosen-container {
	width: 250px !important;
}

#map-canvas {
	height: 300px
}
</style>
<%@include file="../../inc/template_end.jsp"%>






