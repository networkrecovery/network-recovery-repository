<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->



<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/template_start.jsp"%>
<%@include file="../../inc/page_head.jsp"%>


<div id="page-content">
    <div class="content-header">
        <div class="header-section">
            <c:if test="${!empty error}">
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert"
                        aria-hidden="true">x</button>
                    <h4>
                        <i class="fa fa-times-circle"></i>
                        <spring:message code="label.error" />
                    </h4>
                    <spring:message code="${error}" />
                </div>
            </c:if>
            <c:if test="${!empty invalid}">
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert"
                        aria-hidden="true">x</button>
                    <h4>
                        <i class="fa fa-times-circle"></i>
                        <spring:message code="label.error" />
                    </h4>
                    
                    <c:out value="${invalid}" />
                </div>
            </c:if>

            <h1>
                <i class="gi gi-crown"></i>
                <spring:message code="heading.corporate" />
                <br> <small><spring:message
                        code="heading.corporatedetails" /></small>
            </h1>
            <span id="errorMsg"></span>
        </div>
    </div>
    <ul class="breadcrumb breadcrumb-top">
        <li><spring:message code="menu.corporate" /></li>
    </ul>
    <form action="SaveCorporate" method="post" class="form-horizontal "
        id="Corporate_form" enctype="multipart/form-data">

        <div class="row">

            <div class="col-md-6">
                <div class="block">
                    <div class="block-title">
                        <h2>
                            <strong><spring:message code="heading.corporateinfo" /></strong>
                        </h2>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="corporate_Name"><spring:message
                                code="label.corporatename" /><span class="text-danger">*</span>
                        </label>
                        <div class="col-md-6">

                            <input id="corporate_Name" name="corporateName"
                                class="form-control"
                                placeholder="<spring:message code='label.corporatename'/>.."
                                type="text" value="${corporate.corporateName}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="example-daterange1"><spring:message
                                code="label.selectdaterange" /> </label>
                        <div class="col-md-8">
                            <div class="input-group input-daterange"
                                data-date-format="mm/dd/yyyy">
                                <input type="text" id="compAgreementStart"
                                    name="compAgreementStart" class="form-control text-center"
                                    placeholder="From" value="${corporate.compAgreementStart}">
                                <span class="input-group-addon"><i
                                    class="fa fa-angle-right"></i></span> <input type="text"
                                    id="compAgreementEnd" name="compAgreementEnd"
                                    class="form-control text-center" placeholder="To"
                                    value="${corporate.compAgreementEnd}">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="example-daterange1"><spring:message
                                code="label.selectagreement" /></label>
                        <div class="col-md-6">
                            <input type="file" name="file" accept="application/pdf" size="50" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="domainName"><spring:message
                                code="label.domainName" /><span class="text-danger">*</label>
                        <div class="col-md-9" style="width: 66%;">
                            <textarea id="domainName" name="domainName" class="form-control"
                                rows="3" placeholder="<spring:message code="label.domaineg"/>.."  >${corporate.domainName}</textarea>
                        </div>
                    </div>
                </div>

                <div class="block">
                    <div class="block-title">
                        <h2>
                            <strong><spring:message code="heading.contactinfo" /></strong>
                        </h2>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="telephone1"><spring:message
                                code="label.telephone1" /><span class="text-danger">*</span></label>
                        <div class="col-md-6">

                            <input id="corporateTelephone1" name="corporateTelephone1"
                                class="form-control"
                                placeholder="<spring:message code='label.telephone1'/>.."
                                type="text" value="${corporate.corporateTelephone1}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label" for="telephone2"><spring:message
                                code="label.telephone2" /></label>
                        <div class="col-md-6">

                            <input id="corporateTelephone2" name="corporateTelephone2"
                                class="form-control"
                                placeholder="<spring:message code='label.telephone2'/>.."
                                type="text" value="${corporate.corporateTelephone2}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label" for="rest_email"><spring:message
                                code="label.corporateemail" /><span class="text-danger">*</span></label>
                        <div class="col-md-6">

                            <input id="corporate_email" name="corporateEmail"
                                class="form-control"
                                placeholder="<spring:message code="label.corporateemail"/>.."
                                type="text" value="${corporate.corporateEmail}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label" for="mobile1"><spring:message
                                code="label.mobile1" /><span class="text-danger">*</span></label>
                        <div class="col-md-6">

                            <input id="corporateMobile1" name="corporateMobile1"
                                class="form-control"
                                placeholder="<spring:message code="label.mobile1"/>.."
                                type="text" value="${corporate.corporateMobile1}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label" for="mobile2"><spring:message
                                code="label.mobile2" /></label>
                        <div class="col-md-6">

                            <input id="corporateMobile2" name="corporateMobile2"
                                class="form-control"
                                placeholder="<spring:message code="label.mobile2"/>.."
                                type="text" value="${corporate.corporateMobile2}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label" for="addressline1"><spring:message
                                code="label.addressline1" /><span class="text-danger">*</span>
                        </label>
                        <div class="col-md-6">

                            <input id="corporateAddressline1" name="corporateAddressLine1"
                                class="form-control"
                                placeholder="<spring:message code="label.addressline1"/>.."
                                type="text" value="${corporate.corporateAddressLine1}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label" for="addressline2"><spring:message
                                code="label.addressline2" /></label>
                        <div class="col-md-6">

                            <input id="corporateAddressline2" name="corporateAddressLine2"
                                class="form-control"
                                placeholder="<spring:message code="label.addressline2"/>.."
                                type="text" value="${corporate.corporateAddressLine2}">
                        </div>
                    </div>



                    
                    
                    
                    
                    
                    
                    
                    
                    <div class="form-group" id="countryDiv">
                        <label class="col-md-4 control-label" for="corporateCountryId"><spring:message
                                code="label.countryname" /><span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <select class="country_chosen" style="width: 200px;"
                                id="corporateCountryId"
                                data-placeholder="<spring:message code='label.choosecountry' />"
                                name="chosenCountryId">
                                <option value=""></option>
                                <c:forEach items="${countryList}" var="country">
                                            <option value="${country.countryId}" 
                                            <c:if test="${country.countryId eq corporate.corporateCountry.countryId}">selected="selected"</c:if>>${country.countryName}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                    
                    <div class="form-group" id="cityDiv">
                        <label class="col-md-4 control-label" for="corporateCityId"><spring:message
                                code="label.cityname" /><span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <select class="city_chosen" style="width: 200px;"
                                id="corporateCityId"
                                data-placeholder="<spring:message code='label.choosecity' />"
                                name="chosenCityId">
                                <c:forEach items="${cityList}" var="city">
                                    <option value="${city.cityId}"  <c:if test="${city.cityId eq corporate.corporateCity.cityId}">selected="selected"</c:if> >${city.cityName}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>

                    <div class="form-group" id="areaDiv">
                        <label class="col-md-4 control-label" for="corporateAreaId"><spring:message
                                code="label.areaname" /><span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <select class="area_chosen" style="width: 200px;"
                                id="corporateAreaId"
                                data-placeholder="<spring:message code='label.choosearea' />"
                                name="chosenAreaId">
                                <c:forEach items="${areaList}" var="area">
                                                <option value="${area.areaId}"  <c:if test="${area.areaId eq corporate.corporateArea.areaId}">selected="selected"</c:if> >${area.areaName}</option>
                                    </c:forEach>
                            </select>
                        </div>
                    </div>
                    
                    
                    

                    <div class="form-group">
                        <label class="col-md-4 control-label" for="corporatePincode"><spring:message
                                code="label.pincode" /><span class="text-danger">*</span></label>
                        <div class="col-md-6">

                            <input id="corporatePincode" name="corporatePincode"
                                class="form-control"
                                placeholder="<spring:message code="label.pincode"/>.."
                                type="text" value="${corporate.corporatePincode}">
                        </div>
                    </div>




                </div>




            </div>



            <div class="col-md-6">
                <div class="block">
                    <div class="block-title">
                        <h2>
                            <strong><spring:message code="heading.admininfo" /></strong>
                        </h2>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="first_Name"><spring:message
                                code="label.firstname" /><span class="text-danger">*</span> </label>
                        <div class="col-md-6">

                            <input id="first_Name" name="firstName" class="form-control"
                                placeholder="<spring:message code='label.firstname'/>.."
                                type="text" value="${user.firstName}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label" for="last_Name"><spring:message
                                code="label.lastname" /><span class="text-danger">*</span> </label>
                        <div class="col-md-6">

                            <input id="last_Name" name="lastName" class="form-control"
                                placeholder="<spring:message code="label.lastname"/>.."
                                type="text" value="${user.lastName}">
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-md-4 control-label" for="email"><spring:message
                                code="label.emailid" /><span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <input id="email" name="emailId" class="form-control"
                                placeholder="test@example.com" type="text"
                                value="${user.emailId}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label" for="mobile"><spring:message
                                code="label.mobile" /><span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <input id="mobile" name="mobile" class="form-control"
                                placeholder='<spring:message code="label.mobile"></spring:message>'
                                type="text" value="${user.mobile}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label" for="login"><spring:message
                                code="label.loginid" /><span class="text-danger">*</span> </label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <span class="input-group-addon"><%=propvalue.corporateLoginPrefix%></span>
                                <input id="login" name="loginId" class="form-control"
                                    placeholder='<spring:message code='label.loginid'/>' type="text"
                                    value="${user.loginId}">
                            </div>  
                             <span
                                class="label label-success" id="availability"
                                style="cursor: pointer;"><i class="fa fa-check"></i> <spring:message
                                    code="label.checkavailability" /></span><span class="text-success"
                                id="available" style="display: none"> <spring:message
                                    code="alert.loginavailable" />
                            </span> <span class="text-danger" id="notavailable"
                                style="display: none"> <spring:message
                                    code="alert.loginnotavailable" />
                            </span>
                            <span id="loading" style="display: none"><i class="fa fa-spinner fa-spin"></i>
                           </span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label" for="password"><spring:message
                                code="label.password" /><span class="text-danger">*</span> </label>
                        <div class="col-md-6">
                            <input id="password" name="password" class="form-control"
                                placeholder="Choose a crazy one.." type="password"
                                value="${user.password}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label" for="confirm_password"><spring:message
                                code="label.confirmpassword" /><span class="text-danger">*</span>
                        </label>
                        <div class="col-md-6">
                            <input id="confirm_password" name="confirmPassword"
                                class="form-control" placeholder="..and confirm it!"
                                type="password" value="${user.password}">
                        </div>
                    </div>
                </div>
            </div>








        </div>

        <!--                    <div class="block"> -->
        <!--                        <div style="text-align: center; margin-bottom: 10px"> -->
        <!--                            <div id="corporate_submit" class="btn btn-sm btn-primary"> -->
        <!--                                <i class="fa fa-user"></i> Submit -->
        <!--                            </div> -->
        <!--                            <button type="reset" class="btn btn-sm btn-warning"> -->
        <!--                                <i class="fa fa-repeat"></i> Reset -->
        <!--                            </button> -->
        <!--                            <div class="btn btn-sm btn-primary" id="cancel"> -->
        <!--                                <i class="fa fa-user"></i> Cancel -->
        <!--                            </div> -->
        <!--                        </div> -->
        <!--                    </div> -->



        <div class="form-group form-actions">
            <div class="col-md-9 col-md-offset-3">
                <button id="corporate_submit" type="submit"
                    class="btn btn-sm btn-primary save">
                    <i class="fa fa-angle-right"></i>
                    <spring:message code="button.save" />
                </button>
                <button type="reset" class="btn btn-sm btn-warning">
                    <i class="fa fa-repeat"></i>
                    <spring:message code="button.reset" />
                </button>
                <a href="<%=contexturl %>ManageCorporate/CorporateList/" class="btn btn-sm btn-primary"><i
                    class="gi gi-remove"></i> <spring:message code="button.cancel" />
                </a>
            </div>
        </div>


    </form>
</div>
<%@include file="../../inc/page_footer.jsp"%>
<%@include file="../../inc/template_scripts.jsp"%>
<%@include file="../../inc/chosen_scripts.jsp"%>

<style>
.wizard-steps span {
    width: 230px !important;
}
</style>

<script type="text/javascript">
    $(document)
            .ready(
                    function() {

                        $(".input-datepicker, .input-daterange").datepicker({
                            weekStart : 1
                        });
                        $(".input-timepicker24").timepicker({
                            minuteStep : 1,
                            showSeconds : !0,
                            showMeridian : !1
                        });

                        $(".area_chosen").chosen();
                        $(".city_chosen").chosen();
                        $(".country_chosen").chosen();
                        
                        
                        $('select[name="chosenCountryId"]').chosen().change( function() {
                            var countryId = ($(this).val());
                            $.ajax({
                                type: "GET",
                                async: false,
                                url: "<%=contexturl %>city/citybycountryid/"+countryId,
                                success: function(data, textStatus, jqXHR){
                                    var obj = jQuery.parseJSON(data);
                                    var len=obj.length;
                                    var myOptions = "<option value></option>";
                                    var myAreaOptions = "<option value></option>";
                                    for(var i=0; i<len; i++){
                                    myOptions += '<option value="'+obj[i].cityId+'">'+obj[i].cityName+'</option>';
                                    }
                                    $(".city_chosen").html(myOptions).chosen().trigger("chosen:updated");
                                    $(".area_chosen").html(myAreaOptions).chosen().trigger("chosen:updated");
                                },
                                dataType: 'html'
                            });
                        });
                        
                        $('select[name="chosenCityId"]').chosen().change( function() {
                            var cityId = ($(this).val());
                            $.ajax({
                                type: "GET",
                                async: false,
                                url: "<%=contexturl %>area/areabycityid/"+cityId,
                                success: function(data, textStatus, jqXHR){
                                    var obj = jQuery.parseJSON(data);
                                    var len=obj.length;
                                    var myOptions = "<option value></option>";
                                    for(var i=0; i<len; i++){
                                    myOptions += '<option value="'+obj[i].areaId+'">'+obj[i].areaName+'</option>';
                                    }
                                    $(".area_chosen").html(myOptions).chosen().trigger("chosen:updated");
                                },
                                dataType: 'html'
                            });
                        });
                        
//                      $('#compAgreementStart').datepicker('setStartDate', new Date());
//                      $('#compAgreementEnd').datepicker('setStartDate', new Date());
                        
                        
                         
                        

                        $("#availability").click(
                                function() {
                                    var data = $("#login").val();
                                    data = '<%=propvalue.corporateLoginPrefix%>'+data
                                    $("#notavailable").css("display", "none");
                                    $("#loading").css("display", "inline");
                                    $("#available").css("display", "none");
                                    if (data != "") {
                                        $.getJSON("<%=contexturl%>ValidateLogin", {
                                            loginId : data
                                        }, function(data) {
                                            if (data == "exist") {
                                                $("#notavailable").css(
                                                        "display", "inline");
                                                $("#loading").css("display", "none");
                                            } else if (data == "available") {
                                                $("#available").css("display",
                                                        "inline");
                                                $("#loading").css("display", "none");
                                            }
                                        });
                                    }
                                });

                        $("#Corporate_form")
                                .validate(
                                        {
                                            errorClass : "help-block animation-slideDown",
                                            errorElement : "div",
                                            errorPlacement : function(e, a) {
                                                a.parents(".form-group > div")
                                                        .append(e)
                                            },
                                            highlight : function(e) {
                                                $(e)
                                                        .closest(".form-group")
                                                        .removeClass(
                                                                "has-success has-error")
                                                        .addClass("has-error")
                                            },
                                            success : function(e) {
                                                e
                                                        .closest(".form-group")
                                                        .removeClass(
                                                                "has-success has-error")
                                            },
                                            rules : {
                                                corporateName : {
                                                    required : !0,
                                                    maxlength : 75
                                                },
                                                firstName : {
                                                    required : !0,
                                                    maxlength : 75
                                                },
                                                lastName : {
                                                    required : !0,
                                                    maxlength : 75
                                                },
                                                emailId : {
                                                    required : !0,
                                                    email : !0,
                                                    maxlength : 75
                                                },
                                                mobile : {
                                                    required : !0,
                                                    phone : !0,
                                                    maxlength : 75
                                                },
                                                domainName : {
                                                    domain:!0,
                                                    required : !0
                                                },
                                                loginId : {
                                                    required : !0,
                                                    minlength : 6,
                                                    maxlength : 75
                                                },
                                                password : {
                                                    required : !0,
                                                    minlength : 6,
                                                    maxlength : 75
                                                },
                                                confirmPassword : {
                                                    required : !0,
                                                    equalTo : "#password"
                                                },

                                                corporateTelephone1 : {
                                                    required : !0,
                                                    phone : !0,
                                                    maxlength : 75
                                                },
                                                corporateTelephone2 : {
                                                    phone : !0,
                                                    maxlength : 75
                                                },
                                                corporateEmail : {
                                                    required : !0,
                                                    email : !0,
                                                    maxlength : 75
                                                },
                                                corporateMobile1 : {
                                                    required : !0,
                                                    phone : !0,
                                                    maxlength : 75
                                                },
                                                corporateMobile2 : {
                                                    phone : !0,
                                                    maxlength : 75
                                                },
                                                corporateAddressLine1 : {
                                                    required : !0,
                                                    maxlength : 75
                                                },
                                                corporateAddressLine2 : {
                                                    maxlength : 75
                                                },
                                                corporatePincode : {
                                                    required : !0,
                                                    digits : !0,
                                                    maxlength : 10
                                                },
                                                chosenCityId: {
                                                    required : !0
                                                },
                                                chosenCountryId: {
                                                    required : !0
                                                },
                                                chosenAreaId: {
                                                    required : !0
                                                },

                                            },
                                            messages : {
                                                corporateName : {
                                                    required : '<spring:message code="validation.pleaseentercorporatename"/>',
                                                    maxlength : '<spring:message code="validation.corporatename75character"/>'
                                                },
                                                firstName : {
                                                    required : '<spring:message code="validation.pleaseenterfirstname"/>',
                                                    maxlength : '<spring:message code="validation.firstname75character"/>'
                                                },
                                                lastName : {
                                                    required : '<spring:message code="validation.pleaseenterlastname"/>',
                                                    maxlength : '<spring:message code="validation.lastname75character"/>'
                                                },
                                                emailId : {
                                                    required : '<spring:message code="validation.pleaseenteremailid"/>',
                                                    email : '<spring:message code="validation.pleaseenteravalidemailid"/>',
                                                    maxlength : '<spring:message code="validation.email75character"/>'
                                                },
                                                mobile : {
                                                    required : '<spring:message code="validation.pleaseenteramobilenumber"/>',
                                                    phone : '<spring:message code="validation.phone"/>',
                                                    maxlength : '<spring:message code="validation.mobile75character"/>'
                                                },

                                                domainName : {
                                                    domain :  '<spring:message code="validation.pleaseentervaliddomain"/>',
                                                    required : '<spring:message code="validation.pleaseenterdomain"/>'
                                                },
                                                loginId : {
                                                    required : '<spring:message code="validation.pleaseenteraloginid"/>',
                                                    minlength : '<spring:message code="validation.loginidmustbeatleast6character"/>',
                                                    maxlength : '<spring:message code="validation.loginid75character"/>'
                                                },
                                                password : {
                                                    required : '<spring:message code="validation.pleaseenterapassword"/>',
                                                    minlength : '<spring:message code="validation.passwordmustbeatleast6character"/>',
                                                    maxlength : '<spring:message code="validation.passworddname75character"/>'
                                                },
                                                confirmPassword : {
                                                    required : '<spring:message code="validation.pleaseconfirmpassword"/>',
                                                    equalTo : '<spring:message code="validation.pleaseenterthesamepasswordasabove"/>'
                                                },
                                                corporateTelephone1 : {
                                                    required : '<spring:message code="validation.pleaseenteratelephonenumber"/>',
                                                    phone : '<spring:message code="validation.phone"/>',
                                                    maxlength : '<spring:message code="validation.telephone75character"/>'
                                                },
                                                corporateTelephone2 : {

                                                    phone : '<spring:message code="validation.phone"/>',
                                                    maxlength : '<spring:message code="validation.telephone75character"/>'
                                                },
                                                corporateEmail : {
                                                    required : '<spring:message code="validation.pleaseenteremailid"/>',
                                                    email : '<spring:message code="validation.pleaseenteravalidemailid"/>',
                                                    maxlength : '<spring:message code="validation.email75character"/>'
                                                },
                                                corporateMobile1 : {
                                                    required : '<spring:message code="validation.pleaseenteramobilenumber"/>',
                                                    phone : '<spring:message code="validation.phone"/>',
                                                    maxlength : '<spring:message code="validation.mobile75character"/>'
                                                },
                                                corporateMobile2 : {

                                                    phone : '<spring:message code="validation.phone"/>',
                                                    maxlength : '<spring:message code="validation.mobile75character"/>'
                                                },
                                                corporateAddressLine1 : {
                                                    required : '<spring:message code="validation.pleaseenteraddressline1"/>',
                                                    maxlength : '<spring:message code="validation.corporateaddress"/>'
                                                },
                                                corporateAddressLine2 : {
                                                    maxlength : '<spring:message code="validation.corporateaddress"/>'
                                                },
                                                corporatePincode : {
                                                    required : '<spring:message code="validation.pleaseenterpincode"/>',
                                                    digits : '<spring:message code="validation.digits"/>',
                                                    maxlength : '<spring:message code="validation.pincode75character"/>'
                                                },
                                                chosenCityId: {
                                                    required : '<spring:message code="validation.selectcity"/>'
                                                },
                                                chosenCountryId: {
                                                    required : '<spring:message code="validation.selectcountry"/>'
                                                },
                                                chosenAreaId: {
                                                    required : '<spring:message code="validation.selectarea"/>'
                                                },
                                            },

                                        });

                        $("#corporate_submit").click(function() {
                            $("#Corporate_form").submit();
                        });
                    });
</script>
<style>
.chosen-container {
    width: 250px !important;
}
</style>
<%@include file="../../inc/template_end.jsp"%>