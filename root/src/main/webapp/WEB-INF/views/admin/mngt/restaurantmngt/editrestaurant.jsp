<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<%@page import="com.astrika.common.concurrency.TransactionType"%>
<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/template_start.jsp"%>
<%@include file="../../inc/page_head.jsp"%>

<style>
.chosen-container {
	width: 250px !important;
}

.rest_photo {
    width: 170px;
    text-align: left;
    font: 11px 'Calibri';
    color: #666;
    border: 10px solid #E6E6E6;;
    float: left;
    margin: 5px;
}

#delete{
float: right;
margin-top: -8px;
margin-left: 142px;
padding-left: 4px;
padding-right: 2px;
position: absolute;
z-index: 100000;
}
 #map-canvas { height: 300px}
</style>

<div id="page-content">
	<div class="content-header">
		<div class="header-section">
			<h1>
				<i class="fa fa-map-marker"></i>
				<spring:message code="heading.restaurant" />
				<br> <small><spring:message
						code="heading.restaurantdetails" /></small>
			</h1>
			<span id="errorMsg"></span>
			<c:if test="${!empty success}">
				<div class="alert alert-success alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-check-circle"></i> <spring:message code="label.success"/>
					</h4>
					<spring:message code="${success}" />
				</div>
			</c:if>
			<c:if test="${!empty error}">
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-times-circle"></i> <spring:message code="label.error"/>
					</h4>
					<spring:message code="${error}" />
				</div>
			</c:if>
			
		</div>
	</div>
	<ul class="breadcrumb breadcrumb-top">
		<li><spring:message code="menu.restaurant" /></li>
	</ul>
	<form action="<%=contexturl %>ManageRestaurant/RestaurantList/UpdateRestaurant" method="post" class="form-horizontal"
		id="Restaurant_form" enctype="multipart/form-data">
		
			<div class="row">

				<div class="col-md-6">
					<div class="block">
						<div class="block-title">
							<h2>
								<strong><spring:message code="heading.restaurantinfo" /></strong>
								 <small style="float:left; margin-top: 4px;">User will view this information as it is so please make sure you add exact information</small>
							</h2>
						</div>
						<div class="form-group">
							<input id="outlet_Id" name="outletId" type="hidden" value="${restaurant.outletId}"> 
							<input id="outlet_Id" name="brandId" type="hidden" value="${restaurant.brand.brandId}"> 
							<label class="col-md-4 control-label" for="company_name"><spring:message
									code="label.companyname" /><span class="text-danger">*</span></label>
							<div class="col-md-6">
								<label >${restaurant.brand.company.companyName}</label>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label" for="brand_name"><spring:message
									code="label.brandname" /><span class="text-danger">*</span></label>
							<div class="col-md-6">
								<label >${restaurant.brand.brandName}</label>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label" for="restaurant_Name"><spring:message
									code="label.restaurantname" /><span class="text-danger">*</span>
							</label>
							<div class="col-md-6">

								<input id="restaurant_Name" name="name" class="form-control" value="${restaurant.name}"
									placeholder="<spring:message code='label.restaurantname'/>.."
									type="text">
							</div>
						</div>
						
<!-- 						<div class="form-group"> -->
<%-- 							<label class="col-md-4 control-label" for="restaurant_displayName"><spring:message --%>
<%-- 									code="label.displayname" /> <span class="text-danger">*</span> --%>
<!-- 							</label> -->
<!-- 							<div class="col-md-6"> -->

<%-- 								<input id="restaurant_dispalyName" name="displayName" class="form-control" value="${restaurant.displayName}" --%>
<%-- 									placeholder="<spring:message code='label.displayname'/>.." --%>
<!-- 									type="text"> -->
<!-- 							</div> -->
<!-- 						</div> -->

						<div class="form-group">
							<label class="col-md-4 control-label" for="currency"><spring:message
									code="label.currency" /><span class="text-danger">*</span> </label>
							<div class="col-md-6">
								<select class="currency_chosen" style="width: 200px;" 
									id="currency_name" name="currency" >
									<option value="${restaurant.baseCurrency.currencyId}">${restaurant.baseCurrency.currencyName}</option>
									<c:forEach items="${currencyList}" var="currency">
										<c:choose>
											<c:when test="${currency.active == true}">
												<option value="${currency.currencyId}">${currency.currencyName}</option>
											</c:when>
											<c:otherwise>
												<option value="placeholder">..</option>
											</c:otherwise>
										</c:choose>
									</c:forEach>
								</select>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label" for="company_name"><spring:message
									code="label.cuisine" /><span class="text-danger">*</span></label>
							<div class="col-md-6">
								<select class="cuisine_chosen"
									data-placeholder="<spring:message code='label.choosecuisine' />"
									multiple="" class="chosen" style="width: 200px;" id="cuisine"
									name="cuisine">
									<c:forEach items="${cuisineList}" var="cuisine">
										<c:set var="present" value="false"/>
											<c:forEach items="${cuisines}" var="restCuisine">											
												<c:if test="${cuisine.name eq restCuisine}">
													<c:set var="present" value="true"/>
												</c:if>
												</c:forEach>
											<option value="${cuisine.name}" <c:if test="${present}">	selected="selected" </c:if>> 
											${cuisine.name}</option>
										
									</c:forEach>
								</select>
							</div>
						</div>
						
						<c:choose>
							<c:when test="${restaurant.logoImage!=null}">
								<div class="form-group">
									<label class="col-md-4 control-label" for="example-daterange1"><spring:message code="label.logo"/>
										</label>
									<div class="col-md-6">
										<img src="<%=contexturl %>${restaurant.logoImage.imagePath}" width="150px"/>
									</div>
								</div>
								
								<div class="form-group">
									<label class="col-md-4 control-label" for="example-daterange1"><spring:message code="label.changelogo"/>
										</label>
									<div class="col-md-6">
										<input type="file" name="logo" accept="image/*"/>
									</div>
								</div>
							</c:when>
							<c:otherwise >
								<div class="form-group">
									<label class="col-md-4 control-label" for="example-daterange1"><spring:message code="label.logo"/>
										</label>
									<div class="col-md-6">
										<input type="file" name="logo" accept="image/*"/>
									</div>
								</div>	
							</c:otherwise>
						</c:choose>
						
						
						<c:choose>
							<c:when test="${restaurant.profileImage!=null}">
								<div class="form-group">
									<label class="col-md-4 control-label" for="example-daterange1"><spring:message code="label.profile"/>
										</label>
									<div class="col-md-6">
										<img src="<%=contexturl %>${restaurant.profileImage.imagePath}" width="150px"/>
									</div>
								</div>
								
								<div class="form-group">
									<label class="col-md-4 control-label" for="example-daterange1"><spring:message code="label.changeprofile"/>
										</label>
									<div class="col-md-6">
										<input type="file" name="profile" accept="image/*"/>
									</div>
								</div>
							</c:when>
							<c:otherwise >
								<div class="form-group">
									<label class="col-md-4 control-label" for="example-daterange1"><spring:message code="label.profile"/>
										</label>
									<div class="col-md-6">
										<input type="file" name="profile" accept="image/*"/>
									</div>
								</div>	
							</c:otherwise>
						</c:choose>
						
					</div>

					<div class="block">
						<div class="block-title">
							<h2>
								<strong><spring:message code="heading.contactinfo" /></strong>
								<small style="float:left; margin-top: 4px;"> All communication from gourmet7 will be done on the contact info you provide</small>
							</h2>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label" for="telephone1"><spring:message
									code="label.telephone1" /><span class="text-danger">*</span></label>
							<div class="col-md-6">

								<input id="telephone1" name="telephone1" class="form-control" value="${restaurant.telephone1}"
									placeholder="<spring:message code='label.telephone1'/>.."
									type="text">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label" for="telephone2"><spring:message
									code="label.telephone2" /></label>
							<div class="col-md-6">

								<input id="telephone2" name="telephone2" class="form-control" value="${restaurant.telephone2}"
									placeholder="<spring:message code='label.telephone2'/>.."
									type="text">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label" for="rest_email"><spring:message
									code="label.restaurantemail" /></label>
							<div class="col-md-6">

								<input id="rest_email" name="restaurantEmail"
									class="form-control" value="${restaurant.emailId}"
									placeholder="<spring:message code="label.restaurantemail"/>.."
									type="text">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label" for="mobile1"><spring:message
									code="label.mobile1" /><span class="text-danger">*</span></label>
							<div class="col-md-6">

								<input id="mobile1" name="mobile1" class="form-control" value="${restaurant.mobile1}"
									placeholder="<spring:message code="label.mobile1"/>.."
									type="text">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label" for="mobile2"><spring:message
									code="label.mobile2" /></label>
							<div class="col-md-6">

								<input id="mobile2" name="mobile2" class="form-control" value="${restaurant.mobile2}"
									placeholder="<spring:message code="label.mobile2"/>.."
									type="text">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label" for="addressline1"><spring:message
									code="label.addressline1" /><span class="text-danger">*</span>
							</label>
							<div class="col-md-6">

								<input id="addressline1" name="addressLine1"
									class="form-control" value="${restaurant.addressLine1}"
									placeholder="<spring:message code="label.addressline1"/>.."
									type="text">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label" for="addressline2"><spring:message
									code="label.addressline2" /></label>
							<div class="col-md-6">

								<input id="addressline2" name="addressLine2"
									class="form-control" value="${restaurant.addressLine2}"
									placeholder="<spring:message code="label.addressline2"/>.."
									type="text">
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-md-4 control-label" for="country"><spring:message
									code="label.countryname" /><span class="text-danger">*</span></label>
							<div class="col-md-6">
								<select class="country_chosen" style="width: 200px;"
									id="country"
									data-placeholder="<spring:message code='label.choosecountry' />"
									name="country">
									
									<c:forEach items="${countryList}" var="country">
												<option value="${country.countryId}" <c:if test="${country.countryId eq restaurant.country.countryId}">selected="selected"</c:if> >${country.countryName}</option>
									</c:forEach>
								</select>
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-md-4 control-label" for="city"><spring:message
									code="label.cityname" /><span class="text-danger">*</span></label>
							<div class="col-md-6">
								<select class="city_chosen" style="width: 200px;" id="city"
									data-placeholder="<spring:message code='label.choosecity' />"
									name="city">
									
									<c:forEach items="${cityList}" var="city">
												<option value="${city.cityId}"  id="country_${city.country.countryId}" <c:if test="${city.cityId eq restaurant.city.cityId}">selected="selected"</c:if> >${city.cityName}</option>
									</c:forEach>
								</select>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label" for="area"><spring:message
									code="label.areaname" /><span class="text-danger">*</span></label>
							<div class="col-md-6">
								<select class="area_chosen" style="width: 200px;" id="area"
									data-placeholder="<spring:message code='label.choosearea' />"
									name="area">
									
									<c:forEach items="${areaList}" var="area">
												<option value="${area.areaId}"    id="city_${area.city.cityId}_${area.latitude}_${area.longitude}"  <c:if test="${area.areaId eq restaurant.area.areaId}">selected="selected"</c:if> >${area.areaName}</option>
									</c:forEach>
								</select>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label" for="pincode"><spring:message
									code="label.pincode" /><span class="text-danger">*</span></label>
							<div class="col-md-6">

								<input id="pincode" name="pincode" class="form-control" value="${restaurant.pincode}"
									placeholder="<spring:message code="label.pincode"/>.."
									type="text">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label" for="latitude"><spring:message
									code="label.latitude" /><span class="text-danger">*</span></label>
							<div class="col-md-6">

								<input id="latitude" name="latitude" class="form-control" value="${restaurant.latitude}"
									placeholder="<spring:message code="label.latitude"/>.." readonly="readonly"
									type="text">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label" for="longitude"><spring:message
									code="label.longitude" /><span class="text-danger">*</span></label>
							<div class="col-md-6">

								<input id="longitude" name="longitude" class="form-control" value="${restaurant.longitude}"
									placeholder="<spring:message code="label.longitude"/>.." readonly="readonly"
									type="text">
							</div>
						</div>
					</div>
					<div class="block">
						<div class="block-title">
							<h2><strong>Map</strong></h2>
							Please Drag Pointer to Locate Restaurant
						</div>
      
						  <div id="map-canvas"></div>
					</div>
					<div class="block">
						<div class="block-title">
							<h2>
								<strong><spring:message code="heading.socialinfo" /></strong>
								<small style="float:left; margin-top: 4px;">Please give your social site details here</small>
							</h2>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label" for="facebook_link"><spring:message
									code="label.facebook" /> </label>
							<div class="col-md-6">
								<input id="facebook_link" name="facebookLink"
									class="form-control" value="${restaurant.facebooklink}"
									placeholder="<spring:message code='label.facebook'/>.."
									type="text">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label" for="twitter_link"><spring:message
									code="label.twitter" /> </label>
							<div class="col-md-6">
								<input id="twitter_link" name="twitterLink" class="form-control" value="${restaurant.twitterlink}"
									placeholder="<spring:message code='label.twitter'/>.."
									type="text">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label" for="keyword"><spring:message
									code="label.keyword" /> </label>
							<div class="col-md-6">
								<input id="keyword" name="keyword" class="form-control" value="${restaurant.keyword}"
									placeholder="<spring:message code='label.keyword'/>.."
									type="text">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label" for="metatag_link"><spring:message
									code="label.metatag" /> </label>
							<div class="col-md-6">
								<input id="metatag_link" name="metatag" class="form-control" value="${restaurant.metatag}"
									placeholder="<spring:message code='label.metatag'/>.."
									type="text">
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6">
						<div class="block">
					<div class="block-title">
						<h2>
							<strong><spring:message code="heading.admininfo"/></strong>
							<small style="float:left; margin-top: 4px;">This info will be used for login. Please make sure you add the exact details and make sure it is not misused</small>
						</h2>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label" for="first_Name"><spring:message code="label.firstname"/><span class="text-danger">*</span>
						</label>
						<div class="col-md-6">
								<input id="restAdmin_id" name="restAdminId" type="hidden" value="${restaurant.admin.userId}">
								<input id="first_Name" name="firstName" class="form-control"
									placeholder="<spring:message code='label.firstname'/>.." type="text" value="${restaurant.admin.firstName}"> 
						</div>
					</div>
	
					<div class="form-group">
						<label class="col-md-4 control-label" for="last_Name"><spring:message code="label.lastname"/><span class="text-danger">*</span>
						</label>
						<div class="col-md-6">
							
								<input id="last_Name" name="lastName" class="form-control"
									placeholder="<spring:message code="label.lastname"/>.." type="text"  value="${restaurant.admin.lastName}">
						</div>
					</div>
	
	
					<div class="form-group">
						<label class="col-md-4 control-label" for="email"><spring:message code="label.emailid"/><span
							class="text-danger">*</span></label>
						<div class="col-md-6">
								<input id="email" name="emailId" class="form-control"
									placeholder="test@example.com" type="text"  value="${restaurant.admin.emailId}">
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-md-4 control-label" for="mobile"><spring:message code="label.mobile"/><span
							class="text-danger">*</span></label>
						<div class="col-md-6">
								<input id="mobile" name="mobile" class="form-control"
									placeholder='<spring:message code="label.mobile"></spring:message>' type="text"  value="${restaurant.admin.mobile}">
						</div>
					</div>
					<div class="form-group">
                        <label class="col-md-4 control-label"><spring:message code="label.userlogin"/></label>
                        <div class="col-md-8">
                            <p class="form-control-static">${restaurant.admin.loginId}</p>
                        </div>
                    </div>
					<div class="form-group">
						<a href="#reset-user-settings" data-toggle="modal" class="col-md-4 control-label" ><spring:message code="label.resetpassword"/></a>
					</div>
				</div>
					


					<!-- 								<div> -->
					<!-- 									<div id="myId" action="UploadRestaurantImage" -->
					<!-- 										class="dropzone dz-clickable" enctype="multipart/form-data"> -->
					<!-- 									</div> -->

					<!-- 								</div> -->

					<div class="block full" id="showImgDiv">
						<div class="block-title">
							<div class="block-options pull-right"></div>
							<h2>
								<strong><spring:message code="label.restaurantimages"/>
								</strong>
								<small style="float:left; margin-top: 4px;">please add clear images so that users can get the best picture of your outlet</small>
							</h2>
							<input type="button" name="answer" value="Upload Image" onclick="showUploadImgDiv()" />
						</div>
						<div style="min-height: 350px; height: auto; overflow:hidden;border: 2px dashed #eaedf1; background-color: #f9fafc; padding: 1em;">
							<c:forEach items="${restaurant.restaurantImage}" var="image">
								<div class="rest_photo">
									<span id="delete" >
										<a href="#delete_restImg_popup" data-toggle="modal"
										onclick="deleteRestImg(${image.image.imageId})" title="Delete"
										class="btn btn-xs btn-danger"><i class="fa fa-times"></i>
									</a>
									</span>
									<img border="0" src="<%=contexturl %>${image.image.imagePath}" width="150"
									height="110">
								</div>
							</c:forEach>
						</div>
					</div>
					
					<div class="block full" style="display:none;" id="uploadImgDiv">
						<div class="block-title">
							<div class="block-options pull-right"></div>
							<h2>
								<strong><spring:message code="heading.restaurantimg" />
								</strong>
							</h2>
							<input type="button" name="answer2" value="Show Image" onclick="showRestImgDiv()" />
						</div>
						<div id="myId" class="dropzone dz-clickable"
							enctype="multipart/form-data">
							<div class="dz-default dz-message">
								<span><spring:message code="heading.uploadmsg" /> </span>
							</div>
						</div>
					</div>
					<div class="block">
						<div class="block-title">
							<h2>
								<strong><spring:message code="heading.additionalinfo" /></strong>
								<small style="float:left; margin-top: 4px;">Please fill in additional info to reach your target customers</small>
							</h2>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label" for="timing"><spring:message
									code="label.timing" /> </label>
							<div class="col-md-6">
								<input id="timing" name="timing" class="form-control" value="${restaurant.timings}"
									placeholder="<spring:message code='label.timing'/>.."
									type="text">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label" for="capacity"><spring:message
									code="label.capacity" /> </label>
							<div class="col-md-6">
								<input id="capacity" name="capacity" class="form-control" value="${restaurant.capacity}"
									placeholder="<spring:message code='label.capacity'/>.."
									type="text">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label" for="avgCostofTwo"><spring:message
									code="label.avgcostoftwo" /> </label>
							<div class="col-md-6">
								<input id="avgCostofTwo" name="averageCostOfTwo"
									class="form-control" value="${restaurant.averageCostOfTwo}"
									placeholder="<spring:message code='label.avgcostoftwo'/>.."
									type="text">
							</div>
						</div>

<!-- 						<div class="form-group"> -->
<%-- 							<label class="col-md-4 control-label" for="avgNoOfVisitor"><spring:message --%>
<!-- 									code="label.avgnoofvisitor" /> </label> -->
<!-- 							<div class="col-md-6"> -->
<!-- 								<input id="avgNoOfVisitor" name="averageNoOfVisitor" -->
<%-- 									class="form-control" value="${restaurant.averageNoOfVisitor}" --%>
<%-- 									placeholder="<spring:message code='label.avgnoofvisitor'/>.." --%>
<!-- 									type="text"> -->
<!-- 							</div> -->
<!-- 						</div> -->

						<div class="form-group">
							<label class="col-md-4 control-label" for="bestdishes"><spring:message
									code="label.bestdishes" /> </label>
							<div class="col-md-6">
								<input id="bestdishes" name="bestdishes" class="form-control"
									placeholder="<spring:message code='label.bestdishes'/>.." value="${restaurant.bestdishes}"
									type="text">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label" for="story"><spring:message
									code="label.story" /> </label>
							<div class="col-md-6">
								<textarea id="story" name="story" class="form-control">${restaurant.story}</textarea>
							</div>
						</div>
					</div>
					<div class="block">
						<%@include file="../featurecontrol/featurecontrol.jsp"%>
					</div>
				</div>

			</div>

			<div class="block">
				<div class="block-title">
					<h2>
						<strong><spring:message code="heading.facilities" /></strong>
						<small style="float:left; margin-top: 4px;">Please click on services you have in your outlet, this will help user search your restaurant on the basis of services. 
						It works real time, so if any of your service "like wifi" is temperiorily not working then you can inactive it and activate it 
						again as soon as your wifi is back</small>
					</h2>
				</div>
				<div class="form-group">
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="ac"><spring:message
								code="label.ac" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="ac"
								name="ac" type="checkbox" <c:if test="${restaurant.ac}"> checked </c:if> /> <span></span></label>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="creditCard"><spring:message
								code="label.acceptscreditcard" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input <c:if test="${restaurant.acceptsCreditCard}"> checked </c:if>
								id="creditCard" name="acceptsCreditCard" type="checkbox" /> <span></span></label>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="wifi"><spring:message
								code="label.wifi" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="wifi" <c:if test="${restaurant.wifi}"> checked </c:if>
								name="wifi" type="checkbox" /> <span></span></label>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="gourmet7_Reach"><spring:message
								code="label.homedelivery" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input <c:if test="${restaurant.homeDelivery}"> checked </c:if>
								id="homedelivery" name="homeDelivery" type="checkbox" /> <span></span></label>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="nonvage"><spring:message
								code="label.nonvage" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="nonvage" <c:if test="${restaurant.nonvegeterian}"> checked </c:if>
								name="nonvegeterian" type="checkbox" /> <span></span></label>
						</div>
					</div>

					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="parking"><spring:message
								code="label.parking" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="parking" <c:if test="${restaurant.parking}"> checked </c:if>
								name="parking" type="checkbox" /> <span></span></label>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="toilet"><spring:message
								code="label.toilet" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="toilet" <c:if test="${restaurant.toilet}"> checked </c:if>
								name="toilet" type="checkbox" /> <span></span></label>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="servesalcohol"><spring:message
								code="label.servesalcohol" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input <c:if test="${restaurant.servesAlcohol}"> checked </c:if>
								id="servesalcohol" name="servesAlcohol" type="checkbox" /> <span></span></label>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="smoking"><spring:message
								code="label.smoking" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="smoking" <c:if test="${restaurant.smokingaAllowed}"> checked </c:if>
								name="smokingaAllowed" type="checkbox" /> <span></span></label>
						</div>
					</div>

					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="music"><spring:message
								code="label.music" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="music" <c:if test="${restaurant.music}"> checked </c:if>
								name="music" type="checkbox" /> <span></span></label>
						</div>
					</div>

					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="happyHours"><spring:message
								code="label.happyhours" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input <c:if test="${restaurant.happyHours}"> checked </c:if>
								id="happyHours" name="happyHours" type="checkbox" /> <span></span></label>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="takeaway"><spring:message
								code="label.takeaway" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input <c:if test="${restaurant.takeAway}"> checked </c:if>
								id="takeaway" name="takeAway" type="checkbox" /> <span></span></label>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="snacks"><spring:message
								code="label.snacks" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="snacks" <c:if test="${restaurant.snacks}"> checked </c:if>
								name="snacks" type="checkbox" /> <span></span></label>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="dinein"><spring:message
								code="label.dinein" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="dinein" <c:if test="${restaurant.dinein}"> checked </c:if>
								name="dinein" type="checkbox" /> <span></span></label>
						</div>
					</div>
					
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="outdoorSeating"><spring:message
								code="label.outdoorSeating" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="outdoorSeating"
								<c:if test="${restaurant.outdoorSeating}">checked</c:if> name="outdoorSeating"
								type="checkbox" /> <span></span></label>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="tableReservation"><spring:message
								code="label.tableReservation" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="tableReservation"
								<c:if test="${restaurant.tableReservation}">checked</c:if> name="tableReservation"
								type="checkbox" /> <span></span></label>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="barArea"><spring:message
								code="label.barArea" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="barArea"
								<c:if test="${restaurant.barArea}">checked</c:if> name="barArea"
								type="checkbox" /> <span></span></label>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="corporateEvents"><spring:message
								code="label.corporateEvents" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="corporateEvents"
								<c:if test="${restaurant.corporateEvents}">checked</c:if> name="corporateEvents"
								type="checkbox" /> <span></span></label>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="liveSportsBroadcast"><spring:message
								code="label.liveSportsBroadcast" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="liveSportsBroadcast"
								<c:if test="${restaurant.liveSportsBroadcast}">checked</c:if> name="liveSportsBroadcast"
								type="checkbox" /> <span></span></label>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="dressCode"><spring:message
								code="label.dressCode" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="dressCode"
								<c:if test="${restaurant.dressCode}">checked</c:if> name="dressCode"
								type="checkbox" /> <span></span></label>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="open24_7"><spring:message
								code="label.open24_7" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="open24_7"
								<c:if test="${restaurant.open24_7}">checked</c:if> name="open24_7"
								type="checkbox" /> <span></span></label>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="privatePartyArea"><spring:message
								code="label.privatePartyArea" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="privatePartyArea"
								<c:if test="${restaurant.privatePartyArea}">checked</c:if> name="privatePartyArea"
								type="checkbox" /> <span></span></label>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="sundayBrunch"><spring:message
								code="label.sundayBrunch" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="sundayBrunch"
								<c:if test="${restaurant.sundayBrunch}">checked</c:if> name="sundayBrunch"
								type="checkbox" /> <span></span></label>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="selfServices"><spring:message
								code="label.selfServices" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="selfServices"
								<c:if test="${restaurant.selfServices}">checked</c:if> name="selfServices"
								type="checkbox" /> <span></span></label>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="petFriendly"><spring:message
								code="label.petFriendly" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="petFriendly"
								<c:if test="${restaurant.petFriendly}">checked</c:if> name="petFriendly"
								type="checkbox" /> <span></span></label>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="jainFood"><spring:message
								code="label.jainFood" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="jainFood"
								<c:if test="${restaurant.jainFood}">checked</c:if> name="jainFood"
								type="checkbox" /> <span></span></label>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="buffetLunch"><spring:message
								code="label.buffetLunch" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="buffetLunch"
								<c:if test="${restaurant.buffetLunch}">checked</c:if> name="buffetLunch"
								type="checkbox" /> <span></span></label>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="buffetDinner"><spring:message
								code="label.buffetDinner" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="buffetDinner"
								<c:if test="${restaurant.buffetDinner}">checked</c:if> name="buffetDinner"
								type="checkbox" /> <span></span></label>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="banquet"><spring:message
								code="label.banquet" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="banquet"
								<c:if test="${restaurant.banquet}">checked</c:if> name="banquet"
								type="checkbox" /> <span></span></label>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="midnightBuffet"><spring:message
								code="label.midnightBuffet" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="midnightBuffet"
								<c:if test="${restaurant.midnightBuffet}">checked</c:if> name="midnightBuffet"
								type="checkbox" /> <span></span></label>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="partyOrders"><spring:message
								code="label.partyOrders" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="partyOrders"
								<c:if test="${restaurant.partyOrders}">checked</c:if> name="partyOrders"
								type="checkbox" /> <span></span></label>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="disablFacility"><spring:message
								code="label.disablFacility" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="disablFacility"
								<c:if test="${restaurant.disablFacility}">checked</c:if> name="disablFacility"
								type="checkbox" /> <span></span></label>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="theme"><spring:message
								code="label.theme" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="theme"
								<c:if test="${restaurant.theme}">checked</c:if> name="theme"
								type="checkbox" /> <span></span></label>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="inMall"><spring:message
								code="label.inMall" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="inMall"
								<c:if test="${restaurant.inMall}">checked</c:if> name="inMall"
								type="checkbox" /> <span></span></label>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="englishSpeakingWaiters"><spring:message
								code="label.englishSpeakingWaiters" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="englishSpeakingWaiters"
								<c:if test="${restaurant.englishSpeakingWaiters}">checked</c:if> name="englishSpeakingWaiters"
								type="checkbox" /> <span></span></label>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="couponsAccepted"><spring:message
								code="label.couponsAccepted" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="couponsAccepted"
								<c:if test="${restaurant.couponsAccepted}">checked</c:if> name="couponsAccepted"
								type="checkbox" /> <span></span></label>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="liveFoodCounter"><spring:message
								code="label.liveFoodCounter" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="liveFoodCounter"
								<c:if test="${restaurant.liveFoodCounter}">checked</c:if> name="liveFoodCounter"
								type="checkbox" /> <span></span></label>
						</div>
					</div>
				</div>
					
			</div>
	

		<div class="form-group form-actions">
			<div class="col-md-8 col-md-offset-4">
					<button id="offer_submit" type="submit"
						class="btn btn-sm btn-primary save">
						<i class="fa fa-angle-right"></i> <spring:message code="button.save" />
					</button>
					 <a class="btn btn-sm btn-primary" href="<%=contexturl %>ManageRestaurant/RestaurantList?id=${restaurant.outletId}"> <spring:message code="button.cancel" /> </a>
			</div>
		</div>
	</form>
</div>
<div id="reset-user-settings" class="modal fade" tabindex="-1"
	role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header text-center">
				<h2 class="modal-title">
					<i class="fa fa-pencil"></i> <spring:message code="label.resetpassword"/>
				</h2>
			</div>
			<span id="passwordErrorMsg"></span>
			<div class="modal-body">
				<form action="<%=contexturl %>ManageRestaurant/RestaurantList/ResetRestaurantAdminPassword" method="post" class="form-horizontal form-bordered"  id="ResetPassword_Form">
					<input  name="outletId"  value="${restaurant.outletId}" type="hidden"> 
					<input  name="userId"  value="${restaurant.admin.userId}" type="hidden"> 
					<fieldset>
						<div class="form-group">
							<label class="col-md-4 control-label"
								for="user-settings-password"><spring:message code="label.newpassword"/></label>
							<div class="col-md-8">
								<input type="password" id="password"
									name="password" class="form-control"
									placeholder="Please choose a complex one..">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label"
								for="user-settings-repassword"><spring:message code="label.confirmnewpassword"/></label>
							<div class="col-md-8">
								<input type="password" id="user-settings-repassword"
									name="confirmPassword" class="form-control"
									placeholder="..and confirm it!">
							</div>
						</div>
					</fieldset>
					<div class="form-group form-actions">
						<div class="col-xs-12 text-right">
							<button type="button" class="btn btn-sm btn-default"
								data-dismiss="modal"><spring:message code="label.close"/></button>
							<button id="submit_password" class="btn btn-sm btn-primary save" type="submit"><spring:message code="label.savechanges"/>
								</button>
						  
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<div id="delete_restImg_popup" class="modal fade" tabindex="-1"
		role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<form action="<%=contexturl %>ManageRestaurant/RestaurantList/DeleteRestImage" method="post" class="form-horizontal form-bordered"
						id="delete_restImg_Form">
						<div style="padding: 10px; height: 110px;">
							<label><spring:message code="validation.doyouwanttodeletetheimage"/></label>
							<div class="col-xs-12 text-right">
								<button type="button" class="btn btn-sm btn-default"
									data-dismiss="modal"><spring:message code="label.no"/></button>
								<div id="delete_restImgYes" class="btn btn-sm btn-primary save"><spring:message code="label.yes"/></div>
								<input type="hidden" name="restImgId" id="restImgId">
								<input type="hidden" name="outletId" id="outletId">
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
<div class="form-group">
			<a href="#concurrency_popup" data-toggle="modal" class="col-md-4 control-label" id="councurrencyLink"></a>
</div>
<div id="concurrency_popup" class="modal fade" tabindex="-1"
	role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<form action="" method="post" class="form-horizontal form-bordered"
					id="fg1">
					<div style="padding: 10px; height: 110px;">
						<label id="error_Msg"></label>
						<div class="col-xs-12 text-right">
							<a class="btn btn-sm btn-default"
								href="<%=contexturl %>ManageRestaurant/RestaurantList"><spring:message code="label.no"/></a>
							<a class="btn btn-sm btn-primary save" href="#" id="extendSession">
							<spring:message code="label.yes"/> </a>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<%@include file="../../inc/page_footer.jsp"%>
<%@include file="../../inc/template_scripts.jsp"%>


 <script type="text/javascript"
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCheQF_yjKacwuVL4_59EO-3BThy85gbu4&sensor=false&region=GB">
    </script>
    <script type="text/javascript">
      function initialize() {
        var mapOptions = {
          center: new google.maps.LatLng($('#latitude').val(), $('#longitude').val()),
          zoom: 13
        };
        var map = new google.maps.Map(document.getElementById("map-canvas"),
            mapOptions);
     
		var marker = new google.maps.Marker({
		position: map.getCenter(),
		map: map,
		title: 'Click to zoom',
		draggable: true
	  });

	  google.maps.event.addListener(map, 'center_changed', function() {
		// 3 seconds after the center of the map has changed, pan back to the
		// marker.
	
		window.setTimeout(function() {
				$('#latitude').val(marker.getPosition().k);
				$('#longitude').val(marker.getPosition().B);
				map.panTo(marker.getPosition());
		}, 1000);
	  });

	  google.maps.event.addListener(marker, 'click', function() {
		map.setZoom(13);
		map.setCenter(marker.getPosition());
	  });
	}

      google.maps.event.addDomListener(window, 'load', initialize);
    </script>
    <script src="<%=contexturl %>resources/js/custom/featurecontrol.js"></script>
<script type="text/javascript">
	$(document)
			.ready(
					function() {
						manageConcurrency();
						Dropzone.autoDiscover = false;
						$("div#myId")
								.dropzone(
										{
											acceptedFiles: "image/*",
											url : "<%=contexturl %>ManageRestaurant/RestaurantList/UploadRestaurantImage"
										});
						$(".chosen").chosen();
						$(".brand_chosen").chosen();
						$(".currency_chosen").chosen();
						$(".area_chosen").chosen();
						$(".city_chosen").chosen();
						$(".country_chosen").chosen();
						$(".cuisine_chosen").chosen();
						
						$('select[name="country"]').chosen().change( function() {
							countryId = ($(this).val());
							$.ajax({
								type: "GET",
								async: false,
								url: "<%=contexturl %>city/citybycountryid/"+countryId,
								success: function(data, textStatus, jqXHR){
									var obj = jQuery.parseJSON(data);
									var len=obj.length;
									var myOptions = "<option value>Select City</option>";
									var myAreaOptions = "<option value>Select Area</option>";
									for(var i=0; i<len; i++){
									myOptions += '<option value="'+obj[i].cityId+'">'+obj[i].cityName+'</option>';
									}
									$(".city_chosen").html(myOptions).chosen().trigger("chosen:updated");
									$(".area_chosen").html(myAreaOptions).chosen().trigger("chosen:updated");
								},
								dataType: 'html'
							});
						});
						
						$('select[name="city"]').chosen().change( function() {
							cityId = ($(this).val());
							$.ajax({
								type: "GET",
								async: false,
								url: "<%=contexturl %>area/areabycityid/"+cityId,
								success: function(data, textStatus, jqXHR){
									var obj = jQuery.parseJSON(data);
									var len=obj.length;
									var myOptions = "<option value>Select Area</option>";
									for(var i=0; i<len; i++){
									myOptions += '<option value="'+obj[i].areaId+'">'+obj[i].areaName+'</option>';
									}
									$(".area_chosen").html(myOptions).chosen().trigger("chosen:updated");
								},
								dataType: 'html'
							});
						});
						
						changeCountry($("#country").val());
						
						function changeCountry(countryId){
							$.ajax({
								type: "GET",
								async: false,
								url: "<%=contexturl %>city/citybycountryid/"+countryId,
								success: function(data, textStatus, jqXHR){
									var obj = jQuery.parseJSON(data);
									var len=obj.length;
									var myOptions = "<option value>Select City</option>";
									for(var i=0; i<len; i++){
										if(${restaurant.city.cityId} != obj[i].cityId){
											myOptions += '<option value="'+obj[i].cityId+'">'+obj[i].cityName+'</option>';
										}
										else{
											myOptions += '<option value="'+obj[i].cityId+'" selected="selected">'+obj[i].cityName+'</option>';
										}
									}
									$(".city_chosen").html(myOptions).chosen().trigger("chosen:updated");
									changeCity($("#city").val());
								},
								dataType: 'html'
							});
						}
						
						function changeCity(cityId){
							$.ajax({
								type: "GET",
								async: false,
								url: "<%=contexturl %>area/areabycityid/"+cityId,
								success: function(data, textStatus, jqXHR){
									var obj = jQuery.parseJSON(data);
									var len=obj.length;
									var myOptions = "<option value>Select Area</option>";
									for(var i=0; i<len; i++){
										if(${restaurant.area.areaId} != obj[i].areaId){
											myOptions += '<option value="'+obj[i].areaId+'">'+obj[i].areaName+'</option>';
										}
										else{
											myOptions += '<option value="'+obj[i].areaId+'" selected="selected">'+obj[i].areaName+'</option>';
										}
									}
									$(".area_chosen").html(myOptions).chosen().trigger("chosen:updated");
								},
								dataType: 'html'
							});
						}
						
						
						$("#ResetPassword_Form").validate(
								{
									errorClass : "help-block animation-slideDown",
									errorElement : "div",
									errorPlacement : function(e, a) {
										a.parents(".form-group > div")
												.append(e)
									},
									highlight : function(e) {
										$(e)
												.closest(".form-group")
												.removeClass(
														"has-success has-error")
												.addClass("has-error")
									},
									success : function(e) {
										e
												.closest(".form-group")
												.removeClass(
														"has-success has-error")
									},
									rules : {
										password : {
											required : !0,
											minlength : 6
										},
										confirmPassword : {
											required : !0,
											equalTo : "#password"
										}
									},
									messages : {
										password : {
											required : '<spring:message code="validation.pleaseenterapassword"/>',
											minlength : '<spring:message code="validation.passwordmustbeatleast6character"/>'
										},
										confirmPassword : {
											required : '<spring:message code="validation.pleaseconfirmpassword"/>',
											equalTo : '<spring:message code="validation.pleaseenterthesamepasswordasabove"/>'
										},
									}
								});
						
						$("#Restaurant_form")
								.validate(
										{
											
												errorClass : "help-block animation-slideDown",
												errorElement : "div",
												errorPlacement : function(e, a) {
													a
															.parents(
																	".form-group > div")
															.append(e)
												},
												highlight : function(e) {
													$(e)
															.closest(
																	".form-group")
															.removeClass(
																	"has-success has-error")
															.addClass(
																	"has-error")
												},
												success : function(e) {
													e
															.closest(
																	".form-group")
															.removeClass(
																	"has-success has-error")
												},
												rules : {
													name : {
														required : !0,
														maxlength : 75
													},
													displayName : {
														required : !0
													},
													company : {
														required : !0
													},
													cuisine : {
														required : !0
													},
													pincode: {
														required : !0,maxlength : 10,
														digits : !0
													},
													latitude : {
														required : !0,maxlength : 75,
														number : !0
													},
													longitude : {
														required : !0,maxlength : 75,
														number : !0
													},
													firstName : {
														required : !0,maxlength : 75,
													},
													lastName : {
														required : !0,maxlength : 75,
													},
													addressLine1: {
														required : !0,maxlength : 75,
													},
													addressLine2: {
														maxlength : 75,
													},
													emailId : {
														required : !0,maxlength : 75,
														email : !0
													},
													mobile : {
														required : !0,
														phone : !0
													},
													loginId : {
														required : !0,maxlength : 75,
														minlength : 6
													},
													facebookLink:{
														urlTrim : !0,
														maxlength : 255
													},
													twitterLink:{
														urlTrim : !0,
														maxlength : 255
													},
													keyword:{
														maxlength : 75
													},
													metatag:{
														maxlength : 75
													},
													city: {
														required : !0
													},
													country: {
														required : !0
													},
													area: {
														required : !0
													},
													timing : {
														maxlength : 75
													},
													capacity : {
														maxlength : 75
													},
													averageCostOfTwo : {
														maxlength : 75
													},
													averageNoOfVisitor : {
														maxlength : 75
													},
													bestdishes : {
														maxlength : 75
													},
													story : {
														maxlength : 1000
													},
													telephone1 : {
														required : !0,maxlength : 75,
														phone : !0
													},
													telephone2 : {
														phone : !0
													},
													restaurantEmail : {
														maxlength : 75,
														email : !0
													},
													mobile1 : {
														required : !0,maxlength : 75,
														phone : !0
													},
													mobile2 : {
														phone : !0
													},
												},
												messages : {
													name : {
														required :  '<spring:message code="validation.pleaseenterrestaurantname"/>',
														maxlength : '<spring:message code="validation.name75character"/>'
													},
													displayName : {
														required :  '<spring:message code="validation.displayname"/>'
													},
													company : {
														required : '<spring:message code="validation.pleaseselectacompany"/>'
													},
													cuisine : {
														required: '<spring:message code="validation.pleaseselectcuisine"/>'
													},
													pincode: {
														required : '<spring:message code="validation.pleaseenterpincode"/>',
														maxlength :'<spring:message code="validation.pincode75character"/>',
														digits : '<spring:message code="validation.digits"/>'
													},
													latitude : {
														required : '<spring:message code="validation.latitude"/>',
														maxlength :'<spring:message code="validation.latitude75character"/>',
														number : '<spring:message code="validation.digits"/>'
													},
													longitude : {
														required : '<spring:message code="validation.longitude"/>',
														maxlength :'<spring:message code="validation.longitude75character"/>',
														number : '<spring:message code="validation.digits"/>'
													},
													firstName : {
														required : '<spring:message code="validation.pleaseenterfirstname"/>',
														maxlength :'<spring:message code="validation.firstname75character"/>'
													},
													lastName : {
														required : '<spring:message code="validation.pleaseenterlastname"/>',
														maxlength :'<spring:message code="validation.lastname75character"/>'
													},
													addressLine1: {
														required : '<spring:message code="validation.pleaseenteraddressline1"/>',
														maxlength :'<spring:message code="validation.address"/>'
													},
													addressLine2: {
														maxlength :'<spring:message code="validation.address"/>'
													},
													emailId : {
														required : '<spring:message code="validation.pleaseenteremailid"/>',
														maxlength : '<spring:message code="validation.email75character"/>',
														email :'<spring:message code="validation.pleaseenteravalidemailid"/>'
													},
													mobile : {
														required : '<spring:message code="validation.pleaseenteramobilenumber"/>',
														phone :'<spring:message code="validation.phone"/>'
													},
													loginId : {
														required:'<spring:message code="validation.pleaseenteraloginid"/>',
														minlength:'<spring:message code="validation.loginidmustbeatleast6character"/>',
														maxlength :'<spring:message code="validation.loginid75character"/>'
													},
													facebookLink:{
														urlTrim : '<spring:message code="validation.pleaseentervalidurl"/>',
														maxlength : '<spring:message code="validation.link255character"/>'
													},
													twitterLink:{
														urlTrim : '<spring:message code="validation.pleaseentervalidurl"/>',
														maxlength : '<spring:message code="validation.link255character"/>'
													},
													
													keyword:{
														maxlength : '<spring:message code="validation.keyword75character"/>'
													},
													metatag:{
														maxlength : '<spring:message code="validation.metatag75character"/>'
													},
													city: {
														required : '<spring:message code="validation.selectcity"/>'
													},
													country: {
														required : '<spring:message code="validation.selectcountry"/>'
													},
													area: {
														required : '<spring:message code="validation.selectarea"/>'
													},
													timing : {
														maxlength : '<spring:message code="validation.timing75character"/>'
													},
													capacity : {
														maxlength : '<spring:message code="validation.capacity75character"/>'
													},
													averageCostOfTwo : {
														maxlength : '<spring:message code="validation.averagecostoftwo"/>'
													},
													averageNoOfVisitor : {
														maxlength : '<spring:message code="validation.averagenoofvisitor"/>'
													},
													bestdishes : {
														maxlength : '<spring:message code="validation.bestdishes"/>'
													},
													story : {
														maxlength : '<spring:message code="validation.story"/>'
													},
													telephone1 : {
														required :'<spring:message code="validation.pleaseenteratelephonenumber"/>',
														phone :'<spring:message code="validation.phone"/>',
															maxlength :'<spring:message code="validation.telephone75character"/>'
													},
													telephone2 : {
														phone : '<spring:message code="validation.phone"/>'
													},
													restaurantEmail : {
														maxlength : '<spring:message code="validation.email75character"/>',
														email : '<spring:message code="validation.pleaseenteravalidemailid"/>'
													},
													mobile1 : {
														required :'<spring:message code="validation.pleaseenteramobilenumber"/>',
														phone :'<spring:message code="validation.phone"/>',
														maxlength :'<spring:message code="validation.mobile75character"/>'
													},
													mobile2 : {
														phone : '<spring:message code="validation.phone"/>'
													}
												}
											
										});


						// 		$("#restaurant_submit").click(function(){
						// 			 $("#Restaurant_form").submit();
						// 		});

						$("#availability").click(
								function() {
									var data = $("#login").val();
									$("#notavailable").css("display", "none")
									$("#available").css("display", "none")
									if (data != "") {
										$.getJSON("<%=contexturl%>ValidateLogin", {
											loginId : data
										}, function(data) {
											if (data == "exist") {
												$("#notavailable").css(
														"display", "inline")
											} else if (data == "available") {
												$("#available").css("display",
														"inline")
											}
										});
									}
								});

				$("#delete_restImgYes").click(function(){
					$("#restImgId").val(selectedId);
					$("#outletId").val(${restaurant.outletId});
					$("#delete_restImg_Form").submit();
				});
				
			});
	
	function showUploadImgDiv() {
		   document.getElementById('uploadImgDiv').style.display = "block";
		   document.getElementById('showImgDiv').style.display = "none";
		}
	
	function showRestImgDiv() {
		   document.getElementById('uploadImgDiv').style.display = "none";
		   document.getElementById('showImgDiv').style.display = "block";
		}
	
	function deleteRestImg(id){
		selectedId = id;
	}
	

function manageConcurrency(){
		setTimeout(function(){
		
			$.ajax({
				type: "GET",
				async: false,
				url: "<%=contexturl%>TimeOutWarning",				
				success: function(data, textStatus, jqXHR){
					$("#councurrencyLink").trigger("click");
					$("#error_Msg").html($.parseJSON(data).Response);
					$("#extendSession").click(function(){
						$('body').removeClass('modal-open');
	 					$('.modal-backdrop').remove();
	 					$('#concurrency_popup').modal('hide');	
						$.ajax({
							type: "GET",
							async: false,
							url: "<%=contexturl%>ExtendSession?transactionId=${restaurant.outletId}&transactionType=<%=TransactionType.RESTAURANT%>",				
							success: function(data, textStatus, jqXHR){
								var html = '<div class="alert alert-warning alert-dismissable">	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="fa fa-times-circle"></i> Warning</h4> <span >'+$.parseJSON(data).Response+'</span></div>';
								$("#errorMsg").html(html).show();
								$("#errorMsg").html(html).fadeOut(20000);
								$('html, body').animate({ scrollTop: $("#errorMsg").offset().top },500);
								manageConcurrency();
							},
							dataType: 'html'
						});
					});
				},
				dataType: 'html'
			});
		},<%= propvalue.concurrencySessionWarning%>);
		
	}
	
	
</script>
<%@include file="../../inc/template_end.jsp"%>