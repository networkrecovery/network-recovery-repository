<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/template_start.jsp"%>
<%@include file="../../inc/page_head.jsp"%>
  <link rel="stylesheet" href="<%=contexturl %>resources/css/imgareaselect-default.css">
<%--   <link rel="stylesheet" href="<%=contexturl %>resources/css/index.css"> --%>
<style>
.memberType {
	float: left;
	margin-right: 40px;
	margin-top: 10px;
	margin-left: 50px;
}

.memberRange {
	float: left;
	margin-right: 10px;
	margin-left: 50px;
}

.wizard-steps span {
	width: 230px !important;
}

.chosen-container {
	width: 250px !important;
}

#map-canvas {
	height: 300px
}
</style>

<div id="page-content">
	<div class="content-header">
		<div class="header-section">
			<c:if test="${!empty error}">
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-times-circle"></i>
						<spring:message code="label.error" />
					</h4>
					<spring:message code="${error}" />
				</div>
			</c:if>

			<h1>
				<i class="fa fa-map-marker"></i>
				<spring:message code="heading.restaurant" />
				<br> <small><spring:message
						code="heading.createrestaurant" /></small>
			</h1>
			<span id="errorMsg"></span>
		</div>
	</div>
	<ul class="breadcrumb breadcrumb-top">
		<li><spring:message code="menu.restaurant" /></li>
	</ul>
	<form action="<%=contexturl %>ManageRestaurant/RestaurantList/SaveRestaurant" method="post"
		class="form-horizontal ui-formwizard" id="Restaurant_form"
		enctype="multipart/form-data">
		<div style="display: block;" id="advanced-first"
			class="step ui-formwizard-content">
			<div class="wizard-steps">
				<div class="row">
					<div class="col-xs-4 active">
						<span><spring:message code="heading.restaurantdetail" /></span>
					</div>
					<div class="col-xs-4">
						<span><spring:message code="heading.discountdetails" /></span>
					</div>
					<div class="col-xs-4">
						<span><spring:message code="heading.termsandconditions" /></span>
					</div>
				</div>
			</div>
			<div class="row">

				<div class="col-md-6">
					<div class="block">
						<div class="block-title">
							<h2>
								<strong><spring:message code="heading.restaurantinfo" /></strong>
								<small style="float:left; margin-top: 4px;">User will view this information as it is so please make sure you add exact information</small>
							</h2>
						</div>
						<security:authorize
							ifAnyGranted="<%=Role.GORMET7_ADMIN.name()%>">
							<div class="form-group">
								<label class="col-md-4 control-label" for="company_name"><spring:message
										code="label.companyname"/><span class="text-danger">*</span></label>
								<div class="col-md-6">
									<select 
										data-placeholder="<spring:message code='label.choosecompany' />"
										class="chosen" style="width: 200px;" id="company_name"
										name="company">
										<option value=""></option>
										<c:forEach items="${companyList}" var="company">
											<option value="${company.companyId}" <c:if test="${company.companyId eq restaurant.brand.company.companyId}">selected="selected"</c:if> >${company.companyName}</option>
										</c:forEach>
										
									</select>
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-4 control-label" for="brand_name"><spring:message
										code="label.brandname" /><span class="text-danger">*</span></label>
								<div class="col-md-6">
									<select class="brand_chosen" style="width: 200px;"
										data-placeholder="<spring:message code='label.choosebrand' />"
										id="brand_name" name="brandId">
										<option value=""></option>
										<c:forEach items="${brandList}" var="brand">
												<option value="${brand.brandId}"  <c:if test="${brand.brandId eq restaurant.brand.brandId}">selected="selected"</c:if> >${brand.brandName}</option>
										</c:forEach>
									</select>
								</div>
							</div>
						</security:authorize>
						<security:authorize
							ifAnyGranted="<%=Role.COMPANY_ADMIN.name()%>">
							<div class="form-group">
								<label class="col-md-4 control-label" for="company_name"><spring:message
										code="label.companyname"/><span class="text-danger">*</span></label>
								<div class="col-md-6">
									<select 
										data-placeholder="<spring:message code='label.choosecompany' />"
										class="chosen" style="width: 200px;" id="company_name"
										name="company">
										<c:forEach items="${companyList}" var="company">
											<option value="${company.companyId}" <c:if test="${company.companyId eq restaurant.brand.company.companyId}">selected="selected"</c:if> >${company.companyName}</option>
										</c:forEach>
										
									</select>
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-4 control-label" for="brand_name"><spring:message
										code="label.brandname" /><span class="text-danger">*</span></label>
								<div class="col-md-6">
									<select class="brand_chosen" style="width: 200px;"
										data-placeholder="<spring:message code='label.choosebrand' />"
										id="brand_name" name="brandId">
										<option value=""></option>
										<c:forEach items="${brandList}" var="brand">
												<option value="${brand.brandId}"  <c:if test="${brand.brandId eq restaurant.brand.brandId}">selected="selected"</c:if> >${brand.brandName}</option>
										</c:forEach>
									</select>
								</div>
							</div>
						</security:authorize>
						<security:authorize ifAnyGranted="<%=Role.BRAND_ADMIN.name()%>">
							<input type="hidden" name="company"
								value="${brand.company.companyId}">
							<input type="hidden" name="brandId" value="${brand.brandId}">
						</security:authorize>
						<div class="form-group">
							<label class="col-md-4 control-label" for="restaurant_Name"><spring:message
									code="label.restaurantname" /><span class="text-danger">*</span>
							</label>
							<div class="col-md-6">
								<input id="restaurant_Name" name="name" class="form-control"
									placeholder="<spring:message code='label.restaurantname'/>.."
									type="text" value="${restaurant.name}">
							</div>
						</div>
		
<!-- 						<div class="form-group"> -->
<%-- 							<label class="col-md-4 control-label" for="restaurant_displayName"><spring:message --%>
<%-- 									code="label.displayname" /> <span class="text-danger">*</span> --%>
<!-- 							</label> -->
<!-- 							<div class="col-md-6"> -->

<!-- 								<input id="restaurant_displayName" name="displayName" class="form-control" -->
<%-- 									placeholder="<spring:message code='label.displayname'/>.." --%>
<!-- 									type="text"> -->
<!-- 							</div> -->
<!-- 						</div> -->
						
						<div class="form-group">
							<label class="col-md-4 control-label" for="currency"><spring:message
									code="label.currency" /><span class="text-danger">*</span> </label>
							<div class="col-md-6">
								<select class="currency_chosen" style="width: 200px;"
									id="currency" name="currency">
									<c:forEach items="${currencyList}" var="currency">
									   <option value=""></option>
										<c:choose>
											<c:when test="${currency.active == true}">
												<option value="${currency.currencyId}"
													<c:if test="${currency.currencyId eq restaurant.baseCurrency.currencyId}">selected="selected"</c:if>>${currency.currencyName} (${currency.currencySign})</option>
											</c:when>
											<c:otherwise>
												<option value=""></option>
											</c:otherwise>
										</c:choose>
									</c:forEach>
									
								</select>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label" for="cuisine"><spring:message
									code="label.cuisine" /><span class="text-danger">*</span></label>
							<div class="col-md-6">
								<select class="cuisine_chosen"
									data-placeholder="<spring:message code='label.choosecuisine' />"
									multiple="" class="chosen" style="width: 200px;" id="cuisine"
									name="cuisine">
									<c:forEach items="${cuisineList}" var="cuisine">
										<c:set var="present" value="false" />
										<c:forEach items="${cuisines}" var="restCuisine">
											<c:if test="${cuisine.name eq restCuisine}">
												<c:set var="present" value="true" />
											</c:if>
										</c:forEach>
										<option value="${cuisine.name}"
											<c:if test="${present}">	selected="selected" </c:if>>
											${cuisine.name}</option>

									</c:forEach>
								</select>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label" for="example-daterange1"><spring:message
									code="label.logo" /> </label>
							<div class="col-md-6">
								<input type="file" name="logo" accept="image/*" />
							</div>
						</div>

<!-- 						<div class="form-group"> -->
<%-- 							<label class="col-md-4 control-label" for="example-daterange1"><spring:message --%>
<%-- 									code="label.profile" /> </label> --%>
<!-- 							<div class="col-md-6"> -->
<!-- 								<input type="file" name="profile" accept="image/*" /> -->
<!-- 							</div> -->
<!-- 						</div> -->

						<div class="form-group" id="profilePic">
							<label class="col-md-4 control-label" for="offer_validtill"><spring:message code="label.profile" /></label>
							 <div id="preview" style="width: 200px; height: 100px; overflow: hidden;" class="col-md-6" >
								<img border="0"
									src="<%=contexturl%>resources/img/placeholders/avatars/profileAvatar.jpg"
									 title="Click here to change photo"
									onclick="OpenFileDialog();" id="profileimage" />
							</div>
							<input type="file" name="profile" id="profileImageChooser" style="display: none;"
								name="imageChooser"
								accept="image/jpg,image/jpeg,image/png,image/gif,image/bmp,image/tiff" />
	
						</div>
						<input type="hidden" id="profileX1" name="profileX1" value="0"/>
						<input type="hidden" id="profileX2" name="profileX2" value="0"/>
						<input type="hidden" id="profileY1" name="profileY1" value="0"/>
						<input type="hidden" id="profileY2" name="profileY2" value="0"/>
						<input type="hidden" id="profileW" name="profileW" value="0"/>
						<input type="hidden" id="profileH" name="profileH" value="0"/>
					</div>
					<div class="block">
						<div class="block-title">
							<h2>
								<strong><spring:message code="heading.contactinfo" /></strong>
								<small style="float:left; margin-top: 4px;"> All communication from gourmet7 will be done on the contact info you provide</small>
							</h2>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label" for="telephone1"><spring:message
									code="label.telephone1" /><span class="text-danger">*</span></label>
							<div class="col-md-6">

								<input id="telephone1" name="telephone1" class="form-control"
									value="${restaurant.telephone1}"
									placeholder="<spring:message code='label.telephone1'/>.."
									type="text">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label" for="telephone2"><spring:message
									code="label.telephone2" /></label>
							<div class="col-md-6">

								<input id="telephone2" name="telephone2" class="form-control"
									value="${restaurant.telephone2}"
									placeholder="<spring:message code='label.telephone2'/>.."
									type="text">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label" for="rest_email"><spring:message
									code="label.restaurantemail" /></label>
							<div class="col-md-6">

								<input id="rest_email" name="restaurantEmail"
									class="form-control" value="${restaurant.emailId}"
									placeholder="<spring:message code="label.restaurantemail"/>.."
									type="text">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label" for="mobile1"><spring:message
									code="label.mobile1" /><span class="text-danger">*</span></label>
							<div class="col-md-6">

								<input id="mobile1" name="mobile1" class="form-control"
									value="${restaurant.mobile1}"
									placeholder="<spring:message code="label.mobile1"/>.."
									type="text">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label" for="mobile2"><spring:message
									code="label.mobile2" /></label>
							<div class="col-md-6">

								<input id="mobile2" name="mobile2" class="form-control"
									value="${restaurant.mobile2}"
									placeholder="<spring:message code="label.mobile2"/>.."
									type="text">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label" for="addressline1"><spring:message
									code="label.addressline1" /><span class="text-danger">*</span>
							</label>
							<div class="col-md-6">

								<input id="addressline1" name="addressLine1"
									class="form-control" value="${restaurant.addressLine1}"
									placeholder="<spring:message code="label.addressline1"/>.."
									type="text">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label" for="addressline2"><spring:message
									code="label.addressline2" /></label>
							<div class="col-md-6">

								<input id="addressline2" name="addressLine2"
									class="form-control" value="${restaurant.addressLine2}"
									placeholder="<spring:message code="label.addressline2"/>.."
									type="text">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label" for="country"><spring:message
									code="label.countryname" /><span class="text-danger">*</span></label>
							<div class="col-md-6">
								<select class="country_chosen" style="width: 200px;"
									id="country"
									data-placeholder="<spring:message code='label.choosecountry' />"
									name="country">
									<option value=""></option>
									<c:forEach items="${countryList}" var="country">
										<option value="${country.countryId}"
											<c:if test="${country.countryId eq restaurant.country.countryId }">selected="selected"</c:if>>${country.countryName}</option>
									</c:forEach>
								</select>
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-md-4 control-label" for="city"><spring:message
									code="label.cityname" /><span class="text-danger">*</span></label>
							<div class="col-md-6">
								<select class="city_chosen" style="width: 200px;" id="city"
									data-placeholder="<spring:message code='label.choosecity' />"
									name="city">
									<c:forEach items="${cityList}" var="city">
												<option value="${city.cityId}"  <c:if test="${city.cityId eq restaurant.city.cityId}">selected="selected"</c:if> >${city.cityName}</option>
									</c:forEach>
								</select>
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-md-4 control-label" for="area"><spring:message
									code="label.areaname" /><span class="text-danger">*</span></label>
							<div class="col-md-6">
								<select class="area_chosen" style="width: 200px;" id="area"
									data-placeholder="<spring:message code='label.choosearea' />"
									name="area">
									<c:forEach items="${areaList}" var="area">
												<option value="${area.areaId}"  <c:if test="${area.areaId eq restaurant.area.areaId}">selected="selected"</c:if> >${area.areaName}</option>
									</c:forEach>
								</select>
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-md-4 control-label" for="pincode"><spring:message
									code="label.pincode" /><span class="text-danger">*</span></label>
							<div class="col-md-6">

								<input id="pincode" name="pincode" class="form-control"
									value="${restaurant.pincode}"
									placeholder="<spring:message code="label.pincode"/>.."
									type="text">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label" for="latitude"><spring:message
									code="label.latitude" /></label>
							<div class="col-md-6">

								<input id="latitude" name="latitude" class="form-control" readonly="readonly"
									value="${restaurant.latitude}"
									placeholder="<spring:message code="label.latitude"/>.."
									type="text" readonly="readonly">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label" for="longitude"><spring:message
									code="label.longitude" /></label>
							<div class="col-md-6">

								<input id="longitude" name="longitude" class="form-control" readonly="readonly"
									value="${restaurant.longitude}"
									placeholder="<spring:message code="label.longitude"/>.."
									type="text" readonly="readonly">
							</div>
						</div>
				


					</div>


					<div class="block">
						<div class="block-title">
							<h2>
								<strong>Map</strong>
							</h2>
							Please Drag Pointer to Locate Restaurant
						</div>

						<div id="map-canvas"></div>
					</div>
					
						<div class="block">
						<div class="block-title">
							<h2>
								<strong><spring:message code="heading.socialinfo" /></strong>
								<small style="float:left; margin-top: 4px;">Please give your social site details here</small>
							</h2>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label" for="facebook_link"><spring:message
									code="label.facebook" /> </label>
							<div class="col-md-6">
								<input id="facebook_link" name="facebookLink"
									class="form-control" value="${restaurant.facebooklink}"
									placeholder="<spring:message code='label.facebook'/>.."
									type="text">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label" for="twitter_link"><spring:message
									code="label.twitter" /> </label>
							<div class="col-md-6">
								<input id="twitter_link" name="twitterLink" class="form-control"
									value="${restaurant.twitterlink}"
									placeholder="<spring:message code='label.twitter'/>.."
									type="text">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label" for="keyword"><spring:message
									code="label.keyword" /> </label>
							<div class="col-md-6">
								<input id="keyword" name="keyword" class="form-control"
									value="${restaurant.keyword}"
									placeholder="<spring:message code='label.keyword'/>.."
									type="text">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label" for="metatag_link"><spring:message
									code="label.metatag" /> </label>
							<div class="col-md-6">
								<input id="metatag_link" name="metatag" class="form-control"
									value="${restaurant.metatag}"
									placeholder="<spring:message code='label.metatag'/>.."
									type="text">
							</div>
						</div>
					</div>
				</div>

				<div class="col-md-6">
					<div class="block">
						<div class="block-title">
							<h2>
								<strong><spring:message code="heading.admininfo" /></strong>
								<small style="float:left; margin-top: 4px;">This info will be used for login. Please make sure you add the exact details and make sure it is not misused</small>
						</h2>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label" for="first_Name"><spring:message
									code="label.firstname" /><span class="text-danger">*</span> </label>
							<div class="col-md-6">
								<input id="restAdmin_id" name="restAdminId" type="hidden"
									value="${restaurant.admin.userId}"> <input
									id="first_Name" name="firstName" class="form-control"
									placeholder="<spring:message code='label.firstname'/>.."
									type="text" value="${restaurant.admin.firstName}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label" for="last_Name"><spring:message
									code="label.lastname" /><span class="text-danger">*</span> </label>
							<div class="col-md-6">

								<input id="last_Name" name="lastName" class="form-control"
									placeholder="<spring:message code="label.lastname"/>.."
									type="text" value="${restaurant.admin.lastName}">
							</div>
						</div>


						<div class="form-group">
							<label class="col-md-4 control-label" for="email"><spring:message
									code="label.emailid" /><span class="text-danger">*</span></label>
							<div class="col-md-6">
								<input id="email" name="emailId" class="form-control"
									placeholder="test@example.com" type="text" onchange="emailVerification()"
									value="${restaurant.admin.emailId}">
									<span class="text-success" id="emailavailable" style="display: none">
										<spring:message code="alert.emailavailable" /> </span> 
									<span class="text-danger" id="emailnotavailable" style="display: none">
										<spring:message code="alert.emailnotavailable" /> </span> 
									<span id="emailloading" style="display: none"><i
										class="fa fa-spinner fa-spin"></i> </span>
							</div>
						</div>


						<div class="form-group">
							<label class="col-md-4 control-label" for="mobile"><spring:message
									code="label.mobile" /><span class="text-danger">*</span></label>
							<div class="col-md-6">
								<input id="mobile" name="mobile" class="form-control"
									placeholder='<spring:message code="label.mobile"></spring:message>'
									type="text" value="${restaurant.admin.mobile}">
							</div>
						</div>
						

						<div class="form-group">
							<label class="col-md-4 control-label" for="login"><spring:message
									code="label.loginid"/><span class="text-danger">*</span></label>
							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon"><%=propvalue.restaurantLoginPrefix%></span>
									<input id="login" name="loginId" class="form-control"
										value="${restaurant.admin.loginId}"
										placeholder='<spring:message code='label.loginid'/>'
										type="text"> 
								</div>
								<span class="label label-success"
									id="availability" style="cursor: pointer;"><i
									class="fa fa-check"></i> <spring:message
										code="label.checkavailability" /></span><span class="text-success"
									id="available" style="display: none"> <spring:message
										code="alert.loginavailable" />
								</span> <span class="text-danger" id="notavailable"
									style="display: none"> <spring:message
										code="alert.loginnotavailable" />
								</span>
								<span id="loading" style="display: none"><i class="fa fa-spinner fa-spin"></i>
						       </span>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label" for="password"><spring:message
									code="label.password" /><span class="text-danger">*</span></label>
							<div class="col-md-6">
								<input id="password" name="password" class="form-control"
									placeholder="Choose a crazy one.." type="password">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label" for="confirm_password"><spring:message
									code="label.confirmpassword" /><span class="text-danger">*</span>
							</label>
							<div class="col-md-6">
								<input id="confirm_password" name="confirmPassword"
									class="form-control" placeholder="..and confirm it!"
									type="password">
							</div>
						</div>
					</div>
					
					<div class="block full">
						<div class="block-title">
							<div class="block-options pull-right"></div>
							<h2>
								<strong><spring:message code="heading.restaurantimg" />
								</strong>
								<small style="float:left; margin-top: 4px;">please add clear images so that users can get the best picture of your outlet</small>
							</h2>
						</div>

						<div id="myId" class="dropzone dz-clickable"
							enctype="multipart/form-data">
							<div class="dz-default dz-message">
								<span><spring:message code="heading.uploadmsg" /> </span>
							</div>
						</div>
					</div>
					
					
				
				


					<!-- 								<div> -->
					<!-- 									<div id="myId" action="UploadRestaurantImage" -->
					<!-- 										class="dropzone dz-clickable" enctype="multipart/form-data"> -->
					<!-- 									</div> -->

					<!-- 								</div> -->

					<div class="block">
						<div class="block-title">
							<h2>
								<strong><spring:message code="heading.additionalinfo" /></strong>
								<small style="float:left; margin-top: 4px;">Please fill in additional info to reach your target customers</small>
							</h2>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label" for="timing"><spring:message
									code="label.timing" /> </label>
							<div class="col-md-6">
								<input id="timing" name="timing" class="form-control"
									value="${restaurant.timings}"
									placeholder="<spring:message code='label.timing'/>.."
									type="text">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label" for="capacity"><spring:message
									code="label.capacity" /> </label>
							<div class="col-md-6">
								<input id="capacity" name="capacity" class="form-control"
									value="${restaurant.capacity}"
									placeholder="<spring:message code='label.capacity'/>.."
									type="text">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label" for="avgCostofTwo"><spring:message
									code="label.avgcostoftwo" /> </label>
							<div class="col-md-6">
								<input id="avgCostofTwo" name="averageCostOfTwo"
									class="form-control" value="${restaurant.averageCostOfTwo}"
									placeholder="<spring:message code='label.avgcostoftwo'/>.."
									type="text">
							</div>
						</div>

<!-- 						<div class="form-group"> -->
<%-- 							<label class="col-md-4 control-label" for="avgNoOfVisitor"><spring:message --%>
<!-- 									code="label.avgnoofvisitor" /> </label> -->
<!-- 							<div class="col-md-6"> -->
<!-- 								<input id="avgNoOfVisitor" name="averageNoOfVisitor" -->
<%-- 									class="form-control" value="${restaurant.averageNoOfVisitor}" --%>
<%-- 									placeholder="<spring:message code='label.avgnoofvisitor'/>.." --%>
<!-- 									type="text"> -->
<!-- 							</div> -->
<!-- 						</div> -->

						<div class="form-group">
							<label class="col-md-4 control-label" for="bestdishes"><spring:message
									code="label.bestdishes" /> </label>
							<div class="col-md-6">
								<input id="bestdishes" name="bestdishes" class="form-control"
									placeholder="<spring:message code='label.bestdishes'/>.."
									value="${restaurant.bestdishes}" type="text">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label" for="story"><spring:message
									code="label.story" /> </label>
							<div class="col-md-6">
								<textarea id="story" name="story" class="form-control">${restaurant.story}</textarea>
							</div>
						</div>
					</div>
					<div class="block" id="featurecontrol">
						<%@include file="../featurecontrol/featurecontrol.jsp"%>
					</div>
				</div>

			</div>

			<div class="block">
				<div class="block-title">
					<h2>
						<strong><spring:message code="heading.facilities" /></strong>
					<small style="float:left; margin-top: 4px;">Please click on services you have in your outlet, this will help user search your restaurant on the basis of services. 
						It works real time, so if any of your service "like wifi" is temperiorily not working then you can inactive it and activate it 
						again as soon as your wifi is back</small>
						</h2>
				</div>
				<div class="form-group">
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="ac"><spring:message
								code="label.ac" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="ac"
								name="ac" type="checkbox"
								<c:if test="${restaurant.ac}">checked</c:if> /> <span></span></label>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="creditCard"><spring:message
								code="label.acceptscreditcard" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input
								<c:if test="${restaurant.acceptsCreditCard}">checked</c:if>
								id="creditCard" name="acceptsCreditCard" type="checkbox" /> <span></span></label>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="wifi"><spring:message
								code="label.wifi" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="wifi"
								<c:if test="${restaurant.wifi}">checked</c:if> name="wifi"
								type="checkbox" /> <span></span></label>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="gourmet7_Reach"><spring:message
								code="label.homedelivery" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input
								<c:if test="${restaurant.homeDelivery}">checked</c:if>
								id="homedelivery" name="homeDelivery" type="checkbox" /> <span></span></label>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="nonvage"><spring:message
								code="label.nonvage" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="nonvage"
								<c:if test="${restaurant.nonvegeterian}">checked</c:if>
								name="nonvegeterian" type="checkbox" /> <span></span></label>
						</div>
					</div>

					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="parking"><spring:message
								code="label.parking" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="parking"
								<c:if test="${restaurant.parking}">checked</c:if> name="parking"
								type="checkbox" /> <span></span></label>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="toilet"><spring:message
								code="label.toilet" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="toilet"
								<c:if test="${restaurant.toilet}">checked</c:if> name="toilet"
								type="checkbox" /> <span></span></label>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="servesalcohol"><spring:message
								code="label.servesalcohol" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input
								<c:if test="${restaurant.servesAlcohol}">checked</c:if>
								id="servesalcohol" name="servesAlcohol" type="checkbox" /> <span></span></label>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="smoking"><spring:message
								code="label.smoking" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="smoking"
								<c:if test="${restaurant.smokingaAllowed}">checked</c:if>
								name="smokingAllowed" type="checkbox" /> <span></span></label>
						</div>
					</div>

					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="music"><spring:message
								code="label.music" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="music"
								<c:if test="${restaurant.music}">checked</c:if> name="music"
								type="checkbox" /> <span></span></label>
						</div>
					</div>

					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="happyHours"><spring:message
								code="label.happyhours" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input
								<c:if test="${restaurant.happyHours}">checked</c:if>
								id="happyHours" name="happyHours" type="checkbox" /> <span></span></label>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="takeaway"><spring:message
								code="label.takeaway" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input
								<c:if test="${restaurant.takeAway}">checked</c:if> id="takeaway"
								name="takeAway" type="checkbox" /> <span></span></label>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="snacks"><spring:message
								code="label.snacks" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="snacks"
								<c:if test="${restaurant.snacks}">checked</c:if> name="snacks"
								type="checkbox" /> <span></span></label>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="dinein"><spring:message
								code="label.dinein" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="dinein"
								<c:if test="${restaurant.dinein}">checked</c:if> name="dinein"
								type="checkbox" /> <span></span></label>
						</div>
					</div>
					
					
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="outdoorSeating"><spring:message
								code="label.outdoorSeating" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="outdoorSeating"
								<c:if test="${restaurant.outdoorSeating}">checked</c:if> name="outdoorSeating"
								type="checkbox" /> <span></span></label>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="tableReservation"><spring:message
								code="label.tableReservation" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="tableReservation"
								<c:if test="${restaurant.tableReservation}">checked</c:if> name="tableReservation"
								type="checkbox" /> <span></span></label>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="barArea"><spring:message
								code="label.barArea" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="barArea"
								<c:if test="${restaurant.barArea}">checked</c:if> name="barArea"
								type="checkbox" /> <span></span></label>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="corporateEvents"><spring:message
								code="label.corporateEvents" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="corporateEvents"
								<c:if test="${restaurant.corporateEvents}">checked</c:if> name="corporateEvents"
								type="checkbox" /> <span></span></label>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="liveSportsBroadcast"><spring:message
								code="label.liveSportsBroadcast" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="liveSportsBroadcast"
								<c:if test="${restaurant.liveSportsBroadcast}">checked</c:if> name="liveSportsBroadcast"
								type="checkbox" /> <span></span></label>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="dressCode"><spring:message
								code="label.dressCode" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="dressCode"
								<c:if test="${restaurant.dressCode}">checked</c:if> name="dressCode"
								type="checkbox" /> <span></span></label>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="open24_7"><spring:message
								code="label.open24_7" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="open24_7"
								<c:if test="${restaurant.open24_7}">checked</c:if> name="open24_7"
								type="checkbox" /> <span></span></label>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="privatePartyArea"><spring:message
								code="label.privatePartyArea" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="privatePartyArea"
								<c:if test="${restaurant.privatePartyArea}">checked</c:if> name="privatePartyArea"
								type="checkbox" /> <span></span></label>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="sundayBrunch"><spring:message
								code="label.sundayBrunch" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="sundayBrunch"
								<c:if test="${restaurant.sundayBrunch}">checked</c:if> name="sundayBrunch"
								type="checkbox" /> <span></span></label>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="selfServices"><spring:message
								code="label.selfServices" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="selfServices"
								<c:if test="${restaurant.selfServices}">checked</c:if> name="selfServices"
								type="checkbox" /> <span></span></label>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="petFriendly"><spring:message
								code="label.petFriendly" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="petFriendly"
								<c:if test="${restaurant.petFriendly}">checked</c:if> name="petFriendly"
								type="checkbox" /> <span></span></label>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="jainFood"><spring:message
								code="label.jainFood" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="jainFood"
								<c:if test="${restaurant.jainFood}">checked</c:if> name="jainFood"
								type="checkbox" /> <span></span></label>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="buffetLunch"><spring:message
								code="label.buffetLunch" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="buffetLunch"
								<c:if test="${restaurant.buffetLunch}">checked</c:if> name="buffetLunch"
								type="checkbox" /> <span></span></label>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="buffetDinner"><spring:message
								code="label.buffetDinner" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="buffetDinner"
								<c:if test="${restaurant.buffetDinner}">checked</c:if> name="buffetDinner"
								type="checkbox" /> <span></span></label>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="banquet"><spring:message
								code="label.banquet" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="banquet"
								<c:if test="${restaurant.banquet}">checked</c:if> name="banquet"
								type="checkbox" /> <span></span></label>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="midnightBuffet"><spring:message
								code="label.midnightBuffet" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="midnightBuffet"
								<c:if test="${restaurant.midnightBuffet}">checked</c:if> name="midnightBuffet"
								type="checkbox" /> <span></span></label>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="partyOrders"><spring:message
								code="label.partyOrders" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="partyOrders"
								<c:if test="${restaurant.partyOrders}">checked</c:if> name="partyOrders"
								type="checkbox" /> <span></span></label>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="disablFacility"><spring:message
								code="label.disablFacility" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="disablFacility"
								<c:if test="${restaurant.disablFacility}">checked</c:if> name="disablFacility"
								type="checkbox" /> <span></span></label>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="theme"><spring:message
								code="label.theme" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="theme"
								<c:if test="${restaurant.theme}">checked</c:if> name="theme"
								type="checkbox" /> <span></span></label>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="inMall"><spring:message
								code="label.inMall" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="inMall"
								<c:if test="${restaurant.inMall}">checked</c:if> name="inMall"
								type="checkbox" /> <span></span></label>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="englishSpeakingWaiters"><spring:message
								code="label.englishSpeakingWaiters" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="englishSpeakingWaiters"
								<c:if test="${restaurant.englishSpeakingWaiters}">checked</c:if> name="englishSpeakingWaiters"
								type="checkbox" /> <span></span></label>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="couponsAccepted"><spring:message
								code="label.couponsAccepted" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="couponsAccepted"
								<c:if test="${restaurant.couponsAccepted}">checked</c:if> name="couponsAccepted"
								type="checkbox" /> <span></span></label>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="liveFoodCounter"><spring:message
								code="label.liveFoodCounter" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="liveFoodCounter"
								<c:if test="${restaurant.liveFoodCounter}">checked</c:if> name="liveFoodCounter"
								type="checkbox" /> <span></span></label>
						</div>
					</div>
				</div>

			</div>
		</div>

		<div style="display: none;" id="advanced-second"
			class="step ui-formwizard-content">
			<div class="wizard-steps">
				<div class="row">
					<div class="col-xs-4 done">
						<span><spring:message code="heading.restaurantdetail" /></span>
					</div>
					<div class="col-xs-4 active">
						<span><spring:message code="heading.discountdetails" /></span>
					</div>
					<div class="col-xs-4">
						<span><spring:message code="heading.termsandconditions" /></span>
					</div>
				</div>
			</div>
			<div class="block">
				<div class="block-title">
					<h2>
						<strong><spring:message code="heading.discountinfo" /></strong>
					</h2>
				</div>
				<div class="table-responsive">
					<table id="example-datatable"
						class="table table-vcenter table-condensed table-bordered">
						<thead>
							<tr>
								<th class="text-center"></th>
								<th class="text-center"><spring:message code="days.monday" /></th>
								<th class="text-center"><spring:message code="days.tuesday" /></th>
								<th class="text-center"><spring:message
										code="days.wednesday" /></th>
								<th class="text-center"><spring:message
										code="days.thursday" /></th>
								<th class="text-center"><spring:message code="days.friday" /></th>
								<th class="text-center"><spring:message
										code="days.saturday" /></th>
								<th class="text-center"><spring:message code="days.sunday" /></th>
							</tr>
						</thead>
						<tbody id="tbody">
							<tr>
								<td class="text-center" colspan="8"
									style="font-weight: bold; background-color: #F8F8F8;">
									<div class="memberType">
										<spring:message code="label.basic" />
									</div>
									<div class="memberRange  form-group">
										<div class="input-group">
												<span class="input-group-addon" id="min_currency"></span>
												<input id="minRange" class="form-control" type="hidden"
													value="0" name="minRange" />
													<input class="form-control text-right"
													type="text" value="0" name="minRange1" id="minRange1" disabled="disabled" />
									 	</div>
									</div>

									<div class="form-group"
										style="margin: 0; width: 185px; float: left; margin-left: 30px;">
										<div>
											<div class="input-group">
													<span class="input-group-addon" id="max_currency"></span>
													<div>
														<input id="maxRange" name="maxRange" class="form-control text-right"
															placeholder="to.." type="text">
													</div>
											</div>
										</div>
									</div>

								</td>
									
							</tr>
							<tr>
								<td class="text-center" style="font-weight: bold;"><spring:message
										code="label.discount" /></td>
								<td class="text-center">
									<div class="form-group" style="margin: 0">
										<div>
											<div class="input-group">
												<input id="d_monday" name="dMonday" class="form-control text-right"
													placeholder="<spring:message code="label.disountmsg"/>.."
													type="text" value="${modal.mondayDiscount}">
												<span class="input-group-addon" >%</span>
											</div>
										</div>
									</div>
								</td>
								<td class="text-center">
									<div class="form-group" style="margin: 0">
										<div>
											<div class="input-group">
												<input id="d_tuesday" name="dTuesday" class="form-control text-right"
													placeholder="<spring:message code="label.disountmsg"/>.."
													type="text" value="${modal.tuesdayDiscount}">
												<span class="input-group-addon" >%</span>
											</div>
										</div>
									</div>
								</td>
								<td class="text-center">
									<div class="form-group" style="margin: 0">
										<div>
											<div class="input-group">
												<input id="d_wednesday" name="dWednesday"
												class="form-control text-right"
												placeholder="<spring:message code="label.disountmsg"/>.."
												type="text" value="${modal.wednesdayDiscount}">
												<span class="input-group-addon" >%</span>
											</div>
										</div>
									</div>
								</td>
								<td class="text-center">
									<div class="form-group" style="margin: 0">
										<div>
											<div class="input-group">
												<input id="d_thursday" name="dThursday" class="form-control text-right"
												placeholder="<spring:message code="label.disountmsg"/>.."
												type="text" value="${modal.thursdayDiscount}">
												<span class="input-group-addon" >%</span>
											</div>
										</div>
									</div>
								</td>
								<td class="text-center">
									<div class="form-group" style="margin: 0">
										<div>
											<div class="input-group">
												<input id="d_friday" name="dFriday" class="form-control text-right"
												placeholder="<spring:message code="label.disountmsg"/>.."
												type="text" value="${modal.fridayDiscount}">
												<span class="input-group-addon" >%</span>
											</div>
										</div>
									</div>
								</td>
								<td class="text-center">
									<div class="form-group" style="margin: 0">
										<div>
											<div class="input-group">
												<input id="d_saturday" name="dSaturday" class="form-control text-right"
												placeholder="<spring:message code="label.disountmsg"/>.."
												type="text" value="${modal.saturdayDiscount}">
												<span class="input-group-addon" >%</span>
											</div>
										</div>
									</div>
								</td>
								<td class="text-center">
									<div class="form-group" style="margin: 0">
										<div>
											<div class="input-group">
												<input id="d_sunday" name="dSunday" class="form-control text-right"
												placeholder="<spring:message code="label.disountmsg"/>.."
												type="text" value="${modal.sundaydiscount}">
												<span class="input-group-addon" >%</span>
											</div>
										</div>
									</div>
								</td>
							</tr>
							<tr>
								<td class="text-center" style="font-weight: bold;"><spring:message
										code="label.complementory" /></td>
								<td class="text-center">
									<div class="form-group" style="margin: 0">
										<div>
											<input id="c_monday" name="cMonday" class="form-control"
												placeholder="<spring:message code="label.complementrymsg"/>.."
												type="text" value="${modal.mondayComp}">
										</div>
									</div>
								</td>
								<td class="text-center">
									<div class="form-group" style="margin: 0">
									   <div>
										<input id="c_tuesday" name="cTuesday" class="form-control"
											placeholder="<spring:message code="label.complementrymsg"/>.."
											type="text" value="${modal.tuesdayComp}">
										</div>
									</div></td>
								<td class="text-center"><div class="form-group"
										style="margin: 0">
										<div>
										<input id="c_wednesday" name="cWednesday" class="form-control"
											placeholder="<spring:message code="label.complementrymsg"/>.."
											type="text" value="${modal.wednesdayComp}">
										</div>
									</div>
								</td>
								<td class="text-center"><div class="form-group"
										style="margin: 0">
										<div>
										<input id="c_thursday" name="cThursday" class="form-control"
											placeholder="<spring:message code="label.complementrymsg"/>.."
											type="text" value="${modal.thursdayComp}">
										</div>
									</div>
								</td>
								<td class="text-center"><div class="form-group"
										style="margin: 0">
										<div>
										<input id="c_friday" name="cFriday" class="form-control"
											placeholder="<spring:message code="label.complementrymsg"/>.."
											type="text" value="${modal.fridayComp}">
										</div>
									</div>
								</td>
								<td class="text-center"><div class="form-group"
										style="margin: 0">
										<div>
										<input id="c_saturday" name="cSaturday" class="form-control"
											placeholder="<spring:message code="label.complementrymsg"/>.."
											type="text" value="${modal.saturdayComp}">
									</div>
								</td>
								<td class="text-center"><div class="form-group"
										style="margin: 0">
										<div>
										<input id="c_sunday" name="cSunday" class="form-control"
											placeholder="<spring:message code="label.complementrymsg"/>.."
											type="text" value="${modal.sundayComp}">
										</div>
									</div>
								</td>
							</tr>
							<tr>
								<td class="text-center" style="font-weight: bold;"><spring:message
										code="label.comment" /></td>
								<td class="text-center"><div class="form-group"
										style="margin: 0">
										<div>
										<input id="comment_monday" name="commentMonday"
											class="form-control"
											placeholder="<spring:message code="label.commentmsg"/>.."
											type="text" value="${modal.tuesdayComment}">
										</div>	
									</div>
								</td>
								<td class="text-center"><div class="form-group"
										style="margin: 0">
										<div>
										<input id="comment_tuesday" name="commentTuesday"
											class="form-control"
											placeholder="<spring:message code="label.commentmsg"/>.."
											type="text" value="${modal.tuesdayComment}">
										</div>	
									</div>
								</td>
								<td class="text-center"><div class="form-group"
										style="margin: 0">
										<div>
										<input id="comment_wednesday" name="commentWednesday"
											class="form-control"
											placeholder="<spring:message code="label.commentmsg"/>.."
											type="text" value="${modal.wednesdayComment}">
										</div>	
									</div>
								</td>
								<td class="text-center"><div class="form-group"
										style="margin: 0">
										<div>
										<input id="comment_thursday" name="commentThursday"
											class="form-control"
											placeholder="<spring:message code="label.commentmsg"/>.."
											type="text" value="${modal.thursdayComment}">
										</div>
									</div>
								</td>
								<td class="text-center"><div class="form-group"
										style="margin: 0">
										<div>
										<input id="comment_friday" name="commentFriday"
											class="form-control"
											placeholder="<spring:message code="label.commentmsg"/>.."
											type="text" value="${modal.fridayComment}">
										</div>
									</div>
								</td>
								<td class="text-center"><div class="form-group"
										style="margin: 0">
										<div>
										<input id="comment_saturday" name="commentSaturday"
											class="form-control"
											placeholder="<spring:message code="label.commentmsg"/>.."
											type="text" value="${modal.saturdayComment}">
										</div>
									</div>
								</td>
								<td class="text-center"><div class="form-group"
										style="margin: 0">
										<div>
										<input id="comment_sunday" name="commentSunday"
											class="form-control"
											placeholder="<spring:message code="label.commentmsg"/>.."
											type="text" value="${modal.sundayComment}">
										</div>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>

		<div style="display: none;" id="advanced-third"
			class="step ui-formwizard-content">
			<div class="wizard-steps">
				<div class="row">
					<div class="col-xs-4 done">
						<span><spring:message code="heading.restaurantdetail" /></span>
					</div>
					<div class="col-xs-4 done">
						<span><spring:message code="heading.discountdetails" /></span>
					</div>
					<div class="col-xs-4 active">
						<span><spring:message code="heading.termsandconditions" /></span>
					</div>
				</div>
			</div>
			<div class="block">
				<div class="block-title">
					<h2>
						<strong><spring:message code="label.termsandcondition" /></strong>
					</h2>
				</div>
				<div class="terms_text">
					<ol type="1">
						<li>The Restaurant has full power, legal capacity, and
							authority to carry out the terms hereof.</li>
						<br>

						<li>To ensure that members of Gourmet7 get accurate
							information, the Restaurantat all times must provideauthentic
							details offood and dishes offered, specialties,key features,
							affiliations and certifications ifany along with photographs and
							offer details on the website.</li>
						<br>

						<li>Creation, management and updating of the Restaurant
							pagealong with the data on the website will be the responsibility
							of the Restaurant.</li>
						<br>

						<li>Gourmet7 at its sole discretion shall be entitled to
							discontinue, halt or suspend displaying of all information on its
							website, related to theRestaurant, in case the Restaurant fails
							to update the information over a period of time or honor any
							points mentioned in the terms and conditions.</li>
						<br>

						<li>The Restaurantagrees and accepts to provide its services
							i.e.,food, drinks,and other allied services,to the customers of
							Gourmet7,either complimentary / at discounted rates.The
							complimentary offerings / discounts will have to be decided by
							the Restaurant. The offerings could be different depending on the
							time of the day (Breakfast/Lunch/Dinner) and also the day of the
							week.</li>
						<br>

						<li>To enhancemember loyalty, Gourmet7 suggests that
							theRestaurant works on a principal whereby a member who visits a
							regularly is offered higher discount than others. In event of the
							Restaurant deciding not to opt for such discounting terms, it
							will still offer a flat pre-decided rate of discount.</li>
						<br>

						<li>TheRestaurant is required to charge and recover the
							charges for its services, directly from the concerned member.
							Gourmet7 shall not be liable in case of any default on the part
							of any member.</li>
						<br>

						<li>Gourmet7is entitled to issue / sellor distribute
							membershipas a part of certain promotional offers, events, and
							activitiesto its prospective customers.</li>
						<br>

						<li>The agreement /arrangement in respect to the said
							membership offer shall be between Gourmet7and the concerned
							member, and the Restaurantshall be no way concerned with the
							same.</li>
						<br>

						<li>Gourmet7 will carry out the marketing and promotional
							activities as stated herein and the company hereby grants
							conditional permission to Gourmet7 to use its logos, brand name
							and trademarks for such purpose.</li>
						<br>

						<li>The Restaurantwill ensure that itrenders due and proper
							and high quality services to the members of Gourmet7.</li>
						<br>

						<li>Gourmet7under any circumstances shall not be responsible
							for the quality of the offerings and /or services made available
							by the Restaurant to the members of Gourmet7, and the same shall
							be the sole responsibility of the Restaurant.</li>
						<br>

						<li>The Restaurant shall be solely liable to address and
							remedy all the claims, disputes made by anyGourmet7member in
							respect to the quality of the offerings and /or services rendered
							by the Restaurant.</li>
						<br>

						<li>The Restaurant shall ensure that it meetsall necessary
							statutory compliances including FDA compliance.</li>
						<br>

						<li>The Restaurant grantsGourmet7, a temporary,
							non-exclusive, royalty free, worldwide license to display name
							logos, trademarks and service marks on Gourmet7’swebsite,related
							domain names and other publications of Gouret7 in its sole
							discretion.</li>
						<br>

						<li>Neither party shall be responsible for delays or in
							performance resulting from acts beyond control. Such acts include
							but are not be limited to acts of gods, labour conflicts, acts of
							war or civil disruption, governmental regulations imposed after
							the fact, public utility out failures, industry wide disruption,
							shortages of labour or material, or natural disasters.</li>
						<br>

						<li>Toutilise the services of Gourmet7, the Restaurantwill
							have to access database and features of Gourmet7 through web, or
							downloadable applications on Android phones / tablets. To ensure
							that all advanced features and services are available and
							function smoothly, the Restaurantmustensure that ithas high speed
							internet connectivity at its location and update the app from
							time to time.Gourmet7 will not be held responsible for delay in
							receiving any service notification due to such connectivity
							issues.</li>
						<br>

						<li>Advanced features and services of Gourmet7 will be
							exclusively available on mobiles and tablets running on Android
							though basic features can be accessed through the web.To make the
							best use of our services, we would recommend using a 7” tablet of
							any brand.</li>
						<br>

						<li>In order to avail the services of the Restaurant, a
							Gourmet7 member will have to generate a GPIN each time he visits.
							This GPIN will have to be verified at the outlet through the
							Gourmet7 app. The Restaurant will have complete rights to deny
							special benefits to a Gourmet7 member if he fails to provide the
							GPIN.</li>
						<br>

						<li>Gourmet7 will keep introducing new free and paid services
							from time to time and the availability of the same will be
							communicated. The Restaurant will have the discretion to decide
							whether to subscribe for them.</li>
						<br>

						<li>The Restaurant acknowledges that in the course of
							performance, it will be given access to confidential information
							that is proprietary and confidential to Gourmet7.</li>
						<br>

						<li>The Restaurant shall not use the Confidential Information
							other than to perform its obligations hereunder and will take all
							reasonable measures to safeguard the Confidential Information and
							prevent its unauthorized disclosure to any third party.</li>
						<br>

						<li>The Restaurant shall not be entitled to claim any
							consideration or part of consideration from the monies received
							by Gourmet7 by way of payment in respect to the said membership
							program.</li>
						<br>




					</ol>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label" for="bestdishes"><spring:message
							code="label.name" /><span class="text-danger">*</span> </label>
					<div class="col-md-6">
						<input id="bestdishes" name="agreementBy" class="form-control"
							placeholder="<spring:message code='label.enteryourname'/>"
							type="text" value="<%=user.getFullName()%>">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label" for="offer_validtill"><spring:message
							code="label.agreementdate" /><span class="text-danger">*</span>
					</label>
					<div class="col-md-6">
						<input type="text" id="agreement-datepicker" name="agreementDate"
							style="width: 200px;" class="form-control input-datepicker"
							data-date-format="<spring:message code="label.dateformat" />"
							placeholder="<spring:message code="label.dateformat" />">
					</div>
				</div>
			</div>
		</div>
		
		<div class="form-group form-actions">
			<div class="col-md-8 col-md-offset-4">
				<input disabled="disabled"  class="btn btn-sm btn-warning ui-wizard-content ui-formwizard-button" id="back2" value="Back" type="reset" style="width: 56px;">
				 <input  class="btn btn-sm btn-primary ui-wizard-content ui-formwizard-button save" id="next2" value="Next" type="submit"  style="width: 56px;">  
				 <a class="btn btn-sm btn-primary" href="<%=contexturl %>ManageRestaurant/RestaurantList"> <spring:message code="button.cancel" /> </a>
				
			</div>
		</div>
	</form>
</div>
<div class="form-group">
			<a href="#imagecrop_popup" data-toggle="modal" class="col-md-4 control-label" id="imagecropLink"></a>
</div>
<div id="imagecrop_popup" class="modal fade" tabindex="-1"
	role="dialog" aria-hidden="true">
	<div class="modal-dialog" style="width: 90%;height: 90%">
		<div class="modal-content">
			<div class="modal-body">
					<div class="col-xs-4 text-center">
						<h3><spring:message code="label.croprestimage"/></h3>
					</div>
					<div class="col-xs-4 text-right" style="margin-top: 20px">
						<button type="button" class="btn btn-sm btn-primary save"
							data-dismiss="modal"><spring:message code="label.done"/></button>
					</div>
					<div class="frame" >
						<img id="photo"  style="width:100%"/>
					</div>
					
			</div>
		</div>
	</div>
</div>

<%@include file="../../inc/page_footer.jsp"%>
<%@include file="../../inc/template_scripts.jsp"%>
<script src="<%=contexturl %>resources/js/jquery.imgareaselect.pack.js"></script>
<%-- <script src="<%=contexturl %>resources/js/additional-methods.js"></script> --%>
<script type="text/javascript"
	src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCheQF_yjKacwuVL4_59EO-3BThy85gbu4&sensor=false&region=GB">
	
</script>
<script src="<%=contexturl %>resources/js/custom/featurecontrol.js"></script>
<script type="text/javascript">
	function initialize() {
		var mapOptions = {
			center : new google.maps.LatLng($('#latitude').val(), $(
					'#longitude').val()),
			zoom : 13
		};
		var map = new google.maps.Map(document.getElementById("map-canvas"),
				mapOptions);

		var marker = new google.maps.Marker({
			position : map.getCenter(),
			map : map,
			title : 'Click to zoom',
			draggable : true
		});

		google.maps.event.addListener(map, 'center_changed', function() {
			// 3 seconds after the center of the map has changed, pan back to the
			// marker.

			window.setTimeout(function() {
				$('#latitude').val(marker.getPosition().k);
				$('#longitude').val(marker.getPosition().B);
				map.panTo(marker.getPosition());
			}, 3000);
		});

		google.maps.event.addListener(marker, 'click', function() {
			map.setZoom(13);
			map.setCenter(marker.getPosition());
		});
	}

	google.maps.event.addDomListener(window, 'load', initialize);
</script>

<script type="text/javascript">
	$(document)
			.ready(
					function() {
						Dropzone.autoDiscover = false;
						var i=0;
						$("div#myId")
								.dropzone(
										{
											acceptedFiles: "image/*",
											url : "<%=contexturl %>ManageRestaurant/RestaurantList/UploadRestaurantImage"
										});

						$(".chosen").chosen();
						$(".brand_chosen").chosen();
						$(".currency_chosen").chosen();
						$(".area_chosen").chosen();
						$(".city_chosen").chosen();
						$(".country_chosen").chosen();
						$(".cuisine_chosen").chosen();

						$('#agreement-datepicker').datepicker('setStartDate', new Date());
						$('#agreement-datepicker').datepicker('setEndDate', new Date());
						$('#agreement-datepicker').datepicker('setDate', new Date());

						$('select[name="currency"]').chosen().change(function() {
							var currency = $("#currency_chosen").text();
					        var currencySign = currency.substring(currency.indexOf("(")+1,currency.indexOf(")"));
					        $("#min_currency").html(currencySign);
					        $("#max_currency").html(currencySign);
						});
						$('select[name="country"]').chosen().change(function() {
							var countryId = ($(this).val());
							$.ajax({
								type: "GET",
								async: false,
								url: "<%=contexturl %>city/citybycountryid/"+countryId,
								success: function(data, textStatus, jqXHR){
									var obj = jQuery.parseJSON(data);
									var len=obj.length;
									var myOptions = "<option value>Select City</option>";
									var myAreaOptions = "<option value>Select Area</option>";
									for(var i=0; i<len; i++){
									myOptions += '<option value="'+obj[i].cityId+'">'+obj[i].cityName+'</option>';
									}
									$(".city_chosen").html(myOptions).chosen().trigger("chosen:updated");
									$(".area_chosen").html(myAreaOptions).chosen().trigger("chosen:updated");
								},
								dataType: 'html'
							});

						});
						
						$('select[name="city"]').chosen().change(function() {
							var cityId = ($(this).val());
							$.ajax({
								type: "GET",
								async: false,
								url: "<%=contexturl %>area/areabycityid/"+cityId,
								success: function(data, textStatus, jqXHR){
									var obj = jQuery.parseJSON(data);
									var len=obj.length;
									var myOptions = "<option value>Select Area</option>";
									for(var i=0; i<len; i++){
									myOptions += '<option value="'+obj[i].areaId+'">'+obj[i].areaName+'</option>';
									}
									$(".area_chosen").html(myOptions).chosen().trigger("chosen:updated");
								},
								dataType: 'html'
							});
						});
						
// 						function changeCity(areaId) {
// 							var areainfo = $('select[name="area"]').find(
// 									'option[value="' + areaId + '"]')
// 									.attr("id").replace("city_", "");
// 							var areainfolist = areainfo.split("_");
// 							var cityId = areainfolist[0];
// 							$('#latitude').val(areainfolist[1]);
// 							$('#longitude').val(areainfolist[2]);
// 							initialize();
// 							$('select[name="city"]').find(
// 									'option[value="' + cityId + '"]').attr(
// 									"selected", true);
// 							$('select[name="city"]').trigger("chosen:updated");
// 							changeCountry(cityId);
// 						}

						$('select[name="company"]').chosen().change( function() {
							var companyId = ($(this).val());
							$.ajax({
								type: "GET",
								async: false,
								url: "<%=contexturl %>brand/BrandMaster/findAllActiveByCompanyId/"+companyId,
								success: function(data, textStatus, jqXHR){
									var obj = jQuery.parseJSON(data);
									var len=obj.length;
									var myOptions = "<option value>Select Brand</option>";
									for(var i=0; i<len; i++){
									myOptions += '<option value="'+obj[i].brandId+'">'+obj[i].brandName+'</option>';
									}
									$(".brand_chosen").html(myOptions).chosen().trigger("chosen:updated");
								},
								dataType: 'html'
							});
						});
	
						$("#next2").click(function(){
							 unsaved=false;
						});
						
						$("#Restaurant_form")
								.formwizard(
										{	
											disableUIStyles : !0,
											validationEnabled : !0,
											validationOptions : {
												
												errorClass : "help-block animation-slideDown",
												errorElement : "div",
												errorPlacement : function(e, a) {a.parents(".form-group > div").append(e);},
												highlight : function(e) {$(e).closest(".form-group").removeClass("has-success has-error").addClass("has-error");
													if(i==0){
														$('html, body').animate({ scrollTop: $("#restaurant_Name").offset().top },500);
													}
													i++;
												},
												success : function(e) {e.closest(".form-group").removeClass("has-success has-error")},
												rules : {
													name : {
														required : !0,
														maxlength : 75,
													},
													displayName : {
														required : !0,
														maxlength : 75,
													},
													company : {
														required : !0
													},
													cuisine : {
														required : !0
													},
													currency:{
														required : !0
													},
													brandId : {
														required : !0
													},
													pincode : {
														required : !0,
														maxlength : 10,
														digits : !0
													},
													latitude : {
														required : !0,
														maxlength : 75,
														number : !0
													},
													longitude : {
														required : !0,
														maxlength : 75,
														number : !0
													},
													firstName : {
														required : !0,
														maxlength : 75,
													},
													lastName : {
														required : !0,
														maxlength : 75,
													},
													
													addressLine1 : {
														required : !0,
														maxlength : 75,
													},
													addressLine2 : {
														maxlength : 75
													},
													emailId : {
														required : !0,
														maxlength : 75,
														email : !0
													},
													mobile : {
														required : !0,
														phone : !0
													},
													loginId : {
														required : !0,
														maxlength : 75,
														minlength : 6
													},
													maxRange : {
														required : !0,
														digits:  !0,
														greaterThan :'#minRange'
													},
													city : {
														required : !0
													},
													country : {
														required : !0
													},
													area : {
														required : !0
													},
													password : {
														required : !0,
														maxlength : 75,
														minlength : 6
													},
													dMonday : {
														required : !0,
														discount : !0,
														
													},
													dTuesday : {
														required : !0,
														discount : !0,
														
													},
													dWednesday : {
														required : !0,
														discount : !0,
														
													},
													dThursday : {
														required : !0,
														discount : !0,
														
													},
													dFriday : {
														required : !0,
														discount : !0,
														
													},
													dSaturday : {
														required : !0,
														discount : !0,
														
													},
													dSunday : {
														required : !0,
														discount : !0,
														
													},
													cMonday : {
														maxlength : 75
													},
													cTuesday : {
														maxlength : 75
													},
													cWednesday : {
														maxlength : 75
													},
													cThursday : {
														maxlength : 75
													},
													cFriday : {
														maxlength : 75
													},
													cSaturday : {
														maxlength : 75
													},
													cSunday : {
														maxlength : 75
													},
													
													commentMonday : {
														maxlength : 75
													},
													commentTuesday : {
														maxlength : 75
													},
													commentWednesday : {
														maxlength : 75
													},
													commentThursday : {
														maxlength : 75
													},
													commentFriday : {
														maxlength : 75
													},
													commentSaturday : {
														maxlength : 75
													},
													commentSunday : {
														maxlength : 75
													},
													confirmPassword : {
														required : !0,
														equalTo : "#password"
													},
													agreementDate : {
														required : !0
													},
													agreementBy : {
														required : !0
													},
													facebookLink : {
														urlTrim : !0,
														maxlength : 255
													},
													twitterLink : {
														urlTrim : !0,
														maxlength : 255
													},
													keyword:{
														maxlength : 75
													},
													metatag:{
														maxlength : 75
													},
													timing : {
														maxlength : 75
													},
													capacity : {
														maxlength : 75
													},
													averageCostOfTwo : {
														maxlength : 75
													},
													averageNoOfVisitor : {
														maxlength : 75
													},
													bestdishes : {
														maxlength : 75
													},
													story : {
														maxlength : 1000
													},
													telephone1 : {
														required : !0,maxlength : 75,
														phone : !0
													},
													telephone2 : {
														phone : !0
													},
													restaurantEmail : {
														maxlength : 75,
														email : !0
													},
													mobile1 : {
														required : !0,maxlength : 75,
														phone : !0
													},
													mobile2 : {
														phone : !0
													}

												},
												messages : {
													name : {
														required : '<spring:message code="validation.pleaseenterrestaurantname"/>',
														maxlength : '<spring:message code="validation.name75character"/>'
													},
													displayName : {
														required : '<spring:message code="validation.displayname"/>'
													},
													company : {
														required : '<spring:message code="validation.pleaseselectacompany"/>'
													},
													cuisine : {
														required : '<spring:message code="validation.pleaseselectcuisine"/>'
													},
													currency : {
														required : '<spring:message code="validation.currency"/>'
													},
													
													brandId : {
														required : '<spring:message code="validation.pleaseselectbrand"/>'
													},
													pincode : {
														required : '<spring:message code="validation.pleaseenterpincode"/>',
														maxlength : '<spring:message code="validation.pincode75character"/>',
														digits : '<spring:message code="validation.digits"/>'
													},
													city : {
														required : '<spring:message code="validation.selectcity"/>',
													},
													country : {
														required : '<spring:message code="validation.selectcountry"/>',
													},
													area : {
														required : '<spring:message code="validation.selectarea"/>',
													},
													latitude : {
														required : '<spring:message code="validation.latitude"/>',
														number : '<spring:message code="validation.digits"/>',
														maxlength : '<spring:message code="validation.latitude75character"/>'
													},
													longitude : {
														required : '<spring:message code="validation.longitude"/>',
														number : '<spring:message code="validation.digits"/>',
														maxlength : '<spring:message code="validation.longitude75character"/>'
													},
													firstName : {
														required : '<spring:message code="validation.pleaseenterfirstname"/>',
														maxlength : '<spring:message code="validation.firstname75character"/>'
													},
													lastName : {
														required : '<spring:message code="validation.pleaseenterlastname"/>',
														maxlength : '<spring:message code="validation.lastname75character"/>'
													},
													
													addressLine1 : {
														required : '<spring:message code="validation.pleaseenteraddressline1"/>',
														maxlength : '<spring:message code="validation.address"/>'
													},
													addressLine2 : {
														maxlength : '<spring:message code="validation.address"/>'
													},
													emailId : {
														required : '<spring:message code="validation.pleaseenteremailid"/>',
														email : '<spring:message code="validation.pleaseenteravalidemailid"/>',
														maxlength : '<spring:message code="validation.email75character"/>'
													},
													mobile : {
														required : '<spring:message code="validation.pleaseenteramobilenumber"/>',
														phone : '<spring:message code="validation.phone"/>'
													},
													loginId : {
														required : '<spring:message code="validation.pleaseenteraloginid"/>',
														minlength : '<spring:message code="validation.loginidmustbeatleast6character"/>',
														maxlength : '<spring:message code="validation.loginid75character"/>'
													},
													maxRange : {
														required : '<spring:message code="validation.pleaseentermaxvalue"/>',
														digits : '<spring:message code="validation.digits"/>',
													   greaterThan :"Range should be greater than minimum range "
													},
													dMonday : {
														required : '<spring:message code="validation.monday"/>',
														discount : '<spring:message code="validation.discount"/>'
													},
													dTuesday : {
														required : '<spring:message code="validation.tuesday"/>',
														discount : '<spring:message code="validation.discount"/>'
													},
													dWednesday : {
														required : '<spring:message code="validation.wednesday"/>',
														discount : '<spring:message code="validation.discount"/>'
													},
													dThursday : {
														required : '<spring:message code="validation.thursday"/>',
														discount : '<spring:message code="validation.discount"/>'
													},
													dFriday : {
														required : '<spring:message code="validation.friday"/>',
														discount : '<spring:message code="validation.discount"/>'
													},
													dSaturday : {
														required : '<spring:message code="validation.saturday"/>',
														discount : '<spring:message code="validation.discount"/>'
													},
													dSunday : {
														required : '<spring:message code="validation.sunday"/>',
														discount : '<spring:message code="validation.discount"/>',
													},
													
													cMonday : {
														maxlength : '<spring:message code="validation.75character"/>'
													},
													cTuesday : {
														maxlength : '<spring:message code="validation.75character"/>'
													},
													cWednesday : {
														maxlength : '<spring:message code="validation.75character"/>'
													},
													cThursday : {
														maxlength : '<spring:message code="validation.75character"/>'
													},
													cFriday : {
														maxlength : '<spring:message code="validation.75character"/>'
													},
													cSaturday : {
														maxlength : '<spring:message code="validation.75character"/>'
													},
													cSunday : {
														maxlength : '<spring:message code="validation.75character"/>'
													},
													commentMonday : {
														maxlength : '<spring:message code="validation.75character"/>'
													},
													commentTuesday : {
														maxlength : '<spring:message code="validation.75character"/>'
													},
													commentWednesday : {
														maxlength : '<spring:message code="validation.75character"/>'
													},
													commentThursday : {
														maxlength : '<spring:message code="validation.75character"/>'
													},
													commentFriday : {
														maxlength : '<spring:message code="validation.75character"/>'
													},
													commentSaturday : {
														maxlength : '<spring:message code="validation.75character"/>'
													},
													commentSunday : {
														maxlength : '<spring:message code="validation.75character"/>'
													},
													
													password : {
														required : '<spring:message code="validation.pleaseenterapassword"/>',
														minlength : '<spring:message code="validation.passwordmustbeatleast6character"/>'
													},
													confirmPassword : {
														required : '<spring:message code="validation.pleaseconfirmpassword"/>',
														equalTo : '<spring:message code="validation.pleaseenterthesamepasswordasabove"/>'
													},
													agreementDate : {
														required : '<spring:message code="validation.pleaseenteragreementdate"/>'
													},
													agreementBy : {
														required : '<spring:message code="validation.pleaseenteryourname"/>'
													},
													facebookLink : {
														urlTrim : '<spring:message code="validation.pleaseentervalidurl"/>',
														maxlength : '<spring:message code="validation.link255character"/>'
													},
													twitterLink : {
														urlTrim : '<spring:message code="validation.pleaseentervalidurl"/>',
														maxlength : '<spring:message code="validation.link255character"/>'
													},
													keyword:{
														maxlength : '<spring:message code="validation.keyword75character"/>'
													},
													metatag:{
														maxlength : '<spring:message code="validation.metatag75character"/>'
													},
													timing : {
														maxlength : '<spring:message code="validation.timing75character"/>'
													},
													capacity : {
														maxlength : '<spring:message code="validation.capacity75character"/>'
													},
													averageCostOfTwo : {
														maxlength : '<spring:message code="validation.averagecostoftwo"/>'
													},
													averageNoOfVisitor : {
														maxlength : '<spring:message code="validation.averagenoofvisitor"/>'
													},
													bestdishes : {
														maxlength : '<spring:message code="validation.bestdishes"/>'
													},
													story : {
														maxlength : '<spring:message code="validation.story"/>'
													},
													telephone1 : {
														required :'<spring:message code="validation.pleaseenteratelephonenumber"/>',
														phone :'<spring:message code="validation.phone"/>',
															maxlength :'<spring:message code="validation.telephone75character"/>'
													},
													telephone2 : {
														phone : '<spring:message code="validation.phone"/>'
													},
													restaurantEmail : {
														maxlength : '<spring:message code="validation.email75character"/>',
														email : '<spring:message code="validation.pleaseenteravalidemailid"/>'
													},
													mobile1 : {
														required :'<spring:message code="validation.pleaseenteramobilenumber"/>',
														phone :'<spring:message code="validation.phone"/>',
														maxlength :'<spring:message code="validation.mobile75character"/>'
													},
													mobile2 : {
														phone : '<spring:message code="validation.phone"/>'
													}
												},
												
											}
										});

								$("#next2").click(function(){
								 i=0;
							});

						$("#availability").click(
								function() {
									var data = $("#login").val();
									data = '<%=propvalue.restaurantLoginPrefix%>'+data;
									$("#notavailable").css("display", "none");
									$("#loading").css("display", "inline");
									$("#available").css("display", "none");
									if (data != "") {
										$.getJSON("<%=contexturl%>ValidateLogin", {
											loginId : data
										}, function(data) {
											if (data == "exist") {
												$("#notavailable").css(
														"display", "inline");
												$("#loading").css("display", "none");
											} else if (data == "available") {
												$("#available").css("display",
														"inline");
												$("#loading").css("display", "none");
											}
										});
									}
								});
						
						
						$("#brand_name").change(function(){
							var id =	$("#brand_name").val();
							$.ajax({
								type: "GET",
								async: false,
								url: "<%=contexturl %>/GetBrandFeatures/"+id,				
								success: function(data, textStatus, jqXHR){
									$('#featurecontrol').html(data);
									$('.feature-control').change(function() {
										if(this.checked){
											$("#message_link" ).trigger( "click" );
										}
										else{
											$("#current_feature").val(this.id);
											$("#warning_message_link" ).trigger( "click" );
										}
									});
		
									$("#no_deselect").click(function(){
										var currentFetaure = $("#current_feature").val();
										$("#"+currentFetaure).prop( "checked", true );
									});
								},
								dataType: 'html'
							});
						});

					});
	
	function emailVerification(){
		var data = $("#email").val();
		$("#emailnotavailable").css("display","none");
		$("#emailloading").css("display", "inline");
		$("#emailavailable").css("display","none");
		if( data != ""){
			$.getJSON("<%=contexturl%>ValidateEmailId",{emailId: data}, function(data){
				if(data == "exist"){
					$("#emailnotavailable").css("display","inline");
					$("#emailloading").css("display", "none");
				}
				else if(data == "available"){
					$("#emailavailable").css("display","inline");
					$("#emailloading").css("display", "none");
				}
			});
		}
	}
</script>
<script type="text/javascript">
	function OpenFileDialog(){
		document.getElementById("profileImageChooser").click();
	}
	
// 	function changePhoto(){
// 		if($('#imageChooser').val() != ""){			
// 			var imageName = document.getElementById("imageChooser").value;
// 			var ext1 = imageName.substring(imageName.lastIndexOf("."));
// 			var ext = ext1.toLowerCase();
// 			if(ext == '.jpg' || ext == '.jpeg' || ext == '.png' || ext == 'gif' || ext == 'bmp' || ext == 'tiff')
// 				$('#changePhoto').submit();		
// 			else
// 				alert("Please select only image file");		
// 		}
// 	}
    function showimages(files) {
        for (var i = 0, f; f = files[i]; i++) {
           
                       
            var reader = new FileReader();
            reader.onload = function (evt) {
            	$( "#profileImage" ).remove();
            	$( "#preview" ).remove();
            	
                var img = '<div id="preview" style="width: 200px; height: 100px; overflow: hidden;" class="col-md-6" ><img border="0" src="' + evt.target.result + '" title="Click here to change photo"  onclick="OpenFileDialog();"  id="profileImage"/></div>';
                $('#profilePic').append(img);
                $('#photo').attr("src",evt.target.result);
                $('#preImage').attr("src",evt.target.result);
                
            }
            reader.readAsDataURL(f);
        }
        $("#imagecropLink").trigger("click");
    }

    $('#profileImageChooser').change(function (evt) {
        showimages(evt.target.files);
    });
</script>
<script type="text/javascript">
function preview(img, selection) {
    if (!selection.width || !selection.height)
        return;
    
    var scaleX = 200 / selection.width;
    var scaleY = 100 / selection.height;

    $('#preview img').css({
        width: Math.round(scaleX * $("#photo").width()),
        height: Math.round(scaleY * $("#photo").height()),
        marginLeft: -Math.round(scaleX * selection.x1),
        marginTop: -Math.round(scaleY * selection.y1)
    });


    $('#profileX1').val(selection.x1);
    $('#profileY1').val(selection.y1);
    $('#profileX2').val(selection.x2);
    $('#profileY2').val(selection.y2);
    $('#profileW').val(selection.width);
    $('#profileH').val(selection.height);    
}

$(function () {
	
    $('#photo').imgAreaSelect({ aspectRatio: '2:1', handles: true,
        fadeSpeed: 200, onSelectChange: preview });
});
</script>
<%@include file="../../inc/template_end.jsp"%>