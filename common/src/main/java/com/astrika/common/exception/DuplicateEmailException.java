package com.astrika.common.exception;

import com.astrika.kernel.exception.BusinessException;
import com.astrika.kernel.exception.CustomException;


public class DuplicateEmailException extends BusinessException{
	
	
	public DuplicateEmailException() {
		super(CustomException.DUPLICATE_EMAIL.getCode());
	}

	public DuplicateEmailException(String errorCode){
		super(errorCode);
	}
}
