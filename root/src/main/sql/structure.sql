
--  17-6-2015
--  Priyanka Jagdale
-- making qrtz empty
drop table if exists `QRTZ_CALENDARS`;
drop table if exists `QRTZ_FIRED_TRIGGERS`;
drop table if exists `QRTZ_JOB_LISTENERS`;
drop table if exists `QRTZ_LOCKS`;
drop table if exists `QRTZ_PAUSED_TRIGGER_GRPS`;
drop table if exists `QRTZ_SCHEDULER_STATE`;
drop table if exists `QRTZ_BLOB_TRIGGERS`;
drop table if exists `QRTZ_CRON_TRIGGERS`;
drop table if exists `QRTZ_SIMPLE_TRIGGERS`;
drop table if exists `QRTZ_TRIGGER_LISTENERS`;
drop table if exists `QRTZ_TRIGGERS`;
drop table if exists `QRTZ_JOB_DETAILS`;


/*Table structure for table `qrtz_calendars` */

drop table if exists `QRTZ_CALENDARS`;

CREATE TABLE `QRTZ_CALENDARS` (
  `CALENDAR_NAME` varchar(200) NOT NULL default '',
  `CALENDAR` blob NOT NULL,
  PRIMARY KEY  (`CALENDAR_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `qrtz_calendars` */



/*Table structure for table `qrtz_fired_triggers` */

drop table if exists `QRTZ_FIRED_TRIGGERS`;

CREATE TABLE `QRTZ_FIRED_TRIGGERS` (
  `ENTRY_ID` varchar(95) NOT NULL default '',
  `TRIGGER_NAME` varchar(200) NOT NULL default '',
  `TRIGGER_GROUP` varchar(200) NOT NULL default '',
  `IS_VOLATILE` char(1) NOT NULL default '',
  `INSTANCE_NAME` varchar(200) NOT NULL default '',
  `FIRED_TIME` bigint(13) NOT NULL default '0',
  `PRIORITY` int(11) NOT NULL default '0',
  `STATE` varchar(16) NOT NULL default '',
  `JOB_NAME` varchar(200) default NULL,
  `JOB_GROUP` varchar(200) default NULL,
  `IS_STATEFUL` char(1) default NULL,
  `REQUESTS_RECOVERY` char(1) default NULL,
  PRIMARY KEY  (`ENTRY_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `qrtz_fired_triggers` */

/*Table structure for table `qrtz_job_details` */

drop table if exists `QRTZ_JOB_DETAILS`;

CREATE TABLE `QRTZ_JOB_DETAILS` (
  `JOB_NAME` varchar(200) NOT NULL default '',
  `JOB_GROUP` varchar(200) NOT NULL default '',
  `DESCRIPTION` varchar(250) default NULL,
  `JOB_CLASS_NAME` varchar(250) NOT NULL default '',
  `IS_DURABLE` char(1) NOT NULL default '',
  `IS_VOLATILE` char(1) NOT NULL default '',
  `IS_STATEFUL` char(1) NOT NULL default '',
  `REQUESTS_RECOVERY` char(1) NOT NULL default '',
  `JOB_DATA` blob,
  PRIMARY KEY  (`JOB_NAME`,`JOB_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `qrtz_job_details` */

/*Table structure for table `qrtz_job_listeners` */

drop table if exists `QRTZ_JOB_LISTENERS`;

CREATE TABLE `QRTZ_JOB_LISTENERS` (
  `JOB_NAME` varchar(200) NOT NULL default '',
  `JOB_GROUP` varchar(200) NOT NULL default '',
  `JOB_LISTENER` varchar(200) NOT NULL default '',
  PRIMARY KEY  (`JOB_NAME`,`JOB_GROUP`,`JOB_LISTENER`),
  KEY `JOB_NAME` (`JOB_NAME`,`JOB_GROUP`),
  CONSTRAINT `qrtz_job_listeners_ibfk_1` FOREIGN KEY (`JOB_NAME`, `JOB_GROUP`) REFERENCES `QRTZ_JOB_DETAILS` (`JOB_NAME`, `JOB_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `qrtz_job_listeners` */

/*Table structure for table `qrtz_locks` */

drop table if exists `QRTZ_LOCKS`;

CREATE TABLE `QRTZ_LOCKS` (
  `LOCK_NAME` varchar(40) NOT NULL default '',
  PRIMARY KEY  (`LOCK_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `qrtz_locks` */

insert into `QRTZ_LOCKS` values ('CALENDAR_ACCESS'),('JOB_ACCESS'),('MISFIRE_ACCESS'),('STATE_ACCESS'),('TRIGGER_ACCESS');

/*Table structure for table `qrtz_paused_trigger_grps` */

drop table if exists `QRTZ_PAUSED_TRIGGER_GRPS`;

CREATE TABLE `QRTZ_PAUSED_TRIGGER_GRPS` (
  `TRIGGER_GROUP` varchar(200) NOT NULL default '',
  PRIMARY KEY  (`TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `qrtz_paused_trigger_grps` */

/*Table structure for table `qrtz_scheduler_state` */

drop table if exists `QRTZ_SCHEDULER_STATE`;

CREATE TABLE `QRTZ_SCHEDULER_STATE` (
  `INSTANCE_NAME` varchar(200) NOT NULL default '',
  `LAST_CHECKIN_TIME` bigint(13) NOT NULL default '0',
  `CHECKIN_INTERVAL` bigint(13) NOT NULL default '0',
  PRIMARY KEY  (`INSTANCE_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `qrtz_scheduler_state` */


/*Table structure for table `qrtz_triggers` */

drop table if exists `QRTZ_TRIGGERS`;

CREATE TABLE `QRTZ_TRIGGERS` (
  `TRIGGER_NAME` varchar(200) NOT NULL default '',
  `TRIGGER_GROUP` varchar(200) NOT NULL default '',
  `JOB_NAME` varchar(200) NOT NULL default '',
  `JOB_GROUP` varchar(200) NOT NULL default '',
  `IS_VOLATILE` char(1) NOT NULL default '',
  `DESCRIPTION` varchar(250) default NULL,
  `NEXT_FIRE_TIME` bigint(13) default NULL,
  `PREV_FIRE_TIME` bigint(13) default NULL,
  `PRIORITY` int(11) default NULL,
  `TRIGGER_STATE` varchar(16) NOT NULL default '',
  `TRIGGER_TYPE` varchar(8) NOT NULL default '',
  `START_TIME` bigint(13) NOT NULL default '0',
  `END_TIME` bigint(13) default NULL,
  `CALENDAR_NAME` varchar(200) default NULL,
  `MISFIRE_INSTR` smallint(2) default NULL,
  `JOB_DATA` blob,
  PRIMARY KEY  (`TRIGGER_NAME`,`TRIGGER_GROUP`),
  KEY `JOB_NAME` (`JOB_NAME`,`JOB_GROUP`),
  CONSTRAINT `qrtz_triggers_ibfk_1` FOREIGN KEY (`JOB_NAME`, `JOB_GROUP`) REFERENCES `QRTZ_JOB_DETAILS` (`JOB_NAME`, `JOB_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `qrtz_triggers` */

/*Table structure for table `qrtz_blob_triggers` */

drop table if exists `QRTZ_BLOB_TRIGGERS`;

CREATE TABLE `QRTZ_BLOB_TRIGGERS` (
  `TRIGGER_NAME` varchar(200) NOT NULL default '',
  `TRIGGER_GROUP` varchar(200) NOT NULL default '',
  `BLOB_DATA` blob,
  PRIMARY KEY  (`TRIGGER_NAME`,`TRIGGER_GROUP`),
  KEY `TRIGGER_NAME` (`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `qrtz_blob_triggers_ibfk_1` FOREIGN KEY (`TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `QRTZ_TRIGGERS` (`TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `qrtz_blob_triggers` */
/*Table structure for table `qrtz_cron_triggers` */

drop table if exists `QRTZ_CRON_TRIGGERS`;

CREATE TABLE `QRTZ_CRON_TRIGGERS` (
  `TRIGGER_NAME` varchar(200) NOT NULL default '',
  `TRIGGER_GROUP` varchar(200) NOT NULL default '',
  `CRON_EXPRESSION` varchar(120) NOT NULL default '',
  `TIME_ZONE_ID` varchar(80) default NULL,
  PRIMARY KEY  (`TRIGGER_NAME`,`TRIGGER_GROUP`),
  KEY `TRIGGER_NAME` (`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `qrtz_cron_triggers_ibfk_1` FOREIGN KEY (`TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `QRTZ_TRIGGERS` (`TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `qrtz_cron_triggers` */
/*Table structure for table `qrtz_simple_triggers` */

drop table if exists `QRTZ_SIMPLE_TRIGGERS`;

CREATE TABLE `QRTZ_SIMPLE_TRIGGERS` (
  `TRIGGER_NAME` varchar(200) NOT NULL default '',
  `TRIGGER_GROUP` varchar(200) NOT NULL default '',
  `REPEAT_COUNT` bigint(7) NOT NULL default '0',
  `REPEAT_INTERVAL` bigint(12) NOT NULL default '0',
  `TIMES_TRIGGERED` bigint(10) NOT NULL default '0',
  PRIMARY KEY  (`TRIGGER_NAME`,`TRIGGER_GROUP`),
  KEY `TRIGGER_NAME` (`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `qrtz_simple_triggers_ibfk_1` FOREIGN KEY (`TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `QRTZ_TRIGGERS` (`TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `qrtz_simple_triggers` */

/*Table structure for table `qrtz_trigger_listeners` */

drop table if exists `QRTZ_TRIGGER_LISTENERS`;

CREATE TABLE `QRTZ_TRIGGER_LISTENERS` (
  `TRIGGER_NAME` varchar(200) NOT NULL default '',
  `TRIGGER_GROUP` varchar(200) NOT NULL default '',
  `TRIGGER_LISTENER` varchar(200) NOT NULL default '',
  PRIMARY KEY  (`TRIGGER_NAME`,`TRIGGER_GROUP`,`TRIGGER_LISTENER`),
  KEY `TRIGGER_NAME` (`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `qrtz_trigger_listeners_ibfk_1` FOREIGN KEY (`TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `QRTZ_TRIGGERS` (`TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*Data for the table `qrtz_trigger_listeners` */
