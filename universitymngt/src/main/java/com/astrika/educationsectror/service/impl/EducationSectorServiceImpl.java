package com.astrika.educationsectror.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.astrika.educationsector.repository.EducationSectorRepository;
import com.astrika.educationsector.service.EducationSectorService;

@Service
public class EducationSectorServiceImpl implements EducationSectorService{


	@Autowired
	private EducationSectorRepository educationSectorRepository;

	
	public List<Object[]> getAllSector() {
		return educationSectorRepository.getAllSector();
	}
	
}
