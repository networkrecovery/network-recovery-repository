package com.astrika.common.service;

import java.util.List;

import com.astrika.common.exception.DuplicateCurrencyException;
import com.astrika.common.exception.NoSuchCurrencyException;
import com.astrika.common.model.CurrencyMaster;

/**
 * Service interface for Currency.
 * 
 * @author Rohit
 */
public interface CurrencyService {

	CurrencyMaster save(String currencyName, String currencySign,
			String currencyCode) throws DuplicateCurrencyException;

	CurrencyMaster delete(long currencyId) throws NoSuchCurrencyException;

	CurrencyMaster restore(long currencyId) throws NoSuchCurrencyException;

	CurrencyMaster update(long currencyId, String currencyName,
			String currencySign, String currencyCode)
			throws NoSuchCurrencyException;

	CurrencyMaster findByName(String currencyName);

	List<CurrencyMaster> findByInActive(int start, int end);

	List<CurrencyMaster> findByActive(int start, int end);

	CurrencyMaster findById(long currencyId) throws NoSuchCurrencyException;

	List<CurrencyMaster> findAllByActive();

	List<CurrencyMaster> findAllByInActive();
	
	CurrencyMaster findByCode(String currencyCode);

}
