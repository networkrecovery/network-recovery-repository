<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/template_start.jsp"%>
<%@include file="../../inc/page_head.jsp"%>
<link rel="stylesheet" type="text/css" href="<%=contexturl %>resources/css/jquery.cleditor.css" />
<style>
.cleditorToolbar {
    background: url('<%=contexturl %>resources/img/cleditor/toolbar.gif') repeat
}

.cleditorButton {
    float: left;
    width: 24px;
    height: 24px;
    margin: 1px 0 1px 0;
    background: url('<%=contexturl %>resources/img/cleditor/buttons.gif')
}

.cleditorMain {
    height: 250px !important
}
</style>
<div id="page-content">
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="fa fa-map-marker"></i>
                <spring:message code="heading.course" />
                <br> <small><spring:message
                        code="heading.coursedetails" /></small>
            </h1>
            <span id="errorMsg"></span>
            <c:if test="${!empty success}">
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert"
                        aria-hidden="true">x</button>
                    <h4>
                        <i class="fa fa-check-circle"></i> <spring:message code="label.success"/>
                    </h4>
                    <spring:message code="${success}" />
                </div>
            </c:if>

            <c:if test="${!empty error}">
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert"
                        aria-hidden="true">�</button>
                    <h4>
                        <i class="fa fa-times-circle"></i> <spring:message code="label.error"/>
                    </h4>
                    <spring:message code="${error}" />
                </div>
            </c:if>
        </div>
    </div>
    <ul class="breadcrumb breadcrumb-top">
        <li><spring:message code="heading.course" /></li>
    </ul>
    <div class = "row">
    <div class="col-md-6">
        <div class="col-md-12">
                <div class="block">
                    <div class="block-title">
                        <h2>
                            <strong><spring:message code="heading.courseinfo" /></strong>
                        </h2>
                    </div>
        
        <form action="<%=contexturl %>ManageCourse/CourseList/UpdateCourse" method="post" class="form-horizontal ui-formwizard"
        id="Course_form" enctype="multipart/form-data">
            
                    <input id="courseId" name="courseId" type="hidden" value="${course.courseId}">
                    <input id="confirmation" name="confirmation" type="hidden" value="false">
                    <input id="publish" name="publish" type="hidden" value="false">
            
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="course_Name"><spring:message
                                code="label.coursename" /><span class="text-danger">*</span> </label>
                        <div class="col-md-7">

                            <input id="courseName" name="courseName" class="form-control"
                                placeholder="<spring:message code='label.coursename'/>.."
                                type="text" value="${course.courseName}" disabled>
                        </div>
                    </div>
                    
                
                            <c:if test="${course.previewImage!=null}">
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-daterange1"><spring:message code="label.previewimage"/>
                                        </label>
                                    <div class="col-md-7" >
                                        <img src="<%=contexturl %>${course.previewImage.imagePath}" width="150px"  />
                                    </div>
                                </div>
                                
                                
                            </c:if>
                            
                    <div class="form-group">
                    <label class="col-md-3 control-label" for="course_duration"><spring:message
                            code="label.introductoryvideo" /><span class="text-danger">*</span>
                    </label>
                    <div class="col-md-7">

                        <input id="introductoryVideo" name="introductoryVideo"
                            class="form-control"
                            placeholder="<spring:message code='label.introductoryvideo'/>.."
                            type="text" value="${course.introductoryVideo}" disabled>
                    </div>
                </div>
                    

                <div class="form-group" id="LanguageDiv">
                    <label class="col-md-3 control-label" for="countryFrom_Name"><spring:message
                            code="label.languagename" /><span class="text-danger">*</span></label>
                    <div class="col-md-7">
                        <select multiple class="Language_chosen" style="width: 400px;" id="fromLanguageId"
                            name="chosenLanguageId"
                            data-placeholder="<spring:message code='label.chooseLanguage' /> " disabled>
                            <option value=""></option>
                            <c:forEach items="${languageList}" var="language">
                                        <c:set var="present" value="false" />
                                        <c:forEach items="${languages}" var="lang">
                                            <c:if test="${language.languageId eq lang.languageId}">
                                                <c:set var="present" value="true" />
                                            </c:if>
                                        </c:forEach>
                                        <option value="${language.languageId}"
                                            <c:if test="${present}">    selected="selected" </c:if>>
                                            ${language.languageName}</option>
                                
                            </c:forEach>
                        </select>
                    </div>

                </div>
                
                <div class="form-group" id="CategoryDiv">
                    <label class="col-md-3 control-label" for="countryFrom_Name"><spring:message
                            code="label.coursecategoryname" /><span class="text-danger">*</span></label>
                    <div class="col-md-7">
                        <select multiple class="Category_chosen" style="width: 400px;" id="fromCategoryId"
                            name="chosenCategoryId"
                            data-placeholder="<spring:message code='label.choosecoursecategory' />" disabled>
                            <option value=""></option>
                            <c:forEach items="${categoryList}" var="category">
                                        <c:set var="present" value="false" />
                                        <c:forEach items="${categories}" var="categories">
                                            <c:if test="${category.categoryId eq categories.categoryId}">
                                                <c:set var="present" value="true" />
                                            </c:if>
                                        </c:forEach>
                                        <option value="${category.categoryId}"
                                            <c:if test="${present}">    selected="selected" </c:if>>
                                            ${category.categoryName}</option>
                            </c:forEach>
                        </select>
                    </div>

                </div>
                <div class="form-group" id="InstructorDiv">
                    <label class="col-md-3 control-label" for="countryFrom_Name"><spring:message
                            code="heading.instructor" /></label>
                    <div class="col-md-7">
                        <select multiple class="Instructor_chosen" style="width: 400px;" id="chosenInstructorId"
                            name="chosenInstructorId"
                            data-placeholder="<spring:message code='label.chooseinstructor' />" disabled>
                            <option value=""></option>
                            <c:forEach items="${instructorList}" var="instructor">
                            <c:set var="present" value="false" />
                                        <c:forEach items="${instructors}" var="instructors">
                                            <c:if test="${instructor.user.userId eq instructors.user.userId}">
                                                <c:set var="present" value="true" />
                                            </c:if>
                                            
                                        </c:forEach>
                                        <option value="${instructor.user.userId}"
                                            <c:if test="${present}">    selected="selected" </c:if>>
                                            ${instructor.user.firstName} ${ instructor.user.lastName}</option>
                            </c:forEach>
                        </select>
                    </div>

                </div>
                
                <div class="form-group">
                <label class="col-md-3 control-label" for="courseType"><spring:message
                        code="label.coursetype" /><span class="text-danger">*</span></label>
                <div class="col-md-7">
                    <input id="courseType" name="courseType" class="form-control"
                        placeholder="<spring:message code="label.coursetype"/>.." value="${course.courseType}" type="text" disabled>
                </div>
            </div>
                
                    
                    <div class="form-group">
                    <label class="col-md-3 control-label" for="offer_termsCondition"><spring:message
                            code="label.aboutcourse" /><span class="text-danger">*</span></label>
                    <div class="col-md-9">
                        <textarea id="aboutCourse" name="aboutCourse" disabled>${course.aboutCourse}</textarea>
                        <label style="color: #e74c3c;" id="termsError"></label>
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-md-3 control-label" for="offer_termsCondition"><spring:message
                            code="label.objective" /><span class="text-danger">*</span></label>
                    <div class="col-md-9">
                        <textarea id="objective" name="objective" disabled>${course.objective}</textarea>
                        <label style="color: #e74c3c;" id="termsError"></label>
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-md-3 control-label" for="offer_termsCondition"><spring:message
                            code="label.whythis" /><span class="text-danger">*</span></label>
                    <div class="col-md-9">
                        <textarea id="whythis" name="whythis" disabled>${course.whyThis}</textarea>
                        <label style="color: #e74c3c;" id="termsError"></label>
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-md-3 control-label" for="offer_termsCondition"><spring:message
                            code="label.syllabus" /><span class="text-danger">*</span></label>
                    <div class="col-md-9">
                        <textarea id="syllabus" name="syllabus" disabled>${course.syllabus}</textarea>
                        <label style="color: #e74c3c;" id="termsError"></label>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label" for="course_duration"><spring:message
                            code="label.courseduration" /><span class="text-danger">*</span>
                    </label>
                    <div class="col-md-7">

                        <input id="courseDuration" name="courseDuration"
                            class="form-control"
                            placeholder="<spring:message code='label.entercoursedurationinnumber'/>.."
                            type="text" value="${course.courseDuration}" disabled>
                    </div>
                </div>
                
            <div class="form-group form-actions">
            
            
                <div class="col-md-9 col-md-offset-4">
                        <a class="btn btn-sm btn-primary" href="<%=contexturl %>ManageCourse/CourseList/"><i ></i>
                        <spring:message code="button.back" /> </a>
                </div>
            </div>
            
    </form>
    </div>
    </div>
    </div>
        <div class="col-md-6">
            
                <div class="block">
                    <div class="block-title">
                        <h2>
                            <strong><spring:message code="heading.courseinviteresult" /></strong>
                        </h2>
                    </div>      
                    <div class="table-responsive">
                    <table class="table table-vcenter table-condensed table-bordered datatable">
                        <thead>
                            <tr>
                                <th class="text-center"><spring:message code="label.id" /></th>
                                <th class="text-center"><spring:message code="label.instructorname" /></th>
                                <th class="text-center"><spring:message code="label.instructorstatus" /></th>
                            </tr>
                        </thead>
                        <tbody id="tbody">
                            <c:forEach var="currentCourseInstructor" items="${courseInstructorList}"
                                varStatus="count">
        
                                <tr>
                                    <td class="text-center">${count.count}</td>
                                    <td class="text-left"><c:out
                                            value="${currentCourseInstructor.instructor.user.firstName}  ${currentCourseInstructor.instructor.user.lastName}" /></td>
                                    <td class="text-right"><c:out
                                            value="${currentCourseInstructor.status}" /></td>
                                </tr>
                            </c:forEach>
        
                        </tbody>
                    </table>
                    </div>
                    <div class="col-md-offset-5">
                        <c:choose>
                            <c:when test="${course.status eq status3}">
                            </c:when>
                            <c:otherwise>
                                <a href="#publish_course_pop" data-toggle="modal"
                                    onclick="publishCourse(${id})" class="btn btn-sm btn-primary"><spring:message
                                        code="button.publish" /> </a>
                            </c:otherwise>
                        </c:choose>
                        <a class="btn btn-sm btn-primary "
                            href="<%=contexturl%>ManageCourse/CourseList/"> <spring:message
                                code="button.back" />
                        </a>

                    </div>

                </div>
                
                        <div id="publish_course_pop" class="modal fade" tabindex="-1"
                            role="dialog" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-body">
                                        <form
                                            action="<%=contexturl%>ManageCourse/CourseList/ViewCourse"
                                            method="post" class="form-horizontal form-bordered"
                                            id="publish_course_Form">
            
                                            <div style="padding: 10px; height: 110px;">
                                                <label><spring:message
                                                        code="validation.doyouwantopublishthiscourse" /></label>
                                                <div class="col-xs-12 text-right">
                                                    <button type="button" class="btn btn-sm btn-default"
                                                        data-dismiss="modal">
                                                        <spring:message code="label.no" />
                                                    </button>
                                                    <div id="publish_course" class="btn btn-sm btn-primary">
                                                        <spring:message code="label.yes" />
                                                    </div>
                                                </div>
                                            </div>
                                            <input type="hidden" name="id" id="publishcourseId">
                                            <input type="hidden" name="publish" value="true">
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
        </div>

        <div class="col-md-6">
            <div class="block">
                <div class="block-title">
                    <h2>
                        <strong><spring:message code="heading.coursebatchlist" /></strong>

                    </h2>
                </div>
        <div class="block full  gridView" >
        <div class="block-title">
            <h2>
                <strong><spring:message code="heading.activecoursebatch" /></strong>
            </h2>
        </div>
        <a href="<%=contexturl %>ManageCourseBatch/CourseBatchList/AddCourseBatch" id="addCourseBatch" class="btn btn-sm btn-primary"><spring:message
                code="label.addcoursebatch" /></a>
        <div class="table-responsive">
            <table id="active-datatable"
                class="table table-vcenter table-condensed table-bordered">
                <thead>
                    <tr>
                        <th class="text-center"><spring:message code="label.id" /></th>                     
<%--                        <th class="text-center"><spring:message code="label.coursename" /></th> --%>
                        <th class="text-center"><spring:message code="label.startdate" /></th>
                        <th class="text-center"><spring:message code="label.enddate" /></th>
                        <th class="text-center"><spring:message code="label.batchstatus" /></th>
                        <th class="text-center"><spring:message code="label.actions" /></th>
                    </tr>
                </thead>
                <tbody id="tbody"> 
                    <c:forEach var="currentCourseBatch" items="${activeList}" 
                        varStatus="count"> 

                        <tr> 
                            <td class="text-center">${count.count}</td> 
<%--                            <td class="text-left"><c:out  --%>
<%--                                    value="${currentCourseBatch.course.courseName}" /></td>  --%>
                            <td class="text-left"><fmt:formatDate
                                    pattern="<%=propvalue.specialDateFormat %>" value="${currentCourseBatch.startDate}" /></td> 
                            <td class="text-right"><fmt:formatDate
                                    pattern="<%=propvalue.specialDateFormat %>" value="${currentCourseBatch.endDate}" /></td> 
                            <td class="text-left"><c:out 
                                    value="${currentCourseBatch.status}" /></td>        

                            <td class="text-center">
                                    <div class="btn-group"> 
                                    <c:if test = "${currentCourseBatch.status eq status }" >
                                    <a href="<%=contexturl %>ManageCourseBatch/CourseBatchList/EditCourseBatch?id=${currentCourseBatch.courseBatchId}" 
                                        data-toggle="tooltip" title="Edit" 
                                        class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a> 
                                    </c:if>
                                    
                                    <c:if test = "${(currentCourseBatch.status eq status2) or (currentCourseBatch.status eq status1)}" >
                                    <a href="<%=contexturl %>ManageCourseBatch/CourseBatchList/ViewCourseBatch?id=${currentCourseBatch.courseBatchId}" 
                                        data-toggle="tooltip" title="View" 
                                        class="btn btn-xs btn-default"><i class="fa fa-file-text-o fa-fw"></i></a> 
                                    </c:if>
                                    
                                    <a href="#delete_course_batch_popup" data-toggle="modal" 
                                        onclick="deleteCourseBatch(${currentCourseBatch.courseBatchId})" 
                                        title="Delete" class="btn btn-xs btn-danger"><i 
                                        class="fa fa-times"></i></a> 
                                    </div>
                                    
                                
                            </td> 
                        </tr>
                    </c:forEach>

                </tbody> 
            </table>
        </div>
    </div>
    
    <div class="block full  gridView">
        <div class="block-title">
            <h2>
                <strong><spring:message code="heading.inactivecoursebatch" /></strong>
            </h2>
        </div>
        <div class="table-responsive">
            <table id="inactive-datatable"
                class="table table-vcenter table-condensed table-bordered ">
                <thead>
                    <tr>
                        <th class="text-center"><spring:message code="label.id" /></th>
<%--                        <th class="text-center"><spring:message code="label.coursename" /></th> --%>
                        <th class="text-center"><spring:message code="label.startdate" /></th>
                        <th class="text-center"><spring:message code="label.enddate" /></th>
                        <th class="text-center"><spring:message code="label.batchstatus" /></th>
                        <th class="text-center"><spring:message code="label.actions" /></th>
                    </tr>
                </thead>
                <tbody id="tbody"> 
                    <c:forEach var="currentCourseBatch" items="${inActiveList}" 
                        varStatus="count"> 

                        <tr> 
                            <td class="text-center">${count.count}</td> 
<%--                            <td class="text-left"><c:out  --%>
<%--                                    value="${currentCourseBatch.course.courseName}" /></td>  --%>
                            <td class="text-left"><fmt:formatDate
                                    pattern="<%=propvalue.specialDateFormat %>" value="${currentCourseBatch.startDate}" /></td> 
                            <td class="text-right"><fmt:formatDate
                                    pattern="<%=propvalue.specialDateFormat %>" value="${currentCourseBatch.endDate}" /></td> 
                            <td class="text-left"><c:out 
                                    value="${currentCourseBatch.status}" /></td> 
                            <td class="text-center"> 
                                <div class="btn-group"> 
                                    <a href="#restore_Course_Batch_popup" data-toggle="modal"
                                        onclick="restoreCourseBatch(${currentCourseBatch.courseBatchId})" 
                                        title="Restore" class="btn btn-xs btn-danger"><i
                                        class="fa fa-times"></i> </a> 
                                </div> 
                            </td> 
                        </tr> 
                    </c:forEach> 
                </tbody> 
            </table>
        </div>
    </div>

            </div>
        </div>

        <%@include file="../../inc/page_footer.jsp"%>
<%@include file="../../inc/template_scripts.jsp"%>
<script type="text/javascript" src="<%=contexturl %>resources/js/jquery.cleditor.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $(".Language_chosen").data("placeholder","Select Language From...").chosen();
    $(".Category_chosen").data("placeholder","Select Category From...").chosen();
    $(".Instructor_chosen").data("placeholder","Select Instructor From...").chosen();
    $(".chosenTime").data("placeholder","Select Time Zone From...").chosen();
    $("#aboutCourse").cleditor(); 
    $("#objective").cleditor();
    $("#whythis").cleditor();
    $("#syllabus").cleditor();
    
    
    
});
</script>
<script src="<%=contexturl%>resources/js/pages/activeTablesDatatables.js"></script>
<script src="<%=contexturl%>resources/js/pages/inactiveTablesDatatables.js"></script>
<script>$(function(){ ActiveTablesDatatables.init(4); });</script>
<script>$(function(){ InActiveTablesDatatables.init(4); });</script>
<script src="<%=contexturl%>resources/js/pages/tablesDatatables.js"></script>
<script>$(function(){ TablesDatatables.init(2); });</script>

<script type="text/javascript">
    var selectedId = 0;
    
        $("#publish_course").click(function(){
            
            $("#publishcourseId").val(selectedId);
            $("#publish_course_Form").submit();
        });
        
    
    function publishCourse(id){
        selectedId = id;
    };
    
</script>

<%@include file="../../inc/template_end.jsp"%>