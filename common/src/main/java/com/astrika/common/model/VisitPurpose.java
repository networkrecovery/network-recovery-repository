package com.astrika.common.model;

public enum VisitPurpose {
	
	BUSINESS(1),
	FAMILY(2),
	FRIENDS(3),
	ROMANTIC(4),
	SOLO(5);
	private final int id;
	
	private VisitPurpose(int id){
		this.id = id;
	}

	public int getId() {
		return id;
	}
	
	public static VisitPurpose fromInt(int id){
		switch (id) {
			case 1:
				return BUSINESS;
			case 2:
				return FAMILY;
			case 3:
				return FRIENDS;
			default:
				return from(id);
		}
	}
	
	public static VisitPurpose from(int id){
        switch (id) {
            case 4:
                return ROMANTIC;
            case 5:
                return SOLO;
            default:
                return null;
        }
    }

}
