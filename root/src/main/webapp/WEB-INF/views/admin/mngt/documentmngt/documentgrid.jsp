<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/template_start.jsp"%>
<%@include file="../../inc/page_head.jsp"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>




<div id="page-content">
	<div class="content-header">
		<div class="header-section">
			<h1>
<!-- 				<i class="fa fa-map-marker"></i> -->
				<spring:message code="heading.documents" />
				<br> <small><spring:message
						code="heading.specifylistofdocuments" /> </small>
			</h1>
			<c:if test="${!empty success}">
				<div class="alert alert-success alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-check-circle"></i>
						<spring:message code="label.success" />
					</h4>
					<spring:message code="${success}" />
				</div>
			</c:if>
			<c:if test="${!empty error}">
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-times-circle"></i>
						<spring:message code="label.error" />
					</h4>
					<spring:message code="${error}" />
				</div>
			</c:if>
		</div>
	</div>
	<ul class="breadcrumb breadcrumb-top">
		<li><spring:message code="heading.managedocuments" /></li>
		<li><a href="#" class="save"><spring:message code="heading.documents" /></a></li>
	</ul>
	<div class="block full  gridView">
		<div class="block-title">
			<h2>
				<strong><spring:message code="heading.activedocument" /></strong>
			</h2>
		</div>
<%-- 		<a href="<%=contexturl %>ManageUsers/UserList/AddUser" id="addUser" class="btn btn-sm btn-primary"><spring:message --%>
<%-- 				code="label.adduser" /></a> --%>
			<a href="<%=contexturl %>ManageDocument/DocumentList/AddDocument" id="addUniversity" class="btn btn-sm btn-primary save"><spring:message
				code="label.adddocument" /></a>
		<div class="table-responsive">
			<table id="active-datatable"
				class="table table-vcenter table-condensed table-bordered">
				<thead>
					<tr>
						<th class="text-center"><spring:message code="label.id" /></th>
						<th class="text-center"><spring:message code="label.modulename" /></th>
						<th class="text-center"><spring:message code="label.categoryname" /></th>
						<th class="text-center"><spring:message code="label.section" /></th>
						<th class="text-center"><spring:message code="label.filename" /></th>
						<th class="text-center"><spring:message code="label.uploadedon" /></th>
						<th class="text-center"><spring:message code="label.uploadedby" /></th>
						<th class="text-center"><spring:message code="label.actions" /></th>
					</tr>
				</thead>
				<tbody id="tbody">
					<c:forEach var="currentDocument" items="${activeDocumentList}"
						varStatus="count">

						<tr>
							<td class="text-center">${count.count}</td>
							<td class="text-left"><c:out
									value="${currentDocument.module.moduleName}" /></td>
							<td class="text-left"><c:out
									value="${currentDocument.category.categoryName}" /></td>
							<td class="text-left"><c:out value="${currentDocument.section}" /></td>
							<c:if test="${currentDocument.contentType == 'Document' && currentDocument.doctype == 'pdf'}">
								<td class="text-left"><i class="fi fi-pdf" style= "font-size : 25px; color: #666666"></i>
								<c:out value="${currentDocument.documentName}"></c:out></td>
							</c:if>
							<c:if test="${(currentDocument.contentType == 'Document' && currentDocument.doctype == 'doc') || (currentDocument.contentType == 'Document' && currentDocument.doctype == 'docx') || (currentDocument.contentType == 'Document' && currentDocument.doctype == 'txt')}">
								<td class="text-left"><i class="fi fi-doc" style= "font-size : 25px; color: #666666"></i>
								<c:out value="${currentDocument.documentName}"></c:out></td>
							</c:if>
							<c:if test="${(currentDocument.contentType == 'Document' && currentDocument.doctype == 'xlsx') || (currentDocument.contentType == 'Document' && currentDocument.doctype == 'xls') }">
								<td class="text-left"><i class="fi fi-xlsx" style= "font-size : 25px; color: #666666"></i>
								<c:out value="${currentDocument.documentName}"></c:out></td>
							</c:if>
							<c:if test="${(currentDocument.contentType == 'Document' && currentDocument.doctype == 'pptx') || (currentDocument.contentType == 'Document' && currentDocument.doctype == 'ppt') || (currentDocument.contentType == 'Document' && currentDocument.doctype == 'pptm') || (currentDocument.contentType == 'Document' && currentDocument.doctype == 'potx') || (currentDocument.contentType == 'Document' && currentDocument.doctype == 'potm')}">
								<td class="text-left"><i class="fi fi-ppt" style= "font-size : 25px; color: #666666"></i>
								<c:out value="${currentDocument.documentName}"></c:out>
							</c:if>
							<c:if test="${(currentDocument.contentType == 'Document' && currentDocument.doctype == 'jpg') || (currentDocument.contentType == 'Document' && currentDocument.doctype == 'jpeg')}">
								<td class="text-left"><i class="fi fi-jpg" style= "font-size : 25px; color: #666666"></i>
								<c:out value="${currentDocument.documentName}"></c:out></td>
							</c:if>
							<c:if test="${currentDocument.contentType == 'Document' && currentDocument.doctype == 'tiff'}">
								<td class="text-left"><i class="fi fi-tiff" style= "font-size : 25px; color: #666666"></i>
								<c:out value="${currentDocument.documentName}"></c:out></td>
							</c:if>
							<c:if test="${currentDocument.contentType == 'Document' && currentDocument.doctype == 'png'}">
								<td class="text-left"><i class="fi fi-png" style= "font-size : 25px; color: #666666"></i>
								<c:out value="${currentDocument.documentName}"></c:out></td>
							</c:if>
							<c:if test="${currentDocument.contentType == 'Document' && currentDocument.doctype == 'bmp'}">
								<td class="text-left"><i class="fi fi-bmp" style= "font-size : 25px; color: #666666"></i>
								<c:out value="${currentDocument.documentName}"></c:out></td>
							</c:if>
							<c:if test="${currentDocument.contentType == 'Document' && empty currentDocument.doctype}">
								<td class="text-left">
								<c:out value=""></c:out></td>
							</c:if>
							<c:if test = "${currentDocument.contentType == 'Link'}">	
									<td class="text-left">
										<p style="word-wrap: break-word; max-width: 250px;">
											 <i class="fa fa-link" style= "font-size : 15px; color: #666666"></i><c:out value="${currentDocument.link}" />
										</p>
									</td>
							</c:if>
							 <td class="text-left"> <fmt:formatDate type="date" value="${currentDocument.activeDocumentUploadedDate}" /></td>
							<td class="text-left"><c:out value="${currentDocument.activeDocumentUploadedByUser.fullName}" /></td>
							<td class="text-center">
								<div class="btn-group">
									<a href="<%=contexturl %>ManageDocument/DocumentList/EditDocument?id=${currentDocument.documentId}"
										data-toggle="tooltip" title="Edit"
										class="btn btn-xs btn-default save"><i class="fa fa-pencil"></i></a>
									<a href="#delete_company_popup" data-toggle="modal"
										onclick="deleteCompany(${currentDocument.documentId})"
										title="Delete" class="btn btn-xs btn-danger save"><i
										class="fa fa-times"></i></a>
								</div>
								
								
							</td>
						</tr>
					</c:forEach>

				</tbody>
			</table>
		</div>
	</div>


	<div class="block full  gridView">
		<div class="block-title">
			<h2>
				<strong><spring:message code="heading.inactivedocument" /></strong>
			</h2>
		</div>
		<div class="table-responsive">
			<table id="inactive-datatable"
				class="table table-vcenter table-condensed table-bordered ">
				<thead>
					<tr>
						<th class="text-center"><spring:message code="label.id" /></th>
						<th class="text-center"><spring:message code="label.modulename" /></th>
						<th class="text-center"><spring:message code="label.categoryname" /></th>
						<th class="text-center"><spring:message code="label.section" /></th>
						<th class="text-center"><spring:message code="label.filename" /></th>
<%-- 						<th class="text-center"><spring:message code="label.contenttype" /></th> --%>
						<th class="text-center"><spring:message code="label.deactivatedon" /></th>
						<th class="text-center"><spring:message code="label.deactivatedby" /></th>
						<th class="text-center"><spring:message code="label.actions" /></th>
					</tr>
				</thead>
				<tbody id="tablebody">
					<c:forEach var="currentDocument" items="${inactiveDocumentList}"
						varStatus="count">
						<tr>
							<td class="text-center">${count.count}</td>
							<td class="text-left"><c:out
									value="${currentDocument.module.moduleName}" /></td>
							<td class="text-left"><c:out
									value="${currentDocument.category.categoryName}" /></td>
							<td class="text-left"><c:out value="${currentDocument.section}" /></td>
							<c:choose>
								<c:when test="${currentDocument.contentType == 'Document' && currentDocument.doctype == 'pdf'}">
									<td class="text-left"><i class="fi fi-pdf" style= "font-size : 25px; color: #666666"></i>
									<c:out value="${currentDocument.documentName}"></c:out></td>
								</c:when>
								<c:when test="${(currentDocument.contentType == 'Document' && currentDocument.doctype == 'doc') || (currentDocument.contentType == 'Document' && currentDocument.doctype == 'docx') || (currentDocument.contentType == 'Document' && currentDocument.doctype == 'txt')}">
									<td class="text-left"><i class="fi fi-doc" style= "font-size : 25px; color: #666666"></i>
									<c:out value="${currentDocument.documentName}"></c:out></td>
								</c:when>
								<c:when test="${currentDocument.contentType == 'Document' && currentDocument.doctype == 'xlsx'}">
									<td class="text-left"><i class="fi fi-xlsx" style= "font-size : 25px; color: #666666"></i>
									<c:out value="${currentDocument.documentName}"></c:out></td>
								</c:when>
								<c:when test="${(currentDocument.contentType == 'Document' && currentDocument.doctype == 'pptx') || (currentDocument.contentType == 'Document' && currentDocument.doctype == 'ppt') || (currentDocument.contentType == 'Document' && currentDocument.doctype == 'pptm') || (currentDocument.contentType == 'Document' && currentDocument.doctype == 'potx') || (currentDocument.contentType == 'Document' && currentDocument.doctype == 'potm')}">
									<td class="text-left"><i class="fi fi-ppt" style= "font-size : 25px; color: #666666"></i>
									<c:out value="${currentDocument.documentName}"></c:out>
								</c:when>
								<c:when test = "${currentDocument.contentType == 'Link'}">
										<td class="text-left">
											<p style="word-wrap: break-word; max-width: 250px;">
												 <i class="fa fa-link" style= "font-size : 15px; color: #666666"></i><c:out value="${currentDocument.link}" />
											</p>
										</td>
								</c:when>
								<c:otherwise>
									<td class="text-left"></td>
								</c:otherwise>
							</c:choose>
							<td class="text-left"> <fmt:formatDate type="date" value="${currentDocument.deActivatedOn.toDate()}" /></td>
							<td class="text-left"><c:out value="${currentDocument.deActivatedBy.fullName}" /></td>
							<td class="text-center">
								<div class="btn-group">
									<a href="#restore_Company_popup" data-toggle="modal"
										onclick="restoreCompany(${currentDocument.documentId})"
										title="Restore" class="btn btn-xs btn-success"><i
										class="fa fa-plus"></i> </a>
								</div>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
	<div id="delete_company_popup" class="modal fade" tabindex="-1"
		role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<form action="<%=contexturl %>ManageDocument/DocumentList/DeleteDocument" method="post" class="form-horizontal form-bordered save"
						id="delete_company_Form">
						<div style="padding: 10px; height: 110px;">
							<label><spring:message
									code="validation.doyouwanttodeletethisdocument" /></label><br>
							<label><spring:message
									code="validation.deletingdocumentwilldeletealldetailsofdocument" /></label>
							<div class="col-xs-12 text-right">
								<button type="button" class="btn btn-sm btn-default"
									data-dismiss="modal">
									<spring:message code="label.no" />
								</button>
								<div id="delete_company" class="btn btn-sm btn-primary">
									<spring:message code="label.yes" />
								</div>
							</div>
						</div>
						<input type="hidden" name="id" id="deletecompanyId">

					</form>

				</div>
			</div>
		</div>
	</div>


	<div id="restore_Company_popup" class="modal fade" tabindex="-1"
		role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<form action="<%=contexturl %>ManageDocument/DocumentList/RestoreDocument" method="post" class="form-horizontal form-bordered save" id="restore_company_Form">
						
						<div style="padding: 10px; height: 110px;">
							<label><spring:message
									code="validation.doyouwanttorestorethisdocument" /></label><br>
							<label><spring:message
									code="validation.restoredocumentwillrestoreallperviouslydeleteddetails" /></label>
							<div class="col-xs-12 text-right">
								<button type="button" class="btn btn-sm btn-default"
									data-dismiss="modal"><spring:message code="label.no" /></button>
								<div id="restore_Company" class="btn btn-sm btn-primary">	<spring:message code="label.yes" /></div>
							</div>
						</div>
						<input type="hidden" name="id" id="restorecompanyId">
					</form>
				</div>
			</div>
		</div>
	</div>

</div>

<%@include file="../../inc/page_footer.jsp"%>
<%@include file="../../inc/template_scripts.jsp"%>
<script
	src="<%=contexturl%>resources/js/pages/activeTablesDatatables.js"></script>
<script
	src="<%=contexturl%>resources/js/pages/inactiveTablesDatatables.js"></script>
<script>$(function(){ ActiveTablesDatatables.init(7); });</script>
<script>$(function(){ InActiveTablesDatatables.init(7); });</script>
<script type="text/javascript">
	var selectedId = 0;
	var restoreId=0;
	$(document).ready(function() {
		
	});
	
	$("#manageDocument").addClass("active");
	
    $("#restore_Company").click(function(){
		
		$("#restorecompanyId").val(restoreId);
		$("#restore_company_Form").submit();
	});
	
	
	$("#delete_company").click(function(){
		
		$("#deletecompanyId").val(selectedId);
		$("#delete_company_Form").submit();
	});
	
	function deleteCompany(id){
		selectedId = id;
	}
	function restoreCompany(id){
		restoreId = id;
	}
</script>
<%@include file="../../inc/template_end.jsp"%>