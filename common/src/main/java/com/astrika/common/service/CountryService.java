package com.astrika.common.service;

import java.io.IOException;
import java.util.List;

import com.astrika.common.exception.DuplicateCountryException;
import com.astrika.common.exception.NoSuchCountryException;
import com.astrika.common.model.location.CountryMaster;

/**
 * Service interface for Country.
 * 
 * @author Priyanka
 */
public interface CountryService {

	CountryMaster save(String countryName)
			throws DuplicateCountryException;

	CountryMaster delete(long countryId) throws NoSuchCountryException;

	CountryMaster restore(long countryId) throws NoSuchCountryException;

	CountryMaster update(long countryId, String countryName)
			throws IOException;

	List<CountryMaster> findByActive(boolean active);

	CountryMaster findById(long countryId) throws NoSuchCountryException;

	List<CountryMaster> findByIds(Long[] countries);

	CountryMaster findByName(String name);
}
