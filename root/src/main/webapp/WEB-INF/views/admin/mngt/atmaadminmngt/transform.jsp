<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/template_start.jsp"%>
<%@include file="../../inc/page_head.jsp"%>



<div id="page-content">
	<div class="content-header">
		<div class="header-section">
			<h1>
				<spring:message code="heading.admin" />
				<br> <small>Transform Passwords </small>
			</h1>
			<c:if test="${!empty success}">
				<div class="alert alert-success alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-check-circle"></i>
						<spring:message code="label.success" />
					</h4>
					<spring:message code="${success}" />
				</div>
			</c:if>
			<c:if test="${!empty error}">
                <div class="alert alert-danger alert-dismissable"
                    style="display: none" id="error">
                    <button type="button" class="close" data-dismiss="alert"
                        aria-hidden="true">x</button>
                    <h4>
                        <i class="fa fa-times-circle"> </i>
                        <spring:message code="label.error" />
                    </h4>
                    <span id="errorMsg">${error}</span>
                </div>
            </c:if>

		</div>
	</div>
	<ul class="breadcrumb breadcrumb-top">
		<li>Transform Password</li>
		<li><a href="#">Transform</a></li>
	</ul>
	<div class="block full  gridView">
		<div class="block-title">
			<h2>
				<strong><spring:message code="heading.activeadmin" /></strong>
			</h2>
		</div>
		<form action="<%=contexturl %>transform" method="post" class="form-horizontal ui-formwizard"
        		id="transform">
            <div class="form-group">
                <label class="col-md-4 control-label">Click the button to transform the passwords. </label>
                <div class="col-md-6">

                    <button id="submit" type="submit"
                    						class="btn btn-sm btn-primary save">
                    						<i class="fa fa-angle-right"></i> Transform
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label"></label>
                <div class="col-md-6">
                    Note: This is time consuming process, please be patient and wait for success or error message.
                </div>
            </div>
        </form>
	</div>

</div>

<%@include file="../../inc/page_footer.jsp"%>
<%@include file="../../inc/template_scripts.jsp"%>
<script>
    $(document).ready(function() {
        $("#transform").addClass("active");
    });

</script>
<%@include file="../../inc/template_end.jsp"%>