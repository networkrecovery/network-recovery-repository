<%@page import="com.astrika.common.model.TimeZoneEnum"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@include file="../../../inc/config.jsp"%>
<%@include file="../../../inc/template_start.jsp"%>
<%@include file="../../../inc/page_head.jsp"%>



<style>

.chosen-container {
	width: 250px !important;
}
 #map-canvas { height: 300px;}
</style>
<div id="page-content">
<div class="content-header">
	<div class="header-section">
		<h1>
			<i class="fa fa-map-marker"></i>
			<spring:message code="heading.citymaster" />
			<br> <small><spring:message
					code="heading.editcity" /> </small>
		</h1>
		<span id="errorMsg"></span>
		<c:if test="${!empty success}">
			<div class="alert alert-success alert-dismissable">
				<button type="button" id="success_close" class="close" data-dismiss="alert"
					aria-hidden="true">x</button>
				<h4>
					<i class="fa fa-check-circle"></i> <spring:message code="label.success"/>
				</h4>
				<spring:message code="${success}" />
			</div>
		</c:if>
		<c:if test="${!empty error}">
			<div class="alert alert-danger alert-dismissable">
				<button type="button" class="close" data-dismiss="alert"
					aria-hidden="true">x</button>
				<h4>
					<i class="fa fa-times-circle"></i> <spring:message code="label.error"/>
				</h4>
				<spring:message code="${error}" />
			</div>
		</c:if>
	</div>
</div>
<ul class="breadcrumb breadcrumb-top">
	<li><spring:message code="menu.managelocations" /></li>
	<li><a href="#"><spring:message code="label.city" /></a></li>
</ul>
<div class="block full" id="formView">
	<div class="block-title">
		<h2>
			<strong><spring:message code="label.editcity" /></strong>
		</h2>
	</div>
	<form action="<%=contexturl %>ManageCity/City/UpdateCity" method="post" class="form-horizontal form-bordered"
		id="City_form">
		
		<div class="form-group">
			<input id="city_Id" name="cityId" type="hidden" value="${city.cityId}"> <label
				class="col-md-3 control-label" for="city_Name"><spring:message
					code="label.cityname" /><span class="text-danger">*</span></label>
			<div class="col-md-9">
				<input id="city_Name" name="cityName" class="form-control"
					placeholder="<spring:message code="label.cityname"/>.."
					type="text" value="${city.cityName}">
			</div>
		</div>			
					
					<div class="form-group" id="countryDiv">
						<label class="col-md-3 control-label" for="country"><spring:message
								code="label.countryname" /><span class="text-danger">*</span></label>
						<div class="col-md-6">
							<select class="country_chosen" style="width: 400px;"
								id="countryId"
								data-placeholder="<spring:message code='label.choosecountry' />"
								name="chosenCountryId">
								<c:forEach items="${countryList}" var="country">
									<option value="${country.countryId}"
										<c:if test="${country.countryId eq city.country.countryId}">selected="selected"</c:if>>${country.countryName}</option>
								</c:forEach>
							</select>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-md-3 control-label" for="city"><spring:message
								code="label.statename" /><span class="text-danger">*</span></label>
						<div class="col-md-6">
							<select class="state_chosen" style="width: 400px;"
								id="cityId"
								data-placeholder="<spring:message code='label.choosestate' />"
								name="chosenstateId">
								<c:forEach items="${stateList}" var="state">
									<option value="${state.stateId}"
										<c:if test="${state.stateId eq city.state.stateId}">selected="selected"</c:if>>${state.stateName}</option>
								</c:forEach>
							</select>
						</div>
					</div>
         
         
		
		<div class="form-group form-actions">
			<div class="col-md-9 col-md-offset-3">
				<div id="city_submit" class="btn btn-sm btn-primary save">
					<i class="fa fa-angle-right"></i> <spring:message code="button.save" />
				</div>
			
				<a class="btn btn-sm btn-primary" id="cancel" href="<%=contexturl %>ManageCity/City">
					
					<spring:message code="button.cancel" />
				</a>
			</div>
		</div>
	</form>
</div>
</div>
<%@include file="../../../inc/page_footer.jsp"%>
<%@include file="../../../inc/template_scripts.jsp"%>
    
	
<script type="text/javascript">
	$(document).ready(function() {
		
		$(".country_chosen").data("placeholder","Select Country From...").chosen();
		
		$(".state_chosen").data("placeholder","Select City From...").chosen();
		
		
		var stateId = ${city.state.stateId} ;
		$('select[name="chosenCountryId"]').chosen().change( function() {
			var countryId = ($(this).val());
			
			$.ajax({
				type: "GET",
				async: false,
				url: "<%=contexturl %>state/statebycountryid/"+countryId,
				success: function(data, textStatus, jqXHR){
					var obj = jQuery.parseJSON(data);
					var len=obj.length;
					var myOptions = "<option value></option>";
					for(var i=0; i<len; i++){
					
					if(stateId  != obj[i].stateId)
					{
					myOptions += '<option value="'+obj[i].stateId+'">'+obj[i].stateName+'</option>';
					}
					else{
					myOptions += '<option value="'+obj[i].stateId+'" selected="selected">'+obj[i].stateName+'</option>';
					}
					}
					$(".state_chosen").html(myOptions).chosen().trigger("chosen:updated");
				},
				dataType: 'html'
			});
		});
		
		
		changeCountry($("#countryId").val());
		
		function changeCountry(countryId){
			$.ajax({
				type: "GET",
				async: false,
				url: "<%=contexturl %>state/statebycountryid/"+countryId,
				success: function(data, textStatus, jqXHR){
					var obj = jQuery.parseJSON(data);
					var len=obj.length;
					var myOptions = "<option value></option>";
					for(var i=0; i<len; i++){
						if(stateId != obj[i].stateId){
							myOptions += '<option value="'+obj[i].stateId+'">'+obj[i].stateName+'</option>';
						}
						else{
							myOptions += '<option value="'+obj[i].stateId+'" selected="selected">'+obj[i].stateName+'</option>';
						}
					}
					$(".state_chosen").html(myOptions).chosen().trigger("chosen:updated");

				},
				dataType: 'html'
			});
		}
		
		$("#City_form").validate(
			{	errorClass:"help-block animation-slideDown",
				errorElement:"div",
				errorPlacement:function(e,a){a.parents(".form-group > div").append(e)},
				highlight:function(e){$(e).closest(".form-group").removeClass("has-success has-error").addClass("has-error")},
				success:function(e){e.closest(".form-group").removeClass("has-success has-error")},
				rules:{ cityName:{required:!0,maxlength : 75},
					    chosenStateId:{required:!0},	
						chosenCountryId:{required:!0,maxlength : 75},
						
				},
				messages:{cityName:{required:'<spring:message code="validation.city"/>',
					maxlength :'<spring:message code="validation.city75character"/>'},
					chosenCityId:{required:'<spring:message code="validation.selectcity"/>'},	
					chosenCountryId:{required:'<spring:message code="validation.selectcountry"/>'},
				},
				
			});
			
			 $("#city_submit").click(function(){
				 $("#City_form").submit();
			 });
			
	});
	
	
</script>
<%@include file="../../../inc/template_end.jsp"%>