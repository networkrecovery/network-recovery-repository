<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->

<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/template_start.jsp"%>
<%@include file="../../inc/page_head.jsp"%>

<div id="page-content">
	<div class="content-header">
		<div class="header-section">
			<c:if test="${!empty error}">
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-times-circle"></i>
						<spring:message code="label.error" />
					</h4>
					<spring:message code="${error}" />
				</div>
			</c:if>

			<h1>
				<!-- 				<i class="fa fa-map-marker"></i> -->
				<spring:message code="heading.admin" />
				<br> <small><spring:message code="heading.admindetails" /></small>
			</h1>
			<span id="errorMsg"></span>
		</div>
	</div>
	<ul class="breadcrumb breadcrumb-top">
		<li><spring:message code="heading.admin" /></li>
	</ul>


	<div class="block">
		<div class="block-title">
			<h2>
				<strong><spring:message code="heading.admininfo" /></strong>
			</h2>
		</div>

		<form action="<%=contexturl %>ManageAdmin/AdminList/SaveAdmin"
			method="post" class="form-horizontal ui-formwizard" id="User_form">

			<div class="form-group">
				<label class="col-md-4 control-label" for="first_Name"><spring:message
						code="label.firstname" /><span class="text-danger">*</span> </label>
				<div class="col-md-6">

					<input id="first_Name" name="firstName" class="form-control"
						placeholder="<spring:message code='label.firstname'/>.."
						type="text" value="${user.firstName}">
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-4 control-label" for="last_Name"><spring:message
						code="label.lastname" /><span class="text-danger">*</span> </label>
				<div class="col-md-6">

					<input id="last_Name" name="lastName" class="form-control"
						placeholder="<spring:message code="label.lastname"/>.."
						type="text" value="${user.lastName}">
				</div>
			</div>


			<div class="form-group">
				<label class="col-md-4 control-label" for="email"><spring:message
						code="label.emailid" /><span class="text-danger">*</span></label>
				<div class="col-md-6">
					<input id="email" name="emailId" class="form-control"
						placeholder="test@example.com" type="text" value="${user.emailId}">
				</div>
			</div>



			<div class="form-group">
				<label class="col-md-4 control-label" for="designation"><spring:message
						code="label.designation" /><span class="text-danger">*</span></label>
				<div class="col-md-6">
					<input id="designation" name="designation" class="form-control"
						placeholder="Designation.." type="text"
						value="${user.designation}">
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-4 control-label" for="description"><spring:message
						code="label.description" /></label>
				<div class="col-md-6">
					<textarea id="description" name="description" class="form-control"
						placeholder="Description.."></textarea>
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-4 control-label" for="approvalRights"><spring:message
						code="label.enquiryAccessRights" /><span class="text-danger">*</span></label>
				<div class="col-md-6">
					<input id="approvalRights" name="approvalRights" class=""
						type="radio" value="1">Yes <br> <input
						id="approvalRights" name="approvalRights" class="" type="radio"
						value="0" checked="checked">No
				</div>
			</div>

			<div class="form-group form-actions">
				<div class="col-md-12 col-md-offset-4">
					<button id="user_submit" type="submit"
						class="btn btn-sm btn-primary save">
						<i class="fa fa-angle-right"></i>
						<spring:message code="button.save" />
					</button>
					<a class="btn btn-sm btn-primary save"
						href="<%=contexturl %>ManageAdmin/AdminList/"><i
						class="gi gi-remove"></i> <spring:message code="button.cancel" />
					</a>
				</div>
			</div>
		</form>
	</div>
</div>
<%@include file="../../inc/page_footer.jsp"%>
<%@include file="../../inc/template_scripts.jsp"%>

<script type="text/javascript">
$(document).ready(function() {
	$("#manageAdmin").addClass("active");
	

	$("#User_form").validate(
			{	errorClass:"help-block animation-slideDown",
				errorElement:"div",
				errorPlacement:function(e,a){a.parents(".form-group > div").append(e)},
				highlight:function(e){$(e).closest(".form-group").removeClass("has-success has-error").addClass("has-error")},
				success:function(e){e.closest(".form-group").removeClass("has-success has-error")},
				rules:{ firstName : {required : !0,maxlength : 75},
					lastName : {required : !0,maxlength : 75},
					emailId : {required : !0,maxlength : 75,email : !0},
					designation :{required : !0,maxlength : 75},
					description : {maxlength : 500}
				},
				messages:{
					firstName : {
						required :'<spring:message code="validation.pleaseenterfirstname"/>',
						maxlength :'<spring:message code="validation.firstname75character"/>'
					},
					lastName : {
						required :'<spring:message code="validation.pleaseenterlastname"/>',
						maxlength :'<spring:message code="validation.lastname75character"/>'
					},
					emailId : {
						required :'<spring:message code="validation.pleaseenteremailid"/>',
						email :'<spring:message code="validation.pleaseenteravalidemailid"/>',
						maxlength :'<spring:message code="validation.email75character"/>'
					},

					designation : {
						required :'<spring:message code="validation.pleaseenterdesignation"/>',
						maxlength :'<spring:message code="validation.designation75character"/>'
					},
					description : {
						maxlength :'<spring:message code="validation.description500character"/>'
					}
					
				},
			});
	
	 $("#user_submit").click(function(){
		 $("#User_form").submit();
	 });
	
});
</script>



<%@include file="../../inc/template_end.jsp"%>