package com.astrika.common.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.envers.Audited;
import org.joda.time.DateTime;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

@Entity
@Audited
public class CurrencyMaster {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long currencyId;

	@Column(unique = true, nullable = false, length = 75)
	private String currencyName;

	@Column(nullable = false, length = 3)
	private String currencySign;

	@CreatedDate
	private DateTime createdOn;
	
	@Column(unique = true, nullable = false, length = 3)
	private String currencyCode;

	@LastModifiedDate
	private DateTime lastModifiedOn;
	
	@Column
	private boolean baseCurrency;

	@JsonIgnore
	@LastModifiedBy
	@ManyToOne(fetch = FetchType.LAZY)
	private User lastModifiedBy;

	@JsonIgnore
	@CreatedBy
	@ManyToOne(fetch = FetchType.LAZY)
	private User createdBy;
	
	@Column(columnDefinition = "boolean default true", nullable = false)
	private boolean active = true;

	public CurrencyMaster() {
		super();
	}

	public CurrencyMaster(String currencyName, String currencySign,
			String currencyCode) {
		super();
		this.currencyName = currencyName;
		this.currencySign = currencySign;
		this.currencyCode = currencyCode;
	}

	public Long getCurrencyId() {
		return currencyId;
	}

	public void setCurrencyId(Long currencyId) {
		this.currencyId = currencyId;
	}

	public String getCurrencyName() {
		return currencyName;
	}

	public void setCurrencyName(String currencyName) {
		this.currencyName = currencyName;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public String getCurrencySign() {
		return currencySign;
	}

	public void setCurrencySign(String currencySign) {
		this.currencySign = currencySign;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public void setLastModifiedOn(DateTime lastModifiedOn) {
        this.lastModifiedOn = lastModifiedOn;
    }
	
	public DateTime getCreatedOn() {
		return createdOn;
	}

	@JsonIgnore
    public void setLastModifiedBy(User lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }
	
	public DateTime getLastModifiedOn() {
		return lastModifiedOn;
	}
	
	@JsonIgnore
	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}

	public void setCreatedOn(DateTime createdOn) {
        this.createdOn = createdOn;
    }
	
	@JsonIgnore
	public User getCreatedBy() {
		return createdBy;
	}
	
	@JsonIgnore
	public User getLastModifiedBy() {
		return lastModifiedBy;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((currencyId == null) ? 0 : currencyId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CurrencyMaster other = (CurrencyMaster) obj;
		if (currencyId == null) {
			if (other.currencyId != null)
				return false;
		} else if (!currencyId.equals(other.currencyId))
			return false;
		return true;
	}

	public boolean isBaseCurrency() {
		return baseCurrency;
	}

	public void setBaseCurrency(boolean baseCurrency) {
		this.baseCurrency = baseCurrency;
	}

}
