<!DOCTYPE html>
<html lang="en">
<%@include file="/WEB-INF/views/admin/inc/config.jsp"%>
<%-- <%@include file="/WEB-INF/views/admin/inc/template_start.jsp"%> --%>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Atma - Strengthning organisations to impact Society</title>
<!-- Style -->
<!-- Bootstrap -->
<link href="<%=contexturl%>resources/css/bootstrap.css" rel="stylesheet"
	media="screen">
<link href="<%=contexturl%>resources/css/fontface.css" rel="stylesheet"
	media="screen">
<link href="<%=contexturl%>resources/css/font-awesome.css"
	rel="stylesheet" media="screen">
<link href="<%=contexturl%>resources/css/jquery.bxslider.css"
	rel="stylesheet" media="screen">
<!--[if IE 9]>
  <script src="<%=contexturl%>resources/js/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="<%=contexturl%>resources/js/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<!--[if IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

<link href="<%=contexturl%>resources/css/style.css" rel="stylesheet"
	media="screen">
</head>


<body>
	<div class="header-bg">
		<nav class="navbar">
			<div class="container">
				<div class="navbar-header">
					<img src="<%=contexturl%>resources/img/atma.jpg" />
				</div>
				<div id="navbar" class="mynav">
					<ul class="nav navbar-nav pull-right">
						<li class="active"><a href="Login"><b>Back to Login
									Page</b></a></li>
					</ul>
				</div>
			</div>
		</nav>
		<div class="blue-container">
			<div class="container">
				
				<div class="row blue">
					<div class="col-md-10">
						<div class="media">
							<a class="pull-left"><img class="media-object"
								src="<%=contexturl%>resources/img/Signin_Thumbs.png"
								alt="Organisation Name"> </a>
							<div class="media-body text-st">
								<p>Register here</p>
								<h4 class="media-heading">Organisation Registration</h4>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


	<div class="container">
		<div class="row">
<%-- 		<c:if test="${!empty success}"> --%>
<!-- 					<div class="alert alert-success alert-dismissable"> -->
<!-- 						<button type="button" class="close" data-dismiss="alert" -->
<!-- 							aria-hidden="true">x</button> -->
<!-- 						<h4> -->
<!-- 							<i class="fa fa-check-circle"></i> -->
<%-- 							<spring:message code="label.success" /> --%>
<!-- 						</h4> -->
<%-- 						<spring:message code="${success}" /> --%>
<!-- 					</div> -->
<%-- 				</c:if> --%>
				<c:if test="${!empty error}">
					<div class="alert alert-danger alert-dismissable">
						<button type="button" class="close" data-dismiss="alert"
							aria-hidden="true">x</button>
						<h4>
							<i class="fa fa-times-circle"></i>
							<spring:message code="label.error" />
						</h4>
						<spring:message code="${error}" />
					</div>
				</c:if>
			<form action="SaveEnquiry" method="post" class="form-horizontal "
				id="member_form" enctype="multipart/form-data">
				<div class="col-md-7">
                    <div class="block full">
                        <div class="block-title">
                            <h4 class="">Personal Details</h4>
                        </div>
						<div class="form-group">
							<label class="col-sm-3 control-label">First Name<span
								class="mandatory">*</span>
							</label>
							<div class="col-sm-8">
								<input type="text" id="first_Name" name="firstName"
									class="form-control" placeholder="First Name *"
									value="${enquiry.firstName}">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Last Name <span
								class="mandatory">*</span>
							</label>
							<div class="col-sm-8">
								<input type="text" id="last_Name" name="lastName" class="form-control"
									placeholder="Last Name *" value="${enquiry.lastName}">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">E-mail <span
								class="mandatory">*</span>
							</label>
							<div class="col-sm-8">
								<input type="email" id="email" name="emailId"
									class="form-control" placeholder="test@example.com*"
									value="${enquiry.emailId}">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label" for="mobile">Mobile<span class="text-danger">*</span></label>
							<div class="col-md-8">
								<input id="mobile" name="mobile" class="form-control"
									placeholder="Mobile no" type="text" value="${enquiry.mobile}">
							</div>
						</div>
					</div>
					<div class="block full">
						<div class="block-title">
							<h4 class="">Company Details</h4>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">NGO Name <span
								class="mandatory">*</span>
							</label>
							<div class="col-sm-8">
								<input id="ngo_Name" name="ngoName" type="text"
									class="form-control" placeholder="NGO Name *"
									value="${enquiry.ngoName}">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Field/Sector or
								Work <span class="mandatory">*</span>
							</label>
							<div class="col-sm-8">
								<input id="sector_work" name="sectorWork" type="text"
									class="form-control" placeholder="Field/Sector or Work*"
									value="${enquiry.sectorWork}">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Address Line1</label>
							<div class="col-sm-8">
								<input id="ngoAddressLine1" name="ngoAddressLine1" type="text"
									class="form-control" placeholder="Address Line1"
									value="${enquiry.ngoAddress1}">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Address Line2</label>
							<div class="col-sm-8">
								<input id="ngoAddressLine2" name="ngoAddressLine2" type="text"
									class="form-control" placeholder="Address Line2"
									value="${enquiry.ngoAddress2}">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Address Line3</label>
							<div class="col-sm-8">
								<input id="ngoAddressLine3" name="ngoAddressLine3" type="text"
									class="form-control" placeholder="Address Line3"
									value="${enquiry.ngoAddress3}">
							</div>
						</div>
						<div class="form-group" id="countryDiv">
							<label class="col-sm-3 control-label">Country Name<span
								class="mandatory">*</span>
							</label>

							<div class="col-sm-8" >
								<select class=" form-control" id = "countrydiv" name = "countryDiv">
									<option value=""></option>
									<c:forEach items="${countryList}" var="country">
										<option value="${country.countryId}"
											<c:if test="${country.countryId eq enquiry.country.countryId}">selected="selected"</c:if>>${country.countryName}</option>
									</c:forEach>
								</select>
							</div>
						</div>
						<div class="form-group" id="stateDiv">
							<label class="col-sm-3 control-label">State Name<span
								class="mandatory">*</span>
							</label>
							<div class="col-sm-8" >
								<select class=" form-control"
									id="statediv" name="stateDiv">
									<option value=""></option>
									<c:forEach items="${stateList}" var="state">
										<option value="${state.stateId}"
											<c:if test="${state.stateId eq enquiry.state.stateId}">selected="selected"</c:if>>${state.stateName}</option>
									</c:forEach>
								</select>
							</div>
						</div>
						<div class="form-group" id="cityDiv">
							<label class="col-sm-3 control-label">City Name<span
								class="mandatory">*</span>
							</label>
							<div class="col-sm-8" >
								<select class=" form-control"
									id="citydiv" name="cityDiv">
									<option value=""></option>
									<c:forEach items="${cityList}" var="city">
										<option value="${city.cityId}"
											<c:if test="${city.cityId eq enquiry.city.cityId}">selected="selected"</c:if>>${city.cityName}</option>
									</c:forEach>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Pincode<span
								class="mandatory">*</span>
							</label>
							<div class="col-sm-8">
								<input id="ngoPincode" name="ngoPincode" type="text"
									class="form-control" placeholder="Pincode"
									value="${enquiry.ngoPincode}">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Telephone</label>
							<div class="col-sm-8">
								<input id="ngoPhone" name="ngoPhone" type="text"
									class="form-control" placeholder="Telephone"
									value="${enquiry.ngoPhone}">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">NGO Email<span
								class="mandatory">*</span>
							</label>
							<div class="col-sm-8">
								<input id="ngo_email" name="ngoEmail" type="email"
									class="form-control" placeholder="NGO Email"
									value="${enquiry.ngoEmail}">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">NGO Website<span
								class="mandatory">*</span>
							</label>
							<div class="col-sm-8">
								<input id="ngo_website" name="ngoWebsite" type="text"
									class="form-control" placeholder="NGO Website"
									value="${enquiry.ngoWebsite}">
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-5">
					<div class="block full">
						<div class="block-title">
							<h4 class="">About Organisation</h4>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">Organisation Vision<span
								class="mandatory">*</span>
							</label>
							<div class="col-sm-8">
								<textarea id="organisationVision" name="organisationVision"
									class="form-control" rows="5" placeholder="Organisation Vision">${enquiry.organisationVision}</textarea>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">Organisation
								Mission<span class="mandatory">*</span>
							</label>
							<div class="col-sm-8">
								<textarea id="organisationMission" name="organisationMission"
									class="form-control" rows="5"
									placeholder="Organisation Mission">${enquiry.organisationMission}</textarea>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">3 sentences
								describing what your organisation does.<span class="mandatory">*</span>
							</label>
							<div class="col-sm-8">
								<textarea id="organisationDescription"
									name="organisationDescription" class="form-control" rows="5"
									placeholder="Orginasation Discription">${enquiry.organisationDescription}</textarea>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-8 control-label">Is your
								organisation presently or has it in the past recieved
								organisational development assistance from any other agency?<span
								class="mandatory">*</span>
							</label>
							<div class="col-sm-4">
								<label class="radio"> <input id="assistance_otheragency"
									type="radio" name="assitanceFromOtherAgency" value="1" /> <span
									class="input-text">Yes</span>
								</label> <label class="radio"> <input
									id="assistance_otheragency" type="radio"
									name="assitanceFromOtherAgency" value="0" checked="checked" />
									<span class="input-text">No</span>
								</label>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-8 control-label">Please Indicate the
								Legal Type of Registration<span class="mandatory">*</span>
							</label>
							<div class="col-sm-4">
								<label class="radio"> <input id="legalType"
									name="legalType" type="radio" value="Societies Reg.Act 1860" />
									<span class="input-text">Societies Reg. Act 1860</span>
								</label> <label class="radio"> <input id="legalType"
									name="legalType" type="radio" value="Public Trust Act" /> <span
									class="input-text">Public Trust Act</span>
								</label> <label class="radio"> <input id="legalType"
									name="legalType" type="radio" value="Section 25 Company"
									checked="checked" /> <span class="input-text">Section
										25 Company</span>
								</label>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-8 control-label">FCRA Registration<span
								class="mandatory">*</span>
							</label>
							<div class="col-sm-4">
								<label class="radio"> <input id="fcra_registration"
									name="fcraRegistarion" type="radio" value="1" /> <span
									class="input-text">Yes</span>
								</label> <label class="radio"> <input id="fcra_registration"
									name="fcraRegistarion" type="radio" value="0" checked="checked" />
									<span class="input-text">No</span>
								</label>
							</div>
						</div>
						<hr>
					</div>
					<div class="block full">
						<div class="block-title">
							<h4 class="">Education Details</h4>
						</div>
						<div class="form-group" id="educationTypeDiv">
							<label class="col-sm-4 control-label">Type of Education
								your organisation offers<span class="mandatory">*</span>
							</label>
								<div class="col-sm-8" id="subFunctionDiv4">
									<select class=" form-control subfunctionChosen"
										id="typeOfEducation" name="typeOfEducation">
										<option value=""></option>
										<c:forEach items="${educationList}" var="EType">
											<option value="${EType}"
												<c:if test="${EType eq enquiry.typeOfEducation}">selected="selected"</c:if>>${EType}</option>
										</c:forEach>
									</select>
								</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>

	<div class="container">
		<div class="row">
			<div class="text-center">
				<button id="member_submit" type="submit" class="btn btn-black">Submit</button>
			</div>
		</div>
	</div>

	</div>

	<footer>
        <div class="container">
            <div class=" social-icon pull-right">
                <strong>Follows us</strong>
                <img src="<%=contexturl %>resources/img/01_twitter.png" alt="twitter">
                <img src="<%=contexturl %>resources/img/02_facebook.png" alt="facebook">
                <img src="<%=contexturl %>resources/img/03_youtube.png" alt="youtube">
                <img src="<%=contexturl %>resources/img/07_linkedin.png" alt="linkedin">
            </div>
        </div>
        <div class="footer-color">
            <div class="container">
                <div class="col-md-4"><div class="coipyrights">Copyright � 2015</div></div>
                <div class="col-md-8">
                    <ul class="footer-menu">
                        <li><a href="">Photo Gallery</a>
                        </li>
                        <li><a href="">Video Gallery</a>
                        </li>
                        <li><a href="">Post Your Testimonial</a>
                        </li>
                        <li><a href="">Media Room</a>
                        </li>
                        <li><a href="">General Enquiry</a>
                        </li>
                        <li><a href="">Sitemap</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>
	<!-- end heder -->
	
	
</body>
<!-- <script src="http://code.jquery.com/jquery.js"></script> -->
<script src="<%=contexturl%>resources/js/vendor/jquery-1.11.0.min.js"></script>
<script src="<%=contexturl%>resources/js/vendor/bootstrap.min.js"></script>
<script src="<%=contexturl %>resources/js/vendor/validate.js"></script>
<script src="<%=contexturl %>resources/js/additional-methods.js"></script>



<script type="text/javascript">
	$(document).ready(function() {
						
						$('select[name="countryDiv"]').change( function() {
							var countryId = ($(this).val());
							$.ajax({
								type: "GET",
								async: false,
								url: "<%=contexturl%>state/statebycountryid/"+countryId,
								success: function(data, textStatus, jqXHR){
									var obj = jQuery.parseJSON(data);
									var len=obj.length;
									var myOptions = "<option value></option>";
									var myCityOptions = "<option value></option>";
									for(var i=0; i<len; i++){
									myOptions += '<option value="'+obj[i].stateId+'">'+obj[i].stateName+'</option>';
									}
									$("#statediv").html(myOptions);
									$("#citydiv").html(myCityOptions);
								},
								dataType: 'html'
							});
							
						});
						
						
						$('select[name="stateDiv"]').change( function() {
							var stateId = ($(this).val());
							$.ajax({
								type: "GET",
								async: false,
								url: "<%=contexturl %>city/citybystateid/"+stateId,
								success: function(data, textStatus, jqXHR){
									var obj = jQuery.parseJSON(data);
									var len=obj.length;
									var myOptions = "<option value></option>";
									for(var i=0; i<len; i++){
									myOptions += '<option value="'+obj[i].cityId+'">'+obj[i].cityName+'</option>';
									}
									$("#citydiv").html(myOptions);
								},
								dataType: 'html'
							});
							
							
						});	
						
						$("#member_form").validate(
										{
											errorClass : "help-block animation-slideDown",
											errorElement : "div",
											errorPlacement : function(e, a) {
												a.parents(".form-group > div")
														.append(e)
											},
											highlight : function(e) {
												$(e)
														.closest(".form-group")
														.removeClass(
																"has-success has-error")
														.addClass("has-error")
											},
											success : function(e) {
												e
														.closest(".form-group")
														.removeClass(
																"has-success has-error")
											},
											rules : {
												firstName : {
													required : !0,maxlength : 75,
												},
												lastName : {
													required : !0,maxlength : 75,
												},
												emailId : {
													required : !0,maxlength : 75,
													email : !0,
												},
												mobile : {
													required : !0,
													mobile : !0,
												},
												ngoName : {
													required : !0,
													maxlength : 75
												},
												sectorWork : {
													required : !0,
													maxlength : 75
												},
												ngoAddressLine1 : {
													maxlength : 75
												},
												ngoAddressLine2 : {
													maxlength : 75
												},
												ngoAddressLine3 : {
													maxlength : 75
												},

												cityDiv : {
													required : !0
												},
												stateDiv : {
													required : !0
												},

												countryDiv : {
													required : !0
												},

												ngoPincode : {
													required : !0,
													maxlength : 10,
													digits : !0,
													pincode : !0
												},
												ngoPhone : {
													phoneUS : !0
												},
												ngoEmail : {
													required : !0,
													maxlength : 75,
													email : !0
												},
												ngoWebsite : {
													required : !0,
													url : true,
													maxlength : 500
												},
												
												organisationVision : {
													maxlength : 500,
													required : !0
												},
												organisationMission : {
													maxlength : 500,
													required : !0
												},
												organisationDescription : {
													maxlength : 255,
													required : !0
												},
												legalType : {
													required : !0
												},
												typeOfEducation : {
													required : !0
												}
											},
											messages : {
												firstName : {
													required :'<spring:message code="validation.pleaseenterfirstname"/>',
													maxlength :'<spring:message code="validation.firstname75character"/>',
												},
												lastName : {
													required :'<spring:message code="validation.pleaseenterlastname"/>',
													maxlength :'<spring:message code="validation.lastname75character"/>',
												},
												emailId : {
													required :'<spring:message code="validation.pleaseenteremailid"/>',
													email :'<spring:message code="validation.pleaseenteravalidemailid"/>',
													maxlength :'<spring:message code="validation.email75character"/>',
												},
												mobile : {
													required :'<spring:message code="validation.pleaseenteramobilenumber"/>',
													mobile :'<spring:message code="validation.mobile"/>',
													maxlength :'<spring:message code="validation.mobile10character"/>',
												},
												ngoName : {
													required : 'Please enter a NGO Name',
													maxlength : 'NGO name should not be more than 75 characters'
												},
												sectorWork : {
													required : 'Please enter a Field/Sector of Work',
													maxlength : 'Field/Sector of Work should not be more than 75 characters'
												},

												ngoAddressLine1 : {

													maxlength : '<spring:message code="validation.addressline1"/>'
												},
												ngoAddressLine2 : {

													maxlength : '<spring:message code="validation.addressline1"/>'
												},
												ngoAddressLine3 : {

													maxlength : '<spring:message code="validation.addressline1"/>'
												},
												cityDiv : {
													required : '<spring:message code="validation.selectcity"/>'
												},
												stateDiv : {
													required : '<spring:message code="validation.selectstate"/>'
												},
												countryDiv : {
													required : '<spring:message code="validation.selectcountry"/>'
												},
												ngoPincode : {
													required : '<spring:message code="validation.pleaseenterpincode"/>',
													maxlength : '<spring:message code="validation.pincode10character"/>',
													digits : '<spring:message code="validation.digits"/>'
												},
												
												ngoPhone : {

													phone : '<spring:message code="validation.phone"/>'
												},
												ngoEmail : {
													required : '<spring:message code="validation.pleaseenteremailid"/>',
													email : '<spring:message code="validation.pleaseenteravalidemailid"/>',
													maxlength : '<spring:message code="validation.email75character"/>'
												},
												ngoWebsite : {
													required : 'Please enter a company website',
													url : '<spring:message code="validation.pleaseenteravalidurl"/>',
													maxlength : 'Link should not be more than 500 characters'
												},
												
												organisationVision : {
													maxlength : 'Vision should not be more than 500 characters',
													required : 'Please enter Organisation Vision'
												},
												organisationMission : {
													maxlength : 'Mission should not be more than 500 characters',
													required : 'Please enter Organisation Mission'
												},
												organisationDescription : {
													maxlength : 'Description should not be more than 255 characters',
													required : 'Please enter Organisation Description'
												},
												legalType : {
													required : 'Please select a Legal Type',
												},
												typeOfEducation : {
													required : 'Please select a type of Education'
												}
											},

										});

						 jQuery.validator.addMethod("phone", function (phone_number, element) {
						        phone_number = phone_number.replace(/\s+/g, "");
						        return this.optional(element) || phone_number.length >= 8 && phone_number.length < 25 && phone_number.match(/^[0-9\+\-]*$/);
						    }, "Please specify a valid phone number");
						    
						 jQuery.validator.addMethod("mobile", function (mobile_number, element) {
						      mobile_number = mobile_number.replace(/\s+/g, "");
						       if (mobile_number.match(/^[7-9][0-9]{9}$/)) return true;
						         //return this.optional(element) || mobile_number.length >= 10 && mobile_number.length < 25 && mobile_number.match(/^[0-9\+\-]*$/);
						     }, "Please specify a valid mobile number");
						    
						    jQuery.validator.addMethod("pincode", function (pincode, element) {
						    	pincode = pincode.replace(/\s+/g, "");
						        return this.optional(element) || pincode.length >=4 && pincode.length <= 10;
						    }, "Please specify a valid pincode");
						
						
						$("#member_submit").click(function() {
							unsaved = false;
							$("#member_form").submit();
						});

					});
</script>

</html>
