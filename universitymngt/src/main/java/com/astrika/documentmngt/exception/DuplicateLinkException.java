package com.astrika.documentmngt.exception;

import com.astrika.kernel.exception.BusinessException;
import com.astrika.kernel.exception.CustomException;

public class DuplicateLinkException extends BusinessException {

	public DuplicateLinkException() {
		super(CustomException.DUPLICATE_LINK.getCode());
	}

	public DuplicateLinkException(String errorCode) {
		super(errorCode);
	} 
	
}
