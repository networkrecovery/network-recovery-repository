<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/template_start.jsp"%>
<%@include file="../../inc/page_head.jsp"%>



<div id="page-content">
	<div class="content-header">
		<div class="header-section">
			<h1>
				<i class="fa fa-map-marker"></i>
				<spring:message code="heading.courseinviteresult" />
				<br> <small><spring:message
						code="heading.specifylistofcourseinvitationresult" /> </small>
			</h1>
			<c:if test="${!empty success}">
				<div class="alert alert-success alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-check-circle"></i>
						<spring:message code="label.success" />
					</h4>
					<spring:message code="${success}" />
				</div>
			</c:if>
			<div class="alert alert-danger alert-dismissable"
				style="display: none" id="error">
				<button type="button" class="close" data-dismiss="alert"
					aria-hidden="true">x</button>
				<h4>
					<i class="fa fa-times-circle"> </i> <spring:message code="label.error"/>
				</h4>
				<span id="errorMsg"></span>
			</div>
		</div>
	</div>
	<ul class="breadcrumb breadcrumb-top">
		<li><spring:message code="heading.courseinviteresult" /></li>
		<li><a href="#"><spring:message code="heading.courseMaster" /></a></li>
	</ul>
	
	<div class = "block">
						<div class="block-title">
                            <ul class="nav nav-tabs" data-toggle="tabs">
                               
                                <li class=""><a href="#search-tab-invited">Invited List</a></li>
                            </ul>
                        </div>
      <div class="tab-content">                  
	<div class="block full  gridView tab-pane active" id = "search-tab-invited" >
		<div class="block-title">
			<h2>
				<strong><spring:message code="heading.activecourse" /></strong>
			</h2>
		</div>
		
		<div class="table-responsive">
			<table id="active-datatable"
				class="table table-vcenter table-condensed table-bordered">
				<thead>
					<tr>
						<th class="text-center"><spring:message code="label.id" /></th>
						<th class="text-center"><spring:message code="label.instructorname" /></th>
						<th class="text-center"><spring:message code="label.instructorstatus" /></th>
<%-- 						<th class="text-center"><spring:message code="label.actions" /></th> --%>
					</tr>
				</thead>
				<tbody id="tbody">
					<c:forEach var="currentCourseInstructor" items="${courseInstructorList}"
						varStatus="count">

						<tr>
							<td class="text-center">${count.count}</td>
							<td class="text-left"><c:out
									value="${currentCourseInstructor.instructor.user.firstName}  ${currentCourseInstructor.instructor.user.lastName}" /></td>
							<td class="text-right"><c:out
									value="${currentCourseInstructor.status}" /></td>
<!-- 							<td class="text-center"> -->
								
<%-- 								<a class="btn btn-sm btn-primary" href="<%=contexturl %>ManageCourse/CourseList/"> --%>
<%-- 							   <spring:message code="button.delete" /> </a> --%>
								
<!-- 							</td> -->
						</tr>
					</c:forEach>

				</tbody>
			</table>
			
			<div class="form-group form-actions">
				<div class="col-md-9 col-md-offset-5">
							<c:choose>
								<c:when test="${course.status eq status}">
								</c:when>
								<c:otherwise>
									<a href="#publish_course_pop" data-toggle="modal"
										onclick="publishCourse(${id})" class="btn btn-sm btn-primary"><spring:message
											code="button.publish" /> </a>
								</c:otherwise>
							</c:choose>
							<a class="btn btn-sm btn-primary " href="<%=contexturl %>ManageCourse/CourseList/">
							<spring:message code="button.back" /> </a>
					
				</div>
			</div>
			
			
		</div>
			
	</div>
		
</div>


	<div id="publish_course_pop" class="modal fade" tabindex="-1"
		role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<form action="<%=contexturl %>ManageCourse/CourseList/PublishCourse" method="post" class="form-horizontal form-bordered" id="publish_course_Form">
						
						<div style="padding: 10px; height: 110px;">
							<label><spring:message
									code="validation.doyouwantopublishthiscourse" /></label>
							<div class="col-xs-12 text-right">
								<button type="button" class="btn btn-sm btn-default"
									data-dismiss="modal"><spring:message code="label.no" /></button>
								<div id="publish_course" class="btn btn-sm btn-primary">	<spring:message code="label.yes" /></div>
							</div>
						</div>
						<input type="hidden" name="id" id="publishcourseId">
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<%@include file="../../inc/page_footer.jsp"%>
<%@include file="../../inc/template_scripts.jsp"%>
<script
	src="<%=contexturl%>resources/js/pages/activeTablesDatatables.js"></script>
<script
	src="<%=contexturl%>resources/js/pages/inactiveTablesDatatables.js"></script>
<script>$(function(){ ActiveTablesDatatables.init(3); });</script>
<script>$(function(){ InActiveTablesDatatables.init(3); });</script>
<script type="text/javascript">
	var selectedId = 0;
	
	
		
        $("#publish_course").click(function(){
			
			$("#publishcourseId").val(selectedId);
			$("#publish_course_Form").submit();
		});
		
	
	function publishCourse(id){
		selectedId = id;
	};
	
</script>
<%@include file="../../inc/template_end.jsp"%>