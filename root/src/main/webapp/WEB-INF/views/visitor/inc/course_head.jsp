
            <!-- START Restaurant Header -->				
<%-- <%@page import="com.astrika.outletmngt.model.restaurantmngt.RestaurantFeatureControl"%> --%>
<%@page import="java.math.BigDecimal"%>
<%-- <%@page import="com.astrika.outletmngt.model.UserOutletRating"%> --%>
<style>
.ratingwidth{
width: 175px !important;
}
.restaurant_logo{
width:200px ;
}

@media (min-width: 767px){
.restaurant_logo_visible{
margin-left: 225px;
}
}
@media (max-width: 767px){
.restaurant_logo_visible{
margin-left: 25px;
}
}


.sidebar-nav a{
padding-bottom:10px;
padding-top:5px;
}
.sidebar-nav li{
border-bottom: 1px solid #cccccc;

}
.sidebar-nav a:hover, .sidebar-nav a.open, .sidebar-nav li.active > a {
text-decoration: none;
background:none;
background: rgba(0, 0, 0, 0.15);
}
.els{
overflow: hidden;
text-overflow: ellipsis;
-o-text-overflow: ellipsis;
white-space: nowrap;
width: 100%;
}
}

#to-top{
z-index:9999;
opacity:0.7

</style>
<header class="navbar navbar-default" style="width: 100%;">
               <%
                BigDecimal rating1 = new  BigDecimal("0") ;
                BigDecimal rating2 =  new  BigDecimal("0") ;
                BigDecimal rating3 = new  BigDecimal("0") ;
                BigDecimal rating4 = new  BigDecimal("0") ;
                BigDecimal rating5 = new  BigDecimal("0") ;
                BigDecimal avgrating = new  BigDecimal("0") ;
			    
			    UserOutletRating userOutletRating=(UserOutletRating)request.getAttribute("userOutletRating");
			    if(userOutletRating != null){
			    rating1=userOutletRating.getRating1();
			    rating2=userOutletRating.getRating2();
			    rating3=userOutletRating.getRating3();
			    rating4=userOutletRating.getRating4();
			    rating5=userOutletRating.getRating5();
			    avgrating=userOutletRating.getAvgrating();	 }
			    
			    RestaurantFeatureControl control = (RestaurantFeatureControl)session.getAttribute("restaurantFeatureControl");
			   %>
                       <!-- Main Sidebar Toggle Button 
                       <ul class="nav navbar-nav-custom">
                           <li>
                               <a href="javascript:void(0)" onclick="App.sidebar('toggle-sidebar');">
                                   <i class="fa fa-bars fa-fw"></i>
                               </a>
                           </li>
                       </ul>
                       <!-- END Main Sidebar Toggle Button -->
                       <div class="header-section">
							<div class="row text-left">
								<ul class="nav navbar-nav-custom visible-xs  visible-sm " style="margin-top: 10px">
				                    <li>
					                    <a href="javascript:void(0)" onclick="App.sidebar('toggle-sidebar');">
					                        <i class="fa fa-bars fa-fw"></i>
					                    </a>
					                </li>
				                </ul>
								<div class="hidden-xs text-center  animation-fadeIn" style="width: 200px;padding: 0 62px;">
									 <c:choose>
										<c:when test="${restaurant.logoImage!=null}">
											<img src="<%=contexturl %>${restaurant.logoImage.imagePath}"  alt="Avatar" class="pull-left img-circle" style="width: 75px;height: 75px;">
										</c:when>
										<c:otherwise >
											<img src="<%=contexturl %>resources/img/application/rest-logo-default.jpg" alt="Avatar" class="pull-left img-circle" style="width: 75px;height: 75px;">
										</c:otherwise>
									  </c:choose>
								
								</div>
								<div class="restaurant_logo_visible col-sm" style="padding: 0 5px;">
									<div class="row" >
										<div class="col-md-6">
											<div class="row text-left">
												<div class="col-sm-11 col-xs-10">
													<div class="row">		
														<h3 class="col-xs-12 themed-color-fire  " style="padding-left:0px;margin-bottom:2px;"><strong>${restaurant.name}</strong></h3>
														<div class="col-xs-12 els"  style="padding-left:0px;" id="specialty" title="${fn:replace(restaurant.specialty, ',', ', ')}" >  ${fn:replace(restaurant.specialty, ',', ', ')}</div>											
														<div class="col-xs-2 ratingwidth"  style="padding-left:0px;" id="restaurant_rating"></div>
														<div class="col-xs-5 text-warning"  style="padding-left:0px;"><i id="ratingCount">(${restaurant.ratingsCount} Ratings)</i></div>
													</div>	
												</div>	
												<div class="col-sm-1 col-md-1 col-lg-1 col-xs-2 text-center " style="font-size: 17px;">
													
															<a href="#" onclick="followChanged('0')" id="unFollowRestDiv" style="display:none;"><i class="fa fa-heart animation-pulse themed-color-fire" title="Un Follow Restaurant" ></i></a>
														
															<a href="#" onclick="followChanged('1')" id="followRestDiv" style="display:none;"><i class="fa fa-heart" style="color: #BDC8CA;" title="Follow Restaurant"></i></a>
													
												<c:choose>
														<c:when test="${! empty restaurant.twitterlink}">
														 <a href="${restaurant.twitterlink}" target="_blank"><i class="fa fa-twitter" style="color:#55acee;" title="Twitter"></i></a>
														</c:when>
														 <c:otherwise>
														  <a href="#"></a>
														</c:otherwise>
														</c:choose>
														
														<c:choose>
														<c:when test="${! empty restaurant.facebooklink}">
														 <a href="${restaurant.facebooklink}" target="_blank"><i class="fa fa-facebook" style="color:#4c66a4;" title="Facebook"></i></a>
														</c:when>
														 <c:otherwise>
														  <a href="#"></a>
														</c:otherwise>
														</c:choose>
												</div>
											</div>

										</div>	
									
								
										<div class="col-md-6 hidden-xs hidden-sm">
											<div class="row text-center" style="padding-top:18px;">
											<% if(control != null && control.isEvite()){ %>
												<div class="col-md-4">
														<a href="#underConst_popup" data-toggle="modal" class="animation-hatch btn btn-warning"><i class="gi gi-celebration"></i> Create an Evite</a>
													
												</div>
											<% }
											if(control != null && control.isPreorder()){ %>
												<div class="col-md-4">
													
														<a href="#underConst_popup" data-toggle="modal" class="animation-hatch btn btn-default"><i class="fa fa-thumbs-o-up"></i> Pre Order Meal</a>
													
												</div>
											<% }
											if(control != null && control.isWaitTimeTracker()){ %>
												<div class="col-md-4">
													
														<a href="#underConst_popup" data-toggle="modal" class="animation-hatch btn btn-default"><i class="fa fa-clock-o"></i> Wait Time Tracker</a>
												</div>
											<% }
											if(control != null && control.isGiftAMeal()){ %>
<!-- 												<div class="col-md-6"> -->
													
<%-- 														 <a href="<%=contexturl %>Restaurant/Meals/${restaurant.outletId}">  --%>
<!-- 														 <button class="animation-hatch btn btn-default"><i class="fa fa-gift"></i> Gift a Meal</button> -->
<!-- 														 </a> -->
													
<!-- 												</div> -->
											<%} %>
											</div>
										</div>
								</div>	
							</div>
						</div>
            
			</header>
            <!-- END Restaurant Header -->
			
          
            <!-- Main Restaurant Sidebar -->
            <div id="sidebar">
                <!-- Wrapper for scrolling functionality -->
                <div class="sidebar-scroll">
                    <!-- Sidebar Content -->
                    <div class="sidebar-content">
                      <!-- Sidebar Navigation -->
                        <ul class="sidebar-nav">
							<li id="left-header-widgets" style="display:none;height: 75px;">
                                <a href="<%=contexturl %>/"><img src="<%=contexturl %>resources/img/logo.png"></a>
                            </li> 		
                            <li>
                                <a id="rest_rest" href="<%=contexturl %>Restaurant/${restaurant.outletId}"><i class="gi gi-asterisk sidebar-nav-icon"></i>Basic Info</a>
                            </li>
                            <% if(control != null && control.isOffer()){ %>                           
                            <li>
                                <a id="rest_offers" href="<%=contexturl %>Restaurant/Offers/${restaurant.outletId}"><i class="hi hi-fire sidebar-nav-icon"></i>Offer</a>
                            </li>
                            <%} %>
                            <li>
                                <a id="rest_menu" href="<%=contexturl %>Restaurant/Menus/${restaurant.outletId}"><i class="fa fa-cutlery sidebar-nav-icon"></i>Menu</a>
                            </li>
                            <% if(control != null && control.isPromocode()){ %>
							 <li>
                                <a id="rest_promocode" href="<%=contexturl %>Restaurant/Promocodes/${restaurant.outletId}"><i class="fa fa-ticket sidebar-nav-icon"></i>Promotions</a>
                            </li>
                            <%}
                            if(control != null && control.isGiftAMeal()){ %>
<!-- 							<li> -->
<%--                                 <a href="<%=contexturl %>Restaurant/Meals/${restaurant.outletId}""><i class="fa fa-gift sidebar-nav-icon"></i>Gift a Meal</a> --%>
<!--                             </li> -->
							<%} %>
                            <li>
                                <a id="rest_rateAndFeedback" href="<%=contexturl %>Restaurant/RateAndFeedback/${restaurant.outletId}"><i class="gi gi-star sidebar-nav-icon"></i>Rating And Feedback</a>
                            </li>
                        </ul>
                         <!-- END Sidebar Navigation -->

                        <!-- Sidebar Notifications -->
<!--                         <div class="sidebar-header"> -->
<!--                             <span class="sidebar-header-options clearfix"> -->
<!--                                 <a href="javascript:void(0)" data-toggle="tooltip" title="Refresh"><i class="gi gi-refresh"></i></a> -->
<!--                             </span> -->
<!--                             <span class="sidebar-header-title">Notice Board</span> -->
<!--                         </div> -->
<!--                         <div class="sidebar-section"> -->
<!--                             <div class="alert alert-success alert-alt"> -->
<!--                                 <small>1000 Points away</small><br> -->
<!--                                 <i class="fa fa-thumbs-up fa-fw"></i> You need 1000 points to get 10% discount -->
<!--                             </div> -->
<!--                             <div class="alert alert-info alert-alt"> -->
<!--                                 <small>New Plan</small><br> -->
<!--                                 <i class="fa fa-arrow-up fa-fw"></i> Upgrade to Pro plan -->
<!--                             </div> -->
<!--                             <div class="alert alert-warning alert-alt"> -->
<!--                                 <small>1 day ago</small><br> -->
<!--                                 <i class="fa fa-exclamation fa-fw"></i> New restaurant opened at<br><strong>Andheri</strong> -->
<!--                             </div> -->
<!--                         </div> -->
                        <!-- END Sidebar Notifications -->
                    </div>
                    <!-- END Sidebar Content -->
                </div>
                <!-- END Wrapper for scrolling functionality -->
            </div>
            <!-- END Main Restaurant Sidebar -->
 
