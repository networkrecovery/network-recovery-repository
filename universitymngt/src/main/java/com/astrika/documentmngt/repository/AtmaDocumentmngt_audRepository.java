package com.astrika.documentmngt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.astrika.documentmngt.model.AtmaDocumentMaster_aud;


public interface AtmaDocumentmngt_audRepository  extends JpaRepository<AtmaDocumentMaster_aud, Long> {

	@Query("select count(*) from AtmaDocumentMaster_aud where lastModifiedBy.userId not in(24,97) and DATEDIFF(NOW(),lastModifiedOn) <30")
	int getLastMonthDownloads();

	@Query("select count(*) from AtmaDocumentMaster_aud where lastModifiedBy.userId not in(24,97) and DATEDIFF(NOW(),lastModifiedOn) between 30 and 60")
	int getsecondLastMonthDownloads();

	@Query("select d.documentId,d.lastModifiedOn,d.lastModifiedBy.firstName,d.lastModifiedBy.lastName,d.documentName,d.flag from AtmaDocumentMaster_aud d where DATEDIFF(now(),d.lastModifiedOn)<7 and d.lastModifiedBy.userId not in(24,97)")
	List<Object[]> getLastDownload();

	@Query("select d from AtmaDocumentMaster_aud d where d.flag=1")
	List<AtmaDocumentMaster_aud> getInactiveFlagLastAuditDownloads();
	
	@Modifying
	@Query("update AtmaDocumentMaster_aud set flag =0 where flag=1")
	void updateActiveAuditDownload();
	
}
