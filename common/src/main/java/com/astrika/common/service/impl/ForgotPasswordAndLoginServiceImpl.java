package com.astrika.common.service.impl;

import java.util.Date;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.astrika.common.exception.NoSuchUserException;
import com.astrika.common.model.User;
import com.astrika.common.model.mail.EmailData;
import com.astrika.common.service.ForgotPasswordAndLoginService;
import com.astrika.common.service.SendMailService;
import com.astrika.common.service.UserService;
import com.astrika.common.util.PropsValues;

@Service
public class ForgotPasswordAndLoginServiceImpl implements ForgotPasswordAndLoginService{
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private PropsValues props;
	
	@Autowired
	private SendMailService mailService;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Override
	public void sendForgetPasswordLink(String loginId, String url) throws NoSuchUserException {
		User user = userService.findByLoginId(loginId);
		double ran = Math.random() * Math.random();
		

		
		if(user != null){
			user.setPasswordResetKey(String.valueOf(ran));
			userService.save(user);
			String finalUrl = url+"/"+"ResetPassword?token="+ran;
			

			EmailData data = new EmailData();
			data.setTo(user.getEmailId());
			data.setSubject("Password Reset Link");
			data.setMailTemplate(props.forgotPassword);
			data.setFrom(props.emailFrom);
			HashMap<String, Object> model = new HashMap<>();
			model.put("UserName", user.getFirstName()+" "+user.getLastName());
			model.put("URL", finalUrl);
			data.setModel(model);
			mailService.sendMail(data);
		}
	}
	
	

	@Override
	public void sendForgotLoginMail(String emailId)  throws NoSuchUserException{
		User user = userService.findByEmailId(emailId);
		if(user != null){
			EmailData data = new EmailData();
			data.setTo(emailId);
			data.setFrom(props.emailFrom);
			data.setSubject("Login Details");
			data.setMailTemplate(props.forgotLogin);
			HashMap<String, Object> model = new HashMap<>();
			model.put("LoginId", user.getLoginId());
			model.put("UserName", user.getFirstName()+" "+user.getLastName());
			data.setModel(model);
			mailService.sendMail(data);
		}
	}



	@Override
	public void updatePassword(String token, String newPassword) throws NoSuchUserException {
		User user = userService.findByPasswordToken(token);
		if(user != null){
			user.setPassword(passwordEncoder.encode(newPassword));
			user.setPasswordModifiedDate(new Date());
			user.setPasswordResetKey(null);
			userService.save(user);
		}
		else{
			throw new NoSuchUserException();
		}
		
	}

	
}
