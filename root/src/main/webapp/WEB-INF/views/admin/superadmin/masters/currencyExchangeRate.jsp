<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@include file="../../inc/config.jsp"%>
<style>
.colwid {
 width: 20%;
}
.chosen-container {
	width: 250px !important;
}
	
</style>
	<div class="content-header">
		<div class="header-section">
			<h1>
				<i class="fa fa-map-marker"></i> <spring:message code="heading.currencyexchangeratemaster" /><br> <small><spring:message code="heading.specifylistofcurrencyexchangerates" />
					</small>
			</h1>
			<c:if test="${!empty success}">
				<div class="alert alert-success alert-dismissable">
					<button type="button" id="success_close" class="close" data-dismiss="alert" aria-hidden="true">x</button>
					<h4><i class="fa fa-check-circle"></i> <spring:message code="label.success"/></h4> <spring:message code="${success}"/>
				</div>
			</c:if>
			<span id="errorMsg"></span>
		</div>
	</div>
	<ul class="breadcrumb breadcrumb-top">
		<li><spring:message code="heading.managecurrencies" /></li>
		<li><a href="#"><spring:message code="heading.currencyexchangerate" /></a>
		</li>
	</ul>
	<div class="block full" id="gridView">
		<div class="block-title">
			<h2>
				<strong><spring:message code="heading.currencyexchangeratemaster" /></strong>
			</h2>
		</div>
		<form id="exgRate_form">
		<div class="table-responsive">
			<table 
				class="table table-vcenter table-condensed table-bordered">
				<thead>
					<tr>
						<th class="text-center"><spring:message code="label.id" /></th>
						<th class="text-center"><spring:message code="label.currencyfrom" /></th>
						<th class="text-center"><spring:message code="label.currencyto" /></th>
						<th class="text-center"><spring:message code="label.rate" /></th>
						<th class="text-center"><spring:message code="label.status" /></th>
						
					</tr>
				</thead>
				<tbody id="tbody">
					<c:forEach var="currentExchangeRate" items="${currencyExchangeRateList}"
						varStatus="count">

						<tr>
							<td class="text-center">${count.count}</td>
							<td class="text-left"><c:out
									value="${currentExchangeRate.currencyFrom.currencyName}" />
							</td>
							<td class="text-left"><c:out
									value="${currentExchangeRate.currencyTo.currencyName}" />
							</td>
							
							<td class="text-center colwid">
							<input value="${currentExchangeRate.rate}" class="form-control" style="width :100%;" type="text" type="text" name="currency_${currentExchangeRate.id}"/></td>
	                        
	                        <td class="text-left">
	                       
								<c:choose>
									<c:when test="${currentExchangeRate.active}">
	                                    Active
	                                 </c:when>
									<c:otherwise>
									Inactive
									</c:otherwise>
	                             </c:choose>
							</td>					
						</tr>
					</c:forEach>

				</tbody>
			</table>
			<div class="form-group form-actions">
					<div class="col-md-9 col-md-offset-3">
						<div id="currencyExchangeRate_submit" class="btn btn-sm btn-primary" style="margin-top: -16px;">
							<i class="fa fa-angle-right"></i> <spring:message code="button.save" />
						</div>
					</div>
		</div>
		</div>
		</form>
	</div>

<script src="<%=contexturl%>resources/js/pages/tablesDatatables.js"></script>
<script>$(function(){ TablesDatatables.init(4); });</script>

<script type="text/javascript">
$(document).ready(function() {
	$("#currencyExchangeRate_submit").click(function(){
		var data = $("#exgRate_form").serialize();
		
		//alert(data);
		$.ajax({
			type: "POST",
			async: false,
			url: "<%=contexturl%>Admin/SaveCurrencyExchangeRate",
			data:data,
			success: function(data, textStatus, jqXHR){
				var i = data.trim().indexOf("errorCode");
				if(i == 2){
					var obj = $.parseJSON(data.trim());
					var html = '<div class="alert alert-danger alert-dismissable">	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="fa fa-times-circle"></i> Error</h4> <span >'+obj.errorCode+'</span></div>';
					$("#errorMsg").html(html).show();
					$("#errorMsg").html(html).fadeOut(8000);
					$('html, body').animate({ scrollTop: $("#errorMsg").offset().top },500);
				}
				else{
					$('html, body').animate({ scrollTop: $("#errorMsg").offset().top },500);
					$('#page-content').html(data);
				}
			},
			dataType: 'html'
		});
	});
});

</script>