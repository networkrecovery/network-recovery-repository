<%@include file="../inc/config.jsp"%>
<%@include file="../inc/template_start.jsp"%>
<%@include file="../inc/page_head.jsp"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<link rel="stylesheet" type="text/css" href="<%=contexturl %>resources/css/jquery.cleditor.css" />
<style>

.cleditorToolbar {
	background: url('<%=contexturl %>resources/img/cleditor/toolbar.gif') repeat;
}

.cleditorButton {
	float: left;
	width: 24px;
	height: 24px;
	margin: 1px 0 1px 0;
	background: url('<%=contexturl %>resources/img/cleditor/buttons.gif');
}

.cleditorMain {
	height: 100% !important;
	width: auto;
}
</style>


<div id="page-content">
	<div class="content-header">
		<div class="header-section">
			<c:if test="${!empty error}">
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-times-circle"></i>
						<spring:message code="label.error" />
					</h4>
					<spring:message code="${error}" />
				</div>
			</c:if>

			<h1>Survey Result Data</h1>
			<span id="errorMsg"></span>
		</div>
	</div>

	<form action="<%=contexturl%>ManageResultData/ResultDataList/UpdateData"
		method="post" class="form-horizontal ui-formwizard"
		id="SurveyResultForm" enctype="multipart/form-data">
		<div class="row">
		<input name="resultId" value="${resultSet.resultId}" type="hidden">
			<div class="">
				<div class="block">
					<div class="block-title">
						<h2>
							<strong>Result Data</strong>
						</h2>
					</div>

					<div class="form-group" id="moduleDiv">
						<label class="col-md-4 control-label" for="chosenModuleId"><spring:message
								code="label.modulename" /><span class="text-danger">*</span></label>
						<div class="col-md-6">
							<select class="module_chosen" style="width: 200px;"
								id="chosenModuleId"
								data-placeholder="<spring:message code='label.choosemodule' />"
								name="chosenModuleId" disabled="disabled">
								<option value=""></option>
								<c:forEach items="${moduleList}" var="module">
									<option value="${module.moduleId}"
										<c:if test="${module.moduleId eq resultSet.module.moduleId}">selected="selected"</c:if>>${module.moduleName}</option>
								</c:forEach>
							</select>
						</div>
					</div>

					<div class="form-group" id="lifestageDiv1">
						<label class="col-md-4 control-label" for="chosenLifeStage1"><spring:message
								code="label.lifestage" /><span class="text-danger">*</span></label>
						<div class="col-md-6">
							<select class="lifestage_chosen1" style="width: 200px;"
								id="chosenLifeStage1"
								data-placeholder="<spring:message code='label.chooselifestage' />"
								name="chosenLifeStage1" disabled="disabled">
								<option value=""></option>
								<c:forEach items="${lifeStageList}" var="lifeStages">
									<option value="${lifeStages}"
										<c:if test="${lifeStages eq resultSet.lifeStages}">selected="selected"</c:if>>${lifeStages.name}</option>
								</c:forEach>
							</select>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label" for="surveyResultData">Result
							data<span class="text-danger">*</span>
						</label>
						<div class="col-md-6">
							<textarea id="surveyResultDataId" name="surveyResultData">${resultSet.resultdata}</textarea>
						</div>
					</div>
				</div>
			</div>


		</div>

		<div style="text-align: center; margin-bottom: 10px">
			<div class="form-group form-actions">
				<!-- 				<div class="col-md-9 col-md-offset-3"> -->
				<button id="company_submit" type="submit"
					class="btn btn-sm btn-primary save">
					<i class="fa fa-angle-right"></i>
					<spring:message code="button.save" />
				</button>
				<a class="btn btn-sm btn-primary save"
					href="<%=contexturl%>ManageResultData/ResultDataList"> <spring:message
						code="button.cancel" />
				</a>
			</div>
		</div>
	</form>
</div>

<%@include file="../inc/page_footer.jsp"%>
<%@include file="../inc/template_scripts.jsp"%>
<%-- <%@include file="../inc/chosen_scripts.jsp"%> --%>
<script src="<%=contexturl %>resources/js/custom/featurecontrol.js"></script>
<script src="http://jqueryvalidation.org/files/dist/additional-methods.min.js"></script>

<script type="text/javascript" src="<%=contexturl %>resources/js/jquery.cleditor.js"></script>

<script type="text/javascript">
	$(document)
			.ready(
					function() {
						
						$("#manageResultData").addClass("active");
					
						$(".module_chosen").data("placeholder","Select Module From...").chosen();	
						$(".lifestage_chosen1").data("placeholder","Select LifeStage From...").chosen();
						$("#surveyResultDataId").cleditor(); 
						
						$("#SurveyResultForm").validate(
						{	errorClass:"help-block animation-slideDown",
							errorElement:"div",
							errorPlacement:function(e,a){a.parents(".form-group > div").append(e)},
							highlight:function(e){$(e).closest(".form-group").removeClass("has-success has-error").addClass("has-error")},
							success:function(e){e.closest(".form-group").removeClass("has-success has-error")},
										rules : {
											
											chosenModuleId: {
												required : !0
											},
											chosenLifeStage1: {
												required : !0
											},
											surveyResultData : {
												required : !0,
											}
											
										},
										messages : {
											
											chosenModuleId: {
												required : '<spring:message code="validation.selectmodule"/>'
											},
											chosenLifeStage1: {
												required: 'Please select a Life Stage'
											},
											surveyResultData : {
												required : 'Please enter some data'
											}											
										},
								});

						$("#company_submit").click(function() {
							var surveyData = $("#surveyResultDataId").val();
							$("#surveyresultid").val(surveyData);
							unsaved=false;
							$("#SurveyResultForm").submit();
						});
						

						
					});
	

</script>

<style>
.chosen-container {
	width: 250px !important;
}
#map-canvas {
	height: 300px
}
</style>

<%@include file="../inc/template_end.jsp"%>