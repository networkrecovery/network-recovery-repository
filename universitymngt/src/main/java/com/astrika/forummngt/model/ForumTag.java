package com.astrika.forummngt.model;

public enum ForumTag {
	
	
	FOR_ADMIN(1, "For Admin"),
	PROBLEM(2, "HELP!"),
	ADVICE(3, "GYAN"),
	QUESTION(4, "Question"),
	FYI(5, "FYI"),
	GENERAL(6, "General");
	
	private final int id;
	private String name;

	private ForumTag(int id, String name) {
		this.id = id;
		this.name = name;
	}

	public int getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}

	public static ForumTag fromInt(int id){
		switch(id){
		case 1:
			return FOR_ADMIN;
		case 2:
			return PROBLEM;
		case 3:
			return ADVICE;
		case 4:
			return QUESTION;
		case 5:
			return FYI;
		case 6:
			return GENERAL;

		default:
			return null;
		}
	}

}
