package com.astrika.globalsponsoredmngt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.astrika.globalsponsoredmngt.model.GlobalSponsoredManagement;

public interface GlobalSponsoredRepository extends JpaRepository<GlobalSponsoredManagement, Long>{
	
		@Query("select g from GlobalSponsoredManagement g left join fetch g.createdBy where g.active=?1")
		List<GlobalSponsoredManagement> findByActive(boolean active);
		
		@Query("select g from GlobalSponsoredManagement g left join fetch g.createdBy where g.globalId = ?1")
		GlobalSponsoredManagement findByImageId(long imageId);

		@Query("select g from GlobalSponsoredManagement g where g.imageType = ?1")
		GlobalSponsoredManagement findByImageType(String imageType);
		
		@Query("select g from GlobalSponsoredManagement g left join fetch g.createdBy where g.active=?1 and g.imageType=?2")
		List<GlobalSponsoredManagement> findByActiveandImageTypeGlobalSponsor(boolean active, String imageType);
		
		@Query("select g from GlobalSponsoredManagement g left join fetch g.createdBy where g.active=?1 and g.imageType=?2")
		GlobalSponsoredManagement findByActiveandImageTypeContact(boolean active, String imageType);
}
