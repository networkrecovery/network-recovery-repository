package com.astrika.common.service.impl;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.astrika.common.exception.FileNameException;
import com.astrika.common.exception.NoSuchImageException;
import com.astrika.common.model.ImageMaster;
import com.astrika.common.repository.ImageRepository;
import com.astrika.common.service.ImageService;
import com.astrika.common.util.ImageUtil;
import com.astrika.common.util.PropsValues;
import com.astrika.kernel.exception.BusinessException;
import com.astrika.kernel.exception.SystemException;
import com.astrika.kernel.util.CharPool;
import com.astrika.kernel.util.StringPool;

/**
 * Service implementation for Image.
 * 
 * @author Priyanka
 */
@Service
public class ImageServiceImpl implements ImageService{
	
	@Autowired
	private ImageRepository imageRepository;
	
	@Autowired
	private ImageUtil imageUtil;
	
	@Autowired
	PropsValues propertyValues;
	
	
	
	@Override
	public ImageMaster addInstitutionImage(byte[] imageInByteArray,String fileFormat,String fileName,boolean applyWaterMark,int...saveDirStructure) throws SystemException,BusinessException,IOException{
		if (!isValidName(fileName)) {
			throw new FileNameException(fileName);
		}
		String locationToSave=imageUtil.getAbsoluteSaveLocationDir(saveDirStructure);
		return addInstitutionImage(imageInByteArray, fileFormat, fileName, locationToSave,applyWaterMark);
	}
	
	@Override
	public ImageMaster addInstitutionImage(byte[] imageInByteArray,String fileFormat,String fileName,boolean memberProfileImage,boolean applyWaterMark,String...saveDirStructure) throws SystemException,BusinessException,IOException{
		if (!isValidName(fileName)) {
			throw new FileNameException(fileName);
		}
		if(memberProfileImage){
			String locationToSave=imageUtil.getAbsoluteSaveLocationDir(saveDirStructure);
			return addMemberImage(imageInByteArray, fileFormat, fileName, locationToSave);
			
		}
	
		String locationToSave=imageUtil.getAbsoluteSaveLocationDir(saveDirStructure);
		return addInstitutionImage(imageInByteArray, fileFormat, fileName, locationToSave,applyWaterMark);
	}
	
	@Override
	public ImageMaster addAdImage(byte[] imageInByteArray,String fileFormat,String fileName,String...saveDirStructure) throws SystemException,BusinessException,IOException{
		if (!isValidName(fileName)) {
			throw new FileNameException(fileName);
		}
		String locationToSave=imageUtil.getAbsoluteSaveLocationDir(saveDirStructure);
		ArrayList<File> file=(ArrayList<File>) imageUtil.saveAd(imageInByteArray, fileFormat, fileName,locationToSave);
		return saveImage(file.get(0),null,fileName,null);
	
	}
	
//	@Override
//	public ImageMaster addMemberImage(byte[] imageInByteArray,String fileFormat,String fileName,String...saveDirStructure) throws SystemException,BusinessException,IOException{
//		if (!isValidName(fileName)) {
//			throw new FileNameException(fileName);
//		}
//		String locationToSave=imageUtil.getAbsoluteSaveLocationDir(saveDirStructure);
//		return addMemberImage(imageInByteArray, fileFormat, fileName, locationToSave);
//	}
	
	/**
	 * private method that saves the User image.This method is internally being called by other methods
	 * 
	 * @param imageInByteArray
	 * @param fileFormat
	 * @param fileName
	 * @param locationToSave
	 * @return {@link ImageMaster}
	 * @throws IOException
	 * @throws BusinessException
	 * 
	 * @author Deb
	 */
	private ImageMaster addMemberImage(byte[] imageInByteArray,String fileFormat,String fileName,String locationToSave) throws IOException, BusinessException
	{
		File file=imageUtil.saveMember(imageInByteArray, fileFormat, fileName,locationToSave);
		return saveMemberImage(file,fileName,null);
	}
	
	/**
	 * private method that saves the restaurant image.This method is internally being called by other methods
	 * 
	 * @param imageInByteArray
	 * @param fileFormat
	 * @param fileName
	 * @param locationToSave
	 * @return {@link ImageMaster}
	 * @throws IOException
	 * @throws BusinessException
	 * 
	 * @author Deb
	 */
	private ImageMaster addInstitutionImage(byte[] imageInByteArray,String fileFormat,String fileName,String locationToSave,boolean applyWaterMark) throws IOException, BusinessException
	{
		ArrayList<File> file=(ArrayList<File>) imageUtil.saveInstitution(imageInByteArray, fileFormat, fileName,locationToSave,applyWaterMark);
		return saveImage(file.get(0),file.get(1),fileName,null);
	}
	@Override
	public ImageMaster addMenuImage(byte[] imageInByteArray, String fileFormat,
			String fileName,int...saveDirStructure) throws SystemException, BusinessException,
			IOException {
		if (!isValidName(fileName)) {
			throw new FileNameException(fileName);
		}
		String locationToSave=imageUtil.getAbsoluteSaveLocationDir(saveDirStructure);
		return addMenuImage(imageInByteArray, fileFormat, fileName, locationToSave);
	}
	
	@Override
	public ImageMaster addMenuImage(byte[] imageInByteArray, String fileFormat,
			String fileName,String...saveDirStructure) throws SystemException, BusinessException,
			IOException {
		if (!isValidName(fileName)) {
			throw new FileNameException(fileName);
		}
		String locationToSave=imageUtil.getAbsoluteSaveLocationDir(saveDirStructure);
		return addMenuImage(imageInByteArray, fileFormat, fileName, locationToSave);
	}
	
	/**
	 * private method that saves the restaurant menu image.This method is internally being called by other methods
	 * 
	 * @param imageInByteArray
	 * @param fileFormat
	 * @param fileName
	 * @param locationToSave
	 * @return {@link ImageMaster}
	 * @throws IOException
	 * @throws BusinessException
	 * 
	 * @author Deb
	 */
	private ImageMaster addMenuImage(byte[] imageInByteArray, String fileFormat,
			String fileName,String locationTosave) throws IOException, BusinessException
	{
		ArrayList<File> file=(ArrayList<File>) imageUtil.saveMenu(imageInByteArray, fileFormat, fileName,locationTosave);
		return saveImage(file.get(0),file.get(1),fileName,null);
	}

	@Override
	public ImageMaster addEviteImage(byte[] imageInByteArray,
			String fileFormat, String fileName,int...saveDirStructure) throws SystemException,
			BusinessException, IOException {
		if (!isValidName(fileName)) {
			throw new FileNameException(fileName);
		}
		String locationToSave=imageUtil.getAbsoluteSaveLocationDir(saveDirStructure);
		ArrayList<File> file=(ArrayList<File>) imageUtil.saveEvite(imageInByteArray, fileFormat, fileName,locationToSave);
		return saveImage(file.get(0),file.get(1),fileName,null);
	}
	
	@Override
	public ImageMaster updateRestaurantImage(long oldImageId,byte[] imageInByteArray,String fileFormat,String fileName) throws SystemException,BusinessException, IOException{
		if (!isValidName(fileName)) {
			throw new FileNameException(fileName);
		}
		ImageMaster image= findById(oldImageId);
		String locationToSave=deleteFromPhysicalLocation(image);
		ArrayList<File> file=(ArrayList<File>) imageUtil.saveInstitution(imageInByteArray, fileFormat, fileName,locationToSave,true);
		return saveImage(file.get(0),file.get(1),fileName,image);
	}
	
	@Override
	public ImageMaster updateMenuImage(long oldImageId,byte[] imageInByteArray,String fileFormat,String fileName) throws SystemException,BusinessException, IOException{
		if (!isValidName(fileName)) {
			throw new FileNameException(fileName);
		}
		ImageMaster image= findById(oldImageId);
		String locationToSave=deleteFromPhysicalLocation(image);
		ArrayList<File> file=(ArrayList<File>) imageUtil.saveInstitution(imageInByteArray, fileFormat, fileName,locationToSave,true);
		return saveImage(file.get(0),file.get(1),fileName,image);
	}
	
	@Override
	public ImageMaster updateEviteImage(long oldImageId,byte[] imageInByteArray,String fileFormat,String fileName) throws SystemException,BusinessException, IOException{
		if (!isValidName(fileName)) {
			throw new FileNameException(fileName);
		}
		ImageMaster image= findById(oldImageId);
		String locationToSave=deleteFromPhysicalLocation(image);
		ArrayList<File> file=(ArrayList<File>) imageUtil.saveEvite(imageInByteArray, fileFormat, fileName,locationToSave);
		return saveImage(file.get(0),file.get(1),fileName,image);
	}
	
	public String delete(long imageId) throws UnsupportedEncodingException, NoSuchImageException{
		ImageMaster image= findById(imageId);
		String s = deleteFromPhysicalLocation(image);
		
		return s;
	}
	/**
	 * Delete from physical location
	 * @param image containing the information of the file to delete from physical location
	 * @return the location of the file deleted
	 * @throws UnsupportedEncodingException
	 * @author Debjyoti Nath
	 */
	private String deleteFromPhysicalLocation(ImageMaster image)throws UnsupportedEncodingException
	{
		//delete image file
		String rootPath=imageUtil.getLocationToFilesDirectory();
		if(rootPath.contains(propertyValues.imageSaveRootDir))
			rootPath=rootPath.substring(0,rootPath.indexOf(propertyValues.imageSaveRootDir));
		rootPath=rootPath.replace("\\", "/");
		rootPath=rootPath.substring(0,rootPath.lastIndexOf(CharPool.FORWARD_SLASH));
		String filesLocation=rootPath+image.getImagePath();
		String locationToSave=filesLocation.substring(0,filesLocation.lastIndexOf(CharPool.FORWARD_SLASH));
		imageUtil.deleteFile(filesLocation);
		
		//delete thumbnailimage
		
		filesLocation=rootPath+image.getThumbImagePath();
		locationToSave=filesLocation.substring(0,filesLocation.lastIndexOf(CharPool.FORWARD_SLASH));
		imageUtil.deleteFile(filesLocation);
		return locationToSave;
	}
	
	public ImageMaster restore(long imageId){
		ImageMaster image=imageRepository.findOne(imageId);
		image.setActive(true);
		return imageRepository.save(image);
	}
	
	public ImageMaster findById(long imageId) throws NoSuchImageException{
		return imageRepository.findOne(imageId);
	}
	
	
	
	/**
	 * Saves or updates an existing image.This method first validates the image 
	 * @param file
	 * @param fileName
	 * @param imageMaster
	 * @return ImageMaster
	 * @throws BusinessException
	 * @throws UnsupportedEncodingException 
	 */
	private ImageMaster saveMemberImage(File file,String fileName,ImageMaster imageMaster) throws BusinessException, UnsupportedEncodingException
	{
		String fileNameVal=file.getName();
		ImageMaster image=null;
		if(imageMaster!=null && imageMaster.getImageId()>0){
			image=imageMaster;
		}
		else
		{
			image= new ImageMaster();
		}
		
		if (file != null) {
			fileNameVal=validate(fileNameVal, true, file);
			image.setSize(file.length());
		}
		else {
			image.setSize(0);
		}
		image.setImageName(fileNameVal);
		image.setExtension(fileNameVal.substring(file.getName().lastIndexOf(StringPool.PERIOD)+1));
		String imagePath=imageUtil.getRelativePath(file.getAbsolutePath()).replace("\\", "/");
		imagePath= imagePath.replace("/Files", "Files");
		image.setImagePath(imagePath);
		fileNameVal=validate(fileNameVal, true, file);
		return imageRepository.save(image);
	}
	
	
	/**
	 * Saves or updates an existing image.This method first validates the image 
	 * @param file
	 * @param fileName
	 * @param imageMaster
	 * @return
	 * @throws BusinessException
	 * @throws UnsupportedEncodingException 
	 */
	private ImageMaster saveImage(File file,File thumbnail,String fileName,ImageMaster imageMaster) throws BusinessException, UnsupportedEncodingException
	{
		String fileNameVal=file.getName();
		ImageMaster image=null;
		if(imageMaster!=null && imageMaster.getImageId()>0){
			image=imageMaster;
		}
		else
		{
			image= new ImageMaster();
		}
		
		if (file != null) {
			fileNameVal=validate(fileNameVal, true, file);
			image.setSize(file.length());
		}
		else {
			image.setSize(0);
		}

		image.setImageName(fileNameVal);
		image.setExtension(fileNameVal.substring(fileNameVal.lastIndexOf(StringPool.PERIOD)+1));
		String imagePath=imageUtil.getRelativePath(file.getAbsolutePath()).replace("\\", "/");
		if(thumbnail!=null)
		image.setThumbImagePath(imageUtil.getRelativePath(thumbnail.getAbsolutePath()).replace("\\", "/"));
		image.setImagePath(imagePath);
		fileNameVal=validate(fileNameVal, true, file);
		
		return imageRepository.save(image);
	}
	
	public String validate(
			String fileName, boolean validateFileExtension, InputStream is)
		throws BusinessException, SystemException {

		return validate(fileName, validateFileExtension);


//		try {
//			if ((is == null) ||
//				((PrefsPropsUtil.getLong(PropsKeys.DL_FILE_MAX_SIZE) > 0) &&
//				 (is.available() >
//					PrefsPropsUtil.getLong(PropsKeys.DL_FILE_MAX_SIZE)))) {
//
//				throw new FileSizeException(fileName);
//			}
//		}
//		catch (IOException ioe) {
//			throw new FileSizeException(ioe.getMessage());
//		}
	}
	
	private String validate(String fileName, boolean validateFileExtension, File file) throws BusinessException{

		return validate(fileName, validateFileExtension);

//		if ((PrefsPropsUtil.getLong(PropsKeys.DL_FILE_MAX_SIZE) > 0) &&
//			((file == null) ||
//			 (file.length() >
//				PrefsPropsUtil.getLong(PropsKeys.DL_FILE_MAX_SIZE)))) {
//
//			throw new FileSizeException(fileName);
//		}
	}
	
	public String validate(String fileName, boolean validateFileExtension) throws BusinessException {

			if (!isValidName(fileName)) {
				throw new FileNameException(fileName);
			}
			if(fileName.contains(" "))
			{
				fileName=fileName.replace(" ", "_");
			}
			
			return fileName;

//			if (validateFileExtension) {
//				boolean validFileExtension = false;
//
//				String[] fileExtensions = PrefsPropsUtil.getStringArray(
//					PropsKeys.DL_FILE_EXTENSIONS, StringPool.COMMA);
//
//				for (int i = 0; i < fileExtensions.length; i++) {
//					if (StringPool.STAR.equals(fileExtensions[i]) ||
//						StringUtil.endsWith(fileName, fileExtensions[i])) {
//
//						validFileExtension = true;
//
//						break;
//					}
//				}
//
//				if (!validFileExtension) {
//					throw new FileExtensionException(fileName);
//				}
//			}
		}

	protected boolean isValidName(String name) {
		if ((name == null) ||
			name.contains("\\") ||
			name.contains("\\\\") ||
			name.contains("//") ||
			name.contains(":") ||
			name.contains("*") ||
			name.contains("?") ||
			name.contains("\"") ||
			name.contains("<") ||
			name.contains(">") ||
			name.contains("|") ||
			name.contains("[") ||
			name.contains("]") ||
			name.contains("../") ||
			name.contains("/..")) {

			return false;
		}

		return true;
	}
	
	public ImageMaster update(ImageMaster image){
		return imageRepository.save(image);
	}

	@Override
	public ImageMaster addInstructorImage(byte[] imageInByteArray, String fileFormat,
			String fileName, boolean memberProfileImage, boolean applyWaterMark,
			String... saveDirStructure) throws BusinessException, IOException {
		
		if (!isValidName(fileName)) {
			throw new FileNameException(fileName);
		}
		String locationToSave=imageUtil.getAbsoluteSaveLocationDir(saveDirStructure);
		return addMemberImage(imageInByteArray, fileFormat, fileName,locationToSave);
	}

	@Override
	public ImageMaster updateInstructorImage(Long imageId, byte[] byteArray,
			String ext, String fileName) throws BusinessException, IOException {
		if (!isValidName(fileName)) {
			throw new FileNameException(fileName);
		}
		ImageMaster image= findById(imageId);
		String locationToSave=deleteFromPhysicalLocation(image);
		ArrayList<File> file=(ArrayList<File>) imageUtil.saveInstructor(byteArray, ext, fileName,locationToSave,true);
		return saveImage(file.get(0),file.get(1),fileName,image);
	}
	
	
	
	public ImageMaster addImage(byte[] imageInByteArray, String fileFormat,
			String fileName, boolean memberProfileImage, boolean applyWaterMark,
			String... saveDirStructure) throws BusinessException, IOException {
		
		if (!isValidName(fileName)) {
			throw new FileNameException(fileName);
		}
		String locationToSave=imageUtil.getAbsoluteSaveLocationDir(saveDirStructure);
		return addMemberImage(imageInByteArray, fileFormat, fileName,locationToSave);
	}
	
}
