<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/template_start.jsp"%>
<%@include file="../../inc/page_head.jsp"%>

<div id="page-content">
<div class="content-header">
	<div class="header-section">
		<h1>
			<i class="fa fa-map-marker"></i> <spring:message code="heading.freemember" /><br> <small><spring:message code="heading.specifylistoffreemember" />
				</small>
		</h1>
		<c:if test="${!empty success}">
			<div class="alert alert-success alert-dismissable">
				<button type="button" class="close" data-dismiss="alert"
					aria-hidden="true">x</button>
				<h4>
					<i class="fa fa-check-circle"></i> <spring:message code="label.success"/>
				</h4>
				<spring:message code="${success}" />
			</div>
		</c:if>
		<c:if test="${!empty error}">
			<div class="alert alert-danger alert-dismissable">
				<button type="button" class="close" data-dismiss="alert"
					aria-hidden="true">x</button>
				<h4>
					<i class="fa fa-times-circle"></i> <spring:message code="label.error"/>
				</h4>
				${error}
			</div>
		</c:if>
	</div>
</div>
<ul class="breadcrumb breadcrumb-top">
	<li><spring:message code="heading.managemember" /></li>
	<li><a href="#"><spring:message code="heading.freemember" /></a></li>
</ul>
<div class="block full gridView">
	<div class="block-title">
		<h2>
			<strong><spring:message code="heading.activemember" /></strong> 
		</h2>
	</div>
	<div class="table-responsive">
		<table id="active-datatable"
			class="table table-vcenter table-condensed table-bordered">
			<thead>
				<tr>
					<th class="text-center"><spring:message code="label.id" /></th>
					<th class="text-center"><spring:message code="label.name" /></th>
					<th class="text-center"><spring:message code="label.email" /></th>
					<th class="text-center"><spring:message code="label.mobile" /></th>
					<th class="text-center"><spring:message code="label.actions" /></th>
				</tr>
			</thead>
			<tbody id="tbody">
				<c:forEach var="currentMember" items="${activeMembers}" varStatus="count">
					<tr>
						<td class="text-center">${count.count}</td>
						<td class="text-left"><c:out
								value="${currentMember.user.fullName}" /></td>
						<td class="text-left"><c:out
								value="${currentMember.user.emailId}" /></td>
						<td class="text-right"><c:out
								value="${currentMember.user.mobile}" /></td>
						<td class="text-center">
<!-- 							<div class="btn-group"> -->
<!-- 								<a href="#delete_member_popup" data-toggle="modal" -->
<%-- 									onclick="deleteMember(${currentMember.user.userId})" title="Delete" --%>
<!-- 									class="btn btn-xs btn-danger"><i class="fa fa-times"></i></a> -->
<!-- 							</div> -->
							<div class="btn-group">
									<a href="javascript:void(0)" data-toggle="dropdown"
										class="btn btn-primary dropdown-toggle"><spring:message code="label.actions"/><span
										class="caret"></span></a>
									<ul class="dropdown-menu text-left">
										<li><a href="#delete_member_popup" data-toggle="modal"
											onclick="deleteMember(${currentMember.user.userId})"
											title="Delete"><spring:message
													code="label.deleteuser" /></a></li>
										<li><a href="<%=contexturl %>ManageMembers/ShowMemberList/ViewMember/${currentMember.user.userId}"><spring:message code="label.viewprofile"/></a></li>
									</ul>
								</div>
						</td>
					</tr>
				</c:forEach>

			</tbody>
		</table>
	</div>
</div>


	<div class="block full gridView">
		<div class="block-title">
			<h2>
				<strong><spring:message code="heading.inactivemember" /></strong>
			</h2>
		</div>
		<div class="table-responsive">
			<table id="inactive-datatable"
				class="table table-vcenter table-condensed table-bordered ">
				<thead>
					<tr>
						<th class="text-center"><spring:message code="label.id" /></th>
						<th class="text-center"><spring:message code="label.name" /></th>
						<th class="text-center"><spring:message code="label.email" /></th>
						<th class="text-center"><spring:message code="label.mobile" /></th>
						<th class="text-center"><spring:message code="label.actions" /></th>
					</tr>
				</thead>
				<tbody id="tablebody">
					<c:forEach var="currentMember" items="${inActiveMembers}"
						varStatus="count">
						<tr>
							<td class="text-center">${count.count}</td>
						<td class="text-left"><c:out value="${currentMember.user.fullName}" /></td>
						<td class="text-left"><c:out value="${currentMember.user.emailId}" /></td>
						<td class="text-right"><c:out 	value="${currentMember.user.mobile}" /></td>
							<td class="text-center">
								<div class="btn-group">
									<a href="#restore_Member_popup" data-toggle="modal"
										onclick="restoreMember(${currentMember.user.userId})"
										title="Restore" class="btn btn-xs btn-danger"><i
										class="fa fa-times"></i> </a>
								</div>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>



<div id="delete_member_popup" class="modal fade" tabindex="-1"
	role="dialog" aria-hidden="true" >
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<form action="<%=contexturl %>ManageMembers/ShowMemberList/InActivateMember" method="post" class="form-horizontal form-bordered"
					id="delete_member_Form">
					<div style="padding: 10px; height: 110px;">
						<label><spring:message code="validation.inactiveuser"/></label>
						<div class="col-xs-12 text-right">
							<button type="button" class="btn btn-sm btn-default"
								data-dismiss="modal"><spring:message code="label.no"/></button>
							<div id="delete_Member" class="btn btn-sm btn-primary"><spring:message code="label.yes"/></div>
						</div>
					</div>
                      <input type="hidden" name="id" id="deletememberId">
                      <input type="hidden"  name="memberType" value="<%=MemberType.FREE_INDIVIDUAL.getId() %>">
				</form>
			</div>
		</div>
	</div>
</div>

<div id="restore_Member_popup" class="modal fade" tabindex="-1"
		role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<form action="<%=contexturl %>ManageMembers/ShowMemberList/ActivateMember" method="post" class="form-horizontal form-bordered" id="restore_member_Form">
						<div style="padding: 10px; height: 110px;">
							<label><spring:message
									code="validation.activeuser" /></label>
							<div class="col-xs-12 text-right">
								<button type="button" class="btn btn-sm btn-default"
									data-dismiss="modal"><spring:message code="label.no" /></button>
								<div id="restore_Member" class="btn btn-sm btn-primary">	<spring:message code="label.yes" /></div>
							</div>
						</div>
						<input type="hidden"  name="id" id="restorememberId">
						<input type="hidden"  name="memberType" value="<%=MemberType.FREE_INDIVIDUAL.getId() %>">
					</form>
				</div>
			</div>
		</div>
	</div>

</div>

<%@include file="../../inc/page_footer.jsp"%>
<%@include file="../../inc/template_scripts.jsp"%>

<script src="<%=contexturl %>resources/js/pages/activeTablesDatatables.js"></script>
<script src="<%=contexturl %>resources/js/pages/inactiveTablesDatatables.js"></script>
<script>$(function(){ ActiveTablesDatatables.init(4); });</script>
<script>$(function(){ InActiveTablesDatatables.init(4); });</script>


<script type="text/javascript">
	var selectedId = 0;
	$(document).ready(function() {
		
        $("#restore_Member").click(function(){
			
			$("#restorememberId").val(restoreId);
			$("#restore_member_Form").submit();
		});
		
         $("#delete_Member").click(function(){
			
			$("#deletememberId").val(selectedId);
			$("#delete_member_Form").submit();
		});
		
	});
	
	function deleteMember(id){
		selectedId = id;
	}
	
	function restoreMember(id){
		restoreId = id;
	}
</script>
<%@include file="../../inc/template_end.jsp"%>