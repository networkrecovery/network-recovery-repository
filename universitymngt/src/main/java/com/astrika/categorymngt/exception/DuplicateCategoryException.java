package com.astrika.categorymngt.exception;

import com.astrika.kernel.exception.BusinessException;
import com.astrika.kernel.exception.CustomException;

public class DuplicateCategoryException extends BusinessException {


	public DuplicateCategoryException() {
		super(CustomException.DUPLICATE_CATEGORY.getCode());
	}

	public DuplicateCategoryException(String errorCode) {
		super(errorCode);
	}
}