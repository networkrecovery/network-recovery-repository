<!DOCTYPE html>
<html lang="en">
<%@include file="/WEB-INF/views/admin/inc/config.jsp"%>
<%-- <%@include file="/WEB-INF/views/admin/inc/template_start.jsp"%> --%>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Atma Network</title>
<!-- Style -->
<!-- Bootstrap -->
<link rel="shortcut icon" href="<%=contexturl %>resources/img/favicon.ico">
<link href="<%=contexturl%>resources/css/bootstrap.css" rel="stylesheet"
	media="screen">
<link href="<%=contexturl%>resources/css/fontface.css" rel="stylesheet"
	media="screen">
<link href="<%=contexturl%>resources/css/font-awesome.css"
	rel="stylesheet" media="screen">
<link href="<%=contexturl%>resources/css/jquery.bxslider.css"
	rel="stylesheet" media="screen">
<!--[if IE 9]>
  <script src="<%=contexturl%>resources/js/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="<%=contexturl%>resources/js/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<!--[if IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

<link href="<%=contexturl%>resources/css/style.css" rel="stylesheet"
	media="screen">
</head>


<body>
	<div class="header-bg">
		<nav class="navbar">
			<div class="container">
				<div class="navbar-header">
					<img src="<%=contexturl%>resources/img/atma.jpg" />
				</div>
				<div id="navbar" class="mynav">
					<ul class="nav navbar-nav pull-right">
						<li class="active"><a href="Login"><b>Back to Login
									Page</b></a></li>
					</ul>
				</div>
			</div>
		</nav>
		<div class="blue-container">
			<div class="container">
				
				<div class="row blue">
					<div class="col-md-10">
						<div class="media">
							<a class="pull-left"><img class="media-object"
								src="<%=contexturl%>resources/img/Signin_Thumbs.png"
								alt="Organisation Name"> </a>
							<div class="media-body text-st">
								<p>Register here</p>
								<h4 class="media-heading">Organisation Registration</h4>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<br>

	<div class="container">
		<div class="row">
<%-- 		<c:if test="${!empty success}"> --%>
<!-- 					<div class="alert alert-success alert-dismissable"> -->
<!-- 						<button type="button" class="close" data-dismiss="alert" -->
<!-- 							aria-hidden="true">x</button> -->
<!-- 						<h4> -->
<!-- 							<i class="fa fa-check-circle"></i> -->
<%-- 							<spring:message code="label.success" /> --%>
<!-- 						</h4> -->
<%-- 						<spring:message code="${success}" /> --%>
<!-- 					</div> -->
<%-- 				</c:if> --%>
				<c:if test="${!empty error}">
					<div class="alert alert-danger alert-dismissable">
						<button type="button" class="close" data-dismiss="alert"
							aria-hidden="true">x</button>
						<h4>
							<i class="fa fa-times-circle"></i>
							<spring:message code="label.error" />
						</h4>
						<spring:message code="${error}" />
					</div>
				</c:if>
			<form action="SaveEnquiry" method="post" class="form-horizontal "
				id="member_form" enctype="multipart/form-data">
				<div class="col-md-7">
                    <div class="block full">
                        <div class="block-title">
                            <h4 class="">Personal Details</h4>
                        </div>
						<div class="form-group">
							<label class="col-sm-3 control-label">First Name<span
								class="mandatory">*</span>
							</label>
							<div class="col-sm-8">
								<input type="text" id="first_Name" name="firstName"
									class="form-control" placeholder="First Name *"
									value="${enquiry.firstName}">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Last Name <span
								class="mandatory">*</span>
							</label>
							<div class="col-sm-8">
								<input type="text" id="last_Name" name="lastName" class="form-control"
									placeholder="Last Name *" value="${enquiry.lastName}">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">E-mail <span
								class="mandatory">*</span>
							</label>
							<div class="col-sm-8">
								<input type="email" id="email" name="emailId"
									class="form-control" placeholder="test@example.com*"
									value="${enquiry.emailId}">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label" for="mobile">Mobile<span class="text-danger">*</span></label>
							<div class="col-md-8">
								<input id="mobile" name="mobile" class="form-control"
									placeholder="Mobile no" type="text" value="${enquiry.mobile}">
							</div>
						</div>
					</div>
					<div class="block full">
						<div class="block-title">
							<h4 class="">Company Details</h4>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">NGO Name <span
								class="mandatory">*</span>
							</label>
							<div class="col-sm-8">
								<input id="ngo_Name" name="ngoName" type="text"
									class="form-control" placeholder="NGO Name *"
									value="${enquiry.ngoName}">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Field/Sector or
								Work <span class="mandatory">*</span>
							</label>
							<div class="col-sm-8">
								<input id="sector_work" name="sectorWork" type="text"
									class="form-control" placeholder="Field/Sector or Work*"
									value="${enquiry.sectorWork}">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Address Line1</label>
							<div class="col-sm-8">
								<input id="ngoAddressLine1" name="ngoAddressLine1" type="text"
									class="form-control" placeholder="Address Line1"
									value="${enquiry.ngoAddress1}">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Address Line2</label>
							<div class="col-sm-8">
								<input id="ngoAddressLine2" name="ngoAddressLine2" type="text"
									class="form-control" placeholder="Address Line2"
									value="${enquiry.ngoAddress2}">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Address Line3</label>
							<div class="col-sm-8">
								<input id="ngoAddressLine3" name="ngoAddressLine3" type="text"
									class="form-control" placeholder="Address Line3"
									value="${enquiry.ngoAddress3}">
							</div>
						</div>
						<div class="form-group" id="countryDiv">
							<label class="col-sm-3 control-label">Country Name<span
								class="mandatory">*</span>
							</label>

							<div class="col-sm-8" >
								<select class=" form-control" id = "countrydiv" name = "countryDiv">
									<option value=""></option>
									<c:forEach items="${countryList}" var="country">
										<option value="${country.countryId}"
											<c:if test="${country.countryId eq enquiry.country.countryId}">selected="selected"</c:if>>${country.countryName}</option>
									</c:forEach>
								</select>
							</div>
						</div>
						<div class="form-group" id="stateDiv">
							<label class="col-sm-3 control-label">State Name<span
								class="mandatory">*</span>
							</label>
							<div class="col-sm-8" >
								<select class=" form-control"
									id="statediv" name="stateDiv">
									<option value=""></option>
									<c:forEach items="${stateList}" var="state">
										<option value="${state.stateId}"
											<c:if test="${state.stateId eq enquiry.state.stateId}">selected="selected"</c:if>>${state.stateName}</option>
									</c:forEach>
								</select>
							</div>
						</div>
						<div class="form-group" id="cityDiv">
							<label class="col-sm-3 control-label">City Name<span
								class="mandatory">*</span>
							</label>
							<div class="col-sm-8" >
								<select class=" form-control"
									id="citydiv" name="cityDiv">
									<option value=""></option>
									<c:forEach items="${cityList}" var="city">
										<option value="${city.cityId}"
											<c:if test="${city.cityId eq enquiry.city.cityId}">selected="selected"</c:if>>${city.cityName}</option>
									</c:forEach>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Pincode<span
								class="mandatory">*</span>
							</label>
							<div class="col-sm-8">
								<input id="ngoPincode" name="ngoPincode" type="text"
									class="form-control" placeholder="Pincode"
									value="${enquiry.ngoPincode}">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Telephone</label>
							<div class="col-sm-8">
								<input id="ngoPhone" name="ngoPhone" type="text"
									class="form-control" placeholder="Telephone"
									value="${enquiry.ngoPhone}">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">NGO Email<span
								class="mandatory">*</span>
							</label>
							<div class="col-sm-8">
								<input id="ngo_email" name="ngoEmail" type="email"
									class="form-control" placeholder="NGO Email"
									value="${enquiry.ngoEmail}">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">NGO Website<span
								class="mandatory">*</span>
							</label>
							<div class="col-sm-8">
								<input id="ngo_website" name="ngoWebsite" type="text"
									class="form-control" placeholder="e.g. http://www.google.com"
									value="${enquiry.ngoWebsite}">
								<span class="help-block">The website url should start with http</span>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-5">
					<div class="block full">
						<div class="block-title">
							<h4 class="">About Organisation</h4>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">Organisation Vision<span
								class="mandatory">*</span>
							</label>
							<div class="col-sm-8">
								<textarea id="organisationVision" name="organisationVision"
									class="form-control" rows="5" placeholder="Organisation Vision">${enquiry.organisationVision}</textarea>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">Organisation
								Mission<span class="mandatory">*</span>
							</label>
							<div class="col-sm-8">
								<textarea id="organisationMission" name="organisationMission"
									class="form-control" rows="5"
									placeholder="Organisation Mission">${enquiry.organisationMission}</textarea>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">3 sentences
								describing what your organisation does.<span class="mandatory">*</span>
							</label>
							<div class="col-sm-8">
								<textarea id="organisationDescription"
									name="organisationDescription" class="form-control" rows="5"
									placeholder="Organisation Description">${enquiry.organisationDescription}</textarea>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-8 control-label">Is your
								organisation presently or has it in the past recieved
								organisational development assistance from any other agency?<span
								class="mandatory">*</span>
							</label>
							<div class="col-sm-4">
								<label class="radio"> <input id="assistance_otheragency"
									type="radio" name="assitanceFromOtherAgency" value="1" /> <span
									class="input-text">Yes</span>
								</label> <label class="radio"> <input
									id="assistance_otheragency" type="radio"
									name="assitanceFromOtherAgency" value="0" checked="checked" />
									<span class="input-text">No</span>
								</label>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-8 control-label">Please Indicate the
								Legal Type of Registration<span class="mandatory">*</span>
							</label>
							<div class="col-sm-4">
								<label class="radio"> <input id="legalType"
									name="legalType" type="radio" value="Societies Reg.Act 1860" />
									<span class="input-text">Societies Reg. Act 1860</span>
								</label> <label class="radio"> <input id="legalType"
									name="legalType" type="radio" value="Public Trust Act" /> <span
									class="input-text">Public Trust Act</span>
								</label> <label class="radio"> <input id="legalType"
									name="legalType" type="radio" value="Section 25 Company"
									/> <span class="input-text">Section
										25 Company</span>
								</label>
								<label class="radio"> <input id="legalType"
									name="legalType" type="radio" value="Private Limited Company"
									checked="checked" /> <span class="input-text">Private Limited Company</span>
								</label>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-8 control-label">FCRA Registration<span
								class="mandatory">*</span>
							</label>
							<div class="col-sm-4">
								<label class="radio"> <input id="fcra_registration"
									name="fcraRegistarion" type="radio" value="1" /> <span
									class="input-text">Yes</span>
								</label> <label class="radio"> <input id="fcra_registration"
									name="fcraRegistarion" type="radio" value="0" checked="checked" />
									<span class="input-text">No</span>
								</label>
							</div>
						</div>
						
						<hr>
					</div>
					<div class="block full">
						<div class="block-title">
							<h4 class=""> Organisation Details</h4>
						</div>
						<div class="form-group" id="educationTypeDiv">
							<label class="col-sm-4 control-label">Type of Organisation<span
								class="mandatory">*</span>
							</label>
								<div class="col-sm-8" id="subFunctionDiv4">
									<select class=" form-control subfunctionChosen"
										id="typeOfEducation" name="typeOfEducation">
										<option value=""></option>
										<c:forEach items="${organisationType}" var="EType">
											<option value="${EType.educationtype}"
												<c:if test="${EType.educationtype eq enquiry.typeOfEducation}">selected="selected"</c:if>>${EType.educationtype}</option>
										</c:forEach>
									</select>
								</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>

	<div class="container">
					
		<div class="row">
			<div class="text-center">
				<div>
			 
				<label style=" margin:1%"><input type="checkbox" style="margin-left: 0%"  onchange="document.getElementById('member_submit').disabled = !this.checked;" />
						I agree to the <a href="#" data-toggle="modal" data-target="#myModal" style="margin-left: 0%">Terms of Use</a> </label>
				<button id="member_submit" type="submit" class="btn btn-black" disabled="">Submit</button>
			
			</div>
			</div>
			<br>
		</div>
		<!-- <div class="row">
			<div class="text-center">
				<a href="#" data-toggle="modal" data-target="#myModal">TERMS OF USE</a>
				<button id="member_submit" type="submit" class="btn btn-black">Submit</button>
			</div>
			<br>
		</div> -->
	</div>
	
	
	
	
	<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog" style="width: 1000px">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h3 class="modal-title">Terms of Use</h3>
      </div>
      <div class="modal-body">
					<div class="container">


						<h4 style="color: #5bc0de">Preamble</h4>
						<p>The Atma Network (“Network”) is  a free online tool
							developed by Atma Education (“Atma”) to help NGOs strengthen
							their organisational processes and systems. It encompasses
							learnings from Atma's experience of assisting multiple NGOs to
							grow bigger, better and stronger. It works on a do-it-yourself
							model helping NGOs with different functional verticals like
							finance, fundraising, HR, Operations, etc. with detailed
							guidelines on each vertical. The Terms of Use, and/or, the public
							license granted herein, are to be read as a part and parcel of
							the Privacy Policy of the Atma Website, and Mobile Application.</p>
						<h3 style="color: #5bc0de">I. DEFINITIONS</h3>
						<h4 style="color: #5bc0de">(a) Adapted Material</h4>
						<p>Adapted Material means material subject to Copyright and
							Similar Rights that is derived from or based upon the Licensed
							Material and in which the Licensed Material is translated,
							altered, arranged, transformed, or otherwise modified in a manner
							requiring permission under the Copyright and Similar Rights held
							by the Licensor. For purposes of the public license granted by
							this agreement, where the Licensed Material is a musical work,
							performance, or sound recording, Adapted Material is always
							produced where the Licensed Material is synced in timed relation
							with a moving image.</p>
						<h4 style="color: #5bc0de">(b) Adapter's License</h4>
						<p>Adapter's License means the license you apply to your
							Copyright and Similar Rights in your contributions to Adapted
							Material in accordance with the terms and conditions of this
							public license.</p>
						<h4 style="color: #5bc0de">(c) Copyright and Similar Rights</h4>
						<p>Copyright and Similar Rights means Copyright and/or Similar
							Rights closely related to Copyright including, without
							limitation, performance, broadcast, sound recording, and sui
							generis database rights, without regard to how the rights are
							labelled or categorized.</p>
						<p>Provided that the term Copyright shall also include any
							claim to any Copyright that may subsist in the concerned work.</p>
						<p>Provided further that the term Similar Right shall also
							include any claim to any Similar Right that may subsist in the
							concerned work/material.</p>
						<h4 style="color: #5bc0de">(d) Effective Technological Measures</h4>
						<p>Effective Technological Measures means those measures that,
							in the absence of proper authority, may not be circumvented under
							laws fulfilling obligations under Article 11 of the WIPO
							Copyright Treaty adopted on December 20, 1996, and/or similar
							international agreements.</p>
						<h4 style="color: #5bc0de">(e) Exceptions and Limitations</h4>
						<p>Exceptions and Limitations means fair use, fair dealing,
							and/or any other exception or limitation to Copyright and Similar
							Rights that applies to your use of the Licensed Material.</p>
						<h4 style="color: #5bc0de">(f) Licensed Material</h4>
						<p>Licensed Material means the artistic or literary work,
							database, or other material to which the Licensor applied this
							public license.</p>
						<h4 style="color: #5bc0de">(g) Licensed Rights</h4>
						<p>Licensed Rights means the rights granted to you subject to
							the terms and conditions of this public license, which are
							limited to all Copyright and Similar Rights that apply to your
							use of the Licensed Material and that the Licensor has authority
							to license.</p>
						<h4 style="color: #5bc0de">(h) Licensor</h4>
						<p>Licensor means the individual(s) or entity(ies) granting
							rights under this public license.</p>
						<h4 style="color: #5bc0de">(i) Non-Commercial</h4>
						<p>Non-Commercial means not primarily intended for or directed
							towards Commercial Advantage or monetary compensation. For the
							purpose of this sub-clause, the term “Commercial Advantage”
							includes any unquantifiable or non-monetary advantage that an
							entity has obtained, obtains, or may obtain, over Atma, by
							engaging in activities that coincide or relate to any activity
							that has been, or is pursued by Atma. The term “Commercial
							Activities” further includes such activities, either announced
							publically or privately communicated to you, by Atma as to be
							pursued in future.</p>
						<h4 style="color: #5bc0de">(j) Share</h4>
						<p>Share means to provide material to the public by any means
							or process that requires permission under the Licensed Rights,
							such as reproduction, public display, public performance,
							distribution, dissemination, communication, or importation, and
							to make material available to the public including in ways that
							members of the public may access the material from a place and at
							a time individually chosen by them.</p>
						<h4 style="color: #5bc0de">(k) Sui Generis Database Rights</h4>
						<p>Sui Generis Database Rights means rights other than
							Copyright resulting from Directive 96/9/EC of the European
							Parliament and of the Council of 11 March 1996 on the legal
							protection of databases, as amended and/or succeeded, as well as
							other essentially equivalent rights anywhere in the world.</p>
							<br>
						<h3 style="color: #5bc0de">II. CONDITIONS OF USE</h3>
						<h4 style="color: #5bc0de">(a) General</h4>
						<p>
							By voluntary registering, or by visiting and using the App
							without registering, or accepting the Terms of Use, you represent
							and warrant to Atma that you have the right, authority and
							capacity to use the Information made available through the
							Network, and agree to and abide by the Terms of Use. You must
							provide Atma accurate information, including your real name, when
							you voluntarily create your account on the Network. By
							registering with <a href="http://www.rimtim.com/">the</a>
							Network, you certify that all information you provide, now or in
							the future, is accurate. The user is responsible for maintaining
							the confidentiality of the user’s account access information and
							password, if the user is registered on the Network.
						</p>
						<h4 style="color: #5bc0de">(b) Registration</h4>
						<p>In order to use the Network, you must first complete the
							registration form and create a user name and password. During
							registration you are required to give contact information (such
							as name, company name, phone number and a verifiable email
							address). Atma uses this information to identify you and/or
							contact you about the services on the Network in which you have
							expressed interest.</p>
						<p>You may optionally provide other related information if
							applicable (such as registration details, organisation address,
							organisation website, organisation logo or property photos) to
							Atma. Atma encourages you to submit this information accurately
							so it can be reflected properly on any saved data, printed data
							or pdf created by you through the Network.</p>
						<h4 style="color: #5bc0de">(c) Updates, Newsletters, and Surveys</h4>
						<p>From time-to-time Atma may provide you updates, newsletters
							and the opportunity to participate in surveys on the Network.
							Atma may use a third party service provider to conduct these
							surveys.</p>
						<h4 style="color: #5bc0de">(d) Customer Service</h4>
						<p>Atma will also communicate with you in response to your
							inquiries, to provide the services you request, and to manage
							your account.</p>
						<h4 style="color: #5bc0de">(e) Profile</h4>
						<p>Atma may store information that Atma collects through
							cookies, sessions, log files, clear gifs, web beacons, and/or
							third party sources to create a profile of your preferences.</p>
						<h4 style="color: #5bc0de">(f) Service-related Announcements</h4>
						<p>Atma will send you service-related announcements on rare
							occasions when it is necessary to do so. For instance, if the
							Network’s service is temporarily suspended for maintenance, Atma
							could send you an email.</p>
						<h4 style="color: #5bc0de">(g) Supplementation of Information</h4>
						<p>In order to provide certain valuation or marketing based
							services to you, Atma may on occasion supplement the personal
							information you submitted to the Network with information from
							third party sources.</p>
						<h4 style="color: #5bc0de">(h) Choice/Opt-out</h4>
						<p>If you no longer wish to receive our newsletter or update
							communication by email, you may opt-out of receiving them by
							following the instructions included in each newsletter or update
							communication. Further, if you do not wish to provide information
							sought in the survey, you have the choice to not participate in
							the survey. You furnish information on a voluntary basis while
							participating in a survey.</p>
						<h4 style="color: #5bc0de">(i) Log Files</h4>
						<p>Atma may gather certain information automatically and store
							it in log files. This information includes internet protocol (IP)
							addresses, browser type, internet service provider (ISP),
							referring/exit pages, operating system, date/time stamp, and
							clickstream data. Atma may use third-party tracking services that
							uses cookies or log files or other means to track non-personally
							identifiable information about visitors to the Network in the
							aggregate.</p>
						<h4 style="color: #5bc0de">(j) Cookies</h4>
						<p>A cookie is a small text file that is stored on a user’s
							computer for record-keeping purposes. Atma may use cookies on the
							Network. Atma uses both session ID cookies and/or persistent
							cookies. If you reject cookies your ability to use some areas of
							the Network will be limited.</p>
						<h4 style="color: #5bc0de">(k) Clear Gifs (Web Beacons/Web Bugs)</h4>
						<p>Atma may employ a software technology called clear gifs
							(also known as Web Beacons/Web Bugs), that helps Atma better
							manage content on the Network by informing Atma what content is
							effective. Atma may tie the information gathered by clear gifs to
							your personally identifiable information.</p>
						<p>Atma may use clear gifs in HTML-based emails to know which
							emails have been opened by recipients. If you would like to
							opt-out of these emails, please see “Choice/Opt-out.”</p>
							<br>
						<h3 style="color: #5bc0de">III. CO-BRANDED SITES</h3>
						<h4 style="color: #5bc0de">(a) Definitions</h4>
						<p>For the purposes of this clause, the following terms are
							assigned the definitions as set out hereunder.</p>
						<p>(i) “Co-brander” means any person or entity approved by
							Atma to be designated as such.</p>
						<p>(ii) “Co-Brander’s Marks” means the Co-Brander’s domain
							name, logos and trademarks provided by Co-Brander to Atma under a
							Co-Branding Agreement.</p>
						<p>(iii) “Atma’s Marks” means the domain name and Atma’s logos
							and trademarks provided by Atma to Co-Brander under this
							Agreement.</p>
						<p>(iv) “Advertisements” means advertisements and sponsorships
							to be placed on the Branded Pages.</p>
						<p>(v) “Branded Pages” means the pages accessible via the
							Domain Name, social media pages and email marketing campaigns
							that incorporate and integrate the Co-Brander Content and the
							Atma Content.</p>
						<p>(vi) “User” means any person who accesses or attempts to
							access the Branded Pages.</p>
						<p>(vii) “Atma(s) Content” means the materials provided by or
							created by or on behalf of Atma (such as the Marks, an HTML
							template for the “look and feel” of the Branded Pages, files,
							data and formulae) for incorporation into the Branded Pages.</p>
						<h4 style="color: #5bc0de">(b) Proprietary Rights, License, Restrictions and
							Termination</h4>
						<p>(i)License Grant by Atma. Upon a notification to this
							effect communicated to the Co-Brander, Atma may grant a
							non-exclusive, worldwide license to:</p>
						<p>1. use the Marks on the Branded Pages;</p>
						<p>2. reproduce Atma’s content and modify it only to the
							extent necessary to create Branded Pages and</p>
						<p>3. reproduce, distribute, publicly perform, publicly
							display and digitally perform the Branded Pages via the internet
							on and from server(s) located on the Co-Brander’s premises.</p>
						<p>Where no such notification is communicated under this
							sub-clause, the Co-Brander shall merely have a license to use
							Atma’s contents on an “as-is” basis, without making any
							modification whatsoever.</p>
						<p>Provided that in any case, the Co-Brander shall have no
							right to sublicense the foregoing rights (including without
							limitation to Web hosts or ISPs) or reuse Atma’s Content without
							Atma’s prior written consent. Any rights not expressly granted by
							Atma to the Co-Brander are reserved by Atma, and all implied
							licenses are disclaimed. The Co-Brander shall not exceed the
							scope of the licenses granted hereunder.</p>
						<p>(ii)Ownership of Marks, Atma’s Content and Branded Pages.
							All right, title and interest in and to the Marks, Atma’s Content
							and the Branded Pages (other than the Co-Brander Content)
							(including without limitation all rights therein under copyright,
							trademark, trade secret and similar laws) shall remain with
							Atma’s or its licensors and/or suppliers</p>
						<p>(iii)Ownership of Co-Brander Content. All right, title and
							interest in and to the Co-Brander Content (including without
							limitation all rights therein under copyright, trademark, trade
							secret and similar laws) shall remain with the Co-Brander or its
							licensors and/or suppliers.</p>
						<p>(iv)Quality Control and Use Restrictions. The Co-Brander
							shall use the Marks exactly in conformance with Atma’s trademark
							usage policies as communicated to the Co-Brander from time to
							time. Atma may immediately terminate the Co-Brander’s license to
							use the Marks if Atma reasonably believes that such use dilutes,
							tarnishes or blurs the value of the Marks. The Co-Brander
							acknowledges that the Co-Brander’s use of the Marks will not
							create in it, nor will it represent it has, any right, title or
							interest in or to the Marks other than the license granted by
							Atma above. The Co-Brander will not challenge the validity of or
							attempt to register any of the Marks or its interest therein as a
							licensee, nor will it adopt any derivative or confusingly similar
							names, brands or marks or create any combination marks with the
							Marks. The Co-Brander acknowledges Atma’s and its affiliates’
							ownership and exclusive right to use the Marks and agrees that
							all goodwill arising as a result of the use of the Marks shall
							inure to the benefit of Atma and its affiliates.</p>
						<p>(v)Restrictions. Atma may terminate the foregoing license
							if, in its sole discretion, the Co-Brander’s use of the marks
							does not conform to the Atma’s standards. Alternatively, Atma may
							specify that certain pages of the Co-Brander’s website may not
							contain Atma’s Marks. Title to and ownership of Atma’s marks
							shall remain with Atma. The Co-Brander shall not form any
							combination marks with Atma’s Marks. The Co-Brander shall not
							take any action inconsistent with Atma’s ownership of the marks
							and any benefits accruing from use of such trademarks shall
							automatically vest in Atma.</p>
						<p>(vi)Notification of Unauthorized Use. The Co-Brander agrees
							to notify Atma of any unauthorized use, unfair competition or
							other infringement by other persons relating to Atma’s Mark
							promptly after such infringement comes to the Co-Brander’s
							attention.</p>
						<p>(vii)Atma may request that the Co-Brander modify the design
							of the Branded Pages. The Co-Brander shall, at its sole expense,
							use commercially reasonable efforts to modify the design of the
							Branded Pages in accordance with Atma’s requests within three (3)
							business days of receipt of Atma’s written request. For
							modifications that would be commercially unreasonable to make
							within three (3) business days, the parties shall in good faith
							agree on a reasonable schedule for completion of such
							modifications. No modifications to the Branded Pages may be made
							by the Co-Brander without Atma’s prior written consent.</p>
							<br>
						<h3 style="color: #5bc0de">IV. LICENSE SCOPE</h3>
						<h4 style="color: #5bc0de">(a) General</h4>
						<p>
							Atma hereby grants you a non-exclusive, non-assignable and
							non-transferable license to use the Information provided by the
							Network to the extent, and only to the extent, necessary to
							access and use the Information in accordance with the terms of
							this Agreement. You shall not use the Information for any illegal
							purpose or in any manner inconsistent with the Terms of Use. <s>
							</s>
						</p>
						<p>
							If you seek a license that grants rights beyond what is granted
							by this clause, you may contact Atma at <a
								href="mailto:network@atma.org.in">network@atma.org.in</a>.
						</p>
						<h4 style="color: #5bc0de">(b) License Grant</h4>
						<p>Subject to the terms and conditions of the public license,
							Atma hereby grants you a worldwide, royalty-free,
							non-sub-licensable, non-exclusive license to exercise the
							Licensed Rights in the Licensed Material to:</p>
						<p>i. reproduce and Share the Licensed Material, in whole or
							in part, for Non-Commercial purposes only; and</p>
						<p>ii. produce, reproduce, and Share Adapted Material for
							Non-Commercial purposes only.</p>
						<h4 style="color: #5bc0de">(c) Exceptions and Limitations</h4>
						<p>For the avoidance of doubt, where exceptions and
							limitations apply to your use, the Public License does not apply,
							and you do not need to comply with the Terms of Use.</p>
						<h4 style="color: #5bc0de">(d) Term</h4>
						<p>
							The term of the Public license is specified in Section <a
								href="https://creativecommons.org/licenses/by-nc/4.0/legalcode#s6a">V(a)</a>.
						</p>
						<h4 style="color: #5bc0de">(e) Media and Formats; Technical Modifications Allowed</h4>
						<p>Atma authorizes you to exercise the licensed rights in all
							media and formats whether now known or hereafter created, and to
							make technical modifications necessary to do so. Atma waives
							and/or agrees not to assert any right or authority to forbid you
							from making technical modifications necessary to exercise the
							licensed rights, including technical modifications necessary to
							circumvent effective technological measures. For purposes of the
							public license, simply making modifications authorized by this
							Clause never produces Adapted Material.</p>
						<h4 style="color: #5bc0de">(f) Downstream Recipients</h4>
						<p>
							i. <u>Offer from the Licensor – Licensed Material</u>: Every
							recipient of the Licensed Material automatically receives an
							offer from Atma to exercise the Licensed Rights under the Terms
							of Use of the public license.
						</p>
						<p>
							ii. <u>No downstream restrictions</u>: you may not offer or
							impose any additional or different terms or conditions on, or
							apply any Effective Technological Measures to, the Licensed
							Material if doing so restricts exercise of the Licensed Rights by
							any recipient of the Licensed Material.
						</p>
						<h4 style="color: #5bc0de">(g) No Endorsement</h4>
						<p>Nothing in this public license constitutes or may be
							construed as permission to assert or imply that you are, or that
							your use of the Licensed Material is, connected with, or
							sponsored, endorsed, or granted official status by, Atma or
							others designated to receive attribution as provided in Clause V.
						</p>
						<h4>(h) Other Rights</h4>
						<p>Moral rights, such as the right of integrity, are not
							licensed under the public license, nor are publicity, privacy,
							and/or other similar personality rights; however, to the extent
							possible, Atma waives and/or agrees not to assert any such rights
							held by Atma to the limited extent necessary to allow you to
							exercise the Licensed Rights, but not otherwise.</p>
						<p>Patent and trademark rights are not licensed under the
							public license.</p>
						<p>To the extent possible, Atma waives any right to collect
							royalties from you for the exercise of the Licensed Rights,
							whether directly or through a collecting society under any
							voluntary or waivable statutory or compulsory licensing scheme.
							In all other cases Atma expressly reserves any right to collect
							such royalties, including when the Licensed Material is used
							other than for Non-Commercial purposes.</p>
							<br>
						<h3 style="color: #5bc0de">V. LICENSE CONDITIONS AND ATTRIBUTION</h3>
						<p>Your exercise of the Licensed Rights is expressly made
							subject to the conditions here below.</p>
						<p>If you Share the Licensed Material (including in modified
							form), you must:</p>
						<p>i. Retain the following if it is supplied by Atma with the
							Licensed Material:</p>
						<p>1. identification of the creator(s) of the Licensed
							Material and any others designated to receive attribution, in any
							reasonable manner requested by Atma (including by pseudonym if
							designated);</p>
						<p>2. a Copyright notice;</p>
						<p>3. a notice that refers to the public license;</p>
						<p>4. a notice that refers to the disclaimer of warranties;</p>
						<p>5. a URL or hyperlink to the Licensed Material to the
							extent reasonably practicable;</p>
						<p>ii. Indicate if you modified the Licensed Material and
							retain an indication of any previous modifications; and</p>
						<p>iii. Indicate the Licensed Material is licensed under the
							public license, and include the text of, or the URL or hyperlink
							to, this public license.</p>
						<p>You may satisfy the conditions in Clause V in any
							reasonable manner based on the medium, means, and context in
							which you share the Licensed Material. For example, it may be
							reasonable to satisfy the conditions by providing a URL or
							hyperlink to a resource that includes the required information.</p>
						<p>If requested by Atma, you must remove any of the
							information required by Clause V to the extent reasonably
							practicable.</p>
						<p>If you share Adapted Material you produce, the Adapter's
							License you apply must not prevent recipients of the Adapted
							Material from complying with the public license.</p>
							<br>
						<h3 style="color: #5bc0de">VI. SUI GENERIS DATABASE RIGHTS</h3>
						<p>Where the Licensed Rights include Sui Generis Database
							Rights that apply to your use of the Licensed Material:</p>
						<p>
							a. For the avoidance of doubt, Clause I<a
								href="https://creativecommons.org/licenses/by-nc/4.0/legalcode#s2a1">V(b)</a>
							grants you the right to extract, reuse, reproduce, and share all
							or a substantial portion of the contents of the database for
							Non-Commercial purposes only;
						</p>
						<p>b. If you include all or a substantial portion of the
							database contents in a database in which you have Sui Generis
							Database Rights, then the database in which you have Sui Generis
							Database Rights (but not its individual contents) is Adapted
							Material; and</p>
						<p>c. You must comply with the conditions in Clause V if you
							Share all or a substantial portion of the contents of the
							database.</p>
						<p>For the avoidance of doubt, Clause VI supplements and does
							not replace your obligations under the public license where the
							Licensed Rights include other Copyright and Similar Rights.</p>
							<br>
						<h3 style="color: #5bc0de">VII. DISCLAIMER OF WARRANTIES AND LIMITATION OF LIABILITY
						</h3>
						<p>Unless otherwise separately undertaken by the Licensor, to
							the extent possible, Atma offers the Licensed Material as-is and
							as-available, and makes no representations or warranties of any
							kind concerning the Licensed Material, whether express, implied,
							statutory, or other. This includes, without limitation,
							warranties of title, merchantability, fitness for a particular
							purpose, non-infringement, absence of latent or other defects,
							accuracy, or the presence or absence of errors, whether or not
							known or discoverable. Where disclaimers of warranties are not
							allowed in full or in part, this disclaimer may not apply to you.
						</p>
						<p>To the extent possible, in no event will Atma be liable to
							you on any legal theory (including, without limitation,
							negligence) or otherwise for any direct, special, indirect,
							incidental, consequential, punitive, exemplary, or other losses,
							costs, expenses, or damages arising out of the public license or
							use of the Licensed Material, even if Atma has been advised of
							the possibility of such losses, costs, expenses, or damages.
							Where a limitation of liability is not allowed in full or in
							part, this limitation may not apply to you.</p>
						<p>The disclaimer of warranties and limitation of liability
							provided above shall be interpreted in a manner that, to the
							extent possible, most closely approximates an absolute disclaimer
							and waiver of all liability.</p>
							<br>
						<h3 style="color: #5bc0de">VIII. LICENSE TERM AND TERMINATION</h3>
						<p>The public license applies for the term of the Copyright
							and Similar Rights licensed here. However, if you fail to comply
							with the public license, then your rights under the public
							license terminate automatically.</p>
						<p>Where your right to use the Licensed Material has
							terminated under Clause VIII, it reinstates:</p>
						<p>1. Automatically as of the date the violation is cured,
							provided it is cured within 30 days of your discovery of the
							violation; or</p>
						<p>2. Upon express reinstatement by the Licensor.</p>
						<p>For the avoidance of doubt, Clause VIII does not affect any
							right Atma may have to seek remedies for your violations of the
							public license.</p>
						<p>For the avoidance of doubt, the Licensor may also offer the
							Licensed Material under separate terms or conditions or stop
							distributing the Licensed Material at any time; however, doing so
							will not terminate this Public license.</p>
						<p>Clauses I, VII, VIII and IX survive termination of the
							public license.</p>
							<br>
						<h3 style="color: #5bc0de">IX. USE OF INFORMATION</h3>
						<p>Use of any information you provide to Atma during the
							account creation process is governed by the Atma Privacy Policy.
							You are responsible for safeguarding the password that you use to
							access the Information and for any activities or actions under
							your password after you register with the Network. Atma
							encourages you to use "strong" passwords (that use a combination
							of upper and lower case letters, numbers and symbols) to secure
							your account. Atma will not be liable for any loss or damage
							arising from your failure to comply with this suggestion.</p>
							<br>
						<h3 style="color: #5bc0de">X. INFORMATION OFFERED BY THE NETWORK</h3>
						<p>Content or Information provided by the Network is for
							informational purposes only. You acknowledge and agree that the
							form and nature of the Information which the Network provides may
							require certain changes in it. Therefore, Atma reserves the right
							to make changes and corrections to any part of the content or
							Information, at any time without prior notice, and without any
							liability.</p>
							<br>
						<h3 style="color: #5bc0de">XI. USER CONTENT/ INFORMATION</h3>
						<p>The information is supplied for users of the Network
							through optional registration. Such Information is generally in
							the form of aggregated statistics on traffic within the Network,
							although from time to time Atma may share personal information on
							users of the App with those marketing products and services that
							Atma believe may be of interest to users, provided that the Atma
							Privacy Policy is applicable to such disclosure to third parties.
							If a user provides any content or information that is untrue,
							inaccurate, not current or incomplete (or becomes untrue,
							inaccurate, not current or incomplete), or Atma has reasonable
							grounds to suspect that such content /information is untrue,
							inaccurate, not current or incomplete, Atma has the right to
							discontinue to provide Information to the User at its sole
							discretion.</p>
							<br>
						<h3 style="color: #5bc0de">XII. PROPRIETARY RIGHTS</h3>
						<p>All rights, titles, and interests in and to the information
							are and will remain the exclusive property of Atma. Atma is
							protected by Copyright, trademark, and other laws of the India.
							Except as expressly provided herein, nothing in the Terms of Use
							gives you a right to use the Atma name or any of the Atma
							trademarks, logos, domain names, and other distinctive brand
							features. Any feedback, comments, or suggestions you may provide
							regarding the Network is entirely voluntary and Atma will be free
							to use such feedback, comments or suggestions as Atma sees fit
							and without any obligation to you.</p>
							<br>
						<h3 style="color: #5bc0de">XIII. CHANGES IN TERMS OF USE</h3>
						<p>If Atma decides to change the Terms of Use, Atma will post
							those changes to the Terms of Use on the homepage, and/or other
							places Atma deems appropriate. Atma reserves the right to modify
							the Terms of Use at any time, so please review it frequently. If
							Atma makes material changes to the Terms of Use, Atma will notify
							you by email, or by means of a notice on the Network’s home page.
						</p>
							<br>
						<h3 style="color: #5bc0de">XIV. TERMINATION</h3>
						<p>Atma reserves the right to suspend/cancel, or discontinue
							any or all information at any time without notice.</p>
							<br>
						<h3 style="color: #5bc0de">XV. LISTING AND DISTRIBUTING CONTENT/ INFORMATION</h3>
						<p>Atma collects aggregated statistics on traffic within the
							App, and displays on the App, relevant information regarding the
							verticals of business listed on its App, such as the business
							location, reviews, contact number, and similar details. Atma
							takes reasonable efforts to ensure that such information is
							updated at frequent intervals. Although Atma screens and vets the
							information, it cannot be held liable for any inaccuracies or
							incompleteness represented from it, despite such reasonable
							efforts.</p>
						<p>During the listing of business, the App may be linked to
							the website of third parties and their affiliates. Atma has no
							control over, and is not liable or responsible for content,
							accuracy, validity, reliability, quality of such websites or made
							available by/through the App. Inclusion of any link on the App
							does not imply that Atma endorses the linked site. The user uses
							such links and services at user’s own risk.</p>
							<br>
						<h3 style="color: #5bc0de">XVI. INDEMNIFICATION</h3>
						<p>You agree to defend, indemnify, and hold Atma, its
							officers, directors, employees and agents, harmless from and
							against any claims, liabilities, damages, losses, and expenses,
							including, without limitation, reasonable legal and accounting
							fees, arising out of or in any way connected with your User
							Content, access to or use of the Network, Services or Atma
							Content, or violation of the Terms of Use.</p>
							<br>
						<h3 style="color: #5bc0de">XVII. WARRANTIES</h3>
						<p>You expressly understand and agree that the information
							which is provided is on an “as is” basis. Atma, and its
							affiliates make no warranties or conditions of any kind (express,
							implied or statutory), including without limitation the implied
							warranties of title, non-infringement and fitness for a
							particular purpose. Atma does not give warranty that any aspect
							of the Network will work properly or will be available to users
							continuously.</p>
							<br>
						<h3 style="color: #5bc0de">XVIII. LIMITATION OF LIABILITY</h3>
						<p>Atma is not liable to you or any other person for damages
							of any kind, including without limitation any punitive,
							exemplary, consequential, incidental, indirect or special damages
							(including without limitation, any personal injury, lost profits,
							business interruption, loss of programs or other data on your
							mobile or otherwise) arising from or in connection with use of
							the Network, the Information, the materials, your content, or any
							third party user generated information available on or through
							the Network, even if Atma has been advised of the possibility of
							such damages. These limitations shall apply notwithstanding the
							failure of essential purpose of any limited remedy.</p>
							<br>
						<h3 style="color: #5bc0de">XIX. GOVERNING LAW AND JURISDICTION</h3>
						<p>The Terms of Use shall be governed by the laws of India and
							you hereby consent to the exclusive jurisdiction of courts in
							Mumbai for all disputes arising out of or relating to the use of
							this agreement.</p>
							<br>
						<h3 style="color: #5bc0de">XX. NOTICE OF COPYRIGHT INFRINGEMENT</h3>
						<p>
							Atma respects the intellectual property rights of others and
							require those who use the Information to do the same. In the
							event that any users of the Information infringe on others’
							Copyrights, Atma reserves the right, in its sole discretion, to
							inter alia terminate those individuals’ rights to use the
							Information and terminate the user account. If you believe that
							your Copyright has been or is being infringed upon by material
							found in the Information, you are required to notify Atma by
							sending a written communication or contact Atma at <a
								href="mailto:network@atma.org.in">network@atma.org.in</a>.
						</p>
							<br>
						<h3 style="color: #5bc0de">XXI. SEVERABILITY</h3>
						<p>If any provision of the Terms of Use shall be held to be
							invalid or unenforceable for any reason, the remaining provisions
							shall continue to be valid and enforceable. If a Court finds that
							any provision of the Terms of Use is invalid or unenforceable,
							and that by limiting such provision it would become valid and
							enforceable, then such provision shall be deemed to be written,
							construed, and enforced as so limited.</p>
							<br>
						<h3 style="color: #5bc0de">XXII. ENTIRE AGREEMENT</h3>
						<p>The Terms of Use along with the Privacy Policy represent
							the entire agreement between the parties and supersedes any and
							all prior agreements or understandings whether oral or in
							writing. No changes, variation, renewal, extension or waiver
							shall be binding unless the same shall be in writing and signed
							by both parties.</p>
							<br>
						<h3 style="color: #5bc0de">XXIII. CONTACT US</h3>
						<p>
							If you have any questions or suggestions regarding the Atma’s
							Terms of Use, please contact Atma at: <a
								href="mailto:network@atma.org.in">network@atma.org.in</a>.
						</p>
						
					<!-- 	<label><input type="checkbox"  onchange="document.getElementById('member_submit').disabled = !this.checked;" />
						You must agree with the terms and conditions</label> -->
					</div>
				</div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
      </div>
    </div>

  </div>
</div>
	
	

	

	<br />
    <br />
    <br />
    <br />
    <br />
        
        <div class="container" id="footer">
        	<%@include file="../../visitor/inc/footer_items.jsp"%>
        </div>
    
	<!-- end heder -->
	
	
</body>
<!-- <script src="http://code.jquery.com/jquery.js"></script> -->
<script src="<%=contexturl%>resources/js/vendor/jquery-1.11.0.min.js"></script>
<script src="<%=contexturl%>resources/js/vendor/bootstrap.min.js"></script>
<script src="<%=contexturl %>resources/js/vendor/validate.js"></script>
<script src="<%=contexturl %>resources/js/additional-methods.js"></script>



<script type="text/javascript">
	$(document).ready(function() {
						
						$('select[name="countryDiv"]').change( function() {
							var countryId = ($(this).val());
							if(countryId == ""){
								$("#statediv").html("");
								$("#citydiv").html("");
							}
							else{
								$.ajax({
									type: "GET",
									async: false,
									url: "<%=contexturl%>state/statebycountryid/"+countryId,
									success: function(data, textStatus, jqXHR){
										var obj = jQuery.parseJSON(data);
										var len=obj.length;
										var myOptions = "<option value></option>";
										var myCityOptions = "<option value></option>";
										for(var i=0; i<len; i++){
										myOptions += '<option value="'+obj[i].stateId+'">'+obj[i].stateName+'</option>';
										}
										$("#statediv").html(myOptions);
										$("#citydiv").html(myCityOptions);
									},
									dataType: 'html'
								});
							}
							
							
						});
						
						
						$('select[name="stateDiv"]').change( function() {
							var stateId = ($(this).val());
							$.ajax({
								type: "GET",
								async: false,
								url: "<%=contexturl %>city/citybystateid/"+stateId,
								success: function(data, textStatus, jqXHR){
									var obj = jQuery.parseJSON(data);
									var len=obj.length;
									var myOptions = "<option value></option>";
									for(var i=0; i<len; i++){
									myOptions += '<option value="'+obj[i].cityId+'">'+obj[i].cityName+'</option>';
									}
									$("#citydiv").html(myOptions);
								},
								dataType: 'html'
							});
							
							
						});	
						
						$("#member_form").validate(
										{
											errorClass : "help-block animation-slideDown",
											errorElement : "div",
											errorPlacement : function(e, a) {
												a.parents(".form-group > div")
														.append(e)
											},
											highlight : function(e) {
												$(e)
														.closest(".form-group")
														.removeClass(
																"has-success has-error")
														.addClass("has-error")
											},
											success : function(e) {
												e
														.closest(".form-group")
														.removeClass(
																"has-success has-error")
											},
											rules : {
												firstName : {
													required : !0,maxlength : 75,
													NameValidate : true,
												},
												lastName : {
													required : !0,maxlength : 75,
													NameValidate : true,
												},
												emailId : {
													required : !0,maxlength : 75,
													email : !0,
												},
												mobile : {
													required : !0,
													mobile : !0,
												},
												ngoName : {
													required : !0,
													maxlength : 75
												},
												sectorWork : {
													required : !0,
													maxlength : 75
												},
												ngoAddressLine1 : {
													maxlength : 75
												},
												ngoAddressLine2 : {
													maxlength : 75
												},
												ngoAddressLine3 : {
													maxlength : 75
												},

												cityDiv : {
													required : !0
												},
												stateDiv : {
													required : !0
												},

												countryDiv : {
													required : !0
												},
												typeOfEducation : {
													required : !0
												},

												ngoPincode : {
													required : !0,
													maxlength : 10,
													digits : !0,
													pincode : !0
												},
												ngoPhone : {
													phone : !0
												},
												ngoEmail : {
													required : !0,
													maxlength : 75,
													email : !0
												},
												ngoWebsite : {
													required : !0,
													url : true,
													maxlength : 500
												},
												
												organisationVision : {
													maxlength : 500,
													required : !0
												},
												organisationMission : {
													maxlength : 500,
													required : !0
												},
												organisationDescription : {
													maxlength : 255,
													required : !0
												},
												legalType : {
													required : !0
												},
											},
											messages : {
												firstName : {
													required :'<spring:message code="validation.pleaseenterfirstname"/>',
													maxlength :'<spring:message code="validation.firstname75character"/>',
													NameValidate : 'Please use only alphabetic characters',
												},
												lastName : {
													required :'<spring:message code="validation.pleaseenterlastname"/>',
													maxlength :'<spring:message code="validation.lastname75character"/>',
													NameValidate : 'Please use only alphabetic characters',
												},
												emailId : {
													required :'<spring:message code="validation.pleaseenteremailid"/>',
													email :'<spring:message code="validation.pleaseenteravalidemailid"/>',
													maxlength :'<spring:message code="validation.email75character"/>',
												},
												mobile : {
													required :'<spring:message code="validation.pleaseenteramobilenumber"/>',
													mobile :'<spring:message code="validation.mobile"/>',
													maxlength :'<spring:message code="validation.mobile10character"/>',
												},
												ngoName : {
													required : 'Please enter a NGO Name',
													maxlength : 'NGO name should not be more than 75 characters'
												},
												sectorWork : {
													required : 'Please enter a Field/Sector or Work',
													maxlength : 'Field/Sector of Work should not be more than 75 characters'
												},

												ngoAddressLine1 : {

													maxlength : '<spring:message code="validation.addressline1"/>'
												},
												ngoAddressLine2 : {

													maxlength : '<spring:message code="validation.addressline1"/>'
												},
												ngoAddressLine3 : {

													maxlength : '<spring:message code="validation.addressline1"/>'
												},
												cityDiv : {
													required : '<spring:message code="validation.selectcity"/>'
												},
												stateDiv : {
													required : '<spring:message code="validation.selectstate"/>'
												},
												countryDiv : {
													required : '<spring:message code="validation.selectcountry"/>'
												},
												typeOfEducation : {
													required : '<spring:message code="validation.typeOfEducation"/>'
												},
												
												ngoPincode : {
													required : '<spring:message code="validation.pleaseenterpincode"/>',
													maxlength : 'Please enter a valid pincode',
													digits : '<spring:message code="validation.digits"/>'
												},
												
												ngoPhone : {

													phone : '<spring:message code="validation.phone"/>'
												},
												ngoEmail : {
													required : '<spring:message code="validation.pleaseenteremailid"/>',
													email : '<spring:message code="validation.pleaseenteravalidemailid"/>',
													maxlength : '<spring:message code="validation.email75character"/>'
												},
												ngoWebsite : {
													required : 'Please enter a company website',
													url : '<spring:message code="validation.pleaseenteravalidurl"/>',
													maxlength : 'Link should not be more than 500 characters'
												},
												
												organisationVision : {
													maxlength : 'Vision should not be more than 500 characters',
													required : 'Please enter Organisation Vision'
												},
												organisationMission : {
													maxlength : 'Mission should not be more than 500 characters',
													required : 'Please enter Organisation Mission'
												},
												organisationDescription : {
													maxlength : 'Description should not be more than 255 characters',
													required : 'Please enter Organisation Description'
												},
												legalType : {
													required : 'Please select a Legal Type',
												},
											},

										});
						
						
						jQuery.validator.addMethod("NameValidate",
								function(value, element) {
									var regex = new RegExp("^[a-zA-Z ]*$");
									var key = value;

									if (!regex.test(key)) {
										return false;
									}
									return true;
								}, "please use only alphabetic characters");

						jQuery.validator.addMethod("phone", function (phone_number, element) {
					         //phone_number = phone_number.replace(/\s+/g, "");
					        // return this.optional(element) || phone_number.length >= 8 && phone_number.length < 25 && phone_number.match(/^[0-9\+\-]*$/);
					         return this.optional(element) || phone_number.length >= 6 && phone_number.length < 12 && phone_number.match(/^[0-9][0-9]{5,10}$/);

					     }, "Please specify a valid phone number");
					    
						    
						 jQuery.validator.addMethod("mobile", function (mobile_number, element) {
						      mobile_number = mobile_number.replace(/\s+/g, "");
						       if (mobile_number.match(/^[7-9][0-9]{9}$/)) return true;
						         //return this.optional(element) || mobile_number.length >= 10 && mobile_number.length < 25 && mobile_number.match(/^[0-9\+\-]*$/);
						     }, "Please specify a valid mobile number");
						    
						    jQuery.validator.addMethod("pincode", function (pincode, element) {
						    	pincode = pincode.replace(/\s+/g, "");
						        return this.optional(element) || pincode.length >=4 && pincode.length <= 6;
						    }, "Please specify a valid pincode");
						
						
						$("#member_submit").click(function() {
							unsaved = false;
							$("#member_form").submit();
						});

					});
</script>

</html>
