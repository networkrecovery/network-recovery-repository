package com.astrika.companymngt.service;



import java.io.IOException;
import java.util.Date;
import java.util.List;

import com.astrika.common.exception.NoSuchUserException;
import com.astrika.common.model.User;
import com.astrika.companymngt.model.company.CompanyMaster;
import com.astrika.companymngt.model.company.CompanySurveyResetManager;




public interface CompanyService {

	public CompanyMaster save(CompanyMaster company);

	public List<CompanyMaster> findByActive(boolean active);

	public List<CompanySurveyResetManager> findByActiveWithSurvey(boolean active);

	public CompanyMaster findByCompanyId(long companyId);

	public CompanyMaster update(CompanyMaster companyMaster)
			throws IOException;

	public CompanyMaster save(CompanyMaster company, User admin)
			throws IOException;

	public CompanyMaster delete(Long companyId) throws NoSuchUserException;

	public CompanyMaster restore(Long companyId) throws NoSuchUserException;

	public CompanyMaster findByUserId(long userId);

	public List<CompanyMaster> findByActiveTrueOrderByCompanyNameAsc( int start, int end);

	public int noOfOrgPerArea(String educationArea);

	public CompanyMaster findByCompanyEmail(String emailId);

	public CompanyMaster findByCompanyName(String companyName);

	public void createCompanyInfoXlSheet(CompanyMaster company) throws IOException;

	public void createCompanyInfoZip(CompanyMaster company) throws IOException;

	public List<Object[]> findOrgByCity();

	public List<Object[]> findByFiveOrgByCity(int start,int end);

	public List<CompanyMaster> activeInLastMonth();

	public List<Object[]> getTopCities();

	public List<Object[]> getAllRecod();

	public List<CompanyMaster> activeInMoteThanMonth();

	public int gettotalOrganisations();

	public int getLastMonthActiveOrganisation();

	public int getSecondLastMonthActiveOrganisation();

	public int getMoreThanLastMonthOrganisation();

	public int getTotalOraganisation();

	public int getLastMonthCreatedOrganisation();

	public List<Object[]> getLastMonthInactiveOrganisation(int i);

	public List<Object[]> getLastTwoMonthInactiveOrganisation();

	public List<Object[]> getInactiveOrg(long t, int yr);

	public int getInactiveOrgListExcel(long totalOrg, int yr, int mmth);

	public long getTotalOraganisation1();

	public int getTotalOrg(int yr);

	public int getlastAddedOrganisations();

	public List<Object[]> getLastAddedOrganisation();

	public List<CompanyMaster> getInactiveFlagLastOrganisation();

	public CompanyMaster update1(CompanyMaster companyMaster);

	public List<Object[]> getLastMonthInactiveOrganisationByModulUser(int i);

	List<Object[]> getInactiveOrg1(int yr2);

	public String createXlSheetForInactiveOrg() throws IOException;

	List<Object[]> findCompanyDetail();

	public String createExcelSheetForInactiveOrg() throws IOException;

	public int getInctiveOrganisation(int i);

	public List<Object[]> topCities();

	public long getTotalOrgUptomonth(Date startDate);

	public long getInactiveOrgOfThatMonth(long totalOrgUptoMonth, int yr, int x);

	

	

	

		
}
