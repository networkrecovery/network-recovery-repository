package com.astrika.surveymngt.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.astrika.companymngt.model.module.ModuleMaster;
import com.astrika.surveymngt.model.LifeStages;
import com.astrika.surveymngt.model.SurveyBuilder;
import com.astrika.surveymngt.model.VisitorSurveyBuilder;
import com.astrika.surveymngt.repository.SurveyBuilderRepository;
import com.astrika.surveymngt.service.SurveyBuilderService;
import com.astrika.surveymngt.service.VisitorSurveyBuilderService;
import com.astrika.universitymngt.model.Status;


@Service
public class SurveyBuilderServiceImpl implements SurveyBuilderService {

	@Autowired
	private SurveyBuilderRepository surveyBuilderRepository;

	@Autowired
	private VisitorSurveyBuilderService visitorSurveyBuilderService;
	
	@Override
	public List<SurveyBuilder> findBySurveyBuilderId(long surveyBuilderId) {
		
		return surveyBuilderRepository.findBySurveyBuilderId(surveyBuilderId);
	}

	@Override
	public SurveyBuilder save(SurveyBuilder surveyBuilder) {
		
		return surveyBuilderRepository.save(surveyBuilder);
	}

	@Override
	public List<SurveyBuilder> findByActive(boolean active) {
		
		return surveyBuilderRepository.findByActive(true);
	}

	@Override
	public List<SurveyBuilder> findByModuleId(long moduleId) {
		/* Auto-generated method stub*/
		return surveyBuilderRepository.findByModuleId(moduleId);
	}

	@Override
	public List<SurveyBuilder> findByModuleIdOrderByQuestionNo(long moduleId) {
		return surveyBuilderRepository.findByModuleIdOrderByQuestionNo(moduleId);
	}

	@Override
	public List<SurveyBuilder> findByLifeStagesAndModuleId(LifeStages lifeStages,long moduleId) {
	
		return surveyBuilderRepository.findByLifeStagesAndModuleId(lifeStages, moduleId);
	}


	@Override
	public List<SurveyBuilder> findByStatus(Status status,long moduleId) {
		return surveyBuilderRepository.findByStatus(status,moduleId);
	}

	@Override
	public void deleteAllByModuleId(long moduleId) {
		 surveyBuilderRepository.deleteAllByModuleId(moduleId);
	}

	@Override
	public List<SurveyBuilder> findByLifeStagesandModuleIdandStatus(
			LifeStages lifeStages, long moduleId, Status status) {
		return surveyBuilderRepository.findByLifeStagesandModuleIdandStatus(lifeStages, moduleId, status);
	}

	@Override
	public List<SurveyBuilder> findByModuleIdandStatus(long moduleId,
			Status status) {
		return surveyBuilderRepository.findByModuleIdandStatus(moduleId, status);
	}

	@Override
	public void processSurveyBuilderUpdateAlongWithVistorSurveys(List<SurveyBuilder> surveyBuilders, ModuleMaster moduleMaster) {
		List<SurveyBuilder> survey1 = findByModuleId(moduleMaster.getModuleId());
		if (survey1 != null) {
			deleteAllByModuleId(moduleMaster.getModuleId());
		}
		surveyBuilderRepository.save(surveyBuilders);

		for (SurveyBuilder surveyBuilder: surveyBuilders) {
			List<VisitorSurveyBuilder> visitorSurveyBuilders = visitorSurveyBuilderService.findByModuleIdandQuestionNo(moduleMaster.getModuleId(),surveyBuilder.getQuestionNo());
			for (VisitorSurveyBuilder visitorSurveyBuilder : visitorSurveyBuilders) {
				visitorSurveyBuilder.setQuestion(surveyBuilder.getQuestion());
				visitorSurveyBuilder.setQuestionExplanation(surveyBuilder.getQuestionExplanation());
			}
			visitorSurveyBuilderService.save(visitorSurveyBuilders);
		}


	}


}
