package com.astrika.common.model;


public enum Role {

	SUPER_ADMIN1(1),
	SUPER_ADMIN(2),
	ATMA_ADMIN(3),
	COMPANY_ADMIN(4),
	RESTAURANT_ADMIN(5),
	CORPORATE_ADMIN(6),
	STUDENT(7),
	INSTRUCTOR(8),
	MODULE_USER(9);

	private final int id;

	private Role(int id){
		this.id = id;
	}

	public int getId() {
		return id;
	}


	public static Role fromInt(int id) {

		switch (id) {
		case 1:
			return SUPER_ADMIN1;
		case 2:
			return SUPER_ADMIN;
		case 3:
			return ATMA_ADMIN;
		case 4:
			return COMPANY_ADMIN;
		default:
			return getRole(id);
		}

	}

	private static Role getRole(int id) {
		switch (id) {
		case 5:
			return RESTAURANT_ADMIN;
		case 6:
			return CORPORATE_ADMIN;
		case 7:
			return STUDENT;
		default:
			return getRoleOne(id);
		}
	}

	private static Role getRoleOne(int id) {

		switch (id) {
		case 8:
			return INSTRUCTOR;
		case 9:
			return MODULE_USER;
		default:
			return null;
		}
	}


}
