package com.astrika.common.model;

import java.util.Collection;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.envers.Audited;
import org.joda.time.DateTime;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.astrika.common.model.location.CityMaster;
import com.astrika.common.model.location.CountryMaster;
import com.astrika.common.model.location.StateMaster;

/**
 * @author Priyanka
 */
@Entity
@Audited
public class User implements UserDetails {
	@ManyToOne
	private LanguageMaster language;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long userId;

	@Column(nullable = false, length = 75)
	private String firstName;

	@Column(length = 75)
	private String lastName;

	@Column(nullable = false)
	private boolean enquiryAccessRights = false;

	@Column(nullable = false, length = 75)
	private String loginId = null;

	@Column(nullable = false, length = 500)
	private String password = null;

	@Column(length = 75)
	private String designation;

	@Column(length = 500)
	private String description;

	@OneToOne
	private ImageMaster profileImage;

	@ManyToOne(fetch = FetchType.LAZY)
	private CityMaster city;

	@ManyToOne(fetch = FetchType.LAZY)
	private CountryMaster country;
	
	@Column
	private int profileCompletion = 0;
	
	
	@Column(columnDefinition = "boolean default false", nullable = false)
	private boolean flag;
	
	@Column
	private int totalProfileCompletion = 0;
	
	@Column
	private int profileCompletionRate = 0;

	@Transient
	private Collection<? extends GrantedAuthority> grantedAuthorities;

	/*
	 * User Role 0 - Astrika/Super Admin 1 - Gourmet 7 Admin 2 - Company Admin 3
	 * - Brand Admin 4 - Outlet Admin 5 - Corporate 6 - Member
	 */
	@Enumerated(EnumType.ORDINAL)
	private Role role;

	@Enumerated(EnumType.ORDINAL)
	private InviteStatus inviteStatus;

	@Column(unique = true, nullable = false, length = 75)
	private String emailId = null;

	@Column(length = 75)
	private String mobile;

	private String macId;

	/*
	 * User Role 0 - Inactive 1 - Active
	 */
	@Enumerated(EnumType.ORDINAL)
	private UserStatus status;

	// Audit fields

	@Column
	private Date lastLoginDate;

	@Column
	private Date passwordModifiedDate;

	@Column(columnDefinition = "boolean default false", nullable = false)
	private boolean emailAddressVerified;

	@Transient
	private String fullName;

	@ManyToOne
	private StateMaster state;

	@Column(length = 525)
	private String address;

	@Column
	private String moduleName;

	@CreatedDate
    private DateTime createdOn;
	
	@Transient
	private String modName;

	@LastModifiedDate
	private DateTime lastModifiedOn;

	@JsonIgnore
	@LastModifiedBy
	@ManyToOne(fetch = FetchType.LAZY)
	private User lastModifiedBy;

	@JsonIgnore
    @CreatedBy
    @ManyToOne(fetch = FetchType.LAZY)
    private User createdBy;
	
	@Column
	private String pincode;

	@Column
	private Date birthdayDate;

	@Column
	private String gender;

	// @Column
	@OneToOne
	private ImageMaster portrait;

	@Column
	private String passwordResetKey;

	@Column
	private String timeZoneId;

	private String token;

	@Column
	private String addressLine1;

	@Column
	private String addressLine2;

	@Column
	private Long moduleId;
	
	public User() {
		super();
	}

	public User(String firstName, String lastName, boolean enquiryAccessRights,
            String loginId, String password, String designation,
            String description, int role, String emailId, int inviteStatus,boolean flag
            ) {
        super();
        this.firstName = firstName;
        this.lastName = lastName;
        this.enquiryAccessRights = enquiryAccessRights;
        this.loginId = loginId;
        this.password = password;
        this.designation = designation;
        this.description = description;
        this.role = Role.fromInt(role);
        this.emailId = emailId;
        this.inviteStatus = InviteStatus.fromInt(inviteStatus);
        this.flag=flag;
        
    }

    public User(String firstName, String lastName, boolean enquiryAccessRights,
            int role, String emailId, String mobile, LanguageMaster language,
            String password) {

        this.firstName = firstName;
        this.lastName = lastName;
        this.enquiryAccessRights = enquiryAccessRights;
        this.password = password;
        this.role = Role.fromInt(role);
        this.emailId = emailId;
        this.mobile = mobile;
        this.language = language;

    }

    public User(String firstName, String lastName, CityMaster city,
            CountryMaster country, int role, String emailId, String mobile,
            StateMaster state, String address, String pincode, String gender,
            UserStatus status) {
        super();
        this.lastName = lastName;
        this.firstName = firstName;
        this.country = country;
        this.city = city;
        this.emailId = emailId;
        this.role = Role.fromInt(role);
        this.state = state;
        this.mobile = mobile;
        this.pincode = pincode;
        this.address = address;
        this.status = status;
        this.gender = gender;
        
    }
    
    public User(String firstName, String lastName, CityMaster city,
            CountryMaster country, int role, String emailId, String mobile,
            StateMaster state, String address) {
        super();
        this.address = address;
        this.city = city;
        this.country = country;
        this.role = Role.fromInt(role);
        this.emailId = emailId;
        this.mobile = mobile;
        this.state = state;
        this.firstName = firstName;
        this.lastName = lastName;
        
    }

    public User(String firstName, String lastName, String designation,
            String emailId, String loginId, String password, int role,
            int inviteStatus, String mobile,
            UserStatus status,boolean flag) {
        super();
        this.firstName = firstName;
        this.lastName = lastName;
        this.designation = designation;
        this.emailId = emailId;
        this.loginId = loginId;
        this.password = password;
        this.role = Role.fromInt(role);
        this.inviteStatus = InviteStatus.fromInt(inviteStatus);
        this.mobile = mobile;
        this.status = status;
        this.flag=flag;
    }
    	
	public boolean isFlag() {
		return flag;
	}

	public void setFlag(boolean flag) {
		this.flag = flag;
	}

	public Long getModuleId() {
		return moduleId;
	}

	public void setModuleId(Long moduleId) {
		this.moduleId = moduleId;
	}

	public InviteStatus getInviteStatus() {
		return inviteStatus;
	}

	public void setInviteStatus(InviteStatus inviteStatus) {
		this.inviteStatus = inviteStatus;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public LanguageMaster getLanguage() {
		return language;
	}

	public void setLanguage(LanguageMaster language) {
		this.language = language;
	}

	public String getAddressLine1() {
		return addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public String getAddressLine2() {
		return addressLine2;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}


	public Date getPasswordModifiedDate() {
		return passwordModifiedDate;
	}

	public void setPasswordModifiedDate(Date passwordModifiedDate) {
		this.passwordModifiedDate = passwordModifiedDate;
	}

	public boolean isEmailAddressVerified() {
		return emailAddressVerified;
	}

	public void setEmailAddressVerified(boolean emailAddressVerified) {
		this.emailAddressVerified = emailAddressVerified;
	}

	public String getTimeZoneId() {
		return timeZoneId;
	}

	public void setTimeZoneId(String timeZoneId) {
		this.timeZoneId = timeZoneId;
	}

	public String getFullName() {
		return firstName + " " + lastName;
	}

	public ImageMaster getPortrait() {
		return portrait;
	}

	public void setPortrait(ImageMaster portrait) {
		this.portrait = portrait;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return grantedAuthorities;
	}

	public void setGrantedAuthorities(
			Collection<? extends GrantedAuthority> grantedAuthorities) {
		this.grantedAuthorities = grantedAuthorities;
	}

	@Override
	public String getUsername() {
		return this.getLoginId();
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	public String getPasswordResetKey() {
		return passwordResetKey;
	}

	public void setPasswordResetKey(String passwordResetKey) {
		this.passwordResetKey = passwordResetKey;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Collection<? extends GrantedAuthority> getGrantedAuthorities() {
		return grantedAuthorities;
	}

	public UserStatus getStatus() {
		return status;
	}

	public void setStatus(UserStatus status) {
		this.status = status;
	}

	public CityMaster getCity() {
		return city;
	}

	public void setCity(CityMaster city) {
		this.city = city;
	}

	public CountryMaster getCountry() {
		return country;
	}

	public void setCountry(CountryMaster country) {
		this.country = country;
	}

	public ImageMaster getProfileImage() {
		return profileImage;
	}

	public void setProfileImage(ImageMaster profileImage) {
		this.profileImage = profileImage;
	}

	public String getMacId() {
		return macId;
	}

	public void setMacId(String macId) {
		this.macId = macId;
	}

	public StateMaster getState() {
		return state;
	}

	public void setState(StateMaster state) {
		this.state = state;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public DateTime getCreatedOn() {
		return createdOn;
	}

	public DateTime getLastModifiedOn() {
		return lastModifiedOn;
	}
	public void setCreatedOn(DateTime createdOn) {
        this.createdOn = createdOn;
    }

	public void setLastModifiedOn(DateTime lastModifiedOn) {
		this.lastModifiedOn = lastModifiedOn;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public User getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }
	
	public void setLastModifiedBy(User lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	public String getPincode() {
        return pincode;
    }
	
	public Date getBirthdayDate() {
		return birthdayDate;
	}

	public void setBirthdayDate(Date birthdayDate) {
		this.birthdayDate = birthdayDate;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getModName() {
		return modName;
	}

	public void setModName(String modName) {
		this.modName = modName;
	}

	public String getModuleName() {
		return moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	public boolean isEnquiryAccessRights() {
		return enquiryAccessRights;
	}

	public void setEnquiryAccessRights(boolean enquiryAccessRights) {
		this.enquiryAccessRights = enquiryAccessRights;
	}
	
	public Date getLastLoginDate() {
		return lastLoginDate;
	}

	public void setLastLoginDate(Date lastLoginDate) {
		this.lastLoginDate = lastLoginDate;
	}	

	public int getProfileCompletion() {
		return profileCompletion;
	}

	public void setProfileCompletion(int profileCompletion) {
		this.profileCompletion = profileCompletion;
	}

	public int getTotalProfileCompletion() {
		return totalProfileCompletion;
	}

	public void setTotalProfileCompletion(int totalProfileCompletion) {
		this.totalProfileCompletion = totalProfileCompletion;
	}
	

	public int getProfileCompletionRate() {
		return profileCompletionRate;
	}

	public void setProfileCompletionRate(int profileCompletionRate) {
		this.profileCompletionRate = profileCompletionRate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((loginId == null) ? 0 : loginId.hashCode());
		result = prime * result + ((userId == null) ? 0 : userId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (loginId == null) {
			if (other.loginId != null)
				return false;
		} else if (!loginId.equals(other.loginId))
			return false;
		if (userId == null) {
			if (other.userId != null)
				return false;
		} else if (!userId.equals(other.userId))
			return false;
		return true;
	}

}
