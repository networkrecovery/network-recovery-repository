package com.astrika.weightagemngt.model;

public enum SurveyWeightage {

	QUESTION1("question_1", 10),
	QUESTION2("question_2", 10),
	QUESTION3("question_3", 10),
	QUESTION4("question_4", 10),
	QUESTION5("question_5", 10),
	QUESTION6("question_6", 10),
	QUESTION7("question_7", 10),
	QUESTION8("question_8", 10),
	QUESTION9("question_9", 10),
	QUESTION10("question_10", 10),
	QUESTION11("question_11", 10),
	QUESTION12("question_12", 10),
	QUESTION13("question_13", 10),
	QUESTION14("question_14", 10),
	QUESTION15("question_15", 10);
	
	
	private String name;
	private final int id;
	
	private SurveyWeightage(String name, int id) {
		this.name = name;
		this.id = id;
	}
	
	public String getName() {
		return name;
	}

	public int getId() {
		return id;
	}



	public static SurveyWeightage fromInt(int id){
		switch(id){
			case 1:
				return QUESTION1;
			case 2: 
				return QUESTION2;
			case 3:
				return QUESTION3;
			case 4: 
				return QUESTION4;
			case 5:
				return QUESTION5;
			case 6:
				return QUESTION6;
			case 7:
				return QUESTION7;
			case 8:
				return QUESTION8;
			case 9:
				return QUESTION9;
			case 10:
				return QUESTION10;
			default:
				return null;
		}
	}
	
	
}
