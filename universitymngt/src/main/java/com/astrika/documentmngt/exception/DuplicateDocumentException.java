package com.astrika.documentmngt.exception;

import com.astrika.kernel.exception.BusinessException;
import com.astrika.kernel.exception.CustomException;

public class DuplicateDocumentException extends BusinessException {
	
	public DuplicateDocumentException() {
		super(CustomException.DUPLICATE_DOCUMENT.getCode());
	}

	public DuplicateDocumentException(String errorCode) {
		super(errorCode);
	}

}
