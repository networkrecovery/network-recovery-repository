package com.astrika.moduleusermanagement.exception;

import com.astrika.kernel.exception.BusinessException;
import com.astrika.kernel.exception.CustomException;

public class FileNotFoundException extends BusinessException{

	public FileNotFoundException() {
		super(CustomException.FILE_NOT_FOUND_EXCEPTION.getCode());
	}

	public FileNotFoundException(String errorCode) {
		super(errorCode);
	}
	

}
