package com.astrika.common.model.location;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.envers.Audited;
import org.joda.time.DateTime;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import com.astrika.common.model.User;

@Entity
@Audited
public class CityMaster {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long cityId;

	@Column(nullable = false, length = 75)
	private String cityName;

	@Column(columnDefinition = "boolean default true", nullable = false)
	private boolean active = true;

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	private CountryMaster country;

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	private StateMaster state;

	@JsonIgnore
	@CreatedBy
	@ManyToOne(fetch = FetchType.LAZY)
	private User createdBy;


	@LastModifiedDate
	private DateTime lastModifiedOn;

	@CreatedDate
    private DateTime createdOn;

	
	@JsonIgnore
	@LastModifiedBy
	@ManyToOne(fetch = FetchType.LAZY)
	private User lastModifiedBy;

	public CityMaster() {
		super();
	}

	public CityMaster(String cityName, CountryMaster country) {
		super();
		this.cityName = cityName;
		this.country = country;
	}

	public CityMaster(String cityName, StateMaster state, CountryMaster country) {
		super();
		this.cityName = cityName;
		this.state = state;
		this.country = country;
	}

	public Long getCityId() {
		return cityId;
	}

	public void setCityId(Long cityId) {
		this.cityId = cityId;
	}

	public String getCityName() {
		return cityName;
	}

	public boolean isActive() {
        return active;
    }
	
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public DateTime getLastModifiedOn() {
		return lastModifiedOn;
	}
	
	public DateTime getCreatedOn() {
        return createdOn;
    }

	public void setLastModifiedOn(DateTime lastModifiedOn) {
		this.lastModifiedOn = lastModifiedOn;
	}

	public void setCreatedOn(DateTime createdOn) {
        this.createdOn = createdOn;
    }

	@JsonIgnore
	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}

	@JsonIgnore
	public void setLastModifiedBy(User lastModifiedBy) {
	    this.lastModifiedBy = lastModifiedBy;
	}
	
	@JsonIgnore
	public User getLastModifiedBy() {
		return lastModifiedBy;
	}

	@JsonIgnore
    public User getCreatedBy() {
        return createdBy;
    }
	
	public CountryMaster getCountry() {
		return country;
	}

	public void setCountry(CountryMaster country) {
		this.country = country;
	}


	public StateMaster getState() {
		return state;
	}

	public void setState(StateMaster state) {
		this.state = state;
	}

	public static String getOffSet(String timZone) {
		if (timZone != null && timZone.length() > 0) {
			return timZone.substring(timZone.indexOf("(") + 1,
					timZone.indexOf(')'));
		} else
			return "UTC";

	}
}
