<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@include file="../../inc/config.jsp"%>
<div class="content-header">
	<div class="header-section">
		<h1>
			<i class="fa fa-map-marker"></i>
			<spring:message code="heading.currencymaster" />
			<br> <small><spring:message
					code="heading.specifylistofcurrencies" /> </small>
		</h1>
		<span id="errorMsg"></span>
		<c:if test="${!empty success}">
			<div class="alert alert-success alert-dismissable">
				<button type="button" id="success_close" class="close"
					data-dismiss="alert" aria-hidden="true">x</button>
				<h4>
					<i class="fa fa-check-circle"></i>
					<spring:message code="label.success" />
				</h4>
				<spring:message code="${success}" />
			</div>
		</c:if>
		<c:if test="${!empty error}">
			<div class="alert alert-danger alert-dismissable">
				<button type="button" class="close" data-dismiss="alert"
					aria-hidden="true">x</button>
				<h4>
					<i class="fa fa-times-circle"></i>
					<spring:message code="label.error" />
				</h4>
				<spring:message code="${error}" />
			</div>
		</c:if>
	</div>
</div>
<ul class="breadcrumb breadcrumb-top">
	<li><spring:message code="heading.managecurrencies" /></li>
	<li><a href="#"><spring:message code="label.currency" /></a></li>
</ul>
<div class="block full gridView">
	<div class="block-title">
		<h2>
			<strong><strong><spring:message code="heading.activecurrency" /></strong></strong>
		</h2>
	</div>
<%-- 	<a href="#" id="addCurrency"><spring:message --%>
<%-- 			code="label.addcurrency" /></a> --%>

	<a id="addCurrency" class="btn btn-sm btn-primary" href="#">
		<i class="fa fa-angle-right"></i>
		<spring:message code="label.addcurrency" />
	</a>
	<!--<p><a href="https://datatables.net/" target="_blank">DataTables</a> is a plug-in for the Jquery Javascript library. It is a highly flexible tool, based upon the foundations of progressive enhancement, which will add advanced interaction controls to any HTML table. It is integrated with template's design and it offers many features such as on-the-fly filtering and variable length pagination.</p> -->
	<div class="table-responsive">
		<table id="active-datatable"
			class="table table-vcenter table-condensed table-bordered">
			<thead>
				<tr>
					<th class="text-center"><spring:message code="label.id" /></th>
					<th class="text-center"><spring:message
							code="label.currencyname" /></th>
					<th class="text-center"><spring:message
							code="label.currencysign" /></th>
					<th class="text-center"><spring:message
							code="label.currencycode" /></th>
					<th class="text-center"><spring:message code="label.actions" /></th>
				</tr>
			</thead>
			<tbody id="tbody">
				<c:forEach var="currentCurrency" items="${currencyList}"
					varStatus="count">
					<tr>
						<td class="text-center">${count.count}</td>
						<td class="text-left"><c:out
								value="${currentCurrency.currencyName}" /></td>
						<td class="text-left"><c:out
								value="${currentCurrency.currencySign}" /></td>
						<td class="text-left"><c:out
								value="${currentCurrency.currencyCode}" /></td>
						<td class="text-center">
							<div class="btn-group">
								<a href="#" data-toggle="tooltip" title="Edit"
									onclick="editCurrency(${currentCurrency.currencyId},${count.count})"
									class="btn btn-xs btn-default"><i class="fa fa-pencil"></i>
								</a> <a href="#delete_currency_popup" data-toggle="modal"
									onclick="deleteCurrency(${currentCurrency.currencyId})"
									title="Delete" class="btn btn-xs btn-danger"><i
									class="fa fa-times"></i> </a>
							</div>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
</div>
<div class="block full gridView">
	<div class="block-title">
		<h2>
			<strong><spring:message code="heading.inactivecurrency" /></strong>
		</h2>
	</div>
	<div class="table-responsive">
		<table id="inactive-datatable"
			class="table table-vcenter table-condensed table-bordered">
			<thead>
				<tr>
					<th class="text-center"><spring:message code="label.id" /></th>
					<th class="text-center"><spring:message
							code="label.currencyname" /></th>
					<th class="text-center"><spring:message
							code="label.currencysign" /></th>
					<th class="text-center"><spring:message
							code="label.currencycode" /></th>
					<th class="text-center"><spring:message code="label.actions" /></th>
				</tr>
			</thead>
			<tbody id="tablebody">
				<c:forEach var="currentCurrency" items="${inActivecurrencyList}"
					varStatus="count">
					<tr>
						<td class="text-center">${count.count}</td>
						<td class="text-left"><c:out
								value="${currentCurrency.currencyName}" /></td>
						<td class="text-left"><c:out
								value="${currentCurrency.currencySign}" /></td>
						<td class="text-left"><c:out
								value="${currentCurrency.currencyCode}" /></td>
						<td class="text-center">
							<div class="btn-group">
								<a href="#restore_Currency_popup" data-toggle="modal"
									onclick="restoreCurrency(${currentCurrency.currencyId})"
									title="Restore" class="btn btn-xs btn-danger"><i
									class="fa fa-times"></i> </a>
							</div>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>


</div>

<div id="delete_currency_popup" class="modal fade" tabindex="-1"
	role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<form action="" method="post" class="form-horizontal form-bordered">
					<div style="padding: 10px; height: 110px;">
						<label><spring:message
								code="validation.doyouwanttodeletethiscurrency" /></label>
						<div class="col-xs-12 text-right">
							<button type="button" class="btn btn-sm btn-default"
								data-dismiss="modal">
								<spring:message code="label.no" />
							</button>
							<div id="delete_currency" class="btn btn-sm btn-primary">
								<spring:message code="label.yes" />
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<div id="restore_Currency_popup" class="modal fade" tabindex="-1"
	role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<form action="" method="post" class="form-horizontal form-bordered">
					<div style="padding: 10px; height: 110px;">
						<label><spring:message code="validation.doyouwanttorestorethiscurrency"/></label>
						<div class="col-xs-12 text-right">
							<button type="button" class="btn btn-sm btn-default"
								data-dismiss="modal"><spring:message code="label.no"/></button>
							<div id="restore_Currency" class="btn btn-sm btn-primary"><spring:message code="label.yes"/></div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<div class="block full" id="formView" style="display: none">
	<div class="block-title">
		<h2>
			<strong><spring:message code="label.currencymaster" /></strong>
		</h2>
	</div>
	<form action="" method="post" class="form-horizontal"
		id="Currency_form">
		<div class="form-group">
			<input id="currency_Id" name="currencyId" type="hidden" value="0">
			<label class="col-md-3 control-label" for="currency_Name"><spring:message
					code="label.currencyname" /><span class="text-danger">*</span> </label>
			<div class="col-md-9">
				<input id="currency_Name" name="currencyName" class="form-control"
					placeholder="<spring:message code="label.currencyname"/>.."
					type="text">
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-3 control-label" for="currency_Sign"><spring:message
					code="label.currencysign" /><span class="text-danger">*</span> </label>
			<div class="col-md-9">
				<input id="currency_Sign" name="currencySign" class="form-control"
					placeholder="<spring:message code="label.currencysign"/>.."
					type="text">
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-3 control-label" for="currency_Code"><spring:message
					code="label.currencycode" /><span class="text-danger">*</span></label>
			<div class="col-md-9">
				<input id="currency_Code" name="currencyCode" class="form-control"
					placeholder="<spring:message code="label.currencycode"/>.."
					type="text">
			</div>
		</div>

		<div class="form-group form-actions">
			<div class="col-md-9 col-md-offset-3">
				<div id="currency_submit" class="btn btn-sm btn-primary">
					<i class="fa fa-angle-right"></i>
					<spring:message code="button.save" />
				</div>
				<button type="reset" class="btn btn-sm btn-warning">
					<i class="fa fa-repeat"></i>
					<spring:message code="button.reset" />
				</button>
				<a class="btn btn-sm btn-primary" id="cancel" href="#">
					<i class="gi gi-remove"></i>
					<spring:message code="button.cancel" />
				</a>
			</div>
		</div>
	</form>
</div>

<script src="<%=contexturl %>resources/js/pages/activeTablesDatatables.js"></script>
<script src="<%=contexturl %>resources/js/pages/inactiveTablesDatatables.js"></script>
<script>$(function(){ ActiveTablesDatatables.init(4); });</script>
<script>$(function(){ InActiveTablesDatatables.init(4); });</script>

<script type="text/javascript">
	$(document).ready(function() {
		
		$("#addCurrency").click(function() {
			$('#Currency_form').find("input[type=text], textarea").val("");
			$('#Currency_form').find("input[type=hidden], textarea").val("0");
			$("#formView").css("display", "block");
			$(".gridView").css("display", "none");
			$("#success_close").click();
		});
		
		$("#cancel").click(function() {
			$("#formView").css("display", "none");
			$(".gridView").css("display", "block");
			$('.help-block').css('display', 'none');
			$("div.form-group").removeClass('has-error');
			$("#success_close").click();
		});
		
		$("#Currency_form").validate(
				{	errorClass:"help-block animation-slideDown",
					errorElement:"div",
					errorPlacement:function(e,a){a.parents(".form-group > div").append(e)},
					highlight:function(e){$(e).closest(".form-group").removeClass("has-success has-error").addClass("has-error")},
					success:function(e){e.closest(".form-group").removeClass("has-success has-error")},
					rules:{ currencyName:{required:!0,maxlength : 75},
							currencySign:{required:!0,maxlength : 3},
							currencyCode:{required:!0,maxlength : 3}
					},
					messages:{currencyName:{required : '<spring:message code="validation.pleaseentercurrencyname"/>',
						maxlength : '<spring:message code="validation.currency75character"/>'},
						currencySign:{required : '<spring:message code="validation.pleaseentercurrencysign"/>',
							maxlength : '<spring:message code="validation.currencysign"/>'},
						currencyCode:{required : '<spring:message code="validation.pleaseentercurrencycode"/>',
							maxlength :'<spring:message code="validation.currencycode"/>'}
					},
					
					submitHandler: function() {
						var data = $("#Currency_form").serialize();
						$.ajax({
							type: "POST",
							async: false,
							url: "<%=contexturl%>Admin/SaveCurrency",
							data:data,
							success: function(data, textStatus, jqXHR){
								var i = data.trim().indexOf("errorCode");
								if(i == 2){
									var obj = $.parseJSON(data.trim());
									var html = '<div class="alert alert-danger alert-dismissable">	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="fa fa-times-circle"></i> Error</h4> <span >'+obj.errorCode+'</span></div>';
									$("#errorMsg").html(html).show();
									$("#errorMsg").html(html).fadeOut(8000);
									$('html, body').animate({ scrollTop: $("#errorMsg").offset().top },500);
								}
								else{
									$('html, body').animate({ scrollTop: $("#errorMsg").offset().top },500);
									$('#page-content').html(data);
								}
							},
							dataType: 'html'
						});
					}
				});
		
		$("#currency_submit").click(function(){
			 $("#Currency_form").submit();
		});
		
		
		$("#restore_Currency").click(function(){
			$.ajax({
				type: "GET",
				async: false,
				url: "<%=contexturl%>Admin/RestoreCurrency?id="+restoreId,
				success: function(data, textStatus, jqXHR){
					$('#page-content').html(data);
					$('body').removeClass('modal-open');
					$('.modal-backdrop').remove();
					$('#restore_currency_popup').modal('hide');
					$('html, body').animate({ scrollTop: $("#errorMsg").offset().top },500);
				},
				dataType: 'html'
			});
		});

		
	$("#delete_currency").click(function(){
			$.ajax({
				type: "GET",
				async: false,
				url: "<%=contexturl%>Admin/DeleteCurrency?id="+selectedId,
				success: function(data, textStatus, jqXHR){
					$('#page-content').html(data);
					$('body').removeClass('modal-open');
					$('.modal-backdrop').remove();
					$('#delete_currency_popup').modal('hide');	
					$('html, body').animate({ scrollTop: $("#errorMsg").offset().top },500);
				},
				dataType: 'html'
			});
		});
	});
	
	function editCurrency(id,position){
		var oTable = $("#active-datatable").dataTable();
		var data = oTable.fnGetData()[position-1];
		$("#currency_Id").val(id);
		$("#currency_Name").val(data[1]);
		$("#currency_Sign").val(data[2]);
		$("#currency_Code").val(data[3]);
		$("#formView").css("display", "block");
		$(".gridView").css("display", "none");
		$('.help-block').css('display', 'none');
		$("div.form-group").removeClass('has-error');
		$("#success_close").click();
	}
	
	function deleteCurrency(id){
		selectedId = id;
	}
	
	function restoreCurrency(id){
		restoreId = id;
	}
</script>