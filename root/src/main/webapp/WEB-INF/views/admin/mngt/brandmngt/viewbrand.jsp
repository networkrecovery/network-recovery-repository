<%@page import="com.astrika.outletmngt.model.BrandType"%>
<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/template_start.jsp"%>
<%@include file="../../inc/page_head.jsp"%>
<div id="page-content">
<div class="content-header">
		<div class="header-section">
			<h1>
				<i class="fa fa-map-marker"></i>
				<spring:message code="heading.brand" />
				<br> <small>View Brand</small>
			</h1>
			<span id="errorMsg"></span>
			<c:if test="${!empty success}">
				<div class="alert alert-success alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-check-circle"></i> <spring:message code="label.success"/>
					</h4>
					<spring:message code="${success}" />
				</div>
			</c:if>

			<c:if test="${!empty error}">
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-times-circle"></i> <spring:message code="label.error"/>
					</h4>
					${error}
				</div>
			</c:if>
		</div>
	</div>
<ul class="breadcrumb breadcrumb-top">
		<li>You can change your details here</li>
		<li><a href="<%=contexturl %>ManageBrand/BrandList/EditBrand?id=${brand.brandId}" id="brand">Edit Brand</a></li>
	</ul>
<form action="<%=contexturl %>ManageBrand/BrandList/UpdateBrand" method="post"
			class="form-horizontal " id="Brand_form">
			
	<div class="row">
		
			<div class="col-md-6">
				<div class="block">
					<div class="block-title">
						<h2>
							<strong><spring:message code="heading.brandinfo"/></strong> 
						</h2>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label" for="currencyFrom_Name"><spring:message code="label.companyname"/><span class="text-danger">*</span></label>
						<div class="col-md-6">
							<label class="control-label">${brand.company.companyName}</label>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label" for="brand_Name"><spring:message code="label.brandname"/> <span class="text-danger">*</span>
						</label>
						<div class="col-md-6">
							
								<input id="brand_Name" name="brandName" class="form-control"
									placeholder="<spring:message code='label.brandname'/>.." type="text"  value="${brand.brandName }"  disabled="disabled"> 
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-md-4 control-label" for="gourmet7_Reach"><spring:message code="label.incudeinmembership"/></label>
						<div class="col-md-6">
							<label class="switch switch-primary">
								<input	id="include" name="include" type="checkbox" <c:if test="${brand.includeInMembership}">checked</c:if>  disabled="disabled"/>
							<span></span></label>
						</div>
					</div>
				</div>
				
					
				<div class="block">
					<div class="block-title">
						<h2>
							<strong><spring:message code="heading.contactinfo" /></strong>
						</h2>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label" for="telephone1"><spring:message
								code="label.telephone1" /><span class="text-danger">*</span></label>
						<div class="col-md-6">

							<input id="brandTelephone1" name="brandTelephone1"
								class="form-control"
								placeholder="<spring:message code='label.telephone1'/>.."
								type="text" value="${brand.brandTelephone1}"  disabled="disabled">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label" for="telephone2"><spring:message
								code="label.telephone2" /></label>
						<div class="col-md-6">

							<input id="brandTelephone2" name="brandTelephone2"
								class="form-control"
								placeholder="<spring:message code='label.telephone2'/>.."
								type="text" value="${brand.brandTelephone2}"  disabled="disabled">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label" for="rest_email"><spring:message
								code="label.brandemail" /><span class="text-danger">*</span></label>
						<div class="col-md-6">

							<input id="brand_email" name="brandEmail"
								class="form-control"
								placeholder="<spring:message code="label.brandemail"/>.."
								type="text" value="${brand.brandEmail}"  disabled="disabled">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label" for="mobile1"><spring:message
								code="label.mobile1" /><span class="text-danger">*</span></label>
						<div class="col-md-6">

							<input id="brandMobile1" name="brandMobile1"
								class="form-control"
								placeholder="<spring:message code="label.mobile1"/>.."
								type="text" value="${brand.brandMobile1}"  disabled="disabled">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label" for="mobile2"><spring:message
								code="label.mobile2" /></label>
						<div class="col-md-6">

							<input id="brandMobile2" name="brandMobile2"
								class="form-control"
								placeholder="<spring:message code="label.mobile2"/>.."
								type="text" value="${brand.brandMobile2}"  disabled="disabled">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label" for="addressline1"><spring:message
								code="label.addressline1" /><span class="text-danger">*</span>
						</label>
						<div class="col-md-6">

							<input id="brandAddressline1" name="brandAddressLine1"
								class="form-control"
								placeholder="<spring:message code="label.addressline1"/>.."
								type="text" value="${brand.brandAddressLine1}"  disabled="disabled">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label" for="addressline2"><spring:message
								code="label.addressline2" /></label>
						<div class="col-md-6">

							<input id="brandAddressline2" name="brandAddressLine2"
								class="form-control"
								placeholder="<spring:message code="label.addressline2"/>.."
								type="text" value="${brand.brandAddressLine2}"  disabled="disabled">
						</div>
					</div>
					
					
					
					
					<div class="form-group">
							<label class="col-md-4 control-label" for="country"><spring:message
									code="label.countryname" /><span class="text-danger">*</span></label>
							<div class="col-md-6">
								<input id="country" name="country" disabled="disabled"
									class="form-control" value="${brand.brandCountry.countryName}"
								type="text">
							</div>
						</div>
					
					<div class="form-group">
							<label class="col-md-4 control-label" for="city"><spring:message
									code="label.countryname" /><span class="text-danger">*</span></label>
							<div class="col-md-6">
								<input id="city" name="city" disabled="disabled"
									class="form-control" value="${brand.brandCity.cityName}"
									
									type="text">
							</div>
						</div>

					<div class="form-group">
							<label class="col-md-4 control-label" for="area"><spring:message
									code="label.countryname" /><span class="text-danger">*</span></label>
							<div class="col-md-6">
								<input id="area" name="area" disabled="disabled"
									class="form-control" value="${brand.brandArea.areaName}"
									
									type="text">
							</div>
						</div>

					<div class="form-group">
						<label class="col-md-4 control-label" for="brandPincode"><spring:message
								code="label.pincode" /><span class="text-danger">*</span></label>
						<div class="col-md-6">

							<input id="brandPincode" name="brandPincode"
								class="form-control"
								placeholder="<spring:message code="label.pincode"/>.."
								type="text" value="${brand.brandPincode }"  disabled="disabled">
						</div>
					</div>




				</div>

				
			</div>
			<div class="col-md-6">
				<div class="block">
					<div class="block-title">
						<h2>
							<strong><spring:message code="heading.admininfo"/></strong>
						</h2>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label" for="first_Name"><spring:message code="label.firstname"/><span class="text-danger">*</span>
						</label>
						<div class="col-md-6">
							
								<input id="first_Name" name="firstName" class="form-control"
									placeholder="<spring:message code='label.firstname'/>.." type="text"  value="${brand.brandAdmin.firstName}"  disabled="disabled"> 
						</div>
					</div>
	
					<div class="form-group">
						<label class="col-md-4 control-label" for="last_Name"><spring:message code="label.lastname"/><span class="text-danger">*</span>
						</label>
						<div class="col-md-6">
							
								<input id="last_Name" name="lastName" class="form-control"
									placeholder="<spring:message code="label.lastname"/>.." type="text" value="${brand.brandAdmin.lastName}"  disabled="disabled">
						</div>
					</div>
	
	
					<div class="form-group">
						<label class="col-md-4 control-label" for="email"><spring:message code="label.emailid"/><span
							class="text-danger">*</span></label>
						<div class="col-md-6">
								<input id="email" name="emailId" class="form-control"
									placeholder="test@example.com" type="text" value="${brand.brandAdmin.emailId}"  disabled="disabled">
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-md-4 control-label" for="mobile"><spring:message code="label.mobile"/><span
							class="text-danger">*</span></label>
						<div class="col-md-6">
								<input id="mobile" name="mobile" class="form-control"
									placeholder='<spring:message code="label.mobile"></spring:message>' type="text" value="${brand.brandAdmin.mobile}"  disabled="disabled">
						</div>
					</div>
					<div class="form-group">
                        <label class="col-md-4 control-label"><spring:message code="label.userlogin"/></label>
                        <div class="col-md-8">
                            <p class="form-control-static">${brand.brandAdmin.loginId}</p>
                        </div>
                    </div>
<!-- 					<div class="form-group"> -->
<%-- 						<a href="#reset-user-settings" data-toggle="modal" class="col-md-4 control-label" ><spring:message code="label.resetpassword"/></a> --%>
<!-- 					</div> -->
					
				</div>
				<div class="block">
					<%@include file="../featurecontrol/featurecontrol.jsp"%>
				</div>
			</div>
	
	</div>
	
	
	
	
<!-- 	<div class="form-group form-actions"> -->
<!-- 						<div class="col-md-9 col-md-offset-3"> -->
<!-- 							<button id="brand_submit" type="submit" -->
<!-- 								class="btn btn-sm btn-primary"> -->
<%-- 								<i class="fa fa-angle-right"></i> <spring:message code="button.submit" /> --%>
<!-- 							</button> -->
<!-- 							<button type="reset" class="btn btn-sm btn-warning"> -->
<!-- 								<i class="fa fa-repeat"></i> -->
<%-- 								<spring:message code="button.reset" /> --%>
<!-- 							</button> -->
<%-- 								<a class="btn btn-sm btn-primary" href="<%=contexturl %>ManageBrand/BrandList"><i class="gi gi-remove"></i> <spring:message code="button.cancel" /> --%>
<!-- 								</a> -->
<!-- 						</div> -->
<!-- 					</div> -->
	
	
	
</form>
</div>

<%@include file="../../inc/page_footer.jsp"%>
<%@include file="../../inc/template_scripts.jsp"%>
<script src="<%=contexturl %>resources/js/custom/featurecontrol.js"></script>
<!-- <div id="reset-user-settings" class="modal fade" tabindex="-1" -->
<!-- 	role="dialog" aria-hidden="true"> -->
<!-- 	<div class="modal-dialog"> -->
<!-- 		<div class="modal-content"> -->
<!-- 			<div class="modal-header text-center"> -->
<!-- 				<h2 class="modal-title"> -->
<%-- 					<i class="fa fa-pencil"></i> <spring:message code="label.resetpassword"/> --%>
<!-- 				</h2> -->
<!-- 			</div> -->
<!-- 			<span id="passwordErrorMsg"></span> -->
<!-- 			<div class="modal-body"> -->
<%-- 				<form action="<%=contexturl %>ManageBrand/BrandList/ResetBrandAdminPassword" method="post" class="form-horizontal form-bordered"  id="ResetPassword_Form"> --%>
<%-- 					<input  name="brandId"  value="${brand.brandId}" type="hidden">  --%>
<%-- 					<input  name="userId"  value="${brand.brandAdmin.userId}" type="hidden">  --%>
<!-- 					<fieldset> -->
<!-- 						<div class="form-group"> -->
<!-- 							<label class="col-md-4 control-label" -->
<%-- 								for="user-settings-password"><spring:message code="label.newpassword"/></label> --%>
<!-- 							<div class="col-md-8"> -->
<!-- 								<input type="password" id="password" -->
<!-- 									name="password" class="form-control" -->
<!-- 									placeholder="Please choose a complex one.."> -->
<!-- 							</div> -->
<!-- 						</div> -->
<!-- 						<div class="form-group"> -->
<!-- 							<label class="col-md-4 control-label" -->
<%-- 								for="user-settings-repassword"><spring:message code="label.confirmnewpassword"/></label> --%>
<!-- 							<div class="col-md-8"> -->
<!-- 								<input type="password" id="user-settings-repassword" -->
<!-- 									name="confirmPassword" class="form-control" -->
<!-- 									placeholder="..and confirm it!"> -->
<!-- 							</div> -->
<!-- 						</div> -->
<!-- 					</fieldset> -->
<!-- 					<div class="form-group form-actions"> -->
<!-- 						<div class="col-xs-12 text-right"> -->
<!-- 							<button type="button" class="btn btn-sm btn-default" -->
<%-- 								data-dismiss="modal"><spring:message code="label.close"/></button> --%>
<%-- 							<button id="submit_password" class="btn btn-sm btn-primary" type="submit"><spring:message code="label.savechanges"/> --%>
<!-- 								</button> -->
<!-- 						</div> -->
<!-- 					</div> -->
<!-- 				</form> -->
<!-- </div> -->
<!-- </div> -->
<!-- </div> -->
<!-- </div> -->



<style>

.chosen-container {
	width: 250px !important;
}
</style>
<%@include file="../../inc/template_end.jsp"%>