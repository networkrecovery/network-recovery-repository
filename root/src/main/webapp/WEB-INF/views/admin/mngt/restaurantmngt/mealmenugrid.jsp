<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->

<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/template_start.jsp"%>
<%@include file="../../inc/page_head.jsp"%>

<div id="page-content">
	<div class="content-header">
		<div class="header-section">
			<h1>
				<i class="fa fa-map-marker"></i> <spring:message code="heading.meal"/><br> <small><spring:message code="heading.specifylistofmeals"/>
					</small>
			</h1>
			<c:if test="${!empty success}">
				<div class="alert alert-success alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-check-circle"></i> <spring:message code="label.success"/>
					</h4>
					<spring:message code="${success}" />
				</div>
			</c:if>
			<c:if test="${!empty error}">
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-times-circle"></i> <spring:message code="label.error"/>
					</h4>
					<spring:message code="${error}" />
				</div>
			</c:if>
		</div>
	</div>
	<ul class="breadcrumb breadcrumb-top">
		<li><spring:message code="heading.managerestaurant"/></li>
		<li><a href=""><spring:message code="heading.meal"/></a></li>
	</ul>
	<div class="block full" id="gridView">
		<div class="block-title">
			<h2>
				<strong><spring:message code="heading.meal"/></strong>
			</h2>
		</div>
		<a href="AddMealMenu?outletId=${outletId}" id="addMealMenu" class="btn btn-sm btn-primary"><spring:message code="label.addmeal"/></a>
		<div class="table-responsive">
			<table id="example-datatable"
				class="table table-vcenter table-condensed table-bordered">
				<thead>
					<tr>
						<th class="text-center"><spring:message code="label.id"/></th>
						<th class="text-center"><spring:message code="label.title"/></th>
						<th class="text-center"><spring:message code="label.cost"/>(<c:out value="${currency.currencySign}" />)</th>
						<th class="text-center"><spring:message code="label.actions"/></th>
					</tr>
				</thead>
				<tbody id="tbody">
					<c:forEach var="currentMeal" items="${mealMenuList}"
						varStatus="count">

						<tr>
							<td class="text-center">${count.count}</td>
							<td class="text-center"><c:out value="${currentMeal.title}" />
							</td>
							<td class="text-center"><c:out value="${currency.currencySign}" /> <c:out value="${currentMeal.cost}" />
							</td>
							<td class="text-center">
								<div class="btn-group">
									<a
										href="EditMealMenu?id=${currentMeal.mealMenuId}&outletId=${outletId}"
										data-toggle="tooltip" title="Edit"
										class="btn btn-xs btn-default"><i class="fa fa-pencil"></i>
									</a> <a href="#delete_mealMenu_popup" data-toggle="modal"
										onclick="deleteMealMenu(${currentMeal.mealMenuId})" title="Delete"
										class="btn btn-xs btn-danger"><i class="fa fa-times"></i>
									</a>
								</div>
							</td>
						</tr>
					</c:forEach>

				</tbody>
			</table>
		</div>
	</div>
	<div id="delete_mealMenu_popup" class="modal fade" tabindex="-1"
		role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<form action="DeleteMealMenu" method="post" class="form-horizontal form-bordered"
						id="delete_mealMenu_Form">
						<div style="padding: 10px; height: 110px;">
							<label><spring:message code="validation.doyouwanttodeletethemeal"/></label>
							<div class="col-xs-12 text-right">
								<button type="button" class="btn btn-sm btn-default"
									data-dismiss="modal"><spring:message code="label.no"/></button>
								<div id="delete_mealYes" class="btn btn-sm btn-primary"><spring:message code="label.yes"/></div>
								<input type="hidden" name="outletId" id="outletId">
								<input type="hidden" name="id" id="mealMenuId">
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<%@include file="../../inc/page_footer.jsp"%>
<%@include file="../../inc/template_scripts.jsp"%>
<script src="../resources/js/pages/tablesDatatables.js"></script>
<script>$(function(){ TablesDatatables.init(); });</script>

<script type="text/javascript">
	var selectedId = 0;
	$(document).ready(function() {
		
		$("#delete_mealYes").click(function(){
			$("#outletId").val(${outletId});
			$("#mealMenuId").val(selectedId);
			$("#delete_mealMenu_Form").submit();
		});
		
	});
	
	function deleteMealMenu(id){
		selectedId = id;
	}
</script>
<%@include file="../../inc/template_end.jsp"%>