package com.astrika.globalsponsoredmngt.service.impl;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.astrika.common.exception.NoSuchImageException;
import com.astrika.common.model.ImageMaster;
import com.astrika.common.service.ImageService;
import com.astrika.globalsponsoredmngt.exception.ContactImagePresentException;
import com.astrika.globalsponsoredmngt.model.GlobalSponsoredManagement;
import com.astrika.globalsponsoredmngt.repository.GlobalSponsoredRepository;
import com.astrika.globalsponsoredmngt.service.GlobalSponsoredService;
import com.astrika.kernel.exception.CustomException;

@Service
public class GlobalSponsoredServiceImpl implements GlobalSponsoredService {

	@Autowired
	private GlobalSponsoredRepository globalSponsoredRepository;

	@Autowired
	private ImageService imageService;

	@Override
	public List<GlobalSponsoredManagement> findByActive(boolean active) {
		return globalSponsoredRepository.findByActive(active);
	}

	@Override
	public GlobalSponsoredManagement save(List<ImageMaster> globalSponsorImage,
			MultipartFile contactimage, String imageType) throws IOException {

		ImageMaster contactImage = null;
		GlobalSponsoredManagement imageManagement = new GlobalSponsoredManagement();
		if("Contact".equals(imageType)){
			GlobalSponsoredManagement imageMaster = findByImageType(imageType);
			if(imageMaster!=null){
				throw new ContactImagePresentException(
						CustomException.CONTACT_IMAGE_PRESENT.getCode());
			}
			if (contactimage != null && contactimage.getSize() > 0) {
				String filename = contactimage.getOriginalFilename();
				String ext = filename.substring(filename.lastIndexOf("."));
				contactImage = imageService.addImage(contactimage.getBytes(), ext,
						"ContactImage" + ext, true, false,"Contact Image");
			}
			imageManagement.setContactImage(contactImage);
			imageManagement.setGlobalImage(null);
			imageManagement.setImageType(imageType);
			globalSponsoredRepository.save(imageManagement);
			return imageManagement;
		}

		else{
			if (globalSponsorImage != null && !globalSponsorImage.isEmpty()) {
				List<GlobalSponsoredManagement> sponsorImages = new ArrayList<>();
				for (ImageMaster image : globalSponsorImage) {
					GlobalSponsoredManagement sponsorImg = new GlobalSponsoredManagement(image, null, imageType);
					sponsorImages.add(sponsorImg);
				}
				globalSponsoredRepository.save(sponsorImages);
			}
			return null;
		}

	}

	@Override
	public void deleteImage(long imageId) throws NoSuchImageException,
	UnsupportedEncodingException {
		GlobalSponsoredManagement image = globalSponsoredRepository.findByImageId(imageId);
		globalSponsoredRepository.delete(image.getGlobalId());
		if("Contact".equals(image.getImageType())){
			imageService.delete(image.getContactImage().getImageId());
		}
		else{
			imageService.delete(image.getGlobalImage().getImageId());
		}
	}

	@Override
	public GlobalSponsoredManagement findByImageType(String imageType) {
		return globalSponsoredRepository.findByImageType(imageType);
	}

	@Override
	public GlobalSponsoredManagement findByImageId(long imageId) {
		return globalSponsoredRepository.findByImageId(imageId);
	}

	@Override
	public GlobalSponsoredManagement update(
			GlobalSponsoredManagement sponsorManagement) {
		return globalSponsoredRepository.save(sponsorManagement);
	}

	@Override
	public List<GlobalSponsoredManagement> findByActiveandImageTypeGlobalSponsor(
			boolean active, String imageType) {
		return globalSponsoredRepository.findByActiveandImageTypeGlobalSponsor(active, imageType);
	}

	@Override
	public GlobalSponsoredManagement findByActiveandImageTypeContact(
			boolean active, String imageType) {
		return globalSponsoredRepository.findByActiveandImageTypeContact(active, imageType);	
	}

}
