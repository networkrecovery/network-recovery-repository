package com.astrika.common.model;

public enum MemberType {
	
	FREE_INDIVIDUAL(1),
	PAID_INDIVIDUAL(2),
	CORPORATE(3);

	private final int id;
	
	private MemberType(int id){
		this.id = id;
	}
	
	public int getId() {
		return id;
	}


	public static MemberType fromInt(int id) {
		switch (id) {
		case 1:
			return FREE_INDIVIDUAL;
		case 2:
			return PAID_INDIVIDUAL;
		case 3:
			return CORPORATE;
		default:
			return null;
		}
	}
}
 