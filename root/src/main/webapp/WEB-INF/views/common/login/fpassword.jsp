<%@include file="../../admin/inc/config.jsp"%>


<!DOCTYPE html>
<html lang="en">


<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Atma - Strengthning organisations to impact Society</title>
<!-- Style -->
<!-- Bootstrap -->
<link href="<%=contexturl %>resources/css/bootstrap.css" rel="stylesheet" media="screen">
<link href="<%=contexturl %>resources/css/fontface.css" rel="stylesheet" media="screen">
<link href="<%=contexturl %>resources/css/font-awesome.css" rel="stylesheet" media="screen">
<link href="<%=contexturl %>resources/css/jquery.bxslider.css" rel="stylesheet" media="screen">
<!--[if lt IE 9]>
  <script src="js/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="js/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
<!--[if IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!--SLIDER -->
<link href="<%=contexturl %>resources/css/login.css" rel="stylesheet" media="screen">
</head>

<body>
	<div class="container">
		<div class="row">
			<div class="col-md-4"></div>
			<div class="col-md-4">
				<div class="bg-white">
					<div class="text-center">

						<img src="<%=contexturl %>resources/img/atma.jpg" />
						<p>Strengthening Orgnisations to impact Education</p>
						<br><small>Forgot <strong>Password ?</strong></small>
					</div>
					<hr>
					<c:if test="${!empty success}">
						<div class="alert alert-success alert-dismissable">
							<button type="button" class="close" data-dismiss="alert"
								aria-hidden="true">x</button>
							<h4>
								<i class="fa fa-check-circle"></i> Success
							</h4>
							<spring:message code="${success}" />
						</div>
					</c:if>

					<c:if test="${ !empty errorCode}">
						<div class="alert alert-danger alert-dismissable">
							<button type="button" class="close" data-dismiss="alert"
								aria-hidden="true">x</button>
							<h4>
								<i class="fa fa-times-circle"></i> Error
							</h4>
							<spring:message code='${errorCode}' />
						</div>
					</c:if>
					<form action="SubmitForgotPassword" method="post"
						id="candidate_login" class="mt10">
						<div class="form-group">
							<div>
							<label>Email </label> <input type="email" class="form-control"
								id="email" name="loginId">
							</div>
						</div>
						
						<div class="form-group">
							<p class="pull-left"><a href="Login" id="link-login">Go back to Login Page</</a></p>
							<button type="submit" class="btn pull-right">Send Mail</button>
							<span class="clearfix"></span>
						</div>
						
					</form>
				</div>
			</div>

			<div class="col-md-4"></div>
		</div>
	</div>
</body>

<script src="<%=contexturl %>resources/js/vendor/jquery-1.11.0.min.js"></script>
<script src="<%=contexturl %>resources/js/vendor/bootstrap.min.js"></script>
<script src="<%=contexturl %>resources/js/plugins.js"></script>
<script src="<%=contexturl %>resources/js/app.js"></script>
<script src="<%=contexturl %>resources/js/pages/login.js"></script>
<script>$(function(){ Login.init(); });</script>
<script type="text/javascript">
$(document).ready(function() {
	$("#candidate_login").validate(
				{	errorClass:"help-block animation-slideDown",
					errorElement:"div",
					errorPlacement:function(e,a){a.parents(".form-group > div").append(e)},
					highlight:function(e){$(e).closest(".form-group").removeClass("has-success has-error").addClass("has-error")},
					success:function(e){e.closest(".form-group").removeClass("has-success has-error")},
					rules:{ loginId:{required:!0}							
					},
					messages:{
						loginId : {
							required : "Please enter a loginId"
						}						
					}
				});		
	});
</script>

</html>
