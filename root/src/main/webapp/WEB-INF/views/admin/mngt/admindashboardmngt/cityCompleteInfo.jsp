<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/template_start.jsp"%>
<%@include file="../../inc/page_head.jsp"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<div id="page-content">
	<div class="content-header">
		<div class="header-section">
			<h1>
				Complete Information of City
			</h1>
			<c:if test="${!empty success}">
				<div class="alert alert-success alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-check-circle"></i>
						<spring:message code="label.success" />
					</h4>
					<spring:message code="${success}" />
				</div>
			</c:if>
			<div class="alert alert-danger alert-dismissable"
				style="display: none" id="error">
				<button type="button" class="close" data-dismiss="alert"
					aria-hidden="true">x</button>
				<h4>
					<i class="fa fa-times-circle"> </i> <spring:message code="label.error"/>
				</h4>
				<span id="errorMsg"></span>
			</div>
		</div>
	</div>
	
	<div class="block full  gridView">
		<a href="<%=contexturl %>SuperAdmin" id="atmaDashboardBack" class="btn btn-sm btn-primary save">Back to dashboard</a>
		<div class="table-responsive">
			<table id="active-datatable"
				class="table table-vcenter table-condensed table-bordered">
				<thead>
					<tr>
						<th class="text-center">City</th>
						<th class="text-center">No Of Companies</th>
					</tr>
				</thead>
				<tbody id="tbody">
					<c:forEach var="org" items="${results}" varStatus="count">
						<tr>
							<td class="text-center">${org.city_cityName}</td>
							<td class="text-center">
							${org.companyId}
								<%-- <div class="btn-group">
									<p class="form-control-static">
										<a
											href="<%=contexturl%>DownloadCompanyZip/${org.companyId}"><spring:message
												code="label.download" /> </a>
									</p>
								</div> --%>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>


</div>

<%@include file="../../inc/page_footer.jsp"%>
<%@include file="../../inc/template_scripts.jsp"%>

<script
	src="<%=contexturl%>resources/js/pages/activeTablesDatatables.js"></script>
<script
	src="<%=contexturl%>resources/js/pages/inactiveTablesDatatables.js"></script>
<script>$(function(){ ActiveTablesDatatables.init(3); });</script>
<script>$(function(){ InActiveTablesDatatables.init(3); });</script>

<%@include file="../../inc/template_end.jsp"%>