package com.astrika.modulemngt.exception;

import com.astrika.kernel.exception.BusinessException;
import com.astrika.kernel.exception.CustomException;

public class DuplicateModuleException extends BusinessException {


	public DuplicateModuleException() {
		super(CustomException.DUPLICATE_MODULE.getCode());
	}

	public DuplicateModuleException(String errorCode) {
		super(errorCode);
	}
}
