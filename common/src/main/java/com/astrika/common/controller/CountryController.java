package com.astrika.common.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.astrika.common.exception.DuplicateCountryException;
import com.astrika.common.exception.NoSuchCountryException;
import com.astrika.common.model.location.CountryMaster;
import com.astrika.common.service.CountryService;
import com.astrika.common.util.PropsValues;

@Controller
@RequestMapping("/*")
public class CountryController {

	@Autowired
	private CountryService countryServiceImpl;

	@Autowired
	private PropsValues propsValue;

	private static final String URL = "redirect:/Country";

	@RequestMapping("/CountryCaptcha")
	@ResponseBody
	public void countryCaptchaView(HttpServletRequest request,
			HttpServletResponse response) {
		/*sonar*/
	}

	@RequestMapping(value = "/Country", method = RequestMethod.POST)
	public String saveCountry(@ModelAttribute("country") CountryMaster country)
			throws DuplicateCountryException {
		countryServiceImpl.save(country.getCountryName());
		return URL;
	}

	@RequestMapping("/EditCountry/{countryId}")
	public ModelAndView countryEditView(
			@PathVariable("countryId") Long countryId) throws NoSuchCountryException {
		CountryMaster country = countryServiceImpl.findById(countryId);
		ModelAndView mv = new ModelAndView("/common/location/country/country",
				"country", country);
		mv.addObject("countries", countryServiceImpl.findByActive(true));
		return mv;
	}

	@RequestMapping(value = "/EditCountry/{countryId}", method = RequestMethod.POST)
	public String updateCountry(
			@ModelAttribute("country") CountryMaster country)
					throws DuplicateCountryException {
		countryServiceImpl.save(country.getCountryName());
		return "/common/location/country/country";
	}

	@RequestMapping("/DeleteCountry/{countryId}")
	public String deleteCountry(@PathVariable("countryId") Long countryId,
			Map<String, Object> model) throws NoSuchCountryException {
		countryServiceImpl.delete(countryId);
		model.put("successMsg", "Country deleted successfully");
		return URL;
	}

	@RequestMapping("/RestoreCountry/{countryId}")
	public String restoreCountry(@PathVariable("countryId") Long countryId,
			Map<String, Object> model) throws NoSuchCountryException {
		countryServiceImpl.restore(countryId);
		model.put("successMsg", "Country restore successfully");
		return URL;
	}

	@RequestMapping("/Country/{countryId}")
	@ResponseBody
	public CountryMaster getCountry(@PathVariable("countryId") Long countryId)
			throws NoSuchCountryException {
		return countryServiceImpl.findById(countryId);
	}


	@RequestMapping(value="/Country/FindAllActive",method=RequestMethod.GET)
	@ResponseBody 
	public List<CountryMaster> getAllActiveCountry() throws NoSuchCountryException {
		return countryServiceImpl.findByActive(true);
	}
}
