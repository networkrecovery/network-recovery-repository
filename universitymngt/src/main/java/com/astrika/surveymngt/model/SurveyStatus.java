package com.astrika.surveymngt.model;

public enum SurveyStatus {
	
	PENDING(0),
	COMPLETED(1),
	INCOMPLETED(2),
	SKIPPED(3);
	
	private final int id;

	private SurveyStatus(int id) {
		this.id = id;
	}
	

    public int getId() {
        return id;
    }

	public static SurveyStatus fromInt(int id){
		switch(id){
		case 0: 
			return PENDING;
		case 1: 
			return COMPLETED;
		case 2:
			return INCOMPLETED;
		case 3:
			return SKIPPED;
		default:
			return null;
		}
			
	}
}
