package com.astrika.common.service.impl;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.astrika.common.exception.DuplicateCountryException;
import com.astrika.common.exception.NoSuchCountryException;
import com.astrika.common.model.location.CountryMaster;
import com.astrika.common.repository.CountryRepository;
import com.astrika.common.service.CountryService;
import com.astrika.common.service.CurrencyService;
import com.astrika.common.util.CommonUtil;
import com.astrika.kernel.exception.CustomException;

/**
 * Service implementation for Country.
 * 
 * @author Priyanka
 */
@Service
public class CountryServiceImpl implements CountryService {


	//@Autowired
	private CommonUtil commonUtil;

	@Autowired
	private CountryRepository countryRepository;

	@Autowired
	private CurrencyService currencyService; 


	@Override
	public CountryMaster save(String countryName)
			throws DuplicateCountryException {
		CountryMaster countryMaster = findByName(countryName);
		if (countryMaster != null) {// country already exist
			throw new DuplicateCountryException(
					CustomException.DUPLICATE_COUNTRY.getCode());
		} else {
			CountryMaster country = new CountryMaster(countryName);
			if (commonUtil != null){
				commonUtil.deleteCache("country");
			}
			return countryRepository.save(country);
		}
	}

	@Override
	public CountryMaster update(long countryId, String countryName)
			throws DuplicateCountryException{
		CountryMaster country = findByName(countryName);
		if (country != null && country.getCountryId() != countryId) {
			throw new DuplicateCountryException(
					CustomException.DUPLICATE_COUNTRY.getCode());
		} else {
			country =findById(countryId);
			country.setCountryName(countryName);
			return countryRepository.save(country);

		}
	}

	public CountryMaster delete(long countryId) throws NoSuchCountryException {
		CountryMaster country = findById(countryId);
		country.setActive(false);
		return countryRepository.save(country);
	}

	public CountryMaster restore(long countryId) throws NoSuchCountryException {
		CountryMaster country = findById(countryId);
		country.setActive(true);
		return countryRepository.save(country);
	}

	public List<CountryMaster> findByActive(boolean active) {
		return countryRepository.findByActiveOrderByCountryNameAsc(active);

	}

	@Override
	public CountryMaster findById(long countryId) throws NoSuchCountryException {
		CountryMaster country = countryRepository.findOne(countryId);
		if (country == null) {
			throw new NoSuchCountryException(
					CustomException.NO_SUCH_COUNTRY.getCode());
		}
		return country;
	}


	@Override
	public List<CountryMaster> findByIds(Long[] countries) {
		Iterable<Long> countryIds = Arrays.asList(countries);
		return countryRepository.findAll(countryIds);
	}

	@Override
	public CountryMaster findByName(String name) {
		return countryRepository.findByCountryName(name);
	}


}
