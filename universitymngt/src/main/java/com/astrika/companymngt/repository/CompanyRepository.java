package com.astrika.companymngt.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.astrika.companymngt.model.company.CompanyMaster;




public interface CompanyRepository extends JpaRepository<CompanyMaster, Long> {

	@Query(value="select c from CompanyMaster c left join fetch c.admin where c.active=1 Order By c.companyName ASC ",countQuery="select count(c) from CompanyMaster c where c.active = 1")
	Page<CompanyMaster> findByActiveTrueOrderByCompanyNameAsc(Pageable page);

	@Query(value="select c from CompanyMaster c left join fetch c.admin left join fetch c.deActivatedBy where c.active=1 Order By c.companyName ASC")
	List<CompanyMaster> findByActiveTrueOrderByCompanyNameAsc();

	@Query(value="select c from CompanyMaster c left join fetch c.admin where c.active=0 Order By c.companyName ASC ",countQuery="select count(c) from CompanyMaster c where c.active = 0")
	Page<CompanyMaster> findByActiveFalseOrderByCompanyNameAsc(Pageable page);
	
	@Query("select c from CompanyMaster c left join fetch c.state left join fetch c.city left join fetch c.country left join fetch c.admin where c.active=?1 Order By c.companyName ASC ")
	List<CompanyMaster> findAllByActive(boolean active);

	@Query("select c from CompanyMaster c where c.companyName=?1")
	CompanyMaster findByCompanyName(String name);
	
	@Query("select c from CompanyMaster c left join fetch c.state left join fetch c.city left join fetch c.country left join fetch c.admin where c.companyId = ?1")
	CompanyMaster findByCompanyCompanyId(long id);

	@Query("select c from CompanyMaster c left join fetch c.state left join fetch c.city left join fetch c.country where c.admin.userId = ?1")
	CompanyMaster findByUserId(long userId);
	
	@Query("select count(companyId) from CompanyMaster c where c.typeOfEducation=?1")
	int NoOfOrgPerEduArea(String educationArea);
	
	@Query("select c from CompanyMaster c where c.typeOfEducation=?1")
	List<CompanyMaster> findByEduArea(String educationArea);
	
	@Query("select c from CompanyMaster c where c.companyEmail=?1")
	CompanyMaster findByCompanyEmail(String emailId);
	
	@Query("select  c, count(c.companyId) from CompanyMaster c left join fetch c.city where c.active = 1  group by c.city.cityId")
	List<Object[]> findOrgByCity();
	
	@Query(value="select  c, count(c.companyId) from CompanyMaster c left join fetch c.city where c.active = 1  group by c.city.cityId ",countQuery="select count(c) from CompanyMaster c where c.active = 1")
	Page<Object[]> findByFiveOrgByCity(Pageable page);

	@Query("select c from CompanyMaster c where c.active=1 and DATEDIFF(now(),c.createdOn) <30")
	List<CompanyMaster> activeInLastMonth();

	@Query("select count(c.companyId) as companyId,c.city.cityName as city_cityName from CompanyMaster c group by c.city.cityId order by count(c.companyId) desc")
	List<Object[]> getTopCities();

	@Query("select count(c.companyId),c.typeOfEducation from CompanyMaster c where c.active=1 GROUP by c.typeOfEducation")
	List<Object[]> getAllRecod();

	@Query("select c from CompanyMaster c where c.active=1 and DATEDIFF(now(),c.createdOn) >30")
	List<CompanyMaster> activeInMoteThanMonth();

	@Query("select count(c.companyId) from CompanyMaster c where c.active=1")
	int gettotalOrganisations();

	@Query("select COUNT(c.companyId) from CompanyMaster c where c.active=1 and DATEDIFF(now(),c.createdOn) <30")
	int getLastMonthActiveOrganisation();

	@Query("select COUNT(c.companyId) from CompanyMaster c where c.active=1 and DATEDIFF(now(),c.createdOn) between 30 and 60")
	int getSecondLastMonthActiveOrganisation();

	@Query("select COUNT(c.companyId) from CompanyMaster c where c.active=1 and DATEDIFF(now(),c.createdOn) >30")
	int getMoreThanLastMonthOrganisation();

	@Query("select COUNT(c.companyId) from CompanyMaster c where c.active=1")
	int getTotalOraganisation();

	@Query("select c.companyId,c.admin.userId from CompanyMaster c where c.admin.userId in(SELECT u.userId from User u where DATEDIFF(now(),u.lastLoginDate)>?1)"
			+ "union "
			+ "select c.companyId,c.admin_userId from CompanyMaster c, User u where"
			+ "c.admin_userId = u.createdBy_userId and DATEDIFF(now(),u.lastLoginDate)>?1 and u.role='8'")
	List<Object[]> getLastMonthInactiveOrganisation(int a);
	
	@Query("select count(c.companyId) from CompanyMaster c where " 
	+" c.admin.userId in(SELECT u.userId from User u where DATEDIFF(now(),u.lastLoginDate)>?1 "
	+" and (u.role='8' or u.role='3') and u.status=1)") 
	int getInctiveOrganisation(int a);
	
	@Query("select c.companyId,c.admin.userId from CompanyMaster c where c.admin.userId in(SELECT u.userId from User u where DATEDIFF(now(),u.lastLoginDate)<60)"
			+ "union "
			+ "select c.companyId,c.admin_userId from CompanyMaster c, User u where"
			+ "c.admin_userId = u.createdBy_userId and DATEDIFF(now(),u.lastLoginDate)<60 ")
	List<Object[]> getLastTwoMonthInactiveOrganisation();
	
	@Query("select ?1-count(distinct c.companyId),month(u.lastLoginDate) from CompanyMaster c, User_aud u where c.admin.userId=u.userId and"
			+ " year(u.lastLoginDate)=?2 and (u.role=8 or u.role=3) and c.active=1 GROUP BY month(u.lastLoginDate)")
	List<Object[]> getInactiveOrg(long t, int yr);

	@Query("select COUNT(c.companyId) from CompanyMaster c where c.active=1")
	long getTotalOraganisation1();

	@Query("select count(c.companyId) ,month(u.lastLoginDate) from CompanyMaster c, User u where c.admin.userId=u.createdBy.userId"
			+ " and year(u.lastLoginDate)=?1 and u.role='8' and c.active=1 GROUP BY month(u.lastLoginDate)")
	List<Object[]> getInactiveOrg1(int yr2);

	@Query("select count(c) from CompanyMaster c where c.flag=1")
	int getlastAddedOrganisations();

	@Query("select c.companyId,c.companyName,c.createdOn,c.createdBy.firstName,c.createdBy.lastName,c.typeOfEducation,c.flag from CompanyMaster c where DATEDIFF(now(),c.createdOn)<7")
	List<Object[]> getLastAddedOrganisation();

	@Query("select c from CompanyMaster c where c.flag=1")
	List<CompanyMaster> getInactiveFlagLastOrganisation();

	@Query("select distinct(c.companyId),c.admin.userId from CompanyMaster c, User u where c.admin.userId=u.createdBy.userId and DATEDIFF(now(),u.lastLoginDate)>?1 and u.role=8")
	List<Object[]> getLastMonthInactiveOrganisationByModulUser(int a);

	@Query("select c.companyName,c.admin.firstName,c.admin.mobile from CompanyMaster c where DATEDIFF(now(),c.admin.lastLoginDate)>30")
	List<Object[]> findInactiveOrgInfo();

	@Query("select count(c.companyId) as companyId,c.city.cityName as city_cityName from CompanyMaster c group by c.city.cityId order by count(c.companyId) desc")
	List<Object[]> topCities();
	
	@Query("select ?1-count(c.companyId) from CompanyMaster c, User u where c.admin.userId=u.userId and"
			+ " year(u.lastLoginDate)=?2 and c.active=1  and month(u.lastLoginDate)=?3")
	int getInactiveOrgListExcel(long totalOraganisation2, int yr2, int month);
	
	@Query("select COUNT(c.companyId) from CompanyMaster c where c.active=1 and year(c.createdOn)<=?1")
	int getTotalOrg(int yr);

	@Query("select COUNT(c.companyId) from CompanyMaster c where c.active=1 and date(c.createdOn)<=?1")
	long getTotalOrgUptomonth(Date d);

	@Query("select ?1-count(distinct c.companyId) from CompanyMaster c, User_aud u where c.admin.userId=u.userId and"
			+ " year(u.lastLoginDate)=?2 and (u.role=8 or u.role=3) and c.active=1 and month(u.lastLoginDate)=?3")
	long getInactiveOrgOfThatMonth(long totalOrgUptoMonth, int yr, int x);

	
}
