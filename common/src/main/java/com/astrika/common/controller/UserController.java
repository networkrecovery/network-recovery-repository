package com.astrika.common.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.astrika.common.exception.NoSuchUserException;
import com.astrika.common.model.User;
import com.astrika.common.service.UserService;

@Controller
@RequestMapping("/User/*")
public class UserController {

	@Autowired
	private UserService userServiceImpl;

	@RequestMapping("/")
	public String showHome(){
		return "home";
	}

	@RequestMapping("/User")
	public ModelAndView userView(){

		return new ModelAndView("/common/user/view","user",new User());
	}


	@RequestMapping(value="/User",method=RequestMethod.POST)
	public String saveCountry(@ModelAttribute("user") User user){
		userServiceImpl.save(user);
		return "/common/user/view";
	}

	@RequestMapping("/User/{userId}")
	@ResponseBody
	public User getUser(@PathVariable("userId") Long userId) throws NoSuchUserException {
		return userServiceImpl.findById(userId);
	}
}
