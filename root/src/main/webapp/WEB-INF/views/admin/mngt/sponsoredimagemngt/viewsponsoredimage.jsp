<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/template_start.jsp"%>
<%@include file="../../inc/page_head.jsp"%>


<div id="page-content">
	<div class="content-header">
		<div class="header-section">
			<c:if test="${!empty error}">
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-times-circle"></i> <spring:message code="label.error"/>
					</h4>
					<spring:message code="${error}" />
				</div>
			</c:if>

			<h1>
				<spring:message code="heading.sponsoredimagemaster" />
				<br> <small><spring:message
						code="heading.newimage" /></small>
			</h1>
			<span id="errorMsg"></span>
		</div>
	</div>
	<ul class="breadcrumb breadcrumb-top">
		<li><spring:message code="menu.sponsoredimage" /></li>
	</ul>
	<form action="<%=contexturl %>ManageSponsoredImage/SponsoredImageList/SaveSponsoredImage" method="post" class="form-horizontal ui-formwizard"
		id="Module_form" enctype="multipart/form-data">

		<div class="col-md-12">
				<div class="block">
					<div class="block-title">
						<h2>
							<strong><spring:message code="heading.sponsoredimageinfo" /></strong>
						</h2>
<%-- 						<input name="moduleId" value="${module.moduleId}" type="hidden"> --%>
					</div>
					<table>
					<tr>

			<c:forEach var="currentImage" items="${moduleList}">
				<div class="form-group" id="moduleDiv">

			<input type="hidden" name="moduleId" value="${currentImage.moduleId}" />
                  <div class="col-md-12" id="">
                      <div class="col-md-2">
                    <h5>
					<strong>${currentImage.moduleName}</strong>
					</h5>
					  </div>
				<div id="profilepic">
				<div class="col-md-4 text-center img-list2" id="imagediv">
                  <img border="0"
				   src="<%=contexturl%>resources/img/application/default_profile.png"
				   width="50" height="50" title="Click here to change photo"
				   onclick="OpenFileDialog1();" id="profileimage"
				   style="cursor: pointer;" /> <span class="text-content"
				   onclick="OpenFileDialog1();"
				   title="Click here to change photo"><br> <br>
				   <span><small>Upload Sponsored Image</small></span></span>
				   </div>
				   <input type="file" name="imageChooser1" id="imageChooser1"
					style="display: none;" name="imageChooser1"
					accept="image/jpg,image/jpeg,image/png,image/gif,image/bmp,image/tiff" />
				</div>
                  <label class="col-md-1 control-label" for="link_Name"><spring:message
				   code="label.linkname" /> <span class="text-danger">*</span></label>
				   <div class="col-md-4">
					<input id="link_Name" name="link" class="form-control"
					placeholder="<spring:message code='label.linkname'/>.."
					type="text" value="${currenytImage.link}">
					</div>
				</div>
			</div>
		</c:forEach>
		</table>
			</div>
		</div>

         



		<div style="text-align: center; margin-bottom: 10px">
			<div class="form-group form-actions">
<!-- 				<div class="col-md-9 col-md-offset-3"> -->
					<button id="module_submit" type="submit"
						class="btn btn-sm btn-primary save">
						<i class="fa fa-angle-right"></i> <spring:message code="button.save" />
					</button>
					<button type="reset" class="btn btn-sm btn-warning">
						<i class="fa fa-repeat"></i>
						<spring:message code="button.reset" />
					</button>
						<a class="btn btn-sm btn-primary" href="<%=contexturl %>ManageSponsoredImage/SponsoredImageList">
							<spring:message code="button.cancel" /> </a>

			</div>
		</div>
	</form>
</div>
<%@include file="../../inc/page_footer.jsp"%>
<%@include file="../../inc/template_scripts.jsp"%>
<%@include file="../../inc/chosen_scripts.jsp"%>
<script src="<%=contexturl %>resources/js/custom/featurecontrol.js"></script>
<script type="text/javascript"
	src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCheQF_yjKacwuVL4_59EO-3BThy85gbu4&sensor=false&region=GB">
	
</script>

<script type="text/javascript">
	$(document)
			.ready(
				function() {
					$("#manageSponsoredImage").addClass("active");
					$(".module_chosen").data("placeholder","Select Module From...").chosen();
						$("#Module_form").validate(
						{	errorClass:"help-block animation-slideDown",
							errorElement:"div",
							errorPlacement:function(e,a){a.parents(".form-group > div").append(e)},
							highlight:function(e){$(e).closest(".form-group").removeClass("has-success has-error").addClass("has-error")},
							success:function(e){e.closest(".form-group").removeClass("has-success has-error")},
										rules : {
											chosenModuleId: {
												required : !0
											},
											link: {
												required : !0,
												urlTrim :!0
											}
											
										},
										messages : {
											chosenModuleId: {
												required : '<spring:message code="validation.selectmodule"/>'
											},
											link : {
												required : '<spring:message code="validation.selectalink"/>',
												urlTrim : '<spring:message code="validation.pleaseenteravalidurl"/>'
											}
										}
								});

						$("#module_submit").click(function() {
							unsaved=false;
							$("#Module_form").submit();
						});
						
				});
						
</script>

<script type="text/javascript">
	function OpenFileDialog1(){
		document.getElementById("imageChooser1").click();
	}
	
	function changePhoto(){
		if($('#imageChooser1').val() != ""){			
			var imageName = document.getElementById("imageChooser1").value;
			var ext1 = imageName.substring(imageName.lastIndexOf("."));
			var ext = ext1.toLowerCase();
			if(ext == '.jpg' || ext == '.jpeg' || ext == '.png' || ext == 'gif' || ext == 'bmp' || ext == 'tiff')
				$('#changePhoto').submit();		
			else
				alert("Please select only image file");		
		}
	}
</script>


<script type="text/javascript">
    function showimages(files) {
        for (var i = 0, f; f = files[i]; i++) {
           
                       
            var reader = new FileReader();
            reader.onload = function (evt) {
            	$( "#image" ).remove();
            	$( "#imagediv" ).remove();
            	
                  var img ='<div class="col-md-2 text-center img-list2" id="imagediv"><img border="0" src="' + evt.target.result + '" width="50" height="50" title="Click here to change photo" onclick="OpenFileDialog1();"  id="image" style="cursor: pointer;" /><span class="text-content" onclick="OpenFileDialog1();" title="Click here to change photo"><br><br><span><small>Change Sponsored Image</small></span></span></div>';
                 $('#profilepic').append(img);
            };
            reader.readAsDataURL(f);
        }
    }

    $('#imageChooser1').change(function (evt) {
        showimages(evt.target.files);
    });
</script>

<style>
.chosen-container {
	width: 250px !important;
}
#map-canvas {
	height: 300px
}
</style>
<%@include file="../../inc/template_end.jsp"%>