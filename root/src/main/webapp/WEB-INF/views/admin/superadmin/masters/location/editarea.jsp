<%@page import="com.astrika.common.model.TimeZoneEnum"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@include file="../../../inc/config.jsp"%>
<%@include file="../../../inc/template_start.jsp"%>
<%@include file="../../../inc/page_head.jsp"%>



<style>

.chosen-container {
	width: 250px !important;
}
 #map-canvas { height: 300px;}
</style>
<div id="page-content">
<div class="content-header">
	<div class="header-section">
		<h1>
			<i class="fa fa-map-marker"></i>
			<spring:message code="heading.areamaster" />
			<br> <small><spring:message
					code="heading.specifylistofarea" /> </small>
		</h1>
		<span id="errorMsg"></span>
		<c:if test="${!empty success}">
			<div class="alert alert-success alert-dismissable">
				<button type="button" id="success_close" class="close" data-dismiss="alert"
					aria-hidden="true">x</button>
				<h4>
					<i class="fa fa-check-circle"></i> <spring:message code="label.success"/>
				</h4>
				<spring:message code="${success}" />
			</div>
		</c:if>
		<c:if test="${!empty error}">
			<div class="alert alert-danger alert-dismissable">
				<button type="button" class="close" data-dismiss="alert"
					aria-hidden="true">x</button>
				<h4>
					<i class="fa fa-times-circle"></i> <spring:message code="label.error"/>
				</h4>
				<spring:message code="${error}" />
			</div>
		</c:if>
	</div>
</div>
<ul class="breadcrumb breadcrumb-top">
	<li><spring:message code="menu.managelocations" /></li>
	<li><a href="#"><spring:message code="label.area" /></a></li>
</ul>
<div class="block full" id="formView">
	<div class="block-title">
		<h2>
			<strong><spring:message code="label.editarea" /></strong>
		</h2>
	</div>
	<form action="<%=contexturl %>ManageLocation/Area/UpdateArea" method="post" class="form-horizontal form-bordered"
		id="Area_form">
		<div class="form-group">
			<input id="area_Id" name="areaId" type="hidden" value="${area.areaId}"> <label
				class="col-md-3 control-label" for="area_Name"><spring:message
					code="label.areaname" /><span class="text-danger">*</span></label>
			<div class="col-md-9">
				<input id="area_Name" name="areaName" class="form-control"
					placeholder="<spring:message code="label.areaname"/>.."
					type="text" value="${area.areaName}">
			</div>
		</div>




						
					<div class="form-group" id="countryDiv">
						<label class="col-md-3 control-label" for="country"><spring:message
								code="label.countryname" /><span class="text-danger">*</span></label>
						<div class="col-md-6">
							<select class="country_chosen" style="width: 400px;"
								id="countryId"
								data-placeholder="<spring:message code='label.choosecountry' />"
								name="chosenCountryId">
								<c:forEach items="${countryList}" var="country">
									<option value="${country.countryId}"
										<c:if test="${country.countryId eq area.country.countryId}">selected="selected"</c:if>>${country.countryName}</option>
								</c:forEach>
							</select>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-md-3 control-label" for="city"><spring:message
								code="label.cityname" /><span class="text-danger">*</span></label>
						<div class="col-md-6">
							<select class="city_chosen" style="width: 400px;"
								id="cityId"
								data-placeholder="<spring:message code='label.choosecity' />"
								name="chosenCityId">
							</select>
						</div>
					</div>
         
         
		<div class="form-group">
			<label class="col-md-3 control-label" for="latitude"><spring:message
					code="label.latitude" /><span class="text-danger">*</span></label>
			<div class="col-md-9">
				<input id="latitude" name="latitude" class="form-control" readonly="readonly"
					placeholder="<spring:message code="label.latitude"/>.."
					type="text" value="${area.latitude}">
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label" for="country_Name"><spring:message
					code="label.longitude" /><span class="text-danger">*</span></label>
			<div class="col-md-9">
				<input id="longitude" name="longitude" class="form-control" readonly="readonly"
					placeholder="<spring:message code="label.longitude"/>.."
					type="text"  value="${area.longitude}">
			</div>
		</div>
		<div class="form-group">
				<li>Please Drag Pointer to Locate Area</li>
			    <div id="map-canvas"/>
		</div>

		<div class="form-group form-actions">
			<div class="col-md-9 col-md-offset-3">
				<div id="area_submit" class="btn btn-sm btn-primary save">
					<i class="fa fa-angle-right"></i> <spring:message code="button.save" />
				</div>
			
				<a class="btn btn-sm btn-primary" id="cancel" href="<%=contexturl %>ManageLocation/Area">
					<i class="gi gi-remove"></i>
					<spring:message code="button.cancel" />
				</a>
			</div>
		</div>
	</form>
</div>
</div>
<%@include file="../../../inc/page_footer.jsp"%>
<%@include file="../../../inc/template_scripts.jsp"%>

 <script type="text/javascript"
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCheQF_yjKacwuVL4_59EO-3BThy85gbu4&sensor=false&region=GB">
    </script>
    <script type="text/javascript">
      function initialize() {
		var latitude="19.018339908263073";
		var longitude="72.84382820129395";
		
		if($('#latitude').val()!="")
			latitude=$('#latitude').val();
			
		if($('#longitude').val()!="")
			longitude=$('#longitude').val();	
			
        var mapOptions = {
          center: new google.maps.LatLng(latitude,longitude),
          zoom: 13
        };
        var map = new google.maps.Map(document.getElementById("map-canvas"),
            mapOptions);
     
		var marker = new google.maps.Marker({
		position: map.getCenter(),
		map: map,
		title: 'Click to zoom',
		draggable: true
	  });

	  google.maps.event.addListener(map, 'center_changed', function() {
		// 3 seconds after the center of the map has changed, pan back to the
		// marker.
	
		window.setTimeout(function() {
				$('#latitude').val(marker.getPosition().k);
				$('#longitude').val(marker.getPosition().B);
				map.panTo(marker.getPosition());
		}, 1000);
	  });

	  google.maps.event.addListener(marker, 'click', function() {
		map.setZoom(13);
		map.setCenter(marker.getPosition());
	  });
	}

      google.maps.event.addDomListener(window, 'load', initialize);
    </script>
    
	
<script type="text/javascript">
	$(document).ready(function() {
		
		$(".country_chosen").data("placeholder","Select Country From...").chosen();
		
		$(".city_chosen").data("placeholder","Select City From...").chosen();
		
		$('select[name="chosenCountryId"]').chosen().change( function() {
			var countryId = ($(this).val());
			$.ajax({
				type: "GET",
				async: false,
				url: "<%=contexturl %>city/citybycountryid/"+countryId,
				success: function(data, textStatus, jqXHR){
					var obj = jQuery.parseJSON(data);
					var len=obj.length;
					var myOptions = "<option value></option>";
					for(var i=0; i<len; i++){
					myOptions += '<option value="'+obj[i].cityId+'">'+obj[i].cityName+'</option>';
					}
					$(".city_chosen").html(myOptions).chosen().trigger("chosen:updated");
				},
				dataType: 'html'
			});
		});
		
		changeCountry($("#countryId").val());
		
		function changeCountry(countryId){
			$.ajax({
				type: "GET",
				async: false,
				url: "<%=contexturl %>city/citybycountryid/"+countryId,
				success: function(data, textStatus, jqXHR){
					var obj = jQuery.parseJSON(data);
					var len=obj.length;
					var myOptions = "<option value></option>";
					for(var i=0; i<len; i++){
						if(${area.city.cityId} != obj[i].cityId){
							myOptions += '<option value="'+obj[i].cityId+'">'+obj[i].cityName+'</option>';
						}
						else{
							myOptions += '<option value="'+obj[i].cityId+'" selected="selected">'+obj[i].cityName+'</option>';
						}
					}
					$(".city_chosen").html(myOptions).chosen().trigger("chosen:updated");
				},
				dataType: 'html'
			});
		}
		
			$("#Area_form").validate(
			{	errorClass:"help-block animation-slideDown",
				errorElement:"div",
				errorPlacement:function(e,a){a.parents(".form-group > div").append(e)},
				highlight:function(e){$(e).closest(".form-group").removeClass("has-success has-error").addClass("has-error")},
				success:function(e){e.closest(".form-group").removeClass("has-success has-error")},
				rules:{ areaName:{required:!0,maxlength : 75},
					    chosenCityId:{required:!0},	
						chosenCountryId:{required:!0,maxlength : 75},
						latitude:{required:!0,number:!0},
						longitude:{required:!0,number:!0}
				},
				messages:{areaName:{required:'<spring:message code="validation.areaname"/>',
					maxlength :'<spring:message code="validation.area75character"/>'},
					chosenCityId:{required:'<spring:message code="validation.selectcity"/>'},	
					chosenCountryId:{required:'<spring:message code="validation.selectcountry"/>'},
					latitude:{required:'<spring:message code="validation.latitude"/>',number:'<spring:message code="validation.digits"/>'},
					longitude:{required:'<spring:message code="validation.longitude"/>',number:'<spring:message code="validation.digits"/>'}
				},
				
			});
			
			 $("#area_submit").click(function(){
				 $("#Area_form").submit();
			 });
	});
	
	
</script>
<%@include file="../../../inc/template_end.jsp"%>