package com.astrika.companymngt.model.module;



import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.envers.Audited;
import org.joda.time.DateTime;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import com.astrika.categorymngt.model.CategoryMaster;
import com.astrika.common.model.ImageMaster;
import com.astrika.common.model.User;



@Entity
@Audited
public class ModuleMaster {
	

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long moduleId;
    
    @Column(unique = true, nullable = false, length = 75)
    private String moduleName;
    
    
    @Column(nullable=false,length=500)
    private String moduleDescription;
    
    @JsonIgnore
    @CreatedBy
    @ManyToOne(fetch = FetchType.LAZY)
    private User createdBy;
    
    @Transient
    private Date createdDate;
    
    @CreatedDate
    private DateTime createdOn;
    
    
    @LastModifiedDate
    private DateTime lastModifiedOn;
    
    
    @Transient
    private CategoryMaster category;
    
    @Transient
    private boolean completed;

    @JsonIgnore@LastModifiedBy
    @ManyToOne(fetch = FetchType.LAZY)
    private User lastModifiedBy;
    
    @Transient
    private long categoryCount;
    

    @OneToOne
    private ImageMaster profileImage;
    
    @OneToOne(cascade=CascadeType.ALL)
    private ImageMaster sponsoredImage;
    
    @Column(length=500)
    private String link;
    
     @OneToOne(cascade=CascadeType.ALL)
    private ImageMaster promotionalImage;
    
    @Column(columnDefinition = "boolean default true", nullable = false)
    private boolean active = true;
    
    public ModuleMaster()
    {
        super();
    }
    
    public ModuleMaster(String moduleName)
    {
        super();
        this.moduleName=moduleName;
    }
    
    public ModuleMaster(String moduleName,String moduleDescription)
    {
    super();
    this.moduleName=moduleName;
    this.moduleDescription=moduleDescription;
    }
    
    @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((moduleId == null) ? 0 : moduleId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ModuleMaster other = (ModuleMaster) obj;
		if (moduleId == null) {
			if (other.moduleId != null)
				return false;
		} else if (!moduleId.equals(other.moduleId))
			return false;
		return true;
	}

	
	public ImageMaster getSponsoredImage() {
		return sponsoredImage;
	}

	public void setSponsoredImage(ImageMaster sponsoredImage) {
		this.sponsoredImage = sponsoredImage;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public Long getModuleId() {
		return moduleId;
	}

	public void setModuleId(Long moduleId) {
		this.moduleId = moduleId;
	}

	public String getModuleName() {
		return moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	public String getModuleDescription() {
		return moduleDescription;
	}

	public void setModuleDescription(String moduleDescription) {
		this.moduleDescription = moduleDescription;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}

	public DateTime getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(DateTime createdOn) {
		this.createdOn = createdOn;
	}

	public DateTime getLastModifiedOn() {
		return lastModifiedOn;
	}

	public void setLastModifiedOn(DateTime lastModifiedOn) {
		this.lastModifiedOn = lastModifiedOn;
	}

	public User getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(User modifiedBy) {
		this.lastModifiedBy = modifiedBy;
	}

	public long getCategoryCount() {
		return categoryCount;
	}

	public void setCategoryCount(long categoryCount) {
		this.categoryCount = categoryCount;
	}
	
	public CategoryMaster getCategory() {
		return category;
	}

	public void setCategory(CategoryMaster category) {
		this.category = category;
	}

	public ImageMaster getProfileImage() {
		return profileImage;
	}

	public void setProfileImage(ImageMaster profileImage) {
		this.profileImage = profileImage;
	}

	public Date getCreatedDate() {
		return createdOn.toDate();
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public boolean isCompleted() {
		return completed;
	}

	public void setCompleted(boolean completed) {
		this.completed = completed;
	}

	public ImageMaster getPromotionalImage() {
		return promotionalImage;
	}

	public void setPromotionalImage(ImageMaster promotionalImage) {
		this.promotionalImage = promotionalImage;
	}
	

}
