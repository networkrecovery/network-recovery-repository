
<%@include file="../inc/config.jsp"%>
<%@include file="../inc/template_start.jsp"%>
<%@include file="../inc/page_head.jsp"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<div id="page-content">
	<div class="content-header">
		<div class="header-section">
			<h1>
				Survey Result Data
			</h1>
			<c:if test="${!empty success}">
				<div class="alert alert-success alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-check-circle"></i>
						<spring:message code="label.success" />
					</h4>
					<spring:message code="${success}" />
				</div>
			</c:if>
			<div class="alert alert-danger alert-dismissable"
				style="display: none" id="error">
				<button type="button" class="close" data-dismiss="alert"
					aria-hidden="true">x</button>
				<h4>
					<i class="fa fa-times-circle"> </i> <spring:message code="label.error"/>
				</h4>
				<span id="errorMsg"></span>
			</div>
		</div>
	</div>
	<div class="block full  gridView">
		<div class="block-title">
			<h2>
				<strong>Result Data List</strong>
			</h2>
		</div>
		<a href="<%=contexturl %>ManageResultData/ResultDataList/AddData" id="addUniversity" class="btn btn-sm btn-primary save">Add Result Data</a>
		<!--<p><a href="https://datatables.net/" target="_blank">DataTables</a> is a plug-in for the Jquery Javascript library. It is a highly flexible tool, based upon the foundations of progressive enhancement, which will add advanced interaction controls to any HTML table. It is integrated with template's design and it offers many features such as on-the-fly filtering and variable length pagination.</p> -->
		<div class="table-responsive">
			<table id="active-datatable"
				class="table table-vcenter table-condensed table-bordered">
				<thead>
					<tr>
					<th class="text-center"><spring:message code="label.id" /></th>
						<th class="text-center"><spring:message code="label.modulename" /></th> 
						<th class="text-center">LifeStage</th> 
				        <th class="text-center"><spring:message code="label.action" /></th>
					</tr>
				</thead>
				<tbody id="tbody">
					<c:forEach var="resultSet" items="${activeResultList}"
						varStatus="count">

						<tr>
							<td class="text-center">${count.count}</td>
							<td class="text-left"><c:out value="${resultSet.module.moduleName}" /></td>
							 
						    <td class="text-left"><c:out value="${resultSet.lifeStages.name}" /></td>

							<td class="text-center">
								<div class="btn-group">
									<a href="<%=contexturl %>ManageResultData/ResultDataList/EditData?id=${resultSet.resultId}"
										data-toggle="tooltip" title="Edit"
										class="btn btn-xs btn-default save"><i class="fa fa-pencil"></i></a>
								</div>
							</td>
						</tr>
					</c:forEach>

				</tbody>
			</table>
		</div>
	</div>
</div>

<%@include file="../inc/page_footer.jsp"%>
<%@include file="../inc/template_scripts.jsp"%>
<%-- <%@include file="../inc/chosen_scripts.jsp"%> --%>
<script src="<%=contexturl %>resources/js/custom/featurecontrol.js"></script>
<script src="http://jqueryvalidation.org/files/dist/additional-methods.min.js"></script>
<script
	src="<%=contexturl%>resources/js/pages/activeTablesDatatables.js"></script>

<script>$(function(){ ActiveTablesDatatables.init(3); });</script>

<script type="text/javascript">
	var selectedId = 0;
	var restoreId=0;
	$(document).ready(function() {
		
		
		
		$("#manageModule").addClass("active");
		
        $("#restore_Company").click(function(){
			$("#restorecompanyId").val(restoreId);
			$("#restore_company_Form").submit();
		});
		
		
		$("#delete_company").click(function(){
			$("#deletecompanyId").val(selectedId);
			$("#delete_company_Form").submit();
		});
	});
	
	function deleteCompany(id){
		selectedId = id;
	}
	function restoreCompany(id){
		restoreId = id;
	}
</script>

<%@include file="../inc/template_end.jsp"%>