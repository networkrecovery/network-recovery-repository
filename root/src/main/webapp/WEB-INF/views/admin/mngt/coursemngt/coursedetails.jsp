<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/template_start.jsp"%>
<%@include file="../../inc/search_page_head.jsp"%>
<%-- <%@include file="../../inc/course_head.jsp"%> --%>
   
            <!-- Main Container -->
            <div id="main-container">                  


<style>

.facilities i{
padding-right: 10px;
}
.style-alt .block-title, .block-title{
background:none !important;
margin-left:0px;
text-transform: capitalize;
font-weight: bold;
font-size: 20px;
margin-top:25px;
}

.facilities label{
font-weight:normal;
}
.fa-2x {
font-size: 1.5em;
}
#map-canvas{ height: 100%; }
.style-alt .block-title, .block-title {
	background: none !important;
	margin-left: 13px;
	margin-right: 0px;
	text-transform: capitalize;
	font-weight: bold;
	font-size: 20px;
	margin-top: 25px;
}

@media (max-width:768px){
	.btn-restaurant-points{
		padding-left:0;
		padding-right:0;
	}
}
</style>


<div id="page-content">
	<div class="row" >
		<div class="col-sm-7" style="padding:0;" >
		<div class="row block" >
			<div class="col-sm-12 block-section text-center  animation-fadeIn" style="padding:0;">
<%-- 				<c:choose> --%>
<%-- 					<c:when test="${restaurant.profileImage!=null}"> --%>
<%-- 						<img src="<%=contexturl %>${restaurant.profileImage.imagePath}" class="img-responsive" alt="image">	 --%>
<%-- 					</c:when> --%>
<%-- 					<c:otherwise > --%>
<%-- 						<img src="<%=contexturl %>resources/img/placeholders/photos/photo3.jpg" class="img-responsive" alt="image">	 --%>
<%-- 					</c:otherwise> --%>
<%-- 				</c:choose> --%>

					<div class="col-xs-12" >
						<div class="widget-simple btn-success active" style="min-height: 20px;">						
								<div class="col-xs-6 text-left" > <b>Course Name : </b>${course.courseName } </div> 
								
                        </div>
					</div>
					<div class="col-xs-12" >
						<div class="widget-simple btn-success active" style="min-height: 20px;">						
								<div class="col-xs-6 text-left" > <b>About Course : </b>${course.aboutCourse } </div> 
								
                        </div>
					</div>
					<div class="col-xs-12" >
						<div class="widget-simple btn-success active" style="min-height: 20px;">						
								<div class="col-xs-6 text-left" > <b>Why This : </b>${course.whyThis } </div> 
								
                        </div>
					</div>
					<div class="col-xs-12" >
						<div class="widget-simple btn-success active" style="min-height: 20px;">						
								<div class="col-xs-6 text-left" > <b>Syllabus : </b>${course.syllabus } </div> 
								
                        </div>
					</div>
					<div class="col-xs-12" >
						<div class="widget-simple btn-success active" style="min-height: 20px;">						
								<div class="col-xs-6 text-left" > <b>Course Objectives : </b>${course.objective } </div> 
								
                        </div>
					</div>
			</div>
			
		
		</div>	
		</div>
		<div class="col-sm-5" style="padding:10px;" >
		
		<div class="row" >
			<div class="col-sm-6 btn-restaurant-points" >
				<div class="row" >
					<div class="col-xs-12" >
						<div class="widget-simple btn-success active" style="min-height: 80px;">						
								<div class="col-xs-6 text-left" > University Name </div> 
								
                        </div>
					</div>

					<div class="col-xs-12">
						<div class="widget-simple btn-danger active"
							style="min-height: 60px;">
							<div class="col-xs-6 text-left" style="height: 100%; margin-top: 20px" >
								Instructors
							</div>
							
						</div>
					</div>
					<div class="col-xs-12" >
						<div class="widget-simple btn-warning active" style="min-height: 60px;"> 	
							<div class="col-xs-6 text-left" style="height: 100%;margin-top: 20px">
								Course Category
							</div>
							
						</div>
					</div>
					<div class="col-xs-12" style="padding: 0px;">
						<div class="widget-simple"  style="min-height: 80px;">
							<button type="button"  class="col-xs-12 btn-lg themed-background-dark">
										<h4 class="widget-content-light text-center">
                                            <a href="../SearchResult" class="themed-color" >Browse More Courses</a>
                                        </h4>
							</button>                                 			
                        </div>
					</div>
				</div>
				<div class="row">
				</div>
				<div class="row">
				</div>
				<div class="row">
				</div>				
			</div>	
			<div class="col-sm-6" style="background-color:#4ac6e0;padding-top:10px;padding-left: 0;padding-right: 0;">
				<div class="row" style="padding-bottom:10px;">
					<div class="col-xs-12">
						<div class="col-xs-2">
							<i class="gi gi-phone_alt fa-2x" ></i>
						</div>							
						<div class="col-xs-10"> video 
							
				</div>
				<div sclass="row" style="height: 200px;width: 100%;float:left;margin-left: 0px;">
					<div id="map-canvas"></div>
				</div>
			</div>
		</div>
		
		
		<div class="row" >
			<div class="col-sm-6" >
							
			</div>	
			<div class="col-sm-6" >
			
					
			</div>
		</div>
		<div class="row" >
			<div class="col-sm-6" >
							
			</div>	
			<div class="col-sm-6" >
			
					
			</div>
		</div>
		
		</div>
	</div>
</div>

<%@include file="../../inc/page_footer.jsp"%>
<%@include file="../../inc/template_scripts.jsp"%>
<script type="text/javascript"
	src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCheQF_yjKacwuVL4_59EO-3BThy85gbu4&sensor=false&region=GB" >
</script>

<script type="text/javascript">

	function openCloseFacility() {
	    var element = document.getElementById("viewdetaillabel");
	    if (element.innerHTML === 'View More..'){
	    	element.innerHTML = 'Hide';
	    }
	    else {
	        element.innerHTML = 'View More..';
	    }
	}

	function initialize() {
		var mapOptions = {
			center : new google.maps.LatLng(${restaurant.latitude} , ${restaurant.longitude} ),
			zoom : 13
		};
		var map = new google.maps.Map(document.getElementById("map-canvas"),
				mapOptions);

		var marker = new google.maps.Marker({
			position : map.getCenter(),
			map : map,
			title : 'Click to zoom'
		});
	}

	google.maps.event.addDomListener(window, 'load', initialize);
	
</script>
<%-- <%@include file="../inc/restaurant_footer.jsp"%> --%>
<%@include file="../../inc/template_end.jsp"%>
