<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/template_start.jsp"%>
<%@include file="../../inc/page_head.jsp"%>

<style>

.rest_photo {
    width: 170px;
    text-align: left;
    font: 11px 'Calibri';
    color: #666;
    border: 10px solid #E6E6E6;;
    float: left;
    margin: 5px;
}
 #map-canvas { height: 300px}
</style>

<div id="page-content">
	<div class="content-header">
		<div class="header-section">
			<h1>
				<i class="fa fa-map-marker"></i>
				<spring:message code="heading.restaurant" />
				<br> <small><spring:message
						code="heading.restaurantdetails" /></small>
			</h1>
			<span id="errorMsg"></span>
			<c:if test="${!empty success}">
				<div class="alert alert-success alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-check-circle"></i> <spring:message code="label.success"/>
					</h4>
					<spring:message code="${success}" />
				</div>
			</c:if>
			
			<c:if test="${!empty error}">
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-times-circle"></i> <spring:message code="label.error"/>
					</h4>
					<spring:message code="${error}" /><c:if test="${!empty lockedBy }">${lockedBy}</c:if>
				</div>
			</c:if>
			
		</div>
	</div>
	<ul class="breadcrumb breadcrumb-top">
		<li>You can change your details here</li>
		<li><a href="EditRestaurant?id=${restaurant.outletId}" id="restaurant"><spring:message code="label.editrestaurant"/></a></li>
	</ul>
	<form action="UpdateRestaurant" method="post" class="form-horizontal"
		id="Restaurant_form" enctype="multipart/form-data">
		<div style="display: block;" id="advanced-first"
			class="step ui-formwizard-content">
			<div class="row">

				<div class="col-md-6">
					<div class="block">
						<div class="block-title">
							<h2>
								<strong><spring:message code="heading.restaurantinfo" /></strong>
							</h2>
						</div>
						<div class="form-group">
							<input id="outlet_Id" name="outletId" type="hidden" value="${restaurant.outletId}"> 
							<input id="outlet_Id" name="brandId" type="hidden" value="${restaurant.brand.brandId}"> 
							<label class="col-md-4 control-label" for="company_name"><spring:message
									code="label.companyname" /><span class="text-danger">*</span></label>
							<div class="col-md-6">
								<input id="company_Name" name="companyname" class="form-control" value="${restaurant.brand.company.companyName}"
									 disabled="disabled" type="text">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label" for="brand_name"><spring:message
									code="label.brandname" /><span class="text-danger">*</span></label>
							<div class="col-md-6">
								<input id="brand_Name" name="brandname" class="form-control" value="${restaurant.brand.brandName}"
									 disabled="disabled" type="text">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label" for="restaurant_Name"><spring:message
									code="label.restaurantname" /> <span class="text-danger">*</span>
							</label>
							<div class="col-md-6">

								<input id="restaurant_Name" name="name" class="form-control" value="${restaurant.name}"
									placeholder="<spring:message code='label.restaurantname'/>.." disabled="disabled"
									type="text">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label" for="currency"><spring:message
									code="label.currency" /><span class="text-danger">*</span> </label>
							<div class="col-md-6">
								<input id="currency_name" name="currency" class="form-control" value="${restaurant.baseCurrency.currencyName}"
									 disabled="disabled" type="text">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label" for="company_name"><spring:message
									code="label.cuisine" /><span class="text-danger">*</span></label>
							<div class="col-md-6">
								<textarea id="cuisine" name="cuisine" class="form-control" disabled="disabled">${restaurant.specialty}</textarea>
							</div>
						</div>
						
						<c:choose>
							<c:when test="${restaurant.logoImage!=null}">
								<div class="form-group">
									<label class="col-md-4 control-label" for="example-daterange1"><spring:message code="label.logo"/>
										</label>
									<div class="col-md-6">
										<img src="<%=contexturl %>${restaurant.logoImage.imagePath}" width="150px"/>
									</div>
								</div>
							</c:when>
							<c:otherwise >
								<div class="form-group">
								</div>	
							</c:otherwise>
						</c:choose>
						
						
						<c:choose>
							<c:when test="${restaurant.profileImage!=null}">
								<div class="form-group">
									<label class="col-md-4 control-label" for="example-daterange1"><spring:message code="label.profile"/>
										</label>
									<div class="col-md-6">
										<img src="<%=contexturl %>${restaurant.profileImage.imagePath}" width="150px"/>
									</div>
								</div>
							</c:when>
							<c:otherwise >
								<div class="form-group">
								</div>	
							</c:otherwise>
						</c:choose>
						
					</div>

					<div class="block">
						<div class="block-title">
							<h2>
								<strong><spring:message code="heading.contactinfo" /></strong>
							</h2>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label" for="telephone1"><spring:message
									code="label.telephone1" /></label>
							<div class="col-md-6">

								<input id="telephone1" name="telephone1" class="form-control" value="${restaurant.telephone1}"
									placeholder="<spring:message code='label.telephone1'/>.." disabled="disabled"
									type="text">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label" for="telephone2"><spring:message
									code="label.telephone2" /></label>
							<div class="col-md-6">

								<input id="telephone2" name="telephone2" class="form-control" value="${restaurant.telephone2}"
									placeholder="<spring:message code='label.telephone2'/>.." disabled="disabled"
									type="text">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label" for="rest_email"><spring:message
									code="label.restaurantemail" /></label>
							<div class="col-md-6">

								<input id="rest_email" name="restaurantEmail"
									class="form-control" value="${restaurant.emailId}"
									placeholder="<spring:message code="label.restaurantemail"/>.." disabled="disabled"
									type="text">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label" for="mobile1"><spring:message
									code="label.mobile1" /></label>
							<div class="col-md-6">

								<input id="mobile1" name="mobile1" class="form-control" value="${restaurant.mobile1}"
									placeholder="<spring:message code="label.mobile1"/>.." disabled="disabled"
									type="text">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label" for="mobile2"><spring:message
									code="label.mobile2" /></label>
							<div class="col-md-6">

								<input id="mobile2" name="mobile2" class="form-control" value="${restaurant.mobile2}"
									placeholder="<spring:message code="label.mobile2"/>.." disabled="disabled"
									type="text">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label" for="addressline1"><spring:message
									code="label.addressline1" /><span class="text-danger">*</span>
							</label>
							<div class="col-md-6">

								<input id="addressline1" name="addressLine1" disabled="disabled"
									class="form-control" value="${restaurant.addressLine1}"
									placeholder="<spring:message code="label.addressline1"/>.."
									type="text">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label" for="addressline2"><spring:message
									code="label.addressline2" /></label>
							<div class="col-md-6">

								<input id="addressline2" name="addressLine2" disabled="disabled"
									class="form-control" value="${restaurant.addressLine2}"
									placeholder="<spring:message code="label.addressline2"/>.."
									type="text">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label" for="area"><spring:message
									code="label.areaname" /><span class="text-danger">*</span></label>
									
							<div class="col-md-6">
								<input id="area" name="area" disabled="disabled"
									class="form-control" value="${restaurant.area.areaName}"
									placeholder="<spring:message code="label.areaname"/>.."
									type="text">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label" for="city"><spring:message
									code="label.cityname" /><span class="text-danger">*</span></label>
							<div class="col-md-6">
								<input id="city" name="city" disabled="disabled"
									class="form-control" value="${restaurant.city.cityName}"
									placeholder="<spring:message code="label.cityname"/>.."
									type="text">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label" for="country"><spring:message
									code="label.countryname" /><span class="text-danger">*</span></label>
							<div class="col-md-6">
								<input id="country" name="country" disabled="disabled"
									class="form-control" value="${restaurant.country.countryName}"
									placeholder="<spring:message code="label.countryname"/>.."
									type="text">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label" for="pincode"><spring:message
									code="label.pincode" /><span class="text-danger">*</span></label>
							<div class="col-md-6">

								<input id="pincode" name="pincode" class="form-control" value="${restaurant.pincode}"
									placeholder="<spring:message code="label.pincode"/>.." disabled="disabled"
									type="text">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label" for="latitude"><spring:message
									code="label.latitude" /><span class="text-danger">*</span></label>
							<div class="col-md-6">

								<input id="latitude" name="latitude" class="form-control" value="${restaurant.latitude}"
									placeholder="<spring:message code="label.latitude"/>.." disabled="disabled"
									type="text">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label" for="longitude"><spring:message
									code="label.longitude" /><span class="text-danger">*</span></label>
							<div class="col-md-6">

								<input id="longitude" name="longitude" class="form-control" value="${restaurant.longitude}"
									placeholder="<spring:message code="label.longitude"/>.." disabled="disabled"
									type="text">
							</div>
						</div>
					</div>
					<div class="block">
						<div class="block-title">
							<h2><strong><spring:message code="label.map"/></strong></h2>
						</div>
      
						  <div id="map-canvas"></div>
					</div>
					<div class="block">
						<div class="block-title">
							<h2>
								<strong><spring:message code="heading.socialinfo" /></strong>
							</h2>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label" for="facebook_link"><spring:message
									code="label.facebook" /> </label>
							<div class="col-md-6">
								<input id="facebook_link" name="facebookLink" disabled="disabled"
									class="form-control" value="${restaurant.facebooklink}"
									placeholder="<spring:message code='label.facebook'/>.."
									type="text">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label" for="twitter_link"><spring:message
									code="label.twitter" /> </label>
							<div class="col-md-6">
								<input id="twitter_link" name="twitterLink" class="form-control" value="${restaurant.twitterlink}"
									placeholder="<spring:message code='label.twitter'/>.." disabled="disabled"
									type="text">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label" for="keyword"><spring:message
									code="label.keyword" /> </label>
							<div class="col-md-6">
								<input id="keyword" name="keyword" class="form-control" value="${restaurant.keyword}"
									placeholder="<spring:message code='label.keyword'/>.." disabled="disabled"
									type="text">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label" for="metatag_link"><spring:message
									code="label.metatag" /> </label>
							<div class="col-md-6">
								<input id="metatag_link" name="metatag" class="form-control" value="${restaurant.metatag}"
									placeholder="<spring:message code='label.metatag'/>.." disabled="disabled"
									type="text">
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6">
						<div class="block">
					<div class="block-title">
						<h2>
							<strong><spring:message code="heading.admininfo"/></strong>
						</h2>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label" for="first_Name"><spring:message code="label.firstname"/><span class="text-danger">*</span>
						</label>
						<div class="col-md-6">
								<input id="restAdmin_id" name="restAdminId" type="hidden" value="${restaurant.admin.userId}">
								<input id="first_Name" name="firstName" class="form-control" disabled="disabled"
									placeholder="<spring:message code='label.firstname'/>.." type="text" value="${restaurant.admin.firstName}"> 
						</div>
					</div>
	
					<div class="form-group">
						<label class="col-md-4 control-label" for="last_Name"><spring:message code="label.lastname"/><span class="text-danger">*</span>
						</label>
						<div class="col-md-6">
							
								<input id="last_Name" name="lastName" class="form-control" disabled="disabled"
									placeholder="<spring:message code="label.lastname"/>.." type="text"  value="${restaurant.admin.lastName}">
						</div>
					</div>
	
	
					<div class="form-group">
						<label class="col-md-4 control-label" for="email"><spring:message code="label.emailid"/><span
							class="text-danger">*</span></label>
						<div class="col-md-6">
								<input id="email" name="emailId" class="form-control" disabled="disabled"
									placeholder="test@example.com" type="text"  value="${restaurant.admin.emailId}">
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-md-4 control-label" for="mobile"><spring:message code="label.mobile"/><span
							class="text-danger">*</span></label>
						<div class="col-md-6">
								<input id="mobile" name="mobile" class="form-control" disabled="disabled"
									placeholder='<spring:message code="label.mobile"></spring:message>' type="text"  value="${restaurant.admin.mobile}">
						</div>
					</div>
					<div class="form-group">
                        <label class="col-md-4 control-label"><spring:message code="label.userlogin"/></label>
                        <div class="col-md-6">
                            <input id="restAdmin_Name" name="restadminname" class="form-control" value="${restaurant.admin.loginId}"
									 disabled="disabled" type="text">
                        </div>
                    </div>
				</div>
					

					<div class="block full" id="showImgDiv">
						<div class="block-title">
							<div class="block-options pull-right"></div>
							<h2>
								<strong><spring:message code="label.restaurantimages"/>
								</strong>
							</h2>
						</div>
						<div style="min-height: 350px; height: auto; overflow:hidden;border: 2px dashed #eaedf1; background-color: #f9fafc; padding: 1em;">
							<c:forEach items="${restaurant.restaurantImage}" var="image">
								<div class="rest_photo">
									<img border="0" src="<%=contexturl %>${image.image.imagePath}" width="150"
									height="110">
								</div>
							</c:forEach>
						</div>
					</div>
					
					<div class="block">
						<div class="block-title">
							<h2>
								<strong><spring:message code="heading.additionalinfo" /></strong>
							</h2>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label" for="timing"><spring:message
									code="label.timing" /> </label>
							<div class="col-md-6">
								<input id="timing" name="timing" class="form-control" value="${restaurant.timings}"
									placeholder="<spring:message code='label.timing'/>.." disabled="disabled"
									type="text">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label" for="capacity"><spring:message
									code="label.capacity" /> </label>
							<div class="col-md-6">
								<input id="capacity" name="capacity" class="form-control" value="${restaurant.capacity}"
									placeholder="<spring:message code='label.capacity'/>.." disabled="disabled"
									type="text">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label" for="avgCostofTwo"><spring:message
									code="label.avgcostoftwo" /> </label>
							<div class="col-md-6">
								<input id="avgCostofTwo" name="averageCostOfTwo"
									class="form-control" value="${restaurant.averageCostOfTwo}" disabled="disabled"
									placeholder="<spring:message code='label.avgcostoftwo'/>.."
									type="text">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label" for="avgNoOfVisitor"><spring:message
									code="label.avgnoofvisitor" /> </label>
							<div class="col-md-6">
								<input id="avgNoOfVisitor" name="averageNoOfVisitor" disabled="disabled"
									class="form-control" value="${restaurant.averageNoOfVisitor}"
									placeholder="<spring:message code='label.avgnoofvisitor'/>.."
									type="text">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label" for="bestdishes"><spring:message
									code="label.bestdishes" /> </label>
							<div class="col-md-6">
								<input id="bestdishes" name="bestdishes" class="form-control" disabled="disabled"
									placeholder="<spring:message code='label.bestdishes'/>.." value="${restaurant.bestdishes}"
									type="text">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label" for="story"><spring:message
									code="label.story" /> </label>
							<div class="col-md-6">
								<textarea id="story" name="story" class="form-control" disabled="disabled">${restaurant.story}</textarea>
							</div>
						</div>
					</div>
					<div class="block">
						<%@include file="../featurecontrol/featurecontrol.jsp"%>
					</div>
				</div>

			</div>

			<div class="block">
				<div class="block-title">
					<h2>
						<strong><spring:message code="heading.facilities" /></strong>
					</h2>
				</div>
				<div class="form-group">
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="ac"><spring:message
								code="label.ac" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="ac" disabled="disabled"
								name="ac" type="checkbox" <c:if test="${restaurant.ac}"> checked </c:if> /> <span></span></label>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="creditCard"><spring:message
								code="label.acceptscreditcard" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input <c:if test="${restaurant.acceptsCreditCard}"> checked </c:if>
								id="creditCard" name="acceptsCreditCard" type="checkbox" 
								disabled="disabled"/> <span></span></label>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="wifi"><spring:message
								code="label.wifi" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="wifi" <c:if test="${restaurant.wifi}"> checked </c:if>
								name="wifi" type="checkbox" disabled="disabled"/> <span></span></label>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="gourmet7_Reach"><spring:message
								code="label.homedelivery" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input <c:if test="${restaurant.homeDelivery}"> checked </c:if>
								id="homedelivery" name="homeDelivery" type="checkbox" disabled="disabled"/> <span></span></label>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="nonvage"><spring:message
								code="label.nonvage" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="nonvage" <c:if test="${restaurant.nonvegeterian}"> checked </c:if>
								name="nonvegeterian" type="checkbox" disabled="disabled"/> <span></span></label>
						</div>
					</div>

					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="parking"><spring:message
								code="label.parking" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="parking" <c:if test="${restaurant.parking}"> checked </c:if>
								name="parking" type="checkbox" disabled="disabled"/> <span></span></label>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="toilet"><spring:message
								code="label.toilet" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="toilet" <c:if test="${restaurant.toilet}"> checked </c:if>
								name="toilet" type="checkbox" disabled="disabled"/> <span></span></label>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="servesalcohol"><spring:message
								code="label.servesalcohol" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input <c:if test="${restaurant.servesAlcohol}"> checked </c:if>
								id="servesalcohol" name="servesAlcohol" type="checkbox" disabled="disabled"/> <span></span></label>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="smoking"><spring:message
								code="label.smoking" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="smoking" <c:if test="${restaurant.smokingaAllowed}"> checked </c:if>
								name="smokingaAllowed" type="checkbox" disabled="disabled"/> <span></span></label>
						</div>
					</div>

					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="music"><spring:message
								code="label.music" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="music" <c:if test="${restaurant.music}"> checked </c:if>
								name="music" type="checkbox" disabled="disabled"/> <span></span></label>
						</div>
					</div>

					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="happyHours"><spring:message
								code="label.happyhours" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input <c:if test="${restaurant.happyHours}"> checked </c:if>
								id="happyHours" name="happyHours" type="checkbox" disabled="disabled"/> <span></span></label>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="takeaway"><spring:message
								code="label.takeaway" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input <c:if test="${restaurant.takeAway}"> checked </c:if>
								id="takeaway" name="takeAway" type="checkbox" disabled="disabled"/> <span></span></label>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="snacks"><spring:message
								code="label.snacks" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="snacks" <c:if test="${restaurant.snacks}"> checked </c:if>
								name="snacks" type="checkbox" disabled="disabled"/> <span></span></label>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="dinein"><spring:message
								code="label.dinein" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="dinein" <c:if test="${restaurant.dinein}"> checked </c:if>
								name="dinein" type="checkbox" disabled="disabled"/> <span></span></label>
						</div>
					</div>
					
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="outdoorSeating"><spring:message
								code="label.outdoorSeating" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="outdoorSeating"
								<c:if test="${restaurant.outdoorSeating}">checked</c:if> name="outdoorSeating"
								type="checkbox" disabled="disabled"/> <span></span></label>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="tableReservation"><spring:message
								code="label.tableReservation" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="tableReservation"
								<c:if test="${restaurant.tableReservation}">checked</c:if> name="tableReservation"
								type="checkbox" disabled="disabled"/> <span></span></label>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="barArea"><spring:message
								code="label.barArea" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="barArea"
								<c:if test="${restaurant.barArea}">checked</c:if> name="barArea"
								type="checkbox" disabled="disabled"/> <span></span></label>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="corporateEvents"><spring:message
								code="label.corporateEvents" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="corporateEvents"
								<c:if test="${restaurant.corporateEvents}">checked</c:if> name="corporateEvents"
								type="checkbox" disabled="disabled"/> <span></span></label>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="liveSportsBroadcast"><spring:message
								code="label.liveSportsBroadcast" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="liveSportsBroadcast"
								<c:if test="${restaurant.liveSportsBroadcast}">checked</c:if> name="liveSportsBroadcast"
								type="checkbox" disabled="disabled"/> <span></span></label>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="dressCode"><spring:message
								code="label.dressCode" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="dressCode"
								<c:if test="${restaurant.dressCode}">checked</c:if> name="dressCode"
								type="checkbox" disabled="disabled"/> <span></span></label>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="open24_7"><spring:message
								code="label.open24_7" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="open24_7"
								<c:if test="${restaurant.open24_7}">checked</c:if> name="open24_7"
								type="checkbox" disabled="disabled"/> <span></span></label>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="privatePartyArea"><spring:message
								code="label.privatePartyArea" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="privatePartyArea"
								<c:if test="${restaurant.privatePartyArea}">checked</c:if> name="privatePartyArea"
								type="checkbox" disabled="disabled"/> <span></span></label>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="sundayBrunch"><spring:message
								code="label.sundayBrunch" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="sundayBrunch"
								<c:if test="${restaurant.sundayBrunch}">checked</c:if> name="sundayBrunch"
								type="checkbox" disabled="disabled"/> <span></span></label>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="selfServices"><spring:message
								code="label.selfServices" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="selfServices"
								<c:if test="${restaurant.selfServices}">checked</c:if> name="selfServices"
								type="checkbox" disabled="disabled"/> <span></span></label>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="petFriendly"><spring:message
								code="label.petFriendly" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="petFriendly"
								<c:if test="${restaurant.petFriendly}">checked</c:if> name="petFriendly"
								type="checkbox" disabled="disabled"/> <span></span></label>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="jainFood"><spring:message
								code="label.jainFood" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="jainFood"
								<c:if test="${restaurant.jainFood}">checked</c:if> name="jainFood"
								type="checkbox" disabled="disabled"/> <span></span></label>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="buffetLunch"><spring:message
								code="label.buffetLunch" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="buffetLunch"
								<c:if test="${restaurant.buffetLunch}">checked</c:if> name="buffetLunch"
								type="checkbox" disabled="disabled"/> <span></span></label>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="buffetDinner"><spring:message
								code="label.buffetDinner" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="buffetDinner"
								<c:if test="${restaurant.buffetDinner}">checked</c:if> name="buffetDinner"
								type="checkbox" disabled="disabled"/> <span></span></label>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="banquet"><spring:message
								code="label.banquet" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="banquet"
								<c:if test="${restaurant.banquet}">checked</c:if> name="banquet"
								type="checkbox" disabled="disabled"/> <span></span></label>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="midnightBuffet"><spring:message
								code="label.midnightBuffet" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="midnightBuffet"
								<c:if test="${restaurant.midnightBuffet}">checked</c:if> name="midnightBuffet"
								type="checkbox" disabled="disabled"/> <span></span></label>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="partyOrders"><spring:message
								code="label.partyOrders" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="partyOrders"
								<c:if test="${restaurant.partyOrders}">checked</c:if> name="partyOrders"
								type="checkbox" disabled="disabled"/> <span></span></label>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="disablFacility"><spring:message
								code="label.disablFacility" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="disablFacility"
								<c:if test="${restaurant.disablFacility}">checked</c:if> name="disablFacility"
								type="checkbox" disabled="disabled"/> <span></span></label>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="theme"><spring:message
								code="label.theme" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="theme"
								<c:if test="${restaurant.theme}">checked</c:if> name="theme"
								type="checkbox" disabled="disabled"/> <span></span></label>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="inMall"><spring:message
								code="label.inMall" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="inMall"
								<c:if test="${restaurant.inMall}">checked</c:if> name="inMall"
								type="checkbox" disabled="disabled"/> <span></span></label>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="englishSpeakingWaiters"><spring:message
								code="label.englishSpeakingWaiters" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="englishSpeakingWaiters"
								<c:if test="${restaurant.englishSpeakingWaiters}">checked</c:if> name="englishSpeakingWaiters"
								type="checkbox" disabled="disabled"/> <span></span></label>
						</div>
					</div>
					
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="couponsAccepted"><spring:message
								code="label.couponsAccepted" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="couponsAccepted"
								<c:if test="${restaurant.couponsAccepted}">checked</c:if> name="couponsAccepted"
								type="checkbox" disabled="disabled"/> <span></span></label>
						</div>
					</div>
					
					<div class="col-sm-4">
						<label class="col-md-6 control-label" for="liveFoodCounter"><spring:message
								code="label.liveFoodCounter" /></label>
						<div class="col-md-4">
							<label class="switch switch-primary"> <input id="liveFoodCounter"
								<c:if test="${restaurant.liveFoodCounter}">checked</c:if> name="liveFoodCounter"
								type="checkbox" disabled="disabled"/> <span></span></label>
						</div>
					</div>
					
				</div>

			</div>
		</div>

	</form>
</div>

<%@include file="../../inc/page_footer.jsp"%>
<%@include file="../../inc/template_scripts.jsp"%>
<script src="<%=contexturl %>resources/js/custom/featurecontrol.js"></script>
<script type="text/javascript"
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCheQF_yjKacwuVL4_59EO-3BThy85gbu4&sensor=false&region=GB">
</script>
<script type="text/javascript">
      function initialize() {
        var mapOptions = {
          center: new google.maps.LatLng($('#latitude').val(), $('#longitude').val()),
          zoom: 13
        };
        var map = new google.maps.Map(document.getElementById("map-canvas"),
            mapOptions);
     
		var marker = new google.maps.Marker({
		position: map.getCenter(),
		map: map,
		title: 'Click to zoom',
		draggable: true
	  });

	  google.maps.event.addListener(map, 'center_changed', function() {
		// 3 seconds after the center of the map has changed, pan back to the
		// marker.
	
		window.setTimeout(function() {
				$('#latitude').val(marker.getPosition().k);
				$('#longitude').val(marker.getPosition().B);
				map.panTo(marker.getPosition());
		}, 1000);
	  });

	  google.maps.event.addListener(marker, 'click', function() {
		map.setZoom(13);
		map.setCenter(marker.getPosition());
	  });
	}

      google.maps.event.addDomListener(window, 'load', initialize);
</script>
    
<%@include file="../../inc/template_end.jsp"%>