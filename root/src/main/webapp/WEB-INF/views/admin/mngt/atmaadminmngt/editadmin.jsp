<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/template_start.jsp"%>
<%@include file="../../inc/page_head.jsp"%>
<div id="page-content">
	<div class="content-header">
		<div class="header-section">
			<h1>
<!-- 				<i class="fa fa-map-marker"></i> -->
				<spring:message code="heading.admin" />
				<br> <small><spring:message
						code="heading.admindetails" /></small>
			</h1>
			<span id="errorMsg"></span>
			<c:if test="${!empty success}">
				<div class="alert alert-success alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-check-circle"></i> <spring:message code="label.success"/>
					</h4>
					<spring:message code="${success}" />
				</div>
			</c:if>

			<c:if test="${!empty error}">
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">�</button>
					<h4>
						<i class="fa fa-times-circle"></i> <spring:message code="label.error"/>
					</h4>
					<spring:message code="${error}" />
				</div>
			</c:if>
		</div>
	</div>
	<ul class="breadcrumb breadcrumb-top">
		<li><spring:message code="heading.admin" /></li>
	</ul>
			
			<div class="block full" >
					<div class="block-title">
						<h2>
							<strong><spring:message code="heading.admininfo" /></strong>
						</h2>
					</div>
					<form action="<%=contexturl %>ManageAdmin/AdminList/UpdateAdmin" method="post" class="form-horizontal "
						id="User_form" >
							<input name="userId" value="${user.userId}"
								type="hidden">
					<div class="form-group">
						<label class="col-md-4 control-label" for="first_Name"><spring:message
								code="label.firstname" /><span class="text-danger">*</span> </label>
						<div class="col-md-6">

							<input id="first_Name" name="firstName" class="form-control"
								placeholder="<spring:message code='label.firstname'/>.."
								type="text" value="${user.firstName}">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label" for="last_Name"><spring:message
								code="label.lastname" /><span class="text-danger">*</span> </label>
						<div class="col-md-6">

							<input id="last_Name" name="lastName" class="form-control"
								placeholder="<spring:message code="label.lastname"/>.."
								type="text" value="${user.lastName}">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label" for="email"><spring:message
								code="label.emailid" /><span class="text-danger">*</span></label>
						<div class="col-md-6">
							<input id="email" name="emailId" class="form-control"
								placeholder="test@example.com" type="text" value="${user.emailId}" disabled="disabled">
						</div>
					</div>

					<div class="form-group">
							<label class="col-md-4 control-label" for="designation"><spring:message
								code="label.designation" /><span class="text-danger">*</span></label>
							<div class="col-md-6">
								<input id="designation" name="designation" class="form-control"
									   placeholder="Designation.." type="text" value="${user.designation}">
							</div>
					</div>
					
					<div class="form-group">
							<label class="col-md-4 control-label" for="description"><spring:message
								code="label.description" /></label>
							<div class="col-md-6">
								<textarea id="description" name="description" class="form-control"
									   placeholder="Description.."  >${user.description}</textarea>
							</div>
					</div>
					
					<div class="form-group">
							<label class="col-md-4 control-label" for="approvalRights"><spring:message
								code="label.enquiryAccessRights" /><span class="text-danger">*</span></label>
							<div class="col-md-6">
								<input id="approvalRightsyes" name="approvalRights" class=""
									    type="radio" <c:if test = "${user.enquiryAccessRights}"> checked = "checked"</c:if> value="1">Yes <br>
								<input id="approvalRightsno" name="approvalRights" class=""
									    type="radio" <c:if test = "${!user.enquiryAccessRights}"> checked = "checked"</c:if> value="0">No
							</div>
					</div>
						

			
			<div style="text-align: center; margin-bottom: 10px">
				<button id="user_submit" class="btn btn-sm btn-primary save"
					type="submit">
					<i class="fa fa-angle-right"></i>
					<spring:message code="button.save" />
				</button>
				<a href="<%=contexturl %>ManageAdmin/AdminList/" class="btn btn-sm btn-primary save"
					style="text-decoration: none;"><i class="gi gi-remove"></i> <spring:message
						code="button.cancel" /></a>
			</div>

	</form>
</div>

	<div id="reset-user-settings" class="modal fade" tabindex="-1"
		role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header text-center">
					<h2 class="modal-title">
						<i class="fa fa-pencil"></i> <spring:message code="label.resetpassword"/>
					</h2>
				</div>
				<span id="passwordErrorMsg"></span>
				<div class="modal-body">
					<form action="<%=contexturl %>ManageUsers/UserList/ResetG7EmpPassword" method="post"
						class="form-horizontal form-bordered" id="ResetPassword_Form">
						<input name="userId" value="${user.userId}"
							type="hidden">
						<fieldset>
							<div class="form-group">
								<label class="col-md-4 control-label"
									for="user-settings-password"><spring:message code="label.newpassword"/></label>
								<div class="col-md-8">
									<input type="password" id="password" name="password"
										class="form-control"
										placeholder="Please choose a complex one..">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label"
									for="user-settings-repassword"><spring:message code="label.confirmnewpassword"/></label>
								<div class="col-md-8">
									<input type="password" id="user-settings-repassword"
										name="confirmPassword" class="form-control"
										placeholder="..and confirm it!">
								</div>
							</div>
						</fieldset>
						<div class="form-group form-actions">
							<div class="col-xs-12 text-right">
								<button type="button" class="btn btn-sm btn-default"
									data-dismiss="modal"><spring:message code="label.close"/></button>
								<button id="submit_password" class="btn btn-sm btn-primary save"
									type="submit"><spring:message code="label.savechanges"/></button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<%@include file="../../inc/page_footer.jsp"%>
<%@include file="../../inc/template_scripts.jsp"%>

<script src="../resources/js/vendor/modernizr-2.7.1-respond-1.4.2.min.js"></script>

<script type="text/javascript">
$(document).ready(function() {
	$("#manageAdmin").addClass("active");

	
$("#User_form").validate(
		{	errorClass:"help-block animation-slideDown",
			errorElement:"div",
			errorPlacement:function(e,a){a.parents(".form-group > div").append(e)},
			highlight:function(e){$(e).closest(".form-group").removeClass("has-success has-error").addClass("has-error")},
			success:function(e){e.closest(".form-group").removeClass("has-success has-error")},
			rules:{ firstName : {required : !0,maxlength : 75},
				lastName : {required : !0,maxlength : 75},
				designation :{required : !0,maxlength : 75},
				description : {maxlength : 500}
			},
			messages:{
				firstName : {
					required :'<spring:message code="validation.pleaseenterfirstname"/>',
					maxlength :'<spring:message code="validation.firstname75character"/>'
				},
				lastName : {
					required :'<spring:message code="validation.pleaseenterlastname"/>',
					maxlength :'<spring:message code="validation.lastname75character"/>'
				},

				designation : {
					required :'<spring:message code="validation.pleaseenterdesignation"/>',
					maxlength :'<spring:message code="validation.designation75character"/>'
				},
				description : {
					maxlength :'<spring:message code="validation.description500character"/>'
				}
			},
		});

	 $("#user_submit").click(function(){
		 $("#User_form").submit();
	 });

});
</script>