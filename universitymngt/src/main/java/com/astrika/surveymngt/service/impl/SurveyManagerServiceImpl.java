package com.astrika.surveymngt.service.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.astrika.common.exception.NoSuchSurveyException;
import com.astrika.common.service.DocumentService;
import com.astrika.common.util.PropsValues;
import com.astrika.companymngt.repository.CompanyRepository;
import com.astrika.surveymngt.model.SurveyManager;
import com.astrika.surveymngt.model.SurveyStatus;
import com.astrika.surveymngt.repository.SurveyManagerRepository;
import com.astrika.surveymngt.service.SurveyManagerService;

@Service
public class SurveyManagerServiceImpl implements SurveyManagerService{

	@Autowired
	private SurveyManagerRepository surveyManagerRepository;

	@Autowired
	PropsValues propertyValues;

	@Autowired
	private CompanyRepository companyRepository;

	@Autowired
	private DocumentService documentService;

	@Override
	public SurveyManager save(SurveyManager surveyManager) {
		return surveyManagerRepository.save(surveyManager);

	}

	@Override
	public SurveyManager findByCompanyId(long companyId) {

		return surveyManagerRepository.findByCompanyId(companyId);
	}

	@Override
	public SurveyManager findByCompanyIdandOngoingSurvey(long companyId,
			boolean ongoingSurvey) {
		SurveyManager survey =  surveyManagerRepository.findByCompanyIdandOngoingSurvey(companyId, ongoingSurvey);
		if(survey!=null){
			return survey;
		}
		else{
			return null;
		}
	}


	@Override
	public SurveyManager findByCompanyIdandSurveyStatus(long companyId,
			SurveyStatus status) {
		return surveyManagerRepository.findByCompanyIdandSurveyStatus(companyId, status);
	}

	@Override
	public SurveyManager findBySurveyManagerId(long surveyManagerId) {
		return surveyManagerRepository.findBySurveyManagerId(surveyManagerId);
	}


	@Override
	public void resetSurveyBySurveyMangerId(Long surveyManagerId){

		SurveyManager surveyManager = surveyManagerRepository.findOne(surveyManagerId);
		if (surveyManager == null){
			throw new NoSuchSurveyException("Survey not found for company id " + surveyManagerId);
		}

		DateTime dateTime = new DateTime().plusMonths(1);

		surveyManager.setLastDateToComplete(dateTime);

		surveyManager.setSurveyStatus(SurveyStatus.PENDING);
		surveyManager.setOngoingSurvey(true);

		surveyManagerRepository.save(surveyManager);
	}

	@Override
	public List<SurveyManager> findSurveyManagerByCompanyIds(List<Long> ids) {
		return surveyManagerRepository.findSurveyManagerByCompanyIds(ids);
	}

	@Override
	public List<SurveyManager> findLatestSurveyManagerByCompanyIds(List<Long> ids) {
		return surveyManagerRepository.findLatestSurveyManagerByCompanyIds(ids);
	}

	@Override
	public List<SurveyManager> findLatestSurveyManagerByCompanyIdsAndStatus(List<Long> ids,SurveyStatus surveyStatus) {
		return surveyManagerRepository.findLatestSurveyManagerByCompanyIdsAndStatus(ids, surveyStatus);
	}

	@Override
	public int getNoOfCompaniesTakenSurvey() {
		return surveyManagerRepository.getNoOfCompaniesTakenSurvey();
	}

	@Override
	public int getNoOdSurveyComplited() {
		return surveyManagerRepository.getNoOdSurveyComplited();
	}


	@Override
	public List<Object[]> getIncompliteLss() {
		return surveyManagerRepository.getIncompliteLss();
	}

	@Override
	public String getIncompliteSurvey() throws IOException {


		Workbook wb = new XSSFWorkbook();
		CreationHelper createHelper = wb.getCreationHelper();
		Sheet sheet = wb.createSheet("Report");
		int rowIndex = 0;
		Row headerRow = sheet.createRow((short) rowIndex++);
		headerRow.createCell(0).setCellValue(
				createHelper.createRichTextString("Organisation Name"));
		headerRow.createCell(1).setCellValue(
				createHelper.createRichTextString("Total module"));
		headerRow.createCell(2).setCellValue(
				createHelper.createRichTextString("Module Completed"));


		List<Object[]> resultlist = getIncompliteLss();

		java.util.Iterator<Object[]> it = resultlist.iterator();
		while(it.hasNext()){
			Object[] obj= it.next();
			Row row1=sheet.createRow(rowIndex);
			row1.createCell(0).setCellValue(createHelper.createRichTextString(obj[0].toString()));
			row1.createCell(1).setCellValue(createHelper.createRichTextString(obj[1].toString()));
			row1.createCell(2).setCellValue(createHelper.createRichTextString(obj[2].toString()));
			rowIndex++;
		}

		String tempUrl=propertyValues.tempCsvDownloadpath;
		FileOutputStream fileOutUrl = new FileOutputStream( tempUrl+ File.separator +"UserDetails.csv");
		String fileoutl=tempUrl + File.separator +"UserDetails.csv";
		wb.write(fileOutUrl);
		fileOutUrl.close();
		return fileoutl;

	}


}
