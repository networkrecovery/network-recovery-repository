<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->

<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/template_start.jsp"%>
<%@include file="page_head.jsp"%>

<div id="page-content">
	<div class="content-header">
		<div class="header-section">
			<c:if test="${!empty error}">
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-times-circle"></i> <spring:message code="label.error"/>
					</h4>
					<spring:message code="${error}" />
				</div>
			</c:if>

			<h1>
<!-- 				<i class="fa fa-map-marker"></i> -->
				<spring:message code="heading.moduleuser" />
				<br> <small><spring:message
						code="heading.moduleuserdetails" /></small>
			</h1>
			<span id="errorMsg"></span>
		</div>
	</div>
	<ul class="breadcrumb breadcrumb-top">
		<li><spring:message code="heading.moduleuser" /></li>
	</ul>
	<form action="<%=contexturl%>ManageModuleUser/ModuleUserList/SaveAssignModule"
		method="post" class="form-horizontal ui-formwizard" id="User_form">

		<div class="col-md-12">
			<div class="block">
				<div class="block-title">
					<h2>
						<strong><spring:message code="heading.moduleuserinfo" /></strong>
					</h2>
					<input name="moduleuserId" value="${moduleUser.moduleuserId}"
						type="hidden">
				</div>
				<table>
					<tr>
						<c:forEach var="currentModule" items="${moduleUserList}">
							<div class="form-group" id="moduleDiv">

								<input type="hidden" name="moduleId"
									value="${currentModule.module.moduleId}" /> <label
									class="col-md-4 control-label" for="chosenUserId">${currentModule.module.moduleName}</label>

								<div class="col-md-6">
									<select class="user_chosen" style="width: 200px;"
										id="chosenUserId"
										data-placeholder="<spring:message code='label.chooseuser' />"
										name="${currentModule.module.moduleId}">
										<option value=""></option>
										<c:forEach items="${userList}" var="user">
											<option value="${user.userId}"
												<c:if test="${user.userId eq currentModule.user.userId}">selected="selected"</c:if>>${user.fullName}</option>
										</c:forEach>
									</select>
								</div>
							</div>
						</c:forEach>
				</table>
			</div>
		</div>

		<div class="form-group form-actions">
			<div class="col-md-9 col-md-offset-3">
				<button id="user_submit" type="submit"
					class="btn btn-sm btn-primary save">
					<i class="fa fa-angle-right"></i>
					<spring:message code="button.save" />
				</button>
				<a class="btn btn-sm btn-primary save"
					href="<%=contexturl%>ManageModuleUser/ModuleUserList/"><i
					class="gi gi-remove"></i> <spring:message code="button.cancel" />
				</a>
			</div>
		</div>
	</form>
</div>
<%@include file="../../inc/page_footer.jsp"%>
<%@include file="../../inc/template_scripts.jsp"%>
<%@include file="../../inc/chosen_scripts.jsp"%>
<script src="<%=contexturl %>resources/js/custom/featurecontrol.js"></script>

<script type="text/javascript">
$(document).ready(function() {
	$(".user_chosen").data("placeholder","Select User From...").chosen();
	$("#manageModuleUser").addClass("active");
// 	$("#availability").click(
// 			function() {
// 				var data = $("#login").val();
// 				$("#notavailable").css("display", "none")
// 				$("#available").css("display", "none")
// 				if (data != "") {
<%-- 					$.getJSON("<%=contexturl%>ValidateLogin", { --%>
// 						loginId : data
// 					}, function(data) {
// 						if (data == "exist") {
// 							$("#notavailable").css(
// 									"display", "inline")
// 						} else if (data == "available") {
// 							$("#available").css("display",
// 									"inline")
// 						}
// 					});
// 				}
// 			});
	
	$("#User_form").validate(
			{	errorClass:"help-block animation-slideDown",
				errorElement:"div",
				errorPlacement:function(e,a){a.parents(".form-group > div").append(e)},
				highlight:function(e){$(e).closest(".form-group").removeClass("has-success has-error").addClass("has-error")},
				success:function(e){e.closest(".form-group").removeClass("has-success has-error")},
				rules:{
					moduleId : {
						required : !0
					}
				},
				messages: {
					moduleId : {
						required :'Please assign the module user'
 					}
 				},
			}); 	



	 $("#user_submit").click(function(){
		 $("#User_form").submit();
	 });
	
});
</script>
 


<%@include file="../../inc/template_end.jsp"%>