<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/template_start.jsp"%>
<%@include file="page_head.jsp"%>
<div id="page-content">
	<div class="content-header">
		<div class="header-section">
			<h1>
				<i class="fa fa-map-marker"></i>
				<spring:message code="heading.moduleuser" />
				<br> <small><spring:message
						code="heading.moduleuserdetails" /></small>
			</h1>
			<span id="errorMsg"></span>
			<c:if test="${!empty success}">
				<div class="alert alert-success alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-check-circle"></i>
						<spring:message code="label.success" />
					</h4>
					<spring:message code="${success}" />
				</div>
			</c:if>

			<c:if test="${!empty error}">
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">�</button>
					<h4>
						<i class="fa fa-times-circle"></i>
						<spring:message code="label.error" />
					</h4>
					<spring:message code="${error}" />
				</div>
			</c:if>
		</div>
	</div>
	<ul class="breadcrumb breadcrumb-top">
		<li><spring:message code="heading.moduleuser" /></li>
	</ul>

	<div class="block full">
		<div class="block-title">
			<h2>
				<strong><spring:message code="heading.moduleuserinfo" /></strong>
			</h2>
		</div>
		<form
			action="<%=contexturl%>ManageModuleUser/ModuleUserList/UpdateModuleUser"
			method="post" class="form-horizontal " id="User_form">
			<input name="userId" value="${user.userId}" type="hidden">
			<div class="form-group">
				<label class="col-md-4 control-label" for="first_Name"><spring:message
						code="label.firstname" /><span class="text-danger">*</span> </label>
				<div class="col-md-6">

					<input id="first_Name" name="firstName" class="form-control"
						placeholder="<spring:message code='label.firstname'/>.."
						type="text" value="${user.firstName}">
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-4 control-label" for="last_Name"><spring:message
						code="label.lastname" /><span class="text-danger">*</span> </label>
				<div class="col-md-6">

					<input id="last_Name" name="lastName" class="form-control"
						placeholder="<spring:message code="label.lastname"/>.."
						type="text" value="${user.lastName}">
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-4 control-label" for="email"><spring:message
						code="label.emailid" /><span class="text-danger">*</span></label>
				<div class="col-md-6">
					<input id="email" name="emailId" class="form-control"
						placeholder="test@example.com" type="text" value="${user.emailId}">
				</div>
			</div>



			<div class="form-group">
				<label class="col-md-4 control-label" for="mobile"><spring:message
						code="label.mobile" /><span class="text-danger">*</span></label>
				<div class="col-md-6">
					<input id="mobile" name="mobile" class="form-control"
						placeholder="Mobile no" type="text" value="${user.mobile}">
				</div>
			</div>

			<div class="form-group form-actions">
				<div class="col-md-9 col-md-offset-3">
					<button id="user_submit" type="submit"
						class="btn btn-sm btn-primary save">
						<i class="fa fa-angle-right"></i>
						<spring:message code="button.save" />
					</button>
					<!-- 					<button type="reset" class="btn btn-sm btn-warning save"> -->
					<!-- 						<i class="fa fa-repeat"></i> -->
					<%-- 						<spring:message code="button.reset" /> --%>
					<!-- 					</button> -->
					<a class="btn btn-sm btn-primary save"
						href="<%=contexturl%>ManageModuleUser/ModuleUserList/"><i
						class="gi gi-remove"></i> <spring:message code="button.cancel" />
					</a>
				</div>
			</div>
		</form>
	</div>
</div>
<%@include file="../../inc/page_footer.jsp"%>
<%@include file="../../inc/template_scripts.jsp"%>
<%@include file="../../inc/chosen_scripts.jsp"%>
<script
	src="../resources/js/vendor/modernizr-2.7.1-respond-1.4.2.min.js"></script>

<script type="text/javascript">
	$(document)
			.ready(
					function() {
						$(".module_chosen").data("placeholder",
								"Select Module From...").chosen();

						$("#User_form")
								.validate(
										{
											errorClass : "help-block animation-slideDown",
											errorElement : "div",
											errorPlacement : function(e, a) {
												a.parents(".form-group > div")
														.append(e)
											},
											highlight : function(e) {
												$(e)
														.closest(".form-group")
														.removeClass(
																"has-success has-error")
														.addClass("has-error")
											},
											success : function(e) {
												e
														.closest(".form-group")
														.removeClass(
																"has-success has-error")
											},
											rules : {
												firstName : {
													required : !0,
													maxlength : 75
												},
												lastName : {
													required : !0,
													maxlength : 75
												},
												emailId : {
													required : !0,
													maxlength : 75,
													email : !0
												},
												mobile : {
													required : !0,
													maxlength : 10,
													mobile : !0
												},
												chosenModuleId : {
													required : !0
												}
											},
											messages : {
												firstName : {
													required : '<spring:message code="validation.pleaseenterfirstname"/>',
													maxlength : '<spring:message code="validation.firstname75character"/>'
												},
												lastName : {
													required : '<spring:message code="validation.pleaseenterlastname"/>',
													maxlength : '<spring:message code="validation.lastname75character"/>'
												},
												emailId : {
													required : '<spring:message code="validation.pleaseenteremailid"/>',
													email : '<spring:message code="validation.pleaseenteravalidemailid"/>',
													maxlength : '<spring:message code="validation.email75character"/>'
												},
												mobile : {
													required : '<spring:message code="validation.pleaseenteramobilenumber"/>',
													maxlength : '<spring:message code="validation.mobile10character"/>',
													mobile : '<spring:message code="validation.phone"/>'
												},
												chosenModuleId : {
													required : '<spring:message code="validation.selectmodule"/>'
												}
											},
										});

						$("#user_submit").click(function() {
							$("#User_form").submit();
						});

					});
</script>
<script type="text/javascript">
	$(document).ready(function() {
		$("#manageModuleUser").addClass("active");
		$(".date-picker").datepicker({
			changeMonth : true,
			changeYear : true,
			dateFormat : "dd/mm/yy"
		});
		$(".date-picker").on("change", function() {
			var id = $(this).attr("id");
			var val = $("label[for='" + id + "']").text();
			$("#msg").text(val + " changed");
		});
	});
</script>