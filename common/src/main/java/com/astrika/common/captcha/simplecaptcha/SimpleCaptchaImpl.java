/**
 * Copyright (c) 2014 Astrika, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.astrika.common.captcha.simplecaptcha;

import java.io.IOException;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import nl.captcha.backgrounds.BackgroundProducer;
import nl.captcha.gimpy.GimpyRenderer;
import nl.captcha.noise.NoiseProducer;
import nl.captcha.servlet.CaptchaServletUtil;
import nl.captcha.text.producer.TextProducer;
import nl.captcha.text.renderer.WordRenderer;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.astrika.common.util.PropsValues;
import com.astrika.common.util.WebKeys;
import com.astrika.kernel.captcha.Captcha;
import com.astrika.kernel.captcha.CaptchaException;
import com.astrika.kernel.captcha.CaptchaMaxChallengesException;
import com.astrika.kernel.captcha.CaptchaTextException;
import com.astrika.kernel.exception.CustomException;
import com.astrika.kernel.util.ContentTypes;
import com.astrika.kernel.util.InstancePool;
import com.astrika.kernel.util.ParamUtil;
import com.astrika.kernel.util.Randomizer;
import com.astrika.kernel.util.StringUtil;
import com.astrika.kernel.util.Validator;
/**
 * @author Priyanka
 */

public class SimpleCaptchaImpl implements Captcha {

    @Autowired
    private PropsValues prop;
    
    private static final String TAGLIBPATH ="/html/taglib/ui/captcha/simplecaptcha.jsp";
            
    private static final Logger LOGGER = Logger.getLogger(SimpleCaptchaImpl.class);

    private BackgroundProducer[] backgroundProducers;
    private GimpyRenderer[] gimpyRenderers;
    private NoiseProducer[] noiseProducers;
    private TextProducer[] textProducers;
    private WordRenderer[] wordRenderers;


    @PostConstruct
    public void init(){
        initBackgroundProducers();
        initGimpyRenderers();
        initNoiseProducers();
        initTextProducers();
        initWordRenderers();
    }

    public void check(HttpServletRequest request) throws CaptchaException {
        if (!isEnabled(request)) {
            return;
        }

        if (!validateChallenge(request)) {
            incrementCounter(request);

            checkMaxChallenges(request);

            throw new CaptchaTextException(CustomException.CAPTCHA_MISMATCH.getCode());
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Captcha text is valid");
        }
    }

    public String getTaglibPath() {
        return TAGLIBPATH;
    }

    public boolean isEnabled(HttpServletRequest request)
            throws CaptchaException {

        checkMaxChallenges(request);







        return prop.captchaMaxChallenges >= 0;

    }

    public void serveImage(
            HttpServletRequest request, HttpServletResponse response)
                    throws IOException {

        HttpSession session = request.getSession();

        nl.captcha.Captcha simpleCaptcha = getSimpleCaptcha();

        session.setAttribute(WebKeys.CAPTCHA_TEXT, simpleCaptcha.getAnswer());

        response.setContentType(ContentTypes.IMAGE_JPEG);

        CaptchaServletUtil.writeImage(
                response.getOutputStream(), simpleCaptcha.getImage());
    }


    protected void checkMaxChallenges(HttpServletRequest request)
            throws CaptchaMaxChallengesException {

        if (prop.captchaMaxChallenges > 0) {
            HttpSession session = request.getSession();

            Integer count = (Integer)session.getAttribute(
                    WebKeys.CAPTCHA_COUNT);

            checkMaxChallenges(count);
        }
    }

    protected void checkMaxChallenges(Integer count)
            throws CaptchaMaxChallengesException {

        if ((count != null) && (count > prop.captchaMaxChallenges)) {
            throw new CaptchaMaxChallengesException();
        }
    }



    protected BackgroundProducer getBackgroundProducer() {
        if (backgroundProducers.length == 1) {
            return backgroundProducers[0];
        }

        Randomizer randomizer = Randomizer.getInstance();

        int pos = randomizer.nextInt(backgroundProducers.length);

        return backgroundProducers[pos];
    }

    protected GimpyRenderer getGimpyRenderer() {
        if (gimpyRenderers.length == 1) {
            return gimpyRenderers[0];
        }

        Randomizer randomizer = Randomizer.getInstance();

        int pos = randomizer.nextInt(gimpyRenderers.length);

        return gimpyRenderers[pos];
    }

    protected int getHeight() {
        return prop.captchaEngineSimplecaptchaHeight;
    }

    protected NoiseProducer getNoiseProducer() {
        if (noiseProducers.length == 1) {
            return noiseProducers[0];
        }

        Randomizer randomizer = Randomizer.getInstance();

        int pos = randomizer.nextInt(noiseProducers.length);

        return noiseProducers[pos];
    }

    protected nl.captcha.Captcha getSimpleCaptcha() {
        nl.captcha.Captcha.Builder captchaBuilder =
                new nl.captcha.Captcha.Builder(getWidth(), getHeight());

        captchaBuilder.addText(getTextProducer(), getWordRenderer());
        captchaBuilder.addBackground(getBackgroundProducer());
        captchaBuilder.gimp(getGimpyRenderer());
        captchaBuilder.addNoise(getNoiseProducer());
        captchaBuilder.addBorder();

        return captchaBuilder.build();
    }

    protected TextProducer getTextProducer() {
        if (textProducers.length == 1) {
            return textProducers[0];
        }

        Randomizer randomizer = Randomizer.getInstance();

        int pos = randomizer.nextInt(textProducers.length);

        return textProducers[pos];
    }

    protected int getWidth() {

        return prop.captchaEngineSimplecaptchaWidth;
    }

    protected WordRenderer getWordRenderer() {
        if (wordRenderers.length == 1) {
            return wordRenderers[0];
        }

        Randomizer randomizer = Randomizer.getInstance();

        int pos = randomizer.nextInt(wordRenderers.length);

        return wordRenderers[pos];
    }

    protected void incrementCounter(HttpServletRequest request) {
        if ((prop.captchaMaxChallenges > 0) &&
                (Validator.isNotNull(request.getRemoteUser()))) {

            HttpSession session = request.getSession();

            Integer count = (Integer)session.getAttribute(
                    WebKeys.CAPTCHA_COUNT);

            session.setAttribute(WebKeys.CAPTCHA_COUNT,
                    incrementCounter(count));
        }
    }



    protected Integer incrementCounter(Integer count) {

        Integer countIncreament;
        if (count == null) {
            countIncreament = new Integer(1);
        }
        else {
            countIncreament = new Integer(count.intValue() + 1);
        }

        return countIncreament;
    }


    protected void initBackgroundProducers() {
        String[] backgroundProducerClassNames =
                StringUtil.split(prop.captchaEngineSimplecaptchaBackgroundProducers);

        backgroundProducers = new BackgroundProducer[
                                                     backgroundProducerClassNames.length];

        for (int i = 0; i < backgroundProducerClassNames.length; i++) {
            String backgroundProducerClassName =
                    backgroundProducerClassNames[i];

            backgroundProducers[i] = (BackgroundProducer)InstancePool.get(
                    backgroundProducerClassName);
        }
    }

    protected void initGimpyRenderers() {
        String[] gimpyRendererClassNames =
                StringUtil.split(prop.captchaEngineSimplecaptchaGimpyRenderers);

        gimpyRenderers = new GimpyRenderer[
                                           gimpyRendererClassNames.length];

        for (int i = 0; i < gimpyRendererClassNames.length; i++) {
            String gimpyRendererClassName = gimpyRendererClassNames[i];

            gimpyRenderers[i] = (GimpyRenderer)InstancePool.get(
                    gimpyRendererClassName);
        }
    }

    protected void initNoiseProducers() {
        String[] noiseProducerClassNames =
                StringUtil.split(   prop.captchaEngineSimplecaptchaNoiseProducers);

        noiseProducers = new NoiseProducer[noiseProducerClassNames.length];

        for (int i = 0; i < noiseProducerClassNames.length; i++) {
            String noiseProducerClassName = noiseProducerClassNames[i];

            noiseProducers[i] = (NoiseProducer)InstancePool.get(
                    noiseProducerClassName);
        }
    }

    protected void initTextProducers() {
        String[] textProducerClassNames =
                StringUtil.split(prop.captchaEngineSimplecaptchaTextProducers);


        textProducers = new TextProducer[textProducerClassNames.length];

        for (int i = 0; i < textProducerClassNames.length; i++) {
            String textProducerClassName = textProducerClassNames[i];

            textProducers[i] = (TextProducer)InstancePool.get(
                    textProducerClassName);
        }
    }

    protected void initWordRenderers() {

        String[] wordRendererClassNames =
                StringUtil.split(prop.captchaEngineSimplecaptchaWordRenderers);

        wordRenderers = new WordRenderer[wordRendererClassNames.length];

        for (int i = 0; i < wordRendererClassNames.length; i++) {
            String wordRendererClassName = wordRendererClassNames[i];

            wordRenderers[i] = (WordRenderer)InstancePool.get(
                    wordRendererClassName);
        }
    }

    protected boolean validateChallenge(HttpServletRequest request)
            throws CaptchaException {

        HttpSession session = request.getSession();

        String captchaText = (String)session.getAttribute(WebKeys.CAPTCHA_TEXT);

        if (captchaText == null) {
            LOGGER.error(
                    "Captcha text is null. User " + request.getRemoteUser() +
                    " may be trying to circumvent the captcha.");

            throw new CaptchaTextException();
        }

        boolean valid = captchaText.equals(
                ParamUtil.getString(request, "captchaText"));

        if (valid) {
            session.removeAttribute(WebKeys.CAPTCHA_TEXT);
        }

        return valid;
    }


    


}
