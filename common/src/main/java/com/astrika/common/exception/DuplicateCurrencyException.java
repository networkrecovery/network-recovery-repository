package com.astrika.common.exception;

import com.astrika.kernel.exception.BusinessException;
import com.astrika.kernel.exception.CustomException;

public class DuplicateCurrencyException extends BusinessException {
	
	
	public DuplicateCurrencyException() {
		super(CustomException.DUPLICATE_CURRENCY.getCode());
	}

	public DuplicateCurrencyException(String errorCode) {
		super(errorCode);
	}

}
