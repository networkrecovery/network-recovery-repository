package com.astrika.companymngt.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.astrika.companymngt.model.module.ModuleMaster;





public interface ModuleRepository extends JpaRepository<ModuleMaster,Long> {
	@Query(value="select c from ModuleMaster c  where c.active=1 Order By c.moduleName ASC ",countQuery="select count(c) from ModuleMaster c where c.active = 1")
	Page<ModuleMaster> findByActiveTrueOrderByModuleNameAsc(Pageable page);

	@Query(value="select c from ModuleMaster c  where c.active=0 Order By c.moduleName ASC ",countQuery="select count(c) from ModuleMaster c where c.active = 0")
	Page<ModuleMaster> findByActiveFalseOrderByModuleNameAsc(Pageable page);
	
	@Query("select c from ModuleMaster c  left join fetch c.createdBy where c.active=?1 Order By c.moduleId ASC ")
	List<ModuleMaster> findAllByActive(boolean active);
    
	@Query("select c from ModuleMaster c where c.moduleName= ?1 order by c.moduleName ASC")
	ModuleMaster findByModuleName(String moduleName);

	@Query("select c from ModuleMaster c  where c.moduleId = ?1")
	ModuleMaster findByModuleModuleId(long id);
	
	@Query("select c from ModuleMaster c where c.moduleId = ?1 and c.active = ?2")
	ModuleMaster findByModuleIdandActive(long moduleId, boolean active);
	
	@Query(value = "select c from ModuleMaster c where c.moduleId > ?1 and c.active = ?2 Order By c.moduleId ASC",countQuery="select count(c) from ModuleMaster c where c.moduleId > ?1 and c.active = ?2")
	Page<ModuleMaster> findTheNextRow(long moduleId, boolean active, Pageable page);
	

}
