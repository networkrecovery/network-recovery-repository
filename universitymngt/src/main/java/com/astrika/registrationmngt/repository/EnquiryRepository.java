package com.astrika.registrationmngt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.astrika.common.model.CompanyApproval;
import com.astrika.registrationmngt.model.EnquiryList;

public interface EnquiryRepository extends JpaRepository<EnquiryList, Long> {
	
	
	@Query("select c from EnquiryList c left join fetch c.state left join fetch c.city left join fetch c.country  where c.enquiryId = ?1")
	EnquiryList findByEnquiryId(long id);
	
	@Query("select c from EnquiryList c where c.ngoEmail = ?1")
	EnquiryList findByNgoEmail(String ngoEmail);

	@Query("select c from EnquiryList c where c.approval =?1")
	List<EnquiryList> findByApproval(CompanyApproval approval);
	
	@Query("select c from EnquiryList c where c.emailId = ?1")
	EnquiryList findByUserEmail(String emailId);
	
	@Query("select c from EnquiryList c where c.ngoName = ?1")
	EnquiryList findByNgoName(String ngoName);

}
