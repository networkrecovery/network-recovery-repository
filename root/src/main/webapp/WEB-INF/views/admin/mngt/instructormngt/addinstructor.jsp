<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->

<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/template_start.jsp"%>
<%@include file="../../inc/page_head.jsp"%>
<link rel="stylesheet" href="<%=contexturl %>resources/css/jquery-ui.css">
<div id="page-content">
	<div class="content-header">
		<div class="header-section">
			<c:if test="${!empty error}">
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-times-circle"></i> <spring:message code="label.error"/>
					</h4>
					<spring:message code="${error}" />
				</div>
			</c:if>

			<h1>
				<i class="fa fa-map-marker"></i>
				<spring:message code="heading.instructor" />
				<br> <small><spring:message
						code="heading.instructordetails" /></small>
			</h1>
			<span id="errorMsg"></span>
		</div>
	</div>
	<ul class="breadcrumb breadcrumb-top">
		<li><spring:message code="heading.instructor" /></li>
	</ul>
	<form action="<%=contexturl %>ManageInstructor/InstructorList/SaveInstructor" method="post" class="form-horizontal ui-formwizard"
		id="Instructor_form" enctype="multipart/form-data">
		
		<div class="col-md-12">
				<div class="block">
					<div class="block-title">
						<h2>
							<strong><spring:message code="heading.Instructorinfo" /></strong>
						</h2>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label" for="first_Name"><spring:message
								code="label.firstname" /><span class="text-danger">*</span> </label>
						<div class="col-md-6">

							<input id="first_Name" name="firstName" class="form-control"
								placeholder="<spring:message code='label.firstname'/>.."
								type="text" value="${instructor.user.firstName}">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label" for="last_Name"><spring:message
								code="label.lastname" /><span class="text-danger">*</span> </label>
						<div class="col-md-6">

							<input id="last_Name" name="lastName" class="form-control"
								placeholder="<spring:message code="label.lastname"/>.."
								type="text" value="${instructor.user.lastName}">
						</div>
					</div>
					
					<div class="form-group img-list1" id="profilepic">
						<label class="col-md-4 control-label" for="offer_validtill"><spring:message code="label.profilepic" /></label>
						<div class="col-md-6 img-list2" id="imagediv">
						
							<img border="0"
								src="<%=contexturl%>resources/img/application/default_profile.png"
								width="120" height="120" title="Click here to change photo"
								onclick="OpenFileDialog1();" class="img-circle pic" id="profileimage" style="cursor: pointer;" />
								
								<span class="text-content" onclick="OpenFileDialog1();" title="Click here to change photo"><br>
								<br><span>Upload Profile pic</span></span>
						</div>
						<input type="file" name="imageChooser1" id="imageChooser1" style="display: none;"
							name="imageChooser1"
							accept="image/jpg,image/jpeg,image/png,image/gif,image/bmp,image/tiff" />

					</div>
					<security:authorize ifAnyGranted="<%=Role.SKILLME_ADMIN.name() %>">
					<div class="form-group" id="universityDiv">
						<label class="col-md-4 control-label" for="choseninstituteId"><spring:message
								code="label.institutionname" /><span class="text-danger">*</span></label>
						<div class="col-md-6">
							<select class="institute_chosen" style="width: 200px;"
								id="chosenInstituteId"
								data-placeholder="<spring:message code='label.choosecompany' />"
								name="chosenInstituteId">
								<option value=""></option>
								<c:forEach items="${activeUniversity}" var="university">
											<option value="${university.universityId}" 
											<c:if test="${university.universityId eq instructor.university.universityId}">selected="selected"</c:if>>${university.universityName}</option>
								</c:forEach>
							</select>
						</div>
					</div>
					</security:authorize>
					<div class="form-group">
						<label class="col-md-4 control-label" for="last_Name"><spring:message
								code="label.department" /><span class="text-danger">*</span> </label>
						<div class="col-md-6">

							<input id="department" name="department" class="form-control ui-autocomplete-input""
								placeholder="<spring:message code="label.department"/>.."
								type="text" value="${instructor.department.departmentName}" autocomplete="off">
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-md-4 control-label" for="last_Name"><spring:message
								code="label.designation" /><span class="text-danger"></span> </label>
						<div class="col-md-6">

							<input id="designation" name="designation" class="form-control"
								placeholder="<spring:message code="label.designation"/>.."
								type="text" value="${instructor.designation}">
						</div>
					</div>

							<div class="form-group">
									<label class="col-md-4 control-label" for="qualification"><spring:message
											code="label.qualification" /><span class="text-danger"></span></label>
									<div class="col-md-4">
										<textarea id="qualification" name="qualification" class="form-control"
											rows="3" placeholder="<spring:message code="label.qualification"/>.."  >${instructor.qualification}</textarea>
									</div>
							</div>
							
							<div class="form-group">
									<label class="col-md-4 control-label" for="aboutinstructor"><spring:message
											code="label.aboutinstructor" /><span class="text-danger"></span></label>
									<div class="col-md-4">
										<textarea id="aboutinstructor" name="aboutinstructor" class="form-control"
											rows="3" placeholder="<spring:message code="label.aboutinstructor"/>.."  >${instructor.aboutInstructor}</textarea>
									</div>
							</div>
					
					<div class="form-group">
						<label class="col-md-4 control-label" for="email"><spring:message
								code="label.emailid" /><span class="text-danger">*</span></label>
						<div class="col-md-6">
							<input id="email" name="emailId" class="form-control"
								placeholder="test@example.com" type="text" value="${instructor.user.emailId}">
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-md-4 control-label" for="website"><spring:message
								code="label.website" /><span class="text-danger"></span> </label>
						<div class="col-md-6">

							<input id="website" name="website" class="form-control"
								placeholder="<spring:message code="label.website"/>.."
								type="text" value="${instructor.website}">
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-md-4 control-label" for="facebook"><spring:message
								code="label.facebook" /><span class="text-danger"></span> </label>
						<div class="col-md-6">

							<input id="facebook" name="facebook" class="form-control"
								placeholder="<spring:message code="label.facebooklink"/>.."
								type="text" value="${instructor.facebookLink}">
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-md-4 control-label" for="twitter"><spring:message
								code="label.twitter" /><span class="text-danger"></span> </label>
						<div class="col-md-6">

							<input id="twitter" name="twitter" class="form-control"
								placeholder="<spring:message code="label.twitterlink"/>.."
								type="text" value="${instructor.twitterLink}">
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-md-4 control-label" for="linkedIn"><spring:message
								code="label.linkedIn" /><span class="text-danger"></span> </label>
						<div class="col-md-6">

							<input id="linkedIn" name="linkedIn" class="form-control"
								placeholder="<spring:message code="label.linkedInLink"/>.."
								type="text" value="${instructor.linkedinLink}">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label" for="mobile"><spring:message
								code="label.mobile" /><span class="text-danger"></span></label>
						<div class="col-md-6">
							<input id="mobile" name="mobile" class="form-control"
								placeholder='<spring:message code="label.mobile"></spring:message>'
								type="text" value="${instructor.user.mobile}">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label" for="login"><spring:message
								code="label.loginid" /> <span class="text-danger">*</span> </label>
						<div class="col-md-6">
							<input id="login" name="loginId" class="form-control"
								placeholder='<spring:message code='label.loginid'/>' type="text" value="${instructor.user.loginId}">
							<span class="label label-success" id="availability"
								style="cursor: pointer;"><i class="fa fa-check"></i>
								<spring:message code="label.checkavailability"/></span><span class="text-success" id="available"
								style="display: none"> <spring:message
									code="alert.loginavailable" />
							</span> <span class="text-danger" id="notavailable"
								style="display: none"> <spring:message
									code="alert.loginnotavailable" />
							</span>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label" for="password"><spring:message
								code="label.password" /> <span class="text-danger">*</span> </label>
						<div class="col-md-6">
							<input id="password" name="password" class="form-control"
								placeholder="Choose a crazy one.." type="password" value="${user.password}">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label" for="confirm_password"><spring:message
								code="label.confirmpassword" /><span class="text-danger">*</span>
						</label>
						<div class="col-md-6">
							<input id="confirm_password" name="confirmPassword"
								class="form-control" placeholder="..and confirm it!"
								type="password" value="${user.password}">
						</div>
					</div>
				</div>
			</div>
			<div class="form-group form-actions">
				<div class="col-md-9 col-md-offset-3">
					<button id="user_submit" type="submit"
						class="btn btn-sm btn-primary save">
						<i class="fa fa-angle-right"></i> <spring:message code="button.save" />
					</button>
					<button type="reset" class="btn btn-sm btn-warning">
						<i class="fa fa-repeat"></i>
						<spring:message code="button.reset" />
					</button>
					<a class="btn btn-sm btn-primary" href="<%=contexturl %>ManageInstructor/InstructorList/">
							<spring:message code="button.cancel" /> </a>
				</div>
			</div>
	</form>
</div>
<%@include file="../../inc/page_footer.jsp"%>

<%@include file="../../inc/template_scripts.jsp"%>
<script src="<%=contexturl %>resources/js/vendor/jquery-ui.js"></script>
<%@include file="../../inc/chosen_scripts.jsp"%>
<script type="text/javascript">
$(document).ready(function() {
	$(".institute_chosen").chosen();
	$("#availability").click(
			function() {
				var data = $("#login").val();
				$("#notavailable").css("display", "none")
				$("#available").css("display", "none")
				if (data != "") {
					$.getJSON("<%=contexturl%>ValidateLogin", {
						loginId : data
					}, function(data) {
						if (data == "exist") {
							$("#notavailable").css(
									"display", "inline")
						} else if (data == "available") {
							$("#available").css("display",
									"inline")
						}
					});
				}
			});
	
	$("#Instructor_form").validate(
			{	errorClass:"help-block animation-slideDown",
				errorElement:"div",
				errorPlacement:function(e,a){a.parents(".form-group > div").append(e)},
				highlight:function(e){$(e).closest(".form-group").removeClass("has-success has-error").addClass("has-error")},
				success:function(e){e.closest(".form-group").removeClass("has-success has-error")},
				rules:{ firstName : {required : !0,maxlength : 75},
					lastName : {required : !0,maxlength : 75},
					department : {required : !0,maxlength : 75},
					emailId : {required : !0,maxlength : 75,email : !0},
					loginId : {required : !0,maxlength : 75,minlength : 6},
					password : {required : !0,maxlength : 75,minlength : 6},
					confirmPassword : {required : !0,equalTo : "#password"},
					chosenInstituteId: {required : !0},
					mobile : {mobile : !0},
				},
				messages:{
					firstName : {
						required :'<spring:message code="validation.pleaseenterfirstname"/>',
						maxlength :'<spring:message code="validation.firstname75character"/>'
					},
					lastName : {
						required :'<spring:message code="validation.pleaseenterlastname"/>',
						maxlength :'<spring:message code="validation.lastname75character"/>'
					},
					department : {
						required :'<spring:message code="validation.pleaseenterdepartment"/>',
						maxlength :'<spring:message code="validation.department75character"/>'
					},
					emailId : {
						required :'<spring:message code="validation.pleaseenteremailid"/>',
						email :'<spring:message code="validation.pleaseenteravalidemailid"/>',
						maxlength :'<spring:message code="validation.email75character"/>'
					},
					loginId : {
						required :'<spring:message code="validation.pleaseenteraloginid"/>',
						minlength :'<spring:message code="validation.loginidmustbeatleast6character"/>',
						maxlength :'<spring:message code="validation.loginid75character"/>'
					},
					password : {
						required :'<spring:message code="validation.pleaseenterapassword"/>',
						minlength :'<spring:message code="validation.passwordmustbeatleast6character"/>',
						maxlength :'<spring:message code="validation.passworddname75character"/>'
					},
					chosenInstituteId: {
						required : '<spring:message code="validation.selectcountry"/>'
					},
					confirmPassword : {
						required :'<spring:message code="validation.pleaseconfirmpassword"/>',
						equalTo :'<spring:message code="validation.pleaseenterthesamepasswordasabove"/>'
					},
					mobile : {
						phone :'<spring:message code="validation.mobile"/>'
					},
				},
			});
	
	 $("#user_submit").click(function(){
		 $("#Instructor_form").submit();
	 });
	 
	 
});
</script>

<script>
$(function() {
	jQuery(".chosen").data("placeholder","Select Department...").chosen();
	var data = [
				<c:forEach var="department" items="${activeList}" varStatus="departmentCount">
					"${department.departmentName}" ${not departmentName.last ? "," : ""}
				</c:forEach>
			];

	$("#department").autocomplete({
	source:data
	});
});
</script>

<script type="text/javascript">
	function OpenFileDialog1(){
		document.getElementById("imageChooser1").click();
	}
	
	function changePhoto(){
		if($('#imageChooser1').val() != ""){			
			var imageName = document.getElementById("imageChooser1").value;
			var ext1 = imageName.substring(imageName.lastIndexOf("."));
			var ext = ext1.toLowerCase();
			if(ext == '.jpg' || ext == '.jpeg' || ext == '.png' || ext == 'gif' || ext == 'bmp' || ext == 'tiff')
				$('#changePhoto').submit();		
			else
				alert("Please select only image file");		
		}
	}
</script>

<script type="text/javascript">
    function showimages(files) {
        for (var i = 0, f; f = files[i]; i++) {
           
                       
            var reader = new FileReader();
            reader.onload = function (evt) {
            	$( "#image" ).remove();
            	$( "#imagediv" ).remove();
            	
//                 var img = '<div class="col-md-6 id="imagediv"><img border="0" src="' + evt.target.result + '" width="120" height="120" title="Click here to change photo" style="cursor: pointer;"  onclick="OpenFileDialog1();" class="img-circle pic" id="image"/></div>';
                  var img ='<div class="col-md-6 img-list2" id="imagediv"><img border="0" src="' + evt.target.result + '" width="120" height="120" title="Click here to change photo" onclick="OpenFileDialog1();" class="img-circle pic" id="image" style="cursor: pointer;" /><span class="text-content" onclick="OpenFileDialog1();" title="Click here to change photo"><br><br><span>Change Profile pic</span></span></div>'
                 $('#profilepic').append(img);
            }
            reader.readAsDataURL(f);
        }
    }

    $('#imageChooser1').change(function (evt) {
        showimages(evt.target.files);
    });
</script>

<%@include file="../../inc/template_end.jsp"%>