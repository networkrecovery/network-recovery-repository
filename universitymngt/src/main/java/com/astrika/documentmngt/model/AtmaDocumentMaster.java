package com.astrika.documentmngt.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.envers.Audited;
import org.joda.time.DateTime;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import com.astrika.categorymngt.model.CategoryMaster;
import com.astrika.common.model.User;
import com.astrika.companymngt.model.module.ModuleMaster;
import com.astrika.surveymngt.model.LifeStages;


@Entity
@Audited
public class AtmaDocumentMaster {

	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long documentId;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JsonIgnore
	private ModuleMaster module;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JsonIgnore
	private CategoryMaster category;
	
	@Column(nullable = false)
	private String section;
	
	@Column(columnDefinition = "boolean default false", nullable = false)
	private boolean flag;
	
	@Column
	private String documentName; 
	
	@Column
	private String documenturlpath;
	
	@Column
	private String doctype;
	
	@Column(nullable = false)
	private String contentType;
	
	@Column(length=500)
	private String link;
	
	@Column
	private String title;
	
	@Column(length=500)
	private String documentDescription;

	@Column
	private Long sizeofFile;
	
	@JsonIgnore
	@OneToMany(mappedBy = "document",fetch = FetchType.LAZY)
	private List<BluePrintImage> blueprintImages;

	@Column(columnDefinition = "boolean default true", nullable = false)
	private boolean active = true;
	
	@CreatedDate
	private DateTime createdOn;
	
	@Transient
	private Date createdDate;

	@Enumerated(EnumType.ORDINAL)
	private LifeStages lifeStages;
	
	@LastModifiedDate
	private DateTime lastModifiedOn;

	@JsonIgnore
	@CreatedBy
	@ManyToOne(fetch = FetchType.LAZY)
	private User createdBy;

	@JsonIgnore
	@LastModifiedBy
	@ManyToOne(fetch = FetchType.LAZY)
	private User lastModifiedBy;
	
	@Column
	private int noOfDownloads = 0;


	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	private User deActivatedBy;

	private DateTime deActivatedOn;

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	private User reActivatedBy;

	private DateTime reActivatedOn;

	public AtmaDocumentMaster() {
		super();
	}

	public AtmaDocumentMaster(ModuleMaster module, CategoryMaster category,
			String section, String documentName, String documenturlpath, String contentType, String link, String doctype, String title) {
		super();
		this.module = module;
		this.category = category;
		this.section = section;
		this.documentName = documentName;
		this.documenturlpath = documenturlpath;
		this.contentType = contentType;
		this.link = link;
		this.doctype = doctype;
		this.title = title;
	}

	public Long getDocumentId() {
		return documentId;
	}

	public void setDocumentId(Long documentId) {
		this.documentId = documentId;
	}

	public ModuleMaster getModule() {
		return module;
	}

	public void setModule(ModuleMaster module) {
		this.module = module;
	}

	public CategoryMaster getCategory() {
		return category;
	}

	public void setCategory(CategoryMaster category) {
		this.category = category;
	}

	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	public String getDocumentName() {
		return documentName;
	}

	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}

	public String getDocumenturlpath() {
		return documenturlpath;
	}

	public void setDocumenturlpath(String documenturlpath) {
		this.documenturlpath = documenturlpath;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}
	
	public DateTime getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(DateTime createdOn) {
		this.createdOn = createdOn;
	}

	public DateTime getLastModifiedOn() {
		return lastModifiedOn;
	}

	public void setLastModifiedOn(DateTime lastModifiedOn) {
		this.lastModifiedOn = lastModifiedOn;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}

	public User getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(User lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getDoctype() {
		return doctype;
	}

	public void setDoctype(String doctype) {
		this.doctype = doctype;
	}
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	public Long getSizeofFile() {
		return sizeofFile;
	}

	public void setSizeofFile(Long sizeofFile) {
		this.sizeofFile = sizeofFile;
	}

	public String getDocumentDescription() {
		return documentDescription;
	}

	public void setDocumentDescription(String documentDescription) {
		this.documentDescription = documentDescription;
	}

	public Date getCreatedDate() {
		return createdOn.toDate();
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public LifeStages getLifeStages() {
		return lifeStages;
	}

	public void setLifeStages(LifeStages lifeStages) {
		this.lifeStages = lifeStages;
	}

	public List<BluePrintImage> getBlueprintImages() {
		return blueprintImages;
	}

	public void setBlueprintImages(List<BluePrintImage> blueprintImages) {
		this.blueprintImages = blueprintImages;
	}

	public int getNoOfDownloads() {
		return noOfDownloads;
	}

	public void setNoOfDownloads(int noOfDownloads) {
		this.noOfDownloads = noOfDownloads;
	}

	public User getDeActivatedBy() {
		return deActivatedBy;
	}

	public void setDeActivatedBy(User deActivatedBy) {
		this.deActivatedBy = deActivatedBy;
	}

	public DateTime getDeActivatedOn() {
		return deActivatedOn;
	}

	public void setDeActivatedOn(DateTime deActivatedOn) {
		this.deActivatedOn = deActivatedOn;
	}

	public User getReActivatedBy() {
		return reActivatedBy;
	}

	public void setReActivatedBy(User reActivatedBy) {
		this.reActivatedBy = reActivatedBy;
	}

	public DateTime getReActivatedOn() {
		return reActivatedOn;
	}

	public void setReActivatedOn(DateTime reActivatedOn) {
		this.reActivatedOn = reActivatedOn;
	}


	public boolean isFlag() {
		return flag;
	}

	public void setFlag(boolean flag) {
		this.flag = flag;
	}

	public Date getActiveDocumentUploadedDate(){
		if(reActivatedOn != null){
			return reActivatedOn.toDate();
		}
		return createdOn.toDate();
	}

	public User getActiveDocumentUploadedByUser(){
		if(reActivatedBy != null){
			return reActivatedBy;
		}
		return createdBy;
	}
}
