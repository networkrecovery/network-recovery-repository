package com.astrika.common.service;

import com.astrika.common.model.mail.EmailData;


public interface SendMailService {
	public void sendMail(final EmailData data);
}
