package com.astrika.moduleusermanagement.service;





import java.io.IOException;
import java.util.List;

import com.astrika.common.exception.NoSuchUserException;
import com.astrika.common.model.Role;
import com.astrika.common.model.User;
import com.astrika.common.model.UserStatus;
import com.astrika.companymngt.model.company.CompanyMaster;
import com.astrika.companymngt.model.module.ModuleMaster;
import com.astrika.modulemngt.exception.DuplicateModuleException;
import com.astrika.moduleusermanagement.model.ModuleUser;


public interface ModuleUserService {

	public ModuleUser findByModuleuserId(long moduleuserId);

	public ModuleUser findByCompanyIdAndModuleId(Long companyId,long moduleId);

	public List<ModuleMaster> findByActive(boolean active);

	public List<User> findByRoleAndStatus(Role role, UserStatus status);

	public ModuleUser findByModuleId(long moduleId);

	public List<ModuleUser> findByUserId(long userId);

	public List<ModuleUser> findByCompanyCompanyId(long companyId);

	public ModuleUser save(ModuleUser moduleUser)throws NoSuchUserException ;

	public ModuleUser findByUserIdandModuleId(long userId, long moduleId);

	public ModuleUser saveSurveyResult(ModuleUser surveyResult)throws DuplicateModuleException;

	public ModuleUser update(ModuleUser moduleUser);

	public List<ModuleMaster> findByModuleModuleId(long moduleId);

	public List<Object[]> getAllByModuleIdAndRating();


	public void createLSSXlSheet(CompanyMaster company) throws IOException;

	public int totalUsersperModule(long moduleId);

	public int getmaxRating();

	public List<Object[]> getAllByModuleIdAndRatingByMonthYear(int mnth, int yr);

	public List<Object[]> getAvgOda();

}
