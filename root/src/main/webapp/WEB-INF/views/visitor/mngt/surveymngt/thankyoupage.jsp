
<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/page_head.jsp"%>

  <div class="blue-container">
            <div class="container">
                <h3>
                	<security:authorize ifAnyGranted="<%=Role.COMPANY_ADMIN.name()%>">
						<a class="light a-breadcrumb" href="<%=contexturl %>CompanyAdmin">Dashboard</a> &gt; 
					</security:authorize>
					<security:authorize ifAnyGranted="<%=Role.MODULE_USER.name()%>">
						<a class="light a-breadcrumb" href="<%=contexturl %>ModuleUser">Dashboard</a> &gt;  
					</security:authorize>
                
                </h3>
                <div class="row blue">
                    <div class="col-md-12  col-sm-12">
                        <div class="media">
                            <div class="contain">
                               
                            </div>
                        </div>
                    </div>
                </div>
            </div>
  </div>
</div>

<div class="container">
	<div class="row">
	
		<h1> Thank you for taking the Survey. You can check your report!</h1>
		<div class="col-md-3 col-sm-3 hidden-xs">
			<div class="adv">
				<img class="img-responsive"
					src="<%=contexturl%>resources/img/network.jpg" />
			</div>
			<div class="need-help">
				<img class="img-responsive"
					src="<%=contexturl%>resources/img/need help-3.jpg" />
			</div>
		</div>
	</div>
</div>

<div class="container">
        <div class="row">
        </div>
</div>

<%-- <script src="<%=contexturl%>resources/js/vendor/jquery-1.11.0.min.js"></script> --%>
<%-- <script src="<%=contexturl%>resources/js/vendor/bootstrap.min.js"></script> --%>
<script src="${pageContext.request.contextPath}/webjars/counter-webapp-frontend/0.0.1/vendor_bundle.js"></script>

 <%@include file="../../inc/page_footer.jsp"%>

