package com.astrika.common.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.astrika.common.exception.NoSuchUserException;
import com.astrika.common.model.Role;
import com.astrika.common.model.User;
import com.astrika.common.model.UserStatus;
import com.astrika.common.service.UserService;
import com.astrika.kernel.exception.CustomException;

public class CustomUserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	private UserService userService;
	
	private static final Logger LOGGER = Logger.getLogger(UserServiceImpl.class);

	private User user;

	public void setUser(User user) {
		this.user = user;
	}

	public UserDetails loadUserByUsername(String loginId)
			throws UsernameNotFoundException {
		User dbUser = null;
		try {
			dbUser = userService.findByLoginId(loginId);
		} catch (NoSuchUserException e) {
			LOGGER.info(e);
			throw new BadCredentialsException(
					CustomException.AUTHENTICATION_FAILURE.getCode()
					);
			
		}
		if (dbUser != null
				&& dbUser.getStatus().getId() == UserStatus.ACTIVE.getId()) {
			dbUser.setGrantedAuthorities(getRoles(dbUser));
		} 

		return dbUser;
	}

	private Collection<GrantedAuthority> getRoles(User user) {
		List<GrantedAuthority> authList = new ArrayList<>(1);
		Role role = user.getRole();
		if (role != null) {
			authList.add(new SimpleGrantedAuthority(role.name()));
		}
		return authList;
	}

}
