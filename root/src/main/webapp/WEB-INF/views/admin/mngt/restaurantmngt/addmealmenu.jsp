<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->

<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/template_start.jsp"%>
<%@include file="../../inc/page_head.jsp"%>

<link rel="stylesheet" type="text/css" href="<%=contexturl %>resources/css/jquery.cleditor.css" />
<style>
.cleditorToolbar {
	background: url('<%=contexturl %>resources/img/cleditor/toolbar.gif') repeat
}

.cleditorButton {
	float: left;
	width: 24px;
	height: 24px;
	margin: 1px 0 1px 0;
	background: url('<%=contexturl %>resources/img/cleditor/buttons.gif')
}

.cleditorMain {
	height: 250px !important
}
</style>

<div id="page-content">
	<div class="content-header">
		<div class="header-section">
			<h1>
				<i class="fa fa-map-marker"></i>
				<spring:message code="heading.meal" />
				<br> <small><spring:message code="heading.createmeals" />
				</small>
			</h1>
			<span id="errorMsg"></span>
		</div>
	</div>
	<ul class="breadcrumb breadcrumb-top">
		<li><spring:message code="heading.meal" />
		</li>
	</ul>
	<div class="block full" id="formView">
		<form action="SaveMealMenu" method="post" class="form-horizontal"
			enctype="multipart/form-data" id="meal_form">
		
			
			<div class="form-group">
				<input id="mealmenu_Id" name="mealmenuId" type="hidden" value="0">
				<input id="outlet_Id" name="outletId" type="hidden"
					value="${outletId}"> <label class="col-md-3 control-label"
					for="mealmenu_Name"><spring:message code="label.title" /><span class="text-danger">*</span>
				</label>
				<div class="col-md-9">
					<input id="mealmenu_Name" name="title" class="form-control"
						placeholder="<spring:message code="label.title"/>.."
						type="text">
				</div>
			</div>
			
			
			<div class="form-group">
				<label class="col-md-3 control-label" for="mealmenu_description"><spring:message
						code="label.description" /><span class="text-danger">*</span></label>
				<div class="col-md-9">
					<textarea id="mealmenu_description" name="description"
						class="form-control" rows="5"
						placeholder="<spring:message code="label.description"/>.."></textarea>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="mealmenu_description"><spring:message
						code="label.cost" /><span class="text-danger">*</span>${currency}</label>
				<div class="col-md-9">
					<input type="text" id="cost" name="cost" class="form-control"
						placeholder="<spring:message code="label.cost"/>.."/>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="mealmenu_image"><spring:message code="label.mealmenupicture"/>
					</label>
				<div class="col-md-9">
					<input type="file" name="photo" id="profile"
						accept="image/jpg,image/jpeg,image/png,image/gif,image/bmp,image/tiff"
						style="padding-top: 7px;" />
				</div>
			</div>
	
				

			<div class="form-group">
				<label class="col-md-3 control-label" for="mealmenu_termsCondition"><spring:message
						code="label.termsandcondition" /><span class="text-danger">*</span></label>
				<div class="col-md-9">
					<textarea id="mealmenu_termsCondition" name="termsandcondition" class="form-control"></textarea>
				</div>
			</div>

			<div class="form-group form-actions">
				<div class="col-md-9 col-md-offset-3">
					<button id="mealmenu_submit" type="submit"
						class="btn btn-sm btn-primary">
						<i class="fa fa-angle-right"></i> <spring:message code="button.save" />
					</button>
					<button type="reset" class="btn btn-sm btn-warning">
						<i class="fa fa-repeat"></i>
						<spring:message code="button.reset" />
					</button>
						<a class="btn btn-sm btn-primary" href="MealMenuList?outletId=${outletId}"><i class="gi gi-remove"></i>
							<spring:message code="button.cancel" /> </a>
				</div>
			</div>
		</form>
	</div>
</div>



<%@include file="../../inc/page_footer.jsp"%>
<%@include file="../../inc/template_scripts.jsp"%>
<script type="text/javascript" src="<%=contexturl %>resources/js/jquery.cleditor.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
 		$("#mealmenu_termsCondition").cleditor(); 
		

		$("#meal_form").validate(
				{	errorClass:"help-block animation-slideDown",
					errorElement:"div",
					errorPlacement:function(e,a){a.parents(".form-group > div").append(e)},
					highlight:function(e){$(e).closest(".form-group").removeClass("has-success has-error").addClass("has-error")},
					success:function(e){e.closest(".form-group").removeClass("has-success has-error")},
					rules:{ title:{required:!0,maxlength : 75},
						    cost:{required:!0,maxlength : 10,number  : !0},
							description:{required:!0,maxlength : 500},
							termsandcondition:{required:!0}
					},
					messages:{title:{required:'<spring:message code="validation.title"/>',
					                 maxlength :'<spring:message code="validation.mealmenutitle75character"/>',},
						     cost:{required:'<spring:message code="validation.cost"/>',
									required :'<spring:message code="validation.pleaseentermealcost"/>',
									maxlength :'<spring:message code="validation.mealcost10character"/>',
									number  :'<spring:message code="validation.digits"/>'},
							description:{required:'<spring:message code="validation.description"/>',
							             maxlength :'<spring:message code="validation.description500character"/>'},
							termsandcondition:{required:'<spring:message code="validation.termsandconditions"/>'}
					},
					
				});
		
		
		
		$("#mealmenu_submit").click(function(){
			 $("#meal_form").submit();
		});
		
	});
</script>
<%@include file="../../inc/template_end.jsp"%>