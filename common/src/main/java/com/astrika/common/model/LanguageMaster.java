package com.astrika.common.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.envers.Audited;

@Entity
@Audited
public class LanguageMaster {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long languageId;

	@Column(unique = true, nullable = false, length = 75)

	private String languageName;

	@Column(unique = true, nullable = false, length = 75)

	private String shortName;

	@Column(columnDefinition = "boolean default true", nullable = false)
	private boolean active = true;

	public LanguageMaster() {
		super();
	}

	public LanguageMaster(String name, String shortName) {
		super();
		this.languageName = name;
		this.shortName = shortName;
	}

	public long getLanguageId() {
		return languageId;
	}

	public void setLanguageId(long languageId) {
		this.languageId = languageId;
	}

	public String getLanguageName() {
		return languageName;
	}

	public void setLanguageName(String languageName) {
		this.languageName = languageName;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

}
