package com.astrika.forummngt.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.envers.Audited;
import org.joda.time.DateTime;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;

import com.astrika.common.model.User;


@Entity
@Audited
public class ForumComments {
		
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long forumCommentId;
	
	@Column
	private String comments;

	@JsonIgnore
	@CreatedBy
	@ManyToOne(fetch = FetchType.LAZY)
	private User createdBy;
	
	
	@Transient
	private Date createdDate;
	
	@CreatedDate
	private DateTime createdOn;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JsonIgnore
	private ForumMaster forum;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JsonIgnore
	private ForumComments comment;
	
	@Column(columnDefinition = "boolean default true", nullable = false)
	private boolean active = true;

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public Long getForumCommentId() {
		return forumCommentId;
	}

	public void setForumCommentId(Long forumCommentId) {
		this.forumCommentId = forumCommentId;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public DateTime getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(DateTime createdOn) {
		this.createdOn = createdOn;
	}

	public ForumMaster getForum() {
		return forum;
	}

	public void setForum(ForumMaster forum) {
		this.forum = forum;
	}

	public ForumComments getComment() {
		return comment;
	}

	public void setComment(ForumComments comment) {
		this.comment = comment;
	}
	
	
}
