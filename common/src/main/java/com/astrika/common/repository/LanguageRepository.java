package com.astrika.common.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.astrika.common.model.LanguageMaster;

public interface LanguageRepository extends JpaRepository<LanguageMaster, Long> {

	LanguageMaster findByShortName(String name);

	LanguageMaster findByLanguageName(String name);

	List<LanguageMaster> findByActiveTrue();
	
	List<LanguageMaster> findByActiveFalse();
}
