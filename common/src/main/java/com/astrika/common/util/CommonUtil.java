package com.astrika.common.util;

import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component
public class CommonUtil {

	private static final Pattern BCRYPT_PATTERN = Pattern.compile("\\A\\$2a?\\$\\d\\d\\$[./0-9A-Za-z]{53}");
	
	@Autowired
	private CacheManager cacheManager;
	
	public void deleteCache(String cacheName)
	{
		if(cacheManager!=null){
			Cache cache=cacheManager.getCache(cacheName);
			if(cache!=null)
				cache.clear();
		}
	}

	public Authentication getCurrentLoggedInUser(){
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (!(authentication instanceof AnonymousAuthenticationToken)) {
			return authentication;
		}
		return null;
	}

	public boolean isBcrypt(String val){
		return BCRYPT_PATTERN.matcher(val).matches();
	}

}
