package com.astrika.forummngt.exception;

import com.astrika.kernel.exception.BusinessException;
import com.astrika.kernel.exception.CustomException;

public class DuplicateForumException extends BusinessException{

	public DuplicateForumException() {
		super(CustomException.DUPLICATE_FORUM.getCode());
	}

	public DuplicateForumException(String errorCode) {
		super(errorCode);
	}
	

}
