/**
 * @author fbielejec
 */

var packageJSON = require('./package.json');
var webpack = require('webpack');
//var HtmlWebpackPlugin = require('html-webpack-plugin');
var path = require('path');
var ExtractTextPlugin = require("extract-text-webpack-plugin");

const PATHS = {
    build: path.join(__dirname, 'target', 'classes', 'META-INF', 'resources', 'webjars', packageJSON.name, packageJSON.version)
};

const sassLoaders = [
    'css-loader', 'postcss-loader', 'sass-loader?indentedSyntax=sass&includePaths[]=' + path.resolve(__dirname, './src')
]

module.exports = {

    entry: {
        app: "./src/app.js",
        vendor: [
            "jquery",
            "jquery-ui",
            "jquery-slimscroll",
            "magnific-popup",
            "chosen",
            "tagsInput",
            "easyPieChart",
            "jquery-placeholder", // jquery and plugins
            "rangy",
            "bootstrap",
            "bootstrap-slider",
            "bootstrap-datepicker",
            "bootstrap-timepicker",
            "wysihtml5", // bootstrap and plugins

        ]
    },

    output: {
        path: PATHS.build,
        // publicPath: '/assets/',
        filename: 'app_bundle.js'
    },

    module: {
        loaders: [
            {
                test: /\.css$/,
                // loader: ExtractTextPlugin.extract({
                //     fallbackLoader: "style-loader",
                    loader: "css-loader"
                // })
            }, {
                test: /\.sass$/,
                loader: ExtractTextPlugin.extract('style-loader', sassLoaders.join('!'))
            }, {
                test: /\.less$/,
                loaders: ['style', 'css', 'less']
            }, {
                test: /\.woff$/,
                loader: "url-loader?limit=10000&mimetype=application/font-woff&name=[path][name].[ext]"
            }, {
                test: /\.woff2$/,
                loader: "url-loader?limit=10000&mimetype=application/font-woff2&name=[path][name].[ext]"
            }, {
                test: /\.(eot|ttf|svg|gif|png|jpg)$/,
                loader: "file-loader"
            }, {
                test: /\.png$/,
                loader: 'url-loader?limit=10000'
            }
        ]
    },

    resolve: {
        modulesDirectories: ['node_modules'],
        alias: {
            // in aliases preffer src/ over dist/, webpack will do it's own optimization
            chosen: 'chosen-npm/public/chosen.jquery.js',
            slider: 'bootstrap-slider',
            tagsInput: "jquery-tags-input/src/jquery.tagsinput.js",
            easyPieChart: "easy-pie-chart/src/easypiechart.js",
            handlebars: 'handlebars/dist/handlebars.min.js',
            rangy: "rangy/lib/rangy-core.js",

            wysihtml5: "bootstrap3-wysihtml5-commonjs", // /src/bootstrap3-wysihtml5.js",
            // wysihtml5: path.join(__dirname, 'src/wysihtml5-0.3.0.min.js'),
        }

    },

    plugins: [
        new webpack.ProvidePlugin({$: "jquery", jQuery: "jquery", 'window.jQuery': "jquery", rangy: "rangy", "window.rangy": "rangy"}),
        new webpack.optimize.CommonsChunkPlugin(chunkName = "vendor", filename = "vendor_bundle.js"),
        // all css in one bundle
        // new ExtractTextPlugin({filename: "style_bundle.css", allChunks: true}),
        new ExtractTextPlugin('style_bundle.css'),
        // Minify the bundle
        new webpack.optimize.UglifyJsPlugin({
            mangle: true,
            beautify: false,
            compress: {
                // suppresses warnings, usually from module minification
                warnings: false
            }
        }),
        new webpack.DefinePlugin({
            'process.env': {
                'NODE_ENV': JSON.stringify('production')
            }
        }),

        //   new HtmlWebpackPlugin({
        //    title: "counter-webapp",
        //    template: __dirname + '/src/index.html',
        //    filename: 'index.html',
        //    inject: 'body'
        //  }),
    ]

};
