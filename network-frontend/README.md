### .:: Network-Frontend ::. ###

## Frontend for [ATMA Network](http://http://atmanetwork.in) ##

# DEV #

```
#!bash
  npm install 
  npm start
```  
Point your browser to http://localhost:8080/root

# DIST #

```
#!bash
  mvn clean install
```

# TODO #
- add jquery.bxslider and ion-rangeSlider to vendor_bundle
- perhaps all css styles could be bundled together
- perhaps all inline jquery script in JSPs could also be bundled with app_bundle
