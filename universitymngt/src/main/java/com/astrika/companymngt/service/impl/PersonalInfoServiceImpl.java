package com.astrika.companymngt.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.astrika.companymngt.model.personaldetails.PersonalInfo;
import com.astrika.companymngt.repository.PersonalInfoRepository;
import com.astrika.companymngt.service.PersonalInfoService;

@Service
public class PersonalInfoServiceImpl implements PersonalInfoService{

	@Autowired
	private PersonalInfoRepository personalInfoRepository;
	
	@Override
	public PersonalInfo findById(Long userId) {
		return personalInfoRepository.findById(userId);
	}

	@Override
	public PersonalInfo save(PersonalInfo personalInfo) {
		return personalInfoRepository.save(personalInfo);
	}

}
