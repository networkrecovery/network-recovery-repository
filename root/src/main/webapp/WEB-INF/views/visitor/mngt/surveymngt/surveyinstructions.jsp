<%@page import="com.astrika.common.model.Role"%>
<%@page import="com.astrika.common.model.User"%>

<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/page_head.jsp"%>


  <div class="blue-container">
            <div class="container">
                <h3 class="atma-breadcrumbs">
                	<security:authorize ifAnyGranted="<%=Role.COMPANY_ADMIN.name()%>">
						<a class="light a-breadcrumb" href="<%=contexturl %>CompanyAdmin">Dashboard</a> &gt;<a class="light a-breadcrumb" href="">Instructions</a>
					</security:authorize>
					<security:authorize ifAnyGranted="<%=Role.MODULE_USER.name()%>">
						<a class="light a-breadcrumb" href="<%=contexturl %>ModuleUser">Dashboard</a> &gt;<a class="light a-breadcrumb" href="">Instructions</a>
					</security:authorize>
				</h3>
                <div class="row blue">
                    <div class="col-md-12  col-sm-12">
                        <div class="media">
                            <div class="contain">
                               <h3 class="light text-white">Welcome to Life Stage Survey!</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
  </div>
</div>


    <div class="container">
        <div class="row">
<!--             <div class="col-md-12"> -->
<!--                 <ol class="breadcrumb"> -->
<!--                     <li><a href="#">Home</a></li> -->
<!--                     <li><a href="#">Library</a></li> -->
<!--                     <li class="active">Data</li> -->
<!--                 </ol> -->
<!--             </div> -->
        </div>
        <div class="row">
		<div class="col-md-9 col-sm-9">
			<div class="row">
				
				<p>This assessment tool will help you gain deeper insight into where your organisation currently is and where its needs to go<br>
				
				Research shows that most organisations advance through a common path of five "life stages" as they are formed, become organised, grow, change and expand<br>
				
				The <strong>Atma Life Stages Survey</strong> covers the ten keyareas of organisational development and functioning:<br>
				<div class="row">
					<div class = "col-md-3">
					<ul>
						<li><strong>Governance</strong></li>
						<li><strong>Administrative Systems</strong></li>
						<li><strong>Fundraising</strong></li>
						<li><strong>Marketing</strong></li>
					</ul>
					</div>
					<div class = "col-md-3">
					<ul>
						<li><strong>Leadership</strong></li>
						<li><strong>Monitoring and Evaluation</strong></li>
						<li><strong>Staffing</strong></li>
					</ul>
					</div>
					<div class = "col-md-3">
					<ul>
						<li><strong>Financial Management</strong></li>
						<li><strong>Organisational Development</strong></li>
						<li><strong>Programmes & Services</strong></li>
					</ul>
					</div>
				</div>
				<br>
				<p>The Life Stage Survey is designed to tell you how your organisation is performing across these areas. Based on your answers, the survey will identify your life stage for each area.<br>
				No life stage is better than any another; it simply describes where you are as an organisation. Knowing your stage can help you understand <strong>areas of strength</strong> that you can leverage.  It also 
				highlights <strong>areas you could improve</strong> and suggest strategies you might want to take on as next steps.<br>
				The <strong>leader of the organisation</strong> should take this Life Stage Survey, ideally in cooperation with her or his team. The team should include other decision makers in the organisation-for example managers or board members- to help get the most well-rounded and objective responses.<br>
				</p>
				<p><strong>What will you get?</strong><br></p>
				<ul>
					<li>You will find out <strong>your life stage in each of the 10 key areas</strong> of organisational development.</li>
					<li>Based on your scores, you will be given a <strong>customized profile</strong> of your stage and <strong>suggestions for next steps based on best practices.</strong></li>
					<li><strong>Links to modules in the Network</strong> that are aligned to your life stage and can help you <strong>get started</strong> on your areas to be developed.</li>
					<li>By taking the survey on an annual or quarterly basis you will be able to <strong>clearly see and track how your organisation is growing.</strong></li>
					<li>You can <strong>share your report</strong> internally with your team as well as externally with the stakeholders of your organisation</li>
				</ul>
				<p><strong>What you will not get:</strong><br>
					This life stage survey <strong>will not give you an overall rating as an organisation.</strong><br>
					Organisations are multi-dimensional, just like people, and you know your organisation in a deep way that a computerized program cannot.<br>
					However when reading the Survey results you may notice a pattern in your life stages across the organisational areas. If it helps you to assess your overall life stage, you can observe this pattern and use it to choose for yourself the life stage that seems closest to where your organisation is. We encourage you to take the information that the Life Stage Survey gives you and use it whatever way is most meaningful to you.<br>    
				</p>
				
				
				<form action="<%=contexturl%>ManageSurvey/TakeSurvey/${moduleList[0].moduleId}" method="post"
					class="form-horizontal ui-formwizard" id="Comment_form">
					<button id="takeSurvey" type="submit" class="pull-right btn btn-sm btn-green save">Take
						Survey</button><br>
					<span class="clearfix"></span>
				</form>
			</div>
		</div>
		<div class="col-md-3 col-sm-3 hidden-xs">
                <div class="adv"><img class="img-responsive" src="<%=contexturl %>resources/img/AtmaNetwork.jpg" />
                </div>
                <c:choose>
					<c:when test = "${empty atmaContactImage.contactImage}">
						 <div class="need-help"><img class="img-responsive" src="<%=contexturl %>resources/img/need help-3.jpg" /><br><br></div>
					</c:when>
					<c:otherwise>
						 <div class="need-help"><img class="img-responsive" src="<%=contexturl%>${atmaContactImage.contactImage.imagePath}" /><br><br></div>
					</c:otherwise>
				</c:choose>
            </div>
        </div>
    </div>
 
<%--  <script src="<%=contexturl%>resources/js/vendor/jquery-1.11.0.min.js"></script> --%>
<%-- <script src="<%=contexturl%>resources/js/vendor/bootstrap.min.js"></script> --%>
<script src="${pageContext.request.contextPath}/webjars/counter-webapp-frontend/0.0.1/vendor_bundle.js"></script>

 <%@include file="../../inc/page_footer.jsp"%>