package com.astrika.surveymngt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.astrika.surveymngt.model.SurveyManager;
import com.astrika.surveymngt.model.SurveyStatus;

public interface SurveyManagerRepository extends JpaRepository<SurveyManager, Long>{
		
	@Query("select s from SurveyManager s where s.company.companyId=?1")
	SurveyManager findByCompanyId(long companyId);
	
	@Query("select s from SurveyManager s where s.company.companyId = ?1 and s.ongoingSurvey = ?2")
	SurveyManager findByCompanyIdandOngoingSurvey(long companyId, boolean ongoingSurvey);
	
	@Query("select s from SurveyManager s where s.company.companyId = ?1 and s.surveyStatus = ?2")
	SurveyManager findByCompanyIdandSurveyStatus(long companyId, SurveyStatus status);
	
	@Query("select s from SurveyManager s left join fetch s.company where s.surveyManagerId = ?1")
	SurveyManager findBySurveyManagerId(long surveyManagerId);

	@Query("select s from SurveyManager s left join s.company where s.company.companyId in (?1)")
	List<SurveyManager> findSurveyManagerByCompanyIds(List<Long> ids);

	@Query("select s from SurveyManager s left join s.company where s.surveyPublished in (" +
			"select max(ss.surveyPublished) from SurveyManager ss where ss.company.companyName = s.company.companyName " +
			") and  s.company.companyId in (?1)")
	List<SurveyManager> findLatestSurveyManagerByCompanyIds(List<Long> ids);

	@Query("select s from SurveyManager s left join s.company where s.surveyPublished in (" +
			"select max(ss.surveyPublished) from SurveyManager ss where ss.company.companyName = s.company.companyName " +
			"and ss.surveyStatus = ?2" +
			") and  s.company.companyId in (?1)")
	List<SurveyManager> findLatestSurveyManagerByCompanyIdsAndStatus(List<Long> ids,SurveyStatus status);

	@Query("select count(s.company.companyId) from SurveyManager s")
	int getNoOfCompaniesTakenSurvey();

	@Query("select count(s.company.companyId) from SurveyManager s where s.totalModules=s.totalModulesCompleted")
	int getNoOdSurveyComplited();

	@Query("select s.company.companyName,s.totalModules,s.totalModulesCompleted from SurveyManager s where s.totalModules!=s.totalModulesCompleted")
	List<Object[]> getIncompliteLss();
}
