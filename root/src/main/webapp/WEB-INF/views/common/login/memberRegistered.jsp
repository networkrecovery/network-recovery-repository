
<%@include file="/WEB-INF/views/admin/inc/config.jsp"%>
<%@include file="/WEB-INF/views/admin/inc/template_start.jsp"%>
 
<div id="page-container" class="sidebar-partial sidebar-visible-lg sidebar-no-animations style-alt footer-fixed">
 			<!-- START Header -->	
			<header class="navbar navbar-inverse">
	          <!-- Navbar Header -->
                    <div class="navbar-header">
					<!-- Main Sidebar Toggle Button -->
                        <ul class="nav navbar-nav-custom">
                           
                                 <!-- Brand -->
								<a href="Index" class="sidebar-brand-logo">
									<img src="<%=contexturl %>resources/img/logo.png">
									
								</a>
								
								<!-- END Brand -->
                           
                        </ul>
<!--                         <ul> -->
<!--                           <a class="btn btn-sm btn-primary" href="Login"> -->
<!-- 								  <i class="gi gi-remove"></i>Back to Login Page  -->
<!-- 				             </a> -->
<!--                         </ul> -->
					</div>
</div>
<div id="page-content">
	<div class="content-header">
		<div class="header-section">
			<c:if test="${!empty success}">
				<div class="alert alert-success alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-check-circle"></i>
						<spring:message code="label.success" />
					</h4>
					<spring:message code="${success}" /><br>
					<a class="btn btn-sm btn-primary" href="Login">
								  <i class="gi gi-remove"></i> <spring:message code="heading.backtologinpage"/>
				   </a>
					
				</div>
			</c:if>
			<c:if test="${!empty error}">
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-times-circle"></i>
						<spring:message code="label.error" />
					</h4>
					<spring:message code="${error}" />
				</div>
			</c:if>
<!-- 			<h1> -->
<%-- 				<i class="fa fa-map-marker"></i> <spring:message code="heading.memberregistration"/><br><small></small> --%>
<!-- 			</h1> -->
			<span id="errorMsg"></span>
		</div>
	</div>
	<form action="" method="post" class="form-horizontal form-bordered"
		id="user_form">
		<div class="form-group form-actions">
			<div class="col-md-9 col-md-offset-3">
			   Continue with free Membership
				<div id="paid_user" class="btn btn-sm btn-primary">
					<i class="fa fa-angle-right"></i>
				 <a href="Index">   Go</a>
				</div>
				
				   Do you want to be paid member
				<div id="currency_popup_window" class="btn btn-sm btn-primary">
					<i class="fa fa-angle-right"></i>
				   <a href="#currency_popup"  data-toggle="modal"> Go</a> 
				</div>
				
				
			</div>
		</div>
	</form>
</div>

<div id="currency_popup" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title"><i class="fa fa-pencil"></i> <spring:message code="label.settings"/></h2>
            </div>
            <div class="modal-body">
				<form action="Member/SaveCurrencyInSession" method="post" id="currency_form"	class="form-horizontal form-bordered">
                    <fieldset>
                        <legend>Select Currency</legend>
                        <div class="form-group">
							<label class="col-md-3 control-label" for="currency"><spring:message
									code="label.currency" /><span class="text-danger">*</span> </label>
							<div class="col-md-6">
								<select class="currency_chosen" style="width: 200px;"
									id="currency" name="currencyId">
									<c:forEach items="${currencyList}" var="currency">
										<c:choose>
											<c:when test="${currency.active == true}">
												<option value="${currency.currencyId}" >${currency.currencyName}</option>
											</c:when>
											<c:otherwise>
												<option value="placeholder">..</option>
											</c:otherwise>
										</c:choose>
									</c:forEach>
								</select>
							</div>
						</div>
                    </fieldset>
                    <div class="form-group form-actions">
                        <div class="col-xs-12 text-right">
                            <button type="button" class="btn btn-sm btn-default" data-dismiss="modal" onClick="window.location.reload()"><spring:message code="label.close"/></button>
							<div id="currency_submit" class="btn btn-sm btn-primary"><spring:message code="label.savechanges"/></div>
                        </div>
                    </div>
                </form>
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>

<%-- <%@include file="/WEB-INF/views/visitor/inc/page_footer.jsp"%> --%>
<%@include file="/WEB-INF/views/admin/inc/template_scripts.jsp"%>



<script type="text/javascript">
	$(document)
			.ready(
					function() {
					
					
						$(".currency_chosen").chosen();	

						// 		$("#cancel").click(function(){
						// 			$.ajax({
						// 				type: "GET",
						// 				async: false,
						// 				url:"",				
						// 				success: function(data, textStatus, jqXHR){
						// 					$('#page-content').html(data);
						// 				},
						// 				dataType: 'html'
						// 			});
						// 		});

					
		
		
		
		
		$("#currency_submit").click(function(){
			$("#currency_form").submit();
		});

	});
</script>
<style>
.chosen-container {
	width: 250px !important;
}
</style>

<%@include file="/WEB-INF/views/admin/inc/template_end.jsp"%>