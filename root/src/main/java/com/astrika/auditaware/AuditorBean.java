package com.astrika.auditaware;

import org.apache.log4j.Logger;
import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.context.SecurityContextHolder;

import com.astrika.common.model.User;


public class AuditorBean implements AuditorAware<User> {

    private static final Logger LOGGER = Logger.getLogger(AuditorBean.class);
    
	public User getCurrentAuditor() {

		User user = null;
		try{
			user = (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		}
		catch(Exception e){
			user = null;
			LOGGER.info(e);
		}

	    return user;
	}

}
