<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/template_start.jsp"%>
<%@include file="../../inc/page_head.jsp"%>



<div id="page-content">
	<div class="content-header">
		<div class="header-section">
			<h1>
				<i class="fa fa-map-marker"></i>
				<spring:message code="heading.instructor" />
				<br> <small><spring:message
						code="heading.specifylistofinstructors" /> </small>
			</h1>
			<c:if test="${!empty success}">
				<div class="alert alert-success alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-check-circle"></i>
						<spring:message code="label.success" />
					</h4>
					<spring:message code="${success}" />
				</div>
			</c:if>
			<div class="alert alert-danger alert-dismissable"
				style="display: none" id="error">
				<button type="button" class="close" data-dismiss="alert"
					aria-hidden="true">x</button>
				<h4>
					<i class="fa fa-times-circle"> </i> <spring:message code="label.error"/>
				</h4>
				<span id="errorMsg"></span>
			</div>
		</div>
	</div>
	<ul class="breadcrumb breadcrumb-top">
		<li><spring:message code="heading.manageinstructor" /></li>
		<li><a href="#"><spring:message code="heading.instructor" /></a></li>
	</ul>
	<div class="block full  gridView">
		<div class="block-title">
			<h2>
				<strong><spring:message code="heading.activeinstructor" /></strong>
			</h2>
		</div>
		<a href="<%=contexturl %>ManageInstructor/InstructorList/AddInstructor" id="addInstructor" class="btn btn-sm btn-primary"><spring:message
				code="label.addInstructor" /></a>
		<div class="table-responsive">
			<table id="active-datatable"
				class="table table-vcenter table-condensed table-bordered">
				<thead>
					<tr>
						<th class="text-center"><spring:message code="label.id" /></th>
						<th class="text-center"><spring:message code="label.name" /></th>
						<th class="text-center"><spring:message code="label.email" /></th>
						<th class="text-center"><spring:message code="label.department" /></th>
						<security:authorize ifAnyGranted="<%=Role.SKILLME_ADMIN.name() %>">
						<th class="text-center"><spring:message code="label.institutionname" /></th>
						</security:authorize>
						<security:authorize ifAnyGranted="<%=Role.UNIVERSITY_ADMIN.name() %>">
						<th class="text-center"><spring:message code="label.designation" /></th>
						</security:authorize>
						<th class="text-center"><spring:message code="label.mobile" /></th>
						<th class="text-center"><spring:message code="label.actions" /></th>
					</tr>
				</thead>
				<tbody id="tbody">
					<c:forEach var="currentInstructor" items="${activeUsers}"
						varStatus="count">

						<tr>
							<td class="text-center">${count.count}</td>
							<td class="text-left"><c:out
									value="${currentInstructor.user.fullName}" /></td>
							<td class="text-left"><c:out
									value="${currentInstructor.user.emailId}" /></td>
							<td class="text-left"><c:out
									value="${currentInstructor.department.departmentName}" /></td>
							<security:authorize ifAnyGranted="<%=Role.SKILLME_ADMIN.name() %>">
							<td class="text-left"><c:out
									value="${currentInstructor.university.universityName}" /></td>
							</security:authorize>
							<security:authorize ifAnyGranted="<%=Role.UNIVERSITY_ADMIN.name() %>">
							<td class="text-left"><c:out
									value="${currentInstructor.designation}" /></td>
							</security:authorize>
							<td class="text-left"><c:out
									value="${currentInstructor.user.mobile}" /></td>
							<td class="text-center">
								<div class="btn-group">
								
									<a href="<%=contexturl %>ManageInstructor/InstructorList/EditInstructor?id=${currentInstructor.userId}"
										data-toggle="tooltip" title="Edit"
										class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>
									<a href="#delete_company_popup" data-toggle="modal"
										onclick="deleteCompany(${currentInstructor.userId})"
										title="Delete Instructor" class="btn btn-xs btn-danger"><i
										class="fa fa-times"></i></a>
								</div>
								
								
							</td>
						</tr>
					</c:forEach>

				</tbody>
			</table>
		</div>
	</div>


	<div class="block full  gridView">
		<div class="block-title">
			<h2>
				<strong><spring:message code="heading.inactiveinstructor" /></strong>
			</h2>
		</div>
		<div class="table-responsive">
			<table id="inactive-datatable"
				class="table table-vcenter table-condensed table-bordered ">
				<thead>
					<tr>
						<th class="text-center"><spring:message code="label.id" /></th>
						<th class="text-center"><spring:message code="label.name" /></th>
						<th class="text-center"><spring:message code="label.email" /></th>
						<th class="text-center"><spring:message code="label.department" /></th>
						<security:authorize ifAnyGranted="<%=Role.SKILLME_ADMIN.name() %>">
						<th class="text-center"><spring:message code="label.institutionname" /></th>
						</security:authorize>
						<security:authorize ifAnyGranted="<%=Role.UNIVERSITY_ADMIN.name() %>">
						<th class="text-center"><spring:message code="label.designation" /></th>
						</security:authorize>
						<th class="text-center"><spring:message code="label.mobile" /></th>
						<th class="text-center"><spring:message code="label.actions" /></th>
					</tr>
				</thead>
				<tbody id="tablebody">
					<c:forEach var="currentInstructor" items="${inactiveUsers}"
						varStatus="count">
						<tr>
							<td class="text-center">${count.count}</td>
							<td class="text-left"><c:out
									value="${currentInstructor.user.fullName}" /></td>
							<td class="text-left"><c:out
									value="${currentInstructor.user.emailId}" /></td>
							<td class="text-right"><c:out
									value="${currentInstructor.department.departmentName}" /></td>
							<security:authorize ifAnyGranted="<%=Role.SKILLME_ADMIN.name() %>">
							<td class="text-left"><c:out
									value="${currentInstructor.university.universityName}" /></td>
							</security:authorize>
							<security:authorize ifAnyGranted="<%=Role.UNIVERSITY_ADMIN.name() %>">
							<td class="text-left"><c:out
									value="${currentInstructor.designation}" /></td>
							</security:authorize>
							<td class="text-right"><c:out
									value="${currentInstructor.user.mobile}" /></td>
							<td class="text-center">
								<div class="btn-group">
									<a href="#restore_Company_popup" data-toggle="modal"
										onclick="restoreCompany(${currentInstructor.userId})"
										title="Restore Instructor" class="btn btn-xs btn-danger"><i
										class="fa fa-times"></i> </a>
								</div>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
	<div id="delete_company_popup" class="modal fade" tabindex="-1"
		role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<form action="<%=contexturl %>ManageInstructor/InstructorList/DeleteInstructor" method="post" class="form-horizontal form-bordered"
						id="delete_company_Form">
						<div style="padding: 10px; height: 110px;">
							<label><spring:message
									code="validation.deleteInstructor" /></label>
							<div class="col-xs-12 text-right">
								<button type="button" class="btn btn-sm btn-default"
									data-dismiss="modal">
									<spring:message code="label.no" />
								</button>
								<div id="delete_company" class="btn btn-sm btn-primary">
									<spring:message code="label.yes" />
								</div>
							</div>
						</div>
						<input type="hidden" name="id" id="deletecompanyId">

					</form>

				</div>
			</div>
		</div>
	</div>


	<div id="restore_Company_popup" class="modal fade" tabindex="-1"
		role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<form action="<%=contexturl %>ManageInstructor/InstructorList/RestoreInstructor" method="post" class="form-horizontal form-bordered" id="restore_company_Form">
						
						<div style="padding: 10px; height: 110px;">
							<label><spring:message
									code="validation.restoreInstructor" /></label>
							<div class="col-xs-12 text-right">
								<button type="button" class="btn btn-sm btn-default"
									data-dismiss="modal"><spring:message code="label.no" /></button>
								<div id="restore_Company" class="btn btn-sm btn-primary">	<spring:message code="label.yes" /></div>
							</div>
						</div>
						<input type="hidden" name="id" id="restorecompanyId">
					</form>
				</div>
			</div>
		</div>
	</div>

</div>

<%@include file="../../inc/page_footer.jsp"%>
<%@include file="../../inc/template_scripts.jsp"%>
<script
	src="<%=contexturl%>resources/js/pages/activeTablesDatatables.js"></script>
<script
	src="<%=contexturl%>resources/js/pages/inactiveTablesDatatables.js"></script>
<script>$(function(){ ActiveTablesDatatables.init(6); });</script>
<script>$(function(){ InActiveTablesDatatables.init(6); });</script>
<script type="text/javascript">
	var selectedId = 0;
	var restoreId=0;
	$(document).ready(function() {
		
        $("#restore_Company").click(function(){
			
			$("#restorecompanyId").val(restoreId);
			$("#restore_company_Form").submit();
		});
		
		
		$("#delete_company").click(function(){
			
			$("#deletecompanyId").val(selectedId);
			$("#delete_company_Form").submit();
		});
	});
	
	function deleteCompany(id){
		selectedId = id;
	}
	function restoreCompany(id){
		restoreId = id;
	}
</script>
<%@include file="../../inc/template_end.jsp"%>