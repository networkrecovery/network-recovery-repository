package com.astrika.common.service;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import com.astrika.common.exception.DuplicateEmailException;
import com.astrika.common.exception.DuplicateLoginException;
import com.astrika.common.exception.NoSuchUserException;
import com.astrika.common.model.Role;
import com.astrika.common.model.User;
import com.astrika.common.model.UserStatus;

/**
 * Service interface for User.
 * 
 * @author Priyanka
 */

public interface UserService {

	User save(User user);

	void sendRegstrationMail(User user, String compName, String url, String tempPassword)
			throws DuplicateEmailException;

	User update(User user);

	public List<User> findByStatus(UserStatus status);

	User findById(long userId) throws NoSuchUserException;

	User findByLoginId(String loginId) throws NoSuchUserException;

	User findByEmailId(String emailId) throws NoSuchUserException;

	User findByModuleName(String moduleName) throws NoSuchUserException;

	User findByPasswordToken(String token) throws NoSuchUserException;

	User changePassword(long userId, String password, String cPassword)
			throws NoSuchUserException;

	User resetMemberPassword(long userId, String oldpassword, String password,
			String confirmPassword) throws NoSuchUserException;

	User findByToken(String token) throws NoSuchUserException;

	public User changeAccountInfo(long userId, String userEmail,
			String oldPassword, String newPassword, String rePassword)
			throws NoSuchUserException;

	public void updateAll(List<User> userList);

	public List<User> findByRoleAndStatus(Role role, UserStatus status);

	public List<User> findByRoleAndStatusAndUser(Role role, UserStatus status,
			long userId);

	public List<User> findByDesignation(String designation, UserStatus status);

	User activateUser(long userId) throws NoSuchUserException;

	User inActivateUser(long userId) throws NoSuchUserException;

	User saveUser(User user) throws DuplicateEmailException;

	User updateUser(long userId, String firstName, String lastName,
			String email, String mobile) throws DuplicateLoginException;

	User save(String firstName, String lastName, String loginId,
			String password, String emailId, String mobile, Long cityId,
			Long stateId, Long countryId, String pincode, String address,
			String gender, Date birthdayDate) throws DuplicateEmailException;
			

	User updateUser1(long userId, String firstName, String lastName,
			String designation, String description,
			boolean enquiryAccessRights)
			throws DuplicateLoginException;

	User save(String firstName, String lastName, String emailId,
			String loginId, String password, String designation, int role,
			Date birthdayDate, String mobile, long moduleId, String moduleName)
			throws NoSuchUserException;

	User update(long userId, String firstName, String lastName, String emailId,
			Date birthdayDate, String mobile) throws NoSuchUserException;

	public User findByMaxLastLogin(Long companyId);
	
	public int totalNoOfUsers(Role role);
	
	public int totalNoOfActiveUsers(Role role, UserStatus status);	
	
	public int getActiveTotalUsers(Role companyUser,Role moduleUser,UserStatus status);
	
	public int noOfUsersProfileReport(int start, int end);

	public int noOfInactiveUser(String date, int a);
	
	public List<Object[]> inactiveUserProfile(int a);
	
	public int noOfInactiveUserGreaterThanSixMonth(String date, int a);
	
	public int totalActiveUsers(UserStatus status);
   
	public int noOfComProfileReport(int start, int end,Role role);

	int fingLastMonthActiveUsers(Role companyAdmin, Role moduleUser);
	
	int activeUsersMoreThanOneMonth(Role companyAdmin, Role moduleUser);

	int getFullProfileComplete();

	int getTotalUser();
	
	int getTUser(int yr);

	public List<Object[]> getInactiveUserList(long totalU, int yr2,Role companyAdmin, Role moduleUser);
	
	long getInactiveUserOfThatMonth( long totalUserUptoMonth,int yr, int x);
	

	int getInactiveUserListExcel(long totaUser, int year, Role companyAdmin,
			Role moduleUser, int month);

	int getTotalNoOfUser();

	int getaciveInSecondLastMonthUsers();

	int getLastAddedUserCount();

	List<Object[]> getLastAddedUser();

	List<User> getInactiveFlagLastUser();

	List<Object[]> getAllUsers();

	String getUserOfIncompleteProfile() throws IOException;
	
	List<Object[]> getUserOfInactiveProfile();

	int getlastMonthCreatedUser(Role companyAdmin, Role moduleUser);

	int secondLastMonthCreatedUser(Role companyAdmin, Role moduleUser);
	
	public void transformPasswordFromClearToHash();

	int noOfInactiveUserExcel(int i);

	long getTotaluserUptomonth(Date startDate);

	


	

	



}