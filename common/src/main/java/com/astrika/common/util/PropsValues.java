/**
 * Copyright (c) 2014 Astrika, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.astrika.common.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author Priyanka
 */
@Component("propsValues")
public class PropsValues {

    @Value("#{portalProperties['session.cookie.domain']}")
    public String sessionCookieDomain;

    @Value("#{portalProperties['session.enable.persistent.cookies']}")
    public boolean sessionEnablePersistentCookies;

    @Value("#{portalProperties['session.test.cookie.support']}")
    public boolean sessionTestCookieSupport;

    @Value("#{portalProperties['session.timeout']}")
    public int sessionTimeout;

    @Value("#{portalProperties['session.timeout.auto.extend']}")
    public boolean sessionTimeoutAutoExtend;

    @Value("#{portalProperties['session.timeout.redirect.on.expire']}")
    public boolean sessionTimeoutRedirectOnExpire;

    @Value("#{portalProperties['session.timeout.warning']}")
    public int sessionTimeoutWarning;

    @Value("#{portalProperties['captcha.engine.simplecaptcha.background.producers']}")
    public String captchaEngineSimplecaptchaBackgroundProducers;

    @Value("#{portalProperties['captcha.engine.simplecaptcha.gimpy.renderers']}")
    public String captchaEngineSimplecaptchaGimpyRenderers;

    @Value("#{portalProperties['captcha.engine.simplecaptcha.height']}")
    public int captchaEngineSimplecaptchaHeight;

    @Value("#{portalProperties['captcha.engine.simplecaptcha.noise.producers']}")
    public String captchaEngineSimplecaptchaNoiseProducers;

    @Value("#{portalProperties['captcha.engine.simplecaptcha.text.producers']}")
    public String captchaEngineSimplecaptchaTextProducers;

    @Value("#{portalProperties['captcha.engine.simplecaptcha.width']}")
    public int captchaEngineSimplecaptchaWidth;

    @Value("#{portalProperties['captcha.engine.simplecaptcha.word.renderers']}")
    public String captchaEngineSimplecaptchaWordRenderers;

    @Value("#{portalProperties['captcha.max.challenges']}")
    public int captchaMaxChallenges = 1;

    @Value("#{portalProperties['pagination.start']}")
    public int paginationSart;

    @Value("#{portalProperties['pagination.end']}")
    public int paginationEnd;

    // Image editing properties
    @Value("#{imageProperties['image.validateAspectRatio']}")
    public boolean imageValidateAspect;

    @Value("#{imageProperties['image.isWatermarkToApply']}")
    public boolean imageWatermarkToApply;

    @Value("#{imageProperties['image.validateSize']}")
    public boolean imageValidateForComress;

    @Value("#{imageProperties['image.isChangeExtention']}")
    public boolean imageChangeExtention;

    @Value("#{imageProperties['image.createThumbnail']}")
    public boolean imageCreateThumbnail;

    @Value("#{imageProperties['image.aspectRatioValueCompare']}")
    public String imageAspectRatioValueCompare;

    @Value("#{imageProperties['image.aspectRatioValueCompareOperator']}")
    public String imageAspectRatioValueCompareOperator;

    // image change values

    @Value("#{imageProperties['image.aspectRatioValue']}")
    public String imageAspectRatioValueChange;

    @Value("#{imageProperties['image.watermarkValue']}")
    public String imageWatermarkValue;

    @Value("#{imageProperties['image.compressValue']}")
    public long imageCompressValue;

    @Value("#{imageProperties['tempCsvDownloadpath']}")
    public String tempCsvDownloadpath;



    @Value("#{imageProperties['image.compressQuality']}")
    public float imageCompressQuality;

    @Value("#{imageProperties['image.extentionValue']}")
    public String imageExtentionValue;

    @Value("#{imageProperties['image.copyTemp']}")
    public String imageCopyTempLocation;

    @Value("#{imageProperties['image.copyDest']}")
    public String imageCopyDestLocation;

    // example 200 x 100 i.e 200 width and 100 height
    @Value("#{imageProperties['image.thumbnailValues']}")
    public String imageThumbnailValues;

    @Value("#{imageProperties['image.saveRootDirName']}")
    public String imageSaveRootDir;

    @Value("#{portalProperties['email.memberauthentication']}")
    public String memberAuthentication;

    @Value("#{portalProperties['email.forgotpassword']}")
    public String forgotPassword;

    @Value("#{portalProperties['email.forgotlogin']}")
    public String forgotLogin;

    @Value("#{portalProperties['email.resetPasswordForInstructor']}")
    public String resetPasswordForInstructor;

    @Value("#{portalProperties['email.restAdminDetailsMail']}")
    public String restaurantAdminDetailsMail;

    @Value("#{portalProperties['email.resetPasswordForAtmaAdmin']}")
    public String resetPasswordForAtmaAdmin;

    @Value("#{portalProperties['email.resetPasswordForModuleUser']}")
    public String resetPasswordForModuleUser;

    @Value("#{portalProperties['email.moduleUserDetailsMail']}")
    public String moduleUserDetailsMail;

    @Value("#{portalProperties['email.ngoDetailsMail']}")
    public String ngoDetailsMail;

    @Value("#{portalProperties['email.ngoEnquiryDetailsMail']}")
    public String ngoEnquiryDetailsMail;

    @Value("#{portalProperties['email.ngoRejectionDetailsMail']}")
    public String ngoRejectionDetailsMail;

    @Value("#{portalProperties['email.ngoRejectionEnquiryDetailsMail']}")
    public String ngoRejectionEnquiryDetailsMail;


    @Value("#{portalProperties['email.resetPasswordForCorporateAdmin']}")
    public String resetPasswordForCorporateAdmin;

    @Value("#{portalProperties['email.companyAdminDetailsMail']}")
    public String companyAdminDetailsMail;

    @Value("#{portalProperties['email.corporateAdminDetailsMail']}")
    public String corporateAdminDetailsMail;

    @Value("#{portalProperties['email.resetPasswordForCompanyAdmin']}")
    public String resetPasswordForCompanyAdmin;

    @Value("#{portalProperties['email.brandAdminDetailsMail']}")
    public String brandAdminDetailsMail;

    @Value("#{portalProperties['root.url']}")
    public String rootUrl;

    @Value("#{portalProperties['email.restFeatureChangeMailToComp']}")
    public String restFeatureChangeToCompany;

    @Value("#{portalProperties['email.restFeatureChangeMailToBrand']}")
    public String restFeatureChangeToBrand;

    @Value("#{portalProperties['email.brandFeatureChangeMailToComp']}")
    public String brandFeatureChangeToCompany;

    @Value("#{portalProperties['context.url']}")
    public String CONTEXT_URL;

    @Value("#{portalProperties['website.name']}")
    public String websiteName;

    @Value("#{portalProperties['root.restpasswordurl']}")
    public String resetPasswprdUrl;

    @Value("#{portalProperties['email.from']}")
    public String emailFrom;

    @Value("#{portalProperties['emailto.admin']}")
    public String emailToAdmin;

    @Value("#{portalProperties['member.firstCardNumber']}")
    public String firstCardNumber;

    @Value("#{portalProperties['evite.eviteInvitation']}")
    public String eviteInvitation;

    @Value("#{portalProperties['evite.eviteCancellation']}")
    public String eviteCancellation;

    @Value("#{portalProperties['evite.eviteEdit']}")
    public String eviteEdit;

    @Value("#{portalProperties['documents.root']}")
    public String documentsRoot;

    @Value("#{portalProperties['documents.url']}")
    public String documentUrl;

    @Value("#{portalProperties['file.sperator']}")
    public String fileSperator;


    @Value("#{portalProperties['promocode.timeDifference']}")
    public long  promocodeTimeDiff;

    @Value("#{portalProperties['email.promocodemail']}")
    public String promocodeMail;

    @Value("#{portalProperties['email.individualvouchermail']}")
    public String individualVoucherMail;

    @Value("#{portalProperties['email.giftedvouchermail']}")
    public String giftedVoucherMail;

    @Value("#{portalProperties['email.corporatevouchermail']}")
    public String corporateVoucherMail;

    @Value("#{portalProperties['base.currency']}")
    public int baseCurrency;

    @Value("#{portalProperties['pagination.interval']}")
    public int paginationInterval;

    @Value("#{portalProperties['email.corporatecodemail']}")
    public String corporateCodeMail;

    @Value("#{portalProperties['email.corporatedlinkmail']}")
    public String corporateDlinkMail;

    @Value("#{portalProperties['email.memberdlinkmail']}")
    public String memberDlinkMail;

    @Value("#{portalProperties['simple.dateformate']}")
    public String simpleDateFormat;

    @Value("#{portalProperties['simple.timeformate']}")
    public String simpleTimeFormat;

    @Value("#{portalProperties['simple.datetimeformate']}")
    public String simpleDatetimeFormat;

    @Value("#{portalProperties['simple.datetimezoneformate']}")
    public String simpleDateTimezoneFormat;

    @Value("#{portalProperties['special.dateformate']}")
    public String specialDateFormat;

    @Value("#{portalProperties['slash.dateformat']}")
    public String dateFormat;

    @Value("#{portalProperties['email.memberdetailsmail']}")
    public String memeberDetailsMail;

    @Value("#{portalProperties['point.membershipDefaultValue']}")
    public int membershipDefaultPointvalue;

    @Value("#{portalProperties['point.membershipCancellationAmount']}")
    public int membershipCancellationValue;

    @Value("#{portalProperties['point.restaurantDefaultValue']}")
    public int restaurantDefaultPointvalue;

    @Value("#{portalProperties['imagesupportedformat']}")
    public String imageSupportedFormat;

    @Value("#{portalProperties['default.timezone']}")
    public String defaultTimezone;


    @Value("#{portalProperties['session.warning']}")
    public int concurrencySessionWarning;


    @Value("#{portalProperties['session.expire']}")
    public int concurrencySessionExpire;

    @Value("#{portalProperties['prefix.company']}")
    public String companyLoginPrefix;

    @Value("#{portalProperties['prefix.brand']}")
    public String brandLoginPrefix;

    @Value("#{portalProperties['prefix.rest']}")
    public String restaurantLoginPrefix;

    @Value("#{portalProperties['prefix.corporate']}")
    public String corporateLoginPrefix;

    @Value("#{portalProperties['trial.tenure']}")
    public String trialTenure;

    @Value("#{portalProperties['trial.cities']}")
    public String trialCities;

    @Value("#{portalProperties['course.header']}")
    public String courseHeader;

    @Value("#{portalProperties['course.courseInvitation']}")
    public String courseInvitation;

    @Value("#{portalProperties['courseBatch.courseBatchInvitation']}")
    public String courseBatchInvitation;

    @Value("#{portalProperties['domain.name']}")
    public String domainName;

    @Value("#{portalProperties['resource.base']}")
    public String resourceBase;

    @Value("#{portalProperties['resource.csv']}")
    public String resourceCsv;


}