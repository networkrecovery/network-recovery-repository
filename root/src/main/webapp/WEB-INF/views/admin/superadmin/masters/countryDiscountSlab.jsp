<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@include file="../../inc/config.jsp"%>
<style>

.chosen-container {
	width: 250px !important;
}
</style>
	<div class="content-header">
		<div class="header-section">
			<h1>
				<i class="fa fa-map-marker"></i>
				<spring:message code="heading.countrydiscountslab" />
				<br> <small><spring:message code="heading.countrydiscountslabdetails" />
				</small>
			</h1>
			<span id="errorMsg"></span>
			<c:if test="${!empty success}">
				<div class="alert alert-success alert-dismissable">
					<button type="button" id="success_close" class="close" data-dismiss="alert"
						aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-check-circle"></i><spring:message code="label.success"/>
					</h4>
					<spring:message code="${success}" />
				</div>
			</c:if>
<!-- 			<div class="alert alert-danger alert-dismissable" -->
<!-- 				style="display: none" id="error"> -->
<!-- 				<button type="button" class="close" data-dismiss="alert" -->
<!-- 					aria-hidden="true">x</button> -->
<!-- 				<h4> -->
<%-- 					<i class="fa fa-times-circle"> </i><spring:message code="label.error"/> --%>
<!-- 				</h4> -->
<!-- 				<span id="errorMsg"></span> -->
<!-- 			</div> -->
		</div>
	</div>
	<ul class="breadcrumb breadcrumb-top">
		<li><spring:message code="menu.discountmodel" /></li>
		<li><a href="#"><spring:message code="label.countrydiscountslab" /></a></li>
	</ul>
	<div class="block full gridView">
		<div class="block-title">
			<h2>
				<strong><spring:message code="heading.activediscountslab" /></strong>
			</h2>

		</div>
		<a id="addCountryDiscountSlab" class="btn btn-sm btn-primary" href="#">
						<i class="fa fa-angle-right"></i> <spring:message code="label.addcountrydiscountslab" />
		</a>
<%-- 		<a href="#" id="addCountryDiscountSlab"><spring:message code="label.addcountrydiscountslab" /></a> --%>
		<!--<p><a href="https://datatables.net/" target="_blank">DataTables</a> is a plug-in for the Jquery Javascript library. It is a highly flexible tool, based upon the foundations of progressive enhancement, which will add advanced interaction controls to any HTML table. It is integrated with template's design and it offers many features such as on-the-fly filtering and variable length pagination.</p> -->
		<div class="table-responsive">
			<table id="active-datatable"
				class="table table-vcenter table-condensed table-bordered">
				<thead>
					<tr>
						<th class="text-center"><spring:message code="label.id" /></th>
					    <th class="text-center"><spring:message code="label.range" /></th>
					    <th class="text-center"><spring:message code="label.discount" />%</th>
						<th class="text-center"><spring:message code="label.actions" /></th>
					</tr>
				</thead>
				<tbody id="tbody">
					<c:forEach var="currentCountDiscountSlab" items="${countDiscountSlabList}" varStatus="count">

						<tr>
							<td class="text-center">${count.count}</td>
							<!-- 							<td class="text-center"><img -->
							<!-- 								src="../img/placeholders/avatars/avatar15.jpg" alt="avatar" -->
							<!-- 								class="img-circle"></td> -->
							<td class="text-right"><c:out
									value="${currentCountDiscountSlab.minRangeValue}-${currentCountDiscountSlab.maxRangeValue}" /></td>
						   <td class="text-right"><c:out
									value="${currentCountDiscountSlab.percentDiscount}" /></td>
							
							<td class="text-center">
								<div class="btn-group">
									<a href="#" data-toggle="tooltip" title="Edit"
										onclick="editCountDiscountSlab(${currentCountDiscountSlab.countDiscountSlabId},${count.count})"
										class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>
										
										  <a href="#delete_countryslab_popup" data-toggle="modal"
										onclick="deleteCountrySlab(${currentCountDiscountSlab.countDiscountSlabId})" title="Delete"
										class="btn btn-xs btn-danger"><i class="fa fa-times"></i></a>

								</div>
							</td>
						</tr>
					</c:forEach>

				</tbody>
			</table>
		</div>
	</div>

<div class="block full gridView">
		<div class="block-title">
			<h2>
			<strong><spring:message code="heading.inactivecountryslab" /></strong>
		   </h2>
		 </div>
		<div class="table-responsive">
			<table id="inactive-datatable"
				class="table table-vcenter table-condensed table-bordered">
				<thead>
					<tr>
					   <th class="text-center"><spring:message code="label.id" /></th>
					    <th class="text-center"><spring:message code="label.range" /></th>
					    <th class="text-center"><spring:message code="label.discount" />%</th>
						<th class="text-center"><spring:message code="label.actions" /></th>
					</tr>
				</thead>
					<tbody id="tbody">
					<c:forEach var="currentCountDiscountSlab" items="${countInactiveDiscountSlabList}" varStatus="count">

						<tr>
							<td class="text-center">${count.count}</td>
							<!-- 							<td class="text-center"><img -->
							<!-- 								src="../img/placeholders/avatars/avatar15.jpg" alt="avatar" -->
							<!-- 								class="img-circle"></td> -->
							<td class="text-right"><c:out
									value="${currentCountDiscountSlab.minRangeValue}-${currentCountDiscountSlab.maxRangeValue}" /></td>
						   <td class="text-right"><c:out
									value="${currentCountDiscountSlab.percentDiscount}" /></td>
							
							<td class="text-center">
								<div class="btn-group">
								
                                      <a href="#restore_CountrySlab_popup" data-toggle="modal"
										onclick="restoreCountrySlab(${currentCountDiscountSlab.countDiscountSlabId})" title="Restore"
										class="btn btn-xs btn-danger"><i class="fa fa-times"></i></a>
								</div>
							</td>
						</tr>
					</c:forEach>

				</tbody>
			</table>
		</div>
	</div>

<div id="delete_countryslab_popup" class="modal fade" tabindex="-1"
		role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<form action="" method="post" class="form-horizontal form-bordered">
						<div style="padding: 10px; height: 110px;">
							<label><spring:message code="validation.doyouwanttodeletethiscountryslab"/></label>
							<div class="col-xs-12 text-right">
								<button type="button" class="btn btn-sm btn-default"
									data-dismiss="modal"><spring:message code="label.no" /></button>
								<div id="delete_countryslab" class="btn btn-sm btn-primary"><spring:message code="label.yes" /></div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
<div id="restore_CountrySlab_popup" class="modal fade" tabindex="-1"
		role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<form action="" method="post" class="form-horizontal form-bordered">
						<div style="padding: 10px; height: 110px;">
							<label><spring:message code="validation.doyouwanttorestorethiscountryslab"/></label>
							<div class="col-xs-12 text-right">
								<button type="button" class="btn btn-sm btn-default"
									data-dismiss="modal"><spring:message code="label.no"/></button>
								<div id="restore_CountrySlab" class="btn btn-sm btn-primary"><spring:message code="label.yes"/></div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

	<div class="block full" id="formView" style="display: none">
		<div class="block-title">
			<h2>
				<strong><spring:message code="label.addcountrydiscountslab" /></strong>
			</h2>
		</div>
		<form action="" method="post" class="form-horizontal form-bordered"
			id="CountryDiscount_form">
			
			<div class="form-group">
				<input id="countryDiscount_Id" name="countryDiscountId" type="hidden" value="0"> 
				<label
					class="col-md-3 control-label" for="countryDiscount_Min"><spring:message
						code="label.minimum" /><span class="text-danger">*</span></label>
				<div class="col-md-9">
					<input id="countryDiscount_Min" name="countryDiscountMin" class="form-control"
						placeholder="<spring:message code="label.minimum"/>.."
						type="text">
				</div>
			</div>
			
			<div class="form-group">
			<label class="col-md-3 control-label" for="countryDiscount_Max"><spring:message
					code="label.maximum" /><span class="text-danger">*</span> </label>
			<div class="col-md-9">
				<input id="countryDiscount_Max" name="countryDiscountMax" class="form-control"
					placeholder="<spring:message code="label.maximum"/>.."
					type="text">
			</div>
		   </div>
			
            <div class="form-group">
			<label class="col-md-3 control-label" for="countryDiscount_Discout"><spring:message
					code="label.discount" /><span class="text-danger">*</span> </label>
			<div class="col-md-9">
				<input id="countryDiscount_Discount" name="countryDiscountDiscount" class="form-control"
					placeholder="<spring:message code="label.discount"/>.."
					type="text">
			</div>
		   </div>

			



			<div class="form-group form-actions">
				<div class="col-md-9 col-md-offset-3">
					<div id="countryDiscount_submit" class="btn btn-sm btn-primary">
						<i class="fa fa-angle-right"></i> <spring:message code="button.save" />
					</div>
					<button type="reset" class="btn btn-sm btn-warning">
						<i class="fa fa-repeat"></i>
						<spring:message code="button.reset" />
					</button>
					<a class="btn btn-sm btn-primary" id="cancel" href="#">
						<i class="gi gi-remove"></i>
						<spring:message code="button.cancel" />
					</a>
				</div>
			</div>
		</form>
	</div>




<script src="<%=contexturl %>resources/js/pages/activeTablesDatatables.js"></script>
<script src="<%=contexturl %>resources/js/pages/inactiveTablesDatatables.js"></script>
<script>$(function(){ ActiveTablesDatatables.init(3); });</script>
<script>$(function(){ InActiveTablesDatatables.init(3); });</script>
<script type="text/javascript">
	$(document).ready(function() {
		
		
		
		$("#addCountryDiscountSlab").click(function() {
			$('#CountryDiscount_form').find("input[type=text], textarea").val("");
			$('#CountryDiscount_form').find("input[type=hidden], textarea").val("0");
			$("#formView").css("display", "block");
			$(".gridView").css("display", "none");
			$("#success_close").click();
		});
		
		$("#cancel").click(function() {
			$("#formView").css("display", "none");
			$(".gridView").css("display", "block");
			$('.help-block').css('display', 'none');
			$("div.form-group").removeClass('has-error');
		});
		

		$("#restore_CountrySlab").click(function(){
			$.ajax({
				type: "GET",
				async: false,
				url: "<%=contexturl%>Admin/RestoreCountrySlab?id="+restoreId,
				success: function(data, textStatus, jqXHR){
					$('#page-content').html(data);
					$('div').removeClass("modal-backdrop fade in");
					$('body').removeClass('modal-open');
					$('.modal-backdrop').remove();
					$('#restore_CountrySlab_popup').modal('hide');	
				},
				dataType: 'html'
			});
		});

		
		$("#delete_countryslab").click(function(){
			$.ajax({
				type: "GET",
				async: false,
				url: "<%=contexturl%>Admin/DeleteCountrySlab?id="+selectedId,
				success: function(data, textStatus, jqXHR){
					$('#page-content').html(data);
					$('body').removeClass('modal-open');
					$('.modal-backdrop').remove();
					$('#delete_countryslab_popup').modal('hide');	
				},
				dataType: 'html'
			});
		});
		  

		$("#CountryDiscount_form").validate(
				{	errorClass:"help-block animation-slideDown",
					errorElement:"div",
					errorPlacement:function(e,a){a.parents(".form-group > div").append(e)},
					highlight:function(e){$(e).closest(".form-group").removeClass("has-success has-error").addClass("has-error")},
					success:function(e){e.closest(".form-group").removeClass("has-success has-error")},
					rules:{ countryDiscountMin:{required:!0,digits : !0,maxlength : 11},	
						countryDiscountMax:{required:!0,digits : !0,maxlength : 11,greaterThan :'#countryDiscount_Min'},
						countryDiscountDiscount:{required:!0,discount :!0}
							
							
					},
					messages:{
						countryDiscountMin:{required:'<spring:message code="validation.minimum"/>',
							digits : '<spring:message code="validation.digits"/>',
							maxlength :'<spring:message code="validation.minimum11character"/>'},	
							countryDiscountMax:{required:'<spring:message code="validation.maximum"/>',
								digits : '<spring:message code="validation.digits"/>',
								maxlength :'<spring:message code="validation.maximum11character"/>',
								greaterThan :"Range should be greater than minimum range"},
							countryDiscountDiscount:{required:'<spring:message code="validation.discount"/>',
								maxlength :'<spring:message code="validation.discount"/>'}
						
					},
					submitHandler: function() {
						var data = $("#CountryDiscount_form").serialize();
						$.ajax({
							type: "POST",
							async: false,
							url: "<%=contexturl%>Admin/SaveCountryDiscount",
							data:data,
							success: function(data, textStatus, jqXHR){
								var i = data.trim().indexOf("errorCode");
								if(i == 2){
									var obj = $.parseJSON(data.trim());
									var html = '<div class="alert alert-danger alert-dismissable">	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="fa fa-times-circle"></i> Error</h4> <span >'+obj.errorCode+'</span></div>';
									$("#errorMsg").html(html).show();
									$("#errorMsg").html(html).fadeOut(8000);
									$('html, body').animate({ scrollTop: $("#errorMsg").offset().top },500);
								}
								else{
									$('html, body').animate({ scrollTop: $("#errorMsg").offset().top },500);
									$('#page-content').html(data);
								}
							},
							dataType: 'html'
						});
	                }
				});
		
		
		
		
		 $("#countryDiscount_submit").click(function(){
			 $("#CountryDiscount_form").submit();
		 });
		
		
	});
	
	function editCountDiscountSlab(id,position){
		var oTable = $("#active-datatable").dataTable();
		var data = oTable.fnGetData()[position-1];
		$("#countryDiscount_Id").val(id);
		var rangeArray = data[1].split("-");
		$("#countryDiscount_Min").val(rangeArray[0]);
		$("#countryDiscount_Max").val(rangeArray[1]);
		$("#countryDiscount_Discount").val(data[2]);
		$("#formView").css("display", "block");
		$(".gridView").css("display", "none");
		$('.help-block').css('display', 'none');
		$("div.form-group").removeClass('has-error');
		$("#success_close").click();
	}
	
	function restoreCountrySlab(id){
		restoreId = id;
	}
	function deleteCountrySlab(id){
		selectedId = id;
	}
</script>
