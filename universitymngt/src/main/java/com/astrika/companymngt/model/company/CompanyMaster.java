package com.astrika.companymngt.model.company;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.envers.Audited;
import org.joda.time.DateTime;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import com.astrika.common.model.User;
import com.astrika.common.model.location.CityMaster;
import com.astrika.common.model.location.CountryMaster;
import com.astrika.common.model.location.StateMaster;
import com.astrika.educationsector.model.EducationSector;







@Entity
@Audited
public class CompanyMaster {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Access(AccessType.PROPERTY)
	private Long companyId;
	
	/* Step 1 fields*/
	@Column(unique = true, nullable = false, length = 75)
	private String companyName;
	
	@Column
	private String sectorWork;
	
	@Column	
	private String companyTelephone1;
	
	@Column
	private String companyTelephone2;
	
	@Column	( nullable = false, length = 75)
	private String companyEmail;
	
	@Column(nullable = false,length=500)
	private String companyWebsite;
	
	@Column	( nullable = false, length = 75)
	private String companyAddressLine1;
	
	@Column
	private String companyAddressLine2;
	
	@Column(columnDefinition = "boolean default false", nullable = false)
	private boolean flag;
	
	@Column
	private String companyAddressLine3;
	
	@Column(nullable = false, length = 75)
	private String companyPincode;
	
	@Column(nullable=false,length=500)
	private String organisationVision;
	
	@Column(nullable=false,length=500)
    private String organisationMission;
	
	@Column(nullable=false,length=255)
	private String organisationDescription;
	
	@Column(nullable= false)
	private boolean assitanceFromOtherAgency;
	
	@Column(nullable= false)
	private String legalType;
	
	@Column(nullable= false)
	private boolean fcraRegistarion;
	
	@Column
	private String typeOfEducation;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JsonIgnore
	private StateMaster state;
	
	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	private CityMaster city;
    
	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	private EducationSector education;
    
	
	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	private CountryMaster country;
	
	@JsonIgnore
	@OneToOne(fetch = FetchType.LAZY)
	private User admin;
	
	@Column(columnDefinition = "boolean default true", nullable = false)
	private boolean active = true;
	
	@Column
	private String quarter;
	
	@Column(nullable= false,length=500)
	private String remark;
	
/* Step 3 fields*/
	
	@Column
	private String noOfEmployees;

	@Column
	private String trustees;
	
	@Column
	private String audit1;
	
	@Column
	private Long sizeOfaudit1;
	
	@Column
	private String audit1DocumentName;
	
	@Column
	private Long sizeOfaudit2;
	
	@Column
	private String audit2;
	
	@Column
	private String audit2DocumentName;
	
	@Column
	private Long budgetSize;
	
	@Column
	private String existingFunders; 
	
	@Column
	private String otherFunders;
	
	/* Step 2 fields*/
	
	@Column
	private boolean religiousInclination;
	
	@Column
	private String reasonOfInclination;
	
	@Column
	private String areaOfOperations;
	
	@Column
	private String reasonForOtherArea;
	
	@Column
	private String registrationNumber;
	
	@Column
	private String panNumber;
	
	@Column
	private String panFile;
	
	@Column
	private String panDocumentName;
	
	@Column
	private String twelveACertificate;
	
	@Column
	private String twelveAFile;
	
	@Column
	private String twelveADocumentName;
	
	@Column
	private String eightyGNumber;
	
	@Column
	private String eightyGFile;
	
	@Column
	private String eightyGDocumentName;
	
	@Column
	private String fcraRegNumber;
	
	@Column
	private String fcraFile;
	
	@Column
	private String fcraDocumentName;
	
	@Column
	private int noOfTrustees;
	
	@Column
	private int noOfChildren;
	
	@Column
	private int noOfGirls;
	
	@Column
	private int noOfBoys;
	
	@Column
	private String ageOfStudents;
	
	@Column
	private String mainSourceOfFunding;
	
	/* Audit fields*/
	
	@CreatedDate
	private DateTime createdOn;

	@LastModifiedDate
	private DateTime lastModifiedOn;

	@JsonIgnore
	@CreatedBy
	@ManyToOne(fetch = FetchType.LAZY)
	private User createdBy;

	@JsonIgnore
	@LastModifiedBy
	@ManyToOne(fetch = FetchType.LAZY)
	private User lastModifiedBy;
	
	

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	private User deActivatedBy;

	private DateTime deActivatedOn;

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	private User reActivatedBy;

	private DateTime reActivatedOn;

	public CompanyMaster(String companyName){
	    super();
	    this.companyName = companyName;
	}

	public CompanyMaster(
	        String companyName,String companyTelephone1,String companyTelephone2,
	        String companyEmail,
	        String companyAddressLine1,String companyAddressLine2,
	        StateMaster companyState,CityMaster companyCity,CountryMaster companyCountry,
	        String companyPincode){
	    super();
	    this.companyName = companyName;
	    this.companyTelephone1 = companyTelephone1;
	    this.companyTelephone2 = companyTelephone2;
	    this.companyEmail = companyEmail;

	    this.companyAddressLine1 = companyAddressLine1;
	    this.companyAddressLine2 = companyAddressLine2;
	    this.state = companyState;
	    this.city = companyCity;
	    this.country = companyCountry;
	    this.companyPincode = companyPincode;
	}

	
	public CompanyMaster(){
        super();
    }
	
	public boolean isFlag() {
		return flag;
	}

	public void setFlag(boolean flag) {
		this.flag = flag;
	}
	
	public String getSectorWork() {
		return sectorWork;
	}

	public void setSectorWork(String sectorWork) {
		this.sectorWork = sectorWork;
	}

	public String getCompanyWebsite() {
		return companyWebsite;
	}

	public void setCompanyWebsite(String companyWebsite) {
		this.companyWebsite = companyWebsite;
	}

	public String getCompanyAddressLine3() {
		return companyAddressLine3;
	}

	public void setCompanyAddressLine3(String companyAddressLine3) {
		this.companyAddressLine3 = companyAddressLine3;
	}

	public String getOrganisationVision() {
		return organisationVision;
	}

	public void setOrganisationVision(String organisationVision) {
		this.organisationVision = organisationVision;
	}

	public String getOrganisationMission() {
		return organisationMission;
	}

	public void setOrganisationMission(String organisationMission) {
		this.organisationMission = organisationMission;
	}

	public String getOrganisationDescription() {
		return organisationDescription;
	}

	public void setOrganisationDescription(String organisationDescription) {
		this.organisationDescription = organisationDescription;
	}

	public boolean isAssitanceFromOtherAgency() {
		return assitanceFromOtherAgency;
	}

	public void setAssitanceFromOtherAgency(boolean assitanceFromOtherAgency) {
		this.assitanceFromOtherAgency = assitanceFromOtherAgency;
	}

	public String getLegalType() {
		return legalType;
	}

	public void setLegalType(String legalType) {
		this.legalType = legalType;
	}

	public boolean isFcraRegistarion() {
		return fcraRegistarion;
	}

	public void setFcraRegistarion(boolean fcraRegistarion) {
		this.fcraRegistarion = fcraRegistarion;
	}

	public String getTypeOfEducation() {
		return typeOfEducation;
	}

	public void setTypeOfEducation(String typeOfEducation) {
		this.typeOfEducation = typeOfEducation;
	}

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCompanyTelephone1() {
		return companyTelephone1;
	}

	public void setCompanyTelephone1(String companyTelephone1) {
		this.companyTelephone1 = companyTelephone1;
	}

	public String getCompanyTelephone2() {
		return companyTelephone2;
	}

	public void setCompanyTelephone2(String companyTelephone2) {
		this.companyTelephone2 = companyTelephone2;
	}

	public String getCompanyEmail() {
		return companyEmail;
	}

	public void setCompanyEmail(String companyEmail) {
		this.companyEmail = companyEmail;
	}

	

	public String getCompanyAddressLine1() {
		return companyAddressLine1;
	}

	public void setCompanyAddressLine1(String companyAddressLine1) {
		this.companyAddressLine1 = companyAddressLine1;
	}

	public String getCompanyAddressLine2() {
		return companyAddressLine2;
	}

	public void setCompanyAddressLine2(String companyAddressLine2) {
		this.companyAddressLine2 = companyAddressLine2;
	}

	public String getCompanyPincode() {
		return companyPincode;
	}

	public void setCompanyPincode(String companyPincode) {
		this.companyPincode = companyPincode;
	}

	public StateMaster getState() {
		return state;
	}

	public void setState(StateMaster state) {
		this.state = state;
	}

	public CityMaster getCity() {
		return city;
	}

	public void setCity(CityMaster city) {
		this.city = city;
	}

	public CountryMaster getCountry() {
		return country;
	}

	public void setCountry(CountryMaster country) {
		this.country = country;
	}

	public User getAdmin() {
		return admin;
	}

	public void setAdmin(User admin) {
		this.admin = admin;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public DateTime getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(DateTime createdOn) {
		this.createdOn = createdOn;
	}

	public DateTime getLastModifiedOn() {
		return lastModifiedOn;
	}

	public void setLastModifiedOn(DateTime lastModifiedOn) {
		this.lastModifiedOn = lastModifiedOn;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}

	public User getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(User lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public String getQuarter() {
		return quarter;
	}

	public void setQuarter(String quarter) {
		this.quarter = quarter;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getNoOfEmployees() {
		return noOfEmployees;
	}

	public void setNoOfEmployees(String noOfEmployees) {
		this.noOfEmployees = noOfEmployees;
	}

	public String getTrustees() {
		return trustees;
	}

	public void setTrustees(String trustees) {
		this.trustees = trustees;
	}

	public String getAudit1() {
		return audit1;
	}

	public void setAudit1(String audit1) {
		this.audit1 = audit1;
	}

	public String getAudit2() {
		return audit2;
	}

	public void setAudit2(String audit2) {
		this.audit2 = audit2;
	}

	public Long getBudgetSize() {
		return budgetSize;
	}

	public void setBudgetSize(Long budgetSize) {
		this.budgetSize = budgetSize;
	}

	public String getExistingFunders() {
		return existingFunders;
	}

	public void setExistingFunders(String existingFunders) {
		this.existingFunders = existingFunders;
	}

	public String getOtherFunders() {
		return otherFunders;
	}

	public void setOtherFunders(String otherFunders) {
		this.otherFunders = otherFunders;
	}

	public Long getSizeOfaudit1() {
		return sizeOfaudit1;
	}

	public void setSizeOfaudit1(Long sizeOfaudit1) {
		this.sizeOfaudit1 = sizeOfaudit1;
	}

	public Long getSizeOfaudit2() {
		return sizeOfaudit2;
	}

	public void setSizeOfaudit2(Long sizeOfaudit2) {
		this.sizeOfaudit2 = sizeOfaudit2;
	}

	public String getAudit1DocumentName() {
		return audit1DocumentName;
	}

	public void setAudit1DocumentName(String audit1DocumentName) {
		this.audit1DocumentName = audit1DocumentName;
	}

	public String getAudit2DocumentName() {
		return audit2DocumentName;
	}

	public void setAudit2DocumentName(String audit2DocumentName) {
		this.audit2DocumentName = audit2DocumentName;
	}

	public boolean isReligiousInclination() {
		return religiousInclination;
	}

	public void setReligiousInclination(boolean religiousInclination) {
		this.religiousInclination = religiousInclination;
	}

	public String getReasonOfInclination() {
		return reasonOfInclination;
	}

	public void setReasonOfInclination(String reasonOfInclination) {
		this.reasonOfInclination = reasonOfInclination;
	}

	public String getAreaOfOperations() {
		return areaOfOperations;
	}

	public void setAreaOfOperations(String areaOfOperations) {
		this.areaOfOperations = areaOfOperations;
	}


	public String getRegistrationNumber() {
		return registrationNumber;
	}

	public void setRegistrationNumber(String registrationNumber) {
		this.registrationNumber = registrationNumber;
	}

	public String getPanNumber() {
		return panNumber;
	}

	public void setPanNumber(String panNumber) {
		this.panNumber = panNumber;
	}

	public String getPanFile() {
		return panFile;
	}

	public void setPanFile(String panFile) {
		this.panFile = panFile;
	}

	public String getPanDocumentName() {
		return panDocumentName;
	}

	public void setPanDocumentName(String panDocumentName) {
		this.panDocumentName = panDocumentName;
	}

	public String getTwelveACertificate() {
		return twelveACertificate;
	}

	public void setTwelveACertificate(String twelveACertificate) {
		this.twelveACertificate = twelveACertificate;
	}

	public String getTwelveAFile() {
		return twelveAFile;
	}

	public void setTwelveAFile(String twelveAFile) {
		this.twelveAFile = twelveAFile;
	}

	public String getTwelveADocumentName() {
		return twelveADocumentName;
	}

	public void setTwelveADocumentName(String twelveADocumentName) {
		this.twelveADocumentName = twelveADocumentName;
	}

	public String getEightyGNumber() {
		return eightyGNumber;
	}

	public void setEightyGNumber(String eightyGNumber) {
		this.eightyGNumber = eightyGNumber;
	}

	public String getEightyGFile() {
		return eightyGFile;
	}

	public void setEightyGFile(String eightyGFile) {
		this.eightyGFile = eightyGFile;
	}

	public String getEightyGDocumentName() {
		return eightyGDocumentName;
	}

	public void setEightyGDocumentName(String eightyGDocumentName) {
		this.eightyGDocumentName = eightyGDocumentName;
	}

	public String getFcraRegNumber() {
		return fcraRegNumber;
	}

	public void setFcraRegNumber(String fcraRegNumber) {
		this.fcraRegNumber = fcraRegNumber;
	}

	public String getFcraFile() {
		return fcraFile;
	}

	public void setFcraFile(String fcraFile) {
		this.fcraFile = fcraFile;
	}

	public String getFcraDocumentName() {
		return fcraDocumentName;
	}

	public void setFcraDocumentName(String fcraDocumentName) {
		this.fcraDocumentName = fcraDocumentName;
	}

	public int getNoOfTrustees() {
		return noOfTrustees;
	}

	public void setNoOfTrustees(int noOfTrustees) {
		this.noOfTrustees = noOfTrustees;
	}

	public int getNoOfChildren() {
		return noOfChildren;
	}

	public void setNoOfChildren(int noOfChildren) {
		this.noOfChildren = noOfChildren;
	}

	public int getNoOfGirls() {
		return noOfGirls;
	}

	public void setNoOfGirls(int noOfGirls) {
		this.noOfGirls = noOfGirls;
	}

	public int getNoOfBoys() {
		return noOfBoys;
	}

	public void setNoOfBoys(int noOfBoys) {
		this.noOfBoys = noOfBoys;
	}

	public String getAgeOfStudents() {
		return ageOfStudents;
	}

	public void setAgeOfStudents(String ageOfStudents) {
		this.ageOfStudents = ageOfStudents;
	}

	public String getMainSourceOfFunding() {
		return mainSourceOfFunding;
	}

	public void setMainSourceOfFunding(String mainSourceOfFunding) {
		this.mainSourceOfFunding = mainSourceOfFunding;
	}

	public String getReasonForOtherArea() {
		return reasonForOtherArea;
	}

	public void setReasonForOtherArea(String reasonForOtherArea) {
		this.reasonForOtherArea = reasonForOtherArea;
	}


	public User getDeActivatedBy() {
		return deActivatedBy;
	}

	public void setDeActivatedBy(User deActivatedBy) {
		this.deActivatedBy = deActivatedBy;
	}

	public DateTime getDeActivatedOn() {
		return deActivatedOn;
	}

	public void setDeActivatedOn(DateTime deActivatedOn) {
		this.deActivatedOn = deActivatedOn;
	}

	public User getReActivatedBy() {
		return reActivatedBy;
	}

	public void setReActivatedBy(User reActivatedBy) {
		this.reActivatedBy = reActivatedBy;
	}

	public DateTime getReActivatedOn() {
		return reActivatedOn;
	}

	public void setReActivatedOn(DateTime reActivatedOn) {
		this.reActivatedOn = reActivatedOn;
	}
}
