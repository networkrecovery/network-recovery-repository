package com.astrika.common.exception;

import com.astrika.kernel.exception.BusinessException;
import com.astrika.kernel.exception.CustomException;


public class DuplicateModuleUserException extends BusinessException {

	public DuplicateModuleUserException()
	{
		super(CustomException.DUPLICATE_MODULE_USER.getCode());
	}
	public DuplicateModuleUserException(String errorCode){
		super(errorCode);
	}
	
}
