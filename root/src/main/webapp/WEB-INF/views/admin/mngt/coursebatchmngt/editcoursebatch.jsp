<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/template_start.jsp"%>
<%@include file="../../inc/page_head.jsp"%>
<div id="page-content">
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="fa fa-map-marker"></i>
                <spring:message code="heading.coursebatch" />
                <br> <small><spring:message
                        code="heading.coursebatchdetails" /></small>
            </h1>
            <span id="errorMsg"></span>
            <c:if test="${!empty success}">
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert"
                        aria-hidden="true">x</button>
                    <h4>
                        <i class="fa fa-check-circle"></i> <spring:message code="label.success"/>
                    </h4>
                    <spring:message code="${success}" />
                </div>
            </c:if>

            <c:if test="${!empty error}">
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert"
                        aria-hidden="true">�</button>
                    <h4>
                        <i class="fa fa-times-circle"></i> <spring:message code="label.error"/>
                    </h4>
                    <spring:message code="${error}" />
                </div>
            </c:if>
        </div>
    </div>
    <ul class="breadcrumb breadcrumb-top">
        <li><spring:message code="heading.coursebatch" /></li>
    </ul>
            
            <div class="block full" >
                    <div class="block-title">
                        <h2>
                            <strong><spring:message code="heading.coursebatchinfo" /></strong>
                        </h2>
                    </div>
                    <form action="<%=contexturl %>ManageCourseBatch/CourseBatchList/UpdateCourseBatch" method="post" class="form-horizontal "
                        id="Course_Batch_Form" >
                            <input name="courseBatchId" value="${courseBatch.courseBatchId}" type="hidden">
                            <input id="confirmation" name="confirmation" type="hidden" value="false">
                            <input id="publish" name="publish" type="hidden" value="false">
                            
                    <div class="form-group" id="CourseBatchDiv">
                    <label class="col-md-3 control-label" for="CourseBatch_Name"><spring:message
                            code="label.course" /><span class="text-danger">*</span></label>
                    <div class="col-md-7">
                        <select class="CourseBatch_chosen" style="width: 400px;" id="chosenCourseId"
                            name="chosenCourseId"
                            data-placeholder="<spring:message code='label.choosecoursebatch' />">
                            <option value=""></option>
                            <c:forEach items="${courseList}" var="course">
                                <option value="${course.courseId}" <c:if test="${courseBatch.course.courseId eq course.courseId}">selected="selected"</c:if>>${course.courseName}</option>
                            </c:forEach>
                        </select>
                    </div>

                </div>
                <div class="form-group" id="InstructorDiv">
                    <label class="col-md-3 control-label" for="countryFrom_Name"><spring:message
                            code="heading.instructor" /></label>
                    <div class="col-md-7">
                        <select multiple class="Instructor_chosen" style="width: 400px;" id="chosenInstructorId"
                            name="chosenInstructorId"
                            data-placeholder="<spring:message code='label.chooseinstructor' />">
                            <option value=""></option>
                            <c:forEach items="${instructorList}" var="instructor">
                            <c:set var="present" value="false" />
                                        <c:forEach items="${instructors}" var="instructors">
                                            <c:if test="${instructor.user.userId eq instructors.user.userId}">
                                            <c:set var="present" value="true" />    
                                            </c:if>
                                            
                                        </c:forEach>
                                        <c:if test="${instructor.user.userId eq instructorMain.user.userId}">
                                                <c:set var="present" value="true" />
                                            </c:if>
                                        <option value="${instructor.user.userId}"
                                            <c:if test="${present}">    selected="selected" </c:if>>
                                            ${instructor.user.firstName} ${ instructor.user.lastName}</option>
                                        
                            </c:forEach>
                                
                        </select>
                    </div>

                </div>
                
                <div class="form-group">
                        <label class="col-md-3 control-label" for="example-daterange1"><spring:message code="label.batchperiod"/>
                            </label>
                        <div class="col-md-7">
                            <div class="input-group input-daterange"
                                data-date-format="mm/dd/yyyy">
                                <input type="text" id="startDate"
                                    name="startDate" class="form-control text-center" data-date-format="MM/dd/yyyy"
                                    placeholder="From" value="<fmt:formatDate pattern="<%=propvalue.dateFormat %>"  value="${courseBatch.startDate}"/>"> <span class="input-group-addon"><i
                                    class="fa fa-angle-right"></i></span> <input type="text"
                                    id="endDate" name="endDate"
                                    class="form-control text-center" placeholder="To" data-date-format="MM/dd/yyyy" value="<fmt:formatDate pattern="<%=propvalue.dateFormat %>"  value="${courseBatch.startDate}"/>">
                            </div>
                        </div>
                    </div>
                
<!--                <div class="form-group" id="startDate"> -->
<%--                <label class="col-md-3 control-label" for="batch_start"><spring:message --%>
<%--                        code='label.startdate' /><span class="text-danger">*</span></label> --%>
<!--                <div class="col-md-9"> -->
<!--                    <input type="text" id="startDate" name="startDate" style="width: 200px;" -->
<!--                        class="form-control input-datepicker"  data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy" -->
<%--                        placeholder="<spring:message code='label.dateformat' />" value="<fmt:formatDate pattern="<%=propvalue.SIMPLE_DATE_FORMAT %>"  value="${courseBatch.startDate}"/>"> --%>
<!--                </div> -->
<!--                </div> -->
                
<!--                <div class="form-group" id="endDate"> -->
<%--                <label class="col-md-3 control-label" for="batch_end"><spring:message --%>
<%--                        code='label.enddate' /><span class="text-danger">*</span></label> --%>
<!--                <div class="col-md-9"> -->
<!--                    <input type="text" id="endDate" name="endDate" style="width: 200px;" -->
<!--                        class="form-control input-datepicker"  data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy" -->
<%--                        placeholder="<spring:message code='label.dateformat' />" value="<fmt:formatDate pattern="<%=propvalue.SIMPLE_DATE_FORMAT %>"  value="${courseBatch.endDate}"/>"> --%>
<!--                </div> -->
<!--                </div> -->
                
                    
                    
                    
                    <div class="block">
            <div style="text-align: center; margin-bottom: 10px">
                    <c:choose>
                        <c:when test="${courseBatch.status eq status}">
                        <button id="coursebatch_submit"
                                class="btn btn-sm btn-primary save" type="submit">
                                <i class="fa fa-angle-right"></i>
                                <spring:message code="button.save" />
                            </button>
                            <button id="course_batch_save_submit" type="submit" style="display: none;"
                                class="btn btn-sm btn-primary save">
                                <i class="fa fa-angle-right"></i>
                                <spring:message code="button.saveandsendConfirmation" />
                            </button>
                            <button id="publish_submit" type="submit"
                                class="btn btn-sm btn-primary save">
                                <i class="fa fa-angle-right"></i>
                                <spring:message code="button.publish" />
                            </button>
                        </c:when>
                        <c:otherwise>
                            
                        </c:otherwise>
                    </c:choose>
                    <a href="<%=contexturl %>ManageCourseBatch/CourseBatchList/" class="btn btn-sm btn-primary"
                    style="text-decoration: none;"><i class="gi gi-remove"></i> <spring:message
                        code="button.back" /></a>
            </div>
        </div>
    </form>
</div>



<%@include file="../../inc/page_footer.jsp"%>
<%@include file="../../inc/template_scripts.jsp"%>



<script type="text/javascript">
$(document).ready(function() {
    $(".CourseBatch_chosen").data("placeholder","Select CourseBatch From...").chosen();
    $(".Instructor_chosen").data("placeholder","Select Instructor From...").chosen();
    
    $("#Course_Batch_Form").validate(
            {   errorClass:"help-block animation-slideDown",
                errorElement:"div",
                errorPlacement:function(e,a){a.parents(".form-group > div").append(e)},
                highlight:function(e){$(e).closest(".form-group").removeClass("has-success has-error").addClass("has-error")},
                success:function(e){e.closest(".form-group").removeClass("has-success has-error")},
                rules:{ 
                    chosenCourseId : {required : !0},
                    
                    startDate:{required:!0},
                    endDate:{required:!0}
                },
                messages:{
                    startDate: {
                        required :'<spring:message code="validation.pleaseselectstartdate"/>',
                    },
                    endDate: {
                        required :'<spring:message code="validation.pleaseselectenddate"/>',
                    },
                },
            });
    
     $("#coursebatch_submit").click(function(){
         $("#Course_Batch_Form").submit();
     });
     $("#course_batch_save_submit").click(function(){
            $("#confirmation").val(true);
            $("#Course_Batch_Form").submit(); 
         });
     $("#publish_submit").click(function(){
            $("#publish").val(true);
            $("#Course_Batch_Form").submit(); 
         });
     var value = $("#chosenInstructorId").val();
     if(value!=null){
          $("#course_batch_save_submit").show();
          $("#publish_submit").hide();
          
      }else{
          $("#course_batch_save_submit").hide();
          $("#publish_submit").show();
         
      }
     
     $("#chosenInstructorId" )
        .change(function() {
          var value = $( this ).val();
          if(value!=null){
              $("#course_batch_save_submit").show();
              $("#publish_submit").hide();
              
          }else{
              $("#course_batch_save_submit").hide();
              $("#publish_submit").show();
             
          }
        });

});
</script>