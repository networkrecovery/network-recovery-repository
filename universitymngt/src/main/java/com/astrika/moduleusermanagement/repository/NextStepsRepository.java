package com.astrika.moduleusermanagement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.astrika.moduleusermanagement.model.NextSteps;
import com.astrika.surveymngt.model.LifeStages;

public interface NextStepsRepository extends JpaRepository<NextSteps, Long> {
	
	@Query("select n from NextSteps n left join fetch n.category where n.lifestages = ?1 and n.module.moduleId = ?2")
	List<NextSteps> findByLifeStageandModuleId(LifeStages lifeStage, long moduleId);
	
	
	@Modifying
	@Transactional
	@Query("delete from NextSteps n where n.module.moduleId =?1")
	void deleteByModuleId(long moduleId);

}
