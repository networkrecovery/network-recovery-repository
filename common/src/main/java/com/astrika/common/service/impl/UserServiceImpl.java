package com.astrika.common.service.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.astrika.common.exception.DuplicateEmailException;
import com.astrika.common.exception.DuplicateLoginException;
import com.astrika.common.exception.NoSuchUserException;
import com.astrika.common.exception.PasswordException;
import com.astrika.common.model.Role;
import com.astrika.common.model.User;
import com.astrika.common.model.UserStatus;
import com.astrika.common.model.location.CityMaster;
import com.astrika.common.model.location.CountryMaster;
import com.astrika.common.model.location.StateMaster;
import com.astrika.common.model.mail.EmailData;
import com.astrika.common.repository.UserRepository;
import com.astrika.common.repository.User_audRepository;
import com.astrika.common.service.DocumentService;
import com.astrika.common.service.SendMailService;
import com.astrika.common.service.UserService;
import com.astrika.common.util.CommonUtil;
import com.astrika.common.util.PropsValues;
import com.astrika.kernel.exception.CustomException;

/**
 * Service implementation for User.
 * 
 * @author Priyanka
 */




@Service
public class UserServiceImpl implements UserService {

    private static final Logger LOGGER = Logger.getLogger(UserServiceImpl.class);

    @Autowired
    private UserRepository userRepository;




    @Autowired
    PropsValues propertyValues;

    @Autowired
    private PropsValues props;

    @Autowired
    private SendMailService mailService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private CommonUtil commonUtil;

    @Autowired
    private User_audRepository userAudRepository;

    @Autowired
    private DocumentService documentService;


    public User save(User user) {
        String encrypt = encryptPassword(user.getPassword());
        User userSave=user;

        userSave.setPassword(encrypt);
        if (userSave != null) {
            userSave = userRepository.saveAndFlush(userSave);
        }
        return userSave;
    }

    private String encryptPassword(String password) {



        if(commonUtil.isBcrypt(password)){
            return password;
        }
        return passwordEncoder.encode(password);
    }

    public boolean passwordCompare(String rawPassword, String encodedPassword) {

        return rawPassword.equals(encodedPassword);
    }

    @Override
    public void sendRegstrationMail(User user, String compName, String url,String tempPassword)
            throws DuplicateLoginException {

        String urlMail;
        double ran = Math.random() * Math.random();
        if (user != null) {
            user.setPasswordResetKey(String.valueOf(ran));
            save(user);
            urlMail = props.domainName + props.CONTEXT_URL+ "ResetPassword?token=" + ran;
            HashMap<String, Object> model = new HashMap<>();
            model.put("adminName",
                    user.getFirstName() + " " + user.getLastName());
            model.put("emailAddress", user.getEmailId());
            model.put("loginId", user.getLoginId());
            model.put("URL", urlMail);
            model.put("password", tempPassword);
            model.put("companyName", compName);
            EmailData data1 = new EmailData();
            data1.setTo(user.getEmailId());
            data1.setSubject("Welcome to Atma Network!");
            data1.setFrom(props.emailFrom);
            data1.setModel(model);
            EmailData data2 = new EmailData();
            data2.setTo(props.emailToAdmin);
            data2.setFrom(props.emailFrom);
            data2.setSubject("New Admin Details");
            data2.setModel(model);

            if (user.getRole() == Role.INSTRUCTOR) {
                data1.setMailTemplate(props.resetPasswordForInstructor);
                data2.setMailTemplate(props.restaurantAdminDetailsMail);
            } else if (user.getRole() == Role.COMPANY_ADMIN) {
                data1.setMailTemplate(props.resetPasswordForCompanyAdmin);
                data2.setMailTemplate(props.companyAdminDetailsMail);
            } else if (user.getRole() == Role.ATMA_ADMIN) {
                data1.setMailTemplate(props.resetPasswordForAtmaAdmin);
                data2.setMailTemplate(props.companyAdminDetailsMail);
            } else if (user.getRole() == Role.CORPORATE_ADMIN) {
                data1.setMailTemplate(props.resetPasswordForCorporateAdmin);
                data2.setMailTemplate(props.corporateAdminDetailsMail);
            } else if (user.getRole() == Role.STUDENT) {
                data1.setMailTemplate(props.memeberDetailsMail);
            } else if (user.getRole() == Role.MODULE_USER) {
                data1.setMailTemplate(props.resetPasswordForModuleUser);
                data2.setMailTemplate(props.moduleUserDetailsMail);
            }

            mailService.sendMail(data1);
            mailService.sendMail(data2);
        }
    }

    public List<User> findByStatus(UserStatus status) {
        return userRepository.findByStatus(status);
    }

    public User findById(long userId) throws NoSuchUserException {
        User user = userRepository.findById(userId);
        if (user == null) {
            throw new NoSuchUserException();
        }
        return user;
    }

    @Override
    public User findByLoginId(String loginId) throws NoSuchUserException {
        User user = userRepository.findByLoginId(loginId);
        if (user == null) {
            throw new NoSuchUserException();
        }
        return user;
    }

	@Override
	public User findByEmailId(String emailId) throws NoSuchUserException {
		User user = userRepository.findByEmailId(emailId);
		if (user == null) {
			throw new NoSuchUserException(
					CustomException.NO_SUCH_USER.getCode());
		}
        return user;
    }

    @Override
    public User findByPasswordToken(String token) throws NoSuchUserException {
        User user = userRepository.findByPasswordToken(token);
        if (user == null) {
            throw new NoSuchUserException(
                    CustomException.NO_SUCH_USER.getCode());
        }
        return user;
    }

    @Override
    public User update(User user) {
        return userRepository.save(user);
    }

    @Override
    public User changePassword(long userId, String password, String cPassword)
            throws NoSuchUserException {
        User user = findById(userId);
        if (password != null && password.equals(cPassword)) {
            String encrypt = encryptPassword(password);
            user.setPassword(encrypt);
        } else {
            throw new PasswordException(
                    CustomException.PASSWORD_EXCEPTION.getCode());
        }
        return userRepository.save(user);
    }

    @Override
    public User resetMemberPassword(long userId, String oldPassword,
            String password, String cPassword) throws NoSuchUserException{
        User user = findById(userId);
        if (oldPassword.equals(user.getPassword())) {
            if (password != null && password.equals(cPassword)) {
                String encrypt = encryptPassword(password);
                user.setPassword(encrypt);
            } else {
                throw new PasswordException(
                        CustomException.PASSWORD_EXCEPTION.getCode());
            }
        } else {
            throw new PasswordException(CustomException.OLD_PASSWORD.getCode());
        }
        return userRepository.save(user);
    }

    @Override
    public User changeAccountInfo(long userId, String userEmail,
            String oldPassword, String newPassword, String rePassword)
                    throws NoSuchUserException {
        User user = findById(userId);
        user.setEmailId(userEmail);
        if (oldPassword.length() > 0) {
            boolean match = passwordCompare(oldPassword, user.getPassword());
            if (match && newPassword != null && newPassword.equals(rePassword)) {
                if (newPassword.length() < 6) {
                    throw new PasswordException(
                            CustomException.PASSWORD_LENGTH.getCode());
                } else {
                    String encrypt = encryptPassword(newPassword);
                    user.setPassword(encrypt);
                }

            } else {
                throw new PasswordException(
                        CustomException.OLD_PASSWORD.getCode());
            }
        }
        userRepository.save(user);
        return userRepository.findById(userId);
    }

    @Override
    public User findByToken(String token) throws NoSuchUserException {
        return userRepository.findByToken(token);
    }

    @Override
    public void updateAll(List<User> userList) {

        userRepository.save(userList);
    }

    @Override
    public List<User> findByRoleAndStatus(Role role, UserStatus status) {
        return userRepository.findByRoleAndStatus(role, status);
    }

    @Override
    public User activateUser(long userId) throws NoSuchUserException {
        User user = findById(userId);
        user.setStatus(UserStatus.ACTIVE);
        return userRepository.save(user);
    }

    @Override
    public User inActivateUser(long userId) throws NoSuchUserException {
        User user = findById(userId);
        user.setStatus(UserStatus.INACTIVE);
        return userRepository.save(user);
    }


    public User saveUser(User user) throws DuplicateEmailException {
        User userSave=user;
        try {
            findByEmailId(userSave.getEmailId());
            throw new DuplicateEmailException(
                    CustomException.DUPLICATE_EMAIL.getCode());
        } catch (NoSuchUserException e) {
            LOGGER.info(e);
        }
        try {
            findByLoginId(userSave.getLoginId());
            throw new DuplicateLoginException(
                    CustomException.DUPLICATE_LOGIN.getCode());
        } catch (NoSuchUserException e) {
            LOGGER.info(e);

        }
        userSave.setStatus(UserStatus.ACTIVE);
        return save(userSave);

    }

    public User updateUser(long userId, String firstName, String lastName,
            String email, String mobile) throws NoSuchUserException {
        try {
            User userMaster = findByEmailId(email);
            if (userMaster != null && userMaster.getUserId() != userId) {
                throw new DuplicateLoginException(
                        CustomException.DUPLICATE_EMAIL.getCode());
            }
        } catch (NoSuchUserException e) {
            throw e;
        }
        User user = findById(userId);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setEmailId(email);
        user.setMobile(mobile);
        return update(user);
    }

    public User updateUser1(long userId, String firstName, String lastName,
            String designation, String description,
            boolean enquiryAccessRights)
                    throws NoSuchUserException {
        try {
            User userMaster = findById(userId);
            if (userMaster != null && userMaster.getUserId() != userId) {
                throw new DuplicateLoginException(
                        CustomException.DUPLICATE_EMAIL.getCode());
            }
        } catch (NoSuchUserException e) {
            throw e;
        }
        User user = findById(userId);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setDesignation(designation);
        user.setDescription(description);
        user.setEnquiryAccessRights(enquiryAccessRights);
        return update(user);
    }

    @Override
    public User save(String firstName, String lastName, String loginId,
            String password, String emailId, String mobile, Long cityId,
            Long stateId, Long countryId, String pincode, String address,
            String gender, Date birthdayDate) throws DuplicateLoginException {

        try {
            findByEmailId(emailId);
            throw new DuplicateEmailException(
                    CustomException.DUPLICATE_EMAIL.getCode());
        } catch (NoSuchUserException e) {
            LOGGER.info(e);
        }

        try {
            findByLoginId(loginId);
            throw new DuplicateEmailException(
                    CustomException.DUPLICATE_LOGIN.getCode());
        } catch (NoSuchUserException e) {
            LOGGER.info(e);
        }

        CityMaster city = new CityMaster();
        city.setCityId(cityId);
        CountryMaster country = new CountryMaster();
        country.setCountryId(countryId);
        StateMaster state = new StateMaster();
        state.setStateId(stateId);
        User user = new User(firstName, lastName, city, country,
                Role.STUDENT.getId(), emailId, mobile, state, address, pincode,
                gender, UserStatus.ACTIVE);
        return userRepository.save(user);

    }

    @Override
    public List<User> findByDesignation(String designation, UserStatus status) {

        return userRepository.findByDesignation(designation,
                status);
    }

    @Override
    public User update(long userId, String firstName, String lastName,
            String emailId, Date birthdayDate, String mobile)
                    throws NoSuchUserException {
        try {
            User userMaster = findByEmailId(emailId);
            if (userMaster != null && userMaster.getUserId() != userId) {
                throw new DuplicateEmailException(
                        CustomException.DUPLICATE_EMAIL.getCode());
            }
        } catch (NoSuchUserException e) {
            throw e;
        }

        User user = findById(userId);

        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setEmailId(emailId);
        user.setBirthdayDate(birthdayDate);
        user.setMobile(mobile);

        return update(user);
    }

    @Override
    public List<User> findByRoleAndStatusAndUser(Role role, UserStatus status,
            long userId) {
        return userRepository.findByRoleAndStatusAndUser(role,
                status, userId);
    }

    @Override
    public User save(String firstName, String lastName, String emailId,
            String loginId, String password, String designation, int role,
            Date birthdayDate, String mobile, long moduleId, String moduleName)
                    throws NoSuchUserException {
        return null;
    }

    @Override
    public User findByModuleName(String moduleName) throws NoSuchUserException {
        User user = userRepository.findByModuleName(moduleName);
        if (user == null) {
            throw new NoSuchUserException(
                    CustomException.NO_SUCH_USER.getCode());
        }
        return user;
    }

    @Override
    public User findByMaxLastLogin(Long companyId) {
        return userRepository.findByMaxLastLogin(companyId);
    }

    @Override
    public int totalNoOfUsers(Role role) {
        List<User> userList = userRepository.findByRole(role);
        if(userList.isEmpty()){
            return 0;
        }
        else{
            return userRepository.totalNoOfUsers(role);
        }
    }

    @Override
    public int totalNoOfActiveUsers(Role role, UserStatus status) {
        List<User> activeUserList = findByRoleAndStatus(role, status);
        if(activeUserList.isEmpty()){
            return 0;
        }
        else{
            return userRepository.totalNoOfActiveUsers(role, status);
        }

    }

    @Override
    public int getActiveTotalUsers(Role companyUser,Role moduleUser,UserStatus status){
        return userRepository.getActiveTotalUsers(companyUser,moduleUser,status);
    }




    @Override
    public int noOfUsersProfileReport(int start, int end) {
        return userRepository.noOfUsersProfileReport(start, end, Role.SUPER_ADMIN, Role.ATMA_ADMIN);
    }

    @Override
    public int noOfComProfileReport(int start, int end,Role role) {
        return userRepository.noOfComProfileReport(start, end, role);
    }

    @Override
    public int totalActiveUsers(UserStatus status) {
        return userRepository.totalActiveUsers(status);
    }

    @Override
    public int noOfInactiveUserExcel(int a) {
        return userRepository.noOfInactiveUserExcel(a);
    }

    
	@Override
	public long getTotaluserUptomonth(Date d) {
		return userRepository.getTotaluserUptomonth(d);
	}
    @Override
    public int noOfInactiveUser(String date, int a) {
        return userRepository.noOfInactiveUser(date,a);
    }

    @Override
    public List<Object[]> inactiveUserProfile(int a) {
        return userRepository.inactiveUserProfile(a);
    }

    @Override
    public int noOfInactiveUserGreaterThanSixMonth(String date, int a) {
        return userRepository.noOfInactiveUserGreaterThanSixMonth(date,a);
    }

    @Override
    public int fingLastMonthActiveUsers(Role companyAdmin, Role moduleUser) {
        return userRepository.fingLastMonthActiveUsers(companyAdmin,moduleUser);
    }
    @Override
    public int activeUsersMoreThanOneMonth(Role companyAdmin, Role moduleUser) {
        return userRepository.activeUsersMoreThanOneMonth(companyAdmin,moduleUser);
    }

    @Override
    public int getFullProfileComplete() {
        return userRepository.getFullProfileComplete();
    }

    @Override
    public int getTotalUser() {
        return userRepository.getTotalUser();
    }

    @Override
    public int getTUser(int yr) {
        return userRepository.getTUser(yr);
    }



    @Override
    public int getTotalNoOfUser() {
        return userAudRepository.getTotalNoOfUser();
    }

    @Override
    public List<Object[]> getAllUsers(){
        return userRepository.getAllUsers();

    }

    @Override
    public int getaciveInSecondLastMonthUsers() {
        return userRepository.getaciveInSecondLastMonthUsers();
    }

    @Override
    public int getLastAddedUserCount() {
        return userRepository.getLastAddedUserCount();
    }


    @Override
    public String getUserOfIncompleteProfile() throws IOException {

        Workbook wb = new XSSFWorkbook();
        CreationHelper createHelper = wb.getCreationHelper();
        Sheet sheet = wb.createSheet("Report");
        int rowIndex = 0;
        Row headerRow = sheet.createRow((short) rowIndex++);
        headerRow.createCell(0).setCellValue(
                createHelper.createRichTextString("Name"));
        headerRow.createCell(1).setCellValue(
                createHelper.createRichTextString("Mobile"));

        List<Object[]> resultlist = getUserOfInactiveProfile();
        java.util.Iterator<Object[]> it = resultlist.iterator();
        while(it.hasNext()){

            Object[] obj= it.next();
            Row row1=sheet.createRow(rowIndex);
            row1.createCell(0).setCellValue(createHelper.createRichTextString(obj[0].toString()+" "+obj[1].toString()));
            row1.createCell(1).setCellValue(createHelper.createRichTextString(obj[2].toString()));
            rowIndex++;
        }

        String tempUrl=propertyValues.tempCsvDownloadpath;
        FileOutputStream fileOutUrl = new FileOutputStream( tempUrl+ File.separator +"UserDetails.csv");
        String fileOut=tempUrl+ File.separator +"UserDetails.csv";
        wb.write(fileOutUrl);
        fileOutUrl.close();
        return fileOut; 
    }

    @Override
    public List<Object[]> getUserOfInactiveProfile() {

        return userRepository.getUserOfInactiveProfile();
    }



    @Override
    public int getlastMonthCreatedUser(Role companyAdmin, Role moduleUser) {

        return userRepository.getlastMonthCreatedUser(companyAdmin, moduleUser);
    }

    @Override
    public int secondLastMonthCreatedUser(Role companyAdmin, Role moduleUser) {

        return userRepository.secondLastMonthCreatedUser(companyAdmin, moduleUser);
    }



    @Override
    public List<Object[]> getInactiveUserList(long totaUser,int year,Role companyAdmin, Role moduleUser) {

        return userAudRepository.getInactiveUserList(totaUser, year,companyAdmin,moduleUser);

    }

	@Override
	public long getInactiveUserOfThatMonth(long userCount,int year,int month) {

		return userAudRepository.getInactiveUserOfThatMonth(userCount,year,month);
		
	}

	@Override
	public int getInactiveUserListExcel(long totaUser,int year,Role companyAdmin, Role moduleUser,int month) {

        return userAudRepository.getInactiveUserListExcel(totaUser, year,companyAdmin,moduleUser,month);
    }


    @Override
    public List<Object[]> getLastAddedUser() {

        List<Object[]> lastAddedUser = userRepository.getLastAddedUser();
        if(!lastAddedUser.isEmpty()){
            return lastAddedUser;
        }else{
            return Collections.emptyList();
        }
    }

    @Override
    public List<User> getInactiveFlagLastUser() {
        return userRepository.getInactiveFlagLastUser();
    }

    @Override
    public void transformPasswordFromClearToHash() {

        List<User> users =  userRepository.findAll();
        List<User> usersToUpdate = new ArrayList<>();
        for (User user : users) {
            if(!commonUtil.isBcrypt(user.getPassword())){
                user.setPassword(encryptPassword(user.getPassword()));
                usersToUpdate.add(user);
            }
        }
        LOGGER.info(String.format("Total users to update %d",usersToUpdate.size()));

        if(!usersToUpdate.isEmpty()){
            userRepository.save(usersToUpdate);
        }
    }
}
