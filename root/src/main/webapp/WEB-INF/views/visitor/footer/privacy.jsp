<!DOCTYPE html>
<html lang="en">
<%@include file="/WEB-INF/views/admin/inc/config.jsp"%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Atma Network</title>
<!-- Style -->
<!-- Bootstrap -->
<link rel="shortcut icon" href="<%=contexturl%>resources/img/favicon.ico">
<%-- <link href="<%=contexturl%>resources/css/bootstrap.css" rel="stylesheet" media="screen"> --%>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<link href="<%=contexturl%>resources/css/fontface.css" rel="stylesheet" media="screen">
<%-- <link href="<%=contexturl%>resources/css/font-awesome.css" rel="stylesheet" media="screen"> --%>
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" media="screen"> 
<!--[if IE 9]>
  <script src="<%=contexturl%>resources/js/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="<%=contexturl%>resources/js/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<!--[if IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

<link href="<%=contexturl%>resources/css/style.css" rel="stylesheet"
	media="screen">
<style type="text/css">

#privacy p {
	font-size: 13.5px;
	font-weight: 300;
	line-height: 1.25
}

</style>	
</head>


<body>
	<div class="header-bg">
		<nav class="navbar">
			<div class="container">
				<div class="navbar-header">
					<img src="<%=contexturl%>resources/img/atma.png" />
				</div>
				<div id="navbar" class="mynav">
					<ul class="nav navbar-nav pull-right">
						<security:authorize access="isAuthenticated()">
							<li class="active"><a href="<%=contexturl%>"><b>Back
										to Home Page</b></a></li>
						</security:authorize>
						<security:authorize access="isAnonymous()">
							<li class="active"><a href="<%=contexturl%>Login"><b>Back
										to Login Page</b></a></li>
						</security:authorize>
					</ul>
				</div>
			</div>
		</nav>
		<div class="blue-container">
			<div class="container"></div>
		</div>
	</div>
	<br />
	<div id="privacy" class="container">
		<div class="row">
			<h2>
				<p>
					
					<strong><h2 class="text-center">Privacy Policy</h2></strong>
				</p>
			</h2>
			<h4>
				<p>
					<small><h3 class="text-center" style="margin-top:inherit;">Information Collection &amp; Use</h3></small>
				</p>
			</h4>
			<br />
			<p>
				<strong><h4 style="color: #5bc0de">1. Preamble</h4></strong>
			</p>
			<blockquote>
				<p>Atma Education(“Atma”) is committed to safeguarding your
					privacy online. This Privacy policy is to be read as a part and
					parcel of the Terms of Use of the Atma Website, and Mobile
					Application</p>
			</blockquote>
			<p>
				<strong><h4 style="color: #5bc0de">2. Personal
						Information</h4></strong>
			</p>
			<p>
				<strong><h5 style="color: #5bc0de">2.1 How Personal
						Information is Collected and Used</h5></strong>
			</p>
			<p>
				<strong><h5 style="color: #5ba8de">Registration</h5></strong>
			</p>
			<blockquote>
				<p>During registration you are required to give contact
					information (such as name and a verifiable email address). Atma
					uses this information to identify you and/or contact you about the
					services on our site in which you have expressed interest.</p>
				<p>You may optionally provide other related information if
					applicable (such as name, position, professional license number,
					organization address, organization phone number, organization
					website, organization logo or property photos) to Atma; we
					encourage you to submit this information accurately so it can be
					reflected properly on any saved data, printed data or pdf created
					by you through the Site.</p>
			</blockquote>
			<p>
				<strong><h5 style="color: #5ba8de">Surveys</h5></strong>
			</p>
			<blockquote>
				<p>From time-to-time Atma may provide you the opportunity to
					participate in surveys on theSite. If you participate, Atma will
					request certain personally identifiable information from you.
					Participation in these surveys is completely voluntary and you
					therefore have a choice whether or not to disclose this information</p>
			</blockquote>

			<p>
				<strong><h5 style="color: #5bc0de">2.2 Use of Personal
						Information Within Atma</h5></strong>
			</p>
			<p>
				<strong><h5 style="color: #5ba8de">Supplementation of
						Information</h5></strong>
			</p>
			<blockquote>
				<p>In order to provide certain valuation or marketing based
					services to you, Atma may on occasion supplement the personal
					information you submitted to Atma with information from third party
					sources.</p>
			</blockquote>

			<p>
				<strong><h5 style="color: #5ba8de">Profile</h5></strong>
			</p>
			<blockquote>
				<p>Atma may store information that it collects through cookies,
					sessions, log files, clear gifs, web beacons, and/or third party
					sources to create a “profile” of your preferences. Atma may tie
					your personally identifiable information to information in the
					profile, in order to provide tailored marketing offers and to
					improve the content of the site for you.</p>
				<p>Atma does not link aggregate user data with personally
					identifiable information other than what is explained above under
					‘Profile’.</p>
			</blockquote>

			<p>
				<strong><h5 style="color: #5ba8de">Updates,
						Newsletters and Service-related Announcements</h5></strong>
			</p>
			<blockquote>
				<p>Based on Personal Information you provide Atma through the
					Site, Atma will occasionally send you information on updates to the
					site, newsletters and service related-announcements.</p>
			</blockquote>

			<p>
				<strong><h5 style="color: #5ba8de">Log Files</h5></strong>
			</p>
			<blockquote>
				<p>As is true of most Web sites, Atma may gather certain
					information automatically and store it in log files. This
					information includes internet protocol (IP) addresses, browser
					type, internet service provider (ISP), referring/exit pages,
					operating system, date/time stamp, and clickstream data.</p>
				<p>Atma uses this information, which does not identify
					individual users, to analyze trends, to administer the Site, to
					track users’ movements around the Site and to gather demographic
					information about Atma’s user base as a whole.</p>
				<p>Atma may link this automatically-collected data to personally
					identifiable information. IP addresses are tied to personally
					identifiable information to ensure that Atma’sTerms of Use are
					complied with.</p>
			</blockquote>

			<p>
				<strong><h5 style="color: #5ba8de">Customer Service</h5></strong>
			</p>
			<blockquote>
				<p>Based upon the personally identifiable information you
					provide Atma, Atma will send you a welcoming email to verify your
					email address. Atma will also communicate with you in response to
					your inquiries, to provide the services you request, and to manage
					your account.</p>
			</blockquote>

			<p>
				<strong><h5 style="color: #5bc0de">2.3 When Personal
						Information is shared outside Atma</h5></strong>
			</p>
			<blockquote>
				<p>Atma will not share the personally identifiable information
					you provide through a survey with other third parties unless Atma
					gives you prior notice and choice.</p>
				<p>Atma does not share your profile individually with other
					third parties. If shared, Atma shares your profile in aggregate
					form only. This may be used to determine the number of unique users
					or number of unique requests to our site or aggregated data based
					on unique user provided data which could be shown on theSite or in
					media releases. For example Atma may show the number of unique
					users by state code without individually identifying you or your
					exact location.</p>
				<p>Atmamay share personally identifiable information with third
					parties for the purposes of determining the number of unique users
					or unique page views. This information may include location and/or
					device information.</p>
			</blockquote>

			<p>
				<strong><h5 style="color: #5bc0de">2.4 Security of
						Your Personal Information</h5></strong>
			</p>

			<blockquote>
				<p>The security of your personal information is important to
					Atma. When you enter sensitive information such as passwords on our
					registration or login forms, Atma encrypts that information using
					secure socket layer technology (SSL).</p>
				<p>Atma follows generally accepted industry standards to protect
					the personal information submitted to Atma, both during
					transmission and upon Atma’s receipt of it. However, no method of
					transmission over the Internet, or method of electronic storage, is
					100% secure. Therefore, while Atma strives to use commercially
					acceptable means to protect your personal information, Atma cannot
					guarantee its absolute security.</p>

			</blockquote>

			<p>
				<strong><h4 style="color: #5bc0de">3. Non-Personal
						Information</h4></strong>
			</p>
			<blockquote>
				<p>Atma shares aggregated demographic information about Atma’s
					user base with Atma’s partners and advertisers. This information
					does not identify individual users. This may be used for targeted
					add marketing or to develop aggregated user data for media
					purposes.</p>
				<p>Atma may also use third party applications,to track
					non-personally identifiable information about visitors to theSite
					in the aggregate. The data collected may contain but may not be
					limited to device or location data which helps Atma improve your
					unique experience.</p>
			</blockquote>

			<p>
				<strong><h4 style="color: #5bc0de">4. Special
						Information Transmission Clauses</h4></strong>
			</p>
			<p>
				<strong><h5 style="color: #5bc0de">4.1 Cookies</h5></strong>
			</p>
			<blockquote>
				<p>A cookie is a small text file that is stored on a user’s
					computer for record-keeping purposes. Atma may use cookies on
					theSite. Atma uses both session ID cookies and/or persistent
					cookies. Atma uses session cookies to make it easier for you to
					navigate theSite. A session ID cookie expires when you close your
					browser. A persistent cookie remains on your hard drive for an
					extended period of time. You can remove persistent cookies by
					following directions provided in your Internet browser’s “help”
					file</p>
				<p>Atma may set a persistent cookie to store your passwords, so
					you don’t have to enter it more than once. Persistent cookies also
					enable Atma to track and target the interests of Atma’s users to
					enhance the experience on theSite.</p>
				<p>If you reject cookies, your ability to use some areas of
					theSite will be limited. Some of Atma’s partners (e.g.,
					advertisers) may use cookies on theSite. Atma has no access to or
					control over these cookies.</p>
				<p>This privacy statement covers the use of cookies by Atma only
					and does not cover the use of cookies by any advertisers.</p>
			</blockquote>

			<p>
				<strong><h5 style="color: #5bc0de">4.2 Clear Gifs(Web
						Beacons/Web Bugs)</h5></strong>
			</p>
			<blockquote>
				<p>Atma may employ a software technology called clear gifs
					(a.k.a. Web Beacons/Web Bugs), that helpsAtma better manage content
					on theSite by informing Atma what content is effective. Clear gifs
					are tiny graphics with a unique identifier, similar in function to
					cookies, and are used to track the online movements of Web users.
					In contrast to cookies, which are stored on a user’s computer hard
					drive, clear gifs are embedded invisibly on Web pages and are about
					the size of the period at the end of this sentence. Atma may tie
					the information gathered by clear gifs to Atma’s customers’
					personally identifiable information.</p>
				<p>Atma may use clear gifs in Atma’s HTML-based emails to let
					Atma know which emails have been opened by recipients. This allows
					Atma to gauge the effectiveness of certain communications and the
					effectiveness of Atma’s marketing campaigns.</p>

			</blockquote>

			<p>
				<strong><h5 style="color: #5bc0de">4.3 Information
						Accessible through Links to Other Websites on the Site</h5></strong>
			</p>
			<blockquote>
				<p>This Web site contains links to other sites that are not
					owned or controlled by Atma. Please be aware that Atmais not
					responsible for the privacy practices of such other websites. Atma
					encourages you to be aware when you leave theSite and to read the
					privacy statements of each and every website that collects
					personally identifiable information.</p>
				<p>This privacy statement applies only to information collected
					by theSite.</p>
			</blockquote>

			<p>
				<strong><h4 style="color: #5bc0de">5. Disclosure of
						Information</h4></strong>
			</p>
			<blockquote>
				<p>Atma reserves the right to disclose your personally
					identifiable information as required by law and when Atma believes
					that disclosure is necessary to protect Atma’s rights and/or to
					comply with a judicial proceeding, court order, or legal process
					served on our Web site. The legal proceeding or request may
					prohibit Atma from contacting you and informing you of this
					request.</p>

			</blockquote>

			<p>
				<strong><h4 style="color: #5bc0de">6. Amendments to
						the Privacy Policy</h4></strong>
			</p>
			<blockquote>
				<p>Atma may amend this policy from time to time. Use of
					information Atma collects now is subject to the Privacy Policy in
					effect at the time such information is used. The most current
					version of the Atma Privacy Policy will govern Atma’s collection,
					use, and disclosure of information about you. By continuing to
					access or use the Site after those changes become effective, you
					agree to the revised Privacy Policy.</p>
			</blockquote>

			<p>
				<strong><h4 style="color: #5bc0de">7. Contact Us</h4></strong>
			</p>
			<blockquote>
				<p>If you have any questions or suggestions regarding Atma’s
					privacy policy, please contact Atma at: network@atma.org.in.</p>
			</blockquote>


		</div>
	</div>
	<br />
	<br />
	<br />
	<br />
	<div class="container" id="footer">
		<%@include file="../../visitor/inc/footer_items.jsp"%>
	</div>
</body>
<%-- <script src="<%=contexturl%>resources/js/vendor/jquery-1.11.0.min.js"></script> --%>
<%-- <script src="<%=contexturl%>resources/js/vendor/bootstrap.min.js"></script> --%>
<script src="${pageContext.request.contextPath}/webjars/counter-webapp-frontend/0.0.1/vendor_bundle.js"></script>


</html>
