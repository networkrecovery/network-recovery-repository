
<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/template_start.jsp"%>
<%@include file="../../inc/page_head.jsp"%>
<style>

.chosen-container {
	width: 250px !important;
}
 
</style>
<div id="page-content">
	<div class="content-header">
		<div class="header-section">
			<c:if test="${!empty success}">
				<div class="alert alert-success alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-check-circle"></i>
						<spring:message code="label.success" />
					</h4>
					<spring:message code="${success}" />
					<br>
				</div>
			</c:if>
			<c:if test="${!empty error}">
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-times-circle"></i>
						<spring:message code="label.error" />
					</h4>
					<spring:message code="${error}" />
				</div>
			</c:if>
			<h1>
				<!-- 				<i class="fa fa-map-marker"></i> -->
				<spring:message code="heading.profileupdation" />
				<br> <small></small>
			</h1>
			<span id="errorMsg"></span>
		</div>
	</div>
	<form action="UpdateProfile" method="post" class="form-horizontal "
		id="profile_form">
		<!-- 		enctype="multipart/form-data"> -->

		<input type="hidden" name="client" value="WEB" />
		<div class="row">

			<div class="">
				<div class="block">
					<div class="block-title">
						<h2>
							<strong><spring:message code="heading.userinfo" /></strong>
						</h2>
					</div>


					<div class="form-group">
						<label class="col-md-4 control-label" for="first_Name"><spring:message
								code="label.firstname" /><span class="text-danger">*</span> </label>
						<div class="col-md-6">
							<input id="first_Name" name="firstName" class="form-control"
								placeholder="<spring:message code='label.firstname'/>.."
								type="text" value="${user.firstName}">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label" for="last_Name"><spring:message
								code="label.lastname" /><span class="text-danger">*</span> </label>
						<div class="col-md-6">

							<input id="last_Name" name="lastName" class="form-control"
								placeholder="<spring:message code="label.lastname"/>.."
								type="text" value="${user.lastName}">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label" for="mobile"><spring:message
								code="label.mobile" /><span class="text-danger">*</span></label>
						<div class="col-md-6">
							<input id="mobile" name="mobile" class="form-control"
								placeholder='<spring:message code="label.mobile"></spring:message>'
								type="text" value="${user.mobile}">
						</div>
					</div>

					<div class="form-group">
						<a href="#reset-user-settings" data-toggle="modal"
							class="col-md-4 control-label"><spring:message
								code="label.resetpassword" /></a>
					</div>
					<div style="text-align: center; margin-bottom: 10px">
						<div class="form-group form-actions">

							<!-- 							<div class="col-md-9 col-md-offset-3"> -->
							<button id="profile_submit" type="submit"
								class="btn btn-sm btn-primary save">
								<i class="fa fa-angle-right"></i>
								<spring:message code="button.save" />
							</button>
							<a class="btn btn-sm btn-primary save"
								href="<%=contexturl%>Index"> <spring:message
									code="button.back" />
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>

	<div id="reset-user-settings" class="modal fade" tabindex="-1"
		role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header text-center">
					<h2 class="modal-title">
						<i class="fa fa-pencil"></i>
						<spring:message code="label.resetpassword" />
					</h2>
				</div>
				<span id="passwordErrorMsg"></span>
				<div class="modal-body">
					<form action="<%=contexturl%>Admin/ResetUserPassword"
						method="post" class="form-horizontal form-bordered"
						id="ResetPassword_Form">
						<input name="userId" value="${user.userId}" type="hidden">
						<fieldset>
							<div class="form-group">
								<label class="col-md-4 control-label"
									for="user-settings-oldpassword"><spring:message
										code="label.oldpassword" /></label>
								<div class="col-md-8">
									<input type="password" id="oldpassword" name="oldpassword"
										class="form-control" placeholder="Current Password..">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label"
									for="user-settings-password"><spring:message
										code="label.newpassword" /></label>
								<div class="col-md-8">
									<input type="password" id="password" name="password"
										class="form-control"
										placeholder="Please choose a complex one..">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label"
									for="user-settings-repassword"><spring:message
										code="label.confirmnewpassword" /></label>
								<div class="col-md-8">
									<input type="password" id="user-settings-repassword"
										name="confirmPassword" class="form-control"
										placeholder="..and confirm it!">
								</div>
							</div>
						</fieldset>
						<div class="form-group form-actions">
							<div class="col-xs-12 text-right">
								<button type="button" class="btn btn-sm btn-default"
									data-dismiss="modal">
									<spring:message code="label.close" />
								</button>
								<button id="submit_password" class="btn btn-sm btn-primary save"
									type="submit">
									<spring:message code="label.savechanges" />
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<%@include file="../../inc/page_footer.jsp"%>
<%@include file="../../inc/template_scripts.jsp"%>
<%@include file="../../inc/chosen_scripts.jsp"%>
<script src="<%=contexturl %>resources/js/additional-methods.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
						$(".chosen").chosen();
						$(".state_chosen").chosen();
						$(".city_chosen").chosen();
						$(".country_chosen").chosen();
						$(".gender_chosen").chosen();
						$(".cuisine_chosen").chosen();
						$('#birthdayDate_datepicker').datepicker('setEndDate', new Date());

						$("#profile_form")
								.validate(
										{
											errorClass : "help-block animation-slideDown",
											errorElement : "div",
											errorPlacement : function(e, a) {
												a.parents(".form-group > div")
														.append(e);
											},
											highlight : function(e) {
												$(e)
														.closest(".form-group")
														.removeClass(
																"has-success has-error")
														.addClass("has-error")
											},
											success : function(e) {
												e
														.closest(".form-group")
														.removeClass(
																"has-success has-error")
											},
											rules : {

												firstName : {
													required : !0,
													maxlength : 75
												},
												lastName : {
													required : !0,
													maxlength : 75
												},
												mobile : {
													required : !0, 
													mobile : !0,
													maxlength : 10
												},
													password : {
													required : !0,
													minlength : 6,
													maxlength : 75
												},
												confirmPassword : {
													required : !0,
													equalTo : "#password"
												},										
											},
											messages : {

												firstName : {
													required : '<spring:message code="validation.pleaseenterfirstname"/>',
													maxlength : '<spring:message code="validation.firstname75character"/>'
												},
												lastName : {
													required : '<spring:message code="validation.pleaseenterlastname"/>',
													maxlength : '<spring:message code="validation.lastname75character"/>'
												},
												mobile : {
													required :'<spring:message code="validation.pleaseenteramobilenumber"/>',
													mobile :'<spring:message code="validation.phone"/>',
													maxlength :'<spring:message code="validation.mobile10character"/>'												},
												password : {
													required : '<spring:message code="validation.pleaseenterapassword"/>',
													minlength : '<spring:message code="validation.passwordmustbeatleast6character"/>',
													maxlength : '<spring:message code="validation.passworddname75character"/>'
												},
												confirmPassword : {
													required : '<spring:message code="validation.pleaseconfirmpassword"/>',
													equalTo : '<spring:message code="validation.pleaseenterthesamepasswordasabove"/>'
												},

											},

										});
						
						$("#profile_submit").click(function(){
							$("#profile_form").submit();
						});


					});
</script>



<%@include file="../../inc/template_end.jsp"%>