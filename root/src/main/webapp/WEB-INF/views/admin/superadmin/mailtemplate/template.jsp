<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/template_start.jsp"%>
<%@include file="../../inc/page_head.jsp"%>


<style>

.chosen-container {
	width: 250px !important;
}
 #map-canvas { height: 300px;}
</style>
<div id="page-content">
<div class="content-header">
	<div class="header-section">
		<h1>
			<i class="fa fa-map-marker"></i>
			<spring:message code="heading.emailtemplates" />
			<br> <small><spring:message
					code="heading.listofmails" /> </small>
		</h1>
		<span id="errorMsg"></span>
		<c:if test="${!empty success}">
			<div class="alert alert-success alert-dismissable">
				<button type="button" id="success_close" class="close" data-dismiss="alert"
					aria-hidden="true">x</button>
				<h4>
					<i class="fa fa-check-circle"></i> <spring:message code="label.success"/>
				</h4>
				<spring:message code="${success}" />
			</div>
		</c:if>
		<c:if test="${!empty error}">
			<div class="alert alert-danger alert-dismissable">
				<button type="button" class="close" data-dismiss="alert"
					aria-hidden="true">x</button>
				<h4>
					<i class="fa fa-times-circle"></i> <spring:message code="label.error"/>
				</h4>
				<spring:message code="${error}" />
			</div>
		</c:if>
	</div>
</div>
<ul class="breadcrumb breadcrumb-top">
	<li><spring:message code="menu.manageemailtemplates" /></li>
	<li><a href="#"><spring:message code="label.emailtemplates" /></a></li>
</ul>
<div class="block full gridView">
	<div class="block-title">
		<h2>
			<strong><spring:message code="heading.emailtemplates" /></strong>
		</h2>
	</div>
	<div class="table-responsive">
		<table id="active-datatable"
			class="table table-vcenter table-condensed table-bordered">
			<thead>
				<tr>
					<th class="text-center"><spring:message code="label.id" /></th>
					<th class="text-center">Template Name</th>
					<th class="text-center"><spring:message code="label.actions" /></th>
				</tr>
			</thead>
			<tbody id="tbody">
				<c:forEach var="template" items="${templateList}" varStatus="count">
					<tr>
						<td class="text-center">${count.count}</td>
						<td class="text-left" style="padding-left: 35%"><c:out
								value="${template.templateName}" /></td>
						<td class="text-center">
							<div class="btn-group">
								<a href="<%=contexturl %>ManageTemplate/EditTemplate?id=${template.id}" data-toggle="tooltip" title="Edit"
									class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>
							</div>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
	</div>
</div>

<%@include file="../../inc/page_footer.jsp"%>
<%@include file="../../inc/template_scripts.jsp"%>

<script type="text/javascript">
          	$(document).ready(function() {
          		
          	});
		
</script>
<%@include file="../../inc/template_end.jsp"%>