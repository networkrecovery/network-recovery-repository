package com.astrika.common.exception;

import com.astrika.kernel.exception.BusinessException;
import com.astrika.kernel.exception.CustomException;


public class DuplicateLoginException extends BusinessException{
	
	
	public DuplicateLoginException() {
		super(CustomException.DUPLICATE_LOGIN.getCode());
	}

	public DuplicateLoginException(String errorCode){
		super(errorCode);
	}
}
