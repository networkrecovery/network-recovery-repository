<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/template_start.jsp"%>
<%@include file="../../inc/page_head.jsp"%>



<div id="page-content">
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="fa fa-map-marker"></i>
                <spring:message code="heading.batchinvitation" />
                <br> <small><spring:message
                        code="heading.specifylistofbatchinvitation" /> </small>
            </h1>
            <c:if test="${!empty success}">
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert"
                        aria-hidden="true">x</button>
                    <h4>
                        <i class="fa fa-check-circle"></i>
                        <spring:message code="label.success" />
                    </h4>
                    <spring:message code="${success}" />
                </div>
            </c:if>
            <div class="alert alert-danger alert-dismissable"
                style="display: none" id="error">
                <button type="button" class="close" data-dismiss="alert"
                    aria-hidden="true">x</button>
                <h4>
                    <i class="fa fa-times-circle"> </i> <spring:message code="label.error"/>
                </h4>
                <span id="errorMsg"></span>
            </div>
        </div>
    </div>
    <ul class="breadcrumb breadcrumb-top">
        <li><spring:message code="heading.managebatchinvitation" /></li>
        <li><a href="#"><spring:message code="heading.batchinvitation" /></a></li>
    </ul>
    <div class="block full  gridView">
        <div class="block-title">
            <h2>
                <strong><spring:message code="heading.batchinvitationlist" /></strong>
            </h2>
        </div>
        <div class="table-responsive">
            <table id="active-datatable"
                class="table table-vcenter table-condensed table-bordered">
                <thead>
                    <tr>
                        <th class="text-center"><spring:message code="label.id" /></th>
                        <th class="text-center"><spring:message code="label.coursename" /></th>
                        <th class="text-center"><spring:message code="label.startdate" /></th>
                        <th class="text-center"><spring:message code="label.enddate" /></th>
                        <th class="text-center"><spring:message code="label.batchstatus" /></th>
                        <th class="text-center"><spring:message code="label.yourstatus" /></th>
                        <th class="text-center"><spring:message code="label.actions" /></th>
                    </tr>
                </thead>
                <tbody id="tbody">
                    <c:forEach var="courseBatchInstructor" items="${courseBatchInstructorList}"
                        varStatus="count">

                        <tr>
                            <td class="text-center">${count.count}</td>
                            <td class="text-center"><c:out
                                    value="${courseBatchInstructor.courseBatch.course.courseName}" /></td>
                                    
                            <td class="text-left"><fmt:formatDate
                                    pattern="<%=propvalue.specialDateFormat %>" value="${courseBatchInstructor.courseBatch.startDate}" /></td> 
                            <td class="text-left"><fmt:formatDate
                                    pattern="<%=propvalue.specialDateFormat %>" value="${courseBatchInstructor.courseBatch.endDate}" /></td> 
                            <td class="text-center"><c:out
                                    value="${courseBatchInstructor.courseBatch.status}" /></td>
                            <td class="text-center"><c:out
                                    value="${courseBatchInstructor.status}" /></td>
                            <td class="text-center">
                                <div class="btn-group">
                                <c:choose>
                                <c:when test="${courseBatchInstructor.courseBatch.status eq status}">
                                <c:out value="COURSE ${courseBatchInstructor.courseBatch.status}"></c:out>
                                </c:when>
                                <c:when test="${courseBatchInstructor.courseBatch.status eq status2}">
                                <c:out value="COURSE ${courseBatchInstructor.courseBatch.status}"></c:out>
                                </c:when>
                                <c:otherwise>
                                    <a href="#accept_invite_pop" data-toggle="modal"
                                        onclick="acceptInvite(${courseBatchInstructor.courseBatchInstructorId})"
                                        title="Accept" class="btn btn-xs btn-success"><i
                                        class="fa fa-check fa-fw"></i> </a>
                                    <a href="#reject_invite_pop" data-toggle="modal"
                                        onclick="rejectInvite(${courseBatchInstructor.courseBatchInstructorId})"
                                        title="Reject" class="btn btn-xs btn-danger"><i
                                        class="fa fa-times"></i></a>
                                </c:otherwise>
                                </c:choose>
                                </div>
                                
                            </td>
                        </tr>
                    </c:forEach>

                </tbody>
            </table>
        </div>
    </div>

    <div id="accept_invite_pop" class="modal fade" tabindex="-1"
        role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <form action="<%=contexturl %>ManageBatchInvitation/BatchInviationList/AcceptBatchInvite" method="post" class="form-horizontal form-bordered"
                        id="accept_invite_Form">
                        <div style="padding: 10px; height: 110px;">
                            <label><spring:message
                                    code="validation.doyouwanttoacceptthecoursebatchinvite" /></label>
                            <div class="col-xs-12 text-right">
                                <button type="button" class="btn btn-sm btn-default"
                                    data-dismiss="modal">
                                    <spring:message code="label.no" />
                                </button>
                                <div id="accept_invite" class="btn btn-sm btn-primary">
                                    <spring:message code="label.yes" />
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="id" id="acceptedId">
                    </form>

                </div>
            </div>
        </div>
    </div>


    <div id="reject_invite_pop" class="modal fade" tabindex="-1"
        role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <form action="<%=contexturl %>ManageBatchInvitation/BatchInviationList/RejectBatchInvite" method="post" class="form-horizontal form-bordered" id="reject_invite_Form">
                        
                        <div style="padding: 10px; height: 110px;">
                            <label><spring:message
                                    code="validation.doyouwanttorejectthecoursebatchinvite" /></label>
                            <div class="col-xs-12 text-right">
                                <button type="button" class="btn btn-sm btn-default"
                                    data-dismiss="modal"><spring:message code="label.no" /></button>
                                <div id="reject_invite" class="btn btn-sm btn-primary"> <spring:message code="label.yes" /></div>
                            </div>
                        </div>
                        <input type="hidden" name="id" id="rejectedId">
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>

<%@include file="../../inc/page_footer.jsp"%>
<%@include file="../../inc/template_scripts.jsp"%>
<script src="<%=contexturl%>resources/js/pages/activeTablesDatatables.js"></script>
<script src="<%=contexturl%>resources/js/pages/inactiveTablesDatatables.js"></script>
<script>$(function(){ ActiveTablesDatatables.init(6); });</script>
<script>$(function(){ InActiveTablesDatatables.init(6); });</script>
<script type="text/javascript">
    var rejectedId = 0;
    var acceptedId=0;
    
    $("#reject_invite").click(function(){
        
        $("#rejectedId").val(rejectedId);
        $("#reject_invite_Form").submit();
    });
    
    $("#accept_invite").click(function(){
        
        $("#acceptedId").val(acceptedId);
        $("#accept_invite_Form").submit();
    });
    
    function rejectInvite(id){
        rejectedId = id;
    }
    function acceptInvite(id){
        acceptedId = id;
    }
</script>
<%@include file="../../inc/template_end.jsp"%>