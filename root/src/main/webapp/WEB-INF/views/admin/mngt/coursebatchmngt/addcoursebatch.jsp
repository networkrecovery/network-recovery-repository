

<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/template_start.jsp"%>
<%@include file="../../inc/page_head.jsp"%>

<div id="page-content">
    <div class="content-header">
        <div class="header-section">
            <c:if test="${!empty error}">
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert"
                        aria-hidden="true">x</button>
                    <h4>
                        <i class="fa fa-times-circle"></i> <spring:message code="label.error"/>
                    </h4>
                    <spring:message code="${error}" />
                </div>
            </c:if>

            <h1>
                <i class="fa fa-map-marker"></i>
                <spring:message code="heading.coursebatch" />
                <br> <small><spring:message
                        code="heading.coursebatchdetails" /></small>
            </h1>
            <span id="errorMsg"></span>
        </div>
    </div>
    <ul class="breadcrumb breadcrumb-top">
        <li><spring:message code="heading.coursebatch" /></li>
    </ul>
    <form action="<%=contexturl %>ManageCourse/CourseBatchList/SaveCourseBatch" method="post" class="form-horizontal ui-formwizard"
        id="Course_Batch_Form" >
        <input id="confirmation" name="confirmation" type="hidden" value="false">
        <input id="publish" name="publish" type="hidden" value="false">
        <div class="col-md-12">
                <div class="block">
                    <div class="block-title">
                        <h2>
                            <strong><spring:message code="heading.coursebatchinfo" /></strong>
                        </h2>
                    </div>
                                        
                <div class="form-group" id="CourseBatchDiv">
                    <label class="col-md-3 control-label" for="CourseBatch_Name"><spring:message
                            code="label.course" /><span class="text-danger">*</span></label>
                    <div class="col-md-7">
                        <select class="CourseBatch_chosen" style="width: 400px;" id="chosenCourseId"
                            name="chosenCourseId"
                            data-placeholder="<spring:message code='label.choosecourse' />">
                            <option value=""></option>
                            <c:forEach items="${courseList}" var="course">
                                <option value="${course.courseId}">${course.courseName}</option>
                            </c:forEach>
                        </select>
                    </div>

                </div>
                <div class="form-group" id="InstructorDiv">
                    <label class="col-md-3 control-label" for="countryFrom_Name"><spring:message
                            code="heading.instructor" /></label>
                    <div class="col-md-7">
                        <select multiple class="Instructor_chosen" style="width: 400px;" id="chosenInstructorId"
                            name="chosenInstructorId"
                            data-placeholder="<spring:message code='label.chooseinstructor' />">
                            <option value=""></option>
                            
                            <c:forEach items="${instructorList}" var="instructor">
                            <c:set var="present" value="false" />
                                        <c:forEach items="${instructors}" var="instructors">
                                            <c:if test="${instructor.user.userId eq instructors.user.userId}">
                                            <c:set var="present" value="true" />    
                                            </c:if>
                                            
                                        </c:forEach>
                                        
                                        <option value="${instructor.user.userId}"
                                            <c:if test="${present}">    selected="selected" </c:if>>
                                            ${instructor.user.firstName} ${ instructor.user.lastName}</option>
                                        
                            </c:forEach>
                        </select>
                    </div>

                </div>
                
                <div class="form-group">
                        <label class="col-md-3 control-label" for="example-daterange1"><spring:message code="label.batchperiod"/>
                            </label>
                        <div class="col-md-7">
                            <div class="input-group input-daterange"
                                data-date-format="mm/dd/yyyy">
                                <input type="text" id="startDate"
                                    name="startDate" class="form-control text-center" data-date-format="MM/dd/yyyy"
                                    placeholder="From" value="<fmt:formatDate pattern="<%=propvalue.dateFormat %>"  value="${courseBatch.startDate}"/>"> <span class="input-group-addon"><i
                                    class="fa fa-angle-right"></i></span> <input type="text"
                                    id="endDate" name="endDate"
                                    class="form-control text-center" placeholder="To" data-date-format="MM/dd/yyyy" value="<fmt:formatDate pattern="<%=propvalue.dateFormat%>"  value="${courseBatch.startDate}"/>">
                            </div>
                        </div>
                    </div>
                
<!--                <div class="form-group" id="startDate"> -->
<%--                <label class="col-md-3 control-label" for="batch_start"><spring:message --%>
<%--                        code='label.startdate' /><span class="text-danger">*</span></label> --%>
<!--                <div class="col-md-9"> -->
<!--                    <input type="text" id="startDate" name="startDate" style="width: 200px;" -->
<!--                        class="form-control input-datepicker"  data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy" -->
<%--                        placeholder="<spring:message code='label.dateformat' />" value="<fmt:formatDate pattern="<%=propvalue.SIMPLE_DATE_FORMAT %>"  value="${courseBatch.startDate}"/>"> --%>
<!--                </div> -->
<!--                </div> -->
                
<!--                <div class="form-group" id="endDate"> -->
<%--                <label class="col-md-3 control-label" for="batch_end"><spring:message --%>
<%--                        code='label.enddate' /><span class="text-danger">*</span></label> --%>
<!--                <div class="col-md-9"> -->
<!--                    <input type="text" id="endDate" name="endDate" style="width: 200px;" -->
<!--                        class="form-control input-datepicker"  data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy" -->
<%--                        placeholder="<spring:message code='label.dateformat' />" value="<fmt:formatDate pattern="<%=propvalue.SIMPLE_DATE_FORMAT %>"  value="${courseBatch.endDate}"/>"> --%>
<!--                </div> -->
<!--                </div> -->
                
                
            </div>
            </div>
            <div class="form-group form-actions">
                <div class="col-md-9 col-md-offset-4">
                    <button id="coursebatch_submit" type="submit"
                        class="btn btn-sm btn-primary save">
                        <i class="fa fa-angle-right"></i> <spring:message code="button.save" />
                    </button>
                    <button id="course_batch_save_submit" type="submit" style="display: none;"
                        class="btn btn-sm btn-primary save">
                        <i class="fa fa-angle-right"></i> <spring:message code="button.saveandsendConfirmation" />
                    </button>
                    <button id="publish_submit" type="submit"
                        class="btn btn-sm btn-primary save">
                        <i class="fa fa-angle-right"></i> <spring:message code="button.publish" />
                    </button>
                    <button type="reset" class="btn btn-sm btn-warning">
                        <i class="fa fa-repeat"></i>
                        <spring:message code="button.reset" />
                    </button>
                    <a class="btn btn-sm btn-primary" href="<%=contexturl %>ManageCourseBatch/CourseBatchList/"><i class="gi gi-remove"></i>
                            <spring:message code="button.cancel" /> </a>
                </div>
            </div>
    </form>
</div>
<%@include file="../../inc/page_footer.jsp"%>
<%@include file="../../inc/template_scripts.jsp"%>

<script type="text/javascript">
$(document).ready(function() {
    $(".CourseBatch_chosen").data("placeholder","Select CourseBatch From...").chosen();
    $(".Instructor_chosen").data("placeholder","Select Instructor From...").chosen();
    
    
    $("#Course_Batch_Form").validate(
            {   errorClass:"help-block animation-slideDown",
                errorElement:"div",
                errorPlacement:function(e,a){a.parents(".form-group > div").append(e)},
                highlight:function(e){$(e).closest(".form-group").removeClass("has-success has-error").addClass("has-error")},
                success:function(e){e.closest(".form-group").removeClass("has-success has-error")},
                rules:{ 
                    chosenCourseId : {required : !0},
                    startDate:{required:!0},
                    endDate:{required:!0}
                },
                messages:{
                    
                    startDate: {
                        required :'<spring:message code="validation.pleaseselectstartdate"/>',
                    },
                    endDate: {
                        required :'<spring:message code="validation.pleaseselectenddate"/>',
                    },
                },
            });
    
     $("#coursebatch_submit").click(function(){
         $("#Course_Batch_Form").submit();
     });
     
     $("#course_batch_save_submit").click(function(){
            $("#confirmation").val(true);
            $("#Course_Batch_Form").submit(); 
    });
     
     $("#publish_submit").click(function(){
            $("#publish").val(true);
            $("#Course_Batch_Form").submit(); 
         });
     
     $("#chosenInstructorId" )
        .change(function() {
          var value = $( this ).val();
          if(value!=null){
              $("#course_batch_save_submit").show();
              $("#publish_submit").hide();
              
          }else{
              $("#course_batch_save_submit").hide();
              $("#publish_submit").show();
             
          }
        });
     
     
     


     $('select[name="chosenCourseId"]').chosen().change( function() {
            
            var courseId = ($(this).val());
            $.ajax({
                type: "GET",
                async: false,
                url: "<%=contexturl %>/instructorbycourseid/"+courseId,
                success: function(data, textStatus, jqXHR){
                    var obj = jQuery.parseJSON(data);
                    var len=obj.length;
                    var myOptions = "<option value></option>";
                    
                
                    
                    <c:forEach items="${instructorList}" var="instructor">
                    var value = ${instructor.userId};
                    var name = "${instructor.user.firstName} ${instructor.user.lastName}";
                    var result = true;
                    for(var i=0; i<len; i++){
                        
                        if(value == obj[i].userId){
                            result= false;
                            myOptions += '<option value="'+obj[i].userId+'" selected="selected" >'+obj[i].user.firstName+" "+obj[i].user.lastName+'</option>';
                        }
                    
                    }
                    if(result){
                    myOptions += '<option value="'+value+'">'+name+'</option>';
                    }
                    </c:forEach>
                    
                    
                    $(".Instructor_chosen").html(myOptions).chosen().trigger("chosen:updated");
                    
                     var value1 = $("#chosenInstructorId :selected").text();
                    
          if(value1!=''){
              $("#course_batch_save_submit").show();
              $("#publish_submit").hide();
              
          }else{
              $("#course_batch_save_submit").hide();
              $("#publish_submit").show();
             
          }
                },
                dataType: 'html'
            });
            
        });
    
});
</script>

<%@include file="../../inc/template_end.jsp"%>