package com.astrika.moduleusermanagement.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.astrika.moduleusermanagement.model.NextSteps;
import com.astrika.moduleusermanagement.repository.NextStepsRepository;
import com.astrika.moduleusermanagement.service.NextStepsService;
import com.astrika.surveymngt.model.LifeStages;


@Service
public class NextStepsServiceImpl implements NextStepsService{

	@Autowired
	private NextStepsRepository nextStepsRepository;
	
	@Override
	public NextSteps save(NextSteps nextSteps) {
		return nextStepsRepository.save(nextSteps);
	}

	@Override
	public List<NextSteps> findByLifeStageandModuleId(LifeStages lifestages,
			long moduleId) {
		return nextStepsRepository.findByLifeStageandModuleId(lifestages, moduleId);
	}

	@Override
	public void deleteByModuleId(long moduleId) {
		nextStepsRepository.deleteByModuleId(moduleId);
	}

}
