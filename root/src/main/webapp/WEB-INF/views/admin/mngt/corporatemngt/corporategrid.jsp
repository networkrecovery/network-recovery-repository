<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/template_start.jsp"%>
<%@include file="../../inc/page_head.jsp"%>
			<div id="page-content">
				<div class="content-header">
					<div class="header-section">
						<h1>
							<i class="fa fa-map-marker"></i> <spring:message code="heading.corporate"/><br> <small><spring:message code="heading.specifylistofcompanies"/>
								</small>
						</h1>
						<c:if test="${!empty success}">
							<div class="alert alert-success alert-dismissable">
								<button type="button" class="close" data-dismiss="alert"
									aria-hidden="true">x</button>
								<h4>
									<i class="fa fa-check-circle"></i> <spring:message code="label.success"/>
								</h4>
								<spring:message code="${success}" />
							</div>
						</c:if>
						<c:if test="${!empty error}">
							<div class="alert alert-danger alert-dismissable">
								<button type="button" class="close" data-dismiss="alert"
									aria-hidden="true">x</button>
								<h4>
									<i class="fa fa-times-circle"></i> <spring:message code="label.error"/>
								</h4>
								<spring:message code="${error}" />
							</div>
						</c:if>
					</div>
				</div>
				<ul class="breadcrumb breadcrumb-top">
					<li> <spring:message code="heading.managelocations"/></li>
					<li><a href="#"><spring:message code="label.corporate"/></a></li>
				</ul>
				<div class="block full" id="gridView">
					<div class="block-title">
						<h2>
							<strong><spring:message code="heading.corporate"/></strong>
						</h2>
					</div>
					<a href="<%=contexturl %>ManageCorporate/CorporateList/AddCorporate" id="addCorporate" class="btn btn-sm btn-primary"><spring:message code="label.addcorporate" /></a>
					<!--<p><a href="https://datatables.net/" target="_blank">DataTables</a> is a plug-in for the Jquery Javascript library. It is a highly flexible tool, based upon the foundations of progressive enhancement, which will add advanced interaction controls to any HTML table. It is integrated with template's design and it offers many features such as on-the-fly filtering and variable length pagination.</p> -->
					<div class="table-responsive">
						<table id="active-datatable"
							class="table table-vcenter table-condensed table-bordered">
							<thead>
								<tr>
									<th class="text-center"><spring:message code="label.id"/></th>
									<th class="text-center"><spring:message code="label.corporate"/></th>
									<th class="text-center"><spring:message code="label.admin"/></th>
									<th class="text-center"><spring:message code="label.actions"/></th>
								</tr>
							</thead>
							<tbody id="tbody">
								<c:forEach var="currentCorporate" items="${corporateList}"
									varStatus="count">

									<tr>
										<td class="text-center">${count.count}</td>
										<td class="text-left"><c:out
												value="${currentCorporate.corporateName}" /></td>
										<td class="text-left"><c:out
												value="${currentCorporate.corporateAdmin.fullName}" /></td>
										<td class="text-center">
											<div class="btn-group">
												<a href="<%=contexturl %>ManageCorporate/CorporateList/EditCorporate?id=${currentCorporate.corporateId}"
													data-toggle="tooltip" title="Edit"
													class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>
												<a href="#delete_corporate_popup" data-toggle="modal"
													onclick="deleteCorporate(${currentCorporate.corporateId})"
													title="Delete" class="btn btn-xs btn-danger"><i
													class="fa fa-times"></i></a>
											</div>
										</td>
									</tr>
								</c:forEach>

							</tbody>
						</table>
					</div>
				</div>
				
				<div class="block full" id="gridView">
					<div class="block-title">
						<h2>
							<strong><spring:message code="heading.corporate"/></strong>
						</h2>
					</div>
				
					<div class="table-responsive">
						<table id="inactive-datatable"
							class="table table-vcenter table-condensed table-bordered">
							<thead>
								<tr>
									<th class="text-center"><spring:message code="label.id"/></th>
									<th class="text-center"><spring:message code="label.corporate"/></th>
									<th class="text-center"><spring:message code="label.admin"/></th>
									<th class="text-center"><spring:message code="label.actions"/></th>
								</tr>
							</thead>
							<tbody id="tbody">
								<c:forEach var="currentCorporate" items="${inactiveCorporateList}"
									varStatus="count">

									<tr>
										<td class="text-center">${count.count}</td>
										<td class="text-left"><c:out
												value="${currentCorporate.corporateName}" /></td>
										<td class="text-left"><c:out
												value="${currentCorporate.corporateAdmin.fullName}" /></td>
										<td class="text-center">
											<div class="btn-group">
												<a href="#restore_corporate_popup" data-toggle="modal"
													onclick="restoreCorporate(${currentCorporate.corporateId})"
													title="Restore" class="btn btn-xs btn-danger"><i
													class="fa fa-times"></i> </a>
											</div>
										</td>
									</tr>
								</c:forEach>

							</tbody>
						</table>
					</div>
				</div>
				
				
				<div id="delete_corporate_popup" class="modal fade" tabindex="-1"
					role="dialog" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-body">
								<form action="<%=contexturl %>ManageCorporate/CorporateList/DeleteCorporate" method="post"
									class="form-horizontal form-bordered" id="delete_corporate_Form">
									<input type="hidden" name="id" id="deletecorporateId">
									<div style="padding: 10px; height: 110px;">
										<label><spring:message code="validation.doyouwanttodeletethiscorporate"/></label><br>
										<label><spring:message code="validation.deletingcorporate"/></label>
										<div class="col-xs-12 text-right">
											<button type="button" class="btn btn-sm btn-default"
												data-dismiss="modal"><spring:message code="label.no"/></button>
											<div id="delete_corporate" class="btn btn-sm btn-primary"><spring:message code="label.yes"/></div>
										</div>
									</div>

								</form>

							</div>
						</div>
				</div>
				</div>
				
				
	<div id="restore_corporate_popup" class="modal fade" tabindex="-1"
		role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<form action="RestoreCorporate" method="post" class="form-horizontal form-bordered" id="restore_corporate_Form">
						
						<div style="padding: 10px; height: 110px;">
							<label><spring:message
									code="validation.corporaterestore" /></label><br>
							<label><spring:message
									code="validation.restoringcorporate" /></label>
							<div class="col-xs-12 text-right">
								<button type="button" class="btn btn-sm btn-default"
									data-dismiss="modal"><spring:message code="label.no" /></button>
								<div id="restore_Corporate" class="btn btn-sm btn-primary">	<spring:message code="label.yes" /></div>
							</div>
						</div>
						<input type="hidden" name="id" id="restorecorporateId">
					</form>
				</div>
			</div>
		</div>
	</div>

			</div>

		<%@include file="../../inc/page_footer.jsp"%>
        <%@include file="../../inc/template_scripts.jsp"%>
   <script src="<%=contexturl %>resources/js/pages/activeTablesDatatables.js"></script>
<script src="<%=contexturl%>resources/js/pages/inactiveTablesDatatables.js"></script>
<script>$(function(){ ActiveTablesDatatables.init(3); });</script>
<script>$(function(){ InActiveTablesDatatables.init(3); });</script>
<script type="text/javascript">
	var selectedId = 0;
	var restoreId = 0;
	$(document).ready(function() {
		   $("#restore_Corporate").click(function(){
				
				$("#restorecorporateId").val(restoreId);
				$("#restore_corporate_Form").submit();
			});
		$("#delete_corporate").click(function(){
			$("#deletecorporateId").val(selectedId);
			$("#delete_corporate_Form").submit();
		});
	});
	
	function deleteCorporate(id){
		selectedId = id;
	}
	
	function restoreCorporate(id){
		restoreId = id;
	}
</script>
<%@include file="../../inc/template_end.jsp"%>