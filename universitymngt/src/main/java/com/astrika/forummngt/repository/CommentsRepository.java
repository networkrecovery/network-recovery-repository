package com.astrika.forummngt.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.astrika.forummngt.model.Comments;


public interface CommentsRepository extends JpaRepository<Comments, Long> {

	@Query("select c from Comments c left join fetch c.forum left join fetch c.module left join fetch c.category where c.commentId = ?1")
	Comments findByCommentId(Long commentId);

	@Query("select c from Comments c left join fetch c.createdBy where c.active=?1 and c.category.categoryId=?2 and c.module.moduleId=?3 ORDER BY c.createdOn DESC")
	List<Comments> findAllByActiveAndCategoryIdAndModuleId(boolean active,long categoryId, long moduleId);

	@Query("select c from Comments c left join fetch c.createdBy left join fetch c.forum left join fetch c.comment where c.active = ?1 and c.forum.forumId = ?2")
	List<Comments> findByActiveandForumId(boolean active, Long id);

	@Query(value = "select c from Comments c left join fetch c.createdBy where c.active=?1 and c.category.categoryId=?2 and c.module.moduleId=?3", countQuery="select count(c) from Comments c where c.active = ?1 and c.category.categoryId=?2 and c.module.moduleId=?3")
	Page<Comments> findAllByActiveTrueAndCategoryIdAndModuleId(boolean active, long categoryId, long moduleId, Pageable page);

	@Query("select c from Comments c where c.forum.forumId is null and DATEDIFF(NOW(),c.createdOn) <7")
	List<Comments> getAllCommentsOfLastMonth();

	@Query("select c from Comments c where c.forum.forumId is null and DATEDIFF(NOW(),c.createdOn) >7")
	List<Comments> getOldComments();

	@Query("select c.commentId,c.createdOn , c.category.categoryName ,c.createdBy.firstName,c.createdBy.lastName,c.flag from Comments c where c.forum.forumId is null and DATEDIFF(now(),c.createdOn)<7")
	List<Object[]> getLastComments();

	@Query("select count(c) from Comments c where c.forum.forumId is null and c.flag=1")
	int getLastCommentCount();

	@Query("select c from Comments c where c.forum.forumId is null and c.flag=1")
	List<Comments> getCommentOfTrueFlag();

	@Query("select count(c) from Comments c where c.forum.forumId is null")
	int getTotalComment();

	@Query("select c.commentId,c.createdOn ,c.createdBy.firstName,c.createdBy.lastName,c.flag from Comments c where DATEDIFF(now(),c.createdOn)<7 and c.category.categoryId is null")
	List<Object[]> getLastCommentsForum();

	@Query("select count(c) from Comments c where c.module.moduleId is null")
	int getTotalPosts();

	@Query("select count(c) from Comments c where c.module.moduleId is null and c.flag=1")
	int getLastSevenDayPosts();

	@Query("select c.commentId,c.createdOn ,c.createdBy.firstName,c.createdBy.lastName,c.flag from Comments c where c.module.moduleId is null and DATEDIFF(now(),c.createdOn)<7")
	List<Object[]> getlastpostss();

	@Query("select count(c) from Comments c where c.module.moduleId is null and DATEDIFF(now(),c.createdOn)<30")
	int getlastMonthPost();
	
	@Query("select count(c) from Comments c where c.module.moduleId is null and DATEDIFF(now(),c.createdOn)>30")
	int getolderThanOneMonthPost();

	@Query("select c from Comments c where c.module.moduleId is null and c.flag=1")
	List<Comments> getInactiveFlagLastPosts();

}
