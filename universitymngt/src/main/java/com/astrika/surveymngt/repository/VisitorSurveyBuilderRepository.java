package com.astrika.surveymngt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.astrika.surveymngt.model.LifeStages;
import com.astrika.surveymngt.model.VisitorSurveyBuilder;

public interface VisitorSurveyBuilderRepository extends JpaRepository<VisitorSurveyBuilder, Long> {
	
	@Query("select s from VisitorSurveyBuilder s where s.module.moduleId = ?1 and s.createdBy.userId = ?2 and s.surveyManager.surveyManagerId = ?3")
	List<VisitorSurveyBuilder> findByModuleIdandUserIdandSurveyManager(long moduleId, long userId, long surveyManagerId);

	@Query("select v from VisitorSurveyBuilder v where v.createdOn in (" +
			"select max(vv.createdOn) from VisitorSurveyBuilder vv where vv.module.moduleId = ?1 and vv.createdBy.userId = ?2 " +
			"and vv.surveyManager.surveyManagerId = ?3 " +
			")")
	List<VisitorSurveyBuilder> findLastSurveyByModuleIdandUserIdandSurveyManager(long moduleId, long userId, long surveyManagerId);

	@Modifying
	@Transactional
	@Query("delete from VisitorSurveyBuilder s where s.module.moduleId =?1 and s.createdBy.userId = ?2 and s.surveyManager.surveyManagerId = ?3")
	void deleteAllByModuleIdandUserIdandSurveyStatus(long moduleId, long userId, long surveyId);
	
	@Query("select c from VisitorSurveyBuilder c where c.lifeStages=?1 and c.module.moduleId=?2 and c.createdBy.userId=?3 and c.surveyManager.surveyManagerId = ?4")
	List<VisitorSurveyBuilder> findByLifeStagesAndModuleIdandUserIdandSurveyManager(LifeStages lifeStages,long moduleId, long userId, long surveyManagerId);

	@Query("select s from VisitorSurveyBuilder s where s.module.moduleId = ?1 and s.questionNo = ?2")
	List<VisitorSurveyBuilder> findByModuleIdandQuestionNo(long moduleId, int questionNo);
	
}
