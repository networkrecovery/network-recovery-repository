package com.astrika.registrationmngt.service;

import java.util.List;

import com.astrika.common.exception.DuplicateEmailException;
import com.astrika.common.exception.NoSuchUserException;
import com.astrika.common.model.CompanyApproval;
import com.astrika.companymngt.exception.DuplicateCompanyException;
import com.astrika.registrationmngt.exception.DuplicateNGOEmailException;
import com.astrika.registrationmngt.model.EnquiryList;

public interface EnquiryService {
	
	public EnquiryList save(EnquiryList enquiry) throws DuplicateEmailException,NoSuchUserException,DuplicateCompanyException,DuplicateNGOEmailException;
	
	public EnquiryList findByEnqiuryId(long enquiryId);

	public EnquiryList findByNgoEmail(String ngoEmail);

	public EnquiryList approve(long enquiryId);

	public EnquiryList reject(long enquiryId, String remark);

	public List<EnquiryList> findByApproval(CompanyApproval approval);

	void sendEnquiryMail(EnquiryList enquiry,String ngoName,String url);

	void sendRejectionMail(EnquiryList enquiry);

	public EnquiryList findByUserEmail(String emailId);

	public EnquiryList findByNgoName(String ngoName);
}
