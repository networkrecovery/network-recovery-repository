package com.astrika.weightagemngt.model;

public enum ProfileWeightage {

    FIRST_NAME("firstName", 10),
    LAST_NAME("lastName", 10),
    EMAIL_ID("emailId", 10),
    MOBILE("mobile", 10),
    GENDER("gender", 10),
    DESIGNATION("designation", 10),
    LINKEDIN("linkedIn", 5),
    EDUCATIONAL_BACKGROUND("educationalBackground", 10),
    WORK_EXPERIENCE("workExperience", 10),
    FUNCTIONAL_AREAS("functionalAreas", 10),
    NETWORK_KNOWLEDGE("aboutNetwork", 10),
    NGO_NAME("companyName",10),
    SECTOR_WORK("sectorWork",10),
    COMPANY_ADDRESSLINE1("companyAddressLine1" , 5),
    COMPANY_ADDRESSLINE2("companyAddressLine2" , 5),
    COMPANY_ADDRESSLINE3("companyAddressLine3" , 5),
    COUNTRY("country", 10),
    CITY("city", 10),
    STATE("state", 10),
    PINCODE("pincode", 10),
    TELEPHONE("telephone", 5),
    NGO_EMAIL("companyEmail", 10),
    NGO_WEBSITE("companyWebsite" , 10),
    ORGANISATION_MISSION("organisationMission", 10),
    ORGANISATION_VISION("organisationVision" , 10),
    ORGANISATION_DESCRIPTION("organisationDescription", 10),
    ASSISTANCE_FROM_OTHER_AGENCY("assitanceFromOtherAgency", 10),
    LEGAL_TYPE("legalType", 10),
    FCRA_REGISTRATION("fcraRegistarion" , 10),
    TYPE_OF_EDUCATION("typeOfEducation", 5),
    AREA_OF_OPERATION("areaOfOperations", 10),
    REGISTRATION_NUMBER("registrationNumber", 10),
    PAN_NUMBER("panNumber", 10),
    PAN_FILE("panFile", 10),
    TWELVE_A_CERTIFICATE("twelveACertificate", 10),
    TWELVE_A_FILE("twelveAFile", 10),
    EIGHTY_G_NUMBER("eightyGNumber", 10),
    EIGHTY_G_FILE("eightyGFile", 10),
    FCRA_REG_NUMBER("fcraRegNumber", 10),
    FCRA_FILE("fcraFile", 10),
    NO_OF_TRUSTEES("noOfTrustees", 10),
    NO_OF_CHILDREN("noOfChildren",10),
    NO_OF_GIRLS("noOfGirls", 10),
    NO_OF_BOYS("noOfBoys", 10),
    AGE_STUDENTS("ageOfStudents", 10),
    MAIN_SOURCE_FUNDING("mainSourceOfFunding", 10),
    NO_OF_EMPLOYEES("noOfEmployees", 10),
    TRUSTEES("trustees", 10),
    AUDIT1("audit1", 10),
    AUDIT2("audit2", 10),
    BUDGET_SIZE("budgetSize", 10),
    EXISTING_FUNDERS("existingFunders", 10);

    private String name;
    private final int id;

    private ProfileWeightage(String name, int id) {
        this.name = name;
        this.id = id;
    } 


    public String getName() {
        return name;
    }	
    public int getId() {
        return id;
    }


    public static ProfileWeightage fromInt(int id){
        switch(id){
        case 1: 
            return FIRST_NAME;
        case 2:
            return LAST_NAME;
        case 3: 
            return EMAIL_ID;
        case 4:
            return MOBILE;
        case 5:
            return GENDER;
        case 6:
            return DESIGNATION;
        case 7:
            return LINKEDIN;	
        case 8:
            return EDUCATIONAL_BACKGROUND;
        case 9:
            return WORK_EXPERIENCE;
        case 10:
            return FUNCTIONAL_AREAS;
        case 11:
            return NETWORK_KNOWLEDGE;
        case 12:
            return NGO_NAME;
        case 13:
            return SECTOR_WORK;
        case 14:
            return COMPANY_ADDRESSLINE1;
        case 15:
            return COMPANY_ADDRESSLINE2;
        case 16:
            return COMPANY_ADDRESSLINE3;
        case 17:
            return COUNTRY;
        case 18:
            return CITY;
        case 19:
            return STATE;
        case 20:
            return PINCODE;
        case 21:
            return TELEPHONE;
        case 22:
            return NGO_EMAIL;
        case 23:
            return NGO_WEBSITE;
        case 24:
            return ORGANISATION_MISSION;
        case 25:
            return ORGANISATION_VISION;
        case 26:
            return ORGANISATION_DESCRIPTION;
        case 27:
            return ASSISTANCE_FROM_OTHER_AGENCY;

        default:
            return weightage(id);	

        }
    }


    private static ProfileWeightage weightage(int id2) {
        switch(id2){

        case 28:
            return LEGAL_TYPE;
        case 29:
            return FCRA_REGISTRATION;
        case 30:
            return TYPE_OF_EDUCATION;
        case 31:
            return AREA_OF_OPERATION;
        case 32:
            return REGISTRATION_NUMBER;
        case 33:
            return PAN_NUMBER;
        case 34:
            return PAN_FILE;
        case 35:
            return TWELVE_A_CERTIFICATE;
        case 36:
            return TWELVE_A_FILE;
        case 37:
            return EIGHTY_G_NUMBER;
        case 38:
            return EIGHTY_G_FILE;
        case 39:
            return FCRA_REG_NUMBER;
        case 40:
            return FCRA_FILE;
        case 41:
            return NO_OF_TRUSTEES;
        case 42:
            return NO_OF_CHILDREN;
        case 43:
            return NO_OF_GIRLS;
        case 44:
            return NO_OF_BOYS;
        case 45:
            return AGE_STUDENTS;
        case 46:
            return MAIN_SOURCE_FUNDING;
        case 47:
            return NO_OF_EMPLOYEES;
        case 48:
            return TRUSTEES;
        case 49:
            return AUDIT1;
        case 50:
            return AUDIT2;
        case 51:
            return BUDGET_SIZE;
        case 52:
            return EXISTING_FUNDERS;
        default:
            return null;
        }

    }

}
