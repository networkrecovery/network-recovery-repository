package com.astrika.companymngt.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.astrika.companymngt.model.personaldetails.PersonalInfo;

public interface PersonalInfoRepository extends JpaRepository<PersonalInfo, Long>{
	
	@Query("select p from PersonalInfo p where p.user.userId=?1")
	PersonalInfo findById(long userId);

}
