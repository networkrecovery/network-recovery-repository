package com.astrika.educationsector.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.astrika.educationsector.model.EducationSector;


public interface EducationSectorRepository extends JpaRepository<EducationSector, Long> {

		@Query("select e.id,e.educationtype from EducationSector e")
		List<Object[]> getAllSector();
		

	}
