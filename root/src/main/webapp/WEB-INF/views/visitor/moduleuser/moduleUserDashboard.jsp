
<%@include file="../inc/config.jsp"%>
<%@include file="../inc/page_head.jsp"%>

   <div class="blue-container">
            <div class="container">
                <div class="row blue">
                    <div class="col-md-10">
                        <div class="media">
                            <c:choose>
                        		<c:when test = "${(user.profileImage!=null) && (not empty user.profileImage)}">
                        			<a class="pull-left" href="#"><img class="media-object pro-pic" height = 90px width=75px src="<%=contexturl%>${user.profileImage.imagePath}" alt="<%=contexturl %>resources/img/User_Thumbs.png" title="Click here to change photo" onclick="OpenFileDialog();">
                            		</a>
                        		</c:when>
                        		<c:otherwise>
                        			<a class="pull-left" href="#"><img class="media-object pro-pic" src="<%=contexturl %>resources/img/User_Thumbs.png" alt="Organisation Name" title="Click here to change photo" onclick="OpenFileDialog();">
                            		</a>
                        		</c:otherwise>
                        	</c:choose>
							<form action="<%=contexturl%>ChangePhoto" id="changePhoto"
								method="post" enctype="multipart/form-data" class="save">
								<input type="file" id="imageChooser" style="display: none;"
									onchange="changePhoto();" name="imageChooser"
									accept="image/jpg,image/jpeg,image/png,image/gif,image/bmp,image/tiff"
									class="save" />
							</form>
                            <div class="media-body text-st">
                                <p>${user.fullName}</p>
                                <h4 class="media-heading">${organisation.companyName}</h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2 text-center">
                    	<div>
                            <button id="personalFormId" type="button" class="btn btn-default" onclick="location.href='<%=contexturl %>EditPersonalDetails/'+${user.userId}">Edit Profile</button>
                        </div> <br>  
                        <span class="text-white">Your profile is  ${user.profileCompletion}% complete.</span>
                        <div class="progress">
                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="${user.profileCompletion}" aria-valuemin="0" aria-valuemax="100" style="width: ${user.profileCompletion}%">
                                <span class="sr-only"> ${user.profileCompletion}% Complete (warning)</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
	<br>
    <div class="container">
    	<div class="row">
			<div class="col-md-12">
				<c:if test="${!empty success}">
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert"
							aria-hidden="true">x</button>
						<h4>
							<i class="fa fa-check-circle"></i>
							<spring:message code="label.success" />
						</h4>
						<spring:message code="${success}" />
					</div>
				</c:if>
			</div>
		</div>
		<br>
        <div class="row">
            <div class="col-md-9">
                <div class="row">
                <c:forEach var = "modules" items = "${modulemaster}" varStatus = "counter">
                    <div class="col-md-3">
                        <div class="box-style">
                            <div><a href="<%=contexturl %>OrganisationalDevelopmentArea?id=${modules.moduleId}">
                            		<img class="img-responsive" src="<%=contexturl%>${modules.profileImage.imagePath}" 
                            		/>
                            	</a>
                                <h5><strong>${modules.moduleName}</strong></h5>
                            </div>
                            <div class="container-devider"></div>
                            <div class="project-status">
                                <div class="row">
                                    <div class="col-md-6 col-sm-6">
                                        <div class="project-left">
                                            <div class="project-numbers"><strong>${moduleUser[counter.index].projectsAccessed}</strong> / ${totalProjectsList[counter.index]}
                                            </div>
                                            <div class="project-acesses"><small>Project Accessed</small>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <div class="project-right">
                                        	<c:choose>
                                        		<c:when test="${moduleUser[counter.index].moduleStarted eq true}">
                                        			<span class="glyphicon glyphicon-ok text-success"></span><br>
                                            		<span class="project-start"><small>Started</small></span>
                                        		</c:when>
                                        		<c:otherwise>
                                        			<span class="glyphicon glyphicon-remove text-danger"></span><br>
                                        			<span class="project-start"><small>Not Started</small></span>
                                        		</c:otherwise>
                                        	</c:choose>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                 </c:forEach>
               </div>
            </div>
		 <div class="col-md-3 col-sm-9 hidden-xs">
            <img class="img-responsive" src="<%=contexturl %>resources/img/AtmaNetwork.jpg" />
            <br></br>
            <c:choose>
					<c:when test = "${empty atmaContactImage.contactImage}">
						 <div class="need-help"><img class="img-responsive" src="<%=contexturl %>resources/img/need help-3.jpg" /><br><br></div>
					</c:when>
					<c:otherwise>
						 <div class="need-help"><img class="img-responsive" src="<%=contexturl%>${atmaContactImage.contactImage.imagePath}" /><br><br></div>
					</c:otherwise>
			</c:choose>
        </div>
        
        </div>
    </div>
    <div class="container">
        <div class="row">
        </div>
    </div>
<%-- <script src="<%=contexturl%>resources/js/vendor/jquery-1.11.0.min.js"></script> --%>
<%-- <script src="<%=contexturl%>resources/js/vendor/bootstrap.min.js"></script> --%>
<script src="${pageContext.request.contextPath}/webjars/counter-webapp-frontend/0.0.1/vendor_bundle.js"></script>
<script>

</script>

<script type="text/javascript">



	function OpenFileDialog(){
		document.getElementById("imageChooser").click();
	}
	
	function changePhoto(){
		if($('#imageChooser').val() != ""){			
			var imageName = document.getElementById("imageChooser").value;
			var ext1 = imageName.substring(imageName.lastIndexOf("."));
			var ext = ext1.toLowerCase();
			if(ext == '.jpg' || ext == '.jpeg' || ext == '.png' || ext == 'gif' || ext == 'bmp' || ext == 'tiff')
				$('#changePhoto').submit();		
			else
				alert("Please select only image file");		
		}
	}
	
</script>
<%@include file="../inc/page_footer.jsp"%>


