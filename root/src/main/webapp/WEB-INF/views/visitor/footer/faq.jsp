<!DOCTYPE html>
<html lang="en">
<%@include file="/WEB-INF/views/admin/inc/config.jsp"%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Atma Network</title>
<!-- Style -->
<!-- Bootstrap -->
<link rel="shortcut icon" href="<%=contexturl %>resources/img/favicon.ico">
<%-- <link href="<%=contexturl%>resources/css/bootstrap.css" rel="stylesheet" media="screen"> --%>
<%-- <link href="<%=contexturl%>resources/css/font-awesome.css" rel="stylesheet" media="screen"> --%>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" media="screen"> 
<link href="<%=contexturl%>resources/css/fontface.css" rel="stylesheet" media="screen">
<!--[if IE 9]>
  <script src="<%=contexturl%>resources/js/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="<%=contexturl%>resources/js/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<!--[if IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

<link href="<%=contexturl%>resources/css/style.css" rel="stylesheet"
	media="screen">
</head>


<body>
	<div class="header-bg">
		<nav class="navbar">
			<div class="container">
				<div class="navbar-header">
					<img src="<%=contexturl%>resources/img/atma.png" />
				</div>
				<div id="navbar" class="mynav">
					<ul class="nav navbar-nav pull-right">
						<security:authorize access="isAuthenticated()">
							<li class="active"><a href="<%=contexturl%>"><b>Back to Home Page</b></a></li>
						</security:authorize>
						<security:authorize access="isAnonymous()">
							<li class="active"><a href="<%=contexturl%>Login"><b>Back to Login Page</b></a></li>
						</security:authorize>
					</ul>
				</div>
			</div>
		</nav>
		<div class="blue-container">
			<div class="container"></div>
		</div>
	</div>
	<br />
	<div class="container">
    		<div class="row">
    		  <h3><p class="text-center"><strong>Frequently Asked Questions (FAQ)</strong></p></h3>
    		  <br />
    		  <blockquote>
    			  <p><strong>Q. <i>What is Atma Education?</i></strong></p>
    			  <p><strong>A.</strong> Atma is a non-profit organisation that provides intensive support to education non-profits and social enterprises by helping them solve their daily operational challenges and plan for their future growth.</p>
    		  </blockquote>
    		  <blockquote>
    			  <p><strong>Q. <i>What is Atma Network?</i></strong></p>
    			  <p><strong>A.</strong> The Atma Network is a platform for entrepreneurs starting and scaling social education companies or NGOs. The Atma Network gives entrepreneurs “how-to guides” for developing their organisation, templates for execution and examples of how other leading entrepreneurs have addressed their most pressing challenges; saving time, resources and building the talent through exposure and networking.</p>
    		  </blockquote>
					<blockquote>
    			  <p><strong>Q. <i>How can Atma Network help?</i></strong></p>
    			  <p><strong>A.</strong> The Atma Network can help you evaluate where your organisation currently stands. It would also help guide you on the best next steps  and projects for your organisation that will help you grow further. The Network would help you guide on a variety of projects that you can take under various development areas of your organisation like Fundraising, Marketing, HR, etc. With network you can accelarate the growth of your organisation.</p>
    		  </blockquote>
					<blockquote>
    			  <p><strong>Q. <i>How can I register on Atma Network?</i></strong></p>
    			  <p><strong>A.</strong> Please refer to our How-to guides to find a step by step procedure to register for the Network.</p>
    		  </blockquote>
					<blockquote>
    			  <p><strong>Q. <i>How to take Life Stage Survey?</i></strong></p>
    			  <p><strong>A.</strong> Please refer to our How-to guides to find a step by step procedure to find the procedure to take Life Stage Survey.</p>
    		  </blockquote>
					<blockquote>
    			  <p><strong>Q. <i>Why should I take Life Stage Survey?</i></strong></p>
    			  <p><strong>A.</strong> The Atma Life stage survey is designed to tell you how your organisation is performing across various functional areas. No life stage rating is better than another, it just describes where you are as an organisation and helps you understand your strengths and weaknesses.</p></br></br>
						<p>This Life Stage Survey is to be taken by the leader of the organisation, ideally in cooperation with their team. The Team should include other decision makers in the organisation whether they are managers or board members to help get the most rounded and objective responses.</p></br></br>
						<p>The survey can highlight areas your need to work on and also suggests strategies you might want to take on as next steps. </p></br></br>
						<p>What do you get:</p>
						<ul class="list-unstyled">
					  	<li><h4><small style="color:black">You will find out your lifestage in 10 different areas of organsiational development</small></h4></li>
					  	<li><h4><small style="color:black">By taking the survey on an annual basis you will be able to understand how your organisation is growing</small></h4></li>
					  	<li><h4><small style="color:black">You will get a report on your lifestage that you can share with the stakeholders of your organisation</small></h4></li>
						</ul>
						<br />
						<br />
						<p>What you will not get: </p>
						<ul class="list-unstyled">
							<li><h4><small style="color:black">The lifestage survey will not give you an overall rating as an organisation</small></h4></li>
						</ul>
    		  </blockquote>
					<blockquote>
    			  <p><strong>Q. <i>How to add Module User?</i></strong></p>
    			  <p><strong>A.</strong> Please refer to our How-to guides to find a step by step procedure to add module user.</p>
    		  </blockquote>
					<blockquote>
    			  <p><strong>Q. <i>What is Organisation Admin?</i></strong></p>
    			  <p><strong>A.</strong> An Organisation Admin is the person representing their NGO or Social Enterrse at the Network. He/She also has the authority to identify the appropriate user for different ODAs. They are esponsible for taking all the Life Stage Surveys an evaluating the progress of ach ODA in their organisation.</p>
    		  </blockquote>
					<blockquote>
    			  <p><strong>Q. <i>Why cant I see a Survey Link?</i></strong></p>
    			  <p><strong>A.</strong> The Survey Link can be seen only for 30 Days after the Survey needs to be taken. If your date of taking the survey has expired, the link will not be visible. To get the link activated again, please mail us at network@atma.org.in.</p>
    		  </blockquote>
					<blockquote>
    			  <p><strong>Q. <i>Why cant I see Life Stage of a module?</i></strong></p>
    			  <p><strong>A.</strong> If you haven't taken the Survey for a particular Organisational Development Area, then you wont get the Life Stage for that particular module. Please go to Surveys and then find that ODA and complete your suvey to find the rating.</p>
    		  </blockquote>
					<blockquote>
    			  <p><strong>Q. <i>Why am I getting error while taking the Life Stage Survey?</i></strong></p>
    			  <p><strong>A.</strong> If there are two same high scores for your sections of the Life Stage Survey, the system will show an Error message. This signifies that the you need to reevaluate your answers in case to get an accurate Life Stage rating.</p>
    		  </blockquote>
				</div>
			  </div>
       	<br />
       	<br />
       	<br />
       	<br />
   	<div class="container" id="footer">
		<%@include file="../../visitor/inc/footer_items.jsp"%>
	</div>
</body>
<script src="<%=contexturl%>resources/js/vendor/jquery-1.11.0.min.js"></script>
<script src="<%=contexturl%>resources/js/vendor/bootstrap.min.js"></script>



</html>
