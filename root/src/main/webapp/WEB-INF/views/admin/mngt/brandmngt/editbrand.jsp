<%@page import="com.astrika.common.concurrency.TransactionType"%>
<%@page import="com.astrika.outletmngt.model.BrandType"%>
<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/template_start.jsp"%>
<%@include file="../../inc/page_head.jsp"%>
<div id="page-content">
<div class="content-header">
	<div class="header-section">
	   
		<h1>
			<i class="fa fa-map-marker"></i> <spring:message code="heading.brand"/><br> <small><spring:message code="heading.editbrand"/></small>
		</h1>
		<span id="errorMsg"></span>
		<c:if test="${!empty success}">
				<div class="alert alert-success alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-check-circle"></i> <spring:message code="label.success"/>
					</h4>
					<spring:message code="${success}" />
				</div>
			</c:if>
			
			<c:if test="${!empty error}">
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">x</button>
					<h4>
						<i class="fa fa-times-circle"></i> <spring:message code="label.error"/>
					</h4>
					<spring:message code="${error}" />
				</div>
			</c:if>
	</div>
</div>
<ul class="breadcrumb breadcrumb-top">
	<li><spring:message code="menu.brand"/> </li>
</ul>
<form action="<%=contexturl %>ManageBrand/BrandList/UpdateBrand" method="post"
			class="form-horizontal " id="Brand_form">
			<input  name="brandId"  value="${brand.brandId}" type="hidden"> 
			<input  name="userId"  value="${brand.brandAdmin.userId}" type="hidden"> 
			<input type="hidden" name="brandType" value="<%=BrandType.RESTAURANT.getId()%>">
	<div class="row">
		
			<div class="col-md-6">
				<div class="block">
					<div class="block-title">
						<h2>
							<strong><spring:message code="heading.brandinfo"/></strong> 
							<small style="float:left; margin-top: 4px;">This info is non editable and for view only purpose.</small>
						</h2>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label" for="currencyFrom_Name"><spring:message code="label.companyname"/><span class="text-danger">*</span></label>
						<div class="col-md-6">
							<label class="control-label">${brand.company.companyName}</label>
							<input name="compName" value="${brand.company.companyName}" type="hidden">
						</div>
					</div>
					<div class="form-group">
                        <label class="col-md-4 control-label"><spring:message code="label.brandname"/></label>
                        <div class="col-md-8">
                            <p class="form-control-static">${brand.brandName}</p>
                            <input name="brandName" value="${brand.brandName}" type="hidden">
                        </div>
                    </div>
					
					<div class="form-group">
						<label class="col-md-4 control-label" for="gourmet7_Reach"><spring:message code="label.incudeinmembership"/></label>
						<div class="col-md-6">
							<label class="switch switch-primary">
								<input	id="include" name="include" type="checkbox" <c:if test="${brand.includeInMembership}">checked</c:if>/>
							<span></span></label>
						</div>
					</div>
				</div>
				
					
				<div class="block">
					<div class="block-title">
						<h2>
							<strong><spring:message code="heading.contactinfo" /></strong>
							<small style="float:left; margin-top: 4px;">All your Gourmet7 related communication will be done on these details. * These details can be same as your admin details.</small>
						</h2>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label" for="telephone1"><spring:message
								code="label.telephone1" /><span class="text-danger">*</span></label>
						<div class="col-md-6">

							<input id="brandTelephone1" name="brandTelephone1"
								class="form-control"
								placeholder="<spring:message code='label.telephone1'/>.."
								type="text" value="${brand.brandTelephone1}" >
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label" for="telephone2"><spring:message
								code="label.telephone2" /></label>
						<div class="col-md-6">

							<input id="brandTelephone2" name="brandTelephone2"
								class="form-control"
								placeholder="<spring:message code='label.telephone2'/>.."
								type="text" value="${brand.brandTelephone2}">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label" for="rest_email"><spring:message
								code="label.brandemail" /><span class="text-danger">*</span></label>
						<div class="col-md-6">

							<input id="brand_email" name="brandEmail"
								class="form-control"
								placeholder="<spring:message code="label.brandemail"/>.."
								type="text" value="${brand.brandEmail}">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label" for="mobile1"><spring:message
								code="label.mobile1" /><span class="text-danger">*</span></label>
						<div class="col-md-6">

							<input id="brandMobile1" name="brandMobile1"
								class="form-control"
								placeholder="<spring:message code="label.mobile1"/>.."
								type="text" value="${brand.brandMobile1}">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label" for="mobile2"><spring:message
								code="label.mobile2" /></label>
						<div class="col-md-6">

							<input id="brandMobile2" name="brandMobile2"
								class="form-control"
								placeholder="<spring:message code="label.mobile2"/>.."
								type="text" value="${brand.brandMobile2}">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label" for="addressline1"><spring:message
								code="label.addressline1" /><span class="text-danger">*</span>
						</label>
						<div class="col-md-6">

							<input id="brandAddressline1" name="brandAddressLine1"
								class="form-control"
								placeholder="<spring:message code="label.addressline1"/>.."
								type="text" value="${brand.brandAddressLine1}">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label" for="addressline2"><spring:message
								code="label.addressline2" /></label>
						<div class="col-md-6">

							<input id="brandAddressline2" name="brandAddressLine2"
								class="form-control"
								placeholder="<spring:message code="label.addressline2"/>.."
								type="text" value="${brand.brandAddressLine2}">
						</div>
					</div>
					
					<div class="form-group" id="countryDiv">
						<label class="col-md-4 control-label" for="country"><spring:message
								code="label.countryname" /><span class="text-danger">*</span></label>
						<div class="col-md-6">
							<select class="country_chosen" style="width: 200px;"
								id="brandCountryId"
								data-placeholder="<spring:message code='label.choosecountry' />"
								name="chosenCountryId">
								<!--<option value="placeholder"></option>-->
								<c:forEach items="${countryList}" var="country">
											<option value="${country.countryId}"  <c:if test="${country.countryId eq brand.brandCountry.countryId}">selected="selected"</c:if>>${country.countryName}</option>
								</c:forEach>
							</select>
						</div>
					</div>
					
					<div class="form-group" id="cityDiv">
						<label class="col-md-4 control-label" for="city"><spring:message
								code="label.cityname" /><span class="text-danger">*</span></label>
						<div class="col-md-6">
							<select class="city_chosen" style="width: 200px;"
								id="brandCityId"
								data-placeholder="<spring:message code='label.choosecity' />"
								name="chosenCityId">
							</select>
						</div>
					</div>

					<div class="form-group" id="areaDiv">
						<label class="col-md-4 control-label" for="area"><spring:message
								code="label.areaname" /><span class="text-danger">*</span></label>
						<div class="col-md-6">
							<select class="area_chosen" style="width: 200px;"
								id="brandAreaId"
								data-placeholder="<spring:message code='label.choosearea' />"
								name="chosenAreaId">
							</select>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label" for="brandPincode"><spring:message
								code="label.pincode" /><span class="text-danger">*</span></label>
						<div class="col-md-6">

							<input id="brandPincode" name="brandPincode"
								class="form-control"
								placeholder="<spring:message code="label.pincode"/>.."
								type="text" value="${brand.brandPincode }">
						</div>
					</div>




				</div>

				
			</div>
			<div class="col-md-6">
				<div class="block">
					<div class="block-title">
						<h2>
							<strong><spring:message code="heading.admininfo"/></strong>
							<small style="float:left; margin-top: 4px;">This info will be used for login. Please make sure you add the exact details and make sure it is not misused.</small>
						</h2>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label" for="first_Name"><spring:message code="label.firstname"/><span class="text-danger">*</span>
						</label>
						<div class="col-md-6">
							
								<input id="first_Name" name="firstName" class="form-control"
									placeholder="<spring:message code='label.firstname'/>.." type="text"  value="${brand.brandAdmin.firstName}"> 
						</div>
					</div>
	
					<div class="form-group">
						<label class="col-md-4 control-label" for="last_Name"><spring:message code="label.lastname"/><span class="text-danger">*</span>
						</label>
						<div class="col-md-6">
							
								<input id="last_Name" name="lastName" class="form-control"
									placeholder="<spring:message code="label.lastname"/>.." type="text" value="${brand.brandAdmin.lastName}">
						</div>
					</div>
	
	
					<div class="form-group">
						<label class="col-md-4 control-label" for="email"><spring:message code="label.emailid"/><span
							class="text-danger">*</span></label>
						<div class="col-md-6">
								<input id="email" name="emailId" class="form-control"
									placeholder="test@example.com" type="text" value="${brand.brandAdmin.emailId}">
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-md-4 control-label" for="mobile"><spring:message code="label.mobile"/><span
							class="text-danger">*</span></label>
						<div class="col-md-6">
								<input id="mobile" name="mobile" class="form-control"
									placeholder='<spring:message code="label.mobile"></spring:message>' type="text" value="${brand.brandAdmin.mobile}">
						</div>
					</div>
					<div class="form-group">
                        <label class="col-md-4 control-label"><spring:message code="label.userlogin"/></label>
                        <div class="col-md-8">
                            <p class="form-control-static">${brand.brandAdmin.loginId}</p>
                            <input name="adminLoginId" value="${brand.brandAdmin.loginId}" type="hidden">
                        </div>
                    </div>
					<div class="form-group">
						<a href="#reset-user-settings" data-toggle="modal" class="col-md-4 control-label" ><spring:message code="label.resetpassword"/></a>
					</div>
					
				</div>
				<div class="block">
					<%@include file="../featurecontrol/featurecontrol.jsp"%>
				</div>
			</div>
	
	</div>
	
	
	
	
	<div class="form-group form-actions">
						<div class="col-md-9 col-md-offset-3">
							<button id="brand_submit" type="submit"
								class="btn btn-sm btn-primary save">
								<i class="fa fa-angle-right"></i> <spring:message code="button.save" />
							</button>
							
								<a class="btn btn-sm btn-primary" href="<%=contexturl %>ManageBrand/BrandList"><i class="gi gi-remove"></i> <spring:message code="button.cancel" />
								</a>
						</div>
					</div>
	
	
	
</form>
</div>

<%@include file="../../inc/page_footer.jsp"%>
<%@include file="../../inc/template_scripts.jsp"%>
<%@include file="../../inc/chosen_scripts.jsp"%>

<div id="reset-user-settings" class="modal fade" tabindex="-1"
	role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header text-center">
				<h2 class="modal-title">
					<i class="fa fa-pencil"></i> <spring:message code="label.resetpassword"/>
				</h2>
			</div>
			<span id="passwordErrorMsg"></span>
			<div class="modal-body">
				<form action="<%=contexturl %>ManageBrand/BrandList/ResetBrandAdminPassword" method="post" class="form-horizontal form-bordered"  id="ResetPassword_Form">
					<input  name="brandId"  value="${brand.brandId}" type="hidden"> 
					<input  name="userId"  value="${brand.brandAdmin.userId}" type="hidden"> 
					<fieldset>
						<div class="form-group">
							<label class="col-md-4 control-label"
								for="user-settings-password"><spring:message code="label.newpassword"/></label>
							<div class="col-md-8">
								<input type="password" id="password"
									name="password" class="form-control"
									placeholder="Please choose a complex one..">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label"
								for="user-settings-repassword"><spring:message code="label.confirmnewpassword"/></label>
							<div class="col-md-8">
								<input type="password" id="user-settings-repassword"
									name="confirmPassword" class="form-control"
									placeholder="..and confirm it!">
							</div>
						</div>
					</fieldset>
					<div class="form-group form-actions">
						<div class="col-xs-12 text-right">
							<button type="button" class="btn btn-sm btn-default"
								data-dismiss="modal"><spring:message code="label.close"/></button>
							<button id="submit_password" class="btn btn-sm btn-primary save" type="submit"><spring:message code="label.savechanges"/>
								</button>
						</div>
					</div>
				</form>
</div>
</div>
</div>
</div>
<div class="form-group">
			<a href="#concurrency_popup" data-toggle="modal" class="col-md-4 control-label" id="councurrencyLink"></a>
</div>
<div id="concurrency_popup" class="modal fade" tabindex="-1"
	role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<form action="" method="post" class="form-horizontal form-bordered"
					id="fg1">
					<div style="padding: 10px; height: 110px;">
						<label id="error_Msg"></label>
						<div class="col-xs-12 text-right">
							<a class="btn btn-sm btn-default"
								href="<%=contexturl %>ManageBrand/BrandList"><spring:message code="label.no"/></a>
							<a class="btn btn-sm btn-primary save" href="#" id="extendSession">
							<spring:message code="label.yes"/> </a>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<script src="<%=contexturl %>resources/js/custom/featurecontrol.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$(".chosen").data("placeholder","Select Currency From...").chosen();
		manageConcurrency();
		$(".area_chosen").chosen();
		$(".city_chosen").chosen();
		$(".country_chosen").chosen();
		
		$('select[name="chosenCountryId"]').chosen().change( function() {
			countryId = ($(this).val());
			$.ajax({
				type: "GET",
				async: false,
				url: "<%=contexturl %>city/citybycountryid/"+countryId,
				success: function(data, textStatus, jqXHR){
					var obj = jQuery.parseJSON(data);
					var len=obj.length;
					var myOptions = "<option value></option>";
					var myAreaOptions = "<option value></option>";
					for(var i=0; i<len; i++){
					myOptions += '<option value="'+obj[i].cityId+'">'+obj[i].cityName+'</option>';
					}
					$(".city_chosen").html(myOptions).chosen().trigger("chosen:updated");
					$(".area_chosen").html(myAreaOptions).chosen().trigger("chosen:updated");
				},
				dataType: 'html'
			});
		});
		
		$('select[name="chosenCityId"]').chosen().change( function() {
			cityId = ($(this).val());
			$.ajax({
				type: "GET",
				async: false,
				url: "<%=contexturl %>area/areabycityid/"+cityId,
				success: function(data, textStatus, jqXHR){
					var obj = jQuery.parseJSON(data);
					var len=obj.length;
					var myOptions = "<option value></option>";
					for(var i=0; i<len; i++){
					myOptions += '<option value="'+obj[i].areaId+'">'+obj[i].areaName+'</option>';
					}
					$(".area_chosen").html(myOptions).chosen().trigger("chosen:updated");
				},
				dataType: 'html'
			});
		});
		
       changeCountry($("#brandCountryId").val());
		
		function changeCountry(countryId){
			$.ajax({
				type: "GET",
				async: false,
				url: "<%=contexturl %>city/citybycountryid/"+countryId,
				success: function(data, textStatus, jqXHR){
					var obj = jQuery.parseJSON(data);
					var len=obj.length;
					var myOptions = "<option value></option>";
					for(var i=0; i<len; i++){
						if(${brand.brandCity.cityId} != obj[i].cityId){
							myOptions += '<option value="'+obj[i].cityId+'">'+obj[i].cityName+'</option>';
						}
						else{
							myOptions += '<option value="'+obj[i].cityId+'" selected="selected">'+obj[i].cityName+'</option>';
						}
					}
					$(".city_chosen").html(myOptions).chosen().trigger("chosen:updated");
					changeCity($("#brandCityId").val());
				},
				dataType: 'html'
			});
		}
		
		function changeCity(cityId){
			$.ajax({
				type: "GET",
				async: false,
				url: "<%=contexturl %>area/areabycityid/"+cityId,
				success: function(data, textStatus, jqXHR){
					var obj = jQuery.parseJSON(data);
					var len=obj.length;
					var myOptions = "<option value></option>";
					for(var i=0; i<len; i++){
						if(${brand.brandArea.areaId} != obj[i].areaId){
							myOptions += '<option value="'+obj[i].areaId+'">'+obj[i].areaName+'</option>';
						}
						else{
							myOptions += '<option value="'+obj[i].areaId+'" selected="selected">'+obj[i].areaName+'</option>';
						}
					}
					$(".area_chosen").html(myOptions).chosen().trigger("chosen:updated");
				},
				dataType: 'html'
			});
		}
		
		$("#availability").click(function(){
			var data = $("#login").val();
			$("#notavailable").css("display","none");
			$("#available").css("display","none");
			if( data != ""){
				$.getJSON("<%=contexturl%>ValidateLogin",{loginId: data}, function(data){
					if(data == "exist"){
						$("#notavailable").css("display","inline");
					}
					else if(data == "available"){
						$("#available").css("display","inline");
					}
				});
			}
		});
		
		
		
		$("#Brand_form").validate(
				{	errorClass:"help-block animation-slideDown",
					errorElement:"div",
					errorPlacement:function(e,a){a.parents(".form-group > div").append(e)},
					highlight:function(e){$(e).closest(".form-group").removeClass("has-success has-error").addClass("has-error")},
					success:function(e){e.closest(".form-group").removeClass("has-success has-error")},
					rules:{ brandName:{required:!0, maxlength : 75},
							firstName:{required:!0,maxlength : 75},
							lastName:{required:!0,maxlength : 75},
							emailId:{required:!0,email:!0,maxlength : 75},
							mobile:{required:!0,digits:!0,maxlength : 75},
							loginId:{required:!0,minlength:6,maxlength : 75},
							password:{required:!0,minlength:6,maxlength : 75},
							confirmPassword:{required:!0,equalTo:"#password"},
							brandTelephone1 : {
								required : !0,maxlength : 75,
								phone : !0
							},
							brandTelephone2 : {
								phone : !0,maxlength : 75,
							},
							brandEmail : {
								required : !0,maxlength : 75,
								email : !0
							},
							brandMobile1 : {
								required : !0,maxlength : 75,
								digits : !0
							},
							brandMobile2 : {
								digits : !0,maxlength : 75,
							},
							brandAddressLine1 : {
								required : !0,maxlength : 75,
							},
							brandAddressLine2 : {
								maxlength : 75,
							},
							brandPincode : {
								required : !0,maxlength : 10,
								digits : !0
							},
							chosenCityId: {
								required : !0
							},
							chosenCountryId: {
								required : !0
							},
							chosenAreaId: {
								required : !0
							}
					},
					messages:{
						brandName:{
							required:'<spring:message code="validation.pleaseenterbrandname"/>',
							maxlength :'<spring:message code="validation.brnadname75character"/>'
							},
						
						firstName:{
							required:'<spring:message code="validation.pleaseenterfirstname"/>',
							maxlength :'<spring:message code="validation.firstname75character"/>'
							},
						lastName:{
							required:'<spring:message code="validation.pleaseenterlastname"/>',
							maxlength :'<spring:message code="validation.lastname75character"/>'
							},
						emailId:{
							required:'<spring:message code="validation.pleaseenteremailid"/>',
							email:'<spring:message code="validation.pleaseenteravalidemailid"/>',
							maxlength : '<spring:message code="validation.email75character"/>'
							},
						mobile:{
							required:'<spring:message code="validation.pleaseenteramobilenumber"/>',
							phone :'<spring:message code="validation.phone"/>',
							maxlength :'<spring:message code="validation.mobile75character"/>'
							},
						loginId:{
							required:'<spring:message code="validation.pleaseenteraloginid"/>',
							minlength:'<spring:message code="validation.loginidmustbeatleast6character"/>',
							maxlength :'<spring:message code="validation.loginid75character"/>'
							},
						password:{
							required:'<spring:message code="validation.pleaseenterapassword"/>',
							minlength:'<spring:message code="validation.passwordmustbeatleast6character"/>',
							maxlength :'<spring:message code="validation.passworddname75character"/>'
							},
						confirmPassword:{
							required:'<spring:message code="validation.pleaseconfirmpassword"/>',
							equalTo:'<spring:message code="validation.pleaseenterthesamepasswordasabove"/>'
							},
						brandTelephone1 : {
							required : '<spring:message code="validation.pleaseenteratelephonenumber"/>',
							phone :'<spring:message code="validation.phone"/>',
							maxlength :'<spring:message code="validation.telephone75character"/>'
						},
						brandTelephone2 : {

							phone :'<spring:message code="validation.phone"/>',
							maxlength :'<spring:message code="validation.telephone75character"/>'
						},
						brandEmail : {
							required : '<spring:message code="validation.pleaseenteremailid"/>',
							email : '<spring:message code="validation.pleaseenteravalidemailid"/>',
							maxlength : '<spring:message code="validation.email75character"/>'
						},
						brandMobile1 : {
							required :'<spring:message code="validation.pleaseenteramobilenumber"/>',
							phone :'<spring:message code="validation.phone"/>',
							maxlength :'<spring:message code="validation.mobile75character"/>'
						},
						brandMobile2 : {
							digits : '<spring:message code="validation.phone"/>',
							maxlength :'<spring:message code="validation.mobile75character"/>'
						},
						brandAddressLine1 : {
							required : '<spring:message code="validation.pleaseenteraddressline1"/>',
							maxlength :'<spring:message code="validation.address"/>'
						},
						brandAddressLine2 : {
							maxlength :'<spring:message code="validation.address"/>'
						},
						brandPincode : {
							required : '<spring:message code="validation.pleaseenterpincode"/>',
							digits :'<spring:message code="validation.digits"/>',
							maxlength :'<spring:message code="validation.pincode75character"/>'
						},
						chosenCityId: {
							required : '<spring:message code="validation.selectcity"/>'
						},
						chosenCountryId: {
							required : '<spring:message code="validation.selectcountry"/>'
						},
						chosenAreaId: {
							required : '<spring:message code="validation.selectarea"/>'
						}
					},
					
				});
		
		$("#ResetPassword_Form")
		.validate(
				{
					errorClass : "help-block animation-slideDown",
					errorElement : "div",
					errorPlacement : function(e, a) {
						a.parents(".form-group > div")
								.append(e)
					},
					highlight : function(e) {
						$(e)
								.closest(".form-group")
								.removeClass(
										"has-success has-error")
								.addClass("has-error")
					},
					success : function(e) {
						e
								.closest(".form-group")
								.removeClass(
										"has-success has-error")
					},
					rules : {
						password : {
							required : !0,
							minlength : 6,maxlength : 75
						},
						confirmPassword : {
							required : !0,
							equalTo : "#password"
						}
					},
					messages : {
						password : {
							required :'<spring:message code="validation.pleaseenterapassword"/>',
							minlength:'<spring:message code="validation.passwordmustbeatleast6character"/>',
							maxlength :'<spring:message code="validation.passworddname75character"/>'
						},
						confirmPassword : {
							required : '<spring:message code="validation.pleaseconfirmpassword"/>',
							equalTo:'<spring:message code="validation.pleaseenterthesamepasswordasabove"/>'
						},
					}
				});
		
	});
	

	function manageConcurrency(){
			setTimeout(function(){
			
				$.ajax({
					type: "GET",
					async: false,
					url: "<%=contexturl%>TimeOutWarning",				
					success: function(data, textStatus, jqXHR){
						$("#councurrencyLink").trigger("click");
						$("#error_Msg").html($.parseJSON(data).Response);
						$("#extendSession").click(function(){
							$('body').removeClass('modal-open');
		 					$('.modal-backdrop').remove();
		 					$('#concurrency_popup').modal('hide');	
							$.ajax({
								type: "GET",
								async: false,
								url: "<%=contexturl%>ExtendSession?transactionId=${brand.brandId}&transactionType=<%=TransactionType.BRAND%>",				
								success: function(data, textStatus, jqXHR){
									var html = '<div class="alert alert-warning alert-dismissable">	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="fa fa-times-circle"></i> Warning</h4> <span >'+$.parseJSON(data).Response+'</span></div>';
									$("#errorMsg").html(html).show();
									$("#errorMsg").html(html).fadeOut(20000);
									$('html, body').animate({ scrollTop: $("#errorMsg").offset().top },500);
									manageConcurrency();
								},
								dataType: 'html'
							});
						});
					},
					dataType: 'html'
				});
			},<%= propvalue.concurrencySessionWarning%>);
			
		}
		
	
	
</script>

<style>

.chosen-container {
	width: 250px !important;
}
</style>
<%@include file="../../inc/template_end.jsp"%>