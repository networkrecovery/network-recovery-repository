<%@page import="com.astrika.common.model.Role"%>
<%@page import="com.astrika.common.model.User"%>


<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/page_head.jsp"%>


  <div class="blue-container">
            <div class="container">
             <h3 class="atma-breadcrumbs">
			<security:authorize ifAnyGranted="<%=Role.COMPANY_ADMIN.name()%>">
				<a class="light a-breadcrumb" href="<%=contexturl%>CompanyAdmin">Dashboard</a>
			</security:authorize>
			<security:authorize ifAnyGranted="<%=Role.MODULE_USER.name()%>">
				<a class="light a-breadcrumb" href="<%=contexturl%>ModuleUser">Dashboard</a>
			</security:authorize>
			</h3>
                <div class="row blue">
                    <div class="col-md-10">
                        <div class="media">
                           <c:choose>
                        		<c:when test = "${(user.profileImage!=null) && (not empty user.profileImage)}">
                        			<a class="pull-left" href="#"><img class="media-object pro-pic" height = 90px width=75px src="<%=contexturl %>${user.profileImage.imagePath}" alt="<%=contexturl %>resources/img/User_Thumbs.png" title="Click here to change photo" onclick="OpenFileDialog();">
                            		</a>
                        		</c:when>
                        		<c:otherwise>
                        			<a class="pull-left" href="#"><img class="media-object pro-pic" src="<%=contexturl %>resources/img/User_Thumbs.png" alt="Organisation Name" title="Click here to change photo" onclick="OpenFileDialog();">
                            		</a>
                        		</c:otherwise>
                        	</c:choose>
                            <div class="media-body text-st">
                                <p>Edit Organisational Details Here</p>
                                <h4 class="media-heading">Organisational Details</h4>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
<br>
<div class="container">
	<div class="row">
		<c:if test="${!empty success}">
			<div class="alert alert-success alert-dismissable">
				<button type="button" class="close" data-dismiss="alert"
					aria-hidden="true">x</button>
				<h4>
					<i class="fa fa-check-circle"></i>
					<spring:message code="label.success" />
				</h4>
				<spring:message code="${success}" />
			</div>
		</c:if>
		<div class="alert alert-danger alert-dismissable"
			style="display: none" id="error">
			<button type="button" class="close" onclick="closeDiv();"
				aria-hidden="true">x</button>
			<h4>
				<i class="fa fa-times-circle"></i>
				<spring:message code="label.error" />
			</h4>
			<span class="errorMsg"></span>
		</div>
	</div>
	<br>
	<div class="row">
		<c:if test="${not adminEdit}">
			<div class="col-md-2"></div>
			<div class="col-md-4">
				<button class="btn btn-default btn-sm btn-block"
					onclick="location.href='<%=contexturl %>EditPersonalDetails/'+${user.userId}">Personal
					Details</button>
			</div>
			<div class="col-md-4">
				<button class="btn btn-sm btn-block">Organisation Details</button>
			</div>
			<div class="col-md-2"></div>
		</c:if>
		<c:if test="${adminEdit}">
				<div>
					<button class="btn btn-sm btn-block">Organisation Details</button>
				</div>
		</c:if>
		<br> <br> <br>
	</div>
	<div class="row">
		<div class="col-md-2"></div>
		<div class="col-md-8">
			<div class="fuelux">
				<div class="wizard" id="MyWizard">
					<ul class="steps">
						<li style="width: 30%" data-target="#step1"><span
							class="badge">1</span>Step<span class="chevron"></span></li>
						<li style="width: 40%" data-target="#step2"><span
							class="badge">2</span>Step<span class="chevron"></span></li>
						<li style="width: 30%" class="active" data-target="#step3"><span
							class="badge badge-info">3</span> Step</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="col-md-2"></div>
	</div>
	<div class="row">
		<form action="<%=contexturl%>SaveOrganisationalDetailsStep3"
			method="post" class="form-horizontal " id="step3_form"
			enctype="multipart/form-data">
			<input name="userId" value="${user.userId}" type="hidden"> <input
				name="companyId" value="${company.companyId}" type="hidden">
			<div class="col-md-2"></div>
			<div class="col-md-8">
				<div class="block full">
					<div class="block-title">
						<h4 class="">Organisation Details</h4>
					</div>
					<div class="form-group">
						<label class="col-sm-4 control-label">Number of employees<span
							class="mandatory">*</span>
						</label>
						<div class="col-sm-6">
							<input id="noOfEmployees" name="noOfEmployees" type="text"
								class="form-control" placeholder="e.g. 50-100"
								value="${company.noOfEmployees}">
						</div>
					</div>

				</div>
				<!--                </div>
                <div class="col-md-6">-->
				<div class="block full">
					<div class="block-title">
						<h4 class="">Legal Details</h4>
					</div>
					<div class="form-group">
						<label class="col-sm-4 control-label">Please List your
							Trustee Names, Designations<span class="mandatory">*</span>
						</label>
						<div class="col-sm-6">
							<textarea id="listOfTrustees"
									name="listOfTrustees" class="form-control" rows="2"
									placeholder="e.g. Name-Designation, Name-Designation..">${company.trustees}</textarea>					
						
<!-- 							<input id="listOfTrustees" name="listOfTrustees" type="text" -->
<!-- 								class="form-control" -->
<!-- 								placeholder="e.g. Name-Designation, Name-Designation.." -->
<%-- 								value="${company.trustees}"> --%>
						</div>
					</div>

					<div class="row">
						<label class="col-sm-4 control-label">Upload 2 most recent
							Annual Reports/Audit <span class="mandatory">*</span>
						</label>
						<div class="form-group col-sm-4">
						<div class="col-sm-12">
							<input type="file" id="audit1" name="audit1"><a class="wrap-word"
								target="_blank" href="<%=contexturl%>Files/${company.audit1}">${company.audit1DocumentName}</a>
						</div>
						</div>
						<div class="form-group col-sm-4">
						<div class="col-sm-12">
							<input type="file" id="audit2" name="audit2"><a class="wrap-word"
								target="_blank" href="<%=contexturl%>Files/${company.audit2}">${company.audit2DocumentName}</a>
						</div>
						</div>
					</div>
				</div>
				<div class="block full">
					<div class="block-title">
						<h4 class="">Funding</h4>
					</div>
					<div class="form-group">
						<label class="col-sm-4 control-label">Total budget size<span
							class="mandatory">*</span>
						</label>
						<div class="col-sm-6">
							<input id="budgetSize" name="budgetSize" type="text"
								class="form-control" placeholder="Number"
								value="${company.budgetSize}">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-4 control-label">Existing funders<span
							class="mandatory">*</span>
						</label>
						<div class="col-sm-6">
							<c:choose>
								<c:when test="${not empty existingFund}">
									<c:forEach items="${existingFundersList}" var="existFundList"
										varStatus="count">
										
										
										<c:forEach items="${existingFund}" var="existingFund"
											varStatus="count">
											<c:if test="${existingFund eq existFundList}">
												<c:set var="checktest" value="true"></c:set>
											</c:if>
										</c:forEach>
										
										
										<label class="checkbox"> <c:choose>
												<c:when test="${checktest eq 'true'}">
													<input id="existFund_${counter.count}" type="checkbox"
														name="existingFunders" value="${existFundList}"
														checked="checked" />
													<span class="input-text">${existFundList}</span>
												</c:when>
												<c:otherwise>			
													<c:choose>
														<c:when test="${existFundList eq 'Others'}">
															<input id="existFund_${counter.count}" type="checkbox"
																name="existingFunders" value="${existFundList}" class="Others" />
															<span class="input-text">${existFundList}</span>
														</c:when>
														<c:otherwise>
															<input id="existFund_${counter.count}" type="checkbox"
																name="existingFunders" value="${existFundList}" />
															<span class="input-text">${existFundList}</span>
														</c:otherwise>
													</c:choose>											
												
<%-- 													<input id="existFund_${counter.count}" type="checkbox" --%>
<%-- 														name="existingFunders" value="${existFundList}" --%>
<!-- 													 /> -->
<%-- 													<span class="input-text">${existFundList}</span> --%>
												</c:otherwise>
											</c:choose>
										</label>
										<c:set var="checktest" value="false"></c:set>
									</c:forEach>
								</c:when>
								<c:otherwise>
									<c:forEach items="${existingFundersList}" var="existFund"
										varStatus="counter">
										<label class="checkbox"> <c:choose>
												<c:when test="${existFund eq 'Others'}">
													<input id="existFund_${counter.count}" type="checkbox"
														name="existingFunders" value="${existFund}" class="Others" />
													<span class="input-text">${existFund}</span>
												</c:when>
												<c:otherwise>
													<input id="existFund_${counter.count}" type="checkbox"
														name="existingFunders" value="${existFund}" />
													<span class="input-text">${existFund}</span>
												</c:otherwise>
											</c:choose>

										</label>
									</c:forEach>
								</c:otherwise>
							</c:choose>
						</div>
					</div>
					<c:if test="${not empty company.otherFunders}">
						<div class="form-group" id="existFundDiv">
							<label class="col-sm-3 control-label">Mention the other
								Funders<span class="mandatory">*</span>
							</label>
							<div class="col-sm-8">
								<input id="existFundOther" name="existFundOther" type="text"
									class="form-control" placeholder="Other"
									value="${company.otherFunders}">
							</div>
						</div>
					</c:if>
					<div class="" id="selectedDiv1"></div>
				</div>
			</div>
			<div class="col-md-2"></div>
		</form>
	</div>

</div>

<div class="container">
	<div class="row">
		<div class="text-center">
			<c:if test="${not adminEdit}">
				<button type="button" class="btn btn-primary" onclick="location.href='<%=contexturl%>EditOrganisationalDetailsStep2/'+${user.userId}">Back</button>
			</c:if>
			<c:if test="${adminEdit}">
				<button type="button" class="btn btn-primary" onclick="location.href='<%=contexturl%>EditOrganisationalDetailsByAdmin/step/2/company/${company.companyId}'">Back</button>
			</c:if>
			<button id="step3" type="submit" class="btn btn-warning">Submit this page
			</button>
			 <c:if test="${adminEdit}">
				<button type="button" class="btn btn-info" onclick="location.href='<%=contexturl %>ManageAdmin/OrganizationList'">Back To Organisation List </button>
			</c:if>
		</div>
		<br> <br> <br>
	</div>
</div>

<div id="tempExistFundDiv" style="display: none">
	<div class="form-group" id="existFundDiv">
		 <label class="col-sm-3 control-label">Mention the other Funders<span class="mandatory">*</span>
      	</label>
      	<div class="col-sm-8">
      		<input id="existFundOther" name="existFundOther" type="text" class="form-control" placeholder="Other" value="${personalInfo.funcAreaReason}">
      	</div>
	</div>	 
</div>


<%-- <script src="<%=contexturl%>resources/js/vendor/jquery-1.11.0.min.js"></script> --%>
<%-- <script src="<%=contexturl%>resources/js/vendor/bootstrap.min.js"></script> --%>
<%-- <script src="<%=contexturl%>resources/js/vendor/validate.js"></script> --%>
<script src="${pageContext.request.contextPath}/webjars/counter-webapp-frontend/0.0.1/vendor_bundle.js"></script>

<%-- <script src="<%=contexturl%>resources/js/additional-methods.js"></script> --%>
<script src="${pageContext.request.contextPath}/webjars/counter-webapp-frontend/0.0.1/app_bundle.js"></script>

 <script type="text/javascript">
		$(document).ready(
				function() {
					
					 var existfundDiv= $("#tempExistFundDiv").contents();
					 
					 $("input[name='existingFunders']").change(function(){
						 if($("input[class='Others']:checked").val() == "Others"){
							 $('#selectedDiv1').empty();	
							 $('#selectedDiv1').append(existfundDiv);
						 }
						 else{
							 $('#selectedDiv1').empty();
						 }
					 });

					$("#step3_form").validate(
							{
								errorClass : "help-block animation-slideDown",
								errorElement : "div",
								errorPlacement : function(e, a) {
									a.parents(".form-group > div").append(e)
								},
								highlight : function(e) {
									$(e).closest(".form-group").removeClass(
											"has-success has-error").addClass(
											"has-error")
								},
								success : function(e) {
									e.closest(".form-group").removeClass(
											"has-success has-error")
								},
								rules : {
									noOfEmployees : {
										required : !0,
										employeeno : true,
									},
									listOfTrustees : {
										required : !0,
										lettersandSpecialchars : true,
										maxlength : 1000,
									},
									<c:if test="${empty company.audit1DocumentName}">
									audit1:{
										required : !0,
										extension : "pdf"
									},
									</c:if>
									
									<c:if test="${empty company.audit2DocumentName}">
									audit2 : {
										required : !0,
										extension : "pdf"
									},
									</c:if>
									
									<c:if test="${not empty company.audit1DocumentName}">
									audit1:{
										extension : "pdf"
									},
									</c:if>
									
									<c:if test="${not empty company.audit2DocumentName}">
									audit2 : {
										extension : "pdf"
									},
									</c:if>
									
									budgetSize : {
										required : !0,
										currency: true,
										maxlength : 7,
									},
									existingFunders : {
										required : !0,
									},
									existFundOther : {
										maxlength : 100,
									},
								},
								messages : {
									noOfEmployees : {
										required : 'Please enter the number of Employees',
										employeeno : 'Please enter a correct value',
									},
									listOfTrustees : {
										required : 'Please enter the list of Trustees',
										lettersandSpecialchars : 'Please enter the name and designation as shown in the example',
										maxlength : 'You cannot enter more than 1000 characters',
									},
									<c:if test="${empty company.audit1DocumentName}">
									audit1 : {
										required : 'Please select a file',
										extension : 'Please upload a .pdf file only',
									},
									</c:if>
									
									<c:if test="${empty company.audit2DocumentName}">
									audit2 : {
										required : 'Please select a file',
										extension : 'Please upload a .pdf file only',
									},
									</c:if>
									
									<c:if test="${not empty company.audit1DocumentName}">
									audit1 : {
										extension : 'Please upload a .pdf file only',
									},
									</c:if>
									
									<c:if test="${not empty company.audit2DocumentName}">
									audit2 : {
										extension : 'Please upload a .pdf file only',
									},
									</c:if>
									budgetSize : {
										required : 'Please enter the budget size',
										currency : 'Please enter a correct amount',
										maxlength : 'Only upto 7 digits allowed',
									},
									existingFunders : {
										required : 'Please select atleast one existing funder',
									},
									existFundOther : {
										maxlength : 'Only 100 characters allowed',
									},
								},
							});
					
					
					jQuery.validator.addMethod("lettersandSpecialchars", function( value, element ) {
				        var regex = new RegExp("^[a-zA-Z-,. ]*$");
				        var key = value;

				        if (!regex.test(key)) {
				           return false;
				        }
				        return true;
				    }, "please use only alphabetic characters");
					
					jQuery.validator.addMethod("currency", function( value, element ) {
						var regex = /^[1-9]\d*(((,\d{3}){1})?(\.\d{0,2})?)$/;
				        var key = value;

				        if (!regex.test(key)) {
				           return false;
				        }
				        return true;
				    }, "Incorrect value");
					
					//At least one (+) digit (\d), followed by an optional group containing a hyphen-minus (-), followed by at least one digit again.	
					jQuery.validator.addMethod("employeeno", function( value, element ) {
						var regex = /^\d{1,5}(-\d{1,5})?$/;
				        var key = value;

				        if (!regex.test(key)) {
				           return false;
				        }
				        return true;
				    }, "Incorrect value");
									
					
					$("#step3").click(function() {
						unsaved = false;
						$("#step3_form").submit();
					});

				});
	</script>

 <%@include file="../../inc/page_footer.jsp"%>