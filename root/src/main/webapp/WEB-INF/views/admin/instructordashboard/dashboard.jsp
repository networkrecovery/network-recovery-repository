<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->



<%@include file="../inc/config.jsp"%>
<%@include file="../inc/template_start.jsp"%>
<%@include file="../inc/page_head.jsp"%>
    

<!-- Page content -->
<div id="page-content">
    <!-- Dashboard Header -->
    <!-- For an image header add the class 'content-header-media' and an image as in the following example -->
    <div class="content-header content-header-media">
        <div class="header-section">
            <div class="row">
                <!-- Main Title (hidden on small devices for the statistics to fit) -->
                <div class="col-md-4 col-lg-6 hidden-xs hidden-sm">
                     <h1><strong>${brand.brandName}</strong><br><small>${brand.company.companyName }</small></h1>
                </div>
                <!-- END Main Title -->

            </div>
        </div>
        <!-- For best results use an image with a resolution of 2560x248 pixels (You can also use a blurred image with ratio 10:1 - eg: 1000x100 pixels - it will adjust and look great!) -->
        <img src="<%=contexturl %>resources/img/placeholders/headers/dashboard_header.jpg" alt="header image" class="animation-pulseSlow">
    </div>
    <!-- END Dashboard Header -->

    <!-- Mini Top Stats Row -->
    <div class="row">
    
        <div class="col-sm-4">
            <div class="widget">
                <div class="widget-simple">
                    <a href="<%=contexturl %>ManageRestaurant/RestaurantList" class="widget-icon pull-left themed-background animation-fadeIn">
                        <i class="gi gi-fast_food"></i>
                    </a>
                    <h3 class="widget-content animation-pullDown text-right">
                        <strong>${restCount}</strong><br>
                         Active Restaurants
                    </h3>
                </div>
            </div>
        </div>
        
        <div class="col-sm-4">
            <!-- Widget -->
            <div class="widget">
                <div class="widget-simple">
                    <a href="#" class="widget-icon pull-left themed-background-fire animation-fadeIn">
                        <i class="hi hi-gift"></i>
                    </a>
                    <h3 class="widget-content text-right animation-pullDown">
                        <strong>${offerCount}</strong><br>
                        Valid Offers
                    </h3>
                </div>
            </div>
            <!-- END Widget -->
        </div>
        
        <div class="col-sm-4">
            <!-- Widget -->
            <div class="widget">
                <div class="widget-simple">
                    <a href="#" class="widget-icon pull-left themed-background-amethyst animation-fadeIn">
                        <i class="gi gi-picture"></i>
                    </a>
                    <h3 class="widget-content text-right animation-pullDown">
                        <strong>${promocodeCount}</strong><br>
                        Promocode Offers
                    </h3>
                </div>
            </div>
            <!-- END Widget -->
        </div>
        
<!--         <div class="col-sm-6"> -->
<!--             <div class="widget"> -->
<!--                 <div class="widget-simple"> -->
<!--                     <a href="page_widgets_stats.php" class="widget-icon pull-left themed-background animation-fadeIn"> -->
<!--                         <i class="gi gi-crown"></i> -->
<!--                     </a> -->
<!--                     <div class="pull-right"> -->
<!--                         <span id="mini-chart-brand"></span> -->
<!--                     </div> -->
<!--                     <h3 class="widget-content animation-pullDown visible-lg"> -->
<!--                         Our <strong>Brand</strong> -->
<!--                         <small>Popularity over time</small> -->
<!--                     </h3> -->
<!--                 </div> -->
<!--             </div> -->
<!--         </div> -->
    </div>
    <!-- END Mini Top Stats Row -->

    <!-- Widgets Row -->
    <div class="row">
        <div class="col-md-6">
        
            <!-- START Top 5 Rated Restaurant Widget -->
            <div class="widget">
                <div class="widget-extra themed-background-dark">
                    <h3 class="widget-content-light">
                        <strong>Top 5 Rated Restaurants</strong> <small>
                            <a href="<%=contexturl %>ManageRestaurant/RestaurantList">
                                    <strong>View All</strong></a></small>
                    </h3>
                </div>
                
                <div class="block">
                        <div class="table-responsive">
                            <table id="general-table" class="table table-vcenter table-hover">
                                <thead>
                                    <tr>
                                        <th class="text-center"><spring:message code="menu.restaurant"/></th>
                                        <th class="text-center"><spring:message code="label.myrating"/></th>
                                    </tr>
                                </thead>
                                <tbody id="tbody">
                                    <c:forEach var="currentUserOutletRating" items="${outletRatingList}" varStatus="count">
                                    <tr>
                                        <td class="text-center" style="min-width: 300px;"><c:choose>
                                                <c:when test="${currentUserOutletRating.logoImage!=null}">
                                                    <img
                                                        src="<%=contexturl%>${currentUserOutletRating.logoImage.thumbImagePath}"
                                                        alt="Avatar" class="pull-left img-circle"
                                                        style="margin-left: 30px; width: 60px;" />
                                                </c:when>
                                                <c:otherwise>
                                                    <img
                                                        src="<%=contexturl%>resources/img/application/rest-logo-default.jpg"
                                                        alt="Avatar" class="pull-left img-circle"
                                                        style="margin-left: 30px; width: 60px;" />
                                                </c:otherwise>
                                            </c:choose>
                                            <div style="margin-top: 15px"><a href="<%=contexturl %>ManageRestaurant/RestaurantList/RestaurantRatingAndFeedback?outletId=${currentUserOutletRating.outletId}" >${currentUserOutletRating.name}</a></div>
                                        </td>
                                        <td class="text-center">
                                                <div id="b_${currentUserOutletRating.outletId}" class="row" >
                                                    <div id="useravgrating${currentUserOutletRating.outletId}" class="col-md-2"
                                                        style="cursor: pointer;"></div>
                                                </div>
                                        </td>
                                    </tr>
                                </c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </div>
            </div>
            
        <!-- END Top 5 Rated Restaurant Widget -->
        
            <!-- Start Checkin Widget -->
            <div class="widget">
                <div class="widget-extra themed-background-dark">
                        <h3 class="widget-content-light">
                         <strong>Checkin Count</strong>
                         </h3>
                </div>
                <div class="widget-extra-full">
                    <div class="widget-content animation-pullDown visible-lg">
                        Currently CheckedIn : ${checkedInCount}
                        <div class="row tab-content">
                            <div style="margin-left: 15px;margin-right: 10px">
                                <div>In current year.</div>
                                <div id="chart-checkin" class="chart"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Checkin Widget -->
            
            <!-- START Top 5 Restaurant Offer Widget -->
            <div class="widget">
                <div class="widget-extra themed-background-dark">
                    <h3 class="widget-content-light">
                        <strong>Restaurants with Max Offers</strong> <small>
                            <a href="<%=contexturl %>ManageRestaurant/RestaurantList">
                                    <strong>View All</strong></a></small>
                    </h3>
                </div>
                
                <div class="block">
                        <div class="table-responsive">
                            <table id="general-table" class="table table-vcenter table-hover">
                                <thead>
                                    <tr>
                                        <th class="text-center"><spring:message code="menu.restaurant"/></th>
                                        <th class="text-center">Offer Count</th>
                                    </tr>
                                </thead>
                                <tbody id="tbody">
                                    <c:forEach var="currentOutletOffer" items="${outletOfferList}" varStatus="count">
                                    <tr>
                                        <td class="text-center" style="min-width: 300px;"><c:choose>
                                                <c:when test="${currentOutletOffer[0].logoImage!=null}">
                                                    <img
                                                        src="<%=contexturl%>${currentOutletOffer[0].logoImage.thumbImagePath}"
                                                        alt="Avatar" class="pull-left img-circle"
                                                        style="margin-left: 30px; width: 60px;" />
                                                </c:when>
                                                <c:otherwise>
                                                    <img
                                                        src="<%=contexturl%>resources/img/application/rest-logo-default.jpg"
                                                        alt="Avatar" class="pull-left img-circle"
                                                        style="margin-left: 30px; width: 60px;" />
                                                </c:otherwise>
                                            </c:choose>
                                            <div style="margin-top: 15px"><a href="<%=contexturl %>ManageRestaurant/RestaurantList/OfferList?outletId=${currentOutletOffer[0].outletId}" >${currentOutletOffer[0].name}</a></div>
                                        </td>
                                        <td class="text-center">
                                                ${currentOutletOffer[1]}
                                        </td>
                                    </tr>
                                </c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </div>
            </div>
            
       <!-- END Top 5 Restaurant Offer Widget -->
            
        </div>
        <div class="col-md-6">
        
           <!--Start of promocode chart widget -->
           <div class="widget">
                <div class="widget-extra themed-background-dark">
                    <h3 class="widget-content-light">
                         <strong>Promocodes</strong>
                     </h3>
                </div>
                <div id="chart-promocode" class="chart"></div>
            </div>
            <!--End of promocode chart widget -->
            
             <!-- Start Restaurant Feedback Widget -->
            <div class="widget">
                <div class="widget-extra themed-background-dark">
                    <h3 class="widget-content-light">
                        <strong>Latest Feedbacks</strong> 
                    </h3>
                </div>

                <div class="widget-extra">
                    <!-- Timeline Content -->
                    <div class="timeline">
                        <ul class="timeline-list">
                            <c:forEach var="currentOutletFeedback"
                                items="${outletFeedbackList}" varStatus="count">
                                <li class="active">
                                    <div class="timeline-icon">
                                        <c:choose>
                                            <c:when
                                                test="${! empty currentOutletFeedback.user.profileImage and !empty currentOutletFeedback.user.profileImage.imagePath}">
                                                <img style="width: 42px; height: 36px; margin-left: -5px;"
                                                    src="<%=contexturl%>${currentOutletFeedback.user.profileImage.imagePath}"
                                                    alt="avatar" class="pull-left img-circle feedback_profile">

                                            </c:when>
                                            <c:otherwise>
                                                <img style="width: 42px; height: 36px; margin-left: -5px;"
                                                    src="<%=contexturl%>resources/img/placeholders/avatars/avatar2.jpg"
                                                    alt="avatar" class="pull-left img-circle feedback_profile">

                                            </c:otherwise>
                                        </c:choose>
                                    </div>
                                    <div class="timeline-time">
                                        <small><fmt:formatDate
                                                pattern="<%=propvalue.simpleDateFormat %>"
                                                value="${currentOutletFeedback.createdDate}" /></small>
                                    </div>
                                    <div class="timeline-content">
                                        <p class="push-bit">
                                            <a href="#"><strong>${currentOutletFeedback.user.firstName}:</strong></a>
                                            <a style="margin-left: 100px;" href="<%=contexturl %>ManageRestaurant/RestaurantList/RestaurantRatingAndFeedback?outletId=${currentOutletFeedback.outlet.outletId}"><strong>${currentOutletFeedback.outlet.name}</strong></a>
                                        </p>
                                        <p class="push-bit">${currentOutletFeedback.feedback}</p>
                                    </div>
                                </li>
                            </c:forEach>
                        </ul>
                    </div>
                </div>
                </div>
            <!-- End Restaurant Feedback Widget -->
            
            <!-- START New Product Info Widget -->
            <div class="widget">
                <div class="widget-extra themed-background-dark">
                    <h3 class="widget-content-light">
                        <strong>New Product Info</strong> 
                    </h3>
                </div>
                    <div class="col-sm-12 block-section text-center  animation-fadeIn" style="padding:0;">
                        <img src="<%=contexturl %>resources/img/placeholders/photos/photo3.jpg" class="img-responsive" alt="image"> 
                    </div>
            </div>
            <!-- END New Product Info Widget -->
            
        </div>
    </div>
    <!-- END Widgets Row -->
</div>
<!-- END Page Content -->
<%@include file="../inc/page_footer.jsp"%>

<%@include file="../inc/template_scripts.jsp"%>

<script type="text/javascript">
    $(document).ready(function() {
        
         <c:forEach var="currentUserOutletRating" items="${outletRatingList}" varStatus="count">
            $('#useravgrating${currentUserOutletRating.outletId}').raty({
                readOnly    : true,
                hints       : ['', '', '', '', ''],
                half        : true,
                path        : '<%=contexturl%>resources/img/raty/',
                scoreName   : "useravgRating${currentUserOutletRating.outletId}",   
                width       : 125,
                score       : '${currentUserOutletRating.avgRating}'
            });
        </c:forEach>
        
         var chartCheckin = $('#chart-checkin');
         var checkinData =${checkinData};
         
         var chartPromocode = $('#chart-promocode');
         var totalData = ${totalData};
         var assignedData = ${assignedData};
         var usedData = ${usedData};
         var chartPromo = ${chartData};
         
         var chartMonths = [[1, 'Jan'], [2, 'Feb'], [3, 'Mar'], [4, 'Apr'], [5, 'May'], [6, 'Jun'], [7, 'Jul'], [8, 'Aug'], [9, 'Sep'], [10, 'Oct'], [11, 'Nov'], [12, 'Dec']];
         
        // Classic Chart for Checkins
         $.plot(chartCheckin,
            [
                {
                    label: 'Total',
                    data: checkinData,
                    lines: {show: true, fill: true, fillColor: {colors: [{opacity: 0.25}, {opacity: 0.25}]}},
                    points: {show: true, radius: 6}
                }
                
            ],
            {
                colors: ['#3498db', '#333333','#aaa'],
                legend: {show: true, position: 'nw', margin: [15, 10]},
                grid: {borderWidth: 0, hoverable: true, clickable: true},
                yaxis: {ticks: 4, tickColor: '#eeeeee'},
                xaxis: {ticks: chartMonths, tickColor: '#ffffff'}
            }
        );
        
        
         $.plot(chartPromocode,
                 [
                     {
                         label: 'Total',
                         data: totalData,
                         lines: {show: true, fill: true, fillColor: {colors: [{opacity: 0.25}, {opacity: 0.25}]}},
                         points: {show: true, radius: 6}
                     },
                     {
                         label: 'Assigned',
                         data: assignedData,
                         lines: {show: true, fill: true, fillColor: {colors: [{opacity: 0.15}, {opacity: 0.15}]}},
                         points: {show: true, radius: 6}
                     },
                     {
                         label: 'Used',
                         data: usedData,
                         lines: {show: true, fill: true, fillColor: {colors: [{opacity: 0.15}, {opacity: 0.15}]}},
                         points: {show: true, radius: 6}
                     }
                 ],
                 {
                     colors: ['#3498db', '#333333','#aaa'],
                     legend: {show: true, position: 'nw', margin: [15, 10]},
                     grid: {borderWidth: 0, hoverable: true, clickable: true},
                     yaxis: {ticks: 4, tickColor: '#eeeeee'},
                     xaxis: {ticks: chartPromo, tickColor: '#ffffff'}
                 }
             );
         
         // Creating and attaching a tooltip to the classic chart
         var previousPoint = null, ttlabel = null;
         chartPromocode.bind('plothover', function(event, pos, item) {

             if (item) {
                 if (previousPoint !== item.dataIndex) {
                     previousPoint = item.dataIndex;

                     $('#chart-tooltip').remove();
                     var x = item.datapoint[0], y = item.datapoint[1];

                     if (item.seriesIndex === 2) {
                         ttlabel = '<strong>' + y + '</strong> used';
                     } 
                     else if (item.seriesIndex === 1) {
                         ttlabel = '<strong>' + y + '</strong> assigned';
                     } else {
                         ttlabel = '<strong>' + y + '</strong> promo';
                     }

                     $('<div id="chart-tooltip" class="chart-tooltip">' + ttlabel + '</div>')
                         .css({top: item.pageY - 45, left: item.pageX + 5}).appendTo("body").show();
                 }
             }
             else {
                 $('#chart-tooltip').remove();
                 previousPoint = null;
             }
         });
        
    });
</script>

<!-- Google Maps API + Gmaps Plugin, must be loaded in the page you would like to use maps (Remove 'http:' if you have SSL) -->
<script src="http://maps.google.com/maps/api/js?sensor=true"></script>
<script src="<%=contexturl %>resources/js/helpers/gmaps.min.js"></script>

<!-- Load and execute javascript code used only in this page -->
<script src="<%=contexturl %>resources/js/pages/index.js"></script>
<script>$(function(){ Index.init(); });</script>

<%@include file="../inc/template_end.jsp"%>