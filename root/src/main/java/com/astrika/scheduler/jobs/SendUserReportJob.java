package com.astrika.scheduler.jobs;


import java.util.List;

import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;

import com.astrika.companymngt.model.company.CompanyMaster;
import com.astrika.companymngt.repository.CompanyRepository;
import com.astrika.organisationAttendancemngt.model.OrganisationAttendance;
import com.astrika.organisationAttendancemngt.repository.OrganisationAttendanceRepository;
import com.astrika.organisationAttendancemngt.service.OrganisationAttendanceService;


@Component
public class SendUserReportJob extends  QuartzJobBean{
	
	
	@Autowired
	private OrganisationAttendanceService attendanceService;
	
	 private static final Logger LOGGER = Logger.getLogger(SendUserReportJob.class);
	
	@Override
	protected void executeInternal(JobExecutionContext context)
			throws JobExecutionException {
		try{
			String path = CompanyMaster.class.getProtectionDomain().getCodeSource().getLocation().getPath();
			path = path.substring(1,path.lastIndexOf("/WEB-INF"));
			ApplicationContext ctx = new FileSystemXmlApplicationContext(path+"/WEB-INF/application-context.xml");
			CompanyRepository compRepo = (CompanyRepository) ctx.getBean("companyRepository");
			OrganisationAttendanceRepository orgRepo = (OrganisationAttendanceRepository) ctx.getBean("organisationAttendanceRepository");
			ctx.getApplicationName();
			List<CompanyMaster> companyList = compRepo.findAllByActive(true);
			for(CompanyMaster company : companyList){
				OrganisationAttendance attendance = new OrganisationAttendance();
				attendance.setCompany(company);
				attendance.setActive(false);
				orgRepo.save(attendance);				
			}
	
		}catch(Exception e){
		    LOGGER.info(e);
		}
		
	}
	
	

}
