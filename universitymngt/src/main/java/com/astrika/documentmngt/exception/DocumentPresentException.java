package com.astrika.documentmngt.exception;

import com.astrika.kernel.exception.BusinessException;
import com.astrika.kernel.exception.CustomException;

public class DocumentPresentException extends BusinessException{

	public DocumentPresentException() {
		super(CustomException.DOCUMENT_ALREADY_PRESENT.getCode());
	}

	public DocumentPresentException(String errorCode) {
		super(errorCode);
	}
		
}
