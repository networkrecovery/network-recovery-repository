package com.astrika.skill.controller.superadmin;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.jfree.io.IOUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.data.domain.Page;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.astrika.categorymngt.model.CategoryMaster;
import com.astrika.categorymngt.service.CategoryService;
import com.astrika.common.exception.DuplicateEmailException;
import com.astrika.common.exception.NoSuchImageException;
import com.astrika.common.exception.NoSuchSurveyException;
import com.astrika.common.exception.NoSuchUserException;
import com.astrika.common.model.CompanyApproval;
import com.astrika.common.model.CurrencyMaster;
import com.astrika.common.model.ImageMaster;
import com.astrika.common.model.InviteStatus;
import com.astrika.common.model.LanguageMaster;
import com.astrika.common.model.Role;
import com.astrika.common.model.User;
import com.astrika.common.model.UserStatus;
import com.astrika.common.model.location.AreaMaster;
import com.astrika.common.model.location.CityMaster;
import com.astrika.common.model.location.CountryMaster;
import com.astrika.common.model.location.StateMaster;
import com.astrika.common.service.AreaService;
import com.astrika.common.service.CityService;
import com.astrika.common.service.CountryService;
import com.astrika.common.service.CurrencyExchangeRateService;
import com.astrika.common.service.CurrencyService;
import com.astrika.common.service.DocumentService;
import com.astrika.common.service.ImageService;
import com.astrika.common.service.LanguageService;
import com.astrika.common.service.StateService;
import com.astrika.common.service.UserService;
import com.astrika.common.util.PropsValues;
import com.astrika.common.util.RandomCodeGenerator;
import com.astrika.companymngt.exception.DuplicateCompanyException;
import com.astrika.companymngt.model.company.CompanyMaster;
import com.astrika.companymngt.model.company.CompanySurveyResetManager;
import com.astrika.companymngt.model.module.ModuleMaster;
import com.astrika.companymngt.model.personaldetails.PersonalInfo;
import com.astrika.companymngt.service.CompanyService;
import com.astrika.companymngt.service.ModuleService;
import com.astrika.companymngt.service.PersonalInfoService;
import com.astrika.documentmngt.model.AtmaDocumentMaster;
import com.astrika.documentmngt.model.AtmaDocumentMaster_aud;
import com.astrika.documentmngt.service.AtmaDocumentService;
import com.astrika.educationsector.service.EducationSectorService;
import com.astrika.forummngt.model.Comments;
import com.astrika.forummngt.model.ForumMaster;
import com.astrika.forummngt.model.ForumTag;
import com.astrika.forummngt.service.CommentService;
import com.astrika.forummngt.service.ForumService;
import com.astrika.globalsponsoredmngt.model.GlobalSponsoredManagement;
import com.astrika.globalsponsoredmngt.service.GlobalSponsoredService;
import com.astrika.kernel.exception.BusinessException;
import com.astrika.kernel.exception.SystemException;
import com.astrika.modulemngt.exception.DuplicateModuleException;
import com.astrika.moduleusermanagement.exception.FileNotFoundException;
import com.astrika.moduleusermanagement.model.ModuleUser;
import com.astrika.moduleusermanagement.model.NextSteps;
import com.astrika.moduleusermanagement.service.ModuleUserService;
import com.astrika.moduleusermanagement.service.NextStepsService;
import com.astrika.organisationAttendancemngt.service.OrganisationAttendanceService;
import com.astrika.registrationmngt.model.EnquiryList;
import com.astrika.registrationmngt.service.EnquiryService;
import com.astrika.surveymngt.model.LifeStageResultMaster;
import com.astrika.surveymngt.model.LifeStages;
import com.astrika.surveymngt.model.SurveyBuilder;
import com.astrika.surveymngt.model.SurveyManager;
import com.astrika.surveymngt.model.SurveyStatus;
import com.astrika.surveymngt.model.VisitorSurveyBuilder;
import com.astrika.surveymngt.service.LifeStageResultService;
import com.astrika.surveymngt.service.SurveyBuilderService;
import com.astrika.surveymngt.service.SurveyManagerService;
import com.astrika.surveymngt.service.SurveyResultService;
import com.astrika.surveymngt.service.VisitorSurveyBuilderService;
import com.astrika.universitymngt.model.Status;
import com.astrika.weightagemngt.model.ProfileWeightage;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Controller
public class SuperAdminController {

    @Autowired
    private AreaService areaService;

    @Autowired
    private EducationSectorService educationSectorService;

    @Autowired
    PropsValues propertyValues;

    private static final Logger LOGGER = Logger.getLogger(SuperAdminController.class);

    @Autowired
    private CompanyService companyService;

    @Autowired
    private CityService cityService;

    @Autowired
    private CountryService countryService;

    @Autowired
    private StateService stateService;

    @Autowired
    private ImageService imageService;

    @Autowired
    private CurrencyService currencyService;

    @Autowired
    private LanguageService languageService;

    @Autowired
    private UserService userService;

    @Autowired
    private ModuleService moduleService;

    @Autowired
    CurrencyExchangeRateService currencyExchangeRateService;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private PropsValues propsValue;

    @Autowired
    private AtmaDocumentService atmadocumentService;

    @Autowired
    private ModuleUserService moduleUserService;

    @Autowired
    private ForumService forumService;

    @Autowired
    private EnquiryService enquiryService;

    @Autowired
    private CommentService commentService;

    @Autowired
    private SurveyBuilderService surveyBuilderService;

    @Autowired
    private VisitorSurveyBuilderService visitorSurveyBuilderService;

    @Autowired
    private SurveyManagerService surveyManagerService;

    @Autowired
    private NextStepsService nextStepsService;

    @Autowired
    private GlobalSponsoredService globalSponsoredService;

    @Autowired
    private SurveyResultService surveyResultService;

    @Autowired
    private PersonalInfoService personalInfoService;

    @Autowired
    private LifeStageResultService lifeStageResultService;

    @Autowired
    private OrganisationAttendanceService attendanceService;

    @Autowired
    private DocumentService documentService;

    private static final String MONITORINGANDEVALUATION = "Monitoring and Evaluation";
    private static final String LEADERSHIP = "Leadership";
    private static final String MARKETING="Marketing";
    private static final String ADMINISTRATION="Administration";
    private static final String HUMANRESOURCES="Human Resources";
    private static final String STRATEGY="Strategy";

    private static final String [] header= {HUMANRESOURCES,ADMINISTRATION,LEADERSHIP, "Finance","Fundraising",MONITORINGANDEVALUATION,"Programs and Services",MARKETING,"Governance",STRATEGY};
    private static final List<String> str = Arrays.asList(HUMANRESOURCES,ADMINISTRATION,LEADERSHIP,"Finance","Fundraising",MONITORINGANDEVALUATION,"Programs and Services",MARKETING,"Governance",STRATEGY);
    private static final List<String> STRARRAY= Arrays.asList("Education","Environmental","Livelihood","Advocacy","Rural Development","Human Rights","Technical Assistance","Corporate Foundations","Healthcare");
    private static final List<String> educationList = Arrays.asList("Full Time School","Early Childhood Organisation","Special Education","Remediation And Coaching","LifeSkills","Girls Education");

    private static final String HEADERR="header";
    private static final String COLOR="color";
    private static final String COMPANYID="companyId";
    private static final String CITYNAME="city_cityName";
    private static final String USERLIST="userList";
    private static final String MODULELIST="moduleList";
    private static final String LABEL="label";
    private static final String COUNT="count";
    private static final String LABLE="lable";
    private static final String MODULE="module";
    private static final String EDUCATIONS="educationList";
    private static final String MODULES="modules";
    private static final String SUCCESS="success";
    private static final String GLOBALSPONSOR="Global Sponsor";
    private static final String CONTACT="Contact";
    private static final String COMPANY="company";
    private static final String MODULEUSERLIST="moduleUserList";
    private static final String GLOBALSPONSORIMAGELIST="globalSponsorImageList";
    private static final String ATMACONTACTIMAGE="atmaContactImage";
    private static final String ERROR="error";
    private static final String FORUMLIST="forumList";
    private static final String BLUEPRINT="BluePrint";
    private static final String TOOLBOX="Toolbox";
    private static final String EXAMPLES="Examples";
    private static final String READING="Reading";
    private static final String CATEGORY="category";
    private static final String CITYLIST="cityList";
    private static final String COUNTRYLIST="countryList";
    private static final String COMPANYLIST="companyList";
    private static final String LIST1="list1";
    private static final String LIST2="list2";
    private static final String LIST3="list3";
    private static final String LIST4="list4";
    private static final String LIST5="list5";
    private static final String MODULEID="moduleId";
    private static final String MLIST="mList";
    private static final String CATEGORYLIST="categoryList";
    private static final String LIFESTAGELIST="lifeStageList";
    private static final String RESTAURANTIMAGES="restaurantImages";
    private static final String CONTENTDISPOSITION="Content-Disposition";
    private static final String INACTIVEUSER="Inactive User";
    private static final String ATMAADMINHISTORY="AtmaAdminHistory_";
    private static final String TEXTCSV="text/csv";
    private static final String TITLE="title";
    private static final String PROFILECOMPLETIONCHART="Profile Completion Chart";
    private static final String INACTIVEORGANIZATION="Inactive Organisation";
    private static final String SUPERADMINHISTORY="SuperAdminHistory_";
    private static final String FIRSTNAME="firstName";
    private static final String RESULTS="results";
    private static final String REDIRECT="redirect:/";
    private static final String REDIRECT_TO_LOGIN="redirect:/Login";
    private static final String ALERT_ACCESS_DENIED="alert.accessdenied";
    private static final String COUNTMODULE="countModule";
    private static final String USERMODULE="userModule";
    private static final String FORUMMODULE="forumModule";
    private static final String DOCLIST="docList";

    private User getLogedInUser() {
        User user = null;
        Object object = SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        if (object instanceof User)
            user = (User) object;

        return user;
    }

    @RequestMapping("/*")
    public String showDashboard(Map<String, Object> model) throws NoSuchUserException{
        User user;
        try {
            user = (User) SecurityContextHolder.getContext()
                    .getAuthentication().getPrincipal();
        } catch (Exception e) {
            user = null;
            LOGGER.info(e);
        }
        if (user != null) {
            user = userService.findById(user.getUserId());
        }

        if(user.getRole() == Role.ATMA_ADMIN){
            return "redirect:/AtmaAdmin";
        }
        else if(user.getRole() == Role.COMPANY_ADMIN){
            return "redirect:/CompanyAdmin";
        }
        else if(user.getRole() == Role.MODULE_USER){
            return "redirect:/ModuleUser";
        }
        else{

            int totalDownloads = atmadocumentService.totalSumofDownloads();
            DateFormat dateFormat= new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            Date date= new Date();
            String s=dateFormat.format(date);

            List<ModuleMaster> modules = moduleService.findByActive(true);
            Map<String, Object> arrayMap=getModuleArray(modules);

            List<Integer> countperModule = (List<Integer>) arrayMap.get(COUNTMODULE);
            List<Integer> userperModule = (List<Integer>) arrayMap.get(USERMODULE);
            List<Integer> forumperModule = (List<Integer>) arrayMap.get(FORUMMODULE);
            List<AtmaDocumentMaster> documentList = (List<AtmaDocumentMaster>) arrayMap.get(DOCLIST);
            ObjectMapper mapper=new ObjectMapper();

            int [] companyProfileArray=getCompanyProfileArray();
            String companyProfileArrayJson=null;
            try {
                companyProfileArrayJson =mapper.writeValueAsString(companyProfileArray);
            } catch (IOException e2) {
                LOGGER.info(e2);
            }

            int [] inactiveUserArray= new int[7];

            inactiveUserArray[0]=0;
            inactiveUserArray[1]=userService.noOfInactiveUser(s,30);
            inactiveUserArray[2]=userService.noOfInactiveUser(s,60);
            inactiveUserArray[3]=userService.noOfInactiveUser(s,90);
            inactiveUserArray[4]=userService.noOfInactiveUser(s,120);
            inactiveUserArray[5]=userService.noOfInactiveUser(s,150);
            inactiveUserArray[6]=userService.noOfInactiveUser(s,180);

            String inactiveUserArrayJson=getJsonString(inactiveUserArray);


            int totalUser=userService.getTotalUser();
            int activeUsersMoreThanOneMonth=userService.activeUsersMoreThanOneMonth(Role.COMPANY_ADMIN, Role.MODULE_USER);
            int lastMonthCreatedUser=userService.getlastMonthCreatedUser(Role.COMPANY_ADMIN,Role.MODULE_USER);
            int secondLastMonthCreatedUser=userService.secondLastMonthCreatedUser(Role.COMPANY_ADMIN,Role.MODULE_USER);
            int resultInpercentage=0;
            if(lastMonthCreatedUser!=0 && secondLastMonthCreatedUser!=0){
                resultInpercentage=(lastMonthCreatedUser-secondLastMonthCreatedUser)*100/secondLastMonthCreatedUser;
            }else{
                if(secondLastMonthCreatedUser==0 && lastMonthCreatedUser==0){
                    resultInpercentage=0;
                }
                if(lastMonthCreatedUser==0){
                    resultInpercentage=-secondLastMonthCreatedUser*100;
                }
                if(secondLastMonthCreatedUser==0){
                    resultInpercentage=lastMonthCreatedUser*100;
                }
            }

            int totalOraganisation=companyService.getTotalOraganisation();
            int lastMonthCreatedOrganisation=companyService.getLastMonthCreatedOrganisation();
            int moreThanLastMonthOrganisation=companyService.getMoreThanLastMonthOrganisation();
            int secondLastMonthActiveOrganisation=companyService.getSecondLastMonthActiveOrganisation();
            int increaseoaganisationPercentage=getIncreaseoaganisationPercentage(secondLastMonthActiveOrganisation,lastMonthCreatedOrganisation);

            int [] inactiveOrganisationArray=new int[7];
            inactiveOrganisationArray[0]=0;
            inactiveOrganisationArray[1]=companyService.getInctiveOrganisation(30);
            inactiveOrganisationArray[2]=companyService.getInctiveOrganisation(60);
            inactiveOrganisationArray[3]=companyService.getInctiveOrganisation(90);
            inactiveOrganisationArray[4]=companyService.getInctiveOrganisation(120);
            inactiveOrganisationArray[5]=companyService.getInctiveOrganisation(150);
            inactiveOrganisationArray[6]=companyService.getInctiveOrganisation(180);
            String inactiveOrganisationArrayJson=getJsonString(inactiveOrganisationArray);

            int oneMonthDownload=atmadocumentService.getLastMonthDownloads();
            int moreThanOneMonthDownloads=totalDownloads-oneMonthDownload;
            int secondLastMonthDownloads=atmadocumentService.getsecondLastMonthDownloads();
            int percentageIncreaseDownloads=getPercentageIncreaseDownloads(oneMonthDownload,secondLastMonthDownloads);


            List<Object[]> recordList=surveyResultService.getAllByModuleIdAndRating();
            Map map2=getsurveyList(recordList);

            String jsonStringexa = null;
            try {
                jsonStringexa = mapper.writeValueAsString(map2);
            } catch (JsonGenerationException e1) {
                LOGGER.info(e1);
            } catch (JsonMappingException e1) {
                LOGGER.info(e1);
            } catch (IOException e1) {
                LOGGER.info(e1);
            }

            List<Object[]> topCities = companyService.getTopCities();

            List<Map<String, Object>> result = getTopCitiesList(topCities);
          
            List<Object[]> educationArea=companyService.getAllRecod();
            String[]  colorArr={"#00F9B7","#51DAB6","#02A278","#8BD4B3","#2BB376","#1BF7F9","#49ADF7","#3B93D4","#00F9B7"};

            List<Map<String, Object>> mapResultTest = new ArrayList<>();

            int k=0;
            for (String string : STRARRAY) {
                mapResultTest=buildEducationmap(k,string,educationArea,colorArr,mapResultTest);
            }

            String eduJason=null;
            try {
                eduJason = mapper.writeValueAsString(mapResultTest);
            } catch (JsonGenerationException e) {
                LOGGER.info(e);
            } catch (JsonMappingException e) {
                LOGGER.info(e);
            } catch (IOException e) {
                LOGGER.info(e);
            }

            List<String> monthList = new ArrayList<>();
            String[] months = new DateFormatSymbols().getMonths();
            for (int i = 0; i < months.length; i++) {
                String month = months[i];
                monthList.add(month);
            }

            Calendar now = Calendar.getInstance(); 
            int currentYear = now.get(Calendar.YEAR);   
            String currentMonth = now.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.ENGLISH);
            List<Integer> yearList = new ArrayList<>();
            yearList.add(2015);

            for(int startYear = 2016; startYear <= currentYear; startYear++){
                yearList.add(startYear);
            }

            model.put("moreThanOneMonthDownloads",moreThanOneMonthDownloads);
            model.put("secondLastMonthCreatedUser", secondLastMonthCreatedUser);
            model.put("lastMonthCreatedUser", lastMonthCreatedUser);
            model.put("totalUser",totalUser);
            model.put("inactiveOrganisationArrayJson", inactiveOrganisationArrayJson);
            model.put("totalOraganisation", totalOraganisation);
            model.put("secondLastMonthActiveOrganisation", secondLastMonthActiveOrganisation);
            model.put("increaseoaganisationPercentage", increaseoaganisationPercentage);
            model.put("moreThanLastMonthOrganisation",moreThanLastMonthOrganisation);
            model.put("lastMonthActiveOrganisation", lastMonthCreatedOrganisation);
            model.put("percentageIncreaseDownloads",percentageIncreaseDownloads);
            model.put("secondLastMonthDownloads",secondLastMonthDownloads);
            model.put("resultInpercentage",resultInpercentage);
            model.put("currentYear", currentYear);
            model.put("currentMonth", currentMonth);
            model.put("yearList", yearList);
            model.put("monthList", monthList);    
            model.put("eduJason", eduJason);
            model.put("cmjson", result);
            model.put("dataset", jsonStringexa);
            model.put("oneMonthDownload", oneMonthDownload);
            model.put("totalDownload", totalDownloads);
            model.put("activeUsersMoreThanOneMonth", activeUsersMoreThanOneMonth);
            model.put("inactiveUserArrayJson", inactiveUserArrayJson);
            model.put("companyProfileArrayJson", companyProfileArrayJson);
            model.put("totalDownloads", totalDownloads);
            model.put("moduleList", modules);
            model.put("countperModule", countperModule);
            model.put("userperModule", userperModule);
            model.put("forumperModule", forumperModule);
            model.put("documentList", documentList);
            return "admin/superadmin/dashboard";
        }
    }

    private static List<Map<String, Object>> getTopCitiesList(List<Object[]> topCities) {

        List<Map<String, Object>> result = new ArrayList<>();
        int count=0;
        
        for(Object[] c:topCities) {
            Map< String, Object> mm= new HashMap<>();
            mm.put(COMPANYID, c[0]);
            mm.put(CITYNAME, c[1]);
            result.add(mm);
            count++;
            if(count>=5){
                break;
            }
        }
        
        return result;
    }

    private static int getPercentageIncreaseDownloads(int oneMonthDownload,
            int secondLastMonthDownloads) {

        int percentageIncreaseDownloads=0;
        if(oneMonthDownload!=0 && secondLastMonthDownloads!=0){
            percentageIncreaseDownloads=(oneMonthDownload-secondLastMonthDownloads)*100/secondLastMonthDownloads;
        }else{
            if(secondLastMonthDownloads==0 && oneMonthDownload==0){
                percentageIncreaseDownloads=0;
            }
            if(oneMonthDownload==0){
                percentageIncreaseDownloads=-secondLastMonthDownloads*100;
            }
            if(secondLastMonthDownloads==0){
                percentageIncreaseDownloads=oneMonthDownload*100;
            }
        }
        
        return percentageIncreaseDownloads;
    }

    private static int getIncreaseoaganisationPercentage(int secondLastMonthActiveOrganisation,int lastMonthCreatedOrganisation) {

        int increaseoaganisationPercentage=0;
        if(secondLastMonthActiveOrganisation!=0 && lastMonthCreatedOrganisation!=0){
            increaseoaganisationPercentage=(lastMonthCreatedOrganisation-secondLastMonthActiveOrganisation)*100/secondLastMonthActiveOrganisation;
        }else{
            if(secondLastMonthActiveOrganisation==0 && lastMonthCreatedOrganisation==0){
                increaseoaganisationPercentage=0;
            }
            if(lastMonthCreatedOrganisation==0){
                increaseoaganisationPercentage=-secondLastMonthActiveOrganisation*100;
            }
            if(secondLastMonthActiveOrganisation==0){
                increaseoaganisationPercentage=lastMonthCreatedOrganisation*100;
            }
        }
        
        return increaseoaganisationPercentage;
    }

    private String getJsonString(int[] inactiveUserArray) {

        ObjectMapper mapper = new ObjectMapper();
        String result=null;
        try {
            result = mapper.writeValueAsString(inactiveUserArray);
        } catch (JsonGenerationException e) {
            LOGGER.info(e);
        } catch (JsonMappingException e) {
            LOGGER.info(e);
        } catch (IOException e) {
            LOGGER.info(e);
        }
        return result;
    }

    private static Map getsurveyList(List<Object[]> recordList) {

        int max=5;
        Map<Integer, int []> map= new HashMap<>();
        for(int i=0;i<5;i++){
            map.put(i+1, new int[header.length]);
        }

        for(Object[] ob:recordList){
            int index=str.indexOf(ob[3].toString());
            map.get(LifeStages.valueOf(ob[2].toString()).getId()-1)[index]=Integer.parseInt(ob[0].toString());
        }

        Map map2=new HashMap<>();
        map2.put("data", map);
        map2.put(HEADERR, str);
        map2.put("maxlenght", max);

        Map<Integer, String> m2= new HashMap<>();
        m2.put(1, "#1BF7F9");
        m2.put(2, "#49ADF7");
        m2.put(3, "#3B93D4");
        m2.put(4, "#0F27A0");
        m2.put(5, "#03177B");
        map2.put(COLOR, m2);

        return map2;
    }

    private int[] getCompanyProfileArray() {

        int [] companyProfileArray= new int[11];
        companyProfileArray[0]=0;
        companyProfileArray[1]=userService.noOfComProfileReport(0, 10,Role.COMPANY_ADMIN);
        companyProfileArray[2] = userService.noOfComProfileReport(11, 20,Role.COMPANY_ADMIN);
        companyProfileArray[3] = userService.noOfComProfileReport(21, 30,Role.COMPANY_ADMIN);
        companyProfileArray[4] = userService.noOfComProfileReport(31, 40,Role.COMPANY_ADMIN);
        companyProfileArray[5] = userService.noOfComProfileReport(41, 50,Role.COMPANY_ADMIN);
        companyProfileArray[6] = userService.noOfComProfileReport(51, 60,Role.COMPANY_ADMIN);
        companyProfileArray[7] = userService.noOfComProfileReport(61, 70,Role.COMPANY_ADMIN);
        companyProfileArray[8] = userService.noOfComProfileReport(71, 80,Role.COMPANY_ADMIN);
        companyProfileArray[9] = userService.noOfComProfileReport(81, 90,Role.COMPANY_ADMIN);
        companyProfileArray[10] = userService.noOfComProfileReport(91, 100,Role.COMPANY_ADMIN);

        return companyProfileArray;
    }

    private Map<String, Object> getModuleArray(List<ModuleMaster> modules) {

        List<Integer> countModule = new ArrayList<>();
        List<Integer> userModule = new ArrayList<>();
        List<Integer> forumModule = new ArrayList<>();
        List<AtmaDocumentMaster> docList = new ArrayList<>();

        Map<String, Object> moduleMap=new HashMap<>();

        for(ModuleMaster module : modules){
            int downloadpermodule = atmadocumentService.totalDownloadsperModule(module.getModuleId());
            int userspermodule = moduleUserService.totalUsersperModule(module.getModuleId());
            int forumspermodule = forumService.totalForumsperModule(module.getModuleId());
            AtmaDocumentMaster document = atmadocumentService.findTopDownload(module.getModuleId());
            countModule.add(downloadpermodule);
            userModule.add(userspermodule);
            forumModule.add(forumspermodule);
            docList.add(document);
        }
        moduleMap.put(COUNTMODULE, countModule);
        moduleMap.put(USERMODULE, userModule);
        moduleMap.put(FORUMMODULE, forumModule);
        moduleMap.put(DOCLIST,docList);

        return moduleMap;
    }

    private List<Map<String, Object>> buildEducationmap(int m, String string, List<Object[]> educationArea, String[] colorArr, List<Map<String, Object>> mapResultTest2) {

        List<Map<String, Object>> mapResultTest = mapResultTest2;
        int k=m;
        int flag=0;
        for(Object[] c:educationArea) {
            String str1=c[1].toString();
            if(str1.equalsIgnoreCase(string)){
                Map<String, Object> map3= new HashMap<>();
                map3.put("y", c[0]);
                map3.put(LABEL, c[1]);
                map3.put(COLOR, colorArr[k]);

                mapResultTest.add(map3);
                flag=1;
                k++;
            }
        }
        if(flag==0){

            Map<String, Object> map3= new HashMap<>();
            map3.put("y", 0);
            map3.put(LABEL, string);
            map3.put(COLOR, colorArr[k]);
            mapResultTest.add(map3);
            k++;
        }
        return mapResultTest;
    }

    @RequestMapping("/AtmaAdmin")
    public String showUniversityDashboard(Map<String, Object> model) throws NoSuchUserException {
        User user;
        try {
            user = (User) SecurityContextHolder.getContext()
                    .getAuthentication().getPrincipal();
        } catch (Exception e) {
            LOGGER.info(e);
            user = null;
        }
        if (user != null && user.getInviteStatus() != InviteStatus.ACTIVATED) {
            user = userService.findById(user.getUserId());
            user.setInviteStatus(InviteStatus.ACTIVATED);
            user.setLastLoginDate(new Date());
            userService.save(user);
        }
        user.setLastLoginDate(new Date());
        userService.save(user);

        Calendar now = Calendar.getInstance();  
        int currentYear = now.get(Calendar.YEAR);   
        List<Integer> yearList = new ArrayList<>();
        yearList.add(2015);
        for(int startYear = 2016; startYear <= currentYear; startYear++){
            yearList.add(startYear);
        }

        List<CompanyMaster> activeInLastMonth=companyService.activeInLastMonth();
        List<CompanyMaster> activeInMoteThanMonth=companyService.activeInMoteThanMonth();

        int totalOrganisations=companyService.gettotalOrganisations();
        int activeLastMonthCompanies=activeInLastMonth.size();
        int activeMoreThanOneMonthCompanies=activeInMoteThanMonth.size();
        int activeTotalUsers=userService.getActiveTotalUsers(Role.COMPANY_ADMIN,Role.MODULE_USER, UserStatus.ACTIVE);
        int oneMonthDownload=atmadocumentService.getLastMonthDownloads();
        int totalDownloads = atmadocumentService.totalSumofDownloads();
        int morethanOneMonthDownload=totalDownloads-oneMonthDownload;

        List<Comments> lastOneWeekComments=commentService.getAllCommentsOfLastMonth();
        List<Comments> oldComments=commentService.getOldComments();

        int noOfLastComments=lastOneWeekComments.size();
        int noOfOldComments=oldComments.size();
        int totalComment=commentService.getTotalComment();

        ObjectMapper mapper= new ObjectMapper();
        List<Object[]> barData=moduleUserService.getAvgOda();
        List<Map<String, Object>> mapResult = new ArrayList<>(); 
        for(Object[] c:barData) {
            Map<String, Object> map3= new HashMap<>();
            map3.put("avg", c[0]);
            map3.put(COUNT, c[1]);
            map3.put(LABLE, c[2]);
            mapResult.add(map3);
        }
        String jsonBar=null;
        try {
            jsonBar = mapper.writeValueAsString(mapResult);
        } catch (JsonGenerationException e) {
            LOGGER.info(e);
        } catch (JsonMappingException e) {
            LOGGER.info(e);
        } catch (IOException e) {
            LOGGER.info(e);
        }

        List<Object[]> pieChartData=atmadocumentService.getDownloadPerModule();
        List<Map<String, Object>> mapResult2 = new ArrayList<>(); 
        for(int k=0;k<pieChartData.size()-1;k++)
        {
            Map<String, Object> map3= new HashMap<>();
            map3.put("sum", pieChartData.get(k)[0]);
            map3.put(MODULE, pieChartData.get(k)[1]);
            mapResult2.add(map3);
        }

        List<Object[]> arrayList = new ArrayList<>();
        for(int i=0; i< pieChartData.size()-1; i++){
            if(i==0){
                Object[] a = new Object[]{HEADERR,COUNT};
                arrayList.add(a);
                arrayList.add(pieChartData.get(i));
            }else{
                arrayList.add(pieChartData.get(i));
            }
        }
        String jsonarrayList=null;
        try {
            jsonarrayList = mapper.writeValueAsString(arrayList);
        } catch (JsonGenerationException e) {
            LOGGER.info(e);
        } catch (JsonMappingException e) {
            LOGGER.info(e);
        } catch (IOException e) {
            LOGGER.info(e);
        }

        int fullProfileComplete=userService.getFullProfileComplete();
        int totalUser=userService.getTotalUser();
        int noOdSurveyComplited=surveyManagerService.getNoOdSurveyComplited();
        int lastComments=commentService.getLastCommentCount();
        int lastDownloadCount=atmadocumentService.getlastDownloadCount();
        int lastAddedUserCount=userService.getLastAddedUserCount();
        int lastAddedOrganisations=companyService.getlastAddedOrganisations();
    //    int lastSevenDayPosts=forumService.getLastSevenDayPosts();
        int lastSevenDayPosts=commentService.getLastSevenDayPosts();
        //   int totalPost=forumService.getTotalPosts();
        int totalPost=commentService.getTotalPosts();
        /*int lastMonthPost=forumService.getlastMonthPost();
        int olderThanOneMonthPost=forumService.getolderThanOneMonthPost();
        */
        int lastMonthPost=commentService.getlastMonthPost();
        int olderThanOneMonthPost=commentService.getolderThanOneMonthPost();
        
        
        int oldUsers=userService.activeUsersMoreThanOneMonth(Role.COMPANY_ADMIN, Role.MODULE_USER);
        int lastMonthCreatedUser=userService.getlastMonthCreatedUser(Role.COMPANY_ADMIN,Role.MODULE_USER);

        model.put("jsonBar", jsonBar);
        model.put("ActiveTotalUsers", activeTotalUsers);
        model.put("lastMonthCreatedUser", lastMonthCreatedUser);
        model.put("oldUsers", oldUsers);
        model.put("totalComment", totalComment);
        model.put("olderThanOneMonthPost",olderThanOneMonthPost);
        model.put("lastMonthPost", lastMonthPost);
        model.put("totalPost", totalPost);
        model.put("lastSevenDayPosts", lastSevenDayPosts);
        model.put("lastAddedOrganisations",lastAddedOrganisations);
        model.put("lastAddedUserCount",lastAddedUserCount);
        model.put("lastDownloadCount",lastDownloadCount);
        model.put("lastComments",lastComments);
        model.put("totalOrganisations",totalOrganisations);
        model.put("noOdSurveyComplited", noOdSurveyComplited);
        model.put("totalUser", totalUser);
        model.put("fullProfileComplete", fullProfileComplete);
        model.put("pieChartDatalist", jsonarrayList);
        model.put("noOfLastComments", noOfLastComments);
        model.put("noOfOldComments", noOfOldComments);
        model.put("totalDownloads",totalDownloads);
        model.put("oneMonthDownload", oneMonthDownload);
        model.put("morethanOneMonthDownload", morethanOneMonthDownload);
        model.put("activeLastMonthCompanies", activeLastMonthCompanies);
        model.put("activeMoreThanOneMonthCompanies", activeMoreThanOneMonthCompanies);
        model.put("yearList", yearList);
        model.put("currentYear", currentYear);

        return "admin/atmaadmin/dashboard";
    }

    @RequestMapping("/CompanyAdmin")
    public String showCompanyDashboard(
            @RequestParam(value = SUCCESS, required = false, defaultValue = "") String success,
            Map<String, Object> model) throws NoSuchUserException {

        if (success != null && !success.isEmpty()) {
            model.put(SUCCESS, success);
        }

        User user;
        try {
            user = (User) SecurityContextHolder.getContext()
                    .getAuthentication().getPrincipal();
        } catch (Exception e) {
            user = null;
            LOGGER.info(e);
        }
        if (user != null && user.getInviteStatus() != InviteStatus.ACTIVATED) {
            user = userService.findById(user.getUserId());
            user.setInviteStatus(InviteStatus.ACTIVATED);
            user.setLastLoginDate(new Date());
            userService.save(user);
        }
        boolean surveyDate;
        user.setLastLoginDate(new Date());
        User user1 = userService.save(user);
        CompanyMaster company = companyService.findByUserId(user1.getUserId());
        try{
            attendanceService.update(company);
        }
        catch(NullPointerException e){
            LOGGER.info(e);
        }

        DateTime date = new DateTime();
        List<ModuleMaster> moduleList = moduleService.findByActive(true);
        SurveyManager surveyManager = surveyManagerService
                .findByCompanyIdandSurveyStatus(company.getCompanyId(),
                        SurveyStatus.PENDING);
        List<ModuleUser> moduleUserList = new ArrayList<>();
        for (ModuleMaster mu : moduleList) {
            ModuleUser moduleUser = moduleUserService
                    .findByCompanyIdAndModuleId(company.getCompanyId(),
                            mu.getModuleId());
            moduleUserList.add(moduleUser);
        }
        if(surveyManager!=null){
            if((date.compareTo(surveyManager.getSurveyPublished()) >= 0) && (date.compareTo(surveyManager.getLastDateToComplete()) < 0)){
                surveyDate = true;
                model.put("surveyDate", surveyDate);
            }
            if((date.compareTo(surveyManager.getLastDateToComplete()) > 0) && surveyManager.getSurveyStatus().equals(SurveyStatus.PENDING)){
                surveyManager.setSurveyStatus(SurveyStatus.INCOMPLETED);
                surveyManager.setOngoingSurvey(false);
                surveyManagerService.save(surveyManager);
            }
        }
        List<ModuleUser> surveyResultList = moduleUserService.findByCompanyCompanyId(company.getCompanyId());
        List<GlobalSponsoredManagement> globalSponsorImageList = globalSponsoredService.findByActiveandImageTypeGlobalSponsor(true, GLOBALSPONSOR);
        GlobalSponsoredManagement atmaContactImage = globalSponsoredService.findByActiveandImageTypeContact(true, CONTACT);
        model.put("user1", user1);
        model.put(COMPANY, company);
        model.put(MODULELIST, moduleList);
        model.put(MODULEUSERLIST, moduleUserList);
        model.put("survey", surveyManager);
        model.put(GLOBALSPONSORIMAGELIST, globalSponsorImageList);
        model.put(ATMACONTACTIMAGE, atmaContactImage);
        model.put("surveyResultList", surveyResultList);
        return "visitor/companyadmin/dashboard";
    }

    @RequestMapping("/ModuleUser")
    public String showStudentDashboard(
            @RequestParam(value = SUCCESS, required = false, defaultValue = "") String success,
            Map<String, Object> model) throws NoSuchUserException {

        if (success != null && !success.isEmpty()) {
            model.put(SUCCESS, success);
        }
        User user;
        try {
            user = (User) SecurityContextHolder.getContext()
                    .getAuthentication().getPrincipal();
        } catch (Exception e) {
            user = null;
            LOGGER.info(e);
        }
        if (user != null) {
            user = userService.findById(user.getUserId());
        }
        user.setLastLoginDate(new Date());
        User user1 = userService.save(user);
        CompanyMaster company = companyService.findByUserId(user.getCreatedBy()
                .getUserId());
        try{
            attendanceService.update(company);
        }
        catch(NullPointerException e){
            LOGGER.info(e);
        }
        List<ModuleUser> moduleUser = moduleUserService.findByUserId(user1
                .getUserId());
        List<ModuleMaster> modulemaster = new ArrayList<>();
        List<Integer> totalProjectsList = new ArrayList<>();
        for (ModuleUser mu : moduleUser) {
            int totalProjects = categoryService.totalActiveCategoriesByModule(mu
                    .getModule().getModuleId());
            totalProjectsList.add(totalProjects);
            ModuleMaster module = moduleService.findByModuleIdandActive(mu
                    .getModule().getModuleId(), true);
            modulemaster.add(module);
        }

        List<GlobalSponsoredManagement> globalSponsorImageList = globalSponsoredService.findByActiveandImageTypeGlobalSponsor(true, GLOBALSPONSOR);
        GlobalSponsoredManagement atmaContactImage = globalSponsoredService.findByActiveandImageTypeContact(true, CONTACT);
        model.put("user", user1);
        model.put("organisation", company);
        model.put("modulemaster", modulemaster);
        model.put(GLOBALSPONSORIMAGELIST, globalSponsorImageList);
        model.put(ATMACONTACTIMAGE, atmaContactImage);
        model.put("moduleUser", moduleUser);
        model.put("totalProjectsList",totalProjectsList);
        return "visitor/moduleuser/moduleUserDashboard";
    }

    @RequestMapping("/OrganisationalDevelopmentArea")
    public String showCategories(@RequestParam(value = "id") Long id,
            @RequestParam(value = ERROR, required = false, defaultValue = "") String error,
            Map<String, Object> model) throws NoSuchUserException {

        if (error != null && !error.isEmpty()) {
            model.put(ERROR, error);
        }

        ModuleMaster module = moduleService.findByModuleId(id);
        List<CategoryMaster> moduleSpecificList = categoryService
                .findByModuleIdandActive(id, true);
        List<ForumMaster> forumList = forumService.findByModuleIdandActive(id,
                true, 0, 3);
        User user = getLogedInUser();
        ModuleUser moduleUser;
        int averageModuleLifeStage;
        if (user.getRole() == Role.MODULE_USER
                && user.getStatus() == UserStatus.ACTIVE) {
            moduleUser = moduleUserService.findByUserIdandModuleId(
                    user.getUserId(), id);
            moduleUser.setModuleStarted(true);
            moduleUserService.update(moduleUser);
            averageModuleLifeStage = surveyResultService.averageModuleResult(moduleUser.getCompany().getCompanyId(), id);
        } else {
            moduleUser = moduleUserService.findByCompanyIdAndModuleId(
                    user.getModuleId(), id);
            averageModuleLifeStage = surveyResultService.averageModuleResult(moduleUser.getCompany().getCompanyId(), id);
        }
        int myModuleLifeStage = 0;
        int myModuleLifeStagePercent = 0;
        if(moduleUser.getLifeStages()!=null){
            myModuleLifeStage = moduleUser.getLifeStages().getId() - 1;
            myModuleLifeStagePercent = 100 * myModuleLifeStage/5;
        }           

        List<NextSteps> nextSteps = nextStepsService
                .findByLifeStageandModuleId(moduleUser.getLifeStages(), id);
        List<ForumTag> tagList = Arrays.asList(ForumTag.values());
        List<GlobalSponsoredManagement> globalSponsorImageList = globalSponsoredService.findByActiveandImageTypeGlobalSponsor(true, GLOBALSPONSOR);
        GlobalSponsoredManagement atmaContactImage = globalSponsoredService.findByActiveandImageTypeContact(true, CONTACT);
        model.put("tagList", tagList);
        model.put(MODULE, module);
        model.put("moduleSpecificList", moduleSpecificList);
        model.put("nextSteps", nextSteps);
        model.put(FORUMLIST, forumList);
        model.put(GLOBALSPONSORIMAGELIST, globalSponsorImageList);
        model.put(ATMACONTACTIMAGE, atmaContactImage);
        model.put("myModuleLifeStage", myModuleLifeStage);
        model.put("myModuleLifeStagePercent", myModuleLifeStagePercent);
        model.put("averageModuleLifeStage", averageModuleLifeStage);
        return "visitor/mngt/visitormngt/module";
    }

    @RequestMapping("/Projects")
    public String showProjectDetails(@RequestParam("id") Long categoryId,
            Map<String, Object> model) throws DuplicateModuleException {

        User user = getLogedInUser();

        CategoryMaster category = categoryService.findByCategoryId(categoryId);
        ModuleMaster module = moduleService.findByModuleId(category.getModule()
                .getModuleId());

        if (user.getRole() == Role.MODULE_USER
                && user.getStatus() == UserStatus.ACTIVE) {
            ModuleUser  moduleUser = moduleUserService.findByUserIdandModuleId(
                    user.getUserId(), module.getModuleId());
            int projectsAccessed = moduleUser.getProjectsAccessed();
            int totalProjects = categoryService.totalActiveCategoriesByModule(module.getModuleId());
            if(projectsAccessed < totalProjects){
                moduleUser.setProjectsAccessed(++projectsAccessed);
                moduleUserService.update(moduleUser);
            }       
        }

        AtmaDocumentMaster listOfBlueprint = atmadocumentService
                .findByModuleandCategoryandSectionandActive(
                        module.getModuleId(), categoryId, BLUEPRINT, true);
        List<AtmaDocumentMaster> listOfToolBox = atmadocumentService
                .findByModuleIdandCategoryIdandSectionandActive(
                        module.getModuleId(), categoryId, TOOLBOX, true);
        List<AtmaDocumentMaster> listOfExamples = atmadocumentService
                .findByModuleIdandCategoryIdandSectionandActive(
                        module.getModuleId(), categoryId, EXAMPLES, true);
        List<AtmaDocumentMaster> listOfReading = atmadocumentService
                .findByModuleIdandCategoryIdandSectionandActive(
                        module.getModuleId(), categoryId, READING, true);
        List<Comments> comments = commentService
                .findByActiveTrueAndCategoryIdandModuleId(categoryId,
                        module.getModuleId(), 0, 3);
        List<GlobalSponsoredManagement> globalSponsorImageList = globalSponsoredService.findByActiveandImageTypeGlobalSponsor(true, GLOBALSPONSOR);
        GlobalSponsoredManagement atmaContactImage = globalSponsoredService.findByActiveandImageTypeContact(true, CONTACT);
        model.put("listOfToolBox", listOfToolBox);
        model.put("listOfExamples", listOfExamples);
        model.put("listOfReading", listOfReading);
        model.put("listOfBlueprint", listOfBlueprint);
        model.put(CATEGORY, category);
        model.put(MODULE, module);
        model.put(GLOBALSPONSORIMAGELIST, globalSponsorImageList);
        model.put(ATMACONTACTIMAGE, atmaContactImage);
        model.put("comments", comments);
        return "visitor/mngt/visitormngt/category";
    }

    @RequestMapping("/Forums/{url1}")
    public String viewForums(@PathVariable("url1") String url1,
            Map<String, Object> model) {

        Long moduleId = Long.parseLong(url1);
        ModuleMaster module = moduleService.findByModuleId(moduleId);
        List<ForumMaster> forumList = forumService.findByModuleandActive(
                moduleId, true);
        List<GlobalSponsoredManagement> globalSponsorImageList = globalSponsoredService.findByActiveandImageTypeGlobalSponsor(true, GLOBALSPONSOR);
        GlobalSponsoredManagement atmaContactImage = globalSponsoredService.findByActiveandImageTypeContact(true, CONTACT);
        model.put(FORUMLIST, forumList);
        model.put(MODULE, module);
        model.put(GLOBALSPONSORIMAGELIST, globalSponsorImageList);
        model.put(ATMACONTACTIMAGE, atmaContactImage);
        return "visitor/mngt/visitormngt/viewforum";
    }

    @RequestMapping("/ForumComments/{url2}")
    public String viewForumComments(@PathVariable("url2") String url2,
            Map<String, Object> model){

        User user = getLogedInUser();
        Long forumId = Long.parseLong(url2);
        ForumMaster forum = forumService.findByForumId(forumId);
        ModuleMaster module = moduleService.findByModuleId(forum.getModule().getModuleId());

        List<Comments> commentList = commentService.findByActiveandForumId(true, forumId);

        List<GlobalSponsoredManagement> globalSponsorImageList = globalSponsoredService.findByActiveandImageTypeGlobalSponsor(true, GLOBALSPONSOR);
        GlobalSponsoredManagement atmaContactImage = globalSponsoredService.findByActiveandImageTypeContact(true, CONTACT);
        if(user.getRole() == Role.ATMA_ADMIN){
            model.put("atmaAdminUser", true);
        }

        model.put("commentList", commentList);
        model.put("forum", forum);
        model.put(GLOBALSPONSORIMAGELIST, globalSponsorImageList);
        model.put(ATMACONTACTIMAGE, atmaContactImage);
        model.put(MODULE, module);
        return "visitor/mngt/visitormngt/forumcomments";
    }

    @RequestMapping("/{url}/Area")
    public String showAreas(
            @RequestParam(value = SUCCESS, required = false, defaultValue = "") String success,
            Map<String, Object> model) {

        List<CityMaster> cityList = new ArrayList<>();
        List<CountryMaster> countryList = countryService.findByActive(true);
        List<AreaMaster> areaList = areaService.findByActive(true);
        List<AreaMaster> inActiveAreaList = areaService.findByActive(false);
        if (success != null && !success.isEmpty()) {
            model.put(SUCCESS, success);
        }

        model.put("inActiveAreaList", inActiveAreaList);
        model.put("areaList", areaList);
        model.put(CITYLIST, cityList);
        model.put(COUNTRYLIST, countryList);
        return "admin/superadmin/masters/location/areagrid";
    }

    @RequestMapping("/{url}/City")
    public String showCities(@RequestParam(value = SUCCESS, required = false, defaultValue = "") String success,
            Map<String, Object> model) {

        if (success != null && !success.isEmpty()) {
            model.put(SUCCESS, success);
        }

        return "admin/superadmin/masters/location/citygrid";
    }

    @RequestMapping("/CityList/{url}")
    @ResponseBody
    public String getCityList(
            @PathVariable("url") String type,
            @RequestParam(value = "sEcho", required = false, defaultValue = "0") Integer sEcho,
            @RequestParam(value = "iDisplayStart", required = false, defaultValue = "0") Integer iDisplayStart,
            @RequestParam(value = "iDisplayLength", required = false, defaultValue = "0") Integer iDisplayLength,
            HttpServletRequest request,
            Map<String, Object> model)  {

        int sortColNo = 0;
        String sortColDir = "";
        String sSearch = null;
        try {
            sortColNo = Integer.parseInt((String) request
                    .getParameter("iSortCol_0"));
            sortColDir = (String) request.getParameter("sSortDir_0");
            sSearch = (String) request.getParameter("sSearch");
            sSearch = sSearch == null ? "" : sSearch;
        } catch (Exception e) {
            sortColNo = 0;
            sortColDir = "asc";
            LOGGER.info(e);
        }
        int pageNo = (iDisplayStart + iDisplayLength) / iDisplayLength;
        int count = (pageNo-1)*iDisplayLength;
        Page<Object[]> cityPage;
        List<Object[]> cityList1;
        List<Object[]> cityList = new ArrayList<>();
        if("Active".equalsIgnoreCase(type)){

            cityPage= cityService.findAllRequiredParaByActive(
                    true, sortColNo, sortColDir, sSearch,
                    pageNo-1, iDisplayLength);
            cityList1 = cityPage.getContent();

            for (Object[] dataArray : cityList1) {
                Object[] dataArray1 = Arrays.copyOf(dataArray, 10);


                String  editDelete = "<div style='position: relative; text-align:center'> "+
                        "<a href='" + propsValue.CONTEXT_URL
                        + "ManageCity/"
                        + "City/EditCity" + "?id="
                        + dataArray1[0] +"'"+ " data-toggle='tooltip' title='Edit' class='btn btn-xs btn-default'><i class='fa fa-pencil'></i> </a>"+
                        "<a href='#delete_area_popup' data-toggle='modal' onclick='deletearea("
                        + dataArray1[0] + ")' title='Delete' class='btn btn-xs btn-danger'><i class='fa fa-times'></i></a>" +               
                        "</div>";


                dataArray1[4] = editDelete;
                if("asc".equals(sortColDir)){
                    count++;
                }
                else{
                    count--;
                }
                dataArray1[0] = count;
                cityList.add(dataArray1);
            }
        }else{
            cityPage= cityService.findAllRequiredParaByActive(
                    false, sortColNo, sortColDir, sSearch,
                    pageNo-1, iDisplayLength);
            cityList1 = cityPage.getContent();
            for (Object[] dataArray : cityList1) {
                Object[] dataArray1 = Arrays.copyOf(dataArray, 10);



                String restore = "<div style='position: relative; text-align:center'> "+
                        "<a href='#restore_Area_popup' data-toggle='modal' "+
                        "       onclick='restoreArea("+dataArray1[0]+")' "+
                        "       title='Restore' class='btn btn-xs btn-success'><i "+
                        "       class='fa fa-plus'></i> </a>"+
                        "</div>";

                dataArray1[4] = restore;
                if("asc".equals(sortColDir)){
                    count++;
                }
                else{
                    count--;
                }
                dataArray1[0] = count;
                cityList.add(dataArray1);
            }
        }
        Long iTotalRecords = (long) cityPage.getTotalElements();
        model.put("draw", pageNo);
        model.put("iTotalRecords", iTotalRecords);
        model.put("iTotalDisplayRecords", iTotalRecords);
        model.put("sEcho", sEcho + "");
        model.put("iDisplayLength", "10");
        model.put("aaData", cityList);
        Gson gson = new GsonBuilder().create();
        return gson.toJson(model);
    }




    @RequestMapping("/*/Country")
    public String showCountries(
            @RequestParam(value = SUCCESS, required = false, defaultValue = "") String success,
            Map<String, Object> model) {

        List<CountryMaster> countryList = countryService.findByActive(true);
        List<CurrencyMaster> currencyList = currencyService.findAllByActive();
        List<CountryMaster> inActiveCountryList = countryService
                .findByActive(false);
        if (success != null && !success.isEmpty()) {
            model.put(SUCCESS, success);
        }
        model.put("inActiveCountryList", inActiveCountryList);
        model.put(COUNTRYLIST, countryList);
        model.put("currencyList", currencyList);
        return "admin/superadmin/masters/location/country";
    }

    @RequestMapping("/*/Currency")
    public String showCurrencies(
            @RequestParam(value = SUCCESS, required = false, defaultValue = "") String success,
            @RequestParam(value = ERROR, required = false, defaultValue = "") String error,
            Map<String, Object> model) {

        if (success != null && !success.isEmpty()) {
            model.put(SUCCESS, success);
        } else if (error != null && !error.isEmpty()) {
            model.put(ERROR, error);
        }
        List<CurrencyMaster> inActivecurrencyList = currencyService
                .findAllByInActive();
        List<CurrencyMaster> currencyList = currencyService.findAllByActive();
        model.put("currencyList", currencyList);
        model.put("inActivecurrencyList", inActivecurrencyList);
        return "admin/superadmin/masters/currency";
    }

    @RequestMapping(value = "/{url1}/{url2}/ActivateMember")
    public String activateMember(
            @PathVariable("url1") String url1,
            @PathVariable("url2") String url2,
            @RequestParam(value = "id", required = false) Long id,
            @RequestParam(value = "memberType", required = false) int memberType,
            Map<String, Object> model) throws NoSuchUserException {

        userService.activateUser(id);
        model.put(SUCCESS, "alert.memberActivated");
        model.put("memberType", memberType);
        return REDIRECT + url1 + "/" + url2;
    }

    @RequestMapping("/{url1}/{url2}/InActivateMember")
    public String inActivateMember(
            @PathVariable("url1") String url1,
            @PathVariable("url2") String url2,
            @RequestParam(value = "id") Long id,
            @RequestParam(value = "memberType", required = false) int memberType,
            Map<String, Object> model)
                    throws NoSuchUserException {

        userService.inActivateUser(id);
        model.put(SUCCESS, "alert.memberInactivated");
        model.put("memberType", memberType);
        return REDIRECT + url1 + "/" + url2;
    }

    @RequestMapping("/*/CompanyList")
    public String showCompany(
            @RequestParam(value = SUCCESS, required = false, defaultValue = "") String success,
            @RequestParam(value = ERROR, required = false, defaultValue = "") String error,
            Map<String, Object> model) {

        if (success != null && !success.isEmpty()) {
            model.put(SUCCESS, success);
        } else if (error != null && !error.isEmpty()) {
            model.put(ERROR, error);
        }
        List<CompanyMaster> companyActiveList = companyService
                .findByActive(true);
        List<CompanyMaster> companyInactiveList = companyService
                .findByActive(false);

        model.put("inactiveCompanyList", companyInactiveList);
        model.put(COMPANYLIST, companyActiveList);
        User user = getLogedInUser();
        model.put("checkApproval", user.isEnquiryAccessRights());
        if (user.getRole().getId() == Role.ATMA_ADMIN.getId()
                && user.isEnquiryAccessRights()) {
            return "admin/mngt/companymngt/companygrid";
        }

        return "admin/mngt/companymngt/companygrid";
    }

    @RequestMapping("/*/ModuleList")
    public String showModule(
            @RequestParam(value = SUCCESS, required = false, defaultValue = "") String success,
            @RequestParam(value = ERROR, required = false, defaultValue = "") String error,
            Map<String, Object> model) {

        if (success != null && !success.isEmpty()) {
            model.put(SUCCESS, success);
        } else if (error != null && !error.isEmpty()) {
            model.put(ERROR, error);
        }
        List<ModuleMaster> moduleActiveList = moduleService.findByActive(true);
        List<ModuleMaster> moduleInactiveList = moduleService
                .findByActive(false);

        for (ModuleMaster m : moduleActiveList) {
            long moduleId = m.getModuleId();
            long categoryCount = categoryService.findByModuleId(moduleId)
                    .size();
            m.setCategoryCount(categoryCount);
        }
        for (ModuleMaster m1 : moduleInactiveList) {
            long moduleId = m1.getModuleId();
            long categoryCount1 = categoryService.findByModuleId(moduleId)
                    .size();
            m1.setCategoryCount(categoryCount1);
        }
        model.put("inactiveModuleList", moduleInactiveList);
        model.put(MODULELIST, moduleActiveList);
        User user = getLogedInUser();
        if (user.getRole().getId() == Role.ATMA_ADMIN.getId()) {
            return "admin/mngt/modulemngt/viewmodule";
        }
        return "admin/mngt/modulemngt/modulegrid";

    }

    @RequestMapping("ManageModule/ListModule")
    @ResponseBody
    public List<ModuleMaster> getModule() {

        List<ModuleMaster> list = moduleService.findByActive(true);
        for (ModuleMaster mu : list) {
            ModuleMaster module = moduleService
                    .findByModuleId(mu.getModuleId());
            List<SurveyBuilder> surveyList = surveyBuilderService.findByStatus(
                    Status.PUBLISH, module.getModuleId());
            if (!surveyList.isEmpty()) {
                mu.setCompleted(true);
            }
        }

        return list;
    }

    @RequestMapping("/*/SponsoredImageList")
    public String showsponsoredimage(
            @RequestParam(value = SUCCESS, required = false, defaultValue = "") String success,
            @RequestParam(value = ERROR, required = false, defaultValue = "") String error,
            Map<String, Object> model) {

        if (success != null && !success.isEmpty()) {
            model.put(SUCCESS, success);
        } else if (error != null && !error.isEmpty()) {
            model.put(ERROR, error);
        }
        List<ModuleMaster> moduleActiveList = moduleService.findByActive(true);
        model.put(MODULELIST, moduleActiveList);

        return "admin/mngt/sponsoredimagemngt/sponsoredimagegrid";

    }

    @RequestMapping("/*/PromotionalImageList")
    public String showpromotionalimage(
            @RequestParam(value = SUCCESS, required = false, defaultValue = "") String success,
            @RequestParam(value = ERROR, required = false, defaultValue = "") String error,
            Map<String, Object> model) {


        if (success != null && !success.isEmpty()) {
            model.put(SUCCESS, success);
        } else if (error != null && !error.isEmpty()) {
            model.put(ERROR, error);
        }

        List<ModuleMaster> moduleActiveList = moduleService.findByActive(true);
        model.put(MODULELIST, moduleActiveList);
        return "admin/mngt/promotionalimagemngt/promotionalimagegrid";
    }

    @RequestMapping("/*/GlobalSponsoredImageList")
    public String showglobalsponsoredimage(
            @RequestParam(value = SUCCESS, required = false, defaultValue = "") String success,
            @RequestParam(value = ERROR, required = false, defaultValue = "") String error,
            Map<String, Object> model) {


        if (success != null && !success.isEmpty()) {
            model.put(SUCCESS, success);
        } else if (error != null && !error.isEmpty()) {
            model.put(ERROR, error);
        }

        List<GlobalSponsoredManagement> globalList = globalSponsoredService
                .findByActive(true);
        model.put("globalList", globalList);
        return "admin/mngt/globalsponsoredmngt/globalsponsoredgrid";
    }

    @RequestMapping("/*/CreateSurvey/{url1}")
    public String createSurvey(
            @PathVariable("url1") String url1,
            @RequestParam(value = SUCCESS, required = false, defaultValue = "") String success,
            @RequestParam(value = ERROR, required = false, defaultValue = "") String error,
            Map<String, Object> model) {

        if (success != null && !success.isEmpty()) {
            model.put(SUCCESS, success);
        } else if (error != null && !error.isEmpty()) {
            model.put(ERROR, error);
        }

        List<ModuleMaster> moduleList = moduleService.findByActive(true);

        for(ModuleMaster mu:moduleList){
            ModuleMaster module = moduleService
                    .findByModuleId(mu.getModuleId());
            List<SurveyBuilder> surveyList = surveyBuilderService.findByStatus(
                    Status.PUBLISH, module.getModuleId());
            if (!surveyList.isEmpty()) {
                mu.setCompleted(true);
            }
        }


        Long moduleId = Long.parseLong(url1);
        ModuleMaster module = moduleService.findByModuleId(moduleId);

        List<SurveyBuilder> list = surveyBuilderService.findByStatus(
                Status.PUBLISH, moduleId);      
        List<SurveyBuilder> list1 = surveyBuilderService
                .findByLifeStagesAndModuleId(LifeStages.LIFE_STAGE1, moduleId);
        List<SurveyBuilder> list2 = surveyBuilderService
                .findByLifeStagesAndModuleId(LifeStages.LIFE_STAGE2, moduleId);
        List<SurveyBuilder> list3 = surveyBuilderService
                .findByLifeStagesAndModuleId(LifeStages.LIFE_STAGE3, moduleId);
        List<SurveyBuilder> list4 = surveyBuilderService
                .findByLifeStagesAndModuleId(LifeStages.LIFE_STAGE4, moduleId);
        List<SurveyBuilder> list5 = surveyBuilderService
                .findByLifeStagesAndModuleId(LifeStages.LIFE_STAGE5, moduleId);

        if (!list.isEmpty()) {
            model.put(LIST1, list1);
            model.put(LIST2, list2);
            model.put(LIST3, list3);
            model.put(LIST4, list4);
            model.put(LIST5, list5);
            model.put("list", list);
            model.put(MODULEID, url1);
            model.put(MODULE, module);
            model.put(MLIST, moduleList);
            return "admin/surveymngt/editsurvey";
        } else if (list1.isEmpty()) {
            model.put(MODULEID, url1);
            model.put(MODULE, module);
            model.put(MLIST, moduleList);
            return "admin/surveymngt/createsurvey";
        } else {
            model.put(LIST1, list1);
            model.put(LIST2, list2);
            model.put(LIST3, list3);
            model.put(LIST4, list4);
            model.put(LIST5, list5);
            model.put(MODULEID, url1);
            model.put(MODULE, module);
            model.put(MLIST, moduleList);
            return "admin/surveymngt/editsurvey";
        }
    }

    @RequestMapping("/ManageSurvey/SurveyInstructions")
    public String viewsurvey(
            @RequestParam(value = SUCCESS, required = false, defaultValue = "") String success,
            @RequestParam(value = ERROR, required = false, defaultValue = "") String error,
            HttpServletRequest request,
            Map<String, Object> model) {

        if (success != null && !success.isEmpty()) {
            model.put(SUCCESS, success);
        } else if (error != null && !error.isEmpty()) {
            model.put(ERROR, error);
        }

        List<ModuleMaster> moduleList = moduleService.findByActive(true);
        model.put(MODULELIST, moduleList);
        User user;
        user = (User) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        CompanyMaster company = companyService.findByUserId(user.getUserId());
        SurveyManager survey = surveyManagerService
                .findByCompanyIdandSurveyStatus(company.getCompanyId(),
                        SurveyStatus.PENDING);
        List<GlobalSponsoredManagement> globalSponsorImageList = globalSponsoredService.findByActiveandImageTypeGlobalSponsor(true, GLOBALSPONSOR);
        HttpSession session = request.getSession(true);
        session.setAttribute("surveyId", survey.getSurveyManagerId());
        model.put(GLOBALSPONSORIMAGELIST, globalSponsorImageList);
        return "visitor/mngt/surveymngt/surveyinstructions";
    }

    @RequestMapping("/*/TakeSurvey/{url1}")
    public String takeSurvey(
            @PathVariable("url1") String url1,
            @RequestParam(value = SUCCESS, required = false, defaultValue = "") String success,
            @RequestParam(value = ERROR, required = false, defaultValue = "") String error,
            HttpServletRequest request,
            Map<String, Object> model) {

        if (success != null && !success.isEmpty()) {
            model.put(SUCCESS, success);
        } else if (error != null && !error.isEmpty()) {
            model.put(ERROR, error);
        }
        User user;
        user = (User) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        Long moduleId = Long.parseLong(url1);
        ModuleMaster module = moduleService.findByModuleId(moduleId);
        List<ModuleMaster> modules = moduleService.findByActive(true);
        long surveyId = (long) request.getSession().getAttribute("surveyId");
        CompanyMaster company = companyService.findByUserId(user.getUserId());
        List<VisitorSurveyBuilder> list1 = visitorSurveyBuilderService
                .findByModuleIdandUserIdandSurveyId(moduleId, user.getUserId(),
                        surveyId);
        List<GlobalSponsoredManagement> globalSponsorImageList = globalSponsoredService.findByActiveandImageTypeGlobalSponsor(true, GLOBALSPONSOR);
        GlobalSponsoredManagement atmaContactImage = globalSponsoredService.findByActiveandImageTypeContact(true, CONTACT);
        List<ModuleUser> moduleUserList = new ArrayList<>();
        for(ModuleMaster mu: modules){
            ModuleUser surveyResult = moduleUserService.findByCompanyIdAndModuleId(company.getCompanyId(), mu.getModuleId());
            moduleUserList.add(surveyResult);
        }

        if (!list1.isEmpty()) {
            List<VisitorSurveyBuilder> sec1 = list1.subList(0, 3);
            List<VisitorSurveyBuilder> sec2 = list1.subList(3, 6);
            List<VisitorSurveyBuilder> sec3 = list1.subList(6, 9);
            List<VisitorSurveyBuilder> sec4 = list1.subList(9, 12);
            List<VisitorSurveyBuilder> sec5 = list1.subList(12, 15);

            model.put(LIST1, sec1);
            model.put(LIST2, sec2);
            model.put(LIST3, sec3);
            model.put(LIST4, sec4);
            model.put(LIST5, sec5);
            model.put(MODULEID, url1);
            model.put(MODULE, module);
            model.put(MODULES, modules);
            model.put(MODULEUSERLIST, moduleUserList);
            model.put(GLOBALSPONSORIMAGELIST, globalSponsorImageList);
            model.put(ATMACONTACTIMAGE, atmaContactImage);

            if (sec1.get(0).getStatus() == Status.COMPLETED) {
                return "visitor/mngt/surveymngt/viewsurvey";
            } else {
                return "visitor/mngt/surveymngt/editsurvey";
            }
        } else {
            List<SurveyBuilder> list6 = surveyBuilderService
                    .findByModuleIdandStatus(moduleId, Status.PUBLISH);

            if (!list6.isEmpty()) {
                List<SurveyBuilder> sec1 = list6.subList(0, 3);
                List<SurveyBuilder> sec2 = list6.subList(3, 6);
                List<SurveyBuilder> sec3 = list6.subList(6, 9);
                List<SurveyBuilder> sec4 = list6.subList(9, 12);
                List<SurveyBuilder> sec5 = list6.subList(12, 15);

                model.put(LIST1, sec1);
                model.put(LIST2, sec2);
                model.put(LIST3, sec3);
                model.put(LIST4, sec4);
                model.put(LIST5, sec5);
            } else {
                model.put(ERROR, "alert.nodataavailableyet");
            }
            model.put(MODULEID, url1);
            model.put(MODULE, module);
            model.put(MODULES, modules);
            model.put(MODULEUSERLIST, moduleUserList);
            model.put(GLOBALSPONSORIMAGELIST, globalSponsorImageList);
            model.put(ATMACONTACTIMAGE, atmaContactImage);
            return "visitor/mngt/surveymngt/takesurvey";
        }

    }

    @RequestMapping("/SkipSurvey")
    public String skipSurvey(
            @RequestParam(value = SUCCESS, required = false, defaultValue = "") String success,
            @RequestParam(value = ERROR, required = false, defaultValue = "") String error,
            @RequestParam(value = "id") Long id, Map<String, Object> model) {

        if (success != null && !success.isEmpty()) {
            model.put(SUCCESS, success);
        } else if (error != null && !error.isEmpty()) {
            model.put(ERROR, error);
        }

        SurveyManager surveyManager = surveyManagerService
                .findBySurveyManagerId(id);
        surveyManager.setSurveyStatus(SurveyStatus.SKIPPED);
        surveyManager.setOngoingSurvey(false);
        SurveyManager sm = surveyManagerService.save(surveyManager);
        CompanyMaster company = companyService.findByCompanyId(surveyManager
                .getCompany().getCompanyId());
        List<ModuleMaster> modules = moduleService.findByActive(true);
        int totalModules = modules.size();
        SurveyManager surveyManager2 = new SurveyManager();
        surveyManager2.setCompany(company);
        surveyManager2.setSurveyStatus(SurveyStatus.PENDING);
        surveyManager2.setTotalModules(totalModules);
        surveyManager2.setTotalModulesCompleted(0);
        surveyManager2.setOngoingSurvey(true);
        surveyManager2.setSurveySkipOption(false);
        SurveyManager surveyManager3 = surveyManagerService
                .save(surveyManager2);
        DateTime date = sm.getSurveyPublished();
        if (date.getMonthOfYear() > 3 && date.getMonthOfYear() <= 6) {
            DateTime dateOne = new DateTime().withMonthOfYear(10)
                    .withDayOfMonth(1).withTimeAtStartOfDay();
            DateTime dateTwo = dateOne.plusMonths(1);
            surveyManager3.setSurveyPublished(dateOne);
            surveyManager3.setLastDateToComplete(dateTwo);
            surveyManagerService.save(surveyManager3);
        } else if (date.getMonthOfYear() > 6 && date.getMonthOfYear() <= 9) {
            DateTime date1 = new DateTime().withMonthOfYear(1)
                    .withDayOfMonth(1).plusYears(1).withTimeAtStartOfDay();
            DateTime date2 = date1.plusMonths(1);
            surveyManager3.setSurveyPublished(date1);
            surveyManager3.setLastDateToComplete(date2);
            surveyManagerService.save(surveyManager3);
        } else if (date.getMonthOfYear() > 9 && date.getMonthOfYear() <= 12) {
            DateTime date11 = new DateTime().withMonthOfYear(4)
                    .withDayOfMonth(1).plusYears(1).withTimeAtStartOfDay();
            DateTime date2 = date11.plusMonths(1);
            surveyManager3.setSurveyPublished(date11);
            surveyManager3.setLastDateToComplete(date2);
            surveyManagerService.save(surveyManager3);
        } else if (date.getMonthOfYear() <= 3) {
            DateTime date1 = new DateTime().withMonthOfYear(7)
                    .withDayOfMonth(1).withTimeAtStartOfDay();
            DateTime date2 = date1.plusMonths(1);
            surveyManager3.setSurveyPublished(date1);
            surveyManager3.setLastDateToComplete(date2);
            surveyManagerService.save(surveyManager3);

        }

        model.put("done", true);
        return "redirect:/CompanyAdmin";
    }

    @RequestMapping("/*/CreateNextSteps/{url1}")
    public String addNextSteps(
            @PathVariable("url1") String url1,
            @RequestParam(value = SUCCESS, required = false, defaultValue = "") String success,
            @RequestParam(value = ERROR, required = false, defaultValue = "") String error,
            Map<String, Object> model) {

        if (success != null && !success.isEmpty()) {
            model.put(SUCCESS, success);
        } else if (error != null && !error.isEmpty()) {
            model.put(ERROR, error);
        }
        List<ModuleMaster> moduleList = moduleService.findByActive(true);
        Long moduleId = Long.parseLong(url1);
        List<NextSteps> nextStepsList1 = nextStepsService
                .findByLifeStageandModuleId(LifeStages.LIFE_STAGE1, moduleId);
        List<NextSteps> nextStepsList2 = nextStepsService
                .findByLifeStageandModuleId(LifeStages.LIFE_STAGE2, moduleId);
        List<NextSteps> nextStepsList3 = nextStepsService
                .findByLifeStageandModuleId(LifeStages.LIFE_STAGE3, moduleId);
        List<NextSteps> nextStepsList4 = nextStepsService
                .findByLifeStageandModuleId(LifeStages.LIFE_STAGE4, moduleId);
        List<NextSteps> nextStepsList5 = nextStepsService
                .findByLifeStageandModuleId(LifeStages.LIFE_STAGE5, moduleId);
        if (nextStepsList1 == null || nextStepsList1.isEmpty()) {
            List<CategoryMaster> categoryList = categoryService
                    .findByModuleIdandActive(moduleId, true);
            ModuleMaster module = moduleService.findByModuleId(moduleId);
            model.put("moList", moduleList);
            model.put(MODULE, module);
            model.put(CATEGORYLIST, categoryList);
            return "admin/mngt/nextstepsmngt/addsteps";
        } else {
            List<CategoryMaster> categoryList = categoryService
                    .findByModuleIdandActive(moduleId, true);
            ModuleMaster module = moduleService.findByModuleId(moduleId);
            model.put(MODULE, module);
            model.put("moList", moduleList);
            model.put(CATEGORYLIST, categoryList);
            model.put("nextStepsList1", nextStepsList1);
            model.put("nextStepsList2", nextStepsList2);
            model.put("nextStepsList3", nextStepsList3);
            model.put("nextStepsList4", nextStepsList4);
            model.put("nextStepsList5", nextStepsList5);
            return "admin/mngt/nextstepsmngt/editsteps";
        }

    }

    @RequestMapping("/*/ListEnquiry")
    public String showEnquiryList(
            @RequestParam(value = SUCCESS, required = false, defaultValue = "") String success,
            @RequestParam(value = ERROR, required = false, defaultValue = "") String error,
            Map<String, Object> model) {

        if (success != null && !success.isEmpty()) {
            model.put(SUCCESS, success);
        } else if (error != null && !error.isEmpty()) {
            model.put(ERROR, error);
        }
        List<EnquiryList> listOfEnquiry = enquiryService
                .findByApproval(CompanyApproval.PENDING);
        List<EnquiryList> listOfRejected = enquiryService
                .findByApproval(CompanyApproval.REJECTED);
        model.put("listOfEnquiry", listOfEnquiry);
        model.put("listOfRejected", listOfRejected);
        return "admin/mngt/enquirymngt/enquirygrid";
    }

    @RequestMapping("/*/CategoryList")
    public String showCategory(
            @RequestParam(value = SUCCESS, required = false, defaultValue = "") String success,
            @RequestParam(value = ERROR, required = false, defaultValue = "") String error,
            Map<String, Object> model) {
        if (success != null && !success.isEmpty()) {
            model.put(SUCCESS, success);
        } else if (error != null && !error.isEmpty()) {
            model.put(ERROR, error);
        }
        List<CategoryMaster> categoryActiveList = categoryService
                .findByActive(true);
        List<CategoryMaster> categoryInactiveList = categoryService
                .findByActive(false);

        model.put("inactiveCategoryList", categoryInactiveList);
        model.put("activecategoryList", categoryActiveList);
        User user = getLogedInUser();
        if (user.getRole().getId() == Role.COMPANY_ADMIN.getId()) {
            return "admin/mngt/categorymngt/viewcategory";
        }
        return "admin/mngt/categorymngt/categorygrid";
    }

    @RequestMapping("/*/ModuleUserList")
    public String showModuleUser(
            @RequestParam(value = SUCCESS, required = false, defaultValue = "") String success,
            @RequestParam(value = ERROR, required = false, defaultValue = "") String error,
            Map<String, Object> model) {

        if (success != null && !success.isEmpty()) {
            model.put(SUCCESS, success);
        } else if (error != null && !error.isEmpty()) {
            model.put(ERROR, error);
        }

        List<User> moduleuserActiveList = userService
                .findByRoleAndStatusAndUser(Role.MODULE_USER,
                        UserStatus.ACTIVE, getLogedInUser().getUserId());
        List<User> moduleuserInactiveList = userService
                .findByRoleAndStatusAndUser(Role.MODULE_USER,
                        UserStatus.INACTIVE, getLogedInUser().getUserId());
        List<ModuleUser> moduleUserList = moduleUserService
                .findByCompanyCompanyId(getLogedInUser().getModuleId());
        model.put("moduleuserActiveList", moduleuserActiveList);
        model.put("moduleuserInactiveList", moduleuserInactiveList);
        model.put("moduleUser", moduleUserList);

        return "admin/mngt/moduleusermngt/moduleusergrid";
    }

    @RequestMapping("/*/DocumentList")
    public String showdocument(
            @RequestParam(value = SUCCESS, required = false, defaultValue = "") String success,
            @RequestParam(value = ERROR, required = false, defaultValue = "") String error,
            Map<String, Object> model) {


        if (success != null && !success.isEmpty()) {
            model.put(SUCCESS, success);
        } else if (error != null && !error.isEmpty()) {
            model.put(ERROR, error);
        }

        List<AtmaDocumentMaster> documentActiveList = atmadocumentService
                .findByActive(true);
        List<AtmaDocumentMaster> documentInactiveList = atmadocumentService
                .findByActive(false);
        model.put("activeDocumentList", documentActiveList);
        model.put("inactiveDocumentList", documentInactiveList);
        return "admin/mngt/documentmngt/documentgrid";
    }

    @RequestMapping("/*/*/AddCompany")
    public String addCompany(
            @RequestParam(value = ERROR, required = false, defaultValue = "") String error,
            Map<String, Object> model) {

        if (error != null && !error.isEmpty()) {
            model.put(ERROR, error);
        }

        List<CountryMaster> countryList = countryService.findByActive(true);
        model.put(COUNTRYLIST, countryList);

        return "admin/mngt/companymngt/addcompany";
    }

    @RequestMapping("/*/*/AddSponsoredImage")
    public String addSponsor(
            @RequestParam(value = "id") Long id,
            @RequestParam(value = ERROR, required = false, defaultValue = "") String error,
            Map<String, Object> model) {

        if (error != null && !error.isEmpty()) {
            model.put(ERROR, error);
        }

        ModuleMaster module = moduleService.findByModuleId(id);
        List<ModuleMaster> moduleList = moduleService.findByActive(true);
        model.put(MODULELIST, moduleList);
        model.put(MODULE, module);
        return "admin/mngt/sponsoredimagemngt/addsponsoredimage";
    }

    @RequestMapping("/*/*/AddPromotionalImage")
    public String addPromoImage(
            @RequestParam(value = "id") Long id,
            @RequestParam(value = ERROR, required = false, defaultValue = "") String error,
            Map<String, Object> model) {

        if (error != null && !error.isEmpty()) {
            model.put(ERROR, error);
        }

        ModuleMaster module = moduleService.findByModuleId(id);
        List<ModuleMaster> moduleList = moduleService.findByActive(true);
        model.put(MODULELIST, moduleList);
        model.put(MODULE, module);

        return "admin/mngt/promotionalimagemngt/addpromotionalimage";
    }

    @RequestMapping("/*/*/AddGlobalSponsoredImage")
    public String addGlobalSponsoredImage(
            @RequestParam(value = ERROR, required = false, defaultValue = "") String error,
            Map<String, Object> model){

        if (error != null && !error.isEmpty()) {
            model.put(ERROR, error);
        }

        return "admin/mngt/globalsponsoredmngt/addglobalsponsoredimage";
    }

    @RequestMapping("/*/*/AddModule")
    public String addModule(
            @RequestParam(value = ERROR, required = false, defaultValue = "") String error,
            Map<String, Object> model) {

        if (error != null && !error.isEmpty()) {
            model.put(ERROR, error);
        }

        List<ModuleMaster> moduleList = moduleService.findByActive(true);
        model.put(MODULELIST, moduleList);

        return "admin/mngt/modulemngt/addmodule";
    }

    @RequestMapping("/*/*/AddCategory")
    public String addCategory(
            @RequestParam(value = ERROR, required = false, defaultValue = "") String error,
            Map<String, Object> model) {

        if (error != null && !error.isEmpty()) {
            model.put(ERROR, error);
        }

        List<CategoryMaster> categoryList = categoryService.findByActive(true);
        List<ModuleMaster> moduleList = moduleService.findByActive(true);
        model.put(CATEGORYLIST, categoryList);
        model.put(MODULELIST, moduleList);

        return "admin/mngt/categorymngt/addcategory";
    }

    @RequestMapping("/*/*/AddDocument")
    public String addDocument(
            @RequestParam(value = ERROR, required = false, defaultValue = "") String error,
            Map<String, Object> model) {

        if (error != null && !error.isEmpty()) {
            model.put(ERROR, error);
        }

        List<String> sectionList = new ArrayList<>();
        sectionList.add(BLUEPRINT);
        sectionList.add(TOOLBOX);
        sectionList.add(EXAMPLES);
        sectionList.add(READING);
        List<ModuleMaster> moduleList = moduleService.findByActive(true);
        List<LifeStages> lifeStageList = new ArrayList<>(
                Arrays.asList(LifeStages.values()));

        model.put(MODULELIST, moduleList);
        model.put("sectionList", sectionList);
        model.put(LIFESTAGELIST, lifeStageList);

        return "admin/mngt/documentmngt/adddocument";
    }

    @RequestMapping("/*/*/EditCompany")
    public String editCompany(
            @RequestParam(value = "id") Long id,
            @RequestParam(value = SUCCESS, required = false, defaultValue = "") String success,
            Map<String, Object> model) {

        CompanyMaster company = companyService.findByCompanyId(id);

        if (success != null && !success.isEmpty()) {
            model.put(SUCCESS, success);
        }
        model.put(COMPANY, company);
        List<CountryMaster> countryList = countryService.findByActive(true);
        model.put(COUNTRYLIST, countryList);

        return "admin/mngt/companymngt/editcompany";
    }

    @RequestMapping("/*/*/EditSponsoredImage")
    public String editSponser(
            @RequestParam(value = "id") Long id,
            @RequestParam(value = SUCCESS, required = false, defaultValue = "") String success,
            Map<String, Object> model) {

        ModuleMaster module = moduleService.findByModuleId(id);

        if (success != null && !success.isEmpty()) {
            model.put(SUCCESS, success);
        }

        model.put(MODULE, module);
        List<ModuleMaster> moduleList = moduleService.findByActive(true);
        model.put(MODULELIST, moduleList);

        return "admin/mngt/sponsoredimagemngt/editsponsoredimage";

    }

    @RequestMapping("/*/*/EditContactImage")
    public String editContactImage(
            @RequestParam(value = "id") Long id,
            @RequestParam(value = SUCCESS, required = false, defaultValue = "") String success,
            Map<String, Object> model){

        if (success != null && !success.isEmpty()) {
            model.put(SUCCESS, success);
        }

        GlobalSponsoredManagement imageManagement = globalSponsoredService.findByImageId(id);
        model.put("imageManagement", imageManagement);
        return "admin/mngt/globalsponsoredmngt/editcontactimage";

    }

    @RequestMapping("/*/*/EditModule")
    public String editModule(
            @RequestParam(value = "id") Long id,
            @RequestParam(value = SUCCESS, required = false, defaultValue = "") String success,
            Map<String, Object> model) {

        ModuleMaster module = moduleService.findByModuleId(id);
        List<CategoryMaster> listofcategories = categoryService
                .findByModuleId(id);

        if (success != null && !success.isEmpty()) {
            model.put(SUCCESS, success);
        }

        model.put(MODULE, module);
        model.put("listofcategories", listofcategories);
        List<ModuleMaster> moduleList = moduleService.findByActive(true);
        model.put(MODULELIST, moduleList);

        return "admin/mngt/modulemngt/editmodule";
    }

    @RequestMapping("/*/*/EditCategory")
    public String editCategory(
            @RequestParam(value = "id") Long id,
            Map<String, Object> model) {

        CategoryMaster category = categoryService.findByCategoryId(id);
        List<CategoryMaster> categoryList = categoryService.findByActive(true);
        List<ModuleMaster> moduleList = moduleService.findByActive(true);

        model.put(CATEGORY, category);
        model.put(CATEGORYLIST, categoryList);
        model.put(MODULELIST, moduleList);

        return "admin/mngt/categorymngt/editcategory";
    }

    @RequestMapping("/*/*/EditDocument")
    public String editDocument(
            @RequestParam(value = "id") Long id,
            HttpServletRequest request,
            Map<String, Object> model) throws NoSuchImageException 
            {

        HttpSession session = request.getSession(true);
        List<ImageMaster> blueprintImages;
        if (session.getAttribute(RESTAURANTIMAGES) != null) {
            blueprintImages = (List<ImageMaster>) session
                    .getAttribute(RESTAURANTIMAGES);
            if (blueprintImages != null && !blueprintImages.isEmpty()) {
                session.removeAttribute(RESTAURANTIMAGES);
                deleteBluePrintImages(blueprintImages);
            }
        }

        AtmaDocumentMaster documentMaster = atmadocumentService
                .findByDocumentId(id);
        List<String> sectionList = new ArrayList<>();
        sectionList.add(BLUEPRINT);
        sectionList.add(TOOLBOX);
        sectionList.add(EXAMPLES);
        sectionList.add(READING);
        List<ModuleMaster> moduleList = moduleService.findByActive(true);
        List<CategoryMaster> categoryList = categoryService
                .findByModuleId(documentMaster.getModule().getModuleId());
        List<LifeStages> lifeStageList = new ArrayList<>(
                Arrays.asList(LifeStages.values()));
        model.put("documentMaster", documentMaster);
        model.put(CATEGORYLIST, categoryList);
        model.put(MODULELIST, moduleList);
        model.put("sectionList", sectionList);
        model.put(LIFESTAGELIST, lifeStageList);

        return "admin/mngt/documentmngt/editdocument";

            }

    private void deleteBluePrintImages(List<ImageMaster> blueprintImages) {

        for (ImageMaster image : blueprintImages) {
            try {
                imageService.delete(image.getImageId());
            } catch (UnsupportedEncodingException e) {
                LOGGER.info(e);
            }
        }
    }

    @RequestMapping("/{url1}/{url2}/DeleteBlueprintImage")
    public String deleteBlueprintImage(@PathVariable("url1") String url1,
            @PathVariable("url2") String url2,
            @RequestParam(value = "blueprintImgId") Long blueprintImgId,
            @RequestParam(value = "docId") Long docId,
            Map<String, Object> model) throws NoSuchImageException 
            {

        try {
            atmadocumentService.deleteBlueprintImage(blueprintImgId);
        } catch (UnsupportedEncodingException e) {
            LOGGER.info(e);

        }
        model.put("id", docId);
        return REDIRECT + url1 + "/" + url2 + "/EditDocument";
            }

    @RequestMapping("/{url1}/{url2}/EditModuleUser")
    public String editState(
            @RequestParam(value = "id") Long id,
            @RequestParam(value = SUCCESS, required = false, defaultValue = "") String success,
            Map<String, Object> model) throws NoSuchUserException {
        User user = userService.findById(id);
        if (success != null && !success.isEmpty()) {
            model.put(SUCCESS, success);
        }
        List<User> moduleUserList = userService.findByStatus(UserStatus.ACTIVE);
        model.put(MODULEUSERLIST, moduleUserList);
        List<ModuleMaster> moduleList = moduleService.findByActive(true);
        model.put(MODULELIST, moduleList);
        model.put("user", user);
        return "admin/mngt/moduleusermngt/editmoduleuser";
    }

    @RequestMapping("/*/*/AddModuleUser")
    public String addModuleUser(
            @RequestParam(value = ERROR, required = false, defaultValue = "") String error,
            Map<String, Object> model) {

        if (error != null && !error.isEmpty()) {
            model.put(ERROR, error);
        }
        List<User> moduleUserList = userService.findByStatus(UserStatus.ACTIVE);
        List<ModuleMaster> moduleList = moduleService.findByActive(true);
        model.put(MODULEUSERLIST, moduleUserList);
        model.put(MODULELIST, moduleList);

        return "admin/mngt/moduleusermngt/addmoduleuser";
    }

    @RequestMapping("/*/*/AssignModule")
    public String assignModule(
            @RequestParam(value = ERROR, required = false, defaultValue = "") String error,
            Map<String, Object> model) {

        if (error != null && !error.isEmpty()) {
            model.put(ERROR, error);
        }
        Long companyId = getLogedInUser().getModuleId();
        List<ModuleMaster> moduleList = moduleService.findByActive(true);
        List<User> userList = userService.findByRoleAndStatusAndUser(
                Role.MODULE_USER, UserStatus.ACTIVE, getLogedInUser()
                .getUserId());
        List<ModuleUser> moduleUserList = moduleUserService
                .findByCompanyCompanyId(companyId);

        for (ModuleMaster mm : moduleList) {
            ModuleUser module1 = new ModuleUser();
            module1.setModule(mm);
            if (!moduleUserList.contains(module1)) {
                moduleUserList.add(module1);
            }

        }

        model.put(MODULEUSERLIST, moduleUserList);
        model.put(USERLIST, userList);
        return "admin/mngt/moduleusermngt/assignmodule";
    }

    @RequestMapping("/*/*/ViewCompany")
    public String viewCompany(
            @RequestParam(value = SUCCESS, required = false, defaultValue = "") String success,
            Map<String, Object> model) {

        if (success != null && !success.isEmpty()) {
            model.put(SUCCESS, success);
        }

        List<CountryMaster> countryList = countryService.findByActive(true);

        model.put(COUNTRYLIST, countryList);

        return "admin/mngt/companymngt/viewcompany";
    }

    @RequestMapping("/*/*/ViewEnquiry")
    public String viewEnquiry(
            @RequestParam(value = "id") Long id,
            @RequestParam(value = ERROR, required = false, defaultValue = "") String error,
            Map<String, Object> model) {

        EnquiryList enquiry = enquiryService.findByEnqiuryId(id);

        if (error != null && !error.isEmpty()) {
            model.put(ERROR, error);
        }

        List<CityMaster> cityList = cityService.findByActive(true);
        List<CountryMaster> countryList = countryService.findByActive(true);
        List<StateMaster> stateList = stateService.findByActive(true);

        model.put(EDUCATIONS, educationList);
        model.put(CITYLIST, cityList);
        model.put(COUNTRYLIST, countryList);
        model.put("stateList", stateList);
        model.put("enquiry", enquiry);

        return "admin/mngt/enquirymngt/viewenquiry";

    }

    @RequestMapping(value = "/{url1}/{url2}/RestoreCompany")
    public String restoreCompany(@PathVariable(value = "url1") String url1,
            @PathVariable(value = "url2") String url2,
            @RequestParam(value = "id") Long id, Map<String, Object> model)
                    throws NoSuchUserException {

        companyService.restore(id);
        model.put(SUCCESS, "alert.companyrestored");

        return REDIRECT + url1 + "/" + url2;
    }

    @RequestMapping(value = "/{url1}/{url2}/RestoreModule")
    public String restoreModule(@PathVariable(value = "url1") String url1,
            @PathVariable(value = "url2") String url2,
            @RequestParam(value = "id") Long id, Map<String, Object> model)
                    throws NoSuchUserException {

        moduleService.restore(id);
        model.put(SUCCESS, "alert.modulerestored");
        return REDIRECT + url1 + "/" + url2;
    }

    @RequestMapping(value = "/{url1}/{url2}/RestoreCategory")
    public String restoreCategory(@PathVariable(value = "url1") String url1,
            @PathVariable(value = "url2") String url2,
            @RequestParam(value = "id") Long id, Map<String, Object> model)
                    throws NoSuchUserException {

        categoryService.restore(id);
        model.put(SUCCESS, "alert.categoryrestored");
        return REDIRECT + url1 + "/" + url2;
    }

    @RequestMapping(value = "/{url1}/{url2}/RestoreDocument")
    public String restoreDocument(@PathVariable("url1") String url1,
            @PathVariable("url2") String url2, @RequestParam("id") Long id,
            Map<String, Object> model) {

        atmadocumentService.restore(id);
        model.put(SUCCESS, "alert.documentrestored");
        return REDIRECT + url1 + "/" + url2;
    }

    @RequestMapping(value = "/{url1}/{url2}/DeleteCompany", method = RequestMethod.POST)
    public String deleteCompany(@PathVariable(value = "url1") String url1,
            @PathVariable(value = "url2") String url2,
            @RequestParam(value = "id") Long id, Map<String, Object> model)
                    throws NoSuchUserException {

        companyService.delete(id);
        model.put(SUCCESS, "alert.companydeleted");
        return REDIRECT + url1 + "/" + url2;
    }

    @RequestMapping(value = "/{url1}/{url2}/ApproveEnquiry", method = RequestMethod.POST)
    public String approveNgo(@PathVariable(value = "url1") String url1,
            @PathVariable(value = "url2") String url2,
            @RequestParam(value = "id") Long id,
            @RequestParam("act") boolean act,
            @RequestParam("remark") String remark,
            Map<String, Object> model)
                    throws DuplicateCompanyException {

        if (act) {
            boolean exceptionFlag = false;

            int profileCompletionRate = 0;
            profileCompletionRate = profileCompletionRate + ProfileWeightage.NGO_NAME.getId();
            profileCompletionRate = profileCompletionRate + ProfileWeightage.SECTOR_WORK.getId();
            profileCompletionRate = profileCompletionRate + ProfileWeightage.NGO_EMAIL.getId();
            profileCompletionRate = profileCompletionRate + ProfileWeightage.COUNTRY.getId();
            profileCompletionRate = profileCompletionRate + ProfileWeightage.STATE.getId();
            profileCompletionRate = profileCompletionRate + ProfileWeightage.CITY.getId();
            profileCompletionRate = profileCompletionRate + ProfileWeightage.PINCODE.getId();
            profileCompletionRate = profileCompletionRate + ProfileWeightage.NGO_WEBSITE.getId();
            profileCompletionRate = profileCompletionRate + ProfileWeightage.ORGANISATION_VISION.getId();
            profileCompletionRate = profileCompletionRate + ProfileWeightage.ORGANISATION_MISSION.getId();
            profileCompletionRate = profileCompletionRate + ProfileWeightage.ORGANISATION_DESCRIPTION.getId();
            profileCompletionRate = profileCompletionRate + ProfileWeightage.LEGAL_TYPE.getId();
            profileCompletionRate = profileCompletionRate + ProfileWeightage.ASSISTANCE_FROM_OTHER_AGENCY.getId();
            profileCompletionRate = profileCompletionRate + ProfileWeightage.FCRA_REGISTRATION.getId();
            profileCompletionRate = profileCompletionRate + ProfileWeightage.FIRST_NAME.getId();
            profileCompletionRate = profileCompletionRate + ProfileWeightage.LAST_NAME.getId();
            profileCompletionRate = profileCompletionRate + ProfileWeightage.MOBILE.getId();
            profileCompletionRate = profileCompletionRate + ProfileWeightage.EMAIL_ID.getId();

            int totalCompletionRate = profileCompletionRate;

            totalCompletionRate = totalCompletionRate + ProfileWeightage.COMPANY_ADDRESSLINE1.getId();
            totalCompletionRate = totalCompletionRate + ProfileWeightage.COMPANY_ADDRESSLINE2.getId();
            totalCompletionRate = totalCompletionRate + ProfileWeightage.COMPANY_ADDRESSLINE3.getId();
            totalCompletionRate = totalCompletionRate + ProfileWeightage.TYPE_OF_EDUCATION.getId();
            totalCompletionRate = totalCompletionRate + ProfileWeightage.TELEPHONE.getId();
            totalCompletionRate = totalCompletionRate + ProfileWeightage.AGE_STUDENTS.getId();
            totalCompletionRate = totalCompletionRate + ProfileWeightage.AREA_OF_OPERATION.getId();
            totalCompletionRate = totalCompletionRate + ProfileWeightage.AUDIT1.getId();
            totalCompletionRate = totalCompletionRate + ProfileWeightage.AUDIT2.getId();
            totalCompletionRate = totalCompletionRate + ProfileWeightage.BUDGET_SIZE.getId();
            totalCompletionRate = totalCompletionRate + ProfileWeightage.DESIGNATION.getId();
            totalCompletionRate = totalCompletionRate + ProfileWeightage.EDUCATIONAL_BACKGROUND.getId();
            totalCompletionRate = totalCompletionRate + ProfileWeightage.EIGHTY_G_FILE.getId();
            totalCompletionRate = totalCompletionRate + ProfileWeightage.EIGHTY_G_NUMBER.getId();
            totalCompletionRate = totalCompletionRate + ProfileWeightage.EXISTING_FUNDERS.getId();
            totalCompletionRate = totalCompletionRate + ProfileWeightage.FCRA_FILE.getId();
            totalCompletionRate = totalCompletionRate + ProfileWeightage.FCRA_REG_NUMBER.getId();
            totalCompletionRate = totalCompletionRate + ProfileWeightage.FUNCTIONAL_AREAS.getId();
            totalCompletionRate = totalCompletionRate + ProfileWeightage.GENDER.getId();
            totalCompletionRate = totalCompletionRate + ProfileWeightage.LINKEDIN.getId();
            totalCompletionRate = totalCompletionRate + ProfileWeightage.MAIN_SOURCE_FUNDING.getId();
            totalCompletionRate = totalCompletionRate + ProfileWeightage.NETWORK_KNOWLEDGE.getId();
            totalCompletionRate = totalCompletionRate + ProfileWeightage.NO_OF_BOYS.getId();
            totalCompletionRate = totalCompletionRate + ProfileWeightage.NO_OF_CHILDREN.getId();
            totalCompletionRate = totalCompletionRate + ProfileWeightage.NO_OF_EMPLOYEES.getId();
            totalCompletionRate = totalCompletionRate + ProfileWeightage.NO_OF_GIRLS.getId();
            totalCompletionRate = totalCompletionRate + ProfileWeightage.NO_OF_TRUSTEES.getId();
            totalCompletionRate = totalCompletionRate + ProfileWeightage.PAN_FILE.getId();
            totalCompletionRate = totalCompletionRate + ProfileWeightage.PAN_NUMBER.getId();
            totalCompletionRate = totalCompletionRate + ProfileWeightage.REGISTRATION_NUMBER.getId();
            totalCompletionRate = totalCompletionRate + ProfileWeightage.TRUSTEES.getId();
            totalCompletionRate = totalCompletionRate + ProfileWeightage.TWELVE_A_CERTIFICATE.getId();
            totalCompletionRate = totalCompletionRate + ProfileWeightage.TWELVE_A_FILE.getId();
            totalCompletionRate = totalCompletionRate + ProfileWeightage.WORK_EXPERIENCE.getId();

            EnquiryList enquiry = enquiryService.findByEnqiuryId(id);

            if(!enquiry.getNgoPhone().isEmpty()){
                profileCompletionRate = profileCompletionRate + ProfileWeightage.TELEPHONE.getId();
            }
            if(!enquiry.getNgoAddress1().isEmpty()){
                profileCompletionRate = profileCompletionRate + ProfileWeightage.COMPANY_ADDRESSLINE1.getId();
            }
            if(!enquiry.getNgoAddress2().isEmpty()){
                profileCompletionRate = profileCompletionRate + ProfileWeightage.COMPANY_ADDRESSLINE2.getId();
            }
            if(!enquiry.getNgoAddress3().isEmpty()){
                profileCompletionRate = profileCompletionRate + ProfileWeightage.COMPANY_ADDRESSLINE3.getId();
            }
            if(!enquiry.getTypeOfEducation().isEmpty()){
                profileCompletionRate = profileCompletionRate + ProfileWeightage.TYPE_OF_EDUCATION.getId();
            }

            int profileCompletionPercent = 100 * profileCompletionRate/totalCompletionRate;
            boolean flag=true;

            CompanyMaster company = new CompanyMaster();
            company.setCompanyName(enquiry.getNgoName());
            company.setFlag(flag);
            company.setSectorWork(enquiry.getSectorWork());
            company.setCompanyAddressLine1(enquiry.getNgoAddress1());
            company.setCompanyAddressLine2(enquiry.getNgoAddress2());
            company.setCompanyAddressLine3(enquiry.getNgoAddress3());
            company.setCompanyEmail(enquiry.getNgoEmail());
            company.setCompanyPincode(enquiry.getNgoPincode());
            company.setCompanyTelephone1(enquiry.getNgoPhone());
            company.setCompanyWebsite(enquiry.getNgoWebsite());
            company.setOrganisationVision(enquiry.getOrganisationVision());
            company.setOrganisationMission(enquiry.getOrganisationMission());
            company.setOrganisationDescription(enquiry
                    .getOrganisationDescription());
            company.setLegalType(enquiry.getLegalType());
            company.setAssitanceFromOtherAgency(enquiry
                    .isAssitanceFromOtherAgency());
            company.setCountry(enquiry.getCountry());
            company.setState(enquiry.getState());
            company.setCity(enquiry.getCity());
            company.setFcraRegistarion(enquiry.isFcraRegistarion());
            company.setTypeOfEducation(enquiry.getTypeOfEducation());
            company.setRemark(remark);
            String password = RandomCodeGenerator.randomString(6);
            User admin = new User(enquiry.getFirstName(),
                    enquiry.getLastName(), true, Role.COMPANY_ADMIN.getId(),
                    enquiry.getEmailId(), enquiry.getMobile(), null, password);

            String loginId = enquiry.getEmailId();
            admin.setLoginId(loginId);
            admin.setDesignation("Company Admin");
            admin.setInviteStatus(InviteStatus.PENDING);
            admin.setProfileCompletion(profileCompletionPercent);
            admin.setTotalProfileCompletion(totalCompletionRate);
            admin.setProfileCompletionRate(profileCompletionRate);

            try {
                CompanyMaster company1=saveCompany(company,admin);

                List<ModuleMaster> modules = moduleService
                        .findByActive(true);
                saveModuleUser(modules,company1);
                userService.sendRegstrationMail(admin,
                        company1.getCompanyName(), null, password);
                enquiryService.approve(id);
            } catch (DuplicateEmailException e) {
                exceptionFlag = true;
                model.put(ERROR, e.getErrorCode());
                LOGGER.info(e);
            } catch (DuplicateCompanyException e) {
                exceptionFlag = true;
                model.put(ERROR, e.getErrorCode());
                LOGGER.info(e);
            }

            if (!exceptionFlag) {
                model.put(SUCCESS, "alert.companysaved");
                return REDIRECT + url1 + "/" + url2;
            } else {
                model.put("user", admin);
                model.put("enquiry", enquiry);
                model.put(COMPANY, company);
                return "admin/mngt/enquirymngt/viewenquiry";
            }
        } else {
            EnquiryList enquiry = enquiryService.reject(id, remark);
            enquiryService.sendRejectionMail(enquiry);
            model.put(SUCCESS, "alert.companyrejected");
            return REDIRECT + url1 + "/" + url2;
        }
    }

    private void saveModuleUser(List<ModuleMaster> modules, CompanyMaster company1) {
        for (ModuleMaster mu : modules) {
            ModuleUser moduleUser = new ModuleUser();
            ModuleMaster module = moduleService.findByModuleId(mu
                    .getModuleId());
            moduleUser.setModule(module);
            moduleUser.setCompany(company1);
            moduleUserService.save(moduleUser);
        }

    }

    private CompanyMaster saveCompany(CompanyMaster company, User admin) {
        CompanyMaster company1 = null;
        try {
            company1 = companyService
                    .save(company, admin);
        } catch (SystemException e) {
            LOGGER.info(e);
        } catch (BusinessException e) {
            LOGGER.info(e);
        } catch (IOException e) {
            LOGGER.info(e);
        }
        return company1;
    }

    @RequestMapping(value = "/{url1}/{url2}/DeleteSponsoredImage", method = RequestMethod.POST)
    public String deleteSponsored(@PathVariable(value = "url1") String url1,
            @PathVariable(value = "url2") String url2,
            @RequestParam(value = "id") Long id, Map<String, Object> model)
                    throws NoSuchUserException {
        try {
            moduleService.deleteSponsoredImage(id);
        } catch (NoSuchImageException e) {
            LOGGER.info(e);
        } catch (UnsupportedEncodingException e) {
            LOGGER.info(e);
        }
        model.put(SUCCESS, "alert.sponsoreddeleted");
        return REDIRECT + url1 + "/" + url2;
    }

    @RequestMapping(value = "/{url1}/{url2}/DeletePromotionalImage", method = RequestMethod.POST)
    public String deletePromo(@PathVariable(value = "url1") String url1,
            @PathVariable(value = "url2") String url2,
            @RequestParam(value = "id") Long id, Map<String, Object> model)
                    throws NoSuchUserException {
        try {
            moduleService.deletePromoImage(id);
        } catch (NoSuchImageException e) {
            LOGGER.info(e);
        } catch (UnsupportedEncodingException e) {
            LOGGER.info(e);
        }

        model.put(SUCCESS, "alert.promodeleted");

        return REDIRECT + url1 + "/" + url2;
    }

    @RequestMapping(value = "/{url1}/{url2}/DeleteGlobalSponsoredImage", method = RequestMethod.POST)
    public String deleteGlobalSponsor(@PathVariable(value = "url1") String url1,
            @PathVariable(value = "url2") String url2,
            @RequestParam(value = "id") Long id, Map<String, Object> model) throws NoSuchImageException{

        try {
            globalSponsoredService.deleteImage(id);
        } catch (UnsupportedEncodingException e) {
            LOGGER.info(e);
        }

        model.put(SUCCESS,"alert.imagedeletedsuccessfully");

        return REDIRECT + url1 + "/" + url2;
    }

    @RequestMapping(value = "/{url1}/{url2}/DeleteModule", method = RequestMethod.POST)
    public String deleteModule(@PathVariable(value = "url1") String url1,
            @PathVariable(value = "url2") String url2,
            @RequestParam(value = "id") Long id, Map<String, Object> model)
                    throws NoSuchUserException {

        moduleService.delete(id);
        model.put(SUCCESS, "alert.moduledeleted");

        return REDIRECT + url1 + "/" + url2;
    }

    @RequestMapping(value = "/{url1}/{url2}/DeleteCategory", method = RequestMethod.POST)
    public String deleteCategory(@PathVariable(value = "url1") String url1,
            @PathVariable(value = "url2") String url2,
            @RequestParam(value = "id") Long id, Map<String, Object> model)
                    throws NoSuchUserException {

        categoryService.delete(id);
        model.put(SUCCESS, "alert.categorydeleted");

        return REDIRECT + url1 + "/" + url2;
    }

    @RequestMapping(value = "/{url1}/{url2}/DeleteDocument", method = RequestMethod.POST)
    public String deleteDocument(@PathVariable("url1") String url1,
            @PathVariable("url2") String url2,
            @RequestParam(value = "id") Long id, Map<String, Object> model) {

        atmadocumentService.delete(id);
        model.put(SUCCESS, "alert.documentdeleted");

        return REDIRECT + url1 + "/" + url2;
    }

    @RequestMapping("/*/*/AddAdmin")
    public String addAdmin(
            @RequestParam(value = ERROR, required = false, defaultValue = "") String error,
            Map<String, Object> model) {

        if (error != null && !error.isEmpty()) {
            model.put(ERROR, error);
        }

        List<User> adminList = userService.findByRoleAndStatus(Role.ATMA_ADMIN,
                UserStatus.ACTIVE);
        model.put("adminList", adminList);

        return "admin/mngt/atmaadminmngt/addadmin";
    }

    @RequestMapping("/*/*/EditAdmin")
    public String editAdmin(
            @RequestParam(value = "id") Long id,
            @RequestParam(value = SUCCESS, required = false, defaultValue = "") String success,
            Map<String, Object> model) throws NoSuchUserException {

        User user = userService.findById(id);

        if (success != null && !success.isEmpty()) {
            model.put(SUCCESS, success);
        }

        model.put("user", user);

        return "admin/mngt/atmaadminmngt/editadmin";
    }

    @RequestMapping("/*/AdminList")
    public String showAdmin(
            @RequestParam(value = SUCCESS, required = false, defaultValue = "") String success,
            @RequestParam(value = ERROR, required = false, defaultValue = "") String error,
            Map<String, Object> model) {

        if (success != null && !success.isEmpty()) {
            model.put(SUCCESS, success);
        } else if (error != null && !error.isEmpty()) {
            model.put(ERROR, error);
        }

        List<User> adminActiveList = userService.findByRoleAndStatus(
                Role.ATMA_ADMIN, UserStatus.ACTIVE);
        List<User> adminInactiveList = userService.findByRoleAndStatus(
                Role.ATMA_ADMIN, UserStatus.INACTIVE);

        model.put("inactiveadminList", adminInactiveList);
        model.put("adminList", adminActiveList);

        return "admin/mngt/atmaadminmngt/admingrid";
    }



    @PreAuthorize("hasRole('SUPER_ADMIN')")
    @RequestMapping("/*/OrganizationList")
    public String showOrganization(
            @RequestParam(value = SUCCESS, required = false, defaultValue = "") String success,
            @RequestParam(value = ERROR, required = false, defaultValue = "") String error,
            Map<String, Object> model) {

        if (success != null && !success.isEmpty()) {
            model.put(SUCCESS, success);
        } else if (error != null && !error.isEmpty()) {
            model.put(ERROR, error);
        }

        List<CompanySurveyResetManager> orgActivatedList = companyService.findByActiveWithSurvey(true);
        List<CompanySurveyResetManager> orgDactivatedList = companyService.findByActiveWithSurvey(false);

        model.put("orgActivatedList", orgActivatedList);
        model.put("orgDactivatedList", orgDactivatedList);

        return "admin/mngt/atmaadminmngt/organization/orggrid";
    }

    @RequestMapping(value = "/{url1}/{url2}/DeleteAdmin", method = RequestMethod.POST)
    public String deleteAdmin(@PathVariable(value = "url1") String url1,
            @PathVariable(value = "url2") String url2,
            @RequestParam(value = "id") Long id, Map<String, Object> model)
                    throws NoSuchUserException {
        userService.inActivateUser(id);
        model.put(SUCCESS, "alert.admindeleted");
        return REDIRECT + url1 + "/" + url2;
    }

    @RequestMapping(value = "/{url1}/{url2}/RestoreAdmin")
    public String restoreAdmin(@PathVariable(value = "url1") String url1,
            @PathVariable(value = "url2") String url2,
            @RequestParam(value = "id") Long id, Map<String, Object> model)
                    throws NoSuchUserException {
        userService.activateUser(id);
        model.put(SUCCESS, "alert.adminrestored");
        return REDIRECT + url1 + "/" + url2;
    }

    @RequestMapping(value = "/{url1}/{url2}/DeleteModuleUser", method = RequestMethod.POST)
    public String deleteModuleUser(@PathVariable(value = "url1") String url1,
            @PathVariable(value = "url2") String url2,
            @RequestParam(value = "id") Long id, Map<String, Object> model)
                    throws NoSuchUserException {
        userService.inActivateUser(id);
        model.put(SUCCESS, "alert.moduleuserdeleted");
        return REDIRECT + url1 + "/" + url2;
    }

    @RequestMapping(value = "/{url1}/{url2}/RestoreModuleUser")
    public String restoreModuleUser(@PathVariable(value = "url1") String url1,
            @PathVariable(value = "url2") String url2,
            @RequestParam(value = "id") Long id,Map<String, Object> model)
                    throws NoSuchUserException {
        userService.activateUser(id);
        model.put(SUCCESS, "alert.moduleuserrestored");
        return REDIRECT + url1 + "/" + url2;
    }


    @RequestMapping(value = "/EditPersonalDetails/{url1}")
    public String editPersonalDetails(
            @RequestParam(value = SUCCESS, required = false, defaultValue = "") String success,
            @RequestParam(value = ERROR, required = false, defaultValue = "") String error,
            @PathVariable(value = "url1") String url1,
            Map<String, Object> model) throws NoSuchUserException {

        if (success != null && !success.isEmpty()) {
            model.put(SUCCESS, success);
        } else if (error != null && !error.isEmpty()) {
            model.put(ERROR, error);
        }
        Long idUser = Long.parseLong(url1);
        User users = userService.findById(idUser);
        User user1 = getLogedInUser();
        if (users != null) {
            if (idUser != user1.getUserId()) {
                model.put(ERROR, ALERT_ACCESS_DENIED);
                return REDIRECT_TO_LOGIN;
            } else {
                PersonalInfo personalInfo = personalInfoService.findById(users
                        .getUserId());

                if (personalInfo != null) {
                    String funcAreaList = personalInfo.getFunctionalAreas();
                    model=checkFunctionList(model,funcAreaList);
                    
                    model.put("personalInfo", personalInfo);
                }           

                List<GlobalSponsoredManagement> globalSponsorImageList = globalSponsoredService.findByActiveandImageTypeGlobalSponsor(true, GLOBALSPONSOR);

                List<String> educationalBackground = new ArrayList<>();
                educationalBackground.add("10");
                educationalBackground.add("10+2");
                educationalBackground.add("Diploma");
                educationalBackground.add("Bachelors");
                educationalBackground.add("Masters");
                educationalBackground.add("Ph.D");
                List<String> experienceList = new ArrayList<>();
                experienceList.add("0-2 years");
                experienceList.add("2-4 years");
                experienceList.add("4-6 years");
                experienceList.add("6-8 years");
                experienceList.add("8-10 years");
                experienceList.add("10+ years");
                List<String> functionalAreas = new ArrayList<>();
                functionalAreas.add(ADMINISTRATION);
                functionalAreas.add("Fund Raising");
                functionalAreas.add(HUMANRESOURCES);
                functionalAreas.add(STRATEGY);
                functionalAreas.add(LEADERSHIP);
                functionalAreas.add(MARKETING);
                functionalAreas.add("Operations");
                functionalAreas.add("IT");
                functionalAreas.add("Other");
                List<String> aboutUs = new ArrayList<>();
                aboutUs.add("Google Search");
                aboutUs.add("Social Media");
                aboutUs.add("NGO");
                aboutUs.add("Colleague");
                aboutUs.add("Other");
                model.put("experienceList", experienceList);
                model.put("educationalList", educationalBackground);
                model.put("functionalAreasList", functionalAreas);
                model.put("aboutUsList", aboutUs);
                model.put("user", users);
                model.put(GLOBALSPONSORIMAGELIST, globalSponsorImageList);
                if(users.getRole() == Role.COMPANY_ADMIN){
                    return "visitor/mngt/profilemngt/personaldetails";
                }
                else{
                    return "visitor/mngt/profilemngt/moduleUserprofile";
                }

            }
        } else {
            return REDIRECT_TO_LOGIN;
        }

    }

    private static Map<String, Object> checkFunctionList(Map<String, Object> model,
            String funcAreaList) {
        if(funcAreaList!= null){
            String[] items = funcAreaList.split(",");
            List<String> funcAreas = Arrays.asList(items);
            model.put("funcAreaList", funcAreas);
        }
        return model;
    }

    @RequestMapping(value = "/EditOrganisationalDetailsStep1/{url1}")
    public String editOrganisationalDetailsStep1(
            @RequestParam(value = SUCCESS, required = false, defaultValue = "") String success,
            @RequestParam(value = ERROR, required = false, defaultValue = "") String error,
            @PathVariable(value = "url1") String url1,
            Map<String, Object> model) throws NoSuchUserException {

        if (success != null && !success.isEmpty()) {
            model.put(SUCCESS, success);
        } else if (error != null && !error.isEmpty()) {
            model.put(ERROR, error);
        }
        Long userId = Long.parseLong(url1);
        User user = userService.findById(userId);
        User userOne = getLogedInUser();
        if (user != null) {
            if (userId != userOne.getUserId()) {
                model.put(ERROR, ALERT_ACCESS_DENIED);
                return REDIRECT_TO_LOGIN;
            } else {
                CompanyMaster company;
                if (user.getRole() == Role.COMPANY_ADMIN) {
                    company = companyService.findByUserId(user.getUserId());
                } else if (user.getRole() == Role.MODULE_USER) {
                    company = companyService.findByUserId(user.getCreatedBy()
                            .getUserId());
                } else {
                    return REDIRECT_TO_LOGIN;
                }

                return processEditOrganisationalDetailsStep1(model, user, company);
            }
        } else {
            return REDIRECT_TO_LOGIN;
        }

    }


    @PreAuthorize("hasRole('SUPER_ADMIN')")
    @RequestMapping(value = "/EditOrganisationalDetailsByAdmin/step/{step}/company/{companyId}")
    public String editOrganisationalDetailsStep1BySuperAdmin(
            @RequestParam(value = SUCCESS, required = false, defaultValue = "") String success,
            @RequestParam(value = ERROR, required = false, defaultValue = "") String error,
            @PathVariable(value = "step") Integer step,
            @PathVariable(value =COMPANYID) Long companyId,
            Map<String, Object> model) throws NoSuchUserException {

        if (success != null && !success.isEmpty()) {
            model.put(SUCCESS, success);
        } else if (error != null && !error.isEmpty()) {
            model.put(ERROR, error);
        }

        User admin = getLogedInUser();
        if (admin != null) {

            CompanyMaster company = companyService.findByCompanyId(companyId);
            User companyAdmin = company.getAdmin();
            model.put("adminEdit",true);

            switch (step){
            case 1: return processEditOrganisationalDetailsStep1(model, companyAdmin, company);
            case 2: return processEditOrganisationalDetailsStep2(model, companyAdmin, company);
            case 3: return processEditOrganisationalDetailsStep3(model, companyAdmin, company);

            default: throw new IllegalArgumentException("Step process not relevant : " + step);
            }

        } else {
            return REDIRECT_TO_LOGIN;
        }

    }

    private String processEditOrganisationalDetailsStep1(Map<String, Object> model, User user, CompanyMaster company) {

        List<GlobalSponsoredManagement> globalSponsorImageList = globalSponsoredService.findByActiveandImageTypeGlobalSponsor(true, GLOBALSPONSOR);
        List<Object[]> educationList1=educationSectorService.getAllSector();
        List<Map<String, Object>> result3 = new ArrayList<>(); 

        for(Object[] c:educationList1){ 
            Map< String, Object> mmm= new HashMap<>();
            mmm.put("id", c[0]);
            mmm.put("educationtype", c[1]);
            result3.add(mmm);
        }

        List<CountryMaster> countryList = countryService
                .findByActive(true);
        List<StateMaster> stateList = stateService.findByActive(true);
        List<CityMaster> cityList = cityService.findByActive(true);

        model.put("result3", result3);
        model.put(COUNTRYLIST, countryList);
        model.put("stateList", stateList);
        model.put(CITYLIST, cityList);
        model.put(EDUCATIONS, educationList);
        model.put(COMPANY, company);
        model.put("user", user);
        model.put(GLOBALSPONSORIMAGELIST, globalSponsorImageList);
        return "visitor/mngt/profilemngt/orgdetailsStep1";
    }

    @RequestMapping(value = "/EditOrganisationalDetailsStep2/{url1}")
    public String editOrganisationalDetailsStep2(
            @RequestParam(value = SUCCESS, required = false, defaultValue = "") String success,
            @RequestParam(value = ERROR, required = false, defaultValue = "") String error,
            @PathVariable(value = "url1") String url1,
            Map<String, Object> model){


        if (success != null && !success.isEmpty()) {
            model.put(SUCCESS, success);
        } else if (error != null && !error.isEmpty()) {
            model.put(ERROR, error);
        }
        boolean exceptionFlag = false;
        Long userIds = Long.parseLong(url1);
        User users = null;
        try{
            users = userService.findById(userIds);
        }catch(NoSuchUserException e){
            exceptionFlag = true;
            LOGGER.info(e);
        }
        if(!exceptionFlag){
            CompanyMaster company;
            if (users.getRole() == Role.COMPANY_ADMIN) {
                company = companyService.findByUserId(users.getUserId());
            } else if (users.getRole() == Role.MODULE_USER) {
                company = companyService.findByUserId(users.getCreatedBy()
                        .getUserId());
            } else {
                return REDIRECT_TO_LOGIN;
            }

            return processEditOrganisationalDetailsStep2(model, users, company);
        }
        else{
            model.put(ERROR, ALERT_ACCESS_DENIED);
            return REDIRECT_TO_LOGIN;
        }   
    }

    private String processEditOrganisationalDetailsStep2(Map<String, Object> model, User user, CompanyMaster company) {

        String areaPrograms = company.getAreaOfOperations();
        if(areaPrograms!= null){
            String[] items = areaPrograms.split(",");
            List<String> areaofPrograms = Arrays.asList(items);
            model.put("areaPrograms", areaofPrograms);
        }

        List<GlobalSponsoredManagement> globalSponsorImageList = globalSponsoredService.findByActiveandImageTypeGlobalSponsor(true, GLOBALSPONSOR);

        List<String> areasOfOperations = new ArrayList<>();
        areasOfOperations.add("Citizenship");
        areasOfOperations.add("Gender Equality");
        areasOfOperations.add("Health");
        areasOfOperations.add("Human Rights");
        areasOfOperations.add("Environment");
        areasOfOperations.add("Disability");
        areasOfOperations.add("Skills Development");
        areasOfOperations.add("Education");
        areasOfOperations.add("Elderly Welfare");
        areasOfOperations.add("Child Welfare");
        areasOfOperations.add("Animal Welfare");
        areasOfOperations.add("Disaster Relief");
        areasOfOperations.add("Community Development");
        areasOfOperations.add("Entrepreneurship");
        areasOfOperations.add("Microfinance");
        areasOfOperations.add("Others");

        List<String> ageList = new ArrayList<>();
        ageList.add("0-3");
        ageList.add("3-5");
        ageList.add("6-8");
        ageList.add("9-13");
        ageList.add("14-16");
        ageList.add("16-25");
        ageList.add("25+");
        model.put("areasOfOperations", areasOfOperations);
        model.put(COMPANY, company);
        model.put("ageList", ageList);
        model.put("user", user);
        model.put(GLOBALSPONSORIMAGELIST, globalSponsorImageList);
        return "visitor/mngt/profilemngt/orgdetailsStep2";
    }


    @RequestMapping(value = "/EditOrganisationalDetailsStep3/{url1}")
    public String editOrganisationalDetailsStep3(
            @RequestParam(value = SUCCESS, required = false, defaultValue = "") String success,
            @RequestParam(value = ERROR, required = false, defaultValue = "") String error,
            @PathVariable(value = "url1") String url1,
            Map<String, Object> model){


        if (success != null && !success.isEmpty()) {
            model.put(SUCCESS, success);
        } else if (error != null && !error.isEmpty()) {
            model.put(ERROR, error);
        }

        boolean exceptionFlag = false;
        Long userId = Long.parseLong(url1);
        User user = null;
        try{
            user = userService.findById(userId);
        }catch(NoSuchUserException e){
            exceptionFlag = true;
            LOGGER.info(e);
        }
        if(!exceptionFlag){
            CompanyMaster company;
            if (user.getRole() == Role.COMPANY_ADMIN) {
                company = companyService.findByUserId(user.getUserId());
            } else if (user.getRole() == Role.MODULE_USER) {
                company = companyService.findByUserId(user.getCreatedBy()
                        .getUserId());
            } else {
                return REDIRECT_TO_LOGIN;
            }
            return processEditOrganisationalDetailsStep3(model, user, company);
        }
        else{
            return REDIRECT_TO_LOGIN;
        }   
    }

    private String processEditOrganisationalDetailsStep3(Map<String, Object> model, User user, CompanyMaster company) {
        String existingFunding = company.getExistingFunders();
        if(existingFunding!= null){
            String[] items = existingFunding.split(",");
            List<String> existingFunders = Arrays.asList(items);
            model.put("existingFund", existingFunders);
        }

        List<GlobalSponsoredManagement> globalSponsorImageList = globalSponsoredService.findByActiveandImageTypeGlobalSponsor(true, GLOBALSPONSOR);

        List<String> existingFundersList = new ArrayList<>();
        existingFundersList.add("Corporate Foundations");
        existingFundersList.add("Individuals");
        existingFundersList.add("Events");
        existingFundersList.add("Grants");
        existingFundersList.add("Fees");
        existingFundersList.add("Others");
        model.put("existingFundersList", existingFundersList);
        model.put(COMPANY, company);
        model.put("user", user);
        model.put(GLOBALSPONSORIMAGELIST, globalSponsorImageList);
        return "visitor/mngt/profilemngt/orgdetailsStep3";
    }


    @RequestMapping(value = "/ManageOrganisations/OrganisationsList")
    public String activeOrganisations(Map<String, Object> model){
        List<CompanyMaster> companyList = companyService.findByActive(true);
        List<User> userList = new ArrayList<>();

        for(CompanyMaster co : companyList){
            User userLastLogin = userService.findByMaxLastLogin(co.getCompanyId());
            userList.add(userLastLogin);
        }

        model.put(COMPANYLIST, companyList);
        model.put(USERLIST, userList);

        return "admin/mngt/admindashboardmngt/activeorg";
    }


    @RequestMapping(value="/Download/{url1}")
    public String numberOfDownloads(
    		@PathVariable("url1") String url1){

    	Long id = Long.parseLong(url1);

    	User user=null;
    	try {
    		user = (User) SecurityContextHolder.getContext()
    				.getAuthentication().getPrincipal();
    	} catch (Exception e) {
    		LOGGER.info(e);

    	}

    	AtmaDocumentMaster document = atmadocumentService.findByDocumentId(id);
    	Long categoryId = document.getCategory().getCategoryId();

    	if(user!=null){
    		if(user.getUserId()!=24 && user.getUserId()!=97){

    			int download = document.getNoOfDownloads();
    			document.setNoOfDownloads(++download);
    			document.setFlag(true);
    			atmadocumentService.update(document);   
    		}
    	}
    	return "redirect:/Projects?id="+categoryId;       
    }



    @RequestMapping("/*/ResultDataList")
    public String showResultData(
            @RequestParam(value = SUCCESS, required = false, defaultValue = "") String success,
            @RequestParam(value = ERROR, required = false, defaultValue = "") String error,
            Map<String, Object> model) {

        if (success != null && !success.isEmpty()) {
            model.put(SUCCESS, success);
        } else if (error != null && !error.isEmpty()) {
            model.put(ERROR, error);
        }

        List<LifeStageResultMaster> activeResultList = lifeStageResultService.findByActive(true);
        model.put("activeResultList", activeResultList);
        return "admin/surveymngt/surveyresultgrid";
    }


    @RequestMapping("/*/*/AddData")
    public String addSurveyResultData(
            @RequestParam(value = ERROR, required = false, defaultValue = "") String error,
            Map<String, Object> model) {

        if (error != null && !error.isEmpty()) {
            model.put(ERROR, error);
        }
        List<ModuleMaster> moduleList = moduleService.findByActive(true);   
        List<LifeStages> lifeStageList = new ArrayList<>();
        lifeStageList.add(LifeStages.LIFE_STAGE1);
        lifeStageList.add(LifeStages.LIFE_STAGE2);
        lifeStageList.add(LifeStages.LIFE_STAGE3);
        lifeStageList.add(LifeStages.LIFE_STAGE4);
        lifeStageList.add(LifeStages.LIFE_STAGE5);
        model.put(MODULELIST, moduleList);
        model.put(LIFESTAGELIST, lifeStageList);
        return "admin/surveymngt/addsurveyresult";
    }


    @RequestMapping("/*/*/EditData")
    public String editSurveyResultData(
            @RequestParam(value = "id") Long resultId,
            @RequestParam(value = ERROR, required = false, defaultValue = "") String error,
            Map<String, Object> model) {

        if (error != null && !error.isEmpty()) {
            model.put(ERROR, error);
        }

        LifeStageResultMaster lifeStageResultMaster = lifeStageResultService.findByResultId(resultId);
        List<ModuleMaster> moduleList = moduleService.findByActive(true);   
        List<LifeStages> lifeStageList = new ArrayList<>();
        lifeStageList.add(LifeStages.LIFE_STAGE1);
        lifeStageList.add(LifeStages.LIFE_STAGE2);
        lifeStageList.add(LifeStages.LIFE_STAGE3);
        lifeStageList.add(LifeStages.LIFE_STAGE4);
        lifeStageList.add(LifeStages.LIFE_STAGE5);

        model.put(MODULELIST, moduleList);
        model.put(LIFESTAGELIST, lifeStageList);
        model.put("resultSet", lifeStageResultMaster);

        return "admin/surveymngt/editsurveyresult";
    }


    @RequestMapping("/YourResults/{url1}")
    public String surveyResults(
            @PathVariable("url1") String url1,
            Map<String, Object> model){

        Long compId = Long.parseLong(url1);
        List<ModuleUser> surveyResultList = moduleUserService.findByCompanyCompanyId(compId);
        List<LifeStageResultMaster> resultSetList = new ArrayList<>();

        for(ModuleUser mu : surveyResultList){
            LifeStageResultMaster stageResult = lifeStageResultService.findByModuleandLifeStage(mu.getModule().getModuleId(), mu.getLifeStages());
            resultSetList.add(stageResult);
        }

        List<GlobalSponsoredManagement> globalSponsorImageList = globalSponsoredService.findByActiveandImageTypeGlobalSponsor(true, GLOBALSPONSOR);
        GlobalSponsoredManagement atmaContactImage = globalSponsoredService.findByActiveandImageTypeContact(true, CONTACT);

        model.put("surveyResultList", surveyResultList);
        model.put("resultSetList", resultSetList);
        model.put(GLOBALSPONSORIMAGELIST, globalSponsorImageList);
        model.put(ATMACONTACTIMAGE, atmaContactImage);

        return "visitor/mngt/surveymngt/surveyresult";
    }

    @RequestMapping(value = "/ManageForums/ForumList")
    public String forumList(
            @RequestParam(value = "id") Long moduleId,
            Map<String, Object> model){
        List<ForumMaster> forumList = forumService.findByModuleandActive(moduleId, true);
        ModuleMaster module = moduleService.findByModuleId(moduleId);
        model.put(FORUMLIST, forumList);
        model.put(MODULE, module);
        return "admin/atmaadmin/forumlist";
    }

    @RequestMapping(value = "/{url1}/{url2}/DeleteForum")
    public String deleteForum(
            @PathVariable("url1") String url1,
            @PathVariable("url2") String url2,
            @RequestParam(value=MODULEID) Long moduleId,
            @RequestParam(value = "id") Long forumId,
            Map<String, Object> model){

        forumService.delete(forumId);

        model.put(SUCCESS, "alert.forumdeletedsuccessfully");

        return REDIRECT + url1 + "/" + url2 + "?id=" +moduleId;
    }

    @RequestMapping(value = "/DeleteComment")
    public String deleteComment(
            @RequestParam(value = "id") Long commentId,
            @RequestParam(value = "forumId") Long forumId,
            Map<String, Object> model){

        commentService.delete(commentId);
        ForumMaster forum = forumService.findByForumId(forumId);
        String title = forum.getTitle();

        model.put(SUCCESS, "alert.commentdeletedsuccessfully");

        return "redirect:ForumComments/"+forumId;
    }

    @RequestMapping(value = "/webinarVideos")
    public String webinarVideos(Map<String, Object> model){
    	
    	System.out.println("in webinar videos");
        return "visitor/companyadmin/webinar";
    }

    @RequestMapping(value = "/InactiveOrganisations/{month}/{year}")
    @ResponseBody 
    public List<Long> getCountInactiveUsers (@PathVariable("month") String month,
            @PathVariable("year") String year)
            {
        int mnth = Integer.parseInt(month);
        int yr = Integer.parseInt(year);
        return  attendanceService.getCountOfInactiveUsers(mnth, yr);
            }

    @RequestMapping(value = "/InactiveOrganisationsAndUser/{year}")
    @ResponseBody 
    public Map<String, Integer[]> getCountInactiveUsers (@PathVariable("year") String year) throws IOException
    {
        int yr = Integer.parseInt(year);
        Calendar c = Calendar.getInstance();
        int currentYear = c.get(Calendar.YEAR);
        int currentMonth = c.get(Calendar.MONTH);

        int[] daysInAMonth = {31,29, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30 };
        Integer [] resultuserArray =new Integer[12];
        Integer [] resultOrgArray=new Integer[12];
        int x;
        for(x=1;x<=12;x++){

            int day = daysInAMonth[x-1];
            boolean isLeapYear = new GregorianCalendar().isLeapYear(yr);
            if (isLeapYear && x == 2) {
                day++;
            }
            String finalyear=year+"-"+x+"-"+day;
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd"); 
            Date startDate=null;
            try {
                startDate = df.parse(finalyear);
            } catch (ParseException e) {
                LOGGER.info(e);
            }

            if(yr==currentYear && x>currentMonth+1){

                resultuserArray[x-1]=0;
                resultOrgArray[x-1]=0;

            }else{

                long totalUserUptoMonth=userService.getTotaluserUptomonth(startDate);
                long inactiveUserCount=userService.getInactiveUserOfThatMonth(totalUserUptoMonth,yr,x);

                resultuserArray[x-1]=(int) inactiveUserCount;

                long totalOrgUptoMonth=companyService.getTotalOrgUptomonth(startDate);
                long inactiveOrgCount=companyService.getInactiveOrgOfThatMonth(totalOrgUptoMonth,yr,x);
                resultOrgArray[x-1]=(int) inactiveOrgCount;
            }
        }

        Map<String, Integer[]> finalmap=new HashMap<>();

        finalmap.put("one", resultuserArray);
        finalmap.put("two", resultOrgArray);

        return finalmap;
    }


    @RequestMapping(value = "/noOFCompaniesPerModule/{month}/{year}")
    @ResponseBody 
    public Map getNoOFCompaniesPerModule (@PathVariable("month") String month,
            @PathVariable("year") String year) throws IOException
            {
        int mnth=Integer.parseInt(month);
        int yr = Integer.parseInt(year);

        String stringDate=yr+"-"+mnth+"-"+"28";
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd"); 
        Date startDate=null;
        try {
            startDate = df.parse(stringDate);
        } catch (ParseException e) {
            LOGGER.info(e);
        }

        List<Object[]> recordList=surveyResultService.getAllByModuleIdAndRatingByYear(startDate);
        return getsurveyList(recordList);

            }

    @RequestMapping(value = "/DownloadXlSheet/{url1}")
    public String downloadXlSheet(
            @PathVariable("url1") String id,
            HttpServletResponse response) throws IOException{

        Long companyId = Long.parseLong(id);
        CompanyMaster company = companyService.findByCompanyId(companyId);
        try {
            moduleUserService.createLSSXlSheet(company);
        } catch (FileNotFoundException e1) {
            LOGGER.info(e1);
        }

        String absolutePath = documentService
                .getAbsoluteSaveLocationDir(company.getCompanyName());
        File file = new File(absolutePath +company.getCompanyName() +"_LSSRating"
                + ".csv");
        response.setHeader(CONTENTDISPOSITION,
                "attachment; filename=\"" +company.getCompanyName()+ "_LSSRating"+".csv" + "\"");
        try{
            OutputStream out = response.getOutputStream();
            response.setContentType("text/plain; charset=utf-8");
            FileInputStream fi = new FileInputStream(file);
            IOUtils.getInstance().copyStreams(fi, out);
            out.flush();
            out.close();
        }catch (IOException e) {
            LOGGER.info(e);
        }
        return null;
    }

    @RequestMapping(value = "/historyReportExcelAtmaAdmin",method=RequestMethod.GET, produces=TEXTCSV)
    @ResponseBody
    public FileSystemResource getHistoryReportExcelAtmaAdmin( HttpServletResponse response) throws IOException{

        Calendar c = Calendar.getInstance();
        int yr = c.get(Calendar.YEAR);
        int mmth = c.get(Calendar.MONTH)+1;

        Workbook wb = new XSSFWorkbook();
        CreationHelper createHelper = wb.getCreationHelper();
        Sheet sheet = wb.createSheet("Report");
        int rowIndex = 0;
        Row headerRow = sheet.createRow((short) rowIndex++);

        headerRow.createCell(0).setCellValue(
                createHelper.createRichTextString("Metric"));
        headerRow.createCell(1).setCellValue(
                createHelper.createRichTextString("Month"));
        headerRow.createCell(2).setCellValue(
                createHelper.createRichTextString("Year"));
        headerRow.createCell(3).setCellValue(
                createHelper.createRichTextString("Value"));

        int totalOrganisations=companyService.gettotalOrganisations();
        int activeTotalUsers=userService.getActiveTotalUsers(Role.COMPANY_ADMIN,Role.MODULE_USER, UserStatus.ACTIVE);
        int totalDownloads = atmadocumentService.totalSumofDownloads();
        int totalPost=forumService.getTotalPosts();
        int totalComment=commentService.getTotalComment();


        Row row1=sheet.createRow(rowIndex++);
        row1.createCell(0).setCellValue(createHelper.createRichTextString("Total Organisations"));
        row1.createCell(1).setCellValue(createHelper.createRichTextString(String.valueOf(mmth)));
        row1.createCell(2).setCellValue(createHelper.createRichTextString(String.valueOf(yr)));
        row1.createCell(3).setCellValue(createHelper.createRichTextString(String.valueOf(totalOrganisations)));

        Row row2=sheet.createRow(rowIndex++);
        row2.createCell(0).setCellValue(createHelper.createRichTextString("Total Users"));
        row2.createCell(1).setCellValue(createHelper.createRichTextString(String.valueOf(mmth)));
        row2.createCell(2).setCellValue(createHelper.createRichTextString(String.valueOf(yr)));
        row2.createCell(3).setCellValue(createHelper.createRichTextString(String.valueOf(activeTotalUsers)));

        Row row3=sheet.createRow(rowIndex++);
        row3.createCell(0).setCellValue(createHelper.createRichTextString("Total Downloads"));
        row3.createCell(1).setCellValue(createHelper.createRichTextString(String.valueOf(mmth)));
        row3.createCell(2).setCellValue(createHelper.createRichTextString(String.valueOf(yr)));
        row3.createCell(3).setCellValue(createHelper.createRichTextString(String.valueOf(totalDownloads)));

        Row row4=sheet.createRow(rowIndex++);
        row4.createCell(0).setCellValue(createHelper.createRichTextString("Total Forums"));
        row4.createCell(1).setCellValue(createHelper.createRichTextString(String.valueOf(mmth)));
        row4.createCell(2).setCellValue(createHelper.createRichTextString(String.valueOf(yr)));
        row4.createCell(3).setCellValue(createHelper.createRichTextString(String.valueOf(totalPost)));

        Row row5=sheet.createRow(rowIndex++);
        row5.createCell(0).setCellValue(createHelper.createRichTextString("Total Comments"));
        row5.createCell(1).setCellValue(createHelper.createRichTextString(String.valueOf(mmth)));
        row5.createCell(2).setCellValue(createHelper.createRichTextString(String.valueOf(yr)));
        row5.createCell(3).setCellValue(createHelper.createRichTextString(String.valueOf(totalComment)));

        int fullProfileComplete=userService.getFullProfileComplete();
        int noOdSurveyComplited=surveyManagerService.getNoOdSurveyComplited();

        Row row6=sheet.createRow(rowIndex++);
        row6.createCell(0).setCellValue(createHelper.createRichTextString("Towards Goal-LSS"));
        row6.createCell(1).setCellValue(createHelper.createRichTextString(String.valueOf(mmth)));
        row6.createCell(2).setCellValue(createHelper.createRichTextString(String.valueOf(yr)));
        row6.createCell(3).setCellValue(createHelper.createRichTextString(String.valueOf(noOdSurveyComplited)));

        Row row7=sheet.createRow(rowIndex++);
        row7.createCell(0).setCellValue(createHelper.createRichTextString("Towards Goal-Profile Completion"));
        row7.createCell(1).setCellValue(createHelper.createRichTextString(String.valueOf(mmth)));
        row7.createCell(2).setCellValue(createHelper.createRichTextString(String.valueOf(yr)));
        row7.createCell(3).setCellValue(createHelper.createRichTextString(String.valueOf(fullProfileComplete)));


        Long toU= new Long(activeTotalUsers);
        long totalU=toU.longValue();

        long totalOrg=toU.longValue();
        int userListExcel=userService.getInactiveUserListExcel(totalU,yr,Role.COMPANY_ADMIN,Role.MODULE_USER,mmth);
        int inactiveOrgListExcel=companyService.getInactiveOrgListExcel(totalOrg,yr,mmth);

        Row row8=sheet.createRow(rowIndex++);
        row8.createCell(0).setCellValue(createHelper.createRichTextString(INACTIVEUSER));
        row8.createCell(1).setCellValue(createHelper.createRichTextString(String.valueOf(mmth)));
        row8.createCell(2).setCellValue(createHelper.createRichTextString(String.valueOf(yr)));
        row8.createCell(3).setCellValue(createHelper.createRichTextString(String.valueOf(userListExcel)));

        Row row9=sheet.createRow(rowIndex++);
        row9.createCell(0).setCellValue(createHelper.createRichTextString("Inactive Organisation"));
        row9.createCell(1).setCellValue(createHelper.createRichTextString(String.valueOf(mmth)));
        row9.createCell(2).setCellValue(createHelper.createRichTextString(String.valueOf(yr)));
        row9.createCell(3).setCellValue(createHelper.createRichTextString(String.valueOf(inactiveOrgListExcel)));

        List<Object[]> barData=moduleUserService.getAvgOda();
        List<Map<String, Object>> mapResult = new ArrayList<>(); 
        for(Object[] cc:barData) {
            Map<String, Object> map3= new HashMap<>();
            map3.put("avg", cc[0]);
            map3.put(COUNT, cc[1]);
            map3.put(LABLE, cc[2]);
            mapResult.add(map3);
        }

        for (Map<String, Object> mm : mapResult) {

            Row rowODA=sheet.createRow(rowIndex++);
            rowODA.createCell(0).setCellValue(createHelper.createRichTextString("Average ODA Value-"+mm.get(LABLE).toString()));
            rowODA.createCell(1).setCellValue(createHelper.createRichTextString(String.valueOf(mmth)));
            rowODA.createCell(2).setCellValue(createHelper.createRichTextString(String.valueOf(yr)));
            rowODA.createCell(3).setCellValue(createHelper.createRichTextString(String.valueOf(mm.get("avg").toString())));

        }

        List<Object[]> pieChartData=atmadocumentService.getDownloadPerModule();
        List<Map<String, Object>> mapResult2 = new ArrayList<>(); 

        for(int k=0;k<pieChartData.size()-1;k++)
        {
            Map<String, Object> map3= new HashMap<>();
            map3.put(MODULE, pieChartData.get(k)[0]);
            map3.put("sum", pieChartData.get(k)[1]);
            mapResult2.add(map3);
        }

        for (Map<String, Object> map : mapResult2) {

            Row rowDownload=sheet.createRow(rowIndex++);
            rowDownload.createCell(0).setCellValue(createHelper.createRichTextString("Download Analysis-"+map.get(MODULE).toString()));
            rowDownload.createCell(1).setCellValue(createHelper.createRichTextString(String.valueOf(mmth)));
            rowDownload.createCell(2).setCellValue(createHelper.createRichTextString(String.valueOf(yr)));
            rowDownload.createCell(3).setCellValue(createHelper.createRichTextString(String.valueOf(map.get("sum").toString())));


        }

		String tempUrl=propertyValues.tempCsvDownloadpath;
        FileOutputStream fileOutUrl = new FileOutputStream( tempUrl+ File.separator +ATMAADMINHISTORY+yr+"_"+mmth+".csv");
        String url=tempUrl+ File.separator +ATMAADMINHISTORY+yr+"_"+mmth+".csv";
        wb.write(fileOutUrl);
        fileOutUrl.close();

        response.setContentType(TEXTCSV); 
        String fileName=ATMAADMINHISTORY+yr+"_"+mmth+".csv";
        String headerKey = CONTENTDISPOSITION;
        String headerValue = String.format("attachment; filename=\"%s\"",
                fileName);
        response.setHeader(headerKey, headerValue);

        return new FileSystemResource(new File(url));

    }   

    @RequestMapping(value = "/historyReportExcel",method=RequestMethod.GET, produces=TEXTCSV)
    @ResponseBody
    public FileSystemResource getHistoryReportExcel(HttpServletResponse response) throws IOException{

        Calendar c = Calendar.getInstance();
        int yr = c.get(Calendar.YEAR);
        int mmth = c.get(Calendar.MONTH)+1;

        String year=String.valueOf(yr);
        String month=String.valueOf(mmth);

        Workbook wb = new XSSFWorkbook();
        CreationHelper createHelper = wb.getCreationHelper();
        Sheet sheet = wb.createSheet("Report");
        int rowIndex = 0;
        Row headerRow = sheet.createRow((short) rowIndex++);
        headerRow.createCell(0).setCellValue(
                createHelper.createRichTextString("Metric Category"));
        headerRow.createCell(1).setCellValue(
                createHelper.createRichTextString("Metric Category 2"));
        headerRow.createCell(2).setCellValue(
                createHelper.createRichTextString("Metric Category 3"));
        headerRow.createCell(3).setCellValue(
                createHelper.createRichTextString("Year"));
        headerRow.createCell(4).setCellValue(
                createHelper.createRichTextString("month"));
        headerRow.createCell(5).setCellValue(
                createHelper.createRichTextString("value"));

        int [] companyProfileArray=getCompanyProfileArray();

        int inactiveUser1=userService.noOfInactiveUserExcel(30);
        int inactiveUser2=userService.noOfInactiveUserExcel(60);
        int inactiveUser3=userService.noOfInactiveUserExcel(90);
        int inactiveUser4=userService.noOfInactiveUserExcel(120);
        int inactiveUser5=userService.noOfInactiveUserExcel(150);
        int inactiveUser6=userService.noOfInactiveUserExcel(180);

        int inactiveOrganisation1=companyService.getInctiveOrganisation(30);
        int inactiveOrganisation2=companyService.getInctiveOrganisation(60);
        int inactiveOrganisation3=companyService.getInctiveOrganisation(90);
        int inactiveOrganisation4=companyService.getInctiveOrganisation(120);
        int inactiveOrganisation5=companyService.getInctiveOrganisation(150);
        int inactiveOrganisation6=companyService.getInctiveOrganisation(180);

        List<Object[]> educationArea=companyService.getAllRecod();
        List<Map<String, Object>> mapResultTest = buildNgoSectorList(educationArea);
        List<Object[]> topCities = companyService.topCities();

        int x=0;
        List<Map<String, Object>> cityResult = new ArrayList<>(); 
        for(Object[] c2:topCities) {
            Map< String, Object> mm= new HashMap<>();
            mm.put(COMPANYID, c2[0]);
            mm.put(CITYNAME, c2[1]);
            cityResult.add(mm);
            x++;
            if(x>=5){
                break;
            }

        }

        List<Object[]> recordList1=surveyResultService.getAllByModuleIdAndRating();

        Map<Integer, int []> surveyMap= new HashMap<>();

        for(int i=0;i<5;i++){
            surveyMap.put(i+1, new int[header.length]);
        }

        for(Object[] ob:recordList1){
            int index=str.indexOf(ob[3].toString());
            surveyMap.get(LifeStages.valueOf(ob[2].toString()).getId()-1)[index]=Integer.parseInt(ob[0].toString());
        }

        List<Map<String, Object>> surveyResultmap;
        surveyResultmap=buildSurveyMap(recordList1);
        List<ModuleMaster> modules = moduleService.findByActive(true);
        List<Map<String, Object>> orgDevMap = getOrganisationDevlopmentList(modules);

        for(int i=1;i<11;i++){
            Row row1=sheet.createRow(rowIndex++);
            row1.createCell(0).setCellValue(createHelper.createRichTextString(PROFILECOMPLETIONCHART));
            row1.createCell(1).setCellValue(createHelper.createRichTextString(String.valueOf(10*i)));
            row1.createCell(3).setCellValue(createHelper.createRichTextString(year));
            row1.createCell(4).setCellValue(createHelper.createRichTextString(month));
            row1.createCell(5).setCellValue(createHelper.createRichTextString(String.valueOf(companyProfileArray[i])));

        }

        Row rowinactive1=sheet.createRow(rowIndex++);
        rowinactive1.createCell(0).setCellValue(createHelper.createRichTextString(INACTIVEUSER));
        rowinactive1.createCell(1).setCellValue(createHelper.createRichTextString("< 1 Month"));
        rowinactive1.createCell(3).setCellValue(createHelper.createRichTextString(year));
        rowinactive1.createCell(4).setCellValue(createHelper.createRichTextString(month));
        rowinactive1.createCell(5).setCellValue(createHelper.createRichTextString(String.valueOf(inactiveUser1)));

        Row rowinactive2=sheet.createRow(rowIndex++);
        rowinactive2.createCell(0).setCellValue(createHelper.createRichTextString(INACTIVEUSER));
        rowinactive2.createCell(1).setCellValue(createHelper.createRichTextString("< 2 Month"));
        rowinactive2.createCell(3).setCellValue(createHelper.createRichTextString(year));
        rowinactive2.createCell(4).setCellValue(createHelper.createRichTextString(month));
        rowinactive2.createCell(5).setCellValue(createHelper.createRichTextString(String.valueOf(inactiveUser2)));

        Row rowinactive3=sheet.createRow(rowIndex++);
        rowinactive3.createCell(0).setCellValue(createHelper.createRichTextString("Inactive User "));
        rowinactive3.createCell(1).setCellValue(createHelper.createRichTextString("< 3 Month"));
        rowinactive3.createCell(3).setCellValue(createHelper.createRichTextString(year));
        rowinactive3.createCell(4).setCellValue(createHelper.createRichTextString(month));
        rowinactive3.createCell(5).setCellValue(createHelper.createRichTextString(String.valueOf(inactiveUser3)));

        Row rowinactive4=sheet.createRow(rowIndex++);
        rowinactive4.createCell(0).setCellValue(createHelper.createRichTextString(INACTIVEUSER));
        rowinactive4.createCell(1).setCellValue(createHelper.createRichTextString("< 4 Month"));
        rowinactive4.createCell(3).setCellValue(createHelper.createRichTextString(year));
        rowinactive4.createCell(4).setCellValue(createHelper.createRichTextString(month));
        rowinactive4.createCell(5).setCellValue(createHelper.createRichTextString(String.valueOf(inactiveUser4)));

        Row rowinactive5=sheet.createRow(rowIndex++);
        rowinactive5.createCell(0).setCellValue(createHelper.createRichTextString(INACTIVEUSER));
        rowinactive5.createCell(1).setCellValue(createHelper.createRichTextString("< 5 Month"));
        rowinactive5.createCell(3).setCellValue(createHelper.createRichTextString(year));
        rowinactive5.createCell(4).setCellValue(createHelper.createRichTextString(month));
        rowinactive5.createCell(5).setCellValue(createHelper.createRichTextString(String.valueOf(inactiveUser5)));

        Row rowinactive6=sheet.createRow(rowIndex++);
        rowinactive6.createCell(0).setCellValue(createHelper.createRichTextString(INACTIVEUSER));
        rowinactive6.createCell(1).setCellValue(createHelper.createRichTextString("< 6 Month"));
        rowinactive6.createCell(3).setCellValue(createHelper.createRichTextString(year));
        rowinactive6.createCell(4).setCellValue(createHelper.createRichTextString(month));
        rowinactive6.createCell(5).setCellValue(createHelper.createRichTextString(String.valueOf(inactiveUser6)));

        Row rowinactive7=sheet.createRow(rowIndex++);
        rowinactive7.createCell(0).setCellValue(createHelper.createRichTextString(INACTIVEORGANIZATION));
        rowinactive7.createCell(1).setCellValue(createHelper.createRichTextString("< 1 Month"));
        rowinactive7.createCell(3).setCellValue(createHelper.createRichTextString(year));
        rowinactive7.createCell(4).setCellValue(createHelper.createRichTextString(month));
        rowinactive7.createCell(5).setCellValue(createHelper.createRichTextString(String.valueOf(inactiveOrganisation1)));

        Row rowinactive8=sheet.createRow(rowIndex++);
        rowinactive8.createCell(0).setCellValue(createHelper.createRichTextString(INACTIVEORGANIZATION));
        rowinactive8.createCell(1).setCellValue(createHelper.createRichTextString("< 2 Month"));
        rowinactive8.createCell(3).setCellValue(createHelper.createRichTextString(year));
        rowinactive8.createCell(4).setCellValue(createHelper.createRichTextString(month));
        rowinactive8.createCell(5).setCellValue(createHelper.createRichTextString(String.valueOf(inactiveOrganisation2)));

        Row rowinactive9=sheet.createRow(rowIndex++);
        rowinactive9.createCell(0).setCellValue(createHelper.createRichTextString(INACTIVEORGANIZATION));
        rowinactive9.createCell(1).setCellValue(createHelper.createRichTextString("< 3 Month"));
        rowinactive9.createCell(3).setCellValue(createHelper.createRichTextString(year));
        rowinactive9.createCell(4).setCellValue(createHelper.createRichTextString(month));
        rowinactive9.createCell(5).setCellValue(createHelper.createRichTextString(String.valueOf(inactiveOrganisation3)));

        Row rowinactive10=sheet.createRow(rowIndex++);
        rowinactive10.createCell(0).setCellValue(createHelper.createRichTextString(INACTIVEORGANIZATION));
        rowinactive10.createCell(1).setCellValue(createHelper.createRichTextString("< 4 Month"));
        rowinactive10.createCell(3).setCellValue(createHelper.createRichTextString(year));
        rowinactive10.createCell(4).setCellValue(createHelper.createRichTextString(month));
        rowinactive10.createCell(5).setCellValue(createHelper.createRichTextString(String.valueOf(inactiveOrganisation4)));

        Row rowinactive11=sheet.createRow(rowIndex++);
        rowinactive11.createCell(0).setCellValue(createHelper.createRichTextString(INACTIVEORGANIZATION));
        rowinactive11.createCell(1).setCellValue(createHelper.createRichTextString("< 5 Month"));
        rowinactive11.createCell(3).setCellValue(createHelper.createRichTextString(year));
        rowinactive11.createCell(4).setCellValue(createHelper.createRichTextString(month));
        rowinactive11.createCell(5).setCellValue(createHelper.createRichTextString(String.valueOf(inactiveOrganisation5)));

        Row rowinactive12=sheet.createRow(rowIndex++);
        rowinactive12.createCell(0).setCellValue(createHelper.createRichTextString(INACTIVEORGANIZATION));
        rowinactive12.createCell(1).setCellValue(createHelper.createRichTextString("< 6 Month"));
        rowinactive12.createCell(3).setCellValue(createHelper.createRichTextString(year));
        rowinactive12.createCell(4).setCellValue(createHelper.createRichTextString(month));
        rowinactive12.createCell(5).setCellValue(createHelper.createRichTextString(String.valueOf(inactiveOrganisation6)));

        for (Map<String, Object> map : mapResultTest) {
            Row rowNgo=sheet.createRow(rowIndex++);
            rowNgo.createCell(0).setCellValue(createHelper.createRichTextString("NGO Sector Chart"));
            rowNgo.createCell(1).setCellValue(createHelper.createRichTextString(map.get(LABEL).toString()));
            rowNgo.createCell(3).setCellValue(createHelper.createRichTextString(year));
            rowNgo.createCell(4).setCellValue(createHelper.createRichTextString(month));
            rowNgo.createCell(5).setCellValue(createHelper.createRichTextString(map.get("y").toString()));
        }

        for (Map<String, Object> objects : cityResult) {
            Row rowCities=sheet.createRow(rowIndex++);
            rowCities.createCell(0).setCellValue(createHelper.createRichTextString("Graphical Distribution Analysis"));
            rowCities.createCell(1).setCellValue(createHelper.createRichTextString(objects.get(CITYNAME).toString()));
            rowCities.createCell(3).setCellValue(createHelper.createRichTextString(year));
            rowCities.createCell(4).setCellValue(createHelper.createRichTextString(month));
            rowCities.createCell(5).setCellValue(createHelper.createRichTextString(objects.get(COMPANYID).toString()));
        }

        for (Map<String, Object> map : surveyResultmap) {
            Row rowSurvey=sheet.createRow(rowIndex++);
            rowSurvey.createCell(0).setCellValue(createHelper.createRichTextString("Life Stage Survey Chart"));
            rowSurvey.createCell(1).setCellValue(createHelper.createRichTextString(map.get(MODULE).toString()));
            rowSurvey.createCell(3).setCellValue(createHelper.createRichTextString(year));
            rowSurvey.createCell(4).setCellValue(createHelper.createRichTextString(month));
            rowSurvey.createCell(5).setCellValue(createHelper.createRichTextString(map.get(COUNT).toString()));
        }

        for (Map<String, Object> cell : orgDevMap) {

            Row rowSurvey=sheet.createRow(rowIndex++);
            rowSurvey.createCell(0).setCellValue(createHelper.createRichTextString("Organisational Developmental Chart"));
            rowSurvey.createCell(1).setCellValue(createHelper.createRichTextString(cell.get(MODULE).toString()));
            rowSurvey.createCell(2).setCellValue(createHelper.createRichTextString(cell.get(TITLE).toString()));
            rowSurvey.createCell(3).setCellValue(createHelper.createRichTextString(year));
            rowSurvey.createCell(4).setCellValue(createHelper.createRichTextString(month));
            rowSurvey.createCell(5).setCellValue(createHelper.createRichTextString(cell.get(COUNT).toString()));
        }

        String tempUrl=propertyValues.tempCsvDownloadpath;

        FileOutputStream fileOutUrl = new FileOutputStream( tempUrl+ File.separator +SUPERADMINHISTORY+yr+"_"+mmth+".csv");
        String url=tempUrl+ File.separator +SUPERADMINHISTORY+yr+"_"+mmth+".csv";
        wb.write(fileOutUrl);
        fileOutUrl.close();

        String headerKey = CONTENTDISPOSITION;
        response.setContentType(TEXTCSV); 
        String fileName=SUPERADMINHISTORY+yr+"_"+mmth+".csv";
        String headerValue = String.format("attachment; filename=\"%s\"",
                fileName);
        response.setHeader(headerKey, headerValue);

        return new FileSystemResource(new File(url));
    }

    private  List<Map<String, Object>> getOrganisationDevlopmentList(List<ModuleMaster> modules) {

        Map<String, Object> arrayMap=getModuleArray(modules);
        List<Integer> countperModule = (List<Integer>) arrayMap.get(COUNTMODULE);
        List<Integer> userperModule = (List<Integer>) arrayMap.get(USERMODULE);
        List<Integer> forumperModule = (List<Integer>) arrayMap.get(FORUMMODULE);
        List<AtmaDocumentMaster> documentList = (List<AtmaDocumentMaster>) arrayMap.get(DOCLIST);

        List<Map<String, Object>> orgDevMap = new ArrayList<>();
        int countModule=0;
        for (ModuleMaster module : modules) {

            Map<String ,Object> map= new HashMap<>();
            map.put(MODULE, module.getModuleName());
            map.put(TITLE, "No. of Users assigned");
            map.put(COUNT, userperModule.get(countModule));
            orgDevMap.add(map);

            Map<String ,Object> map1= new HashMap<>();
            map1.put(MODULE, module.getModuleName());
            map1.put(TITLE, "No. of Downloads");
            map1.put(COUNT, countperModule.get(countModule));
            orgDevMap.add(map1);

            Map<String ,Object> map2= new HashMap<>();
            map2.put(MODULE, module.getModuleName());
            map2.put(TITLE, "No. of Forum Interactions");
            map2.put(COUNT, forumperModule.get(countModule));
            orgDevMap.add(map2);

            Map<String ,Object> map3= new HashMap<>();
            map3.put(MODULE, module.getModuleName());
            map3.put(TITLE, "Most Popular Download");
            map3.put(COUNT, documentList.get(countModule).getTitle());
            orgDevMap.add(map3);

            countModule++;

        }

        return orgDevMap;
    }

    private static List<Map<String, Object>> buildNgoSectorList(List<Object[]> educationArea) {

        List<Map<String, Object>> mapResultTest = new ArrayList<>();
        for (String string : STRARRAY) {
            int flag=0;
            for(Object[] c1:educationArea) {
                String str1=c1[1].toString();

                if(str1.equalsIgnoreCase(string)){
                    Map<String, Object> map3= new HashMap<>();
                    map3.put("y", c1[0]);
                    map3.put(LABEL, c1[1]);
                    mapResultTest.add(map3);
                    flag=1;
                }
            }
            if(flag==0){

                Map<String, Object> map3= new HashMap<>();
                map3.put("y", 0);
                map3.put(LABEL, string);
                mapResultTest.add(map3);
            }
        }
        return mapResultTest;
    }


    private static List<Map<String, Object>> buildSurveyMap(List<Object[]> recordList1) {

        List<Map<String, Object>> surveyResultmap = new ArrayList<>();
        String department=null;
        int i=1;
        for(Object[] ob:recordList1){

            int b=i;

            int lifestage=LifeStages.valueOf(ob[2].toString()).getId()-1;
            if(lifestage>=i){
                department=ob[3].toString();

                for (int z=b;z<=lifestage;z++){

                    surveyResultmap= lifestages(lifestage,i,ob,surveyResultmap);
                    i++;
                }
            }else{

                for(int n=i;n<=5;n++){

                    Map< String, Object> mm= new HashMap<>();
                    mm.put(MODULE, department);
                    mm.put(COUNT, "0");
                    mm.put("life", lifestage);
                    surveyResultmap.add(mm);
                    i++;
                }

                if(i==6){
                    i=1;
                    surveyResultmap=lifestagesOffset(lifestage, ob, surveyResultmap);
                    i=i+lifestage;
                }

            }
            if(i==6){
                i=1;
            }
        }
        return surveyResultmap;
    }

    private static List<Map<String, Object>> lifestagesOffset(int lifestage,
            Object[] ob, List<Map<String, Object>> surveyResultmap) {

        for(int y=1;y<=lifestage;y++){

            if(y==lifestage){
                Map< String, Object> mm= new HashMap<>();
                mm.put(MODULE, ob[3]);
                mm.put(COUNT, ob[0]);
                mm.put("life", lifestage);
                surveyResultmap.add(mm);
            }else{
                Map< String, Object> mm= new HashMap<>();
                mm.put(MODULE, ob[3]);
                mm.put(COUNT, "0");
                mm.put("life", y);
                surveyResultmap.add(mm);
            }
        }

        return surveyResultmap;
    }

    private static List<Map<String, Object>> lifestages(int lifestage, int i,
            Object[] ob, List<Map<String, Object>> surveyResultmap) {
        int k=i;
        if(lifestage==k){
            Map< String, Object> mm= new HashMap<>();
            mm.put(MODULE, ob[3]);
            mm.put(COUNT, ob[0]);
            mm.put("life", lifestage);
            surveyResultmap.add(mm);
        }else{
            Map< String, Object> mm= new HashMap<>();
            mm.put(MODULE, ob[3]);
            mm.put(COUNT, "0");
            mm.put("life", lifestage);
            surveyResultmap.add(mm);
        }
        return surveyResultmap;
    }

    @RequestMapping(value = "/DownloadInfoExcel",method=RequestMethod.GET, produces=TEXTCSV)
    @ResponseBody
    public FileSystemResource downloadInfo( HttpServletResponse response) throws IOException{


        String url=null;
        try {
            url=companyService.createXlSheetForInactiveOrg();
        } catch (FileNotFoundException e1) {
            LOGGER.info(e1);

        }
        response.setContentType(TEXTCSV); 
        String excelName="inactiveOrganisation.csv";
        String headerVal = CONTENTDISPOSITION;
        String headerValue = String.format("attachment; filename=\"%s\"",
                excelName);
        response.setHeader(headerVal, headerValue);

        return new FileSystemResource(new File(url));
    }


    @RequestMapping(value = "/excelOfLSSTowardGoal",method=RequestMethod.GET, produces=TEXTCSV)
    @ResponseBody
    public FileSystemResource excelOfLSSTowardGoal( HttpServletResponse response) throws FileNotFoundException{
        String url=null;
        try {
            url=surveyManagerService.getIncompliteSurvey();
        } catch (Exception e) {
            LOGGER.info(e);
        }    

        response.setContentType(TEXTCSV); 

        String fileName="LSS.csv";
        String headerKey = CONTENTDISPOSITION;
        String headerValue = String.format("attachment; filename=\"%s\"",
                fileName);
        response.setHeader(headerKey, headerValue);

        return new FileSystemResource(new File(url));
    }


    @RequestMapping(value = "/DownloadExcelOfInactiveUserAndOrg",method=RequestMethod.GET, produces=TEXTCSV)
    @ResponseBody
    public FileSystemResource downloadExcelOfInactiveUserAndOrg(HttpServletResponse response) throws IOException{
        String url=null;

        try {
            url=companyService.createExcelSheetForInactiveOrg();
        } catch (FileNotFoundException e1) {
            LOGGER.info(e1);
        }
        response.setContentType(TEXTCSV); 
        String fileName="inactiveOrganisations.csv";
        String headerKey = CONTENTDISPOSITION;
        String headerValue = String.format("attachment; filename=\"%s\"",
                fileName);
        response.setHeader(headerKey, headerValue);

        return new FileSystemResource(new File(url));
    }


    @RequestMapping(value = "/excelOfUserTowardGoal",method=RequestMethod.GET, produces=TEXTCSV)
    @ResponseBody
    public FileSystemResource excelOfUserTowardGoal( HttpServletResponse response) throws IOException{

        String url=null;
        try {
            url=userService.getUserOfIncompleteProfile();        
        } catch (Exception e) {
            LOGGER.info(e);
        }
        response.setContentType(TEXTCSV); 
        String fileName="user.csv";
        String headerKey = CONTENTDISPOSITION;
        String headerValue = String.format("attachment; filename=\"%s\"",
                fileName);
        response.setHeader(headerKey, headerValue);
        return new FileSystemResource(new File(url));
    }

    @RequestMapping(value = "/DownloadCompanyZip/{url1}")
    public String downloadCompanyZip(
            @PathVariable("url1") String id,
            HttpServletResponse response) throws IOException{
        Long companyId = Long.parseLong(id);
        CompanyMaster company = companyService.findByCompanyId(companyId);

        try {
            companyService.createCompanyInfoZip(company);
        } catch (FileNotFoundException e1) {
            LOGGER.info(e1);
        }
        String absolutePath = documentService
                .getAbsoluteSaveLocationDir(company.getCompanyName());
        File file = new File(absolutePath +".zip");
        response.setHeader(CONTENTDISPOSITION,
                "attachment; filename=\"" +company.getCompanyName() +".zip" + "\"");
        try{
            response.setContentType("text/plain; charset=utf-8");
            OutputStream out = response.getOutputStream();
            FileInputStream fi = new FileInputStream(file);
            IOUtils.getInstance().copyStreams(fi, out);
            out.flush();
            out.close();
        }catch (IOException e) {
            LOGGER.info(e);
        }
        return null;
    }

    @RequestMapping(value = "/LSSRatings")
    public String getActiveOrganisations(
            Map<String, Object> model){
        List<CompanyMaster> companyList = companyService.findByActive(true);
        model.put(COMPANYLIST, companyList);
        return "admin/mngt/admindashboardmngt/lssrating";
    }


    @RequestMapping(value = "/lastComments")
    public String getLastComments(
            Map<String, Object> model) throws IOException{
        List<Object[]> lastComment=commentService.getLastComments();

  //      List<Object[]> lastComForum=commentService.getLastCommentsForum();

        if(lastComment!=null){

            List<Map<String, Object>> results = new ArrayList<>(); 
            for(Object[] c:lastComment) {
                Map< String, Object> mm= new HashMap<>();
                mm.put("id", c[0]);
                mm.put("postedOn", c[1]);
                mm.put(CATEGORY, c[2]);
                mm.put(FIRSTNAME, c[3]);
                mm.put("Lastname", c[4]);
                mm.put("flag", c[5]);

                results.add(mm);
            }

            model.put(RESULTS, results);

        }
        return "admin/mngt/recentActivity/lastComment";
    }

    @RequestMapping(value = "/InactiveFlagLastComments")
    @ResponseBody 
    public String getInactiveFlagLastComments () throws IOException
    {
        List<Comments> com=commentService.getCommentOfTrueFlag();
        for (Comments comments : com) {
            comments.setFlag(false);
            commentService.update(comments);
        }

        return SUCCESS;
    }

    @RequestMapping(value = "/lastDownloads")
    public String getLastDownloads(
            Map<String, Object> model) throws IOException{
        List<Object[]> lastDownload=atmadocumentService.getLastDownload();

        if(lastDownload!=null){

            List<Map<String, Object>> lastDownloadResults = new ArrayList<>(); 
            for(Object[] c:lastDownload) {
                Map< String, Object> mm= new HashMap<>();
                mm.put("id", c[0]);
                mm.put("date", c[1]);
                mm.put(FIRSTNAME, c[2]);
                mm.put("lastName", c[3]);
                mm.put("docName", c[4]);
                mm.put("flag", c[5]);
                lastDownloadResults.add(mm);
            }

            model.put("lastDownload", lastDownloadResults);
        }

        return "admin/mngt/recentActivity/lastDownload";
    }

    @RequestMapping(value = "/inactiveFlagLastDownloads")
    @ResponseBody 
    public String getIctiveFlagLastDownloads () throws IOException
    {
        List<AtmaDocumentMaster> doc=atmadocumentService.getInactiveFlagLastDownloads();
        for (AtmaDocumentMaster atmaDocumentMaster : doc) {
            atmaDocumentMaster.setFlag(false);
            atmadocumentService.update(atmaDocumentMaster);
        }
        
        atmadocumentService.updateActiveAuditDownload();
        return SUCCESS;
    }


    @RequestMapping(value = "/inactiveFlagLastPosts")
    @ResponseBody 
    public String getInactiveFlagLastPosts () throws IOException
    {

    	List<Comments> posts=commentService.getInactiveFlagLastPosts();
    	for (Comments comments : posts) {
            comments.setFlag(false);
            commentService.update(comments);
        }
    	
        return SUCCESS;
    }

    @RequestMapping(value = "/inactiveFlagLastUser")
    @ResponseBody 
    public String getInactiveFlagLastUser () throws IOException
    {
        List<User> users=userService.getInactiveFlagLastUser();
        for (User user : users) {
            user.setFlag(false);
            userService.update(user);
        }

        return SUCCESS;
    }

    @RequestMapping(value = "/inactiveFlagLastOrganisation")
    @ResponseBody 
    public String getInactiveFlagLastOrganisation () throws IOException
    {
        List<CompanyMaster> companyMasters=companyService.getInactiveFlagLastOrganisation();
        for (CompanyMaster companyMaster : companyMasters) {
            companyMaster.setFlag(false);
            companyService.update1(companyMaster);
        }

        return SUCCESS;
    }

    @RequestMapping(value = "/lastAddedUsers")
    public String getLastAddedUsers(
            Map<String, Object> model) throws IOException{
        List<Object[]> lastAddedUser=userService.getLastAddedUser();

        if(lastAddedUser!=null){

            List<Map<String, Object>> results = new ArrayList<>(); 
            for(Object[] c:lastAddedUser) {
                Map< String, Object> mm= new HashMap<>();
                mm.put("id", c[0]);
                mm.put(FIRSTNAME, c[1]);
                mm.put("lastName", c[2]);
                mm.put("date", c[3]);
                mm.put("by", c[4]);
                mm.put("role", c[5]);
                mm.put("status", c[6]);

                results.add(mm);
            }

            model.put(RESULTS, results);
        }

        return "admin/mngt/recentActivity/lastAddedUser";
    }

    @RequestMapping(value = "/lastAddedOrganisation")
    public String getlastAddedOrganisation(
            Map<String, Object> model) throws IOException{
        List<Object[]> lastAddedOrganisation=companyService.getLastAddedOrganisation();

        List<Map<String, Object>> results = new ArrayList<>(); 
        for(Object[] c:lastAddedOrganisation) {
            Map< String, Object> mm= new HashMap<>();
            mm.put("id", c[0]);
            mm.put("cName", c[1]);
            mm.put("date", c[2]);           
            mm.put("byFirstName", c[3]);
            mm.put("byLastName", c[4]);
            mm.put("type", c[5]);
            mm.put("flag", c[6]);

            results.add(mm);
        }

        model.put(RESULTS, results);
        return "admin/mngt/recentActivity/lastAddedOrganisations";
    }

    @RequestMapping(value = "/lastposts")
    public String getLastposts(
            Map<String, Object> model) throws IOException{
       // List<Object[]> lastposts=forumService.getlastpostss();
    	List<Object[]> lastposts=commentService.getlastpostss();
        if(lastposts!=null){
        	 List<Map<String, Object>> results = new ArrayList<>(); 
             for(Object[] c:lastposts) {
                 Map< String, Object> mm= new HashMap<>();
                 mm.put("id", c[0]);
                 mm.put("postedOn", c[1]);
                 mm.put(FIRSTNAME, c[2]);
                 mm.put("Lastname", c[3]);
                 mm.put("flag", c[4]);

                 results.add(mm);
             }
        	
            model.put(RESULTS, results);

        }
        return "admin/mngt/recentActivity/lastPostss";
    }


    @RequestMapping(value = "/CompleteOrgInfo")
    public String getOrg(
            Map<String, Object> model){
        List<CompanyMaster> companyList = companyService.findByActive(true);
        model.put(COMPANYLIST, companyList);
        return "admin/mngt/admindashboardmngt/companycompleteinfo";
    }

    @RequestMapping(value = "/CompleteCityInfo")
    public String getCityInfo(
            Map<String, Object> model){
        List<Object[]> cityList = companyService.getTopCities();

        List<Map<String, Object>> results = new ArrayList<>(); 
        for(Object[] c:cityList) {
            Map< String, Object> mm= new HashMap<>();
            mm.put(COMPANYID, c[0]);
            mm.put(CITYNAME, c[1]);
            results.add(mm);
        }

        model.put(RESULTS, results);
        return "admin/mngt/admindashboardmngt/cityCompleteInfo";
    }

    @RequestMapping(value = "/InactiveUserOrganisation")
    public String getInactiveUserOrganisation(
            Map<String, Object> model) throws IOException{

        List<Object[]> inactiveUserProfile=userService.inactiveUserProfile(30);
        List<Map<String, Object>> rs = new ArrayList<>(); 

        for(Object[] c:inactiveUserProfile) {
            Map< String, Object> mm= new HashMap<>();
            mm.put("userId", c[0]);
            mm.put(FIRSTNAME, c[1]);
            mm.put("lastLoginDate",c[2]);
            rs.add(mm);
        }
        model.put("rec", rs);
        return "admin/mngt/admindashboardmngt/userOrganisationstatus";
    }

    @RequestMapping(value = "/OrgByCity")
    public String getOrgByCity(
            Map<String, Object> model){
        List<Object[]> orgList = companyService.findOrgByCity();
        model.put("orgList", orgList);
        return "admin/mngt/admindashboardmngt/orgbycity";
    }


    @RequestMapping("/*/Language")
    public String showLanguage(
            @RequestParam(value = SUCCESS, required = false, defaultValue = "") String success,
            @RequestParam(value = ERROR, required = false, defaultValue = "") String error,
            Map<String, Object> model) {

        if (success != null && !success.isEmpty()) {
            model.put(SUCCESS, success);
        } else if (error != null && !error.isEmpty()) {
            model.put(ERROR, error);
        }
        List<LanguageMaster> languageList = languageService.findAllByActive();
        List<LanguageMaster> inActivelanguageList = languageService
                .findAllByInActive();
        model.put("languageList", languageList);
        model.put("inactivelanguageList", inActivelanguageList);
        return "admin/superadmin/masters/language";
    }

    @RequestMapping("/*/UserList")
    public String showUsers(
            @RequestParam(value = SUCCESS, required = false, defaultValue = "") String success,
            @RequestParam(value = ERROR, required = false, defaultValue = "") String error,
            Map<String, Object> model) {

        if (success != null && !success.isEmpty()) {
            model.put(SUCCESS, success);
        } else if (error != null && !error.isEmpty()) {
            model.put(ERROR, error);
        }
        List<User> activeUsers = userService.findByRoleAndStatus(Role.STUDENT,
                UserStatus.ACTIVE);
        List<User> inactiveUsers = userService.findByRoleAndStatus(
                Role.STUDENT, UserStatus.INACTIVE);
        model.put("activeUsers", activeUsers);
        model.put("inactiveUsers", inactiveUsers);
        return "admin/mngt/usermngt/usergrid";
    }

    @RequestMapping("/*/*/AddUser")
    public String addUser(
            @RequestParam(value = ERROR, required = false, defaultValue = "") String error,
            Map<String, Object> model) {
        if (error != null && !error.isEmpty()) {
            model.put(ERROR, error);
        }
        return "admin/mngt/usermngt/adduser";
    }

    @RequestMapping(value = "/{url1}/{url2}/RestoreUser")
    public String restoreUser(@PathVariable("url1") String url1,
            @PathVariable("url2") String url2,
            @RequestParam(value = "id") Long id, Map<String, Object> model)
                    throws NoSuchUserException {

        userService.activateUser(id);
        model.put(SUCCESS, "alert.userrestored");
        return REDIRECT + url1 + "/" + url2;
    }

    @RequestMapping("/{url1}/{url2}/DeleteUser")
    public String deleteUser(@RequestParam(value = "id") Long id,
            @PathVariable("url1") String url1,
            @PathVariable("url2") String url2, Map<String, Object> model)
                    throws NoSuchUserException {
        userService.inActivateUser(id);
        model.put(SUCCESS, "alert.userdeleted");
        return REDIRECT + url1 + "/" + url2;
    }

    @PreAuthorize("hasRole('SUPER_ADMIN')")
    @RequestMapping(value = "/survey/reset/{id}",method = RequestMethod.POST)
    public String resetSurvey(@PathVariable(value = "id") Long id,Map<String, Object> model) {
        try {
            surveyManagerService.resetSurveyBySurveyMangerId(id);
            model.put(SUCCESS, "alert.successsurveyreset");
        }catch (NoSuchSurveyException e){
            model.put(ERROR, "alert.error404surveyreset");
            LOGGER.info(e);
        }catch (Exception e){
            model.put(ERROR, "alerxt.error503surveyreset");
            LOGGER.info(e);
        }
        return "redirect:/ManageAdmin/OrganizationList";
    }

}
