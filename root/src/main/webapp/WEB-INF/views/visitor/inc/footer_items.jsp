<!-- <style>
	<link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
</style> -->

<nav class="footer-adjust navbar navbar-inverse navbar-fixed-bottom">
	
<!-- 	<div class="row">
		
			
				<div class="alert alert-danger alert-dismissible" role="alert" style="background-color:#222222;margin-bottom: 0px;padding-bottom: 0px;padding-top: 10px;">
					<marquee onMouseOver="this.stop()" onMouseOut="this.start()">
						<p style="font-family: initial; font-size: 14pt;color:#07A654 ">
							Atma Network presents FREE webinar on STRATEGY PLANNING - PART 2 on 21st September 2016 from 3 PM to 4 PM.
							 At the 2nd session of the webinar series you get to learn how to convert the long term strategies to specific 
							 action point and set up target plans.
							 Dont worry if you have missed the PART 1, we have a recap. <a href="https://goo.gl/rosZv5" target="_blank">REGISTER HERE</a>
						</p>
					</marquee>
				</div>
	</div> -->

	<div class="col-md-12">
		
		<ul class="list-inline">
			<li><a href="<%=contexturl%>footer/privacy"><h6>Privacy Policy</h6></a></li>
			<li><a href="http://atma.org.in/network/" target="_blank"><h6>About Network</h6></a></li>
			<li><a href="<%=contexturl%>footer/guides"><h6>How to guides</h6></a></li>
			<li><a href="<%=contexturl%>footer/contribute"><h6>Contribute to the Network</h6></a></li>
			<li><a href="<%=contexturl%>footer/faq"><h6>FAQ</h6></a></li>
		</ul>
		
	</div>
</nav>