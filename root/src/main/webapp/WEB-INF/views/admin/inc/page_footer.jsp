<%
/**
 * page_footer.jsp
 *
 * Author: pixelcave
 *
 * The footer of each page
 *
 */
%>
        <!-- Footer -->
        <footer class="clearfix">
            <div class="pull-right">
               <spring:message code="heading.technologypartner"/> : <a href="http://www.mindnerves.com" target="_blank"><spring:message code="heading.astrika"/></a>
            </div>
            <div class="pull-left">
                <div class="coipyrights">Copyright &copy; 2015</div>
            </div>
        </footer>
        <!-- END Footer -->
    </div>
    <!-- END Main Container -->
</div>
<!-- END Page Container -->

<!-- Scroll to top link, initialized in js/app.js - scrollToTop() -->
<a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>

<!-- User Settings, modal which opens from Settings link (found in top right user menu) and the Cog link (found in sidebar user info) -->



<div id="logout_popup" class="modal fade" tabindex="-1"
	role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<form action="" method="post" class="form-horizontal form-bordered"
					id="fg">
					<div style="padding: 10px; height: 110px;">
						<label><spring:message code="alert.logoutmsg"/></label>
						<div class="col-xs-12 text-right">
							<button type="button" class="btn btn-sm btn-default"
								data-dismiss="modal"><spring:message code="label.no"/></button>
							<a class="btn btn-sm btn-primary save" href="<c:url value="/j_spring_security_logout"/>"></i>
							<spring:message code="label.yes"/> </a>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<div id="underConst_popup" class="modal fade" tabindex="-1"
	role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content" style="width: 402px; height: 257px;">
			<div class="modal-body">
				<div style="margin-top: -20px; margin-left: -20px; height: 257px; width: 400px;">
					<a href="#" data-dismiss="modal" style="margin-top: -6px; position: fixed; margin-left: 385px;"
						class="btn btn-xs btn-danger"><i class="fa fa-times"></i></a>
					<img src="<%=contexturl %>resources/img/application/under_const.png" >
				</div>
			</div>
		</div>
	</div>
</div>

<div id="modal-user-settings" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title"><i class="fa fa-pencil"></i> <spring:message code="label.settings"/></h2>
            </div>
            <!-- END Modal Header -->
			 <!-- Message Header -->
			<span id="vitalInfoErrorMsg"></span>
			<span id="vitalInfoSuccessMsg"></span>
		
			 <!-- END Message Header -->
            <!-- Modal Body -->
            <div class="modal-body">
				<form action="" method="post" id="vitalInfo_form"	class="form-horizontal form-bordered" onsubmit="return false;">
                    <fieldset>
                        <legend><spring:message code="label.accountinfo"/></legend>
                        <div class="form-group">
                            <label class="col-md-4 control-label"><spring:message code="label.name"/></label>
                            <div class="col-md-8">
                                <p class="form-control-static"><%= user.getFullName()%></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label"><spring:message code="label.userlogin"/></label>
                            <div class="col-md-8">
                                <p class="form-control-static"><%= user.getLoginId()%></p>
                            </div>
                        </div>
                       	<div class="form-group">
							<label class="col-md-4 control-label" for="user-settings-email"><spring:message code="label.email"/></label>
							<div class="col-md-8">
								<input id="user_Id" name="userId" type="hidden" value=<%= user.getUserId()%>>
								<input type="email" id="user-settings-email"
									name="useremail" class="form-control"
									value=<%= user.getEmailId()%>>
							</div>
						</div>
						<div class="form-group">
							<a href="#" id="user-settings-reset-password" class="col-md-4 control-label"><spring:message code="label.resetpassword"/></a>
						</div>
                    </fieldset>
                    <fieldset id="user-settings-password_update" style="display:none;">
                        <legend><spring:message code="label.passwordupdate"/></legend>
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="user-settings-oldpassword"><spring:message code="label.oldpassword"/></label>
                            <div class="col-md-8">
                                <input type="password" id="user-settings-oldpassword" name="oldpassword" class="form-control" placeholder="<spring:message code="label.pleaseenteroldpassword"/>..">
                            </div>
                        </div>
                       <div class="form-group">
							<label class="col-md-4 control-label" for="user-settings-password"><spring:message code="label.newpassword"/></label>
							<div class="col-md-8">
								<input type="password" id="user-settings-password" name="newpassword" class="form-control" placeholder="<spring:message code="label.pleasechooseacomplexone"/>..">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label" for="user-settings-repassword"><spring:message code="label.confirmnewpassword"/></label>
							<div class="col-md-8">
								<input type="password" id="user-settings-repassword" name="repassword" class="form-control" placeholder="<spring:message code="label.confirmpassword"/>..">
							</div>
						</div>
                    </fieldset>
                    <div class="form-group form-actions">
                        <div class="col-xs-12 text-right">
                            <button type="button" class="btn btn-sm btn-default" data-dismiss="modal" onClick="window.location.reload()"><spring:message code="label.close"/></button>
							<div id="vitalInfo_submit" class="btn btn-sm btn-primary"><spring:message code="label.savechanges"/></div>
                        </div>
                    </div>
                </form>
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>
<!-- END User Settings -->
