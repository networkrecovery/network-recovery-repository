package com.astrika.common.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.astrika.common.util.PropsValues;

@Controller
@RequestMapping("/resource")
public class ResourceController {

	private Map<String,String> resourceMap = new HashMap<>();

	private static final Logger LOGGER = Logger.getLogger(ResourceController.class);

	@Autowired private PropsValues propsValues;

	@PostConstruct
	public void init(){
		String resourceCSV = propsValues.resourceCsv;
		String[] parts = resourceCSV.split(",");
		for(String part : parts){
			String[] kv = part.split("=");
			resourceMap.put(kv[0],kv[1]);
		}
	}

	@RequestMapping("/open/{name}")
	public void open(@PathVariable("name") String name,HttpServletRequest request,HttpServletResponse response) {
		if(!resourceMap.containsKey(name)){
			throw new IllegalArgumentException("Mapping failed for " + name);
		}
		prepareFile(resourceMap.get(name), request, response);
	}

	private void prepareFile(String name, HttpServletRequest request, HttpServletResponse response) {
		ServletContext context = request.getServletContext();

		File downloadFile = new File(propsValues.resourceBase + File.separatorChar + name);
		FileInputStream inputStream = null;
		OutputStream outStream = null;

		try {
			inputStream = new FileInputStream(downloadFile);

			response.setContentLength((int) downloadFile.length());
			response.setContentType(context.getMimeType(downloadFile.getAbsolutePath()));

			// response header
			String headerKey = "Content-Disposition";
			String headerValue = String.format("attachment; filename=\"%s\"",downloadFile.getName());
			response.setHeader(headerKey, headerValue);

			// Write response
			outStream = response.getOutputStream();
			IOUtils.copy(inputStream, outStream);

		} catch (Exception e) {
			throw new IllegalStateException(e);
		} finally {
			try {
				if (null != inputStream)
					inputStream.close();
				if (null != outStream)
					outStream.close();
			} catch (IOException e) {
				LOGGER.info(e);
			}

		}
	}


}
