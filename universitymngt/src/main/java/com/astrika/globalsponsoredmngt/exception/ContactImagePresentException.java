package com.astrika.globalsponsoredmngt.exception;

import com.astrika.kernel.exception.BusinessException;
import com.astrika.kernel.exception.CustomException;

public class ContactImagePresentException extends BusinessException{

	public ContactImagePresentException() {
		super(CustomException.CONTACT_IMAGE_PRESENT.getCode());
	}

	public ContactImagePresentException(String errorCode) {
		super(errorCode);
	}
	
	

}
