<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->


<%@include file="../inc/config.jsp"%>
<%@include file="../inc/template_start.jsp"%>
<%@include file="../companyadmin/page_head.jsp"%>
	

<!-- Page content -->
<div id="page-content">
    <!-- Dashboard Header -->
    <!-- For an image header add the class 'content-header-media' and an image as in the following example -->
    <div class="content-header content-header-media">
        <div class="header-section">
            <div class="row">
                <!-- Main Title (hidden on small devices for the statistics to fit) -->
                <div class="col-md-4 col-lg-6 hidden-xs hidden-sm">
                    <h1><strong>${company.companyName}</strong></h1>
                </div>
                <!-- END Main Title -->

            </div>
        </div>
        <!-- For best results use an image with a resolution of 2560x248 pixels (You can also use a blurred image with ratio 10:1 - eg: 1000x100 pixels - it will adjust and look great!) -->
        <img src="<%=contexturl %>resources/img/placeholders/headers/dashboard_header.jpg" alt="header image" class="animation-pulseSlow">
    </div>
    <!-- END Dashboard Header -->

 

<!--     Widgets Row -->
<%-- 	<c:choose> --%>
<%-- 		<c:when test="${done == true}"> --%>
<!-- 			<div class="row"> -->
<!-- 				<div class=""> -->
<!-- 					<div class="widget"> -->
<!-- 						<div class="widget-extra themed-background-dark"> -->
<!-- 							<h3 class="widget-content-light text-center"> -->
<!-- 								<strong> Thank you ! You have successufully completed -->
<!-- 									the survey</strong> -->
<!-- 							</h3> -->
<!-- 						</div> -->

<!-- 					</div> -->
<!-- 				</div> -->
<!-- 			</div> -->
<%-- 		</c:when> --%>
<%-- 		<c:otherwise> --%>
<!-- 			<div class="row"> -->
<!-- 				<div class=""> -->

<!-- 					START Top 5 Rated Restaurant Widget -->
<!-- 					<div class="widget"> -->

<!-- 						<div class="widget-extra themed-background-dark"> -->
<!-- 							<h3 class="widget-content-light text-center"> -->
<%-- 								<a href="<%=contexturl%>ManageSurvey/TakeSurvey"> <strong>Take --%>
<!-- 										Survey</strong></a> -->
<!-- 							</h3> -->
<%-- 						<c:if test = "${survey.surveySkipOption == true}"> --%>
<!-- 							<div class="btn-group"> -->
									
<!-- 										<a href="#delete_company_popup" data-toggle="modal" -->
<%-- 											onclick="deleteCompany(${survey.surveyManagerId})" --%>
<!-- 											title="delete Company" class="btn btn-sm btn-danger save"><i -->
<!-- 											class="fa fa-times"></i> Skip</a> -->
									

<!-- 								</div> -->
<%-- 						</c:if> --%>
<!-- 						</div> -->
<!-- 					</div> -->
<!-- 				</div> -->
<!-- 			</div> -->
<%-- 		</c:otherwise> --%>
<%-- 	</c:choose> --%>

	<div id="delete_company_popup" class="modal fade" tabindex="-1"
		role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<form action="<%=contexturl %>ManageSurvey/SkipSurvey" method="post" class="form-horizontal form-bordered save"
						id="delete_company_Form">
						<div style="padding: 10px; height: 110px;">
							<label><spring:message
									code="validation.doyouwanttoskipthissurvey" /></label>
							<div class="col-xs-12 text-right">
								<button type="button" class="btn btn-sm btn-default"
									data-dismiss="modal">
									<spring:message code="label.no" />
								</button>
								<div id="delete_company" class="btn btn-sm btn-primary">
									<spring:message code="label.yes" />
								</div>
							</div>
						</div>
						<input type="hidden" name="id" id="deletecompanyId">

					</form>

				</div>
			</div>
		</div>
	</div>


</div>
<!-- END Page Content -->
<%@include file="../inc/page_footer.jsp"%>

<%@include file="../inc/template_scripts.jsp"%>

<script type="text/javascript">
var selectedId = 0;
	$(document).ready(function() {
		
	
		
         $("#delete_company").click(function(){
 			$("#deletecompanyId").val(selectedId);
 			$("#delete_company_Form").submit();
 		});

	});
	
	
	function deleteCompany(id){
		selectedId = id;
	}

</script>

<!-- Google Maps API + Gmaps Plugin, must be loaded in the page you would like to use maps (Remove 'http:' if you have SSL) -->
<script src="http://maps.google.com/maps/api/js?sensor=true"></script>
<script src="<%=contexturl %>resources/js/helpers/gmaps.min.js"></script>

<!-- Load and execute javascript code used only in this page -->
<script src="<%=contexturl %>resources/js/pages/index.js"></script>
<script>$(function(){ Index.init(); });</script>

<%@include file="../inc/template_end.jsp"%>
