<%@page import="com.astrika.common.model.Role"%>
<%@page import="com.astrika.common.model.User"%>



<%@include file="../../inc/config.jsp"%>
<%@include file="../../inc/page_head.jsp"%>


  <div class="blue-container">
            <div class="container">
            <h3 class="atma-breadcrumbs">
			<security:authorize ifAnyGranted="<%=Role.COMPANY_ADMIN.name()%>">
				<a class="light a-breadcrumb" href="<%=contexturl%>CompanyAdmin">Dashboard</a>
			</security:authorize>
			<security:authorize ifAnyGranted="<%=Role.MODULE_USER.name()%>">
				<a class="light a-breadcrumb" href="<%=contexturl%>ModuleUser">Dashboard</a>
			</security:authorize>
			</h3>
            
            
                <div class="row blue">
                    <div class="col-md-10">
                        <div class="media">
                            <c:choose>
                        		<c:when test = "${(user.profileImage!=null) && (not empty user.profileImage)}">
                        			<a class="pull-left" href="#"><img class="media-object pro-pic" height = 90px width=75px src="<%=contexturl %>${user.profileImage.imagePath}" alt="<%=contexturl %>resources/img/User_Thumbs.png" title="Click here to change photo" onclick="OpenFileDialog();">
                            		</a>
                        		</c:when>
                        		<c:otherwise>
                        			<a class="pull-left" href="#"><img class="media-object pro-pic" src="<%=contexturl %>resources/img/User_Thumbs.png" alt="Organisation Name" title="Click here to change photo" onclick="OpenFileDialog();">
                            		</a>
                        		</c:otherwise>
                        	</c:choose>
							<form action="<%=contexturl%>ChangePhoto" id="changePhoto"
								method="post" enctype="multipart/form-data" class="save">
								<input type="file" id="imageChooser" style="display: none;"
									onchange="changePhoto();" name="imageChooser"
									accept="image/jpg,image/jpeg,image/png,image/gif,image/bmp,image/tiff"
									class="save" />
							</form>
                            <div class="media-body text-st">
                                <p>Edit Profile here</p>
                                <h4 class="media-heading">User Profile</h4>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
<div class="container">
	<div class="row"><div class="col-md-12">
		<c:if test="${!empty success}">
			<div class="alert alert-success alert-dismissable">
				<button type="button" class="close" data-dismiss="alert"
					aria-hidden="true">x</button>
				<h4>
					<i class="fa fa-check-circle"></i>
					<spring:message code="label.success" />
				</h4>
				<spring:message code="${success}" />
			</div>
		</c:if>
		<div class="alert alert-danger alert-dismissable"
			style="display: none" id="error">
			<button type="button" class="close" onclick="closeDiv();"
				aria-hidden="true">x</button>
			<h4>
				<i class="fa fa-times-circle"></i>
				<spring:message code="label.error" />
			</h4>
			<span class="errorMsg"></span>
		</div>
	</div>
	</div>
 <br>

	<div class="row">
		<div class="col-md-2"></div>
		<div class="col-md-8">
			<form action="<%=contexturl%>SavePersonalDetails" method="post"
				class="form-horizontal " id="personalDetails_form"
				enctype="multipart/form-data">
				<input name="userId" value="${user.userId}" type="hidden">
				<div class="block full">
					<div class="block-title">
						<h4 class="">Personal Details</h4>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">First Name<span
							class="mandatory">*</span>
						</label>
						<div class="col-sm-8">
							<input type="text" id="first_Name" name="firstName"
								class="form-control" placeholder="First Name *"
								value="${user.firstName}">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">Last Name <span
							class="mandatory">*</span>
						</label>
						<div class="col-sm-8">
							<input type="text" id="last_Name" name="lastName"
								class="form-control" placeholder="Last Name *"
								value="${user.lastName}">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">E-mail Id<span
							class="mandatory">*</span>
						</label>
						<div class="col-sm-8">
							<input type="email" id="email" name="emailId"
								class="form-control" placeholder="test@example.com*"
								value="${user.emailId}" disabled="disabled">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">Mobile</label>
						<div class="col-md-8">
							<input id="mobile" name="mobile" class="form-control"
								placeholder="Mobile no" type="text" value="${user.mobile}">
						</div>
					</div>
					<div class="form-group form-inline">
						<label class="col-sm-3 control-label">Gender<span
							class="mandatory">*</span>
						</label>
						<div class="col-sm-8">
							<c:choose>
								<c:when
									test="${(user.gender eq 'Male') || (user.gender eq 'Female')}">
									<label class="radio"> <input id="genderMale"
										name="gender" class="usergender" type="radio" value="Male"
										<c:if test="${user.gender eq 'Male'}"> checked = "checked"</c:if>><span
										class="input-text">Male</span>
									</label>
									<label class="radio"> <input id="genderFemale"
										name="gender" class="usergender" type="radio" value="Female"
										<c:if test="${user.gender eq 'Female'}"> checked = "checked"</c:if>><span
										class="input-text">Female</span>
									</label>
								</c:when>
								<c:otherwise>
									<label class="radio"> <input id="genderMale"
										name="gender" class="usergender" type="radio" value="Male"><span
										class="input-text">Male</span>
									</label>
									<label class="radio"> <input id="genderFemale"
										name="gender" class="usergender" type="radio" value="Female"
										checked="checked"><span class="input-text">Female</span>
									</label>
								</c:otherwise>
							</c:choose>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">Designation <span
							class="mandatory">*</span>
						</label>
						<div class="col-sm-8">
							<input id="designation" name="designation" type="text"
								class="form-control" placeholder="Designation"
								value="${user.designation}">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">LinkedIn Profile </label>
						<div class="col-sm-8">
							<input id="linkedinUrl" name="linkedinUrl" type="text"
								class="form-control" placeholder="LinkedIn URL"
								value="${personalInfo.linkedin}">
						</div>
					</div>
					<div class="form-group" id="eduDiv">
						<label class="col-sm-3 control-label">Educational
							Background<span class="mandatory">*</span>
						</label>
						<div class="col-sm-8">
							<select class=" form-control" id="eduBackground"
								name="eduBackground">
								<option value=""></option>
								<c:forEach items="${educationalList}" var="eduList">
									<option value="${eduList}"
										<c:if test="${eduList eq personalInfo.educationalBackground}">selected="selected"</c:if>>${eduList}</option>
								</c:forEach>
							</select>
						</div>
					</div>
					<div class="form-group" id="workDiv">
						<label class="col-sm-3 control-label">Work Experience<span
							class="mandatory">*</span>
						</label>
						<div class="col-sm-8" id="experiencediv">
							<select class=" form-control subfunctionChosen"
								id="workExperience" name="workExperience">
								<option value=""></option>
								<c:forEach items="${experienceList}" var="experience">
									<option value="${experience}"
										<c:if test="${experience eq personalInfo.workExperience}">selected="selected"</c:if>>${experience}</option>
								</c:forEach>
							</select>
						</div>
					</div>
					<div class="form-group form-inline">
						<label class="col-sm-3 control-label">Functional areas<span
							class="mandatory">*</span>
						</label>
						<div class="col-sm-8">
							<c:choose>
								<c:when test="${not empty funcAreaList}">
									<c:forEach items="${functionalAreasList}" var="funcArea"
										varStatus="counter">

										<c:forEach items="${funcAreaList}" var="funcList"
											varStatus="counter">
											<c:if test="${funcList eq funcArea}">
												<c:set var="funcCheck" value="true"></c:set>
											</c:if>
										</c:forEach>
										
										<label class="checkbox"> <c:choose>
												<c:when test="${funcCheck eq 'true'}">
													<input id="funcArea_${counter.count}" type="checkbox"
														name="functionalArea" value="${funcArea}"
														checked="checked" />
													<span class="input-text">${funcArea}</span>
												</c:when>
												<c:otherwise>
													<c:choose>
														<c:when test="${funcArea eq 'Other'}">
															<input id="funcArea_${counter.count}" type="checkbox"
																name="functionalArea" value="${funcArea}" class="Other" />
															<span class="input-text">${funcArea}</span>
														</c:when>
														<c:otherwise>
															<input id="funcArea_${counter.count}" type="checkbox"
																name="functionalArea" value="${funcArea}" />
															<span class="input-text">${funcArea}</span>
														</c:otherwise>
													</c:choose>												
<%-- 													<input id="funcArea_${counter.count}" type="checkbox" --%>
<%-- 														name="functionalArea" value="${funcArea}" /> --%>
<%-- 													<span class="input-text">${funcArea}</span> --%>
												</c:otherwise>
											</c:choose>
										</label>
										<c:set var="funcCheck" value="false"></c:set>
										
									</c:forEach>
								</c:when>
								<c:otherwise>
									<c:forEach items="${functionalAreasList}" var="funcArea"
										varStatus="counter">
										<label class="checkbox"> <c:choose>
												<c:when test="${funcArea eq 'Other'}">
													<input id="funcArea_${counter.count}" type="checkbox"
														name="functionalArea" value="${funcArea}" class="Other" />
													<span class="input-text">${funcArea}</span>
												</c:when>
												<c:otherwise>
													<input id="funcArea_${counter.count}" type="checkbox"
														name="functionalArea" value="${funcArea}" />
													<span class="input-text">${funcArea}</span>
												</c:otherwise>
											</c:choose>
										</label>
									</c:forEach>
								</c:otherwise>
							</c:choose>
						</div>
					</div>
					<div class="" id="selectedDiv1"></div>
					<c:if test="${not empty personalInfo.funcAreaReason}">
						<div class="form-group" id="funcAreaDiv">
							<label class="col-sm-3 control-label">Mention the other
								Functional Area<span class="mandatory">*</span>
							</label>
							<div class="col-sm-8">
								<input id="funcAreaOther" name="funcAreaOther" type="text"
									class="form-control" placeholder="Other"
									value="${personalInfo.funcAreaReason}">
							</div>
						</div>
					</c:if>
					<div class="form-group" id="networkDiv">
						<label class="col-sm-3 control-label">How did you learn
							about Atma Network?<span class="mandatory">*</span>
						</label>
						<div class="col-sm-8">
							<select class=" form-control" id="aboutNetwork"
								name="aboutNetwork">
								<option value=""></option>
								<c:forEach items="${aboutUsList}" var="aboutUs">
									<option value="${aboutUs}"
										<c:if test="${aboutUs eq personalInfo.aboutNetwork}">selected="selected"</c:if>>${aboutUs}</option>
								</c:forEach>
							</select>
						</div>
					</div>
					<div class="" id="selectedDiv2"></div>
					<c:if test="${not empty personalInfo.networkReason}">
						<div class="form-group" id="netDiv">
							<label class="col-sm-3 control-label">Mention the other
								way through which you heard about Atma Network?<span
								class="mandatory">*</span>
							</label>
							<div class="col-sm-8">
								<input id="networkOther" name="networkOther" type="text"
									class="form-control" placeholder="Other"
									value="${personalInfo.networkReason}">
							</div>
						</div>
					</c:if>
				</div>
			</form>
		</div>
		<!-- END Personal Details  -->
		<div class="col-md-2"></div>
	</div>
	<div class="row">
		<div class="text-center">
			<button id="personalDetailsSubmit" type="submit"
				class="btn btn-warning">Submit</button>
			<security:authorize ifAnyGranted="<%=Role.COMPANY_ADMIN.name()%>">
				<button type="button" class="btn btn-primary"
					onclick="location.href='<%=contexturl%>CompanyAdmin'">Back
				</button>
			</security:authorize>
			<security:authorize ifAnyGranted="<%=Role.MODULE_USER.name()%>">
				<button type="button" class="btn btn-primary"
					onclick="location.href='<%=contexturl%>ModuleUser'">Back
				</button>
			</security:authorize>
		</div>
	</div>
	<br>
</div>

<div id="tempfuncAreaDiv" style="display: none">
	<div class="form-group" id="funcAreaDiv">
		 <label class="col-sm-3 control-label">Mention the other Functional Area<span class="mandatory">*</span>
      	</label>
      	<div class="col-sm-8">
      		<input id="funcAreaOther" name="funcAreaOther" type="text" class="form-control" placeholder="Other" value="${personalInfo.funcAreaReason}">
      	</div>
	</div>	 
</div>

<div id="tempNetworkDiv" style="display: none">
	 <div class="form-group" id="netDiv">
		 <label class="col-sm-3 control-label">Mention the other way through which you heard about Atma Network?<span class="mandatory">*</span>
      	</label>
      	<div class="col-sm-8">
      		<input id="networkOther" name="networkOther" type="text" class="form-control" placeholder="Other" value="${personalInfo.networkReason}">
      	</div>
	</div>	
	 
</div>
    
<%-- <script src="<%=contexturl%>resources/js/vendor/jquery-1.11.0.min.js"></script> --%>
<%-- <script src="<%=contexturl%>resources/js/vendor/bootstrap.min.js"></script> --%>
<%-- <script src="<%=contexturl%>resources/js/vendor/validate.js"></script> --%>
<script src="${pageContext.request.contextPath}/webjars/counter-webapp-frontend/0.0.1/vendor_bundle.js"></script>

<%-- <script src="<%=contexturl%>resources/js/additional-methods.js"></script> --%>
<script src="${pageContext.request.contextPath}/webjars/counter-webapp-frontend/0.0.1/app_bundle.js"></script>

 <script type="text/javascript">
		$(document)
				.ready(
						function() {

							var funcDiv = $("#tempfuncAreaDiv").contents();
							var netDiv = $("#tempNetworkDiv").contents();

							$("input[name='functionalArea']").click(
									function() {
										if ($("input[class='Other']:checked")
												.val() == "Other") {
											$('#selectedDiv1').empty();
											$('#selectedDiv1').append(funcDiv);
										} else {
											$('#selectedDiv1').empty();
										}
									});

							$("#aboutNetwork").change(function() {
								if ($(this).val() == "Other") {
									$('#selectedDiv2').empty();
									$('#selectedDiv2').append(netDiv);
								} else {
									$('#selectedDiv2').empty();
								}
							});

							$("#personalDetails_form")
									.validate(
											{
												errorClass : "help-block animation-slideDown",
												errorElement : "div",
												errorPlacement : function(e, a) {
													a
															.parents(
																	".form-group > div")
															.append(e)
												},
												highlight : function(e) {
													$(e)
															.closest(
																	".form-group")
															.removeClass(
																	"has-success has-error")
															.addClass(
																	"has-error")
												},
												success : function(e) {
													e
															.closest(
																	".form-group")
															.removeClass(
																	"has-success has-error")
												},
												rules : {
													firstName : {
														required : !0,
														maxlength : 75,
														NameValidate : true,
													},
													lastName : {
														required : !0,
														maxlength : 75,
														NameValidate : true,
													},
													emailId : {
														required : !0,
														maxlength : 75,
														email : !0,
													},
													mobile : {
														required : !0,
														mobile : !0,
													},
													designation : {
														required : !0,
														maxlength : 75,
													},
													linkedinUrl : {
														urlTwitterTrim : true,
													},
													eduBackground : {
														required : !0
													},
													functionalArea : {
														required : !0
													},
													funcAreaOther : {
														required : !0,
														maxlength : 100
													},
													workExperience : {
														required : !0
													},
													aboutNetwork : {
														required : !0
													},
													networkOther : {
														required : !0,
														maxlength : 100
													}
												},
												messages : {
													firstName : {
														required : '<spring:message code="validation.pleaseenterfirstname"/>',
														maxlength : '<spring:message code="validation.firstname75character"/>',
														NameValidate : 'Please use only alphabetic characters',
													},
													lastName : {
														required : '<spring:message code="validation.pleaseenterlastname"/>',
														maxlength : '<spring:message code="validation.lastname75character"/>',
														NameValidate : 'Please use only alphabetic characters',
													},
													emailId : {
														required : '<spring:message code="validation.pleaseenteremailid"/>',
														email : '<spring:message code="validation.pleaseenteravalidemailid"/>',
														maxlength : '<spring:message code="validation.email75character"/>',
													},
													mobile : {
														required : '<spring:message code="validation.pleaseenteramobilenumber"/>',
														mobile : '<spring:message code="validation.mobile"/>',
														maxlength : '<spring:message code="validation.mobile10character"/>',
													},
													designation : {
														required : 'Please enter your designation',
														maxlength : 'Designation should not be more than 75 characters'
													},
													linkedinUrl : {
														urlTwitterTrim : 'Please enter a valid URL'
													},
													eduBackground : {
														required : 'Please select an Educational Qualification'
													},
													functionalArea : {
														required : 'Please select atleast one checkbox'
													},
													funcAreaOther : {
														required : 'Please mention the other functional areas',
														maxlength : 'Only 100 characters allowed'
													},
													workExperience : {
														required : 'Please select your years of Expeirence'
													},
													aboutNetwork : {
														required : 'Please select a network through which you came to know about us'
													},
													networkOther : {
														required : 'Please mention the other way through which you came to know about the network',
														maxlength : 'Only 100 characters allowed'
													}
												},
											});

							jQuery.validator.addMethod("NameValidate",
									function(value, element) {
										var regex = new RegExp("^[a-zA-Z ]*$");
										var key = value;

										if (!regex.test(key)) {
											return false;
										}
										return true;
									}, "please use only alphabetic characters");

							jQuery.validator.addMethod("phone",
									function(phone_number, element) {
										phone_number = phone_number.replace(
												/\s+/g, "");
										return this.optional(element)
												|| phone_number.length >= 8
												&& phone_number.length < 25
												&& phone_number
														.match(/^[0-9\+\-]*$/);
									}, "Please specify a valid phone number");

							jQuery.validator.addMethod("mobile", function(
									mobile_number, element) {
								mobile_number = mobile_number.replace(/\s+/g,
										"");
								if (mobile_number.match(/^[7-9][0-9]{9}$/))
									return true;
								//return this.optional(element) || mobile_number.length >= 10 && mobile_number.length < 25 && mobile_number.match(/^[0-9\+\-]*$/);
							}, "Please specify a valid mobile number");

							jQuery.validator.addMethod("pincode", function(
									pincode, element) {
								pincode = pincode.replace(/\s+/g, "");
								return this.optional(element)
										|| pincode.length >= 4
										&& pincode.length <= 10;
							}, "Please specify a valid pincode");

							jQuery.validator
									.addMethod(
											"urlTwitterTrim",
											function(urlTwitterTrim, element) {
												var urlregex = new RegExp(
														"^(http:\/\/|https:\/\/|ftp:\/\/){1}([0-9A-Za-z]+\.)");

												return urlregex
														.test(urlTwitterTrim
																.trim())
														|| urlTwitterTrim.length == 0;
											}, "Invalid url");
							

							
							

							$("#personalDetailsSubmit").click(function() {
								unsaved = false;
								$("#personalDetails_form").submit();
							});

						});
	</script>
<script>
function OpenFileDialog(){
	document.getElementById("imageChooser").click();
}

function changePhoto(){
	if($('#imageChooser').val() != ""){			
		var imageName = document.getElementById("imageChooser").value;
		var ext1 = imageName.substring(imageName.lastIndexOf("."));
		var ext = ext1.toLowerCase();
		if(ext == '.jpg' || ext == '.jpeg' || ext == '.png' || ext == 'gif' || ext == 'bmp' || ext == 'tiff')
			$('#changePhoto').submit();		
		else
			alert("Please select only image file");		
	}
}

</script>
 <%@include file="../../inc/page_footer.jsp"%>