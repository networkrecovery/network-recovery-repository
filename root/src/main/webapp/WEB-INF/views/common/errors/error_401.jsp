<%@include file="../../admin/inc/config.jsp"%>
<%@include file="../../admin/inc/template_start.jsp"%>
<!-- Error Container -->
<div id="error-container">
    <div class="error-options">
        <h3><i class="fa fa-chevron-circle-left text-muted"></i> <a href="<%=contexturl %>Index">Go Back</a></h3>
    </div>
    <div class="row">
        <div class="col-sm-8 col-sm-offset-2 text-center">
            <h1 class="animation-fadeIn"><i class="fa fa-times-circle-o text-muted"></i> 401</h1>
            <h2 class="h3">Oops, we are sorry but you are not authorized to access this page..</h2>
        </div>

    </div>
</div>
<!-- END Error Container -->

<%@include file="../../admin/inc/template_end.jsp"%>