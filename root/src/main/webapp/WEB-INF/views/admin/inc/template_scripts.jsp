<%
/**
 * template_scripts.jsp
 *
 * Author: pixelcave
 *
 * All vital JS scripts are included here
 *
 */
%>

<!-- Include Jquery library from Google's CDN but if something goes wrong get Jquery from local file (Remove 'http:' if you have SSL) -->
<script src="<%=contexturl %>resources/js/vendor/jquery-1.11.0.min.js"></script>
<script>!window.jQuery && document.write(unescape('%3Cscript src="<%=contexturl %>resources/js/vendor/jquery-1.11.0.min.js"%3E%3C/script%3E'));</script>

<!-- Bootstrap.js, Jquery plugins and Custom JS code -->
<script src="<%=contexturl %>resources/js/vendor/bootstrap.min.js"></script>
<script src="<%=contexturl %>resources/js/plugins.js"></script>
<script src="<%=contexturl %>resources/js/app.js"></script>
<script src="<%=contexturl %>resources/js/vendor/jquery.raty.min.js"></script>	
<script src="<%=contexturl %>resources/js/jquery.autoellipsis-1.0.10.min.js"></script>
<script type="text/javascript">
var unsaved = false;
var pathname = window.location.pathname;
var path = pathname.substring(pathname.indexOf("<%=contexturl%>"));

// $("#moduleList").on("click","li", function(){
// 	try{
// 		if(path.indexOf("ManageSurvey") >= 0){
// 			$("#surveyBuilder").addClass("active");
// 			if(path.indexOf("CreateSurvey") > 0){	
// 				$("#moduleList").addClass("active");
// 				if(path.indexOf("1") > 0){
// 					$("#surveyList_1").addClass("active");
// 				}
// 				else if(path.indexOf("2") > 0){			
// 					$("#surveyList_2").addClass("active");
// 				}
// 			}
// 		}
// 	}
// 	catch(e){
		
// 	}
// });

$(document).ready(function() {
	
	// Menu Selection
	
	try{
		$("a").removeClass("active");$("li").removeClass("active");
		
		if(path.indexOf("ManageAdmin") >= 0){

			if(path.indexOf("AdminList") > 0){
				$("#manageAdmin").addClass("active");
				$("#adminList").addClass("active");
			}else if(path.indexOf("OrganizationList") > 0){
				$("#manageOrg").addClass("active");
				$("#organizationList").addClass("active");
			}
		}
		
		else if(path.indexOf("ManageModule") >= 0){
			$("#manageModule").addClass("active");
			if(path.indexOf("ModuleList") > 0){
				$("#moduleList").addClass("active");
			}
		}
		
		else if(path.indexOf("ManageCategory") >= 0){
			$("#manageCategory").addClass("active");
			if(path.indexOf("CategoryList") > 0){
				$("#categoryList").addClass("active");
			}
		}
		
		else if(path.indexOf("ManageDocument") >= 0){
			$("#manageDocument").addClass("active");
			if(path.indexOf("DocumentList") > 0){
				$("#documentList").addClass("active");
			}
		}
		
		else if(path.indexOf("ManageSponsoredImage") >= 0){
			$("#manageSponsoredImage").addClass("active");
			if(path.indexOf("SponsoredImageList") > 0){
				$("#sponsoredImageList").addClass("active");
			}
		}
		
		else if(path.indexOf("ManageGlobalSponsoredImage") >= 0){
			$("#manageGlobalSponsoredImage").addClass("active");
			if(path.indexOf("GlobalSponsoredImageList") > 0){
				$("#globalSponsoredImageList").addClass("active");
			}
		}
		
		else if(path.indexOf("ManageEnquiry") >= 0){
			$("#manageEnquiry").addClass("active");
			if(path.indexOf("ListEnquiry") > 0){
				$("#enquiryList").addClass("active");
			}
		}
		
		else if(path.indexOf("ManageEnquiry") >= 0){
			$("#manageEnquiry").addClass("active");
			if(path.indexOf("ListEnquiry") > 0){
				$("#enquiryList").addClass("active");
			}
		}
		
		
		else if(path.indexOf("ManageResultData") >= 0){
			$("#manageResultData").addClass("active");
			if(path.indexOf("ResultDataList") > 0){
				$("#resultList").addClass("active");
			}
		}
		
		else if(path.indexOf("ManageSurvey") >= 0){
			$("#surveyBuilder").addClass("active");		
				<c:forEach var="module1" items="${mList}" varStatus="check2">
					if(path.indexOf("CreateSurvey/${module1.moduleId}") > 0){
						$("#surveyList_${module1.moduleId}").addClass("active");
					}
				</c:forEach>
				if(path.indexOf("CreateSurvey/SaveSurvey") > 0){
					$("#surveyList_${moduleId}").addClass("active");
				}
		}
	
		

		else if(path.indexOf("ManageLocation") >= 0){
			$("#manageLocation").addClass("active");
			if(path.indexOf("Country") > 0){
				$("#country").addClass("active");
			}
			if(path.indexOf("State/AddState") > 0){
				$("#state").addClass("active");
			}
		}
		
		else if(path.indexOf("Manage/StateList") >= 0){
			$("#state").addClass("active");	
		}
		else if(path.indexOf("ManageState/StateList") >= 0){
			$("#state").addClass("active");	
		}
		
		else if(path.indexOf("ManageCity/City") >= 0){
			$("#city").addClass("active");	
		}
		
		else if(path.indexOf("ManageNextSteps") >= 0){
			$("#manageNextSteps").addClass("active");		
				<c:forEach var="module1" items="${moList}" varStatus="check2">
					if(path.indexOf("CreateNextSteps/${module.moduleId}") > 0){
						$("#nextStepsList_${module.moduleId}").addClass("active");
					}
				</c:forEach>
		}
		
		
		else{
			$("#dashboard").addClass("active");
		}
	}
	catch(e){
		
	}
	
	
	
	
	
	
	 jQuery.validator.addMethod("filesize", function(value, element, param) {
		    // param = size (en bytes) 
		    // element = element to validate (<input>)
		    // value = value of the element (file name)
		    return this.optional(element) || (element.files[0].size <= param) 
		}, "Size of the image is too large.");
	
	
 
	 jQuery.validator.addMethod("phone", function (phone_number, element) {
	        phone_number = phone_number.replace(/\s+/g, "");
	        return this.optional(element) || phone_number.length >= 8 && phone_number.length < 25 && phone_number.match(/^[0-9\+\-]*$/);
	    }, "Please specify a valid phone number");
	    
	    jQuery.validator.addMethod("mobile", function (mobile_number, element) {
	    	mobile_number = mobile_number.replace(/\s+/g, "");
	        return this.optional(element) || mobile_number.length >= 10 && mobile_number.length < 25 && mobile_number.match(/^[0-9\+\-]*$/);
	    }, "Please specify a valid mobile number");
	    
	    jQuery.validator.addMethod("pincode", function (pincode, element) {
	    	pincode = pincode.replace(/\s+/g, "");
	        return this.optional(element) || pincode.length >=4 && pincode.length <= 10;
	    }, "Please specify a valid pincode");
	    
	    
	    
	    jQuery.validator.addMethod("greaterThan", 
	    		function(value, element, params) {
                          return  (Number(value) > Number($(params).val())); 
	    		},'Must be greater than {0}.');
	    
	    jQuery.validator.addMethod("discount", function (number, element) {
	        return  number <100 ;
	    }, "Please specify a valid discount ");
	    
	    jQuery.validator.addMethod("urlTrim", function (urlTrim, element) {
	    	var urlregex = new RegExp("^(http:\/\/www.|https:\/\/www.|ftp:\/\/www.|www.){1}([0-9A-Za-z]+\.)");
	   
	     return urlregex.test(urlTrim.trim()) || urlTrim.length == 0;
	    }, "Please Invalid url");
	    
	    
	    jQuery.validator.addMethod("maxno", function (number, element) {
	        number =number.replace(/\s+/g, "");
	        return this.optional(element) || number <13 ;
	    }, "Please specify a valid month ");
	    
	    jQuery.validator.addMethod("minno", function (number, element) {
	        number =number.replace(/\s+/g, "");
	        return this.optional(element) || number >13 ;
	    }, "Please specify a valid month ");
	    
	    jQuery.validator.addMethod("domain",function(domains,element)
	    		{
	    	 var regex = /^\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	    	 var flag=false;
	    		  var split = domains.split(',');
	    		  
	    		  for(var i = 0; i < split.length; ++i) {
	    			  flag= split[i] != '' && regex.test(split[i]);
	    		  }
	    		  return flag;
		}, "Email id already assigned");
	    
	    jQuery.validator.addMethod("hash", function (nameTrim, element) {
	     return (nameTrim.indexOf('#') == -1)
	    }, "Please Invalid Login Id");
	    
	    
	$.validator.setDefaults({ ignore: ":hidden:not(select)" })

	
	

	 
	
	
	
	
	$("#country").click(function(){
		$("a").removeClass("active");$("li").removeClass("active");
		$("#country").addClass("active");
		$.ajax({
			type: "GET",
			async: false,
			url: "<%=contexturl %>Admin/Country",				
			success: function(data, textStatus, jqXHR){
				$('#page-content').html(data);
			},
			dataType: 'html'
		});
	});
	
	$("#currency").click(function(){
		$("a").removeClass("active");$("li").removeClass("active");
		$("#currency").addClass("active");
		$.ajax({
			type: "GET",
			async: false,
			url: "<%=contexturl %>Admin/Currency",				
			success: function(data, textStatus, jqXHR){
				$('#page-content').html(data);
			},
			dataType: 'html'
		});
	});
	
	
	
	$("#language").click(function(){
		$("a").removeClass("active");$("li").removeClass("active");
		$("#language").addClass("active");
		$.ajax({
			type: "GET",
			async: false,
			url: "<%=contexturl %>Admin/Language",				
			success: function(data, textStatus, jqXHR){
				$('#page-content').html(data);
			},
			dataType: 'html'
		});
	});
	

	$("#user-settings-reset-password").click(function(){
		
		$("#user-settings-password_update").show();
		$("#user-settings-oldpassword").rules( "add", {
			  required:!0,
			  messages: {
			    required: "Please enter old password"
			  }
			});
		
		$("#user-settings-password").rules( "add", {
			  required:!0,
			  minlength : 6,
			  messages: {
				  required : "Please enter a new password",
				  minlength : "Password must be atleast 6 character"
			  }
			});
		
		
		$("#user-settings-repassword").rules( "add", {
			  required:true,
			  equalTo : "#user-settings-password",
			  messages: {
				  required : "Please enter a new password",
				  equalTo : "Please enter the same password as above"
			  }
			});
		
		

		
	});

});
</script>

<script type="text/javascript">
	$(document).ready(function() {
		
		$("#vitalInfo_submit").click(function(){
			$("#vitalInfo_submit").submit();
		});
		
		$("#vitalInfo_form").validate(
				{	errorClass:"help-block animation-slideDown",
					errorElement:"div",
					errorPlacement:function(e,a){a.parents(".form-group > div").append(e)},
					highlight:function(e){$(e).closest(".form-group").removeClass("has-success has-error").addClass("has-error")},
					success:function(e){e.closest(".form-group").removeClass("has-success has-error")},
					rules:{ 
						useremail:{
							required:!0,
							email:!0
						}					
					},
					messages:{
						useremail : {
							required : "Please enter an emailId",
							email : "Please enter a valid emailId"
						}
					},
					submitHandler: function() {
						var data = $("#vitalInfo_form").serialize();
						$.ajax({
							type: "POST",
							async: false,
							url: "<%=contexturl%>Admin/SaveAccountInfo",
							data:data,
							success: function(data, textStatus, jqXHR){
								var i = data.trim().indexOf("errorCode");
								if(i == 2){
									var obj = $.parseJSON(data.trim());
									var html = '<div class="alert alert-danger alert-dismissable">	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="fa fa-times-circle"></i> Error</h4> <span >'+obj.errorCode+'</span></div>';
									$("#vitalInfoErrorMsg").html(html).show();
									$("#vitalInfoErrorMsg").html(html).fadeIn();
									$("#vitalInfoErrorMsg").html(html).fadeOut(8000);
								}
								else{
									var obj = $.parseJSON(data.trim());
									var html = '<div class="alert alert-success alert-dismissable">	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="fa fa-times-circle"></i> Success</h4> <span >'+obj.success+'</span></div>';
									$("#vitalInfoSuccessMsg").html(html).show();
									$("#vitalInfoSuccessMsg").html(html).fadeIn();
									$("#vitalInfoSuccessMsg").html(html).fadeOut(8000);
								}
							},
							dataType: 'html'
						});
	                }
				});	
		
	});
	
</script>
<script type="text/javascript">
	function OpenFileDialog(){
		document.getElementById("imageChooser").click();
	}
	
	function changePhoto(){
		if($('#imageChooser').val() != ""){			
			var imageName = document.getElementById("imageChooser").value;
			var ext1 = imageName.substring(imageName.lastIndexOf("."));
			var ext = ext1.toLowerCase();
			if(ext == '.jpg' || ext == '.jpeg' || ext == '.png' || ext == 'gif' || ext == 'bmp' || ext == 'tiff')
				$('#changePhoto').submit();		
			else
				alert("Please select only image file");		
		}
	}
	
</script>

<script type="text/javascript">
	var unsaved = false;
	$(function(){

		$(".save").click(function(){ //trigers change in all input fields including text type
		     unsaved = false;
		 });
		
// 		 $(":input").change(function(){ //trigers change in all input fields including text type
// 		     unsaved = true;
// 		 });
		 
		 $(":input").change(function(){ //trigers change in all input fields including text type
			   	if(this.id!="imageChooser")
				unsaved = true;
			});

		 function unloadPage(){ 
		     if(unsaved){
		         return "You have unsaved changes on this page. Do you want to leave this page and discard your changes or stay on this page?";
		     }
		 }

		 window.onbeforeunload = unloadPage;
	});
	 function getModule(moduleId){
 		$.ajax({
 			type: "GET",
 			async: false,
 			url:"<%=contexturl%>ManageModule/ListModule",
 			success: function(data, textStatus, jqXHR){
 				var obj = jQuery.parseJSON(data);
 				var len=obj.length;
 				var myList = "<li></li>";
 				for(var i=0; i<len; i++){
 					var	label = "";
 					if( obj[i].completed){
 						myList += '<li><a class="col-md-10" href="<%=contexturl %>ManageSurvey/CreateSurvey/'+obj[i].moduleId+'" id="surveyList_'+obj[i].moduleId+'" class="save">'+obj[i].moduleName +' </a><span class="fa fa-check col-md-2" style="margin-top: 10px;"><i></i></span></li>';
 					}
 					else{
 						myList += '<li><a href="<%=contexturl %>ManageSurvey/CreateSurvey/'+obj[i].moduleId+'" id="surveyList_'+obj[i].moduleId+'" class="save">'+obj[i].moduleName +'  </a></li>';
 					}
 				}
 				$("#moduleList").html(myList);
 				
 			},
 			dataType: 'html'
 		});
     }
     
     function getModuleforNextSteps(moduleId){
         $.ajax({
          type: "GET",
          async: false,
          url:"<%=contexturl%>ManageModule/ListModule",
          success: function(data, textStatus, jqXHR){
           var obj = jQuery.parseJSON(data);
           var len=obj.length;
           var myModuleList = "<li></li>";
           for(var i=0; i<len; i++){
        	   var	label = "";
           	myModuleList += '<li><a href="<%=contexturl %>ManageNextSteps/CreateNextSteps/'+obj[i].moduleId+'" id="nextStepsList_'+obj[i].moduleId+'" class="save">'+obj[i].moduleName+'</a></li>';
           }
           $("#List").html(myModuleList);
          },
          dataType: 'html'
         });
       }
	 
	 
	 
</script>
